/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2016.9.8   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>
#include <chipmunk/chipmunk.h>

using namespace nspi;
using namespace std;

static inline vec2 cpVect_Vec2(const cpVect& v)
{
    return vec2(v.x, v.y);
}

static inline vec3 cpVect_Vec3(const cpVect& v)
{
    return vec3(v.x, v.y, 0);
}

static inline cpVect Vec2_cpVect(const vec2& v)
{
    cpVect cpv;
    cpv.x = v.x;
    cpv.y = v.y;
    return cpv;
}

static inline cpVect Vec3_cpVect(const vec3& v)
{
    cpVect cpv;
    cpv.x = v.x;
    cpv.y = v.y;
    return cpv;
}

static inline cpBodyType piType_cpType(int type)
{
    switch (type)
    {
        case ePhysicsBody_Dynamic:
            return CP_BODY_TYPE_DYNAMIC;
        case ePhysicsBody_Static:
            return CP_BODY_TYPE_STATIC;
        case ePhysicsBody_Kinematic:
            return CP_BODY_TYPE_KINEMATIC;
        default:
            return CP_BODY_TYPE_DYNAMIC;
    }
}

static inline int cpType_piType(cpBodyType type)
{
    switch (type)
    {
        case CP_BODY_TYPE_DYNAMIC:
            return ePhysicsBody_Dynamic;
        case CP_BODY_TYPE_STATIC:
            return ePhysicsBody_Static;
        case CP_BODY_TYPE_KINEMATIC:
            return ePhysicsBody_Kinematic;
        default:
            return -1;
    }
}

//==================================================================================================

class PhysicsBody2D_Chipmunk : public iPhysicsBody2D
{
private:
    cpBody *mBody;
    bool mRelease;
    
public:
    PhysicsBody2D_Chipmunk(cpBody *body):
    mBody(body), mRelease(true)
    {
    }
    
    PhysicsBody2D_Chipmunk(cpBody *body, bool release):
    mBody(body), mRelease(release)
    {
    }
    
    virtual ~PhysicsBody2D_Chipmunk()
    {
        if (mBody != nullptr && mRelease)
        {
            cpBodyFree(mBody);
            mBody = nullptr;
        }
    }
    
    virtual void SetPosition(const vec3& position)
    {
        cpBodySetPosition(mBody, Vec3_cpVect(position));
    }
    
    virtual vec3 GetPosition() const
    {
        return cpVect_Vec3(cpBodyGetPosition(mBody));
    }
    
    // degrees，not radius
    virtual float GetAngle() const
    {
        return -cpBodyGetAngle(mBody) / M_PI * 180.0;
    }
    
    // degrees，not radius
    virtual void SetAngle(float degrees)
    {
        cpBodySetAngle(mBody, -degrees / 180.0 * M_PI);
    }
    
    virtual void SetMass(float mass)
    {
        cpBodySetMass(mBody, mass);
    }
    
    virtual float GetMass() const
    {
        return cpBodyGetMass(mBody);
    }
    
    virtual vec2 GetForce() const
    {
        return cpVect_Vec2(cpBodyGetForce(mBody));
    }
    
    virtual void SetForce(const vec2& force)
    {
        cpBodySetForce(mBody, Vec2_cpVect(force));
    }
    
    virtual float GetTorque() const
    {
        return cpBodyGetTorque(mBody);
    }
    
    virtual void SetTorque(float torque)
    {
        cpBodySetTorque(mBody, torque);
    }
    
    virtual void SetType(int type)
    {
        cpBodySetType(mBody, piType_cpType(type));
    }
    
    virtual int GetType() const
    {
        return cpType_piType(cpBodyGetType(mBody));
    }
    
    virtual vec3 GetVelocity() const
    {
        return cpVect_Vec3(cpBodyGetVelocity(mBody));
    }
    
    virtual void SetVelocity(const vec3& velocity)
    {
        cpBodySetVelocity(mBody, Vec3_cpVect(velocity));
    }
    
    virtual vec3 WorldToLocal(const vec3& point)
    {
        return cpVect_Vec3(cpBodyWorldToLocal(mBody, Vec3_cpVect(point)));
    }
    
    cpBody* GetHandle() const
    {
        return mBody;
    }
};

//==================================================================================================

class PhysicsShape2D_Chipmunk : public iPhysicsShape2D
{
private:
    cpShape *mShape;
    SmartPtr<iPhysicsBody2D> mBody;
    
public:
    PhysicsShape2D_Chipmunk(cpShape *shape, iPhysicsBody2D* body):
    mShape(shape), mBody(body)
    {
    }
    
    virtual ~PhysicsShape2D_Chipmunk()
    {
        if (mShape != nullptr)
        {
            cpShapeFree(mShape);
            mShape = nullptr;
        }
    }
    
    cpShape* GetHandle() const
    {
        return mShape;
    }
    
    virtual void SetElasticity(float value)
    {
        piAssert(value >= 0.0f && value <= 1.0f, ;);
        cpShapeSetElasticity(mShape, value);
    }
    
    virtual float GetElasticity() const
    {
        return cpShapeGetElasticity(mShape);
    }
};


//==================================================================================================


class PhysicsJoint2D_Chipmunk : public iPhysicsJoint2D
{
private:
    cpConstraint *mHandle;
    
public:
    PhysicsJoint2D_Chipmunk(cpConstraint* handle):
    mHandle(handle)
    {
    }
    
    virtual ~PhysicsJoint2D_Chipmunk()
    {
        if (mHandle != nullptr)
        {
            cpConstraintFree(mHandle);
            mHandle = nullptr;
        }
    }
    
    cpConstraint* GetHandle() const
    {
        return mHandle;
    }
    
    virtual float GetMaxForce() const
    {
        return cpConstraintGetMaxForce(mHandle);
    }
    
    virtual void SetMaxForce(float value)
    {
        cpConstraintSetMaxForce(mHandle, value);
    }
    
    virtual float GetMaxBias() const
    {
        return cpConstraintGetMaxBias(mHandle);
    }
    
    virtual void SetMaxBias(float value)
    {
        cpConstraintSetMaxBias(mHandle, value);
    }
    
    virtual float GetErrorBias() const
    {
        return cpConstraintGetErrorBias(mHandle);
    }
    
    virtual void SetErrorBias(float value)
    {
        cpConstraintSetErrorBias(mHandle, value);
    }
};

//==================================================================================================

class PhysicsWorld2D_Chipmunk : public iPhysicsWorld2D
{
private:
    cpSpace *mSpace;
    
public:
    PhysicsWorld2D_Chipmunk(const vec2& gravity, int32_t iterations)
    {
        mSpace = cpSpaceNew();
        
        cpVect g;
        g.x = gravity.x;
        g.y = gravity.y;
        cpSpaceSetGravity(mSpace, g);
        
        cpSpaceSetIterations(mSpace, iterations);
    }
    
    virtual ~PhysicsWorld2D_Chipmunk()
    {
    }
    
    virtual void Step(float delta)
    {
        cpSpaceStep(mSpace, delta);
    }
    
    virtual void SetGravity(const vec2& gravity)
    {
        cpSpaceSetGravity(mSpace, Vec2_cpVect(gravity));
    }
    
    virtual vec2 GetGravity() const
    {
        return cpVect_Vec2(cpSpaceGetGravity(mSpace));
    }
    
    virtual void SetIterations(int32_t steps)
    {
        piAssert(steps >= 1, ;);
        
        cpSpaceSetIterations(mSpace, steps);
    }
    
    virtual int32_t GetIterations() const
    {
        return cpSpaceGetIterations(mSpace);
    }
    
    virtual iPhysicsBody2D* GetStaticBody() const
    {
        return new PhysicsBody2D_Chipmunk(cpSpaceGetStaticBody(mSpace), false);
    }
    
    virtual iPhysicsBody2D* CreatePolygonBody(float mass,
                                              iVec3Array* vertices,
                                              const vec2& offset,
                                              float radius)
    {
        vector<cpVect> cpVertices(vertices->GetCount());
        for (int32_t i = 0; i < vertices->GetCount(); ++i)
        {
            cpVertices.push_back(Vec3_cpVect(vertices->GetItem(i)));
        }
        
        cpFloat moment = cpMomentForPoly(mass, (int)cpVertices.size(), cpVertices.data(), Vec2_cpVect(offset), radius);
        return new PhysicsBody2D_Chipmunk(cpBodyNew(mass, moment));
    }
    
    virtual iPhysicsBody2D* CreateCircleBody(float mass,
                                             float innerDiameter,
                                             float outerDiameter,
                                             const vec2& offset)
    {
        cpFloat moment = cpMomentForCircle(mass, innerDiameter, outerDiameter, Vec2_cpVect(offset));
        return new PhysicsBody2D_Chipmunk(cpBodyNew(mass, moment));
    }
    
    virtual iPhysicsBody2D* CreateSegmentBody(float mass,
                                              const vec2& begin,
                                              const vec2& end,
                                              float radius)
    {
        cpFloat moment = cpMomentForSegment(mass, Vec2_cpVect(begin), Vec2_cpVect(end), radius);
        return new PhysicsBody2D_Chipmunk(cpBodyNew(mass, moment));
    }
    
    virtual iPhysicsBody2D* CreateBoxBody(float mass, float width, float height)
    {
        cpFloat moment = cpMomentForBox(mass, width, height);
        return new PhysicsBody2D_Chipmunk(cpBodyNew(mass, moment));
    }
    
    virtual iPhysicsBody2D* CreateKinematicBody()
    {
        return new PhysicsBody2D_Chipmunk(cpBodyNewKinematic());
    }
    
    virtual iPhysicsBody2D* CreateStaticBody()
    {
        return new PhysicsBody2D_Chipmunk(cpBodyNewStatic());
    }
    
    virtual void AddBody(iPhysicsBody2D *body)
    {
        PhysicsBody2D_Chipmunk *b = dynamic_cast<PhysicsBody2D_Chipmunk*>(body);
        piAssert(b != nullptr, ;);
        
        body->Retain();
        cpSpaceAddBody(mSpace, b->GetHandle());
    }
    
    virtual void RemoveBody(iPhysicsBody2D *body)
    {
        PhysicsBody2D_Chipmunk *b = dynamic_cast<PhysicsBody2D_Chipmunk*>(body);
        piAssert(b != nullptr, ;);
        
        cpSpaceRemoveBody(mSpace, b->GetHandle());
        body->Release();
    }
    
    virtual bool ContainsBody(iPhysicsBody2D *body) const
    {
        PhysicsBody2D_Chipmunk *b = dynamic_cast<PhysicsBody2D_Chipmunk*>(body);
        piAssert(b != nullptr, false);
        
        return cpSpaceContainsBody(mSpace, b->GetHandle());
    }
    
    virtual iPhysicsShape2D* CreatePolygonShape(iPhysicsBody2D* body,
                                                iVec3Array* vertices,
                                                const vec2& offset,
                                                float radius)
    {
        PhysicsBody2D_Chipmunk *b = dynamic_cast<PhysicsBody2D_Chipmunk*>(body);
        piAssert(b != nullptr, nullptr);
        
        vector<cpVect> cpVertices(vertices->GetCount());
        for (int32_t i = 0; i < vertices->GetCount(); ++i)
        {
            cpVertices.push_back(Vec3_cpVect(vertices->GetItem(i)));
        }
        
        cpShape *shape = cpPolyShapeNew(b->GetHandle(), (int)cpVertices.size(), cpVertices.data(), cpTransformIdentity, radius);
        cpShapeSetUserData(shape, body);
        return new PhysicsShape2D_Chipmunk(shape, body);
    }
    
    virtual iPhysicsShape2D* CreateCircleShape(iPhysicsBody2D* body,
                                               float radius,
                                               const vec2& offset)
    {
        PhysicsBody2D_Chipmunk *b = dynamic_cast<PhysicsBody2D_Chipmunk*>(body);
        piAssert(b != nullptr, nullptr);
        
        cpShape *shape = cpCircleShapeNew(b->GetHandle(), radius, Vec2_cpVect(offset));
        return new PhysicsShape2D_Chipmunk(shape, body);
    }
    
    virtual iPhysicsShape2D* CreateSegmentShape(iPhysicsBody2D* body,
                                                const vec2& begin,
                                                const vec2& end,
                                                float radius)
    {
        PhysicsBody2D_Chipmunk *b = dynamic_cast<PhysicsBody2D_Chipmunk*>(body);
        piAssert(b != nullptr, nullptr);
        
        cpShape *shape = cpSegmentShapeNew(b->GetHandle(), Vec2_cpVect(begin), Vec2_cpVect(end), radius);
        return new PhysicsShape2D_Chipmunk(shape, body);
    }
    
    virtual iPhysicsShape2D* CreateBoxShape(iPhysicsBody2D* body,
                                            float width,
                                            float height,
                                            float radius)
    {
        PhysicsBody2D_Chipmunk *b = dynamic_cast<PhysicsBody2D_Chipmunk*>(body);
        piAssert(b != nullptr, nullptr);
        
        cpShape* shape = cpBoxShapeNew(b->GetHandle(), width, height, radius);
        return new PhysicsShape2D_Chipmunk(shape, body);
    }
    
    virtual void AddShape(iPhysicsShape2D *shape)
    {
        PhysicsShape2D_Chipmunk *s = dynamic_cast<PhysicsShape2D_Chipmunk*>(shape);
        piAssert(s != nullptr, ;);
        
        shape->Retain();
        cpSpaceAddShape(mSpace, s->GetHandle());
    }
    
    virtual void RemoveShape(iPhysicsShape2D *shape)
    {
        PhysicsShape2D_Chipmunk *s = dynamic_cast<PhysicsShape2D_Chipmunk*>(shape);
        piAssert(s != nullptr, ;);
        
        cpSpaceRemoveShape(mSpace, s->GetHandle());
        shape->Release();
    }
    
    virtual bool ContainsShape(iPhysicsShape2D *shape) const
    {
        PhysicsShape2D_Chipmunk *s = dynamic_cast<PhysicsShape2D_Chipmunk*>(shape);
        piAssert(s != nullptr, false);
        
        return cpSpaceContainsShape(mSpace, s->GetHandle());
    }
    
    //==============================================================================================
    // Joint
    
    virtual iPhysicsJoint2D* CreatePivotJointPivot(iPhysicsBody2D* bodyA,
                                                   iPhysicsBody2D* bodyB,
                                                   const vec3& pivot)
    {
        PhysicsBody2D_Chipmunk *a = dynamic_cast<PhysicsBody2D_Chipmunk*>(bodyA);
        piAssert(a != nullptr, nullptr);
        
        PhysicsBody2D_Chipmunk *b = dynamic_cast<PhysicsBody2D_Chipmunk*>(bodyB);
        piAssert(b != nullptr, nullptr);
        
        cpConstraint *c = cpPivotJointNew(a->GetHandle(),
                                          b->GetHandle(),
                                          Vec3_cpVect(pivot));
        return new PhysicsJoint2D_Chipmunk(c);
    }
    
    virtual iPhysicsJoint2D* CreatePivotJointAnchors(iPhysicsBody2D* bodyA,
                                                     iPhysicsBody2D* bodyB,
                                                     const vec3& anchorA,
                                                     const vec3& anchorB)
    {
        PhysicsBody2D_Chipmunk *a = dynamic_cast<PhysicsBody2D_Chipmunk*>(bodyA);
        piAssert(a != nullptr, nullptr);
        
        PhysicsBody2D_Chipmunk *b = dynamic_cast<PhysicsBody2D_Chipmunk*>(bodyB);
        piAssert(b != nullptr, nullptr);
        
        cpConstraint *c = cpPivotJointNew2(a->GetHandle(),
                                           b->GetHandle(),
                                           Vec3_cpVect(anchorA),
                                           Vec3_cpVect(anchorB));
        return new PhysicsJoint2D_Chipmunk(c);
    }
    
    virtual void AddJoint(iPhysicsJoint2D* joint)
    {
        PhysicsJoint2D_Chipmunk *j = dynamic_cast<PhysicsJoint2D_Chipmunk*>(joint);
        piAssert(j != nullptr, ;);
        
        cpSpaceAddConstraint(mSpace, j->GetHandle());
    }
    
    virtual void RemoveJoint(iPhysicsJoint2D* joint)
    {
        PhysicsJoint2D_Chipmunk *j = dynamic_cast<PhysicsJoint2D_Chipmunk*>(joint);
        piAssert(j != nullptr, ;);
        
        cpSpaceRemoveConstraint(mSpace, j->GetHandle());
    }
    
    virtual bool ContainsJoint(iPhysicsJoint2D* joint) const
    {
        PhysicsJoint2D_Chipmunk *j = dynamic_cast<PhysicsJoint2D_Chipmunk*>(joint);
        piAssert(j != nullptr, false);
        
        return cpSpaceContainsConstraint(mSpace, j->GetHandle());
    }
};

iPhysicsWorld2D* piCreatePhysicsWorld_Chipmunk(const vec2& gravity, int32_t iterations)
{
    piAssert(iterations >= 1, nullptr);
    return new PhysicsWorld2D_Chipmunk(gravity, iterations);
}































