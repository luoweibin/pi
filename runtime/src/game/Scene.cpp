/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.23   1     Create
 ******************************************************************************/
#include "../asset/AssetImpl.h"

#include <pi/lua/LuaScript.h>

using namespace std;

NSPI_BEGIN()

extern float g_Near;
extern float g_Far;

class SceneScriptLoader : public iScriptLoader
{
private:
    iAssetManager* mAssetMgr;
    iClassLoader* mClassLoader;
    
public:
    SceneScriptLoader(iAssetManager* assetMgr, iClassLoader* classLoader):
    mAssetMgr(assetMgr), mClassLoader(classLoader)
    {
    }
    
    virtual ~SceneScriptLoader()
    {
    }
    
    virtual iMemory* Load(const std::string& path)
    {
        SmartPtr<iMemoryAsset> asset = piLoadAsset<iMemoryAsset>(mAssetMgr, mClassLoader, path);
        piCheck(!asset.IsNull(), nullptr);
        
        SmartPtr<iMemory> mem = asset->GetMemory();
        asset.PtrAndSetNull();
        
        return mem.PtrAndSetNull();
    }
};

class Scene : public AssetImpl<iScene>
{
protected:
    SmartPtr<iEntity> mRootEntity;
    SmartPtr<iSystemArray> mSystems;
    
    SmartPtr<iI32Array> mCaps;
    
    SmartPtr<iAssetManager> mAssetMgr;
    
    iGame* mGame;
    
    SmartPtr<iMessageQueue> mLowMQ;
    SmartPtr<iMessageQueue> mNormalMQ;
    SmartPtr<iMessageQueue> mHighMQ;
    
    SmartPtr<iEntityArray> mCameraEntities;
    
    vec2 mDesignSize;
    
    vec2 mResolution;
    
    float mRenderScale;
    
    // using to set pixel size of 2D render. If this value is 1.0, the pixel of 2D obj will be
    // calculated base by the designSize.x. default value is 0.0 for the system has been designed
    // before 2017.7.
    float mXScaling;
    
    float mPixelSize;
    
    mat4 mFaceProjMatrix;
    
    string mName;
    
    SmartPtr<iScript> mScript;
    
public:
    Scene():
    mGame(nullptr),
    mDesignSize(720, 1280)
    , mResolution(720, 1280)
    , mXScaling(0.0f)
    , mRenderScale(1.0f)
    {
        mCaps = CreateI32Array();
        mSystems = CreateSystemArray();
        
        mLowMQ = CreateMessageQueue();
        mNormalMQ = CreateMessageQueue();
        mHighMQ = CreateMessageQueue();
        
        mCameraEntities = CreateEntityArray();
        
        mScript = CreateLuaScript();
        
        mPixelSize = mDesignSize.x * GetXScaling() + mDesignSize.y * (1.0f - GetXScaling());
        
        mFaceProjMatrix = piProject(0,
                                    720,
                                    0,
                                    1280,
                                    g_Near,
                                    g_Far);
    }
    
    virtual ~Scene()
    {
    }
    
    virtual void SetName(const string& name)
    {
        mName = name;
    }
    
    virtual string GetName() const
    {
        return mName;
    }
    
    virtual void SetRootEntity(iEntity* entity)
    {
        piAssert(entity != nullptr, ;);
        mRootEntity = entity;
    }
    
    virtual iEntity* GetRootEntity() const
    {
        return mRootEntity;
    }
    
    virtual iScript* GetScript() const
    {
        return mScript;
    }
    
    virtual iHID* GetHID() const
    {
        return mGame->GetHID();
    }
    
    virtual iMotionManager* GetMotionManager() const
    {
        return mGame->GetMotionManager();
    }
    
    virtual iClassLoader* GetClassLoader() const
    {
        return mGame->GetClassLoader();
    }
    
    virtual void SetGame(iGame* game)
    {
        mGame = game;
    }
    
    virtual void SetAssetManager(iAssetManager* manager)
    {
        piAssert(manager != nullptr, ;);
        mAssetMgr = manager;
    }
    
    virtual iAssetManager* GetAssetManager() const
    {
        return mAssetMgr;
    }
    
    virtual void AddEntity(iEntity* entity)
    {
        piAssert(entity != nullptr, ;);
        
        SmartPtr<iCamera> camera = piGetEntityComponent<iCamera>(entity);
        if (!camera.IsNull())
        {
            mCameraEntities->PushBack(entity);
        }
        
        entity->SetScene(this);
    }
    
    virtual void AddEntityRecursively(iEntity* entity)
    {
        AddEntity(entity);
        
        SmartPtr<iEntityArray> children = entity->GetChildren();
        for (auto i = 0; i < children->GetCount(); ++i)
        {
            SmartPtr<iEntity> child = children->GetItem(i);
            AddEntityRecursively(child);
        }
    }
    
    virtual void RemoveEntity(iEntity* entity)
    {
        piAssert(entity != nullptr, ;);
        
        entity->SetScene(nullptr);
        
        RemoveFromCameraEntities(entity);
        
        for (int32_t i = 0; i < mSystems->GetCount(); ++i)
        {
            SmartPtr<iSystem> s = mSystems->GetItem(i);
            s->RemoveEntity(entity);
        }
    }
    
    virtual void RemoveEntityRecursively(iEntity* entity)
    {
        RemoveEntity(entity);
        
        SmartPtr<iEntityArray> children = entity->GetChildren();
        for (auto i = 0; i < children->GetCount(); ++i)
        {
            SmartPtr<iEntity> child = children->GetItem(i);
            RemoveEntityRecursively(child);
        }
    }
    
    
    virtual void AddSystem(iSystem* system)
    {
        piAssert(system != nullptr, ;);
        
        for (int32_t i = 0; i < mSystems->GetCount(); ++i)
        {
            SmartPtr<iSystem> s = mSystems->GetItem(i);
            if (s == system)
            {
                return;
            }
        }
        
        system->SetScene(this);
        mSystems->PushBack(system);
    }
    
    virtual void RemoveSystem(iSystem* system)
    {
        for (int32_t i = 0; i < mSystems->GetCount(); ++i)
        {
            SmartPtr<iSystem> s = mSystems->GetItem(i);
            if (s == system)
            {
                mSystems->Remove(i);
                break;
            }
        }
    }
    
    virtual void PostLocalMessage(iSystem* sender, int priority, int32_t message, const Var& arg1, const Var& arg2)
    {
        switch (priority)
        {
            case eGameMsgPri_High:
                mHighMQ->PostMessage(sender, message, arg1, arg2);
                break;
            case eGameMsgPri_Low:
                mLowMQ->PostMessage(sender, message, arg1, arg2);
                break;
            case eGameMsgPri_Normal:
            default:
                mNormalMQ->PostMessage(sender, message, arg1, arg2);
                break;
        }
    }
    
    virtual void PostGlobalMessage(int priority, int32_t message, const Var& arg1, const Var& arg2)
    {
        mGame->PostMessage(this, priority, message, arg1, arg2);
    }
    
    virtual void NotifyEntityChanged(iEntity* entity)
    {
        SmartPtr<iClassArray> entityClassArray = entity->GetCompClassArray();
        
        for (int32_t i = 0; i < mSystems->GetCount(); ++i)
        {
            SmartPtr<iSystem> s = mSystems->GetItem(i);
            if (!s->Accept(entity))
            {
                s->RemoveEntity(entity);
            }
        }
        
        SmartPtr<iCamera> camera = piGetEntityComponent<iCamera>(entity);
        if (camera.IsNull())
        {
            RemoveFromCameraEntities(entity);
        }
    }
    
    virtual void SetCaps(iI32Array* caps)
    {
        piAssert(caps != nullptr, ;);
        mCaps = caps;
    }
    
    virtual iI32Array* GetCaps() const
    {
        return mCaps;
    }
    
    virtual iEntity* FindEntity(const std::string& name) const
    {
        piAssert(!name.empty(), nullptr);
        
        return FindEntityRecursively(mRootEntity, name);
    }
    
    virtual rect GetBounds() const
    {
        return mGame->GetBounds();
    }
    
    virtual iEntityArray* GetCameraEntities() const
    {
        return mCameraEntities;
    }
    
    virtual iEntity* GetFirstActiveCameraEntity() const
    {
        for (auto i = 0; i < mCameraEntities->GetCount(); ++i)
        {
            SmartPtr<iEntity> e = mCameraEntities->GetItem(i);
            SmartPtr<iCamera> cam = piGetEntityComponent<iCamera>(e);
            if (cam->IsActive())
            {
                return e;
            }
        }
        
        return nullptr;
    }
    
    /**
     * -----------------------------------------------------------------------------------------
     * DesignSize
     */
    
    virtual vec2 GetDesignSize() const
    {
        return mDesignSize;
    }
    
    PISetter()
    virtual void SetDesignSize(const vec2& value)
    {
        mDesignSize = value;
        mPixelSize = mDesignSize.x * GetXScaling() + mDesignSize.y * (1.0f - GetXScaling());
    }
    
    virtual float GetXScaling() const
    {
        return mXScaling;
    }
    
    virtual void SetXScaling(float percent)
    {
        mXScaling = percent;
        mPixelSize = mDesignSize.x * GetXScaling() + mDesignSize.y * (1.0f - GetXScaling());
    }

    virtual mat4 GetFaceProjMatrix()
    {
        return mFaceProjMatrix;
    }
    
    virtual float GetPixelSize() const
    {
        return mPixelSize;
    }
    
    virtual float GetRenderScale() const
    {
        return mRenderScale;
    }

    virtual vec2 VideoUVToScreenPos(const vec2& uv)
    {
        vec2 result = vec2(uv.x*GetDesignSize().x,uv.y*GetDesignSize().y);
        
        return result;
    }
    
    virtual vec3 VideoUVToScreenPosV3(const vec3& uv)
    {
        vec3 result = vec3(uv.x*GetDesignSize().x,uv.y*GetDesignSize().y,uv.z);
        
        return result;
    }
    
    virtual vec2 ScreenSizeToGLOrthSize(const vec2& screenSize)
    {
        if (mResolution.y <= 0.0f)
            return vec2();
        
        float ratio = mResolution.x / mResolution.y;

        vec2 result;
        
        result.x = screenSize.x / GetDesignSize().x * ratio;
        result.y = screenSize.y / GetDesignSize().y;
        
        return result;
    }
    
    virtual vec3 ScreenSizeToGLOrthSizeV3(const vec3& screenSize)
    {
        if (mResolution.y <= 0.0f)
            return vec3();
        
        float ratio = mResolution.x / mResolution.y;
        
        vec3 result;
        
        result.x = screenSize.x / GetDesignSize().x * ratio;
        result.y = screenSize.y / GetDesignSize().y;
        result.z = 0;
        
        return result;
    }
    
    virtual vec2 ScreenPosToGLOrthPos(const vec2& pos)
    {
        if (mResolution.y <= 0.0f)
            return vec2();
        
        float ratio = mResolution.x / mResolution.y;
        
        
        vec2 result;
        
        result.x = pos.x / GetDesignSize().x * ratio - 0.5f * ratio;
        result.y = pos.y / GetDesignSize().y - 0.5f;
     
        return result;
    }
    
    virtual vec3 ScreenPosToGLOrthPosV3(const vec3& pos)
    {
        if (mResolution.y <= 0.0f)
            return vec3();
        
        float ratio = mResolution.x / mResolution.y;

        vec3 result;
        
        result.x = pos.x / GetDesignSize().x * ratio - 0.5f * ratio;
        result.y = pos.y / GetDesignSize().y - 0.5f;
        result.z = 0.0f;
        
        return result;
    }
    
    /**
     * ---------------------------------------------------------------------------------------------
     * Callbacks
     */
    
    virtual void OnLoad()
    {
        piAssert(!mAssetMgr.IsNull(), ;);
        mAssetMgr->OnLoad();
        
        mScript->RegisterLoader(new SceneScriptLoader(mAssetMgr, GetClassLoader()));
        
        piEach<iSystemArray, iSystem*>(mSystems.Ptr(),
                                       [](iSystem* s) {
                                           s->OnLoad();
                                       });
        
        EntityOnLoadRecursively(mRootEntity);
    }
    
    virtual void OnLoadFromEntityArray(iSystemArray* systems, iEntity* entity)
    {
        for (int32_t j = 0; j < systems->GetCount(); ++j)
        {
            SmartPtr<iSystem> system = systems->GetItem(j);
            if (system->Accept(entity))
            {
                system->AddEntity(entity);
            }
        }
        
        SmartPtr<iEntityArray> array = entity->GetChildren();
        
        for (int32_t i = 0; i < array->GetCount(); ++i)
        {
            SmartPtr<iEntity> child = array->GetItem(i);
            OnLoadFromEntityArray(systems,child);
        }
    }
    
    virtual void OnUnload()
    {
        EntityOnUnloadRecursively(mRootEntity);
        
        piEach<iSystemArray, iSystem*>(mSystems.Ptr(),
                                       [](iSystem* s) {
                                           s->OnUnload();
                                       });
        
        mSystems->Clear();
        mCameraEntities = nullptr;
        
        mScript->OnUnload();
        mScript->GarbageCollect();
        mScript = nullptr;
        
        mRootEntity = nullptr;
        
        if (!mAssetMgr.IsNull())
        {
            mAssetMgr->OnUnload();
        }
    }
    
    virtual void OnUpdate(float delta)
    {
        PILOGD(PI_GAME_TAG, "delta:%0.3f", delta);
        
        piPushGroupMarker("Scene:OnUpdate");
        
        // messages must be handled before update.
        SmartPtr<iMessage> msg;
        while (!(msg = PopMessage()).IsNull())
        {
            OnMessage(msg);
        }
        
        EntityUpdateRecursively(mRootEntity, delta);
        
        piEach<iSystemArray, iSystem*>(mSystems.Ptr(),
                                       [delta](iSystem* s) {
                                           s->OnBeforeUpdate(delta);
                                           s->OnUpdate(delta);
                                           s->OnAfterUpdate(delta);
                                       });
        
        mScript->GarbageCollect();
        
        piPopGroupMarker();
    }
    
    virtual bool OnMessage(iMessage* msg)
    {
        if (EntityOnMessageRecursively(mRootEntity, msg))
        {
            return true;
        }
        
        for (int32_t i = 0; i < mSystems->GetCount(); ++i)
        {
            SmartPtr<iSystem> s = mSystems->GetItem(i);
            SmartPtr<iRefObject> sender = msg->GetSender();
            if (s != sender)
            {
                if (s->OnMessage(msg))
                {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    virtual bool OnHIDEvent(iHIDEvent* event)
    {
        if (EntityOnHIDEventRecursively(mRootEntity, event))
        {
            return true;
        }
        
        for (int32_t i = 0; i < mSystems->GetCount(); ++i)
        {
            SmartPtr<iSystem> s = mSystems->GetItem(i);
            if (s->OnHIDEvent(event))
            {
                return true;
            }
        }
        
        return false;
    }
    
    virtual void OnResize(const rect& bounds)
    {
        //float scaleX = bounds.width /  GetDesignSize().x;
        //float scaleY = bounds.height / GetDesignSize().y;
        
        mResolution.x = bounds.width;
        mResolution.y = bounds.height;
        
        //mRenderScale = bounds.height / GetDesignSize().y;//scaleX*GetXScaling() + scaleY*(1.0f - GetXScaling()); //max(scaleX,scaleY);
        
        mFaceProjMatrix = piProject(bounds.x,
                                    bounds.x + bounds.width,
                                    bounds.y,
                                    bounds.y + bounds.height,
                                    g_Near,
                                    g_Far);
        
        EntityOnResizeRecursively(mRootEntity, bounds);
        
        piEach<iSystemArray, iSystem*>(mSystems.Ptr(),
                                       [bounds](iSystem* s) {
                                           s->OnResize(bounds);
                                       });
    }
    
    virtual void SwapBuffer()
    {
        return mGame->SwapBuffer();
    }
    
    virtual int32_t GetBackBufferTex() const
    {
        return mGame->GetBackBufferTex();
    }
    
    virtual int32_t GetFrontBufferTex() const
    {
        return mGame->GetFrontBufferTex();
    }
    
private:
    
    void RemoveFromCameraEntities(iEntity* entity)
    {
        for (auto i = 0; i < mCameraEntities->GetCount(); ++i)
        {
            if (mCameraEntities->GetItem(i) == entity)
            {
                mCameraEntities->Remove(i);
                break;
            }
        }
    }
    
    iMessage* PopMessage()
    {
        if (!mHighMQ->Empty())
        {
            return mHighMQ->PopMessage();
        }
        
        if (!mNormalMQ->Empty())
        {
            return mNormalMQ->PopMessage();
        }
        
        if (!mLowMQ->Empty())
        {
            return mLowMQ->PopMessage();
        }
        
        return nullptr;
    }
    
    iEntity* FindEntityRecursively(iEntity* entity, const string& name) const
    {
        if (name == entity->GetName())
        {
            return entity;
        }
        
        SmartPtr<iEntityArray> children = entity->GetChildren();
        for (int i = 0; i < children->GetCount(); ++i)
        {
            SmartPtr<iEntity> child = children->GetItem(i);
            SmartPtr<iEntity> ret = FindEntityRecursively(child, name);
            if (!ret.IsNull())
            {
                return ret;
            }
        }
        
        return nullptr;
    }
    
    void EntityOnLoadRecursively(iEntity* entity)
    {
        entity->OnLoad();
        
        for (int j = 0; j < mSystems->GetCount(); ++j)
        {
            SmartPtr<iSystem> system = mSystems->GetItem(j);
            
            if (system->Accept(entity))
            {
                system->AddEntity(entity);
            }
        }
        
        SmartPtr<iEntityArray> children = entity->GetChildren();
        for (int i = 0; i < children->GetCount(); ++i)
        {
            SmartPtr<iEntity> child = children->GetItem(i);
            EntityOnLoadRecursively(child);
        }
    }
    
    void EntityOnUnloadRecursively(iEntity* entity)
    {
        entity->OnUnload();
        
        for (int j = 0; j < mSystems->GetCount(); ++j)
        {
            SmartPtr<iSystem> system = mSystems->GetItem(j);
            system->RemoveEntity(entity);
        }
        
        SmartPtr<iEntityArray> children = entity->GetChildren();
        for (int i = 0; i < children->GetCount(); ++i)
        {
            SmartPtr<iEntity> child = children->GetItem(i);
            EntityOnUnloadRecursively(child);
        }
    }
    
    void EntityUpdateRecursively(iEntity* entity, float delta)
    {
        piCheck(entity->IsActive(), ;);
        
        entity->OnUpdate(delta);
        
        SmartPtr<iEntityArray> children = entity->GetChildren();
        for (int i = 0; i < children->GetCount(); ++i)
        {
            SmartPtr<iEntity> child = children->GetItem(i);
            EntityUpdateRecursively(child,delta);
        }
    }
    
    bool EntityOnMessageRecursively(iEntity* entity, iMessage* message)
    {
        piCheck(entity->IsActive(), false);

        if (entity->OnMessage(message))
        {
            return true;
        }
        
        SmartPtr<iEntityArray> children = entity->GetChildren();
        for (auto i = 0; i < children->GetCount(); ++i)
        {
            SmartPtr<iEntity> e = children->GetItem(i);
            if (EntityOnMessageRecursively(e,message))
            {
                return true;
            }
        }
        
        return false;
    }
    
    bool EntityOnHIDEventRecursively(iEntity* entity, iHIDEvent* event)
    {
        piCheck(entity->IsActive(), false);

        if (entity->OnHIDEvent(event))
        {
            return true;
        }
        
        SmartPtr<iEntityArray> children = entity->GetChildren();
        for (auto i = 0; i < children->GetCount(); ++i)
        {
            SmartPtr<iEntity> e = children->GetItem(i);
            if (EntityOnHIDEventRecursively(e,event))
            {
                return true;
            }
        }
        
        return false;
    }
    
    void EntityOnResizeRecursively(iEntity* entity, const rect& bounds)
    {
        entity->OnResize(bounds);
        
        SmartPtr<iEntityArray> children = entity->GetChildren();
        for (auto i = 0; i < children->GetCount(); ++i)
        {
            SmartPtr<iEntity> e = children->GetItem(i);
            EntityOnResizeRecursively(e,bounds);
        }
    }
};


iScene* CreateScene()
{
    return new Scene();
}


NSPI_END()






















