/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.7.3   1.0     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()


class AnimTrack : public iAnimTrack
{
private:
    SmartPtr<iAnimKeyArray> mKeys;
    SmartPtr<iAnimator> mAnimator;
    float mBegin;
    bool mRepeat;
    
public:
    AnimTrack():
    mBegin(0)
    {
        mKeys = CreateAnimKeyArray();
    }
    
    virtual ~AnimTrack()
    {
    }
    
    virtual iAnimKeyArray* GetKeys() const
    {
        return mKeys;
    }
    
    virtual void SetKeys(iAnimKeyArray* keys)
    {
        piAssert(keys != nullptr, ;);
        mKeys = keys;
    }
    
    virtual float GetBegin() const
    {
        return mBegin;
    }
    
    virtual void SetBegin(float value)
    {
        mBegin = value;
    }
    
    virtual float GetEnd() const
    {
        if (!mKeys->IsEmpty())
        {
            return mKeys->GetBack()->GetTime();
        }
        else
        {
            return 0;
        }
    }
    
    virtual bool IsRepeat() const
    {
        return mRepeat;
    }
    
    virtual void SetRepeat(bool value)
    {
        mRepeat = value;
    }
    
    virtual iAnimator* GetAnimator() const
    {
        return mAnimator;
    }
    
    virtual void SetAnimator(iAnimator* animator)
    {
        piAssert(animator != nullptr, ;);
        mAnimator = animator;
    }
};

iAnimTrack* CreateAnimTrack()
{
    return new AnimTrack();
}


NSPI_END()















