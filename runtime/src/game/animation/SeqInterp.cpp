/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.30   1     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()


class SeqInterp : public iSeqInterp
{
private:
    SmartPtr<iAnimSeq> mSeq;
    string mSeqUri;
    
public:
    SeqInterp()
    {
    }
    
    virtual ~SeqInterp()
    {
    }
    
    virtual string GetUri() const
    {
        return mSeqUri;
    }
    
    virtual void SetUri(const string& uri)
    {
        piAssert(!uri.empty(), ;);
        mSeqUri = uri;
    }
    
    virtual iAnimSeq* GetSeq() const
    {
        return mSeq;
    }
    
    virtual void SetSeq(iAnimSeq* seq)
    {
        piAssert(seq != nullptr, ;);
        mSeq = seq;
    }
    
    virtual Var Evaluate(float time)
    {
        piCheck(!mSeq.IsNull(), Var());
        
        int32_t count = mSeq->GetFrames()->GetCount();
        int32_t frame = floor(count * time);
        return mSeq->GetFrame(frame);
    }
};


iSeqInterp* CreateSeqInterp()
{
    return new SeqInterp();
}


NSPI_END()























