/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.3   1.0     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()


class AnimChannel : public iAnimChannel
{
private:
    SmartPtr<iAnimCurveKeyArray> mKeys;
    iProperty* mProp;
    iRefObject* mTarget;
    
public:
    AnimChannel():
    mTarget(nullptr), mProp(nullptr)
    {
        mKeys = CreateAnimCurveKeyArray();
    }
    
    virtual ~AnimChannel()
    {
    }
    
    virtual void SetProperty(iProperty* prop)
    {
        piAssert(prop != nullptr, ;);
        mProp = prop;
    }
    
    virtual iProperty* GetProperty() const
    {
        return mProp;
    }
    
    virtual iAnimCurveKeyArray* GetKeys() const
    {
        return mKeys;
    }
    
    virtual void SetKeys(iAnimCurveKeyArray* keys)
    {
        piAssert(keys != nullptr, ;);
        mKeys = keys;
    }
    
    virtual void SetTarget(iRefObject* target)
    {
        piAssert(target != nullptr, ;);
        mTarget = target;
    }
    
    virtual iRefObject* GetTarget() const
    {
        return mTarget;
    }
};

iAnimChannel* CreateAnimChannel()
{
    return new AnimChannel();
}


NSPI_END()














