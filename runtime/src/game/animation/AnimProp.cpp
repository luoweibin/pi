/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.23   1     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()

class AnimProp : public iAnimProp
{
private:
    iRefObject* mTarget;
    iProperty* mProp;
    
public:
    AnimProp(iRefObject* target, iProperty* prop):
    mTarget(target), mProp(prop)
    {
    }
    
    virtual ~AnimProp()
    {
    }
    
    virtual void SetValue(const Var& value)
    {
        mProp->SetValue(mTarget, value);
    }
    
    virtual int GetType() const
    {
        return mProp->GetTypeClass()->GetType();
    }
};


iAnimProp* CreateAnimProp(iRefObject* target, iProperty* prop)
{
    return new AnimProp(target, prop);
}


NSPI_END()























