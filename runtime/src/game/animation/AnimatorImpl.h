/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.29   0.1     Create
 ******************************************************************************/
#ifndef PI_SRC_GAME_ANIMATION_ANIMATORIMPL_H
#define PI_SRC_GAME_ANIMATION_ANIMATORIMPL_H
#include <pi/game/impl/ComponentImpl.h>

using namespace std;

NSPI_BEGIN()

template <class I>
class AnimatorImpl : public ComponentImpl<I>
{
protected:
    string mCompName;
    string mPropName;
    bool mIgnore;
    
public:
    AnimatorImpl():
    mIgnore(false)
    {
    }
    
    virtual ~AnimatorImpl()
    {
    }
    
    virtual std::string GetComp() const
    {
        return mCompName;
    }
    
    virtual void SetComp(const string& name)
    {
        mCompName = name;
        mIgnore = false;
    }
    
    virtual std::string GetProperty() const
    {
        return mPropName;
    }
    
    virtual void SetProperty(const string& name)
    {
        mPropName = name;
        mIgnore = false;
    }
    
protected:
    
    Var Evaluate(int mode, const Var& leftValue, const Var& rightValue, float time)
    {
        Var value;
        switch (mode)
        {
            case eInterp_Const:
                return leftValue;
            case eInterp_Step:
                return Step(leftValue, rightValue, time);
            case eInterp_Linear:
                return Linear(leftValue, rightValue, time);
            case eInterp_Cosine:
                return Linear(leftValue, rightValue, -cos(M_PI * time) / 2 + 0.5);
            case eInterp_SmoothStep:
                return Linear(leftValue, rightValue, time * time * (3 - 2 * time));
            case eInterp_Accel:
                return Linear(leftValue, rightValue, time * time);
            case eInterp_Decel:
                return Linear(leftValue, rightValue, 1 - (1 - time * time));
            default:
                return leftValue;
        }
    }
    
    Var Step(const Var& leftValue, const Var& rightValue, float time)
    {
        return time < 0.5 ? leftValue : rightValue;
    }
    
    Var Linear(const Var& leftValue, const Var& rightValue, float time)
    {
        switch (leftValue.GetType())
        {
            case eType_I8:
            case eType_U8:
            case eType_I16:
            case eType_U16:
            case eType_I32:
            case eType_U32:
            case eType_I64:
            case eType_U64:
            case eType_F32:
            case eType_F64:
                return piglm::mix(leftValue.GetF32(), rightValue.GetF32(), time);
            case eType_Vec2:
                return piglm::mix(leftValue.GetVec2(), rightValue.GetVec2(), time);
            case eType_Vec3:
                return piglm::mix(leftValue.GetVec3(), rightValue.GetVec3(), time);
            case eType_Vec4:
                return piglm::mix(leftValue.GetVec4(), rightValue.GetVec4(), time);
            case eType_Quat:
                return piglm::slerp(leftValue.GetQuat(), rightValue.GetQuat(), time);
            case eType_Mat4:
                return piglm::interpolate(leftValue.GetMat4(), rightValue.GetMat4(), time);
            case eType_Rect:
            {
                rect leftRect = leftValue;
                rect rightRect = rightValue;
                rect value;
                value.x = piglm::mix(leftRect.x, rightRect.x, time);
                value.y = piglm::mix(leftRect.y, rightRect.y, time);
                value.width = piglm::mix(leftRect.width, rightRect.width, time);
                value.height = piglm::mix(leftRect.height, rightRect.height, time);
                return value;
            }
            default:
                return leftValue;
        }
    }
};


NSPI_END()


#endif
























