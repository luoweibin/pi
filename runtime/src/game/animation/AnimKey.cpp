/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.7.3   1.0     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()

class AnimKey : public iAnimKey
{
private:
    Var mLeft;
    Var mRight;
    int mMode;
    float mTime;
    
public:
    AnimKey():
    mMode(eInterp_Linear), mTime(0)
    {
    }
    
    virtual ~AnimKey()
    {
    }
    
    virtual void SetLeft(const Var& value)
    {
        mLeft = value;
    }
    
    virtual Var GetLeft() const
    {
        return mLeft;
    }
    
    virtual void SetRight(const Var& value)
    {
        mRight = value;
    }
    
    virtual Var GetRight() const
    {
        return mRight;
    }
    
    virtual void SetMode(int mode)
    {
        mMode = mode;
    }
    
    virtual int GetMode() const
    {
        return mMode;
    }
    
    virtual float GetTime() const
    {
        return mTime;
    }
    
    virtual void SetTime(float value)
    {
        mTime = value;
    }
    
    virtual Var Evaluate(float time)
    {
        switch (mMode)
        {
            case eInterp_Step:
                return Step(time);
            case eInterp_Linear:
                return Linear(time);
            case eInterp_Cosine:
                return Linear(-cos(M_PI * time) / 2 + 0.5);
            case eInterp_SmoothStep:
                return Linear(time * time * (3 - 2 * time));
            case eInterp_Accel:
                return Linear(time * time);
            case eInterp_Decel:
                return Linear(1 - (1 - time * time));
            default:
                return mLeft;
        }
    }
    
private:
    
    Var Step(float time)
    {
        return time < 0.5 ? mLeft : mRight;
    }
    
    Var Linear(float time)
    {
        switch (mLeft.GetType())
        {
            case eType_I8:
            case eType_U8:
            case eType_I16:
            case eType_U16:
            case eType_I32:
            case eType_U32:
            case eType_I64:
            case eType_U64:
            case eType_F32:
            case eType_F64:
                return piglm::mix(mLeft.GetF32(), mRight.GetF32(), time);
            case eType_Vec2:
                return piglm::mix(mLeft.GetVec2(), mRight.GetVec2(), time);
            case eType_Vec3:
                return piglm::mix(mLeft.GetVec3(), mRight.GetVec3(), time);
            case eType_Vec4:
                return piglm::mix(mLeft.GetVec4(), mRight.GetVec4(), time);
            case eType_Quat:
                return piglm::slerp(mLeft.GetQuat(), mRight.GetQuat(), time);
            case eType_Mat4:
                return piglm::interpolate(mLeft.GetMat4(), mRight.GetMat4(), time);
            case eType_Rect:
            {
                rect left = mLeft;
                rect right = mRight;
                rect value;
                value.x = piglm::mix(left.x, right.x, time);
                value.y = piglm::mix(left.y, right.y, time);
                value.width = piglm::mix(left.width, right.width, time);
                value.height = piglm::mix(left.height, right.height, time);
                return value;
            }
            default:
                return mLeft;
        }
    }
};


iAnimKey* CreateAnimKey()
{
    return new AnimKey();
}

NSPI_END()





















