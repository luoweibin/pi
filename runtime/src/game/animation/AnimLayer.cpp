/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.3   1.0     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()

class AnimLayer : public iAnimLayer
{
private:
    string mName;
    SmartPtr<iAnimChannelArray> mChannels;
    
public:
    AnimLayer()
    {
        mChannels = CreateAnimChannelArray();
    }
    
    virtual ~AnimLayer()
    {
    }
    
    virtual std::string GetName() const
    {
        return mName;
    }
    
    virtual void SetName(const std::string& name)
    {
        mName = name;
    }
    
    virtual iAnimChannelArray* GetChannels() const
    {
        return mChannels;
    }
    
    virtual void SetChannels(iAnimChannelArray* channels)
    {
        piAssert(channels != nullptr, ;);
        mChannels = channels;
    }
};

iAnimLayer* CreateAnimLayer()
{
    return new AnimLayer();
}


NSPI_END()














