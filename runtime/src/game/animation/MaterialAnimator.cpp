/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.30   1     Create
 ******************************************************************************/
#include "AnimatorImpl.h"

using namespace std;

NSPI_BEGIN()

class MaterialAnimator : public AnimatorImpl<iMaterialAnimator>
{
private:
    SmartPtr<iMaterialProp> mProp;
    
public:
    MaterialAnimator()
    {
    }
    
    virtual ~MaterialAnimator()
    {
    }
    
    virtual void OnUpdate(iEntity* entity, iAnimKey* leftKey, iAnimKey* rightKey, float time)
    {
        piCheck(!mIgnore, ;);
        
        if (mProp.IsNull())
        {
            SmartPtr<iDynamicMesh> mesh = piGetEntityComponent<iDynamicMesh>(entity);
            if (mesh.IsNull())
            {
                mIgnore = true;
                return;
            }
            
            SmartPtr<iMaterial> material = mesh->GetMaterial();
            if (material.IsNull())
            {
                mIgnore = true;
                return;
            }
            
            SmartPtr<iMaterialProp> prop = material->FindProperty(mPropName);
            if (prop.IsNull())
            {
                mIgnore = true;
                return;
            }
            
            mProp = prop;
        }
        
        Var value = Evaluate(leftKey->GetMode(), leftKey->GetRight(), rightKey->GetLeft(), time);
        mProp->SetValue(value);
    }
};

iMaterialAnimator* CreateMaterialAnimator()
{
    return new MaterialAnimator();
}

NSPI_END()


















