/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.7.3   1.0     Create
 ******************************************************************************/
#include "../../asset/AssetImpl.h"

using namespace std;

NSPI_BEGIN()


class Animation : public AssetImpl<iAnimation>
{
private:
    bool mRepeat;
    float mLocalTime;
    float mDuration;
    
    SmartPtr<iAnimTrackArray> mTracks;
    
    bool mIgnore;
    bool mInit;
    
public:
    Animation():
    mRepeat(false), mLocalTime(0), mIgnore(false), mDuration(0), mInit(false)
    {
        mTracks = CreateAnimTrackArray();
    }
    
    virtual ~Animation()
    {
    }
    
    virtual void SetTracks(iAnimTrackArray* tracks)
    {
        piAssert(tracks != nullptr, ;);
        mTracks = tracks;
    }
    
    virtual iAnimTrackArray* GetTracks() const
    {
        return mTracks;
    }
    
    virtual void SetRepeat(bool value)
    {
        mRepeat = value;
    }
    
    virtual bool IsRepeat() const
    {
        return mRepeat;
    }
    
    virtual void Reset()
    {
        mLocalTime = 0;
        mInit = false;
    }
    
    virtual void OnUpdate(iEntity* entity, float delta)
    {
        if (!mInit)
        {
            OnInit(entity);
            mInit = true;
        }
        
        mLocalTime += delta;
        
        float time = mLocalTime;
        if (mRepeat)
        {
            time = fmod(mLocalTime, mDuration);
        }
        else
        {
            time = mLocalTime > mDuration ? mDuration : mLocalTime;
        }
        
        for (int32_t i = 0; i < mTracks->GetCount(); ++i)
        {
            SmartPtr<iAnimTrack> track = mTracks->GetItem(i);
            
            if (time >= track->GetBegin() && time <= track->GetEnd())
            {
                OnUpdateTrack(entity, track, time);
            }
        }
    }
    
protected:
    
    void OnInit(iEntity* entity)
    {
        for (int32_t i = 0; i < mTracks->GetCount(); ++i)
        {
            SmartPtr<iAnimTrack> track = mTracks->GetItem(i);
            
            float end = track->GetEnd();
            if (end > mDuration)
            {
                mDuration = end;
            }
        }
    }
    
    void OnUpdateTrack(iEntity* entity, iAnimTrack* track, float localTime)
    {
        SmartPtr<iAnimator> animator = track->GetAnimator();
        piCheck(!animator.IsNull(), ;);
        
        SmartPtr<iAnimKeyArray> keys = track->GetKeys();
        
        SmartPtr<iAnimKey> leftKey;
        SmartPtr<iAnimKey> rightKey;
        
        for (auto i = 0; i < keys->GetCount(); ++i)
        {
            SmartPtr<iAnimKey> key = keys->GetItem(i);
            float time = key->GetTime();
            
            if (localTime < time)
            {
                rightKey = key;
                if (i > 0)
                {
                    leftKey = keys->GetItem(i - 1);
                }
                else
                {
                    leftKey = keys->GetItem(keys->GetCount() - 1);
                }
                break;
            }
        }
        
        piAssert(!leftKey.IsNull(), ;);
        
        float time = (localTime - leftKey->GetTime()) / (rightKey->GetTime() - leftKey->GetTime());
        animator->OnUpdate(entity, leftKey, rightKey, time);
    }
    
private:
    
   
};

iAnimation* CreateAnimation()
{
    return new Animation();
}


NSPI_END()

























