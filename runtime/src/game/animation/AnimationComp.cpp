/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.7.3   1.0     Create
 ******************************************************************************/
#include <pi/game/impl/ComponentImpl.h>

using namespace std;

NSPI_BEGIN()


class AnimationComp : public ComponentImpl<iAnimationComp>
{
private:
    string mUri;
    SmartPtr<iAnimation> mAnim;
    
public:
    AnimationComp()
    {
    }
    
    virtual ~AnimationComp()
    {
    }
    
    virtual std::string GetUri() const
    {
        return mUri;
    }
    
    virtual void SetUri(const std::string& uri)
    {
        mUri = uri;
    }
    
    virtual iAnimation* GetAnimation() const
    {
        return mAnim;
    }
    
    virtual void SetAnimation(iAnimation* animation)
    {
        piAssert(animation != nullptr, ;);
        mAnim = animation;
    }
    
    virtual void OnUpdate(float delta)
    {
        if (!mAnim.IsNull())
        {
            mAnim->OnUpdate(GetEntity(), delta);
        }
    }
};

iAnimationComp* CreateAnimationComp()
{
    return new AnimationComp();
}


NSPI_END();





























