/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.28   1     Create
 ******************************************************************************/
#include "AnimatorImpl.h"

using namespace std;

NSPI_BEGIN()

class CompAnimator : public AnimatorImpl<iCompAnimator>
{
private:
    SmartPtr<iProperty> mProp;
    SmartPtr<iComponent> mComp;
    
public:
    CompAnimator()
    {
    }
    
    virtual ~CompAnimator()
    {
    }
    
    virtual void OnUpdate(iEntity* entity, iAnimKey* leftKey, iAnimKey* rightKey, float time)
    {
        piCheck(!mIgnore, ;);
        
        if (mProp.IsNull())
        {
            SmartPtr<iComponent> comp = entity->GetCompByClassName(mCompName);
            if (comp.IsNull())
            {
                mIgnore = true;
                return;
            }
            
            SmartPtr<iClass> klass = comp->GetClass();
            SmartPtr<iProperty> prop = klass->GetProperty(mPropName);
            if (prop.IsNull())
            {
                mIgnore = true;
                return;
            }
            
            mProp = prop;
            mComp = comp;
        }
        
        Var value = Evaluate(leftKey->GetMode(), leftKey->GetRight(), rightKey->GetLeft(), time);
        mProp->SetValue(mComp, value);
    }
};


iCompAnimator* CreateCompAnimator()
{
    return new CompAnimator();
}


NSPI_END()





























