/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.3   1.0     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()

class AnimCurve : public iAnimCurve
{
private:
    SmartPtr<iAnimCurveKeyArray> mKeys;
    
public:
    AnimCurve()
    {
        mKeys = CreateAnimCurveKeyArray();
    }
    
    virtual ~AnimCurve()
    {
    }
    
    virtual iAnimCurveKeyArray* GetKeys() const
    {
        return mKeys;
    }
    
    virtual void SetKeys(iAnimCurveKeyArray* keys)
    {
        piAssert(keys != nullptr, ;);
        mKeys = keys;
    }
};


iAnimCurve* CreateAnimCurve()
{
    return new AnimCurve();
}


NSPI_END()







