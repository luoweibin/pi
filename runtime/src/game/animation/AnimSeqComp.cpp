/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.7.5   1.0     Create
 ******************************************************************************/
#include <pi/game/impl/ComponentImpl.h>

using namespace std;

NSPI_BEGIN()


class AnimSeqComp : public ComponentImpl<iAnimSeqComp>
{
private:
    string mUri;
    SmartPtr<iAnimSeq> mSeq;
    float mLocalTime;
    bool mRepeat;
    float mDuration;
    SmartPtr<iMaterialProp> mProp;
    bool mIgnore;
    
    string mPropName;
    
public:
    AnimSeqComp():
    mLocalTime(0), mRepeat(false), mDuration(1), mIgnore(false)
    {
    }
    
    virtual ~AnimSeqComp()
    {
    }
    
    virtual float GetDuration() const
    {
        return mDuration;
    }
    
    virtual void SetDuration(float duration)
    {
        piAssert(duration > 0, ;);
        mDuration = duration;
    }
    
    virtual bool IsRepeat() const
    {
        return mRepeat;
    }
    
    virtual void SetRepeat(bool value)
    {
        mRepeat = value;
    }
    
    virtual std::string GetUri() const
    {
        return mUri;
    }
    
    virtual void SetUri(const std::string& uri)
    {
        mUri = uri;
    }
    
    virtual iAnimSeq* GetSeq() const
    {
        return mSeq;
    }
    
    virtual void SetSeq(iAnimSeq* seq)
    {
        piAssert(seq != nullptr, ;);
        mSeq = seq;
    }
    
    virtual void SetProperty(const string& name)
    {
        mPropName = name;
    }
    
    virtual string GetProperty() const
    {
        return mPropName;
    }
    
    virtual void OnUpdate(float delta)
    {
        piCheck(!mIgnore, ;);
        piCheck(!mSeq.IsNull(), ;);
        
        if (mProp.IsNull())
        {
            SmartPtr<iDynamicMesh> mesh = piGetEntityComponent<iDynamicMesh>(GetEntity());
            if (mesh.IsNull())
            {
                mIgnore = true;
                return;
            }
            
            SmartPtr<iMaterial> material = mesh->GetMaterial();
            if (material.IsNull())
            {
                mIgnore = true;
                return;
            }
            
            SmartPtr<iMaterialProp> prop = material->FindProperty(mPropName);
            if (prop.IsNull())
            {
                mIgnore = true;
                return;
            }
            
            mProp = prop;
        }
        
        mLocalTime += delta;
        
        float time = mLocalTime;
        if (mRepeat)
        {
            time = fmod(mLocalTime, mDuration);
        }
        else
        {
            time = mLocalTime > mDuration ? mDuration : mLocalTime;
        }
        
        time /= mDuration;
        
        int32_t frame = mSeq->GetFrames()->GetCount() * time;
        SmartPtr<iTexture2D> tex = mSeq->GetFrame(frame);
        if (!tex.IsNull())
        {
            mProp->SetValue(tex.Ptr());
        }
    }
};

iAnimSeqComp* CreateAnimSeqComp()
{
    return new AnimSeqComp();
}


NSPI_END()

























