/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.3   1.0     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()

static int DecodeTangentMode(int32_t flags)
{
    return ((int32_t)0x00FF) & flags;
}

static int32_t EncodeTangentMode(int32_t flags, int mode)
{
    return (((int32_t)0xFF00) & flags) & mode;
}

static int DecodeTangentOption(int32_t flags)
{
    return (((int32_t)0xFF00) & flags) >> 16;
}

static void EncodeTangentOption(int32_t& flags, int option)
{
    flags = (((int32_t)0x00FF) & flags) & (option << 16);
}


class AnimCurveKey : public iAnimCurveKey
{
private:
    int64_t mTimeMS;
    
    int mInterp;
    int32_t mTangentFlags;
    Var mValue;
    float mData0; // left tangent or tension
    float mData1; // right tangent
    
public:
    AnimCurveKey():
    mInterp(eInterpMode_Constant),
    mTangentFlags(0), mData0(0), mData1(0), mTimeMS(0)
    {
    }
    
    virtual ~AnimCurveKey()
    {
    }
    
    virtual void SetTime(int64_t ms)
    {
        mTimeMS = ms;
    }
    
    virtual int64_t GetTime() const
    {
        return mTimeMS;
    }
    
    virtual int GetInterpMode() const
    {
        return mInterp;
    }
    
    virtual void SetInterpMode(int mode)
    {
        mInterp = mode;
    }
    
    virtual int GetTangentMode() const
    {
        return DecodeTangentMode(mTangentFlags);
    }
    
    virtual void SetTangentMode(int mode)
    {
        EncodeTangentMode(mTangentFlags, mode);
    }
    
    virtual void SetValue(const Var& value)
    {
        mValue = value;
    }
    
    virtual Var GetValue() const
    {
        return mValue;
    }
    
    virtual int GetTangentOption() const
    {
        return DecodeTangentOption(mTangentFlags);
    }
    
    virtual void SetTangentOption(int option)
    {
        EncodeTangentOption(mTangentFlags, option);
    }
    
    virtual float GetLeftTangent() const
    {
        return mData0;
    }
    
    virtual void SetLeftTangent(float value)
    {
        mData0 = value;
    }
    
    virtual float GetRightTangent() const
    {
        return mData1;
    }
    
    virtual void SetRightTangent(float value)
    {
        mData1 = value;
    }
    
    virtual void SetTension(float value)
    {
        mData0 = value;
    }
    
    virtual float GetTension() const
    {
        return mData0;
    }
};

iAnimCurveKey* CreateAnimCurveKey()
{
    return new AnimCurveKey();
}


NSPI_END()







