/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.21   1.0     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()

class ModelAnim : public iModelAnim
{
private:
    string mName;
    SmartPtr<iAnimLayerArray> mLayers;
    
    float mLocalTime;
    
public:
    ModelAnim():
    mLocalTime(0)
    {
        mLayers = CreateAnimLayerArray();
    }
    
    virtual ~ModelAnim()
    {
    }
    
    virtual void SetName(const std::string& name)
    {
        mName = name;
    }
    
    virtual std::string GetName() const
    {
        return mName;
    }
    
    virtual void SetLayers(iAnimLayerArray* layers)
    {
        piAssert(layers != nullptr, ;);
        mLayers = layers;
    }
    
    virtual iAnimLayerArray* GetLayers() const
    {
        return mLayers;
    }
    
    /**
     * Callbacks
     */
    
    virtual void OnUpdate(float delta)
    {
        piAssert(delta >= 0, ;);
        
        mLocalTime += delta;
        
        for (auto i = 0; i < mLayers->GetCount(); ++i)
        {
            SmartPtr<iAnimLayer> layer = mLayers->GetItem(i);
            
            SmartPtr<iAnimChannelArray> channels = layer->GetChannels();
            for (auto j = 0; j < channels->GetCount(); ++j)
            {
                SmartPtr<iAnimChannel> ch = channels->GetItem(j);
                OnUpdateAnimChannel(ch, mLocalTime);
            }
        }
    }
    
private:
    
    static void OnUpdateAnimChannel(iAnimChannel* ch, float animTime)
    {
        SmartPtr<iAnimCurveKeyArray> keys = ch->GetKeys();
        piCheck(keys->GetCount() > 0, ;);
        
        SmartPtr<iRefObject> target = ch->GetTarget();
        piCheck(!target.IsNull(), ;);
        
        SmartPtr<iProperty> prop = ch->GetProperty();
        piCheck(!prop.IsNull(), ;);
        
        if (!prop.IsNull())
        {
            Var value;
            switch (prop->GetTypeClass()->GetType())
            {
                case eType_I8:
                case eType_U8:
                case eType_I16:
                case eType_U16:
                case eType_I32:
                    prop->SetValue(target, EvaluateValue<int32_t>(keys, animTime));
                    break;
                case eType_U32:
                    prop->SetValue(target, EvaluateValue<uint32_t>(keys, animTime));
                    break;
                case eType_I64:
                    prop->SetValue(target, EvaluateValue<int64_t>(keys, animTime));
                    break;
                case eType_U64:
                    prop->SetValue(target, EvaluateValue<uint64_t>(keys, animTime));
                    break;
                case eType_F32:
                    prop->SetValue(target, EvaluateValue<float>(keys, animTime));
                    break;
                case eType_F64:
                    prop->SetValue(target, EvaluateValue<double>(keys, animTime));
                    break;
                case eType_Mat4:
                    prop->SetValue(target, EvaluateMatrixValue(keys, animTime));
                    break;
                default:
                    piAssertDontStop(false);
                    break;
            }
        }
    }
    
    template <typename T>
    static T EvaluateValue(iAnimCurveKeyArray* keys, float animTime)
    {
        if (keys->GetCount() == 1)
        {
            return keys->GetItem(0)->GetValue();
        }
        
        SmartPtr<iAnimCurveKey> endKey = keys->GetItem(keys->GetCount() - 1);
        
        float channelDuration = endKey->GetTime() / 1000.0;
        float localTime = fmod(animTime, channelDuration);
        
        SmartPtr<iAnimCurveKey> leftKey;
        SmartPtr<iAnimCurveKey> rightKey;
        
        for (auto i = 0; i < keys->GetCount(); ++i)
        {
            SmartPtr<iAnimCurveKey> key = keys->GetItem(i);
            float time = key->GetTime() / 1000.0;
            
            if (localTime < time)
            {
                rightKey = key;
                if (i > 0)
                {
                    leftKey = keys->GetItem(i - 1);
                }
                else
                {
                    leftKey = keys->GetItem(keys->GetCount() - 1);
                }
                break;
            }
        }
        
        float leftTime = leftKey->GetTime() / 1000.0;
        float rightTime = rightKey->GetTime() / 1000.0;
        float frameTime = (localTime - leftTime) / (rightTime - leftTime);
        
        T leftValue = leftKey->GetValue();
        T rightValue = rightKey->GetValue();
        
        return leftValue * (1 - frameTime) + rightValue * frameTime;
    }
    
    static mat4 EvaluateMatrixValue(iAnimCurveKeyArray* keys, float animTime)
    {
        if (keys->GetCount() == 1)
        {
            return keys->GetItem(0)->GetValue();
        }
        
        SmartPtr<iAnimCurveKey> endKey = keys->GetItem(keys->GetCount() - 1);
        
        float channelDuration = endKey->GetTime() / 1000.0;
        float localTime = fmod(animTime, channelDuration);
        
        SmartPtr<iAnimCurveKey> leftKey;
        SmartPtr<iAnimCurveKey> rightKey;
        
        for (auto i = 0; i < keys->GetCount(); ++i)
        {
            SmartPtr<iAnimCurveKey> key = keys->GetItem(i);
            float time = key->GetTime() / 1000.0;
            
            if (localTime < time)
            {
                rightKey = key;
                if (i > 0)
                {
                    leftKey = keys->GetItem(i - 1);
                }
                else
                {
                    leftKey = keys->GetItem(keys->GetCount() - 1);
                }
                break;
            }
        }
        
        float leftTime = leftKey->GetTime() / 1000.0;
        float rightTime = rightKey->GetTime() / 1000.0;
        float frameTime = (localTime - leftTime) / (rightTime - leftTime);
        
        mat4 leftValue = leftKey->GetValue();
        mat4 rightValue = rightKey->GetValue();
        
        return piglm::interpolate(leftValue, rightValue, frameTime);
    }
    
};


iModelAnim* CreateModelAnim()
{
    return new ModelAnim();
}


NSPI_END()




















