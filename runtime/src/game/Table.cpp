/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.24   1     Create
 ******************************************************************************/
#include <pi/game/impl/ComponentImpl.h>

using namespace std;

NSPI_BEGIN()


class Table : public ComponentImpl<iTable>
{
private:
    typedef map<string, Var> VarMap;
    VarMap mValues;
    
    mutable bool mDirty;
    mutable SmartPtr<iStringArray> mKeys;
    
public:
    Table():
    mDirty(true)
    {
        mKeys = CreateStringArray();
    }
    
    virtual ~Table()
    {
    }
    
    virtual void Set(const string& key, const Var& value)
    {
        piAssert(!key.empty(), ;);
        mValues[key] = value;
        mDirty = true;
    }
    
    virtual Var Get(const string& key) const
    {
        auto it = mValues.find(key);
        return it != mValues.end() ? it->second : Var();
    }
    
    virtual iStringArray* GetKeys() const
    {
        if (mDirty)
        {
            mKeys->Clear();
            
            for (auto pair : mValues)
            {
                mKeys->PushBack(pair.first);
            }
            
            mDirty = false;
        }
        return mKeys;
    }
};


iTable* CreateTable()
{
    return new Table();
}


NSPI_END()


























