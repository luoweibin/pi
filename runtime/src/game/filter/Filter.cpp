/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.4   0.1     Create
 ******************************************************************************/
#include <pi/game/impl/ComponentImpl.h>

using namespace std;

NSPI_BEGIN()


class Filter : public ComponentImpl<iFilter>
{
public:
    Filter()
    {
    }
    
    virtual ~Filter()
    {
    }
};

iFilter* CreateFilter()
{
    return new Filter();
}


NSPI_END()




























