/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.4   0.1     Create
 ******************************************************************************/
#include <pi/game/impl/SystemImpl.h>

using namespace std;

NSPI_BEGIN()


class FilterSystem : public SystemImpl<iFilterSystem>
{
private:
    int32_t mTexID;
    
    SmartPtr<iGraphicsObject> mTexObj;
    
public:
    FilterSystem():
    mTexID(0)
    {
        mTexObj = CreateGraphicsObject();
    }
    
    virtual ~FilterSystem()
    {
    }
    
    virtual bool Accept(iEntity* entity)
    {
        piCheck(HasClass(entity->GetCompClassArray(), iFilter::StaticClass()), false);
        
        SmartPtr<iMaterial2D> mat2D = piGetEntityComponent<iMaterial2D>(entity);
        piCheck(!mat2D.IsNull(), false);
        
        mat2D->SetColorTex(mTexObj);
        
        return true;
    }
    
    virtual void OnLoad()
    {
        mTexID = piCreateTexture();
        
        piBindTexture(eTexTarget_2D, mTexID);
        piTexParam(eTexTarget_2D, eTexConfig_WrapS, eTexValue_Clamp);
        piTexParam(eTexTarget_2D, eTexConfig_WrapT, eTexValue_Clamp);
        piTexParam(eTexTarget_2D, eTexConfig_MinFilter, eTexValue_Linear);
        piTexParam(eTexTarget_2D, eTexConfig_MagFilter, eTexValue_Linear);
        
        piBindTexture(mTexID, 0);
        
        mTexObj->SetName(mTexID);
        
        GetMotionManager()->Start();
    }

    
    virtual void OnUnload()
    {
        piReleaseGraphicsObject(mTexID);
        mTexID = 0;
        
        GetMotionManager()->Stop();
    }
    
    
    virtual void OnBeforeUpdate(float delta)
    {
        SmartPtr<iTouchEvent> ev = GetHID()->GetTapEvent();
        if (!ev.IsNull())
        {
            PILOGI(PI_GAME_TAG, "OnBeforeUpdate:%s", ev->ToString().c_str());
        }
        
        SmartPtr<iFaceTrackerResult> result = GetHID()->GetFaceTrackerResult();
        if (!result.IsNull())
        {
            for (auto i = 0; i < result->GetFaceCount(); ++i)
            {
                SmartPtr<iFaceInfo> info = result->GetFace(i);
                
                if (info->ActionFlagsIs(eHumanAction_HeadPitch))
                {
                    PILOGI(PI_GAME_TAG, "[%d]Head Pitch", i);
                }
                
                if (info->ActionFlagsIs(eHumanAction_HeadYaw))
                {
                    PILOGI(PI_GAME_TAG, "[%d]Head Yaw", i);
                }
                
                if (info->ActionFlagsIs(eHumanAction_EyeBlink))
                {
                    PILOGI(PI_GAME_TAG, "[%d]Eye Blink", i);
                }
                
                if (info->ActionFlagsIs(eHumanAction_MouthOpen))
                {
                    PILOGI(PI_GAME_TAG, "[%d]Mouth open", i);
                }
                
                if (info->ActionFlagsIs(eHumanAction_BrowJump))
                {
                    PILOGI(PI_GAME_TAG, "[%d]Brow Jump", i);
                }
            }
            
            for (auto i = 0; i < result->GetHandCount(); ++i)
            {
                SmartPtr<iHandInfo> info = result->GetHand(i);
                
                PILOGI(PI_GAME_TAG,
                       "Score:%f, Action Score:%f, Rect:%s, Point:%s",
                       info->GetScore(),
                       info->GetActionScore(),
                       piToString(info->GetRect()).c_str(),
                       piToString(info->GetPoint()).c_str());
                
                if (info->ActionFlagsIs(eHumanAction_HandCong))
                {
                    PILOGI(PI_GAME_TAG, "[%d]Hand Cong", i);
                }
                
                if (info->ActionFlagsIs(eHumanAction_HandGood))
                {
                    PILOGI(PI_GAME_TAG, "[%d]Hand Good", i);
                }
                
                if (info->ActionFlagsIs(eHumanAction_HandLove))
                {
                    PILOGI(PI_GAME_TAG, "[%d]Hand Love", i);
                }
                
                if (info->ActionFlagsIs(eHumanAction_HandPalm))
                {
                    PILOGI(PI_GAME_TAG, "[%d]Hand Palm", i);
                }
                
                if (info->ActionFlagsIs(eHumanAction_FingerHeart))
                {
                    PILOGI(PI_GAME_TAG, "[%d]Finger Heart", i);
                }
                
                if (info->ActionFlagsIs(eHumanAction_HandHoldup))
                {
                    PILOGI(PI_GAME_TAG, "[%d]Hand Holdup", i);
                }
            }
        }
        
        SmartPtr<iDeviceMotion> motion = GetMotionManager()->GetMotion();
        if (!motion.IsNull())
        {
            PILOGI(PI_GAME_TAG, "motion:%s", motion->ToString().c_str());
        }
    }
    
    virtual void OnUpdate(float delta)
    {
        SmartPtr<iFaceTrackerResult> result = GetHID()->GetFaceTrackerResult();
        piCheck(!result.IsNull(), ;);
        
        if (result->HasBgMask())
        {
            SmartPtr<iGraphicsObject> bgMask = result->GetBgMask();
            mTexObj->SetName(bgMask->GetName());
        }
        else
        {
            SmartPtr<iBitmap> bitmap = GetHID()->GetOriginBitmap();
            piCheck(!bitmap.IsNull(), ;);
            
            piActiveTexture(0);
            piBindTexture(eTexTarget_2D, mTexID);
            piTexImage2D(eTexTarget_2D, 0, ePixelFormat_RGBA, bitmap, 0);
            piBindTexture(eTexTarget_2D, 0);
            mTexObj->SetName(mTexID);
        }
    }
    
    virtual void OnAfterUpdate(float delta)
    {
        
    }
    
    virtual void OnResize(const rect& bounds)
    {
        
    }
    
    virtual bool OnMessage(iMessage* message)
    {
        return false;
    }
    
    virtual bool OnHIDEvent(iHIDEvent* event)
    {
        if (event->GetID() == eHIDEvent_Tap)
        {
            SmartPtr<iTouchEvent> ev = dynamic_cast<iTouchEvent*>(event);
            PILOGI(PI_GAME_TAG, "OnHIDEvent:%s", ev->ToString().c_str());
        }
        
        return false;
    }
};


iFilterSystem* CreateFilterSystem()
{
    return new FilterSystem();
}


NSPI_END()




























