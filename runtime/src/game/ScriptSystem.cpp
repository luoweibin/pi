/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.8   1     Create
 ******************************************************************************/
#include <pi/game/impl/SystemImpl.h>

using namespace std;

NSPI_BEGIN()

class ScriptSystem : public SystemImpl<iScriptSystem>
{
private:
    SmartPtr<iScriptRef> mRef;
    string mUri;
    
public:
    ScriptSystem()
    {
    }
    
    virtual ~ScriptSystem()
    {
    }
    
    virtual void SetUri(const string& uri)
    {
        mUri = uri;
    }
    
    virtual string GetUri() const
    {
        return mUri;
    }
    
    virtual void OnResize(const rect& bounds)
    {
        piCheck(!mRef.IsNull(), ;);

        mRef->Call("OnResize", this, bounds);
    }
    
    virtual void OnBeforeUpdate(float delta)
    {
        piCheck(!mRef.IsNull(), ;);
        mRef->Call("OnBeforeUpdate", this, delta);
    }
    
    virtual void OnUpdate(float delta)
    {
        piCheck(!mRef.IsNull(), ;);
        mRef->Call("OnUpdate", this, delta);
    }
    
    virtual void OnAfterUpdate(float delta)
    {
        piCheck(!mRef.IsNull(), ;);
        mRef->Call("OnAfterUpdate", this, delta);
    }
    
    virtual bool OnHIDEvent(iHIDEvent* event)
    {
        piCheck(!mRef.IsNull(), false);
        return mRef->Call("OnHIDEvent", this, event);
    }
    
    virtual bool OnMessage(iMessage* message)
    {
        piCheck(!mRef.IsNull(), false);
        return mRef->Call("OnMessage", this, message);
    }
    
    virtual void OnLoad()
    {
        mRef = LoadScript(mUri);
        
        piCheck(!mRef.IsNull(), ;);
        
        mRef->Call("OnLoad", this);
    }
    
    virtual void OnUnload()
    {
        piCheck(!mRef.IsNull(), ;);
        mRef->Call("OnUnload", this);
    }
    
    virtual bool Accept(iEntity* entity)
    {
        piCheck(!mRef.IsNull(), false);
        return mRef->Call("Accept", this, entity);
    }
    
protected:
    
    virtual void OnRemoveEntity(iEntity* entity)
    {
        piCheck(!mRef.IsNull(), ;);
        mRef->Call("OnRemoveEntity", this, entity);
    }
    
    iScriptRef* LoadScript(const string& uri)
    {
        SmartPtr<iScript> script = GetScript();
        return script->EvaluateFile(uri);
    }
    
};

iScriptSystem* CreateScriptSystem()
{
    return new ScriptSystem();
}


NSPI_END()

































