/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.24   1     Create
 ******************************************************************************/
#include <pi/game/impl/SystemImpl.h>

using namespace std;

#include "FCS_Program.h"

NSPI_BEGIN()

class FrameCaptureSystem : public SystemImpl<iFrameCaptureSystem>
{
private:
    int32_t mFramebuffer;
    int32_t mColorBuffer;
    int32_t mDepthBuffer;
    int32_t mTexID;
    
    int32_t mIBO;
    int32_t mVBO;
    int32_t mVAO;
    
    int32_t mProgram;
    
    int32_t mLastFBO;
    
    bool mCapture;
    
public:
    FrameCaptureSystem():
    mLastFBO(0), mVBO(0), mVAO(0), mIBO(0), mCapture(false),
    mFramebuffer(0), mColorBuffer(0), mDepthBuffer(0), mTexID(0), mProgram(0)
    {
    }
    
    virtual ~FrameCaptureSystem()
    {
    }
    
    virtual void OnLoad()
    {
        CreateBuffers();
        
        mProgram = FCS_CreateProgram();
    }
    
    virtual void OnUnload()
    {
        DestroyFramebuffer();
        
        piReleaseGraphicsObject(mProgram);
        mProgram = 0;
        
        DestroyBuffers();
    }
    
    virtual void OnBeforeUpdate(float delta)
    {
        if (mCapture)
        {
            piPushGroupMarker("FrameCaptureSystem::OnBeforeUpdate");
            
            mLastFBO = piGetFramebuffer();
            piBindFramebuffer(eFramebuffer_DrawRead, mFramebuffer);
            
            piPopGroupMarker();
        }
    }
    
    virtual void OnAfterUpdate(float delta)
    {
        if (mCapture)
        {
            piPushGroupMarker("FrameCaptureSystem::OnAfterUpdate");
            
            piBindFramebuffer(eFramebuffer_DrawRead, mLastFBO);
            
            piUseProgram(mProgram);
            
            piBindVertexArray(mVAO);
            
            piActiveTexture(0);
            piBindTexture(eTexTarget_2D, mTexID);
            piUniform1i(mProgram, "texture0", 0);
            
            piDrawElements(eGraphicsDraw_TriangleStrip, 4, eType_U8);
            
            piUseProgram(0);
            piBindVertexArray(0);
            piBindTexture(eTexTarget_2D, 0);
            
            piPopGroupMarker();
            
            GetScene()->PostLocalMessage(this,
                                         eGameMsgPri_High,
                                         eGameMsg_UpdateFrame,
                                         mTexID);
            
            mCapture = false;
        }
    }
    
    virtual bool Accept(iEntity* entity)
    {
        return false;
    }
    
    virtual void OnResize(const rect& bounds)
    {
        DestroyFramebuffer();
        CreateFramebuffer();
    }
    
    virtual bool OnMessage(iMessage* message)
    {
        piCheck(message->GetID() == eGameMsg_CaptureFrame, false);
        
        mCapture = true;
        
        return false;
    }

    
private:
    
    void CreateBuffers()
    {
        {
            const float pos[] =
            {
                -1, -1, 0, 0, 0,
                1, -1, 0, 1, 0,
                -1,  1, 0, 0, 1,
                1,  1, 0, 1, 1,
            };
            
            SmartPtr<iMemory> mem = CreateMemoryCopy(pos, sizeof(pos));
            mVBO = piCreateBuffer();
            piBindBuffer(eGraphicsBuffer_Vertex, mVBO);
            piBufferData(eGraphicsBuffer_Vertex, mVBO, mem->Size(), mem);
            piBindBuffer(eGraphicsBuffer_Vertex, 0);
        }
        
        {
            const uint8_t indices[] = {0, 1, 2, 3};
            SmartPtr<iMemory> mem = CreateMemoryCopy(indices, sizeof(indices));
            mIBO = piCreateBuffer();
            piBindBuffer(eGraphicsBuffer_Index, mIBO);
            piBufferData(eGraphicsBuffer_Index, mIBO, mem->Size(), mem);
            piBindBuffer(eGraphicsBuffer_Index, 0);
        }
        
        mVAO = piCreateVertexArray();
        piBindVertexArray(mVAO);
        
        piBindBuffer(eGraphicsBuffer_Index, mIBO);
        piBindBuffer(eGraphicsBuffer_Vertex, mVBO);
        
        piEnableVertexAttr(0);
        piEnableVertexAttr(1);
        
        piVertexAttr(0, 3, eType_F32, sizeof(float) * 5, 0);
        piVertexAttr(1, 2, eType_F32, sizeof(float) * 5, sizeof(float) * 3);
        
        piBindVertexArray(0);
        piBindBuffer(eGraphicsBuffer_Vertex, 0);
        piBindBuffer(eGraphicsBuffer_Index, 0);
    }
    
    void DestroyBuffers()
    {
        piReleaseGraphicsObject(mVAO);
        mVAO = 0;
        
        piReleaseGraphicsObject(mVBO);
        mVBO = 0;
        
        piReleaseGraphicsObject(mIBO);
        mIBO = 0;
    }
    
    void CreateFramebuffer()
    {
        rect bounds = GetScene()->GetBounds();
        
        mTexID = piCreateTexture();
        piBindTexture(eTexTarget_2D, mTexID);
        piTexParam(eTexTarget_2D, eTexConfig_MinFilter, eTexValue_Linear);
        piTexParam(eTexTarget_2D, eTexConfig_MagFilter, eTexValue_Linear);
        SmartPtr<iBitmap> bitmap = CreateBitmapEmpty(ePixelFormat_RGBA,
                                                     bounds.width,
                                                     bounds.height);
        piTexImage2D(eTexTarget_2D, 0, ePixelFormat_RGBA, bitmap, 0);
        
        mFramebuffer = piCreateFramebuffer();
        piBindFramebuffer(eFramebuffer_DrawRead, mFramebuffer);
        piFramebufferTexture2D(eFramebuffer_DrawRead, eAttachment_Color0, eTexTarget_2D, mTexID, 0);
        
        mDepthBuffer = piCreateRenderbuffer();
        piBindRenderbuffer(mDepthBuffer);
        piRenderbufferStorage(ePixelFormat_Depth24Stencil8, bounds.width, bounds.height);
        piFramebufferRenderbuffer(eFramebuffer_DrawRead, eAttachment_Depth, mDepthBuffer);
        
        piVerifyFramebufferState(eFramebuffer_DrawRead);
        
        piBindFramebuffer(eFramebuffer_DrawRead, 0);
        piBindRenderbuffer(0);
        piBindTexture(eTexTarget_2D, 0);
    }
    
    void DestroyFramebuffer()
    {
        piReleaseGraphicsObject(mTexID);
        mTexID = 0;
        
        piReleaseGraphicsObject(mDepthBuffer);
        mDepthBuffer = 0;
        
        piReleaseGraphicsObject(mFramebuffer);
        mFramebuffer = 0;
    }
};


iFrameCaptureSystem* CreateFrameCaptureSystem()
{
    return new FrameCaptureSystem();
}


NSPI_END()



















