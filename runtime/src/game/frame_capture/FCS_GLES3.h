/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.11.21   0.1     Create
 ******************************************************************************/

NSPI_BEGIN()

const static char g_FCS_VS_GLES3[] =
"#version 300 es\n"
"precision highp float;\n"
"layout(location=0) in vec4 position;\n"
"layout(location=1) in vec4 uv;\n"
"out vec2 uv0;\n"
"void main(void)\n"
"{\n"
"    gl_Position = position;\n"
"    uv0 = uv.st;\n"
"}";

const static char g_FCS_FS_GLES3[] =
"#version 300 es\n"
"precision highp float;\n"
"in vec2 uv0;\n"
"uniform sampler2D texture0;\n"
"layout(location=0) out vec4 fragColor;\n"
"void main(void) {\n"
"    vec4 color = texture(texture0, uv0);\n"
"    fragColor = color;\n"
"}";

NSPI_END()
