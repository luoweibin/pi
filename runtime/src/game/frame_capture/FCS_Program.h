/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.03.20   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>
#include <pi/core/impl/ThreadLocalImpl.h>

using namespace std;

#if defined(PI_OPENGL_ES)
#   include "FCS_GLES3.h"
#   include "FCS_GLES2.h"
#endif

#if defined(PI_OPENGL)
#   include "FCS_OpenGL.h"
#endif

NSPI_BEGIN()
static int32_t FCS_CreateProgram()
{
    int32_t program = piCreateProgram();
    int type = piGetGraphicsType();
    switch (type)
    {
#if defined(PI_OPENGL_ES)
        case eGraphicsBackend_OpenGL_ES3:
            piCompileProgram(program, g_FCS_VS_GLES3, g_FCS_FS_GLES3);
            break;
        case eGraphicsBackend_OpenGL_ES2:
            piCompileProgram(program, g_FCS_VS_GLES2, g_FCS_FS_GLES2);
            piBindVertexAttr(program, 0, "position");
            piBindVertexAttr(program, 1, "uv");
            break;
#endif
            
#if defined(PI_OPENGL)
        case eGraphicsBackend_OpenGL3:
        case eGraphicsBackend_OpenGL4:
            piCompileProgram(program, g_FCS_VS_OpenGL, g_FCS_FS_OpenGL);
            break;
#endif
        default:
            piAssertDontStop(false);
            break;
    }
    
    piLinkProgram(program);
    
    return program;
}

NSPI_END()
