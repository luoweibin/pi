/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.11.21   0.1     Create
 ******************************************************************************/

NSPI_BEGIN()

const static char g_FCS_VS_GLES2[] =
"precision highp float;\n"
"attribute vec4 position;\n"
"attribute vec4 uv;\n"
"varying vec2 uv0;\n"
"void main(void)\n"
"{\n"
"    gl_Position = position;\n"
"    uv0 = uv.st;\n"
"}";

const static char g_FCS_FS_GLES2[] =
"precision highp float;\n"
"varying vec2 uv0;\n"
"uniform sampler2D texture0;\n"
"void main(void) {\n"
"    vec4 color = texture2D(texture0, uv0);\n"
"    gl_FragColor = color;\n"
"}";

NSPI_END()
