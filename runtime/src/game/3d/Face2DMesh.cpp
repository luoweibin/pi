/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 Sword     2017.7.6   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>

#include <pi/game/impl/DynamicMeshImpl.h>

using namespace std;

NSPI_BEGIN()

typedef uint8_t face2DIndexType;

// Face2DMeshIndexes_AddExtendPoint
const face2DIndexType Face2DMeshIndexes[] =
{
    0,  1,  33,
    1,  2,  33,
    2,  34, 33,
    2,  3,  34,
    3,  4,  34,
    4,  35, 34,
    4,  5,  35,
    5,  36, 35,
    5,  6,  36,
    6,  7,  36,
    7,  37, 36,
    7,  8,  37,
    8,  9,  37,
    9,  10, 37,
    10, 11, 37,
    11, 12, 37,
    12, 13, 37,
    13, 14, 37,
    14, 15, 37,
    15, 16, 37,
    
    37, 16, 38,
    
    38, 16, 17,
    38, 17, 18,
    38, 18, 19,
    38, 19, 20,
    38, 20, 21,
    38, 21, 22,
    38, 22, 23,
    38, 23, 24,
    38, 24, 25,
    38, 25, 26,
    38, 26, 27,
    38, 27, 28,
    38, 39, 28,
    39, 28, 29,
    39, 29, 30,
    39, 30, 40,
    40, 30, 31,
    40, 31, 41,
    41, 31, 42,
    42, 31, 32,
    
    106, 0,  33,
    106, 33, 34,
    106, 34, 35,
    
    106, 35, 107,
    
    107, 35, 36,
    107, 36, 37,
    107, 37, 38,
    
    107, 38, 108,
    
    108, 38, 39,
    108, 39, 40,
    108, 40, 109,
    
    109, 40, 41,
    109, 41, 42,
    109, 42, 32,
};


const face2DIndexType Face2DMeshIndexes_outborder[] =
{
    0,  1,  33,
    1,  2,  33,
    2,  34, 33,
    2,  3,  34,
    3,  4,  34,
    4,  35, 34,
    4,  5,  35,
    5,  36, 35,
    5,  6,  36,
    6,  7,  36,
    7,  37, 36,
    7,  8,  37,
    8,  9,  37,
    9,  10, 37,
    10, 11, 37,
    11, 12, 37,
    12, 13, 37,
    13, 14, 37,
    14, 15, 37,
    15, 16, 37,
    
    37, 16, 38,
    
    38, 16, 17,
    38, 17, 18,
    38, 18, 19,
    38, 19, 20,
    38, 20, 21,
    38, 21, 22,
    38, 22, 23,
    38, 23, 24,
    38, 24, 25,
    38, 25, 26,
    38, 26, 27,
    38, 27, 28,
    38, 39, 28,
    39, 28, 29,
    39, 29, 30,
    39, 30, 40,
    40, 30, 31,
    40, 31, 41,
    41, 31, 42,
    42, 31, 32,
    
};


const face2DIndexType Face2DMeshIndexes_Simple[] =
{
    2, 52, 34,
    2, 8, 52,
    52, 8, 84,
    8, 12, 84,
    84, 12, 16,
    34, 52, 104,
    52, 82, 74,
    52, 84, 82,
    84, 97, 82,
    84, 103,97,
    84, 16, 103,
    34, 74, 43,
    74, 82, 43,
    82, 46, 43,
    82, 97, 46,
    97, 99, 46,
    97, 103,99,
    103,101,99,
    103, 16, 101,
    34, 43, 41,
    43, 46, 83,
    46, 99, 83,
    99, 90, 83,
    99, 101,90,
    101,16, 90,
    16, 20, 90,
    43, 77, 41,
    43, 83, 77,
    83, 61, 77,
    83, 90, 61,
    90, 24, 61,
    90, 20, 24,
    41, 77, 61,
    41, 61, 30,
    61, 24, 30,
};

//const face2DIndexType* Face2DMeshIndexes = Face2DMeshIndexes_Border;


struct FaceMeshVertex
{
    float pos[3];
    float videoUV[2];
    float maskUV[2];
};

class Face2DMesh : public DynamicMeshImpl<iFace2DMesh>
{
private:
    int32_t mFaceID;
    bool    mIsLockFace;

    bool    mIsBaseXScaling;
    
    rect    mPosRect;
    
    // Screen position base by designed size of face rect.
    rect    mFaceRect;
    
    float   mMul1;
    float   mMul2;
    
    vec2    mAnchor;
    
public:
    Face2DMesh()
    : mFaceID(0)
    , mIsLockFace(true)
    , mIsBaseXScaling(false)
    , mMul1(1.5f)
    , mMul2(2.0f)
    {
        mMesh->SetCount(sizeof(Face2DMeshIndexes)/sizeof(face2DIndexType));
        
        mVertexBuffer = CreateMemory(110 * sizeof(FaceMeshVertex));
        mIndexBuffer = CreateMemory(sizeof(Face2DMeshIndexes)/sizeof(face2DIndexType));
    }
    
    virtual ~Face2DMesh()
    {
        
    }

    virtual bool IsLockFace() const
    {
        return mIsLockFace;
    }
    
    virtual void SetLockFace(bool flag)
    {
        mIsLockFace = flag;
    }

    virtual void SetAnchor(const vec2& anchor)
    {
        mAnchor = anchor;
    }
    
    virtual vec2 GetAnchor() const
    {
        return mAnchor;
    }
    
    virtual rect GetPosRect() const
    {
        return mPosRect;
    }
    
    virtual void SetPosRect(const rect& posRect)
    {
        mPosRect = posRect;
    }
    
    virtual vec2 GetPos() const
    {
        return vec2( mPosRect.x, mPosRect.y);
    }
    
    virtual void SetPos(const vec2& pos)
    {
        mPosRect.x = pos.x;
        mPosRect.y = pos.y;
    }
    
    virtual float GetFaceHeadMul1() const
    {
        return mMul1;
    }
    
    virtual void SetFaceHeadMul1(float mul1)
    {
        mMul1 = mul1;
    }
    
    virtual float GetFaceHeadMul2() const
    {
        return mMul2;
    }
    
    virtual void SetFaceHeadMul2(float mul2)
    {
        mMul2 = mul2;
    }

    virtual bool IsBaseXScaling() const
    {
        return mIsBaseXScaling;
    }
    
    virtual void SetBaseXScaling(bool flag)
    {
        mIsBaseXScaling = flag;
    }
    
    virtual int32_t GetFaceID() const
    {
        return mFaceID;
    }
    
    virtual void SetFaceID(int32_t faceID)
    {
        mFaceID = faceID;
    }
 
    virtual rect GetFaceScreenRect()
    {
        return mFaceRect;
    }
//    virtual void OnResize(const rect& bounds)
//    {
//        Apply();
//    }
    
    virtual void OnUpdate(float delta)
    {
        Apply();
    }
    
    virtual bool InitMeshBuffer()
    {
        SmartPtr<iEntity> entity = GetEntity();
        piAssert(!entity.IsNull(), false;);
        SmartPtr<iScene> scene = entity->GetScene();
        piAssert(!scene.IsNull(), false;);
        
        SmartPtr<iHID> HID = scene->GetHID();
        piCheck(!HID.IsNull(), false;);
        
        SmartPtr<iFaceTrackerResult> faceTrackerResult =  HID->GetFaceTrackerResult();
        piCheck(!faceTrackerResult.IsNull(), false;);
        
        piCheck(faceTrackerResult->GetFaceCount() > mFaceID, false; );
        
        SmartPtr<iFaceInfo> faceInfo = faceTrackerResult->GetFace(mFaceID);
        
        SmartPtr<iVec3Array> facePostions = faceInfo->GetPoints();
        
        mVertexBuffer = CreateMemory((facePostions->GetCount() + 4) * sizeof(FaceMeshVertex) );
        mIndexBuffer = CreateMemory(sizeof(Face2DMeshIndexes)/sizeof(face2DIndexType));
        
        BuildMeshBuffer(scene, entity, faceInfo, facePostions);
        BuildMeshIndex();
        
        return true;
    }
    
#define BUILDVERTEXCODE_LOCKCENTER( faceUVPosV3, CenterVideoUV, i)\
        //vertexes[i].pos[0] = (faceUVPosV3.x - CenterVideoUV.x) * scene->GetDesignSize().x / scene->GetPixelSize() * scene->GetRenderScale() ;\
        vertexes[i].pos[1] = (faceUVPosV3.y - CenterVideoUV.y) * scene->GetDesignSize().y / scene->GetPixelSize() * scene->GetRenderScale() ;\
        vertexes[i].pos[2] = faceUVPosV3.z;\
        \
        vertexes[i].videoUV[0] = faceUVPosV3.x;\
        vertexes[i].videoUV[1] = faceUVPosV3.y;\
        \
        vertexes[i].maskUV[0] = faceUVPosV3.x;\
        vertexes[i].maskUV[1] = faceUVPosV3.y;\

#define BUILDVERTEXCODE_IN_VIDEO( facePosV3, i)\
    //vertexes[i].pos[0] = (facePosV3.x - 0.5f) * scene->GetDesignSize().x / scene->GetPixelSize() * scene->GetRenderScale() ;\
    vertexes[i].pos[1] = (facePosV3.y - 0.5f) * scene->GetDesignSize().y / scene->GetPixelSize() * scene->GetRenderScale() ;\
    vertexes[i].pos[2] = facePosV3.z;\
    \
    vertexes[i].videoUV[0] = facePosV3.x;\
    vertexes[i].videoUV[1] = facePosV3.y;\
    \
    vertexes[i].maskUV[0] = facePosV3.x;\
    vertexes[i].maskUV[1] = facePosV3.y;
    
    void BuildMeshBuffer(iScene* scene, iEntity* entity, iFaceInfo* info, iVec3Array* FacePoints)
    {
        piAssert(entity != nullptr, ;);
        piAssert(scene != nullptr, ;);
        piAssert(info != nullptr, ;);
        piAssert(FacePoints != nullptr, ;);
        piAssert(!mVertexBuffer.IsNull(), ;);
        
        SmartPtr<iTransform> transform = piGetEntityComponent<iTransform>(GetEntity());
        
        /*
         0 - 2
         | / |
         1 - 3
         */
        
        //mRenderScale = 1.0f;
        
        piCheck(scene->GetPixelSize() > 0, ;);
        
        FaceMeshVertex* vertexes = (FaceMeshVertex*)mVertexBuffer->Ptr();
        
        if ( !IsLockFace() )
        {
        
            int32_t i = 0;
            for ( ; i < FacePoints->GetCount(); ++i)
            {
                BUILDVERTEXCODE_IN_VIDEO(FacePoints->GetItem(i),i);
            }
        
            vec3 temp;
        
            //-------Add the point for the top of head.
        
            //mMul1 = 2.0f;
            //mMul2 = 2.35f;
        
            //-------------------- left0 --------------------------
        
            temp = FacePoints->GetItem(0);
            temp += (FacePoints->GetItem(0) - FacePoints->GetItem(2)) * mMul1;
        
            BUILDVERTEXCODE_IN_VIDEO(temp,i);
            //-------------------- left1 --------------------------
            ++i;
        
            temp = FacePoints->GetItem(34);
            temp += (FacePoints->GetItem(34) - FacePoints->GetItem(52)) * mMul2;
        
            BUILDVERTEXCODE_IN_VIDEO(temp,i);
        
            //-------------------- right0 --------------------------
            ++i;
        
            temp = FacePoints->GetItem(41);
            temp += (FacePoints->GetItem(41) - FacePoints->GetItem(61)) * mMul2;
        
            BUILDVERTEXCODE_IN_VIDEO(temp,i);
        
            //-------------------- right1 --------------------------
            ++i;
        
            temp = FacePoints->GetItem(32);
            temp += (FacePoints->GetItem(32) - FacePoints->GetItem(30)) * mMul1;
            BUILDVERTEXCODE_IN_VIDEO(temp,i);
        }
        else
        {
            rect rtFace = info->GetRect();
            
            vec2 CenterVideoUV;
            CenterVideoUV.x = rtFace.x + rtFace.width*0.5f;
            CenterVideoUV.y = rtFace.y + rtFace.height*0.5f;
            
            vec3 totalCenter;
            vec4 faceModelBound; // left, bottom, right, top,
            
            faceModelBound.x = 1000000.0f;
            faceModelBound.y = 1000000.0f;
            faceModelBound.z = -1000000.0f;
            faceModelBound.w = -1000000.0f;
            
            int32_t leftestIndex;
            int32_t rightestIndex;
            
            int32_t i = 0;
            for ( ; i < FacePoints->GetCount(); ++i)
            {
                vec3 cur = FacePoints->GetItem(i);
                totalCenter += cur;
                if (cur.x < faceModelBound.x)
                {
                    faceModelBound.x = cur.x;
                    leftestIndex = i;
                }
                
                if (cur.y < faceModelBound.y)
                {
                    faceModelBound.y = cur.y;
                }
                
                if (cur.x > faceModelBound.z)
                {
                    faceModelBound.z = cur.x;
                    rightestIndex = i;
                }
                
                if (cur.y > faceModelBound.w)
                {
                    faceModelBound.w = cur.y;
                }
            }
            
            vec2 facePos;
            vec2 faceSize;

            vec2 faceUVSize = vec2(faceModelBound.z - faceModelBound.x, faceModelBound.w - faceModelBound.y);
            
            vec2 faceScreenSize = scene->VideoUVToScreenPos(faceUVSize);
            
            // the Scale of face show do the things:
            // 1. Make the face x:y as the truly world.
            // 2. Make the face fixed pos rect.
            vec3 ScaleSize = vec3(1,1,1);
            
            vec2 facePosV2;
            vec2 facePosSize;
            
            if (IsBaseXScaling())
            {
                facePosSize.x = GetPosRect().width;
                facePosSize.y = GetPosRect().width / (faceScreenSize.x / faceScreenSize.y);
                facePosV2.x = GetPosRect().x;
                facePosV2.y = GetPosRect().y + 0.5f*(GetPosRect().height - facePosSize.y);
                
                faceSize = scene->ScreenSizeToGLOrthSize(facePosSize);
                facePos = scene->ScreenPosToGLOrthPos(facePosV2);
            }
            else
            {
                facePosSize.x = GetPosRect().height * (faceScreenSize.x / faceScreenSize.y);
                facePosSize.y = GetPosRect().height;
                facePosV2.x = GetPosRect().x + 0.5f*(GetPosRect().width - facePosSize.x);
                facePosV2.y = GetPosRect().y;
                
                faceSize = scene->ScreenSizeToGLOrthSize(facePosSize);
                facePos = scene->ScreenPosToGLOrthPos(facePosV2);
            
            }
            
            mFaceRect.x         = facePosV2.x;
            mFaceRect.y         = facePosV2.y;
            mFaceRect.width     = facePosSize.x;
            mFaceRect.height    = facePosSize.y;
            
            ScaleSize = vec3(facePosSize.x/faceScreenSize.x,facePosSize.y/faceScreenSize.y,1);
            
            totalCenter /= FacePoints->GetCount();
            totalCenter.x = (faceModelBound.x + faceModelBound.z)*0.5f;
            totalCenter.y = (faceModelBound.y + faceModelBound.w)*0.5f;
            
            i = 0;
            for ( ; i < FacePoints->GetCount(); ++i)
            {
                BUILDVERTEXCODE_LOCKCENTER(FacePoints->GetItem(i),totalCenter,i);
            }
            
            vec3 temp;
            
            //-------Add the point for the top of head.
            
            //float Mul1 = 1.5f;
            //float Mul2 = 1.2f;
            
            //-------------------- left0 --------------------------
            
            temp = FacePoints->GetItem(0);
            temp += (FacePoints->GetItem(0) - FacePoints->GetItem(2)) * mMul1;
            
            BUILDVERTEXCODE_LOCKCENTER(temp,totalCenter,i);
            
            //-------------------- left1 --------------------------
            ++i;
            
            temp = FacePoints->GetItem(34);
            temp += (FacePoints->GetItem(34) - FacePoints->GetItem(52)) * mMul2;
            
            BUILDVERTEXCODE_LOCKCENTER(temp,totalCenter,i);
            
            //-------------------- right0 --------------------------
            ++i;
            
            temp = FacePoints->GetItem(41);
            temp += (FacePoints->GetItem(41) - FacePoints->GetItem(61)) * mMul2;
            
            BUILDVERTEXCODE_LOCKCENTER(temp,totalCenter,i);
            
            //-------------------- right1 --------------------------
            ++i;
            
            temp = FacePoints->GetItem(32);
            temp += (FacePoints->GetItem(32) - FacePoints->GetItem(30)) * mMul1;
            
            BUILDVERTEXCODE_LOCKCENTER(temp,totalCenter,i);
            
            //------------Lock Roll
            SmartPtr<iModelNode> modelNode = GetModelNode();
            piAssert(!modelNode.IsNull(), ;);
            
            SmartPtr<iTransform> localTransform = piGetEntityComponent<iTransform>(entity);
            
            SmartPtr<iTransform> transform = modelNode->GetTransform();
            
            float rollAngle = piglm::degrees(info->GetRoll());
            
            vec3 positionOffset = vec3(totalCenter.x - CenterVideoUV.x,totalCenter.y - CenterVideoUV.y,0); // the center position of face.
            
            // Apply position infor in pos rect.
            vec3 posRectOffset = scene->ScreenPosToGLOrthPosV3(vec3(facePosV2.x + 0.5f*facePosSize.x,facePosV2.y + 0.5f*facePosSize.y, 0));
            
            // Apply anchor infor.
            if (!localTransform.IsNull())
            {
                vec3 anchorOffset = scene->ScreenSizeToGLOrthSizeV3( vec3( (GetAnchor().x - 0.5f)*facePosSize.x,(GetAnchor().y - 0.5f)*facePosSize.y, 0 ));
                positionOffset -= anchorOffset;
                
                localTransform->SetTranslation(anchorOffset + posRectOffset);
                transform->SetTranslation(positionOffset);
            }
            else
            {
                // Do not have itransform, can't rotate of scale the obj, anchor is no need.  
                transform->SetTranslation(positionOffset+posRectOffset);
            }
            
            
            
            transform->SetOrientation(vec3(0,0,-rollAngle));
            transform->SetScale(ScaleSize);
            
        }
    }
    
#undef BUILDVERTEXCODE_LOCKCENTER
#undef BUILDVERTEXCODE_IN_VIDEO
    
    void BuildMeshIndex()
    {
        int32_t IndexCount = sizeof(Face2DMeshIndexes)/sizeof(face2DIndexType);
        
        switch (mMesh->GetType())
        {
            case eType_U8:
            {
                
                uint8_t* buf = (uint8_t *)mIndexBuffer->Ptr();
                
                for (int32_t i = 0; i < IndexCount; ++i)
                {
                    buf[i] = Face2DMeshIndexes[i];
                }
            }
                break;
            case eType_U16:
            {
                uint16_t* buf = (uint16_t *)mIndexBuffer->Ptr();
                
                for (int32_t i = 0; i < IndexCount; ++i)
                {
                    buf[i] = Face2DMeshIndexes[i];
                }
                
            }
                break;
            case eType_U32:
            {
                uint32_t* buf = (uint32_t *)mIndexBuffer->Ptr();
                
                for (int32_t i = 0; i < IndexCount; ++i)
                {
                    buf[i] = Face2DMeshIndexes[i];
                }
                
            }
                break;
                
            default:
                break;
        }
    }
    
    virtual void ApplyVAO()
    {
        // TO DO if the vertex format is dynamic, this logic should be change.
        if (mVAO == 0)
        {
            mVAO = piCreateVertexArray();
            PILOGI(PI_GAME_TAG, "Face2DMesh VAO:[%d]", mVAO);
            
            piBindVertexArray(mVAO);
            
            piBindBuffer(eGraphicsBuffer_Vertex, mVBO);
            piBindBuffer(eGraphicsBuffer_Index, mIBO);
            
            piEnableVertexAttr(0);
            piEnableVertexAttr(1);
            piEnableVertexAttr(2);
            
            piVertexAttr(0, 3, eType_F32, sizeof(FaceMeshVertex), offsetof(FaceMeshVertex, pos));
            piVertexAttr(1, 2, eType_F32, sizeof(FaceMeshVertex), offsetof(FaceMeshVertex, videoUV));
            piVertexAttr(2, 2, eType_F32, sizeof(FaceMeshVertex), offsetof(FaceMeshVertex, maskUV));
            
            piBindVertexArray(0);
            piBindBuffer(eGraphicsBuffer_Vertex, 0);
            piBindBuffer(eGraphicsBuffer_Index, 0);
        }
    }

};

iFace2DMesh* CreateFace2DMesh()
{
    return new Face2DMesh();
}

NSPI_END()
