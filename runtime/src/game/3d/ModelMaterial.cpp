/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.9.25   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()

class ModelMaterial : public iModelMaterial
{
protected:
    int mRenderMode;
    string mName;
    
    string mAlbedoUri;
    string mMaskUri;
    string mEnvMapUri;
    
    SmartPtr<iTexture2D> mAlbedoTex;
    SmartPtr<iTexture2D> mMaskTex;
    SmartPtr<iCubeMap> mEnvMap;
    
public:
    ModelMaterial():
    mRenderMode(eRenderMode_Opaque)
    {
    }
    
    virtual ~ModelMaterial()
    {
    }
    
    virtual void SetName(const std::string& name)
    {
        mName = name;
    }
    
    virtual string GetName() const
    {
        return mName;
    }
    
    virtual void SetRenderMode(int mode)
    {
        mRenderMode = mode;
    }
    
    virtual int GetRenderMode() const
    {
        return mRenderMode;
    }
    
    virtual void SetAlbedoUri(const string& uri)
    {
        mAlbedoUri = uri;
    }
    
    virtual string GetAlbedoUri() const
    {
        return mAlbedoUri;
    }
    
    virtual void SetMaskUri(const string& uri)
    {
        mMaskUri = uri;
    }
    
    virtual string GetMaskUri() const
    {
        return mMaskUri;
    }
    
    
    virtual void SetAlbedoTex(iTexture2D* tex)
    {
        mAlbedoTex = tex;
    }
    
    virtual iTexture2D* GetAlbedoTex() const
    {
        return mAlbedoTex;
    }
    
    virtual void SetMaskTex(iTexture2D* tex)
    {
        mMaskTex = tex;
    }
    
    virtual iTexture2D* GetMaskTex() const
    {
        return mMaskTex;
    }
    
    
    virtual void SetEnvMapUri(const string& uri)
    {
        mEnvMapUri = uri;
    }
    
    virtual string GetEnvMapUri() const
    {
        return mEnvMapUri;
    }
    
    virtual void SetEnvMap(iCubeMap* cubeMap)
    {
        mEnvMap = cubeMap;
    }
    
    virtual iCubeMap* GetEnvMap() const
    {
        return mEnvMap;
    }
};


iModelMaterial* CreateModelMaterial()
{
    return new ModelMaterial();
}




NSPI_END()























