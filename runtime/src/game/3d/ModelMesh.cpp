/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.7   1.0     Create
 ******************************************************************************/
#include <pi/Game.h>
#include "../../asset/AssetImpl.h"

using namespace std;

NSPI_BEGIN()

class MeshComp : public iMeshComp
{
private:
    int mName;
    SmartPtr<iMemory> mData;
    
public:
    MeshComp():
    mName(0)
    {
    }
    
    virtual ~MeshComp()
    {
    }
    
    virtual int GetName() const
    {
        return mName;
    }
    
    virtual void SetName(int name)
    {
        mName = name;
    }
    
    virtual void SetData(iMemory* data)
    {
        mData = data;
    }
    
    virtual iMemory* GetData() const
    {
        return mData;
    }
};

iMeshComp* CreateMeshComp()
{
    return new MeshComp();
}


class ModelMesh : public AssetImpl<iModelMesh>
{
private:
    string mName;
    int32_t mVertexCount;
    SmartPtr<iMeshCompArray> mComps;
    
    SmartPtr<iMaterial> mMat;
    
public:
    ModelMesh():
    mVertexCount(0)
    {
        mComps = CreateMeshCompArray();
    }
    
    virtual ~ModelMesh()
    {
    }
    
    virtual void SetVertexCount(int32_t count)
    {
        piAssert(count > 0, ;);
        mVertexCount = count;
    }
    
    virtual int32_t GetVertexCount() const
    {
        return mVertexCount;
    }
    
    virtual void SetName(const string& name)
    {
        mName = name;
    }
    
    virtual string GetName() const
    {
        return mName;
    }
    
    virtual iMeshCompArray* GetComps() const
    {
        return mComps;
    }
    
    virtual void SetComps(iMeshCompArray* components)
    {
        piAssert(components != nullptr, ;);
        mComps = components;
    }
    
    virtual iMeshComp* GetComp(int name) const
    {
        for (auto i = 0; i < mComps->GetCount(); ++i)
        {
            SmartPtr<iMeshComp> c = mComps->GetItem(i);
            if (c->GetName() == name)
            {
                return c;
            }
        }
        
        return nullptr;
    }
    
    virtual void SetMaterial(iMaterial* material)
    {
        mMat = material;
    }
    
    virtual iMaterial* GetMaterial() const
    {
        return mMat;
    }
};

iModelMesh* CreateModelMesh()
{
    return new ModelMesh();
}

NSPI_END()












