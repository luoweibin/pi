/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.4.9   1     Create
 ******************************************************************************/
#include <pi/game/impl/SystemImpl.h>
#include "ModelSkinDebug_Program.h"

using namespace std;


NSPI_BEGIN()


struct SkelDebugVertex
{
    float pos[3];
};


class SkeletonDebug : public SystemImpl<iModelSkinDebug>
{
private:
    int32_t mProgram;
    
    int32_t mVAO;
    int32_t mVBO;
    int32_t mIBO;
    
public:
    SkeletonDebug():
    mProgram(0), mVAO(0), mVBO(0), mIBO(0)
    {
    }
    
    virtual ~SkeletonDebug()
    {
    }
    
    virtual bool Accept(iEntity* entity)
    {
        SmartPtr<iModelInstance> model = piGetEntityComponent<iModelInstance>(entity);
        piCheck(!model.IsNull(), false);
        
        SmartPtr<iModelScene> lib = model->GetModelScene();
        piCheck(!lib.IsNull(), false);
        
        return !lib->GetSkins()->IsEmpty();
    }
    
    virtual void OnLoad()
    {
        mProgram = SkeletonDebug_CreateProgram();
        
        mVBO = CreateVBO();
        mIBO = CreateIBO();
        
        mVAO = piCreateVertexArray();
        piBindVertexArray(mVAO);
        piBindBuffer(eGraphicsBuffer_Vertex, mVBO);
        piBindBuffer(eGraphicsBuffer_Index, mIBO);
        piEnableVertexAttr(0);
        piVertexAttr(0, 3, eType_F32, sizeof(SkelDebugVertex), 0);
        piBindVertexArray(0);
    }
    
    virtual void OnUnload()
    {
        piReleaseGraphicsObject(mProgram);
        mProgram = 0;
        
        piReleaseGraphicsObject(mVAO);
        mVAO = 0;
        
        piReleaseGraphicsObject(mVBO);
        mVBO = 0;
        
        piReleaseGraphicsObject(mIBO);
        mIBO = 0;
    }
    
    virtual void OnUpdate(float delta)
    {
        piCheck(!mEntities->IsEmpty(), ;);
        
        SmartPtr<iScene> scene = GetScene();
        mat4 matVP;
        piCheck(BuildMatVP(scene, matVP), ;);
        
        piEach<iEntityArray, iEntity*>(mEntities,
                                       [this, matVP](iEntity* e) {
                                           Render(e, matVP);
                                       });
    }
    
private:
    
    int32_t CreateVBO()
    {
        int32_t vbo = piCreateBuffer();
        piBindBuffer(eGraphicsBuffer_Vertex, vbo);
        
        float size = 0.05;
        vec3 a(-size, -size, -size);
        vec3 b(size, size, size);
        
        static const SkelDebugVertex vertices[] =
        {
            // 上表面
            {a.x, b.y, b.z},
            {a.x, b.y, a.z},
            {b.x, b.y, a.z},
            {b.x, b.y, b.z},
            
            // 下表面
            {a.x, a.y, b.z},
            {a.x, a.y, a.z},
            {b.x, a.y, a.z},
            {b.x, a.y, b.z},
        };
        
        SmartPtr<iMemory> mem = CreateMemoryStatic(vertices, sizeof(vertices));
        piBufferData(eGraphicsBuffer_Vertex, vbo, mem->Size(), mem);
        
        piBindBuffer(eGraphicsBuffer_Vertex, 0);
        return vbo;
    }
    
    int32_t CreateIBO()
    {
        static const uint16_t indices[] =
        {
            0, 1, 2, 2, 3, 0,
            4, 7, 6, 6, 5, 4,
            5, 6, 2, 2, 1, 5,
            3, 7, 6, 6, 2, 3,
            4, 0, 1, 1, 5, 4,
            4, 0, 3, 3, 7, 4,
        };
        SmartPtr<iMemory> idxMem = CreateMemoryStatic(indices, sizeof(indices));
        
        int32_t name = piCreateBuffer();
        piBindBuffer(eGraphicsBuffer_Index, name);
        piBufferData(eGraphicsBuffer_Index, name, idxMem->Size(), idxMem);
        piBindBuffer(eGraphicsBuffer_Index, 0);
        
        return name;
    }
    
    bool BuildMatVP(iScene* scene, mat4& matrix)
    {
        SmartPtr<iEntity> camEntity = scene->GetFirstActiveCameraEntity();
        piCheck(!camEntity.IsNull(), false);
        
        SmartPtr<iCamera> cam = piGetEntityComponent<iCamera>(camEntity);
        int mode = cam->GetMode();
        
        float near = cam->GetNear();
        float far = cam->GetFar();
        
        rect bounds = scene->GetBounds();
        
        float ratio = bounds.width / bounds.height;
        float width = ratio / 2;
        float height = 0.5;
        
        mat4 projMatrix = piglm::ortho(-width, width, -height, height, near, far);
        if (mode == eCameraMode_Persp)
        {
            projMatrix = piglm::frustum(-width, width, -height, height, near, far);
        }
        
        mat4 viewMatrix = piglm::lookAt(vec3(0, 0, 5), vec3(0, 0, 0), vec3(0, 1, 0));
        
        matrix = projMatrix * viewMatrix;
        
        return true;
    }
    
    void Render(iEntity* entity, const mat4& vp)
    {
        mat4 mvp = vp * entity->EvaluateGlobalTransform();
        
        SmartPtr<iModelNodeArray> jms = BuildJMs(entity);
        piCheck(!jms.IsNull(), ;);
        
        piPushGroupMarker(piFormat("SkeletonDebug::Render[%s]", entity->GetName().c_str()));
        
        int32_t features = eGraphicsFeature_DepthTest | eGraphicsFeature_Blend;
        piEnableFeatures(features);
        
        piBlendFunc(eBlendFunc_SrcAlpha, eBlendFunc_OneMinusSrcAlpha);
        
        piUseProgram(mProgram);
        
        piBindVertexArray(mVAO);
        
        for (auto i = 0; i < jms->GetCount(); ++i)
        {
            piUniformMat4f(mProgram, "MVPMatrix", mvp * jms->GetItem(i)->GetTransform()->GetMatrix());
            //        piUniformMat4fv(mProgram, "JMs", jms);
            //            piDrawElementsInstanced(eGraphicsDraw_Triangles, 36, eType_U16, jms->GetCount());
            piDrawElements(eGraphicsDraw_Triangles, 36, eType_U16);
        }
        
        piBindVertexArray(0);
        piBindTexture(eTexTarget_2D, 0);
        piUseProgram(0);
        piDisableFeatures(features);
        
        
        piPopGroupMarker();
    }
    
    iModelNodeArray* BuildJMs(iEntity* entity)
    {
        SmartPtr<iModelInstance> model = piGetEntityComponent<iModelInstance>(entity);
        SmartPtr<iModelScene> modeLib = model->GetModelScene();
        SmartPtr<iModelSkin> skelLib = modeLib->GetSkins()->GetItem(0);
        return skelLib->GetJoints();
    }
};


iModelSkinDebug* CreateModelSkinDebug()
{
    return new SkeletonDebug();
}




NSPI_END()
























