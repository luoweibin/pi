/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.3   1.0     Create
 ******************************************************************************/
#include <pi/Game.h>
#include "../../asset/AssetImpl.h"

using namespace std;


NSPI_BEGIN()


class ModelSkin : public AssetImpl<iModelSkin>
{
private:
    string mName;
    SmartPtr<iModelNodeArray> mJoints;
    SmartPtr<iMat4Array> mIBMs;
    SmartPtr<iModelMesh> mMesh;
    
public:
    ModelSkin()
    {
        mIBMs = CreateMat4Array();
        mJoints = CreateModelNodeArray();
    }
    
    virtual ~ModelSkin()
    {
    }
    
    virtual void SetName(const std::string& name)
    {
        mName = name;
    }
    
    virtual std::string GetName() const
    {
        return mName;
    }
    
    virtual iMat4Array* GetIBMs() const
    {
        return mIBMs;
    }
    
    virtual void SetMesh(iModelMesh* mesh)
    {
        piAssert(mesh != nullptr, ;);
        mMesh = mesh;
    }
    
    virtual iModelMesh* GetMesh() const
    {
        return mMesh;
    }
    
    virtual void SetJoints(iModelNodeArray* joints)
    {
        piAssert(joints != nullptr, ;);
        mJoints = joints;
    }
    
    virtual iModelNodeArray* GetJoints() const
    {
        return mJoints;
    }
 };


iModelSkin* CreateModelSkin()
{
    return new ModelSkin();
}


NSPI_END()



















