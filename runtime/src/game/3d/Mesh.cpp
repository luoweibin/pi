/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.4.28   1.0     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()

static int MakeType(int32_t count)
{
    if (count <= std::numeric_limits<uint8_t>::max())
    {
        return eType_U8;
    }
    else if (count <= std::numeric_limits<uint16_t>::max())
    {
        return eType_U16;
    }
    else
    {
        return eType_U32;
    }
}

class Mesh : public iMesh
{
private:
    int32_t mIBO;
    int32_t mVBO;
    int32_t mCount;
    int mType;
    
public:
    Mesh():
    mIBO(0), mVBO(0), mCount(0), mType(eType_U16)
    {
    }
    
    virtual ~Mesh()
    {
        piReleaseGraphicsObject(mIBO);
        piReleaseGraphicsObject(mVBO);
    }
    
    virtual void SetVBO(int32_t vbo)
    {
        piAssert(vbo > 0, ;);
        
        piRetainGraphicsObject(vbo);
        piReleaseGraphicsObject(mVBO);
        
        mVBO = vbo;
    }
    
    virtual int32_t GetVBO() const
    {
        return mVBO;
    }
    
    virtual void SetIBO(int32_t ibo)
    {
        piAssert(ibo > 0, ;);
        
        piRetainGraphicsObject(ibo);
        piReleaseGraphicsObject(mIBO);
        
        mIBO = ibo;
    }
    
    virtual int32_t GetIBO() const
    {
        return mIBO;
    }
    
    virtual void SetCount(int32_t count)
    {
        piAssert(count > 0, ;);
        mCount = count;
        
        mType = MakeType(count);
    }
    
    virtual int32_t GetCount() const
    {
        return mCount;
    }
    
    virtual void SetType(int type)
    {
        piAssert(type == eType_U8 || type == eType_U16 || type == eType_U32, ;);
        mType = type;
    }
    
    virtual int GetType() const
    {
        return mType;
    }
    
    virtual int GetIndexSize() const
    {
        switch (mType)
        {
            case eType_U8:
                return 1;
            case eType_U16:
                return 2;
            case eType_U32:
                return 4;
            default:
                return 0;
        }
    }
};


iMesh* CreateMesh()
{
    return new Mesh();
}

static int32_t CreateIBO(int32_t vertCount)
{
    int indexType = MakeType(vertCount);
    
    SmartPtr<iMemory> data;
    switch (indexType)
    {
        case eType_U8:
        {
            vector<uint8_t> buffer(vertCount);
            for (auto i = 0; i < vertCount; ++i)
            {
                buffer[i] = i;
            }
            data = CreateMemoryCopy(buffer.data(), buffer.size());
            break;
        }
        case eType_U16:
        {
            vector<uint16_t> buffer(vertCount);
            for (auto i = 0; i < vertCount; ++i)
            {
                buffer[i] = i;
            }
            data = CreateMemoryCopy(buffer.data(), buffer.size() * sizeof(uint16_t));
            break;
        }
        default:
        {
            vector<uint32_t> buffer(vertCount);
            for (auto i = 0; i < vertCount; ++i)
            {
                buffer[i] = i;
            }
            data = CreateMemoryCopy(buffer.data(), buffer.size() * sizeof(uint32_t));
            break;
        }
    }
    
    int32_t ibo = piCreateBuffer();
    piBindBuffer(eGraphicsBuffer_Index, ibo);
    piBufferData(eGraphicsBuffer_Index, ibo, data->Size(), data);
    piBindBuffer(eGraphicsBuffer_Index, 0);
    
    return ibo;
}



static int32_t CreateVBO(iModelMesh* mesh)
{
    int32_t vertCount = mesh->GetVertexCount();
    piAssert(vertCount > 0, 0);
    
    SmartPtr<iMeshCompArray> comps = mesh->GetComps();
    
    vector<MeshVertex> vertices(vertCount);
    for (auto& v : vertices)
    {
        v.jindex[0] = 0;
        v.jindex[1] = 0;
        v.jindex[2] = 0;
        v.jindex[3] = 0;
        
        v.jweight[0] = 1;
        v.jweight[1] = 0;
        v.jweight[2] = 0;
    }
    
    for (auto cIndex = 0; cIndex < comps->GetCount(); ++cIndex)
    {
        SmartPtr<iMeshComp> comp = comps->GetItem(cIndex);
        SmartPtr<iMemory> data = comp->GetData();
        
        switch (comp->GetName())
        {
            case eMesh_Position:
            {
                float* buffer = (float*)data->Ptr();
                
                for (auto i = 0; i < vertCount; ++i)
                {
                    MeshVertex& v = vertices[i];
                    
                    int32_t vIndex = i * 3;
                    v.pos[0] = buffer[vIndex];
                    v.pos[1] = buffer[vIndex + 1];
                    v.pos[2] = buffer[vIndex + 2];
                }
                
                break;
            }
            case eMesh_UV:
            {
                float* buffer = (float*)data->Ptr();
                
                for (auto i = 0; i < vertCount; ++i)
                {
                    MeshVertex& v = vertices[i];
                    
                    int32_t vIndex = i * 2;
                    v.uv[0] = buffer[vIndex];
                    v.uv[1] = buffer[vIndex + 1];
                }
                
                break;
            }
            case eMesh_JointWeight:
            {
                VertexWeight* w = (VertexWeight*)data->Ptr();
                int32_t count = (int32_t)(data->Size() / sizeof(VertexWeight));
                
                int32_t wIndex = 0;
                int32_t lastVertex = -1;
                for (auto i = 0; i < count; ++i)
                {
                    if (lastVertex != w->vertexIndex)
                    {
                        wIndex = 0;
                    }
                    
                    MeshVertex& v = vertices[w->vertexIndex];
                    v.jindex[wIndex] = w->jointIndex;
                    v.jweight[wIndex] = w->bias;
                    
                    lastVertex = w->vertexIndex;
                    ++wIndex;
                    ++w;
                }
                
                break;
            }
            case eMesh_Normal:
            {
                float* buffer = (float*)data->Ptr();
                
                for (auto i = 0; i < vertCount; ++i)
                {
                    MeshVertex& v = vertices[i];
                    
                    int32_t vIndex = i * 3;
                    v.normal[0] = buffer[vIndex];
                    v.normal[1] = buffer[vIndex + 1];
                    v.normal[2] = buffer[vIndex + 2];
                }
                
                break;
            }
            default:
                break;
        }
    }
    
    int32_t vbo = piCreateBuffer();
    piBindBuffer(eGraphicsBuffer_Vertex, vbo);
    SmartPtr<iMemory> buffer = CreateMemoryCopy(vertices.data(), vertices.size() * sizeof(MeshVertex));
    piBufferData(eGraphicsBuffer_Vertex, vbo, buffer->Size(), buffer);
    piBindBuffer(eGraphicsBuffer_Vertex, 0);
    
    return vbo;
}



iMesh* CreateMeshFromLib(iModelMesh* lib)
{
    int32_t count = lib->GetVertexCount();
    piAssert(count > 0, nullptr);
    
    SmartPtr<iMesh> mesh = CreateMesh();
    
    mesh->SetCount(count);
    
    int32_t ibo = CreateIBO(count);
    mesh->SetIBO(ibo);
    piReleaseGraphicsObject(ibo);
    
    int32_t vbo = CreateVBO(lib);
    mesh->SetVBO(vbo);
    piReleaseGraphicsObject(vbo);
    
    return mesh.PtrAndSetNull();
}



NSPI_END()






























