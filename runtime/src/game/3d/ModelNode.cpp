/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.5   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()


class ModelNode : public iModelNode
{
private:
    int mType;
    string mName;
    iModelNode* mParent;
    SmartPtr<iTransform> mTrans;
    
    SmartPtr<iModelMeshArray> mMeshes;
    SmartPtr<iModelSkinArray> mSkins;
    
    SmartPtr<iModelNodeArray> mChildren;
    
public:
    ModelNode():
    mType(eModelNode_Null), mParent(nullptr)
    {
        mChildren = CreateModelNodeArray();
        mMeshes = CreateModelMeshArray();
        mTrans = CreateTransform();
        mSkins = CreateModelSkinArray();
    }
    
    virtual ~ModelNode()
    {
    }
    
    virtual int GetType() const
    {
        return mType;
    }
    
    virtual void SetType(int type)
    {
        mType = type;
    }
    
    virtual std::string GetName() const
    {
        return mName;
    }
    
    virtual void SetName(const std::string& name)
    {
        mName = name;
    }
    
    virtual iModelNodeArray* GetChildren() const
    {
        return mChildren;
    }
    
    virtual void SetChildren(iModelNodeArray* children)
    {
        piAssert(children != nullptr, ;);
        mChildren = children;
    }
    
    virtual iTransform* GetTransform() const
    {
        return mTrans;
    }
    
    virtual void SetTransform(iTransform* transform)
    {
        piAssert(transform != nullptr, ;);
        mTrans = transform;
    }
    
    virtual iModelNode* GetParent() const
    {
        return mParent;
    }
    
    virtual void SetParent(iModelNode* node)
    {
        mParent = node;
    }
    
    virtual iModelMeshArray* GetMeshes() const
    {
        return mMeshes;
    }
    
    virtual void SetMeshes(iModelMeshArray* meshes)
    {
        piAssert(meshes != nullptr, ;);
        mMeshes = meshes;
    }
    
    virtual void SetSkins(iModelSkinArray* skins)
    {
        piAssert(skins != nullptr, ;);
        mSkins = skins;
    }
    
    virtual iModelSkinArray* GetSkins() const
    {
        return mSkins;
    }
    
    virtual mat4 EvaluateGlobalMatrix() const
    {
        if (mParent != nullptr)
        {
            return mParent->EvaluateGlobalMatrix() * mTrans->GetMatrix();
        }
        else
        {
            return mTrans->GetMatrix();
        }
    }
};



iModelNode* CreateModelNode()
{
    return new ModelNode();
}

NSPI_END()

















