/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.9   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()

class MeshLoader : public iAssetLoader
{
public:
    MeshLoader()
    {
    }
    
    virtual ~MeshLoader()
    {
    }
    
    virtual iAsset* Load(iAssetManager* manager, iClassLoader* classLoader, iStream* stream)
    {
        MeshFile meshFile;
        piAssert(stream->Read(&meshFile, sizeof(meshFile)) == sizeof(meshFile), nullptr);
        
        SmartPtr<iModelMesh> mesh = CreateModelMesh();
        
        mesh->SetName(meshFile.name);
        mesh->SetVertexCount(meshFile.vertexCount);
        
        SmartPtr<iMeshCompArray> comps = mesh->GetComps();
        
        vector<MeshAttr> attrs;
        
        for (auto i = 0; i < meshFile.attrCount; ++i)
        {
            MeshAttr attr;
            piAssert(stream->Read(&attr, sizeof(attr)) == sizeof(attr), nullptr);
            attrs.push_back(attr);
        }
        
        for (auto i = 0; i < meshFile.attrCount; ++i)
        {
            const MeshAttr& attr = attrs[i];
            
            SmartPtr<iMeshComp> comp = CreateMeshComp();
            
            SmartPtr<iMemory> mem = CreateMemory(attr.size);
            piAssert(stream->Read(mem->Ptr(), attr.size) == attr.size, nullptr);
            
            comp->SetName(attr.type);
            comp->SetData(mem);
            
            comps->PushBack(comp);
            
            uint32_t alignSize = piAlignPow2(attr.size, 2);
            stream->Seek(alignSize - attr.size, eSeek_Current);
        }
        
        return mesh.PtrAndSetNull();
    }
};



class MeshLoaderFactory : public iAssetLoaderFactory
{
private:
    SmartPtr<iAssetLoader> mLoader;
    
public:
    MeshLoaderFactory()
    {
        mLoader = new MeshLoader();
    }
    
    virtual ~MeshLoaderFactory()
    {
    }
    
    virtual iAssetLoader* CreateLoader(const std::string& fileExt, iStream* stream)
    {
        return mLoader;
    }
};

iAssetLoaderFactory* CreateMeshLoaderFactory()
{
    return new MeshLoaderFactory();
}


NSPI_END()





















