/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.12   0.1     Create
 ******************************************************************************/
#include <pi/game/impl/ComponentImpl.h>
#include "../../asset/AssetImpl.h"

using namespace std;

NSPI_BEGIN()


class ModelScene : public AssetImpl<ComponentImpl<iModelScene>>
{
private:
    string mName;
    
    SmartPtr<iModelAnim> mModelAnim;
    SmartPtr<iModelMeshArray> mMeshes;
    SmartPtr<iModelSkinArray> mSkins;
    SmartPtr<iMaterialArray> mMats;
    
    SmartPtr<iModelNode> mRootNode;
    
public:
    ModelScene()
    {
        mMeshes = CreateModelMeshArray();
        mSkins = CreateModelSkinArray();
        mMats = CreateMaterialArray();
    }
    
    virtual ~ModelScene()
    {
    }
    
    virtual std::string GetName() const
    {
        return mName;
    }
    
    virtual void SetName(const std::string& name)
    {
        mName = name;
    }
    
    virtual void SetRootNode(iModelNode* root)
    {
        mRootNode = root;
    }
    
    virtual iModelNode* GetRootNode() const
    {
        return mRootNode;
    }
    
    virtual void SetAnimation(iModelAnim* animation)
    {
        mModelAnim = animation;
    }
    
    virtual iModelAnim* GetAnimation() const
    {
        return mModelAnim;
    }
    
    virtual void SetMeshes(iModelMeshArray* meshes)
    {
        piAssert(meshes != nullptr, ;);
        mMeshes = meshes;
    }
    
    virtual iModelMeshArray* GetMeshes() const
    {
        return mMeshes;
    }
    
    virtual iModelMesh* FindMesh(const std::string& name) const
    {
        piAssert(!name.empty(), nullptr);
        
        int32_t index = piIndexOf<iModelMeshArray, iModelMesh*>(mMeshes,
                                                                [name](iModelMesh* mesh) {
                                                                    return mesh->GetName() == name;
                                                                });
        return index >= 0 ? mMeshes->GetItem(index) : nullptr;
    }
    
    virtual void SetSkins(iModelSkinArray* skins)
    {
        piAssert(skins != nullptr, ;);
        mSkins = skins;
    }
    
    virtual iModelSkinArray* GetSkins() const
    {
        return mSkins;
    }
    
    virtual iModelSkin* FindSkin(const std::string& name) const
    {
        piAssert(!name.empty(), nullptr);
        
        int32_t index = piIndexOf<iModelSkinArray, iModelSkin*>(mSkins,
                                                                [name](iModelSkin* skin) {
                                                                    return skin->GetName() == name;
                                                                });
        return index >= 0 ? mSkins->GetItem(index) : nullptr;
    }
    
    virtual iMaterialArray* GetMaterials() const
    {
        return mMats;
    }
    
    virtual void SetMaterials(iMaterialArray* mats)
    {
        piAssert(mats != nullptr, ;);
        mMats = mats;
    }
    
    virtual iMaterial* FindMaterial(const string& name) const
    {
        piAssert(!name.empty(), nullptr);
        
        int32_t index = piIndexOf<iMaterialArray, iMaterial*>(mMats,
                                                              [name](iMaterial* mat) {
                                                                  return mat->GetName() == name;
                                                              });
        return index >= 0 ? mMats->GetItem(index) : nullptr;
    }
    
    virtual void OnUpdate(float delta)
    {
        if (!mModelAnim.IsNull())
        {
            mModelAnim->OnUpdate(delta);
        }
    }
};



iModelScene* CreateModelScene()
{
    return new ModelScene();
}



NSPI_END()
































