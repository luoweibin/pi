/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 Sword     2017.6.30   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>
#include "../../asset/AssetImpl.h"

using namespace std;

NSPI_BEGIN()

class RenderState : public AssetImpl<iRenderState>
{
private:
    int32_t mFeatures;
    int32_t mSrcBlendFunc;
    int32_t mDstBlendFunc;
    
    bool    mIsZWrite;
    union
    {
        bool    boolMask[4];
        int32_t intMask;
    }mColorMask;
    
public:
    RenderState()
    : mFeatures(eGraphicsFeature_CullFace | eGraphicsFeature_DepthTest | eGraphicsFeature_Blend)
    , mSrcBlendFunc(eBlendFunc_SrcAlpha)
    , mDstBlendFunc(eBlendFunc_OneMinusSrcAlpha)
    , mIsZWrite(true)
    {
        mColorMask.intMask = 0x01010101;
    }
    
    virtual ~RenderState()
    {
    }
    
    virtual int32_t GetFeatures() const
    {
        return mFeatures;
    }
    
    virtual void SetFeatures(int32_t features)
    {
        mFeatures = features;
    }
    
    virtual int32_t GetSrcBlendFunc() const
    {
        return mSrcBlendFunc;
    }
    
    virtual void SetSrcBlendFunc(int32_t func)
    {
        mSrcBlendFunc = func;
    }
    
    virtual int32_t GetDstBlendFunc() const
    {
        return mDstBlendFunc;
    }
    
    virtual void SetDstBlendFunc(int32_t func)
    {
        mDstBlendFunc = func;
    }
    
    virtual bool IsZWrite() const
    {
        return mIsZWrite;
    }
    
    virtual void SetZWrite(bool ZWrite)
    {
        mIsZWrite = ZWrite;
    }
    
    virtual int32_t GetColorMask() const
    {
        return mColorMask.intMask;
    }
    
    virtual void SetColorMask(int32_t mask)
    {
        mColorMask.intMask = mask;
    }
    
    virtual void EnableState()
    {
        piEnableFeatures(mFeatures);
        piBlendFunc(mSrcBlendFunc, mDstBlendFunc);
        
        piDepthMask(mIsZWrite);
        
        piColorMask(mColorMask.boolMask[0], mColorMask.boolMask[1], mColorMask.boolMask[2], mColorMask.boolMask[3]);
    }
    
    virtual void DisableState()
    {
        piDisableFeatures(mFeatures);

        piDepthMask(true);
        
        piColorMask(true, true, true, true);
    }

};

iRenderState* CreateRenderState()
{
    return new RenderState;
}

NSPI_END()
