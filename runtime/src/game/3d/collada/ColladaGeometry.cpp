/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.11.21   0.1     Create
 ******************************************************************************/
#include "ColladaUtil.h"

NSPI_BEGIN()

typedef map<string, vector<float>> SourceMap;

static void MapVerticesPosition(iDOMElement* vertices, SourceMap& sources)
{
    string name = vertices->GetAttr("id");
    piAssert(!name.empty(), ;);
    
    SmartPtr<iDOMElementArray> inputs = vertices->GetElementsByTagName("input", false);
    for (int32_t i = 0; i < inputs->GetCount(); ++i)
    {
        SmartPtr<iDOMElement> input = inputs->GetItem(i);
        
        string semantic = input->GetAttr("semantic");
        
        if (semantic != "POSITION")
        {
            continue;
        }
        
        string sourceName = input->GetAttr("source");
        piAssert(!sourceName.empty(), ;);
        sourceName = sourceName.substr(1, sourceName.size() - 1);
        
        SourceMap::iterator it = sources.find(sourceName);
        piAssert(it != sources.end(), ;);
        
        sources[name] = it->second;
        break;
    }
}

static string ReadSource(iDOMElement* src, vector<float>& values)
{
    string name = src->GetAttr("id");
    piAssert(!name.empty(), string());
    
    SmartPtr<iDOMElement> array = src->GetElementByTagName("float_array");
    piAssert(!array.IsNull(), string());
    
    string txtCount = array->GetAttr("count");
    int32_t count = (int32_t)strtol(txtCount.c_str(), nullptr, 10);
    piAssert(count > 0, string());
    
    string data = array->GetText();
    ParseFloatArray(data, values);
    
    return name;
}

static void ReadSources(iDOMElementArray* sources, SourceMap& sourceMems)
{
    for (int32_t i = 0; i < sources->GetCount(); ++i)
    {
        SmartPtr<iDOMElement> src = sources->GetItem(i);
        vector<float> values;
        string name = ReadSource(src, values);
        if (!name.empty())
        {
            sourceMems[name] = values;
        }
    }
}

static iMeshComp* ReadComponent(int32_t inputCount,
                                iDOMElement* i,
                                const SourceMap& sources,
                                const vector<int32_t>& values,
                                int32_t count,
                                const mat4& matOffset)
{
    string name = i->GetAttr("source");
    piAssert(!name.empty(), nullptr);
    name = name.substr(1, name.size() - 1);
    
    SourceMap::const_iterator it = sources.find(name);
    piAssert(it != sources.end(), nullptr);
    
    const vector<float>& source = it->second;
    piAssert(!source.empty(), nullptr);
    
    string semantic = i->GetAttr("semantic");
    piAssert(!semantic.empty(), nullptr);
    
    string txtOffset = i->GetAttr("offset");
    piAssert(!txtOffset.empty(), nullptr);
    int32_t offset = (int32_t)strtol(txtOffset.c_str(), nullptr, 10);
    piAssert(offset >= 0, nullptr);
    
    SmartPtr<iMeshComp> comp = CreateMeshComp();
    
    if (count == values.size())
    {
        if (semantic == "VERTEX")
        {
            SmartPtr<iMemory> mem = CreateMemory(3 * sizeof(float) * count);
            float* p = (float*)mem->Ptr();
            
            for (int32_t i = 0; i < count; ++i)
            {
                piAssert(i < values.size(), nullptr);
                
                float* v = p + i * 3;
                
                int32_t valueIndex = values[i];
                
                vec4 p(source[valueIndex * 3], source[valueIndex * 3 + 1], source[valueIndex * 3 + 2], 1);
                p = matOffset * p;
                
                v[0] = p.x / p.w;
                v[1] = p.y / p.w;
                v[2] = p.z / p.w;
            }
            
            comp->SetData(mem);
            comp->SetName(eMesh_Position);
        }
        else if (semantic == "NORMAL")
        {
            SmartPtr<iMemory> mem = CreateMemory(3 * sizeof(float) * count);
            float* p = (float*)mem->Ptr();
            
            for (int32_t i = 0; i < count; ++i)
            {
                piAssert(i < values.size(), nullptr);
                
                float* v = p + i * 3;

                int32_t valueIndex = values[i];
                
                vec4 p(source[valueIndex * 3], source[valueIndex * 3 + 1], source[valueIndex * 3 + 2], 1);
                p = matOffset * p;
                
                v[0] = p.x / p.w;
                v[1] = p.y / p.w;
                v[2] = p.z / p.w;
            }
            
            comp->SetData(mem);
            comp->SetName(eMesh_Normal);
        }
        else if (semantic == "TEXCOORD")
        {
            SmartPtr<iMemory> mem = CreateMemory(2 * sizeof(float) * count);
            float* p = (float*)mem->Ptr();
            
            for (int32_t i = 0; i < count; ++i)
            {
                piAssert(i < values.size(), nullptr);
                
                float* v = p + i * 2;

                int32_t valueIndex = values[i];
                v[0] = source[valueIndex * 2];
                v[1] = source[valueIndex * 2 + 1];
            }
            
            comp->SetData(mem);
            comp->SetName(eMesh_UV);
        }
    }
    else
    {
        if (semantic == "VERTEX")
        {
            SmartPtr<iMemory> mem = CreateMemory(3 * sizeof(float) * count);
            float* p = (float*)mem->Ptr();
            
            for (int32_t i = 0; i < count; ++i)
            {
                int32_t index = inputCount * i + offset;
                piAssert(index < values.size(), nullptr);
                
                float* v = p + i * 3;
                
                int32_t valueIndex = values[index];
                
                vec4 p(source[valueIndex * 3], source[valueIndex * 3 + 1], source[valueIndex * 3 + 2], 1);
                p = matOffset * p;
                
                v[0] = p.x / p.w;
                v[1] = p.y / p.w;
                v[2] = p.z / p.w;
            }
            
            comp->SetData(mem);
            comp->SetName(eMesh_Position);
        }
        else if (semantic == "NORMAL")
        {
            SmartPtr<iMemory> mem = CreateMemory(3 * sizeof(float) * count);
            float* p = (float*)mem->Ptr();
            
            for (int32_t i = 0; i < count; ++i)
            {
                int32_t index = inputCount * i + offset;
                piAssert(index < values.size(), nullptr);
                
                float* v = p + i * 3;

                int32_t valueIndex = values[index];
                
                vec4 p(source[valueIndex * 3], source[valueIndex * 3 + 1], source[valueIndex * 3 + 2], 1);
                p = matOffset * p;
                
                v[0] = p.x / p.w;
                v[1] = p.y / p.w;
                v[2] = p.z / p.w;
            }
            
            comp->SetData(mem);
            comp->SetName(eMesh_Normal);
        }
        else if (semantic == "TEXCOORD")
        {
            SmartPtr<iMemory> mem = CreateMemory(2 * sizeof(float) * count);
            float* p = (float*)mem->Ptr();
            
            for (int32_t i = 0; i < count; ++i)
            {
                int32_t index = inputCount * i + offset;
                piAssert(index < values.size(), nullptr);
                
                float* v = p + i * 2;

                int32_t valueIndex = values[index];
                
                v[0] = source[valueIndex * 2];
                v[1] = source[valueIndex * 2 + 1];
            }
            
            comp->SetData(mem);
            comp->SetName(eMesh_UV);
        }
    }
    
    return comp.PtrAndSetNull();
}

static int32_t ReadOffset(iDOMElementArray* inputs)
{
    int32_t inputCount = inputs->GetCount();
    for (int32_t i = 0; i < inputCount; ++i)
    {
        SmartPtr<iDOMElement> el = inputs->GetItem(i);
        
        string semantic = el->GetAttr("semantic");
        if (semantic != "VERTEX")
        {
            continue;
        }
        
        string txtOffset = el->GetAttr("offset");
        piAssert(!txtOffset.empty(), 0);
        
        return (int32_t)strtol(txtOffset.c_str(), nullptr, 10);
    }
    
    return -1;
}

static bool ReadFragment(iModelMesh* modelMesh,
                         iDOMElement* triangles,
                         const SourceMap& sources,
                         const mat4& matOffset,
                         const ColladaSkin& skin,
                         iMeshComp* weightsComp)
{
    int32_t vertexCount = 0;
    {
        // 读取顶点数
        string txtCount = triangles->GetAttr("count");
        vertexCount = (int32_t)strtol(txtCount.c_str(), nullptr, 10) * 3;
        piCheck(vertexCount > 0, false);
    }
    
    vector<int32_t> values;
    {
        SmartPtr<iDOMElement> indices = triangles->GetElementByTagName("p");
        piCheck(!indices.IsNull(), false);
        string data = indices->GetText();
        ParseIntArray(data, values);
    }
    piAssert(!values.empty(), false);
    
    
    // 读取输入信息
    SmartPtr<iDOMElementArray> inputs = triangles->GetElementsByTagName("input", false);
    piCheck(!inputs->IsEmpty(), false);
    
    SmartPtr<iMeshCompArray> comps = modelMesh->GetComps();
    
    const auto& weights = skin.weights;
    
    if (weightsComp != nullptr && !weights.empty())
    {
        int32_t inputCount = inputs->GetCount();
        
        vector<VertexWeight> buffer;
        for (auto i = 0; i < vertexCount; ++i)
        {
            int32_t index = 0;
            if (values.size() == vertexCount)
            {
                index = values[i];
            }
            else
            {
                int32_t offset = ReadOffset(inputs);
                index = values[i * inputCount + offset];
            }
            
            const auto& array = weights[index];
            
            for (auto w : array)
            {
                VertexWeight v;
                v.vertexIndex = i;
                v.bias = w.bias;
                v.jointIndex = w.jointIndex;
                buffer.push_back(v);
            }
        }
        
        SmartPtr<iMemory> mem = CreateMemoryCopy(buffer.data(), buffer.size() * sizeof(VertexWeight));
        weightsComp->SetData(mem);
        
        comps->PushBack(weightsComp);
    }
    
        
    modelMesh->SetVertexCount(vertexCount);
    
    {
        // read other comp
        int32_t inputCount = inputs->GetCount();
        for (int32_t i = 0; i < inputCount; ++i)
        {
            SmartPtr<iDOMElement> el = inputs->GetItem(i);
            
            SmartPtr<iMeshComp> comp = ReadComponent(inputCount, el, sources, values, vertexCount, matOffset);
            if (!comp.IsNull())
            {
                comps->PushBack(comp);
            }
        }
    }
    
    return true;
}

static void ReadMesh(iModelMesh* modelMesh,
                     iDOMElement* root,
                     const mat4& matOffset,
                     ParserContext& context,
                     iMeshComp* weightComp,
                     const string& name)
{
    SmartPtr<iDOMElementArray> sourceElements = root->GetElementsByTagName("source", false);
    piAssert(!sourceElements->IsEmpty(), ;);
    
    SourceMap sources;
    ReadSources(sourceElements, sources);
    
    SmartPtr<iDOMElementArray> vertices = root->GetElementsByTagName("vertices", false);
    piAssert(!vertices->IsEmpty(), ;);
    
    for (int32_t i = 0; i < vertices->GetCount(); ++i)
    {
        SmartPtr<iDOMElement> v = vertices->GetItem(i);
        MapVerticesPosition(v, sources);
    }
    
    // 读取片段
    SmartPtr<iDOMElementArray> trianglesArray = root->GetElementsByTagName("triangles", false);
    piAssert(!trianglesArray->IsEmpty(), ;);
    
    piAssert(trianglesArray->GetCount() >= 1, ;);
    
    SmartPtr<iDOMElement> t = trianglesArray->GetItem(0);
    
    ColladaSkin skin;
    for(auto& s: context.skins)
    {
        if(s.geoName == name)
        {
            skin = s;
            break;
        }
    }
    
    ReadFragment(modelMesh, t, sources, matOffset, skin, weightComp);
}


static void ReadGeometry(iModelScene* model, iDOMElement* root, iDOMElement* elGeo, ParserContext& context)
{
    SmartPtr<iDOMElementArray> elMeshes = elGeo->GetElementsByTagName("mesh", false);
    piAssert(!elMeshes.IsNull(), ;);
    
    piAssert(elMeshes->GetCount() >= 1, ;);
    
    string meshName = elGeo->GetAttr("id");
    piAssert(!meshName.empty(), ;);
    
    SmartPtr<iModelMesh> mesh = context.model->FindMesh(meshName);
    piAssert(!mesh.IsNull(), ;);
    
    SmartPtr<iDOMElement> elMesh = elMeshes->GetItem(0);
    
    SmartPtr<iMeshComp> weightsComp;
    
    mat4 matOffset;
    for(auto& s: context.skins)
    {
        if(s.geoName == meshName)
        {
            weightsComp = CreateMeshComp();
            weightsComp->SetName(eMesh_JointWeight);
            matOffset = s.BSM;
            break;
        }
    }
    
    ReadMesh(mesh, elMesh, matOffset, context, weightsComp, meshName);
}


static iModelMesh* PreparseGeometry(iModelScene* model, iDOMElement* root, iDOMElement* elGeo, ParserContext& context)
{
    SmartPtr<iDOMElementArray> elMeshes = elGeo->GetElementsByTagName("mesh", false);
    piAssert(!elMeshes.IsNull(), nullptr);
    
    piAssert(elMeshes->GetCount() >= 1, nullptr);
    
    string meshName = elGeo->GetAttr("id");
    piAssert(!meshName.empty(), nullptr);
    
    SmartPtr<iModelMesh> modelMesh = CreateModelMesh();
    modelMesh->SetName(meshName);
    
    return modelMesh.PtrAndSetNull();
}

void PreparseGeometrys(iDOMElement* root, ParserContext& context)
{
    SmartPtr<iDOMElement> ilbGeometries = root->GetElementByTagName("library_geometries");
    piCheck(!ilbGeometries.IsNull(), ;);
    
    SmartPtr<iModelMeshArray> meshes = context.model->GetMeshes();
    
    SmartPtr<iDOMElementArray> geometries = ilbGeometries->GetElementsByTagName("geometry", false);
    for (int32_t i = 0; i < geometries->GetCount(); ++i)
    {
        SmartPtr<iDOMElement> elGeo = geometries->GetItem(i);
        SmartPtr<iModelMesh> modelMesh = PreparseGeometry(context.model, root, elGeo, context);
        if (!modelMesh.IsNull())
        {
            meshes->PushBack(modelMesh);
        }
    }
}


void ReadGeometrys(iDOMElement* root, ParserContext& context)
{
    SmartPtr<iDOMElement> ilbGeometries = root->GetElementByTagName("library_geometries");
    piCheck(!ilbGeometries.IsNull(), ;);
    
    SmartPtr<iDOMElementArray> geometries = ilbGeometries->GetElementsByTagName("geometry", false);
    for (int32_t i = 0; i < geometries->GetCount(); ++i)
    {
        SmartPtr<iDOMElement> elGeo = geometries->GetItem(i);
        ReadGeometry(context.model, root, elGeo, context);
    }
}


NSPI_END()





















