/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.11.21   0.1     Create
 ******************************************************************************/
#include "ColladaUtil.h"

NSPI_BEGIN()

typedef map<string, SmartPtr<iTexture2D>> TexMap;
typedef map<string, string> SurfaceMap;
typedef map<string, string> MaterialAliasMap;

static void ReadTextureMap(iDOMElement* root, TexMap& texMap, ParserContext& context)
{
    SmartPtr<iDOMElement> libImages = root->GetElementByTagName("library_images");
    piCheck(!libImages.IsNull(), ;);
    
    SmartPtr<iDOMElementArray> images = libImages->GetElementsByTagName("image", false);
    for (int32_t i = 0; i < images->GetCount(); ++i)
    {
        SmartPtr<iDOMElement> image = images->GetItem(i);
        string name = image->GetAttr("id");
        SmartPtr<iDOMElement> elPath = image->GetElementByTagName("init_from");
        if (name.empty() || elPath.IsNull())
        {
            continue;
        }
        
        string path = context.dir + elPath->GetText();
        SmartPtr<iBitmapAsset> bmpAsset = piLoadAsset<iBitmapAsset>(context.assetManager, context.classLoader, path);
        if (bmpAsset.IsNull())
        {
            continue;
        }
        
        SmartPtr<iBitmap> bitmap = bmpAsset->GetBitmap();
        if (!bitmap.IsNull())
        {
            int32_t tex = piCreateTexture();
            piBindTexture(eTexTarget_2D, tex);
            
            piTexParam(eTexTarget_2D, eTexConfig_MinFilter, eTexValue_Linear);
            piTexParam(eTexTarget_2D, eTexConfig_MagFilter, eTexValue_Linear);
            piTexParam(eTexTarget_2D, eTexConfig_WrapS, eTexValue_Clamp);
            piTexParam(eTexTarget_2D, eTexConfig_WrapT, eTexValue_Clamp);
            
            piTexImage2D(eTexTarget_2D, 0, bitmap->GetPixelFormat()->GetName(), bitmap, 0);
            
            piBindTexture(eTexTarget_2D, 0);
            
            SmartPtr<iTexture2D> object = CreateTexture2D();
            object->SetGraphicsName(tex);
            object->SetWidth(bitmap->GetWidth());
            object->SetHeight(bitmap->GetHeight());
            texMap[name] = object;
            
            piReleaseGraphicsObject(tex);
        }
    }
}

static void ReadSurface(iDOMElement* p, map<string, string>& surfaces)
{
    SmartPtr<iDOMElement> surface = p->GetElementByTagName("surface");
    piCheck(!surface.IsNull(), ;);
    
    string sid = p->GetAttr("sid");
    piAssert(!sid.empty(), ;);
    
    SmartPtr<iDOMElement> elInitFrom = surface->GetElementByTagName("init_from");
    piAssert(!elInitFrom.IsNull(), ;);
    
    string name = elInitFrom->GetText();
    piAssert(!name.empty(), ;);
    
    surfaces[sid] = name;
}


struct Sampler
{
    int32_t texture;
    map<int, int> config;
};

static void ReadSampler2D(iDOMElement* p,
                          const SurfaceMap& surfaces,
                          TexMap& texMap)
{
    SmartPtr<iDOMElement> elSampler = p->GetElementByTagName("sampler2D");
    piCheck(!elSampler.IsNull(), ;);
    
    string name = p->GetAttr("sid");
    piAssert(!name.empty(), ;);
 
    Sampler sampler;
    
    SmartPtr<iDOMNode> node = elSampler->GetFirstChild();
    while (node != elSampler->NullNode())
    {
        SmartPtr<iDOMElement> el = dynamic_cast<iDOMElement*>(node.Ptr());
        if (el.IsNull())
        {
            continue;
        }
        
        string tagName = el->GetTagName();
        if (tagName == "source")
        {
            string surface = el->GetText();
            piAssert(!surface.empty(), ;);
            
            SurfaceMap::const_iterator it = surfaces.find(surface);
            piAssert(it != surfaces.end(), ;);
            
            string texName = it->second;
            
            TexMap::const_iterator texIt = texMap.find(texName);
            piAssert(texIt != texMap.end(), ;);
            
            texMap[name] = texIt->second;
            
            sampler.texture = texIt->second->GetGraphicsName();
        }
        else if (tagName == "wrap_s")
        {
            string value = el->GetText();
            piAssert(!value.empty(), ;);
            
            int v = ToWrap(value);
            piAssert(v != eTexValue_Unknown, ;);
            
            sampler.config[eTexConfig_WrapS] = v;
        }
        else if (tagName == "wrap_t")
        {
            string value = el->GetText();
            piAssert(!value.empty(), ;);
            
            int v = ToWrap(value);
            piAssert(v != eTexValue_Unknown, ;);
            
            sampler.config[eTexConfig_WrapT] = v;
        }
        else if (tagName == "wrap_p")
        {
            string value = el->GetText();
            piAssert(!value.empty(), ;);
            
            int v = ToWrap(value);
            piAssert(v != eTexValue_Unknown, ;);
            
            sampler.config[eTexConfig_WrapR] = v;
        }
        else if (tagName == "minfilter")
        {
            string value = el->GetText();
            piAssert(!value.empty(), ;);
            
            int v = ToFilter(value);
            sampler.config[eTexConfig_MinFilter] = v;
        }
        else if (tagName == "magfilter")
        {
            string value = el->GetText();
            piAssert(!value.empty(), ;);
            
            int v = ToFilter(value);
            sampler.config[eTexConfig_MagFilter] = v;
        }
        else if (tagName == "mipfilter")
        {
            // TODO 未处理mipmap
        }
        else
        {
            piAssertDontStop(false);
        }
        
        node = node->GetNextSibling();
    }
    
    piBindTexture(eTexTarget_2D, sampler.texture);
    for (auto pair : sampler.config)
    {
        piTexParam(eTexTarget_2D, pair.first, pair.second);
    }
}

static iTexture2D* ReadAlbedo(iDOMElement* root,
                              iAssetManager* assetMgr,
                              iClassLoader* classLoader,
                              const TexMap& texMap)
{
    SmartPtr<iDOMElement> elDiff = root->GetElementByTagName("diffuse");
    if (!elDiff.IsNull())
    {
        SmartPtr<iDOMElement> elTexture = elDiff->GetElementByTagName("texture");
        if (!elTexture.IsNull())
        {
            string samplerName = elTexture->GetAttr("texture");
            if (!samplerName.empty())
            {
                auto it = texMap.find(samplerName);
                if (it != texMap.end())
                {
                    return it->second.Ptr();
                }
            }
        }
    }
    
    SmartPtr<iTexture2D> tex = piLoadAsset<iTexture2D>(assetMgr,
                                                       classLoader,
                                                       "share://1.1/texture/opacity_1.meta");
    return tex.PtrAndSetNull();
}

static iTexture2D* ReadOpacity(iDOMElement* root,
                               iAssetManager* assetMgr,
                               iClassLoader* classLoader,
                               const TexMap& texMap)
{
    SmartPtr<iDOMElement> elTrans = root->GetElementByTagName("transparent");
    if (!elTrans.IsNull())
    {
        SmartPtr<iDOMElement> elTexture = elTrans->GetElementByTagName("texture");
        if (!elTexture.IsNull())
        {
            string samplerName = elTexture->GetAttr("texture");
            if (!samplerName.empty())
            {
                auto it = texMap.find(samplerName);
                if (it != texMap.end())
                {
                    return it->second.Ptr();
                }
            }
        }
    }
    
    SmartPtr<iTexture2D> tex = piLoadAsset<iTexture2D>(assetMgr,
                                                       classLoader,
                                                       "share://1.1/texture/opacity_0.meta");
    return tex.PtrAndSetNull();
}

static void ReadTechnique(iDOMElement* elTech,
                          iMaterial* material,
                          TexMap& texMap,
                          ParserContext& context)
{
    SmartPtr<iDOMElement> root;
    
    {
        SmartPtr<iDOMElement> elBlinn = elTech->GetElementByTagName("blinn");
        if (!elBlinn.IsNull())
        {
            root = elBlinn;
        }
    }
    
    {
        SmartPtr<iDOMElement> elPhong = elTech->GetElementByTagName("phong");
        if (!elPhong.IsNull())
        {
            root = elPhong;
        }
    }
    
    {
        SmartPtr<iDOMElement> elConstant = elTech->GetElementByTagName("constant");
        if (!elConstant.IsNull())
        {
            root = elConstant;
        }
    }
    
    {
        SmartPtr<iDOMElement> elLambert = elTech->GetElementByTagName("lambert");
        if (!elLambert.IsNull())
        {
            root = elLambert;
        }
    }
    
    piAssert(!root.IsNull(), ;);
    
    SmartPtr<iMaterialPropArray> props = material->GetProps();

    {
        SmartPtr<iTexture2D> albedo = ReadAlbedo(root, context.assetManager, context.classLoader, texMap);
        if (!albedo.IsNull())
        {
            SmartPtr<iMaterialProp> prop = CreateMaterialProp();
            prop->SetName("u_albedo");
            prop->SetValue(albedo.Ptr());
            props->PushBack(prop);
        }
    }
    
    {
        SmartPtr<iTexture2D> opacity = ReadOpacity(root, context.assetManager, context.classLoader, texMap);
        if (!opacity.IsNull())
        {
            SmartPtr<iMaterialProp> prop = CreateMaterialProp();
            prop->SetName("u_opacity");
            prop->SetValue(opacity.Ptr());
            props->PushBack(prop);
        }
    }
}

static iMaterial* ReadEffect(iDOMElement* e,
                             TexMap& texMap,
                             const MaterialAliasMap& matAlias,
                             ParserContext& context)
{
    string id = e->GetAttr("id");
    piAssert(!id.empty(), nullptr);
    
    SmartPtr<iDOMElement> profileCommon = e->GetElementByTagName("profile_COMMON");
    piAssert(!profileCommon.IsNull(), nullptr);
    
    SurfaceMap surfaces;
    auto newParams = profileCommon->GetElementsByTagName("newparam", false);
    for (int32_t i = 0; i < newParams->GetCount(); ++i)
    {
        SmartPtr<iDOMElement> p = newParams->GetItem(i);
        ReadSurface(p, surfaces);
    }
    
    for (int32_t i = 0; i < newParams->GetCount(); ++i)
    {
        SmartPtr<iDOMElement> p = newParams->GetItem(i);
        ReadSampler2D(p, surfaces, texMap);
    }
    
    SmartPtr<iDOMElement> elTech = profileCommon->GetElementByTagName("technique");
    piAssert(!elTech.IsNull(), nullptr);
    
    string name = id;
    const auto it = matAlias.find(id);
    if (it != matAlias.end())
    {
        name = it->second;
    }
    
    SmartPtr<iMaterial> material = CreateMaterial();
    material->SetName(name);
    ReadTechnique(elTech, material, texMap, context);
    
    return material.PtrAndSetNull();
}

static void ReadMaterialAlias(iDOMElement* m, MaterialAliasMap& aliasMap)
{
    string name = m->GetAttr("id");
    piAssert(!name.empty(), ;);
    
    SmartPtr<iDOMElement> elEffect = m->GetElementByTagName("instance_effect");
    piAssert(!elEffect.IsNull(), ;);
    
    string url = elEffect->GetAttr("url");
    piAssert(!url.empty(), ;);
    
    string alias = url.substr(1, url.size() - 1);
    piAssert(!alias.empty(), ;);
    
    aliasMap[alias] = name;
}

static void ReadMaterialAliasMap(iDOMElement* root, MaterialAliasMap& aliasMap)
{
    SmartPtr<iDOMElement> libMaterials = root->GetElementByTagName("library_materials");
    piCheck(!libMaterials.IsNull(), ;);
    
    auto materials = libMaterials->GetElementsByTagName("material", false);
    for (int32_t i = 0; i < materials->GetCount(); ++i)
    {
        SmartPtr<iDOMElement> m = materials->GetItem(i);
        ReadMaterialAlias(m, aliasMap);
    }
}

void ReadMaterials(iDOMElement* root, ParserContext& context)
{
    TexMap texMap;
    ReadTextureMap(root, texMap, context);
    
    MaterialAliasMap materialAliasMap;
    ReadMaterialAliasMap(root, materialAliasMap);
    
    SmartPtr<iDOMElement> libEffects = root->GetElementByTagName("library_effects");
    piCheck(!libEffects.IsNull(), ;);
    
    SmartPtr<iModelScene> modelScene = context.model;
    SmartPtr<iMaterialArray> mats= modelScene->GetMaterials();
    
    auto effects = libEffects->GetElementsByTagName("effect", false);
    for (int32_t i = 0; i < effects->GetCount(); ++i)
    {
        SmartPtr<iDOMElement> e = effects->GetItem(i);
        SmartPtr<iMaterial> material = ReadEffect(e, texMap, materialAliasMap, context);
        if (!material.IsNull())
        {
            mats->PushBack(material);
        }
    }
}



NSPI_END()






























