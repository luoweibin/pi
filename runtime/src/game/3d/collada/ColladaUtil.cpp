/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.11.21   0.1     Create
 ******************************************************************************/
#include "ColladaUtil.h"

NSPI_BEGIN()

void ParseFloatArray(const string& data, vector<float>& values)
{
    const char* p = data.c_str();
    const char* end = p + data.size();
    
    char buffer[256];
    int len = 0;
    while (p < end)
    {
        if (iswspace(*p))
        {
            if (len > 0)
            {
                // 词结束
                
                piAssert(len < sizeof(buffer)-1, ;);
                
                buffer[len] = '\0';
                float v = strtof(buffer, nullptr);
                values.push_back(v);
                
                len = 0;
            }
            
            ++p;
        }
        else
        {
            buffer[len] = *p;
            ++len;
            ++p;
        }
    }
    
    if (len > 0)
    {
        piAssert(len < sizeof(buffer)-1, ;);
        
        buffer[len] = '\0';
        float v = strtof(buffer, nullptr);
        values.push_back(v);
    }
}

void ParseIntArray(const string& data, vector<int32_t>& values)
{
    const char* p = data.c_str();
    const char* end = p + data.size();
    
    char buffer[256];
    int len = 0;
    while (p < end)
    {
        if (iswspace(*p))
        {
            if (len > 0)
            {
                // 词结束
                
                piAssert(len < sizeof(buffer)-1, ;);
                
                buffer[len] = '\0';
                int32_t v = (int32_t)strtol(buffer, nullptr, 10);
                values.push_back(v);
                
                len = 0;
            }
            
            ++p;
        }
        else
        {
            buffer[len] = *p;
            ++len;
            ++p;
        }
    }
    
    if (len > 0)
    {
        piAssert(len < sizeof(buffer)-1, ;);
        
        buffer[len] = '\0';
        int32_t v = (int32_t)strtol(buffer, nullptr, 10);
        values.push_back(v);
    }
}

void ParseStringArray(const string& data, vector<string>& values)
{
    const char* p = data.c_str();
    const char* end = p + data.size();
    
    char buffer[256];
    int len = 0;
    while (p < end)
    {
        if (iswspace(*p))
        {
            if (len > 0)
            {
                // 词结束
                
                piAssert(len < sizeof(buffer)-1, ;);
                
                buffer[len] = '\0';
                values.push_back(buffer);
                
                len = 0;
            }
            
            ++p;
        }
        else
        {
            buffer[len] = *p;
            ++len;
            ++p;
        }
    }
    
    if (len > 0)
    {
        piAssert(len < sizeof(buffer)-1, ;);
        
        buffer[len] = '\0';
        values.push_back(buffer);
    }
}

int ToWrap(const string& value)
{
    if (value == "WRAP")
    {
        return eTexValue_Repeat;
    }
    else if (value == "CLAMP")
    {
        return eTexValue_Repeat;
    }
    else if (value == "BORDER")
    {
        return eTexValue_Border;
    }
    else if (value == "MIRROR")
    {
        return eTexValue_Mirror;
    }
    else
    {
        return eTexValue_Unknown;
    }
}

int ToFilter(const string& value)
{
    if (value == "LINEAR")
    {
        return eTexValue_Linear;
    }
    else if (value == "NEAREST")
    {
        return eTexValue_Nearest;
    }
    else if (value == "ANISOTROPIC")
    {
        piAssert(false, eTexValue_Unknown);
        return eTexValue_Unknown;
    }
    else
    {
        return eTexValue_Unknown;
    }
}

vec4 ReadColor(iDOMElement* el)
{
    SmartPtr<iDOMElement> elColor = el->GetElementByTagName("color");
    piCheck(!elColor.IsNull(), vec4());
    
    vector<float> color;
    string data = elColor->GetText();
    ParseFloatArray(data, color);
    piCheck(color.size() == 4, vec4());
    
    return vec4(color[0], color[1], color[2], color[3]);
}

float ReadFloat(iDOMElement* el)
{
    SmartPtr<iDOMElement> elFloat = el->GetElementByTagName("float");
    piAssert(!elFloat.IsNull(), 0);
    
    string data = elFloat->GetText();
    piAssert(!data.empty(), 0);
    
    return strtof(data.c_str(), nullptr);
}

vec3 ReadRotate(const string& txt)
{
    vector<float> values;
    ParseFloatArray(txt, values);
    
    vec3 rotate;
    if (piEqual(values[0], 1))
    {
        rotate.x = values[3];
    }
    else if (piEqual(values[1], 1))
    {
        rotate.y = values[3];
    }
    else if (piEqual(values[2], 1))
    {
        rotate.z = values[3];
    }
    else
    {
        piAssertDontStop(false);
    }
    
    return rotate;
}

mat4 ReadMatrix(const string& data)
{
    vector<float> values;
    ParseFloatArray(data, values);
    piAssert(values.size() == 16, mat4());
    
    return mat4(values[0], values[4], values[8],  values[12],
                values[1], values[5], values[9],  values[13],
                values[2], values[6], values[10], values[14],
                values[3], values[7], values[11], values[15]);
}

void ParseMatrixArray(const string& data, vector<mat4>& values)
{
    vector<float> tmp;
    ParseFloatArray(data, tmp);
    piAssert(tmp.size() % 16 == 0, ;);
    
    for (int32_t j = 0; j < tmp.size(); j += 16)
    {
        mat4 mat(tmp[j + 0], tmp[j + 4], tmp[j + 8],  tmp[j + 12],
                 tmp[j + 1], tmp[j + 5], tmp[j + 9],  tmp[j + 13],
                 tmp[j + 2], tmp[j + 6], tmp[j + 10], tmp[j + 14],
                 tmp[j + 3], tmp[j + 7], tmp[j + 11], tmp[j + 15]);
        values.push_back(mat);
    }
}

iDOMElement* FindElement(iDOMElementArray* elements, const string& id)
{
    for (int32_t i = 0; i < elements->GetCount(); ++i)
    {
        SmartPtr<iDOMElement> el = elements->GetItem(i);
        if (el->GetAttr("id") == id)
        {
            return el;
        }
    }
    
    return nullptr;
}


iTransform* ReadTransform(iDOMElement *node)
{
    SmartPtr<iTransform> transform = CreateTransform();
    
    // Read Matrix
    {
        SmartPtr<iDOMElement> elMatrix = node->GetElementByTagName("matrix");
        if (!elMatrix.IsNull())
        {
            string txtMatrix = elMatrix->GetText();
            mat4 matrix = ReadMatrix(txtMatrix);
            transform->SetMatrix(matrix);
            return transform.PtrAndSetNull();
        }
    }
    
    // Read scale
    {
        SmartPtr<iDOMElement> elScale = node->GetElementByTagName("scale");
        if (!elScale.IsNull())
        {
            string txtScale = elScale->GetText();
            vector<float> values;
            ParseFloatArray(txtScale, values);
            transform->SetScale(vec3(values[0], values[1], values[2]));
        }
    }
    
    // Read rotate
    {
        SmartPtr<iDOMElementArray> elRotates = node->GetElementsByTagName("rotate", false);
        for (int32_t i = 0; i < elRotates->GetCount(); ++i)
        {
            SmartPtr<iDOMElement> el = elRotates->GetItem(i);
            string txt = el->GetText();
            vec3 orient = ReadRotate(txt);
            transform->SetOrientation(orient);
        }
    }
    
    // Read translate
    {
        SmartPtr<iDOMElement> elTranslate = node->GetElementByTagName("translate");
        if (!elTranslate.IsNull())
        {
            string txtTranslate = elTranslate->GetText();
            vector<float> values;
            ParseFloatArray(txtTranslate, values);
            transform->SetTranslation(vec3(values[0], values[1], values[2]));
        }
    }
    
    return transform.PtrAndSetNull();
}

string GenerateMatName()
{
    static int generateIndex = 0;
    char szTemp[260] = "";
    sprintf(szTemp, "unamed_%d", generateIndex);
    
    return szTemp;
}

string ReadMaterialName(iDOMElement* el)
{
    SmartPtr<iDOMElement> elBindMat = el->GetElementByTagName("bind_material");
    piCheck(!elBindMat.IsNull(), "");
    
    SmartPtr<iDOMElement> elTech = elBindMat->GetElementByTagName("technique_common");
    piCheck(!elTech.IsNull(), "");
    
    SmartPtr<iDOMElement> elInstMat = elTech->GetElementByTagName("instance_material");
    piCheck(!elInstMat.IsNull(), "");
    
    string target = elInstMat->GetAttr("target");
    if (!target.empty())
    {
        target = target.substr(1);
    }
    
    return target;
}


iModelNode* FindSceneNode(iModelNode* node, const string& target)
{
    string name = node->GetName();
    if (name == target)
    {
        return node;
    }
    
    SmartPtr<iModelNodeArray> children = node->GetChildren();
    for (auto i = 0; i < children->GetCount(); ++i)
    {
        SmartPtr<iModelNode> child = children->GetItem(i);
        SmartPtr<iModelNode> ret = FindSceneNode(child, target);
        if (!ret.IsNull())
        {
            return ret.PtrAndSetNull();
        }
    }

    return nullptr;
}


NSPI_END()


















