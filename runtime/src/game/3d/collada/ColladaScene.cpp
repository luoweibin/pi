/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.5   0.1     Create
 ******************************************************************************/
#include "ColladaUtil.h"

NSPI_BEGIN()

static int ParseNodeType(const string& type)
{
    if (type == "JOINT")
    {
        return eModelNode_Joint;
    }
    else
    {
        return eModelNode_Null;
    }
}


static string GetUrl(iDOMElement* el)
{
    string url = el->GetAttr("url");
    if (!url.empty())
    {
        url = url.substr(1);
    }
    return url;
}

static iModelNode* ParseSceneNode(iDOMElement* el,
                                  iModelNode* parent,
                                  const ParserContext& context)
{
    SmartPtr<iModelNode> modelNode = CreateModelNode();
    
    string name = el->GetAttr("id");
    modelNode->SetName(name);
    
    string typeName = el->GetAttr("type");
    int type = ParseNodeType(typeName);
    modelNode->SetType(type);
    
    if (parent != nullptr)
    {
        modelNode->SetParent(parent);
    }
    
    SmartPtr<iTransform> transform = ReadTransform(el);
    modelNode->SetTransform(transform);
    
    SmartPtr<iModelScene> modelScene = context.model;
    
    {
        SmartPtr<iDOMElementArray> elCtrls = el->GetElementsByTagName("instance_controller", false);
        piEach<iDOMElementArray, iDOMElement*>(elCtrls,
                                               [&modelNode, context, modelScene](iDOMElement* elCtrl) {
                                                   string skinName = GetUrl(elCtrl);
                                                   piCheck(!skinName.empty(), ;);
                                                   
                                                   SmartPtr<iModelSkin> skin = modelScene->FindSkin(skinName);
                                                   piCheck(!skin.IsNull(), ;);
                                                   
                                                   SmartPtr<iModelMesh> mesh = skin->GetMesh();
                                                   piCheck(!mesh.IsNull(), ;);
                                                   
                                                   string matName = ReadMaterialName(elCtrl);
                                                   SmartPtr<iMaterial> mat = modelScene->FindMaterial(matName);
                                                   mat->SetShaderUri("share://1.1/shader/skel/skel.meta");
                                                   mesh->SetMaterial(mat);
                                                   
                                                   modelNode->GetSkins()->PushBack(skin);
                                               });
    }
    
    {
        SmartPtr<iDOMElementArray> elGeos = el->GetElementsByTagName("instance_geometry", false);
        piEach<iDOMElementArray, iDOMElement*>(elGeos,
                                               [&modelNode, context, modelScene](iDOMElement* elGeo) {
                                                   string meshName = GetUrl(elGeo);
                                                   piCheck(!meshName.empty(), ;);
                                                   
                                                   SmartPtr<iModelMesh> mesh = modelScene->FindMesh(meshName);
                                                   piCheck(!mesh.IsNull(), ;);
                                                   
                                                   mesh->SetName(meshName);
                                                   
                                                   string matName = ReadMaterialName(elGeo);
                                                   
                                                   if (matName.empty())
                                                   {
                                                       matName = GenerateMatName();
                                                   }
                                                   
                                                   SmartPtr<iMaterial> mat = modelScene->FindMaterial(matName);
                                                   
                                                   if (mat.IsNull())
                                                   {
                                                       mat = CreateDefaultMaterial();
                                                   }
                                                   else if (mat->GetShaderUri().empty())
                                                   {
                                                       mat->SetShaderUri("share://1.1/shader/mesh/mesh.meta");
                                                   }
                                                   mesh->SetMaterial(mat);
                                                   
                                                   modelNode->GetMeshes()->PushBack(mesh);
                                               });
    }
    
    SmartPtr<iModelNodeArray> children = modelNode->GetChildren();
    
    SmartPtr<iDOMElementArray> elChildren = el->GetElementsByTagName("node", false);
    for (auto i = 0; i < elChildren->GetCount(); ++i)
    {
        SmartPtr<iDOMElement> elChild = elChildren->GetItem(i);
        
        SmartPtr<iModelNode> childNode = ParseSceneNode(elChild, modelNode, context);
        if (!childNode.IsNull())
        {
            children->PushBack(childNode);
        }
    }
    
    return modelNode.PtrAndSetNull();
}

void ParseScene(iDOMElement* root, const ParserContext& context)
{
    SmartPtr<iDOMElement> scenes = root->GetElementByTagName("library_visual_scenes");
    piAssert(!scenes.IsNull(), ;);
    
    SmartPtr<iDOMElement> elScene = scenes->GetElementByTagName("visual_scene");
    piAssert(!elScene.IsNull(), ;);
    
    SmartPtr<iModelNode> rootNode = ParseSceneNode(elScene, nullptr, context);
    if (!rootNode.IsNull())
    {
        context.model->SetRootNode(rootNode);
    }
}


NSPI_END()































