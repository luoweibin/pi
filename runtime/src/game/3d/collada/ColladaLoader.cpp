/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.11.17   1     Create
 ******************************************************************************/
#include <pi/Game.h>
#include <pi/DOM.h>
#include "ColladaUtil.h"

using namespace std;


NSPI_BEGIN()


class ColladaLoader : public iAssetLoader
{
public:
    ColladaLoader()
    {
    }
    
    virtual ~ColladaLoader()
    {
    }
    
    virtual iAsset* Load(iAssetManager* manager, iClassLoader* classLoader, iStream* stream)
    {
        int64_t begin = piGetSystemTimeMS();
        
        SmartPtr<iDOMDocument> doc = piParseXML(stream);
        piAssert(!doc.IsNull(), nullptr);
        
        SmartPtr<iDOMElement> root = doc->GetDocumentElement();
        piAssert(!root.IsNull(), nullptr);
        
        ParserContext context;
        context.assetManager = manager;
        context.classLoader = classLoader;
        context.model = CreateModelScene();
        
        string dir = piGetDirectory(stream->GetUri());
        context.dir = dir;
        
        ReadMaterials(root, context);
        PreparseGeometrys(root, context);
        PreparseSkins(root, context);
        ParseScene(root, context);
        ReadSkins(root, context);
        ReadGeometrys(root, context);
        ReadModelAnim(root, context);
       
        SmartPtr<iModelScene> model = context.model;
        context.model = nullptr;
        
        piCheck(!model.IsNull(), nullptr);
        
        model->SetAnimation(context.animation);
        
        int64_t timecost = piGetSystemTimeMS() - begin;
        PILOGI(PI_GAME_TAG, "Timecost for loading collada model:%lldMS, uri:%s", timecost, stream->GetUri().c_str());
        
        SmartPtr<iMaterialArray> mats = model->GetMaterials();
        piEach<iMaterialArray, iMaterial*>(mats,
                                           [manager, classLoader](iMaterial* m) {
                                               string shaderUri = m->GetShaderUri();
                                               SmartPtr<iShaderProgram> shader = piLoadAsset<iShaderProgram>(manager, classLoader, shaderUri);
                                               m->SetShader(shader);
                                           });
    
        return model.PtrAndSetNull();
    }
};


class ColladaLoaderFactory : public iAssetLoaderFactory
{
private:
    SmartPtr<iAssetLoader> mLoader;
    
public:
    ColladaLoaderFactory()
    {
        mLoader = new ColladaLoader();
    }
    
    virtual ~ColladaLoaderFactory()
    {
    }
    
    virtual iAssetLoader* CreateLoader(const std::string& fileExt, iStream* stream)
    {
        return mLoader;
    }
};

iAssetLoaderFactory* CreateColladaLoaderFactory()
{
    return new ColladaLoaderFactory();
}

NSPI_END()






















