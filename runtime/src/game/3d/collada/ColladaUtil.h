/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.11.21   0.1     Create
 ******************************************************************************/
#ifndef PI_SRC_GAME_COLLADA_COLLADAUTIL_H
#define PI_SRC_GAME_COLLADA_COLLADAUTIL_H

#include <pi/Game.h>
#include <pi/DOM.h>

using namespace std;

NSPI_BEGIN()

#define eVertexJointCount 4

void ParseFloatArray(const string& data, vector<float>& values);
void ParseIntArray(const string& data, vector<int32_t>& values);
void ParseStringArray(const string& data, vector<string>& values);
int ToWrap(const string& value);
int ToFilter(const string& value);
int ToOpaque(const string& data);
vec4 ReadColor(iDOMElement* el);
float ReadFloat(iDOMElement* el);
vec3 ReadRotate(const string& txt);
mat4 ReadMatrix(const string& data);
void ParseMatrixArray(const string& data, vector<mat4>& values);
iTransform* ReadTransform(iDOMElement *node);
iModelNode* FindSceneNode(iModelNode* node, const string& target);

typedef map<string, SmartPtr<iModelScene>> ModelMap;
typedef map<string, mat4> MatMap;

struct ColladaWeight
{
    float bias;
    int32_t jointIndex;
};

typedef vector<ColladaWeight> ColladaWeightArray;

struct ColladaSkin
{
    SmartPtr<iModelSkin> modelSkin;
    string geoName;
    vector<ColladaWeightArray> weights;
    vector<string> joints;
    mat4 BSM;
    
    ColladaSkin()
    {
    }
    
    ColladaSkin(const ColladaSkin& skin):
    weights(skin.weights), geoName(skin.geoName), joints(skin.joints), BSM(skin.BSM), modelSkin(skin.modelSkin)
    {
    }
};

struct ColladaSkeletonComponent
{
    string name;
    string geometry;
    mat4 bindShapeMatrix;
    
    ColladaSkeletonComponent()
    {
    }
    
    ColladaSkeletonComponent(const ColladaSkeletonComponent& skelComp):
    geometry(skelComp.geometry), bindShapeMatrix(skelComp.bindShapeMatrix), name(skelComp.name)
    {
    }
};

typedef vector<ColladaSkeletonComponent> ColladaSkelCompArray;
typedef vector<ColladaSkin> ColladaSkinArray;

struct ColladaSkeleton
{
    SmartPtr<iModelSkin> lib;
    ColladaSkelCompArray comps;
    ColladaSkinArray skins;
    
    ColladaSkeleton()
    {
    }
    
    ColladaSkeleton(const ColladaSkeleton& s):
    lib(s.lib), comps(s.comps), skins(s.skins)
    {
    }
};

string GenerateMatName();

string ReadMaterialName(iDOMElement* el);


iDOMElement* FindElement(iDOMElementArray* elements, const string& id);


struct ParserContext
{
    SmartPtr<iModelScene> model;
    vector<ColladaSkin> skins;
    SmartPtr<iModelAnim> animation;
    iAssetManager* assetManager;
    iClassLoader* classLoader;
    string dir;
};

void PreparseGeometrys(iDOMElement* root, ParserContext& context);
void ReadGeometrys(iDOMElement* root, ParserContext& context);
void ReadMaterials(iDOMElement* root, ParserContext& contexts);
void PreparseSkins(iDOMElement* root, ParserContext& context);
void ReadSkins(iDOMElement* root, ParserContext& context);
void ReadModelAnim(iDOMElement* root, ParserContext& context);
void ParseScene(iDOMElement* root, const ParserContext& context);

NSPI_END()

#endif





















