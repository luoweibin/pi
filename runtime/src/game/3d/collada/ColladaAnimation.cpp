/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.5   0.1     Create
 ******************************************************************************/
#include "ColladaUtil.h"

NSPI_BEGIN()


static iAnimChannel* ReadChannel(iDOMElement* elChannel, iDOMElementArray* elSamplers, iDOMElementArray* elSources)
{
    string samplerName = elChannel->GetAttr("source");
    piCheck(!samplerName.empty(), nullptr);
    samplerName = samplerName.substr(1, samplerName.size() - 1);
    
    SmartPtr<iDOMElement> elSampler = FindElement(elSamplers, samplerName);
    piCheck(!elSampler.IsNull(), nullptr);
    
    SmartPtr<iDOMElementArray> elInputs = elSampler->GetElementsByTagName("input", false);
    piCheck(!elInputs.IsNull(), nullptr);
    
    vector<float> frameTimes;
    vector<mat4> frameMatrices;
    
    for (int32_t i = 0; i < elInputs->GetCount(); ++i)
    {
        SmartPtr<iDOMElement> elInput = elInputs->GetItem(i);
        
        string source = elInput->GetAttr("source");
        if (source.empty())
        {
            continue;
        }
        source = source.substr(1, source.size() - 1);
        
        SmartPtr<iDOMElement> elSource = FindElement(elSources, source);
        if (elSource.IsNull())
        {
            continue;
        }
        
        string semantic = elInput->GetAttr("semantic");
        if (semantic == "INPUT")
        {
            SmartPtr<iDOMElement> floatArray = elSource->GetElementByTagName("float_array");
            if (floatArray.IsNull())
            {
                continue;
            }
            
            string txtValues = floatArray->GetText();
            ParseFloatArray(txtValues, frameTimes);
        }
        else if (semantic == "OUTPUT")
        {
            SmartPtr<iDOMElement> floatArray = elSource->GetElementByTagName("float_array");
            if (floatArray.IsNull())
            {
                continue;
            }
            
            string txtValues = floatArray->GetText();
            ParseMatrixArray(txtValues, frameMatrices);
        }
    }
    
    piCheck(!frameTimes.empty(), nullptr);
    piCheck(!frameMatrices.empty(), nullptr);
    piCheck(frameTimes.size() == frameMatrices.size(), nullptr);
    
    SmartPtr<iAnimChannel> channel = CreateAnimChannel();
    SmartPtr<iAnimCurveKeyArray> keys = channel->GetKeys();
    
    for (int32_t i = 0; i < frameTimes.size(); ++i)
    {
        SmartPtr<iAnimCurveKey> key = CreateAnimCurveKey();
        
        key->SetTime(frameTimes[i] * 1000);
        key->SetValue(frameMatrices[i]);
        
        keys->PushBack(key);
    }
    
    return channel.PtrAndSetNull();
}

static void DoReadAnimations(iModelAnim* lib, iDOMElement* root, ParserContext& context);

static iAnimLayer* ReadAnimLayer(iDOMElement* elAnim, ParserContext& context)
{
    SmartPtr<iDOMElementArray> elSources = elAnim->GetElementsByTagName("source", false);
    piCheck(!elSources.IsNull(), nullptr);
    
    SmartPtr<iDOMElementArray> elSamplers = elAnim->GetElementsByTagName("sampler", false);
    piCheck(!elSamplers.IsNull(), nullptr);
    
    SmartPtr<iDOMElementArray> elChannels = elAnim->GetElementsByTagName("channel", false);
    piCheck(!elChannels.IsNull(), nullptr);
    
    SmartPtr<iAnimLayer> layer = CreateAnimLayer();
    SmartPtr<iAnimChannelArray> channels = layer->GetChannels();
    
    for (int32_t i = 0; i < elChannels->GetCount(); ++i)
    {
        SmartPtr<iDOMElement> elChannel = elChannels->GetItem(i);
        
        string target = elChannel->GetAttr("target");
        vector<string> frags = piStrSplit(target.c_str(), target.size(), '/');
        if (frags.size() != 2)
        {
            continue;
        }
        
        if (frags[1] != "matrix")
        {
            continue;
        }
        
        SmartPtr<iAnimChannel> channel = ReadChannel(elChannel, elSamplers, elSources);
        if (channel.IsNull())
        {
            continue;
        }
        
        string jointName = frags[0];
        SmartPtr<iModelNode> jointNode = FindSceneNode(context.model->GetRootNode(), jointName);
        if (jointNode.IsNull())
        {
            continue;
        }
        
        SmartPtr<iTransform> transform = jointNode->GetTransform();
        if (transform.IsNull())
        {
            continue;
        }
        
        SmartPtr<iClass> klass = transform->GetClass();
        SmartPtr<iProperty> prop = klass->GetProperty("Matrix");
        if (prop.IsNull())
        {
            continue;
        }
        
        channel->SetTarget(transform);
        channel->SetProperty(prop);
        
        channels->PushBack(channel);
    }
    
    return layer.PtrAndSetNull();
}

static void DoReadAnimations(iModelAnim* lib, iDOMElement* root, ParserContext& context)
{
    SmartPtr<iDOMElementArray> elAnims = root->GetElementsByTagName("animation", false);
    piCheck(!elAnims.IsNull(), ;);
    
    SmartPtr<iAnimLayerArray> layers = lib->GetLayers();
    
    for (int32_t i = 0; i < elAnims->GetCount(); ++i)
    {
        SmartPtr<iDOMElement> elAnim = elAnims->GetItem(i);
        
        string name = elAnim->GetAttr("id");
        
        SmartPtr<iDOMElementArray> elLayers = elAnim->GetElementsByTagName("animation", false);
        for (auto l = 0; l < elLayers->GetCount(); ++l)
        {
            SmartPtr<iDOMElement> elLayer = elLayers->GetItem(l);
            
            SmartPtr<iAnimLayer> layer = ReadAnimLayer(elLayer, context);
            if (!layer.IsNull())
            {
                layer->SetName(name);
                layers->PushBack(layer);
            }
        }
    }
}

void ReadModelAnim(iDOMElement* root, ParserContext& context)
{
    SmartPtr<iDOMElement> libAnims = root->GetElementByTagName("library_animations");
    piCheck(!libAnims.IsNull(), ;);
    
    SmartPtr<iModelAnim> modelAnim = CreateModelAnim();
    
    DoReadAnimations(modelAnim, libAnims, context);
    if (!modelAnim.IsNull())
    {
        context.animation = modelAnim;
    }
}


NSPI_END()
































