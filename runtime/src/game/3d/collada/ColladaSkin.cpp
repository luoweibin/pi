/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.11.25   0.1     Create
 ******************************************************************************/
#include "ColladaUtil.h"
#include <regex>

NSPI_BEGIN()


static void ParseJointsName(iDOMElementArray* sources, const string& id, vector<string>& values)
{
    SmartPtr<iDOMElement> elSource = FindElement(sources, id);
    piCheck(!elSource.IsNull(), ;);
    
    SmartPtr<iDOMElement> elNameArray = elSource->GetElementByTagName("Name_array");
    piCheck(!elNameArray.IsNull(), ;);
    
    string txtValues = elNameArray->GetText();
    ParseStringArray(txtValues, values);
}

static iDOMElement* FindInput(iDOMElementArray* elInputs, const string& semantic)
{
    for (int32_t i = 0; i < elInputs->GetCount(); ++i)
    {
        SmartPtr<iDOMElement> input = elInputs->GetItem(i);
        if (input->GetAttr("semantic") == semantic)
        {
            return input;
        }
    }
    
    return nullptr;
}

static void ReadIBMs(iDOMElementArray* elInputs, iDOMElementArray* sources, iMat4Array* IBMs)
{
    SmartPtr<iDOMElement> elInput = FindInput(elInputs, "INV_BIND_MATRIX");
    piCheck(!elInput.IsNull(), ;);
    
    string source = elInput->GetAttr("source");
    piCheck(!source.empty(), ;);
    source = source.substr(1, source.size() - 1);
    
    SmartPtr<iDOMElement> elSource = FindElement(sources, source);
    piCheck(!elSource.IsNull(), ;);
    
    SmartPtr<iDOMElement> elFloatArray = elSource->GetElementByTagName("float_array");
    piCheck(!elFloatArray.IsNull(), ;);
    
    string txtValues = elFloatArray->GetText();
    
    vector<mat4> matrixs;
    ParseMatrixArray(txtValues, matrixs);
    
    for (auto m : matrixs)
    {
        IBMs->PushBack(m);
    }
}

static bool ReadJoints(iModelSkin* skin,
                       iDOMElement* elJoints,
                       iDOMElementArray* sources,
                       vector<string>& jointNames)
{
    SmartPtr<iDOMElementArray> elInputs = elJoints->GetElementsByTagName("input", false);
    
    ReadIBMs(elInputs, sources, skin->GetIBMs());
    
    SmartPtr<iDOMElement> elInput = FindInput(elInputs, "JOINT");
    piCheck(!elInput.IsNull(), false);
    
    string source = elInput->GetAttr("source");
    piCheck(!source.empty(), false);
    source = source.substr(1, source.size() - 1);
    
    ParseJointsName(sources, source, jointNames);
    
    return true;
}

static void ReadWeights(ColladaSkin& skin,
                        iDOMElement* elVertexWeights,
                        iDOMElementArray* sources)
{
    string txtVertCount = elVertexWeights->GetAttr("count");
    int32_t vertCount = atoi(txtVertCount.c_str());
    piCheck(vertCount> 0, ;);
    
    SmartPtr<iDOMElementArray> elInputs = elVertexWeights->GetElementsByTagName("input", false);
    piCheck(!elInputs.IsNull(), ;);
    
    vector<int32_t> vCount;
    {
        SmartPtr<iDOMElement> elVCount = elVertexWeights->GetElementByTagName("vcount");
        piCheck(!elVCount.IsNull(), ;);
        string txtValues = elVCount->GetText();
        ParseIntArray(txtValues, vCount);
    }
    
    vector<int32_t> v;
    {
        SmartPtr<iDOMElement> elV = elVertexWeights->GetElementByTagName("v");
        piCheck(!elV.IsNull(), ;);
        string txtValues = elV->GetText();
        ParseIntArray(txtValues, v);
    }
    
    vector<float> weightValues;
    
    int32_t inputCount = elInputs->GetCount();
    int32_t jointOffset = 0;
    int32_t weightOffset = 0;
    
    for (int32_t i = 0; i < inputCount; ++i)
    {
        SmartPtr<iDOMElement> elInput = elInputs->GetItem(i);
        
        string txtOffset = elInput->GetAttr("offset");
        int32_t offset = atoi(txtOffset.c_str());
        piAssert(offset >= 0, ;);
        
        string source = elInput->GetAttr("source");
        piCheck(!source.empty(), ;);
        source = source.substr(1, source.size() - 1);
        
        SmartPtr<iDOMElement> elSource = FindElement(sources, source);
        piCheck(!elSource.IsNull(), ;);
        
        string semantic = elInput->GetAttr("semantic");
        if (semantic == "WEIGHT")
        {
            weightOffset = offset;
            
            SmartPtr<iDOMElement> elFloatArray = elSource->GetElementByTagName("float_array");
            piCheck(!elFloatArray.IsNull(), ;);
            
            {
                string txtCount = elFloatArray->GetAttr("count");
                int32_t count = atoi(txtCount.c_str());
                piCheck(count > 0, ;);
                
                string txtValues = elFloatArray->GetText();
                ParseFloatArray(txtValues, weightValues);
                piAssert(count == weightValues.size(), ;);
            }
        }
    }
    
    auto& weights = skin.weights;
    
    int32_t vCurrent = 0;
    
    for (auto j = 0; j < vCount.size() ; ++j)
    {
        auto count = vCount[j];
        
        ColladaWeightArray array;
        
        auto readCount = piMin(eVertexJointCount, count);
        for (auto i = 0; i < readCount; ++i)
        {
            int32_t jointIndex = v[(vCurrent + i) * inputCount + jointOffset];
            ColladaWeight w;
            w.jointIndex = jointIndex;
            
            int32_t weightIndex = v[(vCurrent + i) * inputCount + weightOffset];
            w.bias = weightValues[weightIndex];
            
            array.push_back(w);
        }
        
        vCurrent += count;
        
        weights.push_back(array);
    }
}



static bool ReadSkin(ColladaSkin& skin, iDOMElement* elSkin, ParserContext& context)
{
    string geoName = elSkin->GetAttr("source");
    piCheck(!geoName.empty(), false);
    
    geoName = geoName.substr(1, geoName.size() - 1);
    
    SmartPtr<iModelSkin> modelSkin = skin.modelSkin;
    
    skin.geoName = geoName;
    
    mat4 BSM;
    SmartPtr<iDOMElement> elMatrix = elSkin->GetElementByTagName("bind_shape_matrix");
    if (!elMatrix.IsNull())
    {
        string txtMatrix = elMatrix->GetText();
        skin.BSM = ReadMatrix(txtMatrix);
    }
    
    SmartPtr<iDOMElementArray> elSources = elSkin->GetElementsByTagName("source", false);
    piCheck(!elSources.IsNull(), false);
    
    SmartPtr<iDOMElement> elJoints = elSkin->GetElementByTagName("joints");
    if (!elJoints.IsNull())
    {
        if (ReadJoints(modelSkin, elJoints, elSources, skin.joints))
        {
            SmartPtr<iModelNode> rootNode = context.model->GetRootNode();
            SmartPtr<iModelNodeArray> joints = modelSkin->GetJoints();
            for (const auto& name : skin.joints)
            {
                SmartPtr<iModelNode> joint = FindSceneNode(rootNode, name);
                if (!joint.IsNull())
                {
                    joints->PushBack(joint);
                }
            }
        }
    }
    
    SmartPtr<iDOMElement> elVertexWeights = elSkin->GetElementByTagName("vertex_weights");
    if (!elVertexWeights.IsNull())
    {
        ReadWeights(skin, elVertexWeights, elSources);
    }
        
    return true;
}


void ReadSkins(iDOMElement* root, ParserContext& context)
{
    SmartPtr<iDOMElement> scenes = root->GetElementByTagName("library_visual_scenes");
    piAssert(!scenes.IsNull(), ;);
    
    SmartPtr<iDOMElement> scene = scenes->GetElementByTagName("visual_scene");
    piAssert(!scene.IsNull(), ;);
    
    SmartPtr<iDOMElement> libControllers = root->GetElementByTagName("library_controllers");
    piCheck(!libControllers.IsNull(), ;);
    
    SmartPtr<iDOMElementArray> controllers = libControllers->GetElementsByTagName("controller", false);
    piCheck(!controllers.IsNull(), ;);
    
    SmartPtr<iModelScene> modelScene = context.model;
    
    for (int i = 0; i < controllers->GetCount(); ++i)
    {
        SmartPtr<iDOMElement> c = controllers->GetItem(i);
        
        SmartPtr<iDOMElement> elSkin = c->GetElementByTagName("skin");
        if (elSkin.IsNull())
        {
            continue;
        }
        
        string skinName = c->GetAttr("id");
        if (skinName.empty())
        {
            continue;
        }
        
        SmartPtr<iModelSkin> modelSkin = modelScene->FindSkin(skinName);
        if (modelSkin.IsNull())
        {
            continue;
        }
        
        ColladaSkin skin;
        skin.modelSkin = modelSkin;
        if (ReadSkin(skin, elSkin, context))
        {
            skin.modelSkin->SetName(skinName);
            context.skins.push_back(skin);
        }
    }
}


void PreparseSkins(iDOMElement* root, ParserContext& context)
{
    SmartPtr<iDOMElement> scenes = root->GetElementByTagName("library_visual_scenes");
    piAssert(!scenes.IsNull(), ;);
    
    SmartPtr<iDOMElement> scene = scenes->GetElementByTagName("visual_scene");
    piAssert(!scene.IsNull(), ;);
    
    SmartPtr<iDOMElement> libControllers = root->GetElementByTagName("library_controllers");
    piCheck(!libControllers.IsNull(), ;);
    
    SmartPtr<iDOMElementArray> controllers = libControllers->GetElementsByTagName("controller", false);
    piCheck(!controllers.IsNull(), ;);
    
    SmartPtr<iModelScene> modelScene = context.model;
    
    SmartPtr<iModelSkinArray> skins = modelScene->GetSkins();
    
    for (int i = 0; i < controllers->GetCount(); ++i)
    {
        SmartPtr<iDOMElement> c = controllers->GetItem(i);
        
        SmartPtr<iDOMElement> elSkin = c->GetElementByTagName("skin");
        if (elSkin.IsNull())
        {
            continue;
        }
        
        string skinName = c->GetAttr("id");
        if (skinName.empty())
        {
            continue;
        }
        
        string meshName = elSkin->GetAttr("source");
        if (meshName.size() < 1)
        {
            continue;
        }
        meshName = meshName.substr(1);
        
        SmartPtr<iModelMesh> modelMesh = modelScene->FindMesh(meshName);
        if (modelMesh.IsNull())
        {
            continue;
        }
        
        SmartPtr<iModelSkin> modelSkin = CreateModelSkin();
        modelSkin->SetName(skinName);
        modelSkin->SetMesh(modelMesh);
        
        skins->PushBack(modelSkin);
    }
}


NSPI_END()





















