/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.7   1.0     Create
 ******************************************************************************/
#include <pi/game/impl/ComponentImpl.h>

using namespace std;

NSPI_BEGIN()

class ModelInstance : public ComponentImpl<iModelInstance>
{
private:
    string mUri;
    string mScript;
    SmartPtr<iModelScene> mLib;
    
public:
    ModelInstance()
    {
    }
    
    virtual ~ModelInstance()
    {
    }
    
    virtual string GetUri() const
    {
        return mUri;
    }
    
    virtual void SetUri(const string& uri)
    {
        mUri = uri;
    }
    
    virtual void SetModelScene(iModelScene* lib)
    {
        piAssert(lib != nullptr, ;);
        mLib = lib;
    }
    
    virtual iModelScene* GetModelScene() const
    {
        return mLib;
    }
    
    virtual void OnUpdate(float delta)
    {
        piCheck(!mLib.IsNull(), ;);
        
        mLib->OnUpdate(delta);
    }
    
    virtual std::string GetScript() const
    {
        return mScript;
    }
    
    virtual void SetScript(const std::string& uri)
    {
        mScript = uri;
    }
};

iModelInstance* CreateModelInstance()
{
    return new ModelInstance();
}


NSPI_END()













