/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.5   1.0     Create
 ******************************************************************************/
#include <pi/game/impl/SystemImpl.h>

#include <set>

using namespace std;

NSPI_BEGIN()

float g_Near = 1;
float g_Far = 1000000;

struct RenderNode
{
    //SmartPtr<iShaderProgram> program;
    int32_t vao;
    SmartPtr<iMaterial> material;
    SmartPtr<iMesh> mesh;
    SmartPtr<iMat4Array> JMs;
    mat4*                AnchorMatrix;
    SmartPtr<iModelSkin> skin;
    SmartPtr<iModelNode> node;
    SmartPtr<iEntity>    parentEntity;
    
    RenderNode()
    : vao(0)
    , AnchorMatrix(nullptr)
    {
    }
    
    RenderNode(const RenderNode& s)
    : mesh(s.mesh)
    //, program(s.program)
    , material(s.material)
    , JMs(s.JMs)
    , AnchorMatrix(s.AnchorMatrix)
    , skin(s.skin)
    , node(s.node)
    , parentEntity(s.parentEntity)
    {
        SetVAO(s.vao);
    }
    
    ~RenderNode()
    {
        piReleaseGraphicsObject(vao);
    }
    
    void SetVAO(int32_t object)
    {
        piRetainGraphicsObject(object);
        vao = object;
    }
};

typedef vector<RenderNode> RenderNodeVector;

struct RenderState3D
{
    RenderNodeVector nodes;
    
    RenderState3D()
    {
    }
    
    RenderState3D(const RenderState3D& s):
    nodes(s.nodes)
    {
    }
};

struct RenderQueueNode
{
    RenderQueueNode():
    distanceToView(0), renderNode(nullptr)
    {
    }
    
    RenderQueueNode(const RenderQueueNode& node)
    : distanceToView(node.distanceToView)
    , renderNode(node.renderNode)
    {
    }
    
    float distanceToView;
    const RenderNode* renderNode;
};

struct RenderQueue
{
    RenderQueue():
    queueIndex(0)
    {
    }
    
    RenderQueue(const RenderQueue& queue)
    : queueIndex(queue.queueIndex)
    {
        for (size_t i = 0; i < eRenderType_Count; ++i)
        {
            renderQueueByType[i] = queue.renderQueueByType[i];
        }
    }
    
    int queueIndex;
    list<RenderQueueNode> renderQueueByType[eRenderType_Count];
};

// This queue need to be sort every render frame. It is no need to deletenode and restore in any place more than one frame.
struct RenderQueueList
{
    RenderQueueList()
    : mCamera(NULL)
    {
        mQueues.reserve(10);
    }
    
    void Reset(iCamera* camera)
    {
        mCamera = camera;

        Clear();
    }
    
    void Clear()
    {
        mQueueIndexToQueue.clear();
        mQueues.clear();
    }
    
    void AddNode( const RenderNode* renderNode)
    {
        int renderType = renderNode->material->GetRenderType();
        piAssert(renderType < eRenderType_Count, ;);
        
        piAssert(!mCamera.IsNull(), ;);
        
        RenderQueueNode newNode;
        
        newNode.renderNode = renderNode;
        
        iEntity* entity = renderNode->parentEntity;
        
        SmartPtr<iFaceCapture> faceCapture = piGetEntityComponent<iFaceCapture>(entity);
        if (!faceCapture.IsNull())
        {
            SmartPtr<iHID> hid = entity->GetScene()->GetHID();
            piAssert( !hid.IsNull(), ;);
            
            SmartPtr<iFaceTrackerResult> result = hid->GetFaceTrackerResult();
            piCheck( !result.IsNull(), ;);
            
            int32_t faceID = faceCapture->GetFaceID();
            piCheck(faceID < result->GetFaceCount(), ;);
            
            ViewTransform = hid->GetFaceViewMatrix(faceID);
        }
        else
        {
            mat4 InvViewWorldTran = mCamera->GetEntity()->EvaluateGlobalTransform();
            ViewTransform = piglm::inverse(InvViewWorldTran);
        }
        
        // Get Queue, create one if not.
        int QueueIndex = renderNode->material->GetRenderQueue();
        RenderQueue* queue = NULL;
        
        auto Iter = mQueueIndexToQueue.find(QueueIndex);
        if (Iter == mQueueIndexToQueue.end())
        {
            RenderQueue queueTemp;
            queueTemp.queueIndex = QueueIndex;
            
            bool IsInsert = false;
            
            for (size_t i = 0; i<mQueues.size(); ++i)
            {
                if (mQueues[i].queueIndex > QueueIndex)
                {
                    mQueues.push_back(queueTemp);
                    mQueueIndexToQueue[QueueIndex] = &mQueues[i];
                    queue = &mQueues[i];
                    IsInsert = true;
                    break;
                }
            }
            
            if (!IsInsert)
            {
                mQueues.push_back(queueTemp);
                mQueueIndexToQueue[QueueIndex] =&mQueues[mQueues.size() - 1];
                queue = &mQueues[mQueues.size() - 1];
            }
        }
        else
        {
            queue = Iter->second;
        }
        
        piAssert(queue != nullptr, ;);
        // Get distanceToView
        
        
        mat4 modelWorldTran = mat4();
        
        if (!renderNode->node.IsNull())
            modelWorldTran = renderNode->node->GetTransform()->EvaluateMatrix();
        
        modelWorldTran *= renderNode->parentEntity->EvaluateGlobalTransform();
        modelWorldTran = ViewTransform * modelWorldTran;
        newNode.distanceToView = -modelWorldTran[3].z;
        
        //piCheck(newNode.distanceToView > 0, ;);
        
        list<RenderQueueNode>& typeList = queue->renderQueueByType[renderType];
        
        switch(renderType)
        {
            case eRenderType_Opaque:
            {
                list<RenderQueueNode>::iterator Iter = typeList.begin();
                
                bool IsInsert = false;
                while (Iter != typeList.end())
                {
                    if ( newNode.distanceToView < Iter->distanceToView )
                    {
                        typeList.insert(Iter, newNode);
                        IsInsert = true;
                        break;
                    }
                    
                    ++Iter;
                }
                
                if (!IsInsert)
                    typeList.push_back(newNode);
            }
            break;
            case eRenderType_Transparent:
            {
                list<RenderQueueNode>::iterator Iter = typeList.begin();
                bool IsInsert = false;
                while (Iter != typeList.end())
                {
                    if ( newNode.distanceToView > Iter->distanceToView )
                    {
                        typeList.insert(Iter, newNode);
                        IsInsert = true;
                        break;
                    }
                    
                    ++Iter;
                }
                if (!IsInsert)
                    typeList.push_back(newNode);
            }
            break;
            default:
            {
                typeList.push_back(newNode);
            }
            break;
        }
    }
    
    int queueIndex;
    
    mat4 ViewTransform;
    
    SmartPtr<iCamera> mCamera;
    
    map<int32_t,RenderQueue*>   mQueueIndexToQueue;
    vector< RenderQueue > mQueues;
};

typedef void (*ApplyPredefinePropFunc)(int32_t program, iMaterialProp* prop, int32_t& texUnit, const RenderNode& renderNode);

static void ApplyVideoTex(int32_t program, iMaterialProp* prop, int32_t& texUnit, const RenderNode& renderNode);

static void ApplyCaptureMaskTex(int32_t program, iMaterialProp* prop, int32_t& texUnit, const RenderNode& renderNode);

template <class I>
class RenderSystem3DImpl : public SystemImpl<I>
{
protected:
    using SystemImpl<I>::GetAssetManager;
    using SystemImpl<I>::GetClassLoader;
    using SystemImpl<I>::GetFirstActiveCameraEntity;
    using SystemImpl<I>::GetBounds;
    using SystemImpl<I>::GetScene;
    using SystemImpl<I>::mEntities;
    
    bool mIsOpenMotionMgr;
    
    int32_t mMaskTex;
    
    mat4 mFaceProjMatrix;
    
    typedef map<iEntity*, RenderState3D> StateMap;
    StateMap mStateMap;

    map<string, ApplyPredefinePropFunc>  PredefinedTextureName;
    map<string, vec3> Vec3PredefinedProperty;
    map<string, mat4> Mat4PredefinedProperty;
public:
    RenderSystem3DImpl()
    : mMaskTex(0)
    , mIsOpenMotionMgr(false)
    {
        mFaceProjMatrix = piProject(0, 720, 0, 1280, g_Near, g_Far);
    }
    
    virtual ~RenderSystem3DImpl()
    {
    }
    
    virtual void OnLoad()
    {
        mMaskTex = CreateColorTexture(vec4(1, 1, 1, 0));
        
        SmartPtr<iAssetManager> assetMgr = GetAssetManager();
        SmartPtr<iClassLoader> classLoader = GetClassLoader();
    
        InitPredefinedProperty();
    }
    
    virtual void OnUnload()
    {
        piReleaseGraphicsObject(mMaskTex);
        mMaskTex = 0;
        
        ClearRenderTextureTable();
        
        if (mIsOpenMotionMgr)
        {
            iMotionManager* motionMgr = GetScene()->GetMotionManager();
            piCheck(motionMgr, ;);
            motionMgr->Stop();
            mIsOpenMotionMgr = false;
        }
    }
    
    virtual void OnResize(const rect& bounds)
    {
        mFaceProjMatrix = piProject(bounds.x,
                                    bounds.x + bounds.width,
                                    bounds.y,
                                    bounds.y + bounds.height,
                                    g_Near,
                                    g_Far);
        
        SmartPtr<iEntityArray> cameraEntities = GetScene()->GetCameraEntities();
        
        for (int i = 0; i<cameraEntities->GetCount(); ++i )
        {
            iEntity* cameraEntity = cameraEntities->GetItem(i);
            
            piAssert(cameraEntity, ;);
            
            SmartPtr<iCamera> camera = piGetEntityComponent<iCamera>(cameraEntity);
            piAssert(!camera.IsNull(), ;);
            
            SmartPtr<iRenderTexture> target = camera->GetRenderTarget();
            
            if (target.IsNull())
                continue;
            
            if (target->IsFixScreenBounds())
            {
                target->Resize(bounds.width,bounds.height);
            }
        }
    }
    
    
    virtual void OnUpdate(float delta)
    {
        piCheck(!mEntities->IsEmpty(), ;);
        
        SmartPtr<iEntityArray> cameraEntities = GetScene()->GetCameraEntities();
        
        for (int i = 0; i<cameraEntities->GetCount(); ++i )
        {
            iEntity* cameraEntity = cameraEntities->GetItem(i);
            
            piAssert(cameraEntity, ;);
            
            SmartPtr<iCamera> camera = piGetEntityComponent<iCamera>(cameraEntity);
            piAssert(!camera.IsNull(), ;);
            
            if (!cameraEntity->IsActiveRecursively())
            {
                continue;
            }
            
            camera->OnPrepareRender();

            RenderQueueList queueList;
            queueList.Reset(camera);
            
            for (int j = 0; j < mEntities->GetCount(); ++j)
            {
                iEntity* e = mEntities->GetItem(j);
                
                if (!e->IsActiveRecursively())
                {
                    continue;
                }
                
                SortEntityToRenderQueue(e, camera, queueList);
                
            }
            DoRenderQueueList(camera, queueList);
            
            camera->OnFinishRender();
        }
    }
    
protected:
    void InitPredefinedProperty()
    {
        PredefinedTextureName.insert(make_pair("VIDEO", ApplyVideoTex));
        PredefinedTextureName.insert(make_pair("CAPTURE_MASK", ApplyCaptureMaskTex));
        
        Vec3PredefinedProperty.insert(make_pair("ViewPosition", vec3()));
        
        Mat4PredefinedProperty.insert(make_pair("WorldMatrix", mat4()));
        
        Mat4PredefinedProperty.insert(make_pair("MMatrix", mat4()));
        Mat4PredefinedProperty.insert(make_pair("I_VMatrix", mat4()));
        Mat4PredefinedProperty.insert(make_pair("PMatrix", mat4()));
        
        Mat4PredefinedProperty.insert(make_pair("VMatrix", mat4()));
        Mat4PredefinedProperty.insert(make_pair("VPMatrix", mat4()));
        
        Mat4PredefinedProperty.insert(make_pair("WorldVPMatrix", mat4()));
        Mat4PredefinedProperty.insert(make_pair("MVPMatrix", mat4()));
    }
    
    int32_t CreateVAO(iMesh* meshObj)
    {
        int32_t vao = piCreateVertexArray();
        
        piBindVertexArray(vao);
        
        piBindBuffer(eGraphicsBuffer_Vertex, meshObj->GetVBO());
        piBindBuffer(eGraphicsBuffer_Index, meshObj->GetIBO());
        
        piEnableVertexAttr(0);
        piEnableVertexAttr(1);
        piEnableVertexAttr(2);
        piEnableVertexAttr(3);
        piEnableVertexAttr(4);
        
        piVertexAttr(0, 3, eType_F32, sizeof(MeshVertex), offsetof(MeshVertex, pos));
        piVertexAttr(1, 2, eType_F32, sizeof(MeshVertex), offsetof(MeshVertex, uv));
        piVertexAttr(2, 4, eType_F32, sizeof(MeshVertex), offsetof(MeshVertex, jindex));
        piVertexAttr(3, 3, eType_F32, sizeof(MeshVertex), offsetof(MeshVertex, jweight));
        piVertexAttr(4, 3, eType_F32, sizeof(MeshVertex), offsetof(MeshVertex, normal));
        
        piBindVertexArray(0);
        piBindBuffer(eGraphicsBuffer_Vertex, 0);
        piBindBuffer(eGraphicsBuffer_Index, 0);
        
        return vao;
    }
    
    void InitNodeState(RenderState3D& state, iModelNode* node, iEntity* entity)
    {
        SmartPtr<iModelMeshArray> meshes = node->GetMeshes();
        
        for (auto i = 0; i < meshes->GetCount(); ++i)
        {
            SmartPtr<iModelMesh> mesh = meshes->GetItem(i);
            SmartPtr<iMesh> meshObj = CreateMeshFromLib(mesh);
            if (!meshObj.IsNull())
            {
                int32_t vao = CreateVAO(meshObj);
                PILOGI(PI_GAME_TAG, "InitNodeState VAO:[%d]", vao);
                
                RenderNode nodeState;
                nodeState.mesh = meshObj;
                nodeState.SetVAO(vao);
                nodeState.material = mesh->GetMaterial();
                
                nodeState.node = node;
                nodeState.parentEntity = entity;
                
                state.nodes.push_back(nodeState);
                
                piReleaseGraphicsObject(vao);
            }
        }
        
        SmartPtr<iModelSkinArray> skins = node->GetSkins();
        for (auto i = 0; i < skins->GetCount(); ++i)
        {
            SmartPtr<iModelSkin> skin = skins->GetItem(i);
            
            SmartPtr<iModelMesh> mesh = skin->GetMesh();
            if (mesh.IsNull())
            {
                continue;
            }
            
            SmartPtr<iMesh> meshObj = CreateMeshFromLib(mesh);
            if (meshObj.IsNull())
            {
                continue;
            }
            
            RenderNode nodeState;
            nodeState.mesh = meshObj;
            nodeState.SetVAO(CreateVAO(meshObj));
            nodeState.material = mesh->GetMaterial();
            
            SmartPtr<iMat4Array> IBMs = skin->GetIBMs();
            SmartPtr<iModelNodeArray> joints = skin->GetJoints();
            if (IBMs->GetCount() != joints->GetCount())
            {
                continue;
            }
            
            nodeState.JMs = CreateMat4Array();
            nodeState.JMs->Resize(IBMs->GetCount());
            
            nodeState.node = node;
            nodeState.skin = skin;
            nodeState.parentEntity = entity;
            
            state.nodes.push_back(nodeState);
        }
    }
    
    void InitRenderState(RenderState3D& state, iModelNode* node, iEntity* entity)
    {
        if (!node->GetMeshes()->IsEmpty() || !node->GetSkins()->IsEmpty())
        {
            InitNodeState(state, node, entity);
        }
        
        SmartPtr<iModelNodeArray> children = node->GetChildren();
        for (auto i = 0; i < children->GetCount(); ++i)
        {
            SmartPtr<iModelNode> child = children->GetItem(i);
            InitRenderState(state, child, entity);
        }
    }
    
    virtual void OnAddEntity(iEntity* entity)
    {
        OnAddModelInstanceEntity(entity);
        OnAddDynamicMeshEntity(entity);
    }
    
    virtual void OnRemoveEntity(iEntity* entity)
    {
        mStateMap.erase(entity);
    }
    
    void OnAddModelInstanceEntity(iEntity* entity)
    {
        SmartPtr<iModelInstance> modelInst = piGetEntityComponent<iModelInstance>(entity);
        piCheck(!modelInst.IsNull(), ;);
        
        SmartPtr<iModelScene> modelScene = modelInst->GetModelScene();
        piAssert(!modelScene.IsNull(), ;);
        
        SmartPtr<iModelNode> rootNode = modelScene->GetRootNode();
        piAssert(!rootNode.IsNull(), ;);
        
        RenderState3D state;
        InitRenderState(state, rootNode, entity);
        
        mStateMap[entity] = state;
    }
    
    void OnAddDynamicMeshEntity(iEntity* entity)
    {
        SmartPtr<iDynamicMesh> dynamicMesh = piGetEntityComponent<iDynamicMesh>(entity);
        piCheck(!dynamicMesh.IsNull(), ;);
        
        RenderState3D state;
        
        RenderNode node;
        
        node.node = dynamicMesh->GetModelNode();
        node.mesh = dynamicMesh->GetMesh();
        node.material = dynamicMesh->GetMaterial();
        
        if (node.material.IsNull())
        {
            PILOGW(PI_GAME_TAG,
                   "%s's dynamicMesh do not have material, create default material instead!",
                   entity->GetName().c_str());
            node.material = CreateDefaultMaterial();
        }
        
        node.SetVAO(dynamicMesh->GetVAO());
        node.parentEntity = entity;
        node.AnchorMatrix = &dynamicMesh->GetAnchorMatrix();
        
        state.nodes.push_back(node);
        
        mStateMap[entity] = state;
    }

    
    void SortEntityToRenderQueue(iEntity* entity, iCamera* camera, RenderQueueList& queueList)
    {
        piCheck(entity->IsActive(), ;);
        piCheck((entity->GetTag() & camera->GetDrawTag()) != 0, ;);
        
        auto it = mStateMap.find(entity);
        piCheck(it != mStateMap.end(), ;);
        
        const RenderState3D& state = it->second;
        
        for (size_t i = 0; i < state.nodes.size(); ++i)
        {
            queueList.AddNode( &state.nodes[i] );
        }
        
//        SmartPtr<iEntityArray> children = entity->GetChildren();
//        for (int32_t i = 0; i < children->GetCount(); ++i)
//        {
//            SortEntityToRenderQueue(children->GetItem(i), camera, queueList);
//        }
    }
    
    void DoRenderQueueList(iCamera* camera, const RenderQueueList& queueList)
    {
        for (const auto& queue : queueList.mQueues)
        {
            for (int type = 0; type < eRenderType_Count; ++type)
            {
                const list<RenderQueueNode>& typeList = queue.renderQueueByType[type];

                for (auto& node : typeList)
                {
                    const RenderNode* renderNode = node.renderNode;
                    piAssert( renderNode != NULL, ;);

                    SmartPtr<iFaceCapture> faceCapture = piGetEntityComponent<iFaceCapture>(renderNode->parentEntity);
                    if (!faceCapture.IsNull())
                    {
                        SmartPtr<iHID> hid = GetScene()->GetHID();
                        piAssert( !hid.IsNull(), ;);
                        
                        SmartPtr<iFaceTrackerResult> result = hid->GetFaceTrackerResult();
                        piCheck( !result.IsNull(), ;);
                        
                        int32_t faceID = faceCapture->GetFaceID();
                        piCheck(faceID < result->GetFaceCount(), ;);
                        
                        if (!faceCapture->IsOrth())
                        {
                            Mat4PredefinedProperty["VMatrix"] = hid->GetFaceViewMatrix(faceID);
                            
                            mat4 InvViewMatrix = piglm::inverse(Mat4PredefinedProperty["VMatrix"]);
                            Mat4PredefinedProperty["I_VMatrix"] = InvViewMatrix;
                            
                            Vec3PredefinedProperty["ViewPosition"] = vec3(InvViewMatrix[3].x,InvViewMatrix[3].y,InvViewMatrix[3].z);
                            
                            Mat4PredefinedProperty["VPMatrix"] = mFaceProjMatrix * hid->GetFaceViewMatrix(faceID);
                            
                            if (renderNode->AnchorMatrix != nullptr)
                            {
                                Mat4PredefinedProperty["WorldVPMatrix"] = Mat4PredefinedProperty["VPMatrix"] * (*renderNode->AnchorMatrix) * renderNode->parentEntity->EvaluateGlobalTransform();
                            }
                            else
                            {
                                Mat4PredefinedProperty["WorldVPMatrix"] = Mat4PredefinedProperty["VPMatrix"] * renderNode->parentEntity->EvaluateGlobalTransform();
                            }
                        }
                        else
                        {
                            // TODO: not finish yet.
                            UpdateCameraMatrixProperty(camera->GetEntity());
                            
                            
                            mat4 faceViewMatrix = hid->GetFaceViewMatrix(faceID);
                            
//                            mat4 backMatrix = piglm::translate(vec3(-faceViewMatrix[3].x,-faceViewMatrix[3].y,0.0f));
                            
                            //faceViewMatrix[3].x = faceViewMatrix[3].y = 0;
                            
                            mat4 faceMatrix = mFaceProjMatrix * faceViewMatrix;
                            
                            if (renderNode->AnchorMatrix != nullptr)
                            {
                                Mat4PredefinedProperty["WorldVPMatrix"] =  Mat4PredefinedProperty["VPMatrix"] * (*renderNode->AnchorMatrix) * renderNode->parentEntity->EvaluateGlobalTransform() * faceMatrix;
                            }
                            else
                            {
//                                Mat4PredefinedProperty["WorldVPMatrix"] =  Mat4PredefinedProperty["VPMatrix"] * faceMatrix * renderNode->parentEntity->EvaluateGlobalTransform();
                                
                                Mat4PredefinedProperty["WorldVPMatrix"] = /*renderNode->parentEntity->EvaluateGlobalTransform() **/ faceMatrix;
                            }
                        }
                    }
                    else
                    {
                        UpdateCameraMatrixProperty(camera->GetEntity());
                        if (renderNode->AnchorMatrix != nullptr)
                        {
                            Mat4PredefinedProperty["WorldVPMatrix"] =  Mat4PredefinedProperty["VPMatrix"] * (*renderNode->AnchorMatrix) * renderNode->parentEntity->EvaluateGlobalTransform();
                        }
                        else
                        {
                            Mat4PredefinedProperty["WorldVPMatrix"] =  Mat4PredefinedProperty["VPMatrix"] * renderNode->parentEntity->EvaluateGlobalTransform();
                        }
                    }
                    
                    OnRender( *renderNode, renderNode->parentEntity);
                }
            }
        }
    }
    
    void OnUpdateEntity(iEntity* entity, iCamera* camera)
    {
        auto it = mStateMap.find(entity);
        piCheck(it != mStateMap.end(), ;);
        
        const RenderState3D& state = it->second;
        
        SmartPtr<iFaceCapture> faceCapture = piGetEntityComponent<iFaceCapture>(entity);
        
        if (!faceCapture.IsNull())
        {
            SmartPtr<iHID> hid = GetScene()->GetHID();
            piAssert( !hid.IsNull(), ;);
            
            SmartPtr<iFaceTrackerResult> result = hid->GetFaceTrackerResult();
            piCheck( !result.IsNull(), ;);
            
            int32_t faceID = faceCapture->GetFaceID();
            piCheck(faceID < result->GetFaceCount(), ;);

            Mat4PredefinedProperty["VMatrix"] = hid->GetFaceViewMatrix(faceID);
            
            mat4 InvViewMatrix = piglm::inverse(Mat4PredefinedProperty["VMatrix"]);
            Mat4PredefinedProperty["I_VMatrix"] = InvViewMatrix;
            
            Vec3PredefinedProperty["ViewPosition"] = vec3(InvViewMatrix[3].x,InvViewMatrix[3].y,InvViewMatrix[3].z);
            
            Mat4PredefinedProperty["VPMatrix"] = mFaceProjMatrix * hid->GetFaceViewMatrix(faceID);
            
            Mat4PredefinedProperty["WorldVPMatrix"] = Mat4PredefinedProperty["VPMatrix"] * entity->EvaluateGlobalTransform();
        }
        else
        {
            Mat4PredefinedProperty["WorldVPMatrix"] = Mat4PredefinedProperty["VPMatrix"] * entity->EvaluateGlobalTransform();
        }
        //mat4 MVP = VP * modelMatrix;
        
        
        for (auto& node : state.nodes)
        {
            OnRender( node, entity);
        }
    }

    
    void UpdateCameraMatrixProperty(iEntity* cameraEnity)
    {
        SmartPtr<iCamera> camera = piGetEntityComponent<iCamera>(cameraEnity);
        SmartPtr<iTransform> cameraTran = piGetEntityComponent<iTransform>(cameraEnity);
        
        float ratio = GetBounds().width / GetBounds().height;
        float width = ratio / 2;
        float height = 0.5;
        
        int mode = camera->GetMode();
        
        if (mode == eCameraMode_Persp)
        {
            //Mat4PredefinedProperty["PMatrix"] = piglm::frustum(-width, width, -height, height, g_Near, g_Far);
            
            Mat4PredefinedProperty["PMatrix"] = piglm::perspectiveFov( camera->GetFov(), width, height, camera->GetNear(), camera->GetFar());
        }
        else
        {
            Mat4PredefinedProperty["PMatrix"] = piglm::ortho(-width, width, -height, height, camera->GetNear(), camera->GetFar());
        }
        
        switch (camera->GetCameraFollowType())
        {
            case eCameraFollowType_Free:
                {
                    Vec3PredefinedProperty["ViewPosition"] = cameraTran->GetTranslation();
                    
                    mat4& viewMat = Mat4PredefinedProperty["I_VMatrix"];
                    viewMat = cameraTran->GetMatrix();
                    Mat4PredefinedProperty["VMatrix"] = piglm::inverse(viewMat);
                }
                break;
            case eCameraFollowType_Motion:
                {
                    iMotionManager* motionMgr = GetScene()->GetMotionManager();
                    piCheck(motionMgr, ;);
                    
                    iDeviceMotion* deviceMotion = motionMgr->GetMotion();
                    piCheck(deviceMotion, ;);
                    
                    vec3 att = deviceMotion->GetAttitude();
                    
                    //If use forward camera should invert the X&Y rotation.
                    //att.x = -att.x;
                    //att.y = -att.y;
                    
                    cameraTran->SetOrientation(att);
                    
                    Vec3PredefinedProperty["ViewPosition"] = cameraTran->GetTranslation();
                    
                    // TO DO using cameraTran make rotation Qy*Qx*Qz will be a mistake when angleZ is 180 degree. This should be fixed later.
                    mat4& viewMat = Mat4PredefinedProperty["I_VMatrix"];
                    viewMat = piglm::toMat4( deviceMotion->GetAttitudeQuat());// cameraTran->GetMatrix(); //
                    
                    //mat4 viewMat1 = piglm::toMat4(deviceMotion->GetAttitudeQuat());
                    
                    Mat4PredefinedProperty["VMatrix"] = piglm::inverse(viewMat);
                }
                break;
            default:
                {
                    mat4& viewMat = Mat4PredefinedProperty["VMatrix"];
                    viewMat = piglm::lookAt(vec3(0, 0, 5), vec3(0, 0, 0), vec3(0, 1, 0));
                    
                    Vec3PredefinedProperty["ViewPosition"] = vec3(0, 0, 5);
                    Mat4PredefinedProperty["I_VMatrix"] = piglm::inverse(viewMat);
                }
                break;
        }
        
        Mat4PredefinedProperty["VPMatrix"] =  Mat4PredefinedProperty["PMatrix"] *Mat4PredefinedProperty["VMatrix"];
        //mat4 VP = BuildVP(GetBounds(), camera);
    }
    
    static mat4 BuildVP(const rect& bounds, iCamera* camera)
    {
        float ratio = bounds.width / bounds.height;
        float width = ratio / 2;
        float height = 0.5;
        
        int mode = camera->GetMode();
        
        mat4 projMatrix;
        if (mode == eCameraMode_Persp)
        {
            projMatrix = piglm::frustum(-width, width, -height, height, g_Near, g_Far);
        }
        else
        {
            projMatrix = piglm::ortho(-width, width, -height, height, g_Near, g_Far);
        }
        
        mat4 viewMatrix = piglm::lookAt(vec3(0, 0, 5), vec3(0, 0, 0), vec3(0, 1, 0));
        
        return projMatrix * viewMatrix;
    }
    
    virtual void OnRender(const RenderNode& state, iEntity* entity) = 0;
};


static void ApplyVideoTex(int32_t program, iMaterialProp* prop, int32_t& texUnit, const RenderNode& renderNode)
{
    SmartPtr<iHID> hid = renderNode.parentEntity->GetScene()->GetHID();
    
    SmartPtr<iNativeTexture> videoTex = hid->GetFilteredFrame();
    
    piCheck(!videoTex.IsNull(), ;);
    piCheck(videoTex->GetName() != 0, ;);
    
    piActiveTexture(texUnit);
    piUniform1i(program, prop->GetName(), texUnit);
    ++texUnit;
    
    piBindTexture(eTexTarget_2D, videoTex->GetName());
}

static void ApplyCaptureMaskTex(int32_t program, iMaterialProp* prop, int32_t& texUnit, const RenderNode& renderNode)
{
    SmartPtr<iHID> hid = renderNode.parentEntity->GetScene()->GetHID();
    
    SmartPtr<iFaceTrackerResult> result = hid->GetFaceTrackerResult();
    piCheck(!result.IsNull(), ;);
    
    SmartPtr<iTexture2D> maskTexture = result->GetBgMaskTexture();
    piCheck(!maskTexture.IsNull(), ;);
    
    piActiveTexture(texUnit);
    piUniform1i(program, prop->GetName(), texUnit);
    ++texUnit;
    
    piBindTexture(eTexTarget_2D, maskTexture->GetGraphicsName());
}

static void ApplyProperty(int32_t program, iMaterialProp* prop, int32_t& texUnit)
{
    auto name = prop->GetName();
    auto value = prop->GetValue();
    switch (value.GetType())
    {
        case eType_I8:
        case eType_U8:
        case eType_I16:
        case eType_U16:
        case eType_I32:
        case eType_U32:
        case eType_I64:
        case eType_U64:
            piUniform1i(program, name, value);
            break;
        case eType_F32:
            piUniform1f(program, name, value.GetF32());
            break;
        case eType_F64:
            piUniform1f(program, name, value.GetF64());
            break;
        case eType_Object:
            if (value.GetObject()->GetClass() == iTexture2D::StaticClass())
            {
                piActiveTexture(texUnit);
                piUniform1i(program, name, texUnit);
                ++texUnit;
                
                SmartPtr<iTexture2D> tex = piQueryVarObject<iTexture2D>(value);
                piBindTexture(eTexTarget_2D, tex->GetGraphicsName());
            }
            else if (value.GetObject()->GetClass() == iCubeMap::StaticClass())
            {
                piActiveTexture(texUnit);
                piUniform1i(program, name, texUnit);
                ++texUnit;
                
                SmartPtr<iCubeMap> cubeMap = piQueryVarObject<iCubeMap>(value);
                piBindTexture(eTexTarget_CubeMap, cubeMap->GetGraphicsName());
            }
            else if (value.GetObject()->GetClass() == iRenderTexture::StaticClass())
            {
                piActiveTexture(texUnit);
                piUniform1i(program, name, texUnit);
                ++texUnit;
                
                SmartPtr<iRenderTexture> renderTex = piQueryVarObject<iRenderTexture>(value);
                
                switch(renderTex->GetDimession())
                {
                    case eRenderTextureDimession_2D:
                    {
                        piBindTexture(eTexTarget_2D, renderTex->GetGraphicsName());
                    }
                    break;
                    case eRenderTextureDimession_3D:
                    case eRenderTextureDimession_Cube:
                    {
                        PILOGE(PI_GAME_TAG,"Render texture only support 2D only.");
                    }
                    break;
                }
            }
            break;
        case eType_Vec2:
            piUniform2f(program, name, value.GetVec2().x, value.GetVec2().y);
            break;
        case eType_Vec3:
            piUniformVec3(program, name, value);
            break;
        case eType_Vec4:
            piUniformVec4(program, name, value);
            break;
        case eType_Mat4:
            piUniformMat4f(program, name, value);
            break;
        case eType_Rect:
            piUniformRect(program, name, value);
            break;
        default:
            break;
    }
}


class RenderSystem3D : public RenderSystem3DImpl<iRenderSystem3D>
{
public:
    RenderSystem3D()
    {
    }
    
    virtual ~RenderSystem3D()
    {
    }
    
    virtual bool Accept(iEntity* entity)
    {
        CheckNeedOpenMotion( entity );
        
        piCheck(!CheckHasModelInstance( entity ), true;);
        
        piCheck(!CheckHasDynamicMesh( entity ), true;);
        
        return false;
    }
    
    
    
protected:
    void CheckNeedOpenMotion(iEntity* entity)
    {
        if (mIsOpenMotionMgr)
            return;
        
        SmartPtr<iCamera> camera = piGetEntityComponent<iCamera>(entity);
        if (!camera.IsNull())
        {
            if ( camera->GetCameraFollowType() == eCameraFollowType_Motion)
            {
                iMotionManager* motionMgr = GetScene()->GetMotionManager();
                piAssert(motionMgr, ;);
                
                motionMgr->Start();
                
                mIsOpenMotionMgr = true;
            }
        }
    }
    
    bool CheckHasModelInstance(iEntity* entity)
    {
        SmartPtr<iModelInstance> inst = piGetEntityComponent<iModelInstance>(entity);
        return !inst.IsNull() && inst->GetModelScene() != nullptr;
    }
    
    bool CheckHasDynamicMesh(iEntity* entity)
    {
        SmartPtr<iDynamicMesh> inst = piGetEntityComponent<iDynamicMesh>(entity);
        
        return !inst.IsNull() && inst->GetMesh() != nullptr;
    }
    
    void UpdatePredefinedPropertyToShader(int32_t program, iMaterial* material)
    {
        
        SmartPtr<iMaterialPropArray> propsArray = material->GetProps();
        
        for (int32_t i = 0; i < propsArray->GetCount(); ++i)
        {
            iMaterialProp* prop = propsArray->GetItem(i);
            piAssert(prop, ;);
            
            switch (prop->GetValue().GetType()) {
                case eType_Vec3:
                {
                    map<string, vec3>::iterator Iter = Vec3PredefinedProperty.find(prop->GetName());
                    
                    if (Iter != Vec3PredefinedProperty.end())
                    {
                        piUniformVec3(program, Iter->first, Iter->second);
                    }
                }
                break;
                case eType_Mat4:
                {
                    map<string, mat4>::iterator Iter = Mat4PredefinedProperty.find(prop->GetName());
                    
                    if (Iter != Mat4PredefinedProperty.end())
                    {
                        piUniformMat4f(program, Iter->first, Iter->second);
                    }
                }
                break;
                default:
                    break;
            }
        
        }
    }
    
    virtual void OnRender(const RenderNode& renderNode, iEntity* entity)
    {
        SmartPtr<iModelNode> node = renderNode.node;
        
        string name = "no name";
        if (!node.IsNull())
        {
            name = node->GetName();
            
            if (renderNode.AnchorMatrix != nullptr)
            {
                Mat4PredefinedProperty["WorldMatrix"] = (*renderNode.AnchorMatrix) * entity->EvaluateGlobalTransform()*node->EvaluateGlobalMatrix();
            }
            else
            {
                Mat4PredefinedProperty["WorldMatrix"] = entity->EvaluateGlobalTransform()*node->EvaluateGlobalMatrix();
            }
            Mat4PredefinedProperty["MMatrix"] = node->EvaluateGlobalMatrix();
            
            Mat4PredefinedProperty["MVPMatrix"] = Mat4PredefinedProperty["WorldVPMatrix"] * node->EvaluateGlobalMatrix();
        }
        else
        {
            if (renderNode.AnchorMatrix != nullptr)
            {
                Mat4PredefinedProperty["WorldMatrix"] = *renderNode.AnchorMatrix * entity-> EvaluateGlobalTransform() ;
            }
            else
            {
                Mat4PredefinedProperty["WorldMatrix"] = entity->EvaluateGlobalTransform() ;
            }
            Mat4PredefinedProperty["MMatrix"] = mat4();
            Mat4PredefinedProperty["MVPMatrix"] = Mat4PredefinedProperty["WorldVPMatrix"];
        }
        
        piPushGroupMarker(piFormat("[%s]RenderSystem3D::OnRender", name.c_str()));
        
        
        SmartPtr<iRenderState> state = renderNode.material->GetRenderState();
        
        piAssert(!state.IsNull(), ;);
        
        state->EnableState();
        
        piBindVertexArray(renderNode.vao);
        
        int32_t program = renderNode.material->GetShader()->GetGraphicsName();
        piUseProgram(program);
        
        //UpdatePredefinedPropertyToShader(program, renderNode.material);
        //piUniformMat4f(program, "MVPMatrix", MVP * modelMatrix);
        
        // skin
        SmartPtr<iModelSkin> skin = renderNode.skin;
        if (!skin.IsNull())
        {
            SmartPtr<iMat4Array> JMs = renderNode.JMs;
            
            SmartPtr<iMat4Array> IBMs = skin->GetIBMs();
            
            SmartPtr<iModelNodeArray> joints = skin->GetJoints();
            for (auto i = 0; i < joints->GetCount(); ++i)
            {
                SmartPtr<iModelNode> joint = joints->GetItem(i);
                mat4 matrix = joint->EvaluateGlobalMatrix() * IBMs->GetItem(i);
                JMs->SetItem(i, matrix);
            }
            
            piUniformMat4fv(program, "JMs", JMs);
        }
        
        // uniforms
        SmartPtr<iMaterial> material = renderNode.material;
        SmartPtr<iMaterialPropArray> props = material->GetProps();
        
        // This matrix will be use for some old scene, so it will be actived in every node.
        piUniformMat4f(program, "MVPMatrix", Mat4PredefinedProperty["MVPMatrix"]);
        
        int32_t textureUnit = 0;
        for (auto i = 0; i < props->GetCount(); ++i)
        {
            SmartPtr<iMaterialProp> prop = props->GetItem(i);
            
            switch (prop->GetValue().GetType())
            {
                case eType_Object:
                    {
                        auto Iter = PredefinedTextureName.find(prop->GetName());
                        
                        if (Iter != PredefinedTextureName.end())
                        {
                            piAssert(Iter->second != NULL, ;);
                            Iter->second( program, prop, textureUnit, renderNode);
                            continue;
                        }
                        
                    }
                    break;
                case eType_Vec3:
                    {
                        map<string, vec3>::iterator Iter = Vec3PredefinedProperty.find(prop->GetName());
                        
                        if (Iter != Vec3PredefinedProperty.end())
                        {
                            piUniformVec3(program, Iter->first, Iter->second);
                            continue;
                        }
                    }
                    break;
                case eType_Mat4:
                    {
                        map<string, mat4>::iterator Iter = Mat4PredefinedProperty.find(prop->GetName());
                        
                        if (Iter != Mat4PredefinedProperty.end())
                        {
                            piUniformMat4f(program, Iter->first, Iter->second);
                            continue;
                        }
                    }
                    break;
            }
            
            ApplyProperty(program, prop, textureUnit);
        }
        
        piDrawElements(eGraphicsDraw_Triangles, renderNode.mesh->GetCount(), renderNode.mesh->GetType());
        
        piUseProgram(0);
        
        piBindVertexArray(0);
        
        state->DisableState();
        
        //piDisableFeatures(features);
        
        piPopGroupMarker();
    }
    
    
};


iRenderSystem3D* CreateRenderSystem3D()
{
    return new RenderSystem3D();
}


NSPI_END()

















