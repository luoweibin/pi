/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.11.21   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_SRC_3D_DEBUG_SKELETONDEBUGPROGRAM_H
#define PI_GAME_SRC_3D_DEBUG_SKELETONDEBUGPROGRAM_H
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()

#if defined(PI_OPENGL)

const static char g_SkeletonDebug_VS_OpenGL[] =
"#version 410\n"
"#define INSTMAX 32\n"
"precision highp float;\n"
"layout(location=0) in vec4 position;\n"
"uniform mat4 MVPMatrix;\n"
"uniform mat4 JMs[INSTMAX];\n"
"void main(void)\n"
"{\n"
"    gl_Position = MVPMatrix * position;\n"
"}";

const static char g_SkeletonDebug_FS_OpenGL[] =
"#version 410\n"
"precision highp float;\n"
"layout(location=0) out vec4 fragColor;\n"
"void main(void) {\n"
"    fragColor = vec4(1, 0, 0, 1);\n"
"}";

#elif defined(PI_OPENGL_ES)

const static char g_SkeletonDebug_VS_ES3[] =
"#version 300 es\n"
"#define INSTMAX 32\n"
"precision highp float;\n"
"layout(location=0) in vec4 position;\n"
"uniform mat4 MVPMatrix;\n"
"uniform mat4 JMs[INSTMAX];\n"
"void main(void)\n"
"{\n"
"    gl_Position = MVPMatrix * position;\n"
"}";

const static char g_SkeletonDebug_FS_ES3[] =
"#version 300 es\n"
"precision highp float;\n"
"layout(location=0) out vec4 fragColor;\n"
"void main(void) {\n"
"    fragColor = vec4(1, 0, 0, 1);\n"
"}";

const static char g_SkeletonDebug_VS_ES2[] =
"precision highp float;\n"
"attribute vec4 position;\n"
"uniform mat4 MVPMatrix;\n"
"void main(void)\n"
"{\n"
"    gl_Position = MVPMatrix * position;\n"
"}";

const static char g_SkeletonDebug_FS_ES2[] =
"precision highp float;\n"
"void main(void) {\n"
"    gl_FragColor = vec4(1, 0, 0, 1);\n"
"}";

#endif


static int SkeletonDebug_CreateProgram()
{
    int32_t program = piCreateProgram();
    
    int type = piGetGraphicsType();
    switch (type)
    {
#if defined(PI_OPENGL_ES)
        case eGraphicsBackend_OpenGL_ES3:
            piCompileProgram(program, g_SkeletonDebug_VS_ES3, g_SkeletonDebug_FS_ES3);
            break;
        case eGraphicsBackend_OpenGL_ES2:
            piCompileProgram(program, g_SkeletonDebug_VS_ES2, g_SkeletonDebug_FS_ES2);
            piBindVertexAttr(program, 0, "position");
            break;
#endif
            
#if defined(PI_OPENGL)
        case eGraphicsBackend_OpenGL3:
        case eGraphicsBackend_OpenGL4:
            piCompileProgram(program, g_SkeletonDebug_VS_OpenGL, g_SkeletonDebug_FS_OpenGL);
            break;
#endif
        default:
            piAssertDontStop(false);
            break;
    }
    
    piLinkProgram(program);
    
    return program;
}




NSPI_END()


#endif

















