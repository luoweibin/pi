/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.6   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>
#include "../../asset/AssetImpl.h"

using namespace std;

NSPI_BEGIN()


class Material : public AssetImpl<iMaterial>
{
private:
    string mName;
    SmartPtr<iMaterialPropArray> mProps;
    
    string mShaderUri;
    SmartPtr<iShaderProgram> mShader;
    
    string mRenderStateUri;
    SmartPtr<iRenderState> mRenderState;
    
    int mRenderQueue;
    int mRenderType;
    
public:
    Material()
    : mRenderQueue(eRenderQueue_Opaque)
    , mRenderType(eRenderType_Opaque)
    {
        mProps = CreateMaterialPropArray();
        mRenderState = CreateRenderState();
    }
    
    virtual ~Material()
    {
    }
    
    virtual void SetName(const string& name)
    {
        mName = name;
    }
    
    virtual string GetName() const
    {
        return mName;
    }
    
    virtual iMaterialPropArray* GetProps() const
    {
        return mProps;
    }
    
    virtual void SetProps(iMaterialPropArray* props)
    {
        piAssert(props != nullptr, ;);
        mProps = props;
    }
    
    virtual void SetProperty(const std::string& name, const Var& value)
    {
        piAssert(!name.empty(), ;);
        
        SmartPtr<iMaterialProp> prop = FindProperty(name);
        if (!prop.IsNull())
        {
            prop->SetValue(value);
        }
    }
    
    virtual Var GetProperty(const std::string& name, const Var& defValue) const
    {
        piAssert(!name.empty(), Var());
        
        SmartPtr<iMaterialProp> prop = FindProperty(name);
        return !prop.IsNull() ? prop->GetValue() : defValue;
    }
    
    virtual std::string GetShaderUri() const
    {
        return mShaderUri;
    }
    
    virtual void SetShaderUri(const std::string& uri)
    {
        mShaderUri = uri;
    }
    
    virtual int GetRenderQueue() const
    {
        return mRenderQueue;
    }
    
    virtual void SetRenderQueue(int Queue)
    {
        mRenderQueue = Queue;
    }
    
    virtual int GetRenderType() const
    {
        return mRenderType;
    }
    
    virtual void SetRenderType(int type)
    {
        mRenderType = type;
    }
    
    virtual iShaderProgram* GetShader() const
    {
        return mShader;
    }
    
    virtual void SetShader(iShaderProgram* shader)
    {
        piAssert(shader != nullptr, ;);
        mShader = shader;
    }
    
    virtual std::string GetRenderStateUri() const
    {
        return mRenderStateUri;
    }
    
    virtual void SetRenderStateUri(const std::string& uri)
    {
        mRenderStateUri = uri;
    }
    
    virtual iRenderState* GetRenderState() const
    {
        return mRenderState;
    }
    
    virtual void SetRenderState(iRenderState* state)
    {
        piAssert(state != nullptr, ;);
        mRenderState = state;
    }
    
    virtual iMaterialProp* FindProperty(const string& name) const
    {
        piAssert(!name.empty(), nullptr);
        
        for (auto i = 0; i < mProps->GetCount(); ++i)
        {
            SmartPtr<iMaterialProp> prop = mProps->GetItem(i);
            if (prop->GetName() == name)
            {
                return prop;
            }
        }
        
        return nullptr;
    }
};


iMaterial* CreateMaterial()
{
    return new Material();
}


iMaterial* CreateDefaultMaterial()
{
    iMaterial* pMaterial = CreateMaterial();
    
    pMaterial->SetName("Default_Material");
    
    SmartPtr<iShaderProgram> shader = CreateDefaultShaderProgram();
    
    pMaterial->SetShaderUri("Default_Shader");
    
    pMaterial->SetShader(shader);
    
    return pMaterial;
}

NSPI_END()



























