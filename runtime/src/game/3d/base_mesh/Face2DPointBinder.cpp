/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 Sword     2017.7.4   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>

#include <pi/game/impl/ComponentImpl.h>

using namespace std;

NSPI_BEGIN()

class Face2DPointBinder  : public ComponentImpl<iFace2DPointBinder>
{
private:
    bool mIsBindTranslate;
    bool mIsBindRotation;
    
    
public:
    virtual void SetBindTranslate(bool flag)
    {
        mIsBindTranslate = flag;
    }
    
    virtual bool IsBindTranslate() const
    {
        return mIsBindTranslate;
    }
    
    virtual void SetBindRotation(bool flag)
    {
        mIsBindRotation = flag;
    }
    
    virtual bool IsBindRotation() const
    {
        return mIsBindRotation;
    }
    
    virtual void OnLoad()
    {

    }
    
    virtual void OnUnload()
    {
        
    }
    
    virtual void OnUpdate(float delta)
    {
        //SmartPtr<iEntity> selfEntity = GetEntity();
        
        //SmartPtr<iUIMesh> selfMesh = piGetEntityComponent<iUIMesh>(selfEntity);
        //piCheck(!selfMesh.IsNull(), ;);
        
        OnFollowFace2DMesh( delta );
    }
    
    void OnFollowFace2DMesh(float delta)
    {
        // TODO: not finish yet.
        SmartPtr<iEntity> selfEntity = GetEntity();
        piCheck(!selfEntity.IsNull(), ;);
        
        SmartPtr<iTransform> transform = piGetEntityComponent<iTransform>(selfEntity);
        piAssert(!transform.IsNull(), ;);
        
        SmartPtr<iEntity> parentEnitity =  selfEntity->GetParent();
        piCheck(!parentEnitity.IsNull(), ;);
        
        SmartPtr<iFace2DMesh> parentFace2DMesh = piGetEntityComponent<iFace2DMesh>(parentEnitity);
        piCheck(!parentFace2DMesh.IsNull(), ;);
        
        SmartPtr<iModelNode> modelNode = parentFace2DMesh->GetModelNode();
        piAssert(!modelNode.IsNull(), ;);
        
        SmartPtr<iTransform> faceModeTransform = modelNode->GetTransform();
        piAssert(!faceModeTransform.IsNull(), ;);
        
        SmartPtr<iScene> scene = parentEnitity->GetScene();
        piCheck(!scene.IsNull(), ;);
        
        SmartPtr<iHID> hid = scene->GetHID();
        piCheck(!hid.IsNull(), ;);
        
        SmartPtr<iFaceTrackerResult> faceTrackerResult = hid->GetFaceTrackerResult();
        piCheck(!faceTrackerResult.IsNull(), ;);
        
        piCheck(parentFace2DMesh->GetFaceID()<faceTrackerResult->GetFaceCount(), ;);
        
        SmartPtr<iFaceInfo> faceInfo = faceTrackerResult->GetFace(parentFace2DMesh->GetFaceID());
        
        piCheck(!faceInfo.IsNull(), ;);
        
        rect curFaceScreenRect = parentFace2DMesh->GetFaceScreenRect();
        
        vec2 faceInVideoBegin = scene->VideoUVToScreenPos(vec2(faceInfo->GetRect().x, faceInfo->GetRect().y));
        
        vec2 faceInVideoSize = scene->VideoUVToScreenPos(vec2(faceInfo->GetRect().width, faceInfo->GetRect().height));
        
        vec3 scale( curFaceScreenRect.width/faceInVideoSize.x, curFaceScreenRect.height/faceInVideoSize.y, 1.0f);
        
        vec3 curScreenCenter(curFaceScreenRect.x + 0.5f*curFaceScreenRect.width, curFaceScreenRect.y + 0.5f*curFaceScreenRect.height, 0);
        
        vec3 curPosInVideo = scene->ScreenPosToGLOrthPosV3(curScreenCenter);
        vec3 facePosInVideo = scene->ScreenPosToGLOrthPosV3(vec3(faceInVideoBegin.x + 0.5f*faceInVideoSize.x,faceInVideoBegin.y + 0.5f*faceInVideoSize.y, 0));
       
        vec3 translate = curPosInVideo - facePosInVideo;// + faceModeTransform->GetTranslation();
        
        ;
        
        translate = vec3(-(faceInfo->GetRect().x - 0.5f)*720.0f/1280,-faceInfo->GetRect().y, 0);
        
        //printf("%f,%f,%f\n",translate.x,translate.y,translate.z);
        
       
        
        //transform->SetTranslation(translate);
        transform->SetOrientation(faceModeTransform->GetOrientation());
        transform->SetScale(scale);
    }
};

iFace2DPointBinder* CreateFace2DPointBinder()
{
    return new Face2DPointBinder();
}

NSPI_END()
