/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 Sword     2017.6.22   0.1     Create
 ******************************************************************************/

#include <pi/Game.h>

#include <pi/game/impl/DynamicMeshImpl.h>

using namespace std;

NSPI_BEGIN()

class UIMesh : public DynamicMeshImpl<iUIMesh>
{
private:
    
    rect mUVRect;
    
    rect mPosRect;
    
    vec2 mAnchor;
    
public:
    UIMesh()
    {
        mUVRect = rect( 0, 0, 1.0f, 1.0f);
        mPosRect = rect( 0.5f, 0.5f, 1.0f, 1.0f);
        
        mMesh->SetCount(6);
    }
    
    virtual ~UIMesh()
    {
        
    }
    
    virtual bool InitMeshBuffer()
    {
        mVertexBuffer = CreateMemory(4*sizeof(MeshVertex));
        mIndexBuffer = CreateMemory(6*mMesh->GetIndexSize());
        
        BuildMeshBuffer();
        switch (mMesh->GetType())
        {
            case eType_U8:
            {
                uint8_t* buf = (uint8_t *)mIndexBuffer->Ptr();
                
                buf[0] = 0;
                buf[1] = 1;
                buf[2] = 2;
                buf[3] = 1;
                buf[4] = 3;
                buf[5] = 2;
                
            }
                break;
            case eType_U16:
            {
                uint16_t* buf = (uint16_t *)mIndexBuffer->Ptr();
                
                buf[0] = 0;
                buf[1] = 1;
                buf[2] = 2;
                buf[3] = 1;
                buf[4] = 3;
                buf[5] = 2;
                
            }
                break;
            case eType_U32:
            {
                uint32_t* buf = (uint32_t *)mIndexBuffer->Ptr();
                
                buf[0] = 0;
                buf[1] = 1;
                buf[2] = 2;
                buf[3] = 1;
                buf[4] = 3;
                buf[5] = 2;
                
            }
                break;
                
            default:
                break;
        }
        ApplyIBO();
        
        return true;
    }
    
    virtual rect GetUVRect() const
    {
        return mUVRect;
    }
    
    virtual void SetUVRect(const rect& rect)
    {
        SetDirty(true);
        mUVRect = rect;
    }
    
    virtual rect GetPosRect() const
    {
        return mPosRect;
    }
    
    virtual void SetPosRect(const rect& rect)
    {
        SetDirty(true);
        mPosRect = rect;
    }
    
    virtual vec2 GetPos() const
    {
        return vec2(mPosRect.x, mPosRect.y);
    }
    
    virtual void SetPos(const vec2& left_bottom_pos)
    {
        mPosRect.x = left_bottom_pos.x;
        mPosRect.y = left_bottom_pos.y;
        SetDirty(true);
    }
    
    virtual vec2 GetPosSize() const
    {
        return vec2(mPosRect.width, mPosRect.height);
    }
    
    virtual void SetPosSize(const vec2& size)
    {
        mPosRect.width = size.x;
        mPosRect.height = size.y;
        SetDirty(true);
    }
    
    virtual vec2 GetUVBegin() const
    {
        return vec2( mUVRect.x, mUVRect.y );
    }
    
    virtual void SetUVBegin(const vec2& uvBegin)
    {
        mUVRect.x = uvBegin.x;
        mUVRect.y = uvBegin.y;
        SetDirty(true);
    }
    
    virtual vec2 GetUVSize() const
    {
        return vec2(mUVRect.width,mUVRect.height);
    }
    
    virtual void SetUVSize(const vec2& uvSize)
    {
        mUVRect.width = uvSize.x;
        mUVRect.height = uvSize.y;
        SetDirty(true);
    }
    
    virtual void SetAnchor(const vec2& anchor)
    {
        SetDirty(true);
        mAnchor = anchor;
    }
    
    virtual vec2 GetAnchor() const
    {
        return mAnchor;
    }
    
    virtual void OnResize(const rect& bounds)
    {
        SmartPtr<iEntity> entity = GetEntity();
        piAssert(!entity.IsNull(), ;);
        SmartPtr<iScene> scene = entity->GetScene();
        piAssert(!scene.IsNull(), ;);
        
        Apply();
    }
    
    virtual void Apply()
    {
        BuildMeshBuffer();
        ApplyVBO();
        ApplyVAO();
        SetDirty(false);
    }
    
    void BuildMeshBuffer()
    {
        piAssert(!mVertexBuffer.IsNull(), ;);
        piAssert(mVertexBuffer->Size() == 4*sizeof(MeshVertex), ;);
        
        SmartPtr<iEntity> entity = GetEntity();
        piAssert(!entity.IsNull(), ;);
        SmartPtr<iScene> scene = entity->GetScene();
        piAssert(!scene.IsNull(), ;);
        
        SmartPtr<iTransform> transform = piGetEntityComponent<iTransform>(GetEntity());
        
        /*
         0 - 2
         | / |
         1 - 3
         */
        
        //mRenderScale = 1.0f;
        
        vec2 ScreenOffset;
        
        
        piCheck(scene->GetPixelSize() > 0, ;);
        
        //ScreenOffset.x = mDesignResolution.x / resolutionValue * mRenderScale ;
        //ScreenOffset.y = mDesignResolution.y / resolutionValue * mRenderScale ;
        
        vec2 GlSize = scene->ScreenSizeToGLOrthSize(vec2(mPosRect.width,mPosRect.height));
        vec2 posBegin = scene->ScreenPosToGLOrthPos(vec2(mPosRect.x,mPosRect.y));
        
        //float width = mPosRect.width / resolutionValue * mRenderScale ;
        //float height = mPosRect.height / resolutionValue * mRenderScale ;
        
        //float posXBegin = (mPosRect.x  / resolutionValue ) * mRenderScale - 0.5f * ScreenOffset.x;
        //float posYBegin = (mPosRect.y  / resolutionValue ) * mRenderScale - 0.5f * ScreenOffset.y;
        
        //vec2 anchorCenterOffset(0, 0);
        
        vec2 anchorOffset;
        
        anchorOffset.x = mAnchor.x*GlSize.x + posBegin.x;
        anchorOffset.y = mAnchor.y*GlSize.y + posBegin.y;
        
        //mAnchorMatrix = piglm::translate(vec3(anchorOffset.x,anchorOffset.y,0));
        
        SmartPtr<iModelNode> modelNode = GetModelNode();
        
        float xBegin = posBegin.x - anchorOffset.x;
        float yBegin = posBegin.y - anchorOffset.y;
        
        vec3 tranPosition = vec3(anchorOffset.x,anchorOffset.y,0.0f);
        if (transform == nullptr)
        {
            modelNode->GetTransform()->SetTranslation(tranPosition);
        }
        else
        {
            transform->SetTranslation(tranPosition);
        }
        
        MeshVertex* vertexes = (MeshVertex*)mVertexBuffer->Ptr();
        
        vertexes[0].pos[0] = xBegin;
        vertexes[0].pos[1] = yBegin + GlSize.y;
        vertexes[0].pos[2] = 0.0f;
        
        vertexes[1].pos[0] = xBegin;
        vertexes[1].pos[1] = yBegin;
        vertexes[1].pos[2] = 0.0f;
        
        vertexes[2].pos[0] = xBegin + GlSize.x;
        vertexes[2].pos[1] = yBegin + GlSize.y;
        vertexes[2].pos[2] = 0.0f;
        
        vertexes[3].pos[0] = xBegin + GlSize.x;
        vertexes[3].pos[1] = yBegin;
        vertexes[3].pos[2] = 0.0f;
        
        //----------------------------------------
        
        vertexes[0].uv[0] = mUVRect.x;
        vertexes[0].uv[1] = mUVRect.y + mUVRect.height;
        
        vertexes[1].uv[0] = mUVRect.x;
        vertexes[1].uv[1] = mUVRect.y;
        
        vertexes[2].uv[0] = mUVRect.x + mUVRect.width;
        vertexes[2].uv[1] = mUVRect.y + mUVRect.height;
        
        vertexes[3].uv[0] = mUVRect.x + mUVRect.width;
        vertexes[3].uv[1] = mUVRect.y;
        
        //----------------------------------------
        
        vertexes[0].jindex[0] = 0;
        vertexes[0].jindex[1] = 0;
        vertexes[0].jindex[2] = 0;
        vertexes[0].jindex[3] = 0;
        
        vertexes[1].jindex[0] = 0;
        vertexes[1].jindex[1] = 0;
        vertexes[1].jindex[2] = 0;
        vertexes[1].jindex[3] = 0;
        
        vertexes[2].jindex[0] = 0;
        vertexes[2].jindex[1] = 0;
        vertexes[2].jindex[2] = 0;
        vertexes[2].jindex[3] = 0;
        
        vertexes[3].jindex[0] = 0;
        vertexes[3].jindex[1] = 0;
        vertexes[3].jindex[2] = 0;
        vertexes[3].jindex[3] = 0;
        
        //----------------------------------------
        
        vertexes[0].jweight[0] = 1;
        vertexes[0].jweight[1] = 0;
        vertexes[0].jweight[2] = 0;
        
        vertexes[1].jweight[0] = 1;
        vertexes[1].jweight[1] = 0;
        vertexes[1].jweight[2] = 0;
        
        vertexes[2].jweight[0] = 1;
        vertexes[2].jweight[1] = 0;
        vertexes[2].jweight[2] = 0;
        
        vertexes[3].jweight[0] = 1;
        vertexes[3].jweight[1] = 0;
        vertexes[3].jweight[2] = 0;
        
        
        //----------------------------------------
        
        vertexes[0].normal[0] = 0;
        vertexes[0].normal[1] = 0;
        vertexes[0].normal[2] = 1.0f;
        
        vertexes[1].normal[0] = 0;
        vertexes[1].normal[1] = 0;
        vertexes[1].normal[2] = 1.0f;
        
        vertexes[2].normal[0] = 0;
        vertexes[2].normal[1] = 0;
        vertexes[2].normal[2] = 1.0f;
        
        vertexes[3].normal[0] = 0;
        vertexes[3].normal[1] = 0;
        vertexes[3].normal[2] = 1.0f;
        
    }
};

iUIMesh* CreateUIMesh()
{
    return new UIMesh();
}

class QuadMesh : public DynamicMeshImpl<iQuadMesh>
{
private:
    
    vec4 mUVRect;
    
    vec4 mPosRect;
    
    
public:
    QuadMesh()
    {
        mUVRect = vec4( 0, 0, 1.0f, 1.0f);
        mPosRect = vec4( 0.5f, 0.5f, 1.0f, 1.0f);
        
        mMesh->SetCount(6);
    }
    
    virtual ~QuadMesh()
    {
        
    }
    
    virtual bool InitMeshBuffer()
    {
        mVertexBuffer = CreateMemory(4*sizeof(MeshVertex));
        mIndexBuffer = CreateMemory(6*mMesh->GetIndexSize());
        
        BuildMeshBuffer();
        switch (mMesh->GetType())
        {
            case eType_U8:
            {
                uint8_t* buf = (uint8_t *)mIndexBuffer->Ptr();
                
                buf[0] = 0;
                buf[1] = 1;
                buf[2] = 2;
                buf[3] = 1;
                buf[4] = 3;
                buf[5] = 2;

            }
                break;
            case eType_U16:
            {
                uint16_t* buf = (uint16_t *)mIndexBuffer->Ptr();
                
                buf[0] = 0;
                buf[1] = 1;
                buf[2] = 2;
                buf[3] = 1;
                buf[4] = 3;
                buf[5] = 2;
                
            }
                break;
            case eType_U32:
            {
                uint32_t* buf = (uint32_t *)mIndexBuffer->Ptr();
                
                buf[0] = 0;
                buf[1] = 1;
                buf[2] = 2;
                buf[3] = 1;
                buf[4] = 3;
                buf[5] = 2;
                
            }
                break;
                
            default:
                break;
        }
        ApplyIBO();
        
        return true;
    }
    
    virtual vec4 GetUVRect() const
    {
        return mUVRect;
    }
    
    virtual void SetUVRect(const vec4& rect)
    {
        mUVRect = rect;
    }
    
    virtual vec4 GetPosRect() const
    {
        return mPosRect;
    }
    
    virtual void SetPosRect(const vec4& rect)
    {
        mPosRect = rect;
    }
    
    virtual void Apply()
    {
        BuildMeshBuffer();
        ApplyVBO();
        ApplyVAO();
        SetDirty(false);
    }
    
    void BuildMeshBuffer()
    {
        piAssert(!mVertexBuffer.IsNull(), ;);
        piAssert(mVertexBuffer->Size() == 4*sizeof(MeshVertex), ;);
        
        /*
         0 - 2
         | / |
         1 - 3
         */
        
        MeshVertex* vertexes = (MeshVertex*)mVertexBuffer->Ptr();
        
        vertexes[0].pos[0] = mPosRect.x;
        vertexes[0].pos[1] = mPosRect.y + mPosRect.w;
        vertexes[0].pos[2] = 0.0f;
        
        vertexes[1].pos[0] = mPosRect.x;
        vertexes[1].pos[1] = mPosRect.y;
        vertexes[1].pos[2] = 0.0f;
        
        vertexes[2].pos[0] = mPosRect.x + mPosRect.z;
        vertexes[2].pos[1] = mPosRect.y + mPosRect.w;
        vertexes[2].pos[2] = 0.0f;
        
        vertexes[3].pos[0] = mPosRect.x + mPosRect.z;
        vertexes[3].pos[1] = mPosRect.y;
        vertexes[3].pos[2] = 0.0f;
        
        //----------------------------------------
        
        vertexes[0].uv[0] = mUVRect.x;
        vertexes[0].uv[1] = mUVRect.y + mUVRect.w;
        
        vertexes[1].uv[0] = mUVRect.x;
        vertexes[1].uv[1] = mUVRect.y;
        
        vertexes[2].uv[0] = mUVRect.x + mUVRect.z;
        vertexes[2].uv[1] = mUVRect.y + mUVRect.w;
        
        vertexes[3].uv[0] = mUVRect.x + mUVRect.z;
        vertexes[3].uv[1] = mUVRect.y;
        
        //----------------------------------------
        
        vertexes[0].jindex[0] = 0;
        vertexes[0].jindex[1] = 0;
        vertexes[0].jindex[2] = 0;
        vertexes[0].jindex[3] = 0;
        
        vertexes[1].jindex[0] = 0;
        vertexes[1].jindex[1] = 0;
        vertexes[1].jindex[2] = 0;
        vertexes[1].jindex[3] = 0;
        
        vertexes[2].jindex[0] = 0;
        vertexes[2].jindex[1] = 0;
        vertexes[2].jindex[2] = 0;
        vertexes[2].jindex[3] = 0;
        
        vertexes[3].jindex[0] = 0;
        vertexes[3].jindex[1] = 0;
        vertexes[3].jindex[2] = 0;
        vertexes[3].jindex[3] = 0;
        
        //----------------------------------------
        
        vertexes[0].jweight[0] = 1;
        vertexes[0].jweight[1] = 0;
        vertexes[0].jweight[2] = 0;
        
        vertexes[1].jweight[0] = 1;
        vertexes[1].jweight[1] = 0;
        vertexes[1].jweight[2] = 0;
        
        vertexes[2].jweight[0] = 1;
        vertexes[2].jweight[1] = 0;
        vertexes[2].jweight[2] = 0;
        
        vertexes[3].jweight[0] = 1;
        vertexes[3].jweight[1] = 0;
        vertexes[3].jweight[2] = 0;

        
        //----------------------------------------
        
        vertexes[0].normal[0] = 0;
        vertexes[0].normal[1] = 0;
        vertexes[0].normal[2] = 1.0f;
        
        vertexes[1].normal[0] = 0;
        vertexes[1].normal[1] = 0;
        vertexes[1].normal[2] = 1.0f;
        
        vertexes[2].normal[0] = 0;
        vertexes[2].normal[1] = 0;
        vertexes[2].normal[2] = 1.0f;
        
        vertexes[3].normal[0] = 0;
        vertexes[3].normal[1] = 0;
        vertexes[3].normal[2] = 1.0f;
        
    }
};

iQuadMesh* CreateQuadMesh()
{
    return new QuadMesh();
}

NSPI_END()
