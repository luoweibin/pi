/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.6   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()


class MaterialProp : public iMaterialProp
{
private:
    string mName;
    string mUri;
    Var mValue;
    
public:
    MaterialProp()
    {
    }
    
    virtual ~MaterialProp()
    {
    }
    
    virtual std::string GetName() const
    {
        return mName;
    }
    
    virtual void SetName(const std::string& name)
    {
        mName = name;
    }
    
    virtual Var GetValue() const
    {
        return mValue;
    }
    
    virtual void SetValue(const Var& value)
    {
        mValue = value;
    }
    
    virtual std::string GetUri() const
    {
        return mUri;
    }
    
    virtual void SetUri(const std::string& uri)
    {
        mUri = uri;
    }
};


iMaterialProp* CreateMaterialProp()
{
    return new MaterialProp();
}


NSPI_END()













