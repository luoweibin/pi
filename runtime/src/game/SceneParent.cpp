/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.9   1     Create
 ******************************************************************************/
#include <pi/game/impl/ComponentImpl.h>

using namespace std;

NSPI_BEGIN()

class SceneParent : public ComponentImpl<iSceneParent>
{
private:
    string mEntityName;
    
public:
    SceneParent()
    {
    }
    
    virtual ~SceneParent()
    {
    }
    
    virtual void SetEntityName(const std::string& name)
    {
        piAssert(!name.empty(), ;);
        mEntityName = name;
    }
    
    virtual std::string GetEntityName() const
    {
        return mEntityName;
    }
};

iSceneParent* CreateSceneParent()
{
    return new SceneParent();
}



NSPI_END()






















