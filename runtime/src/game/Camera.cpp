/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2013.3.31   0.1     Create
 ******************************************************************************/
#include <pi/game/impl/CameraImpl.h>

using namespace std;

NSPI_BEGIN()

class Camera : public ComponentImpl<iCamera>
{
private:
    
    float       mNear;
    float       mFar;
    vec3        mUp;
    int         mMode;
    int         mDrawTag;
    bool        mActive;
    int         mCameraFollowType;
    float       mFov;
    int         mClearFlag;
    vec4        mClearColor;

    string      mRenderTargetUri;
    SmartPtr<iRenderTexture> mRenderTarget;
    int         mLastFBO;
public:
    Camera()
    : mActive(true)
    , mMode(eCameraMode_Ortho)
    , mDrawTag(eTag_Default)
    , mUp(0.0f, 1.0f, 0.0f)
    , mFov(60.0f)
    , mNear(1)
    , mFar(1000000)
    , mCameraFollowType(eCameraFollowType_Free)
    , mClearFlag(eClearFlag_Color)
    , mClearColor(0,0,0,1)
    , mLastFBO(0)
    {
    }
    
    virtual ~Camera()
    {
    }
    
    virtual void SetMode(int mode)
    {
        mMode = mode;
    }
    
    virtual int GetMode() const
    {
        return mMode;
    }

    virtual void SetDrawTag(int tag)
    {
        mDrawTag = tag;
    }
    
    virtual int GetDrawTag() const
    {
        return mDrawTag;
    }
    
    virtual void SetFov(float fovAngle)
    {
        mFov = fovAngle;
    }
    
    virtual float GetFov() const
    {
        return mFov;
    }
    
    virtual void SetNear(float value)
    {
        mNear = value;
    }
    
    virtual float GetNear() const
    {
        return mNear;
    }
    
    virtual void SetFar(float value)
    {
        mFar = value;
    }
    
    virtual float GetFar() const
    {
        return mFar;
    }
  
    virtual void SetUp(const vec3& up)
    {
        mUp = up;
    }
    
    virtual vec3 GetUp() const
    {
        return mUp;
    }
    
    virtual void SetActive(bool value)
    {
        mActive = value;
    }
    
    virtual bool IsActive() const
    {
        return mActive;
    }
    
    virtual std::string GetRenderTargetUri() const
    {
        return mRenderTargetUri;
    }
    
    virtual void SetRenderTargetUri(const std::string& uri)
    {
        mRenderTargetUri = uri;
    }
    
    virtual void SetRenderTarget(iRenderTexture* renderTarget)
    {
        mRenderTarget = renderTarget;
    }
    
    virtual iRenderTexture* GetRenderTarget() const
    {
        return mRenderTarget;
    }
    
    virtual void SetCameraFollowType(int type)
    {
        mCameraFollowType = type;
    }
    
    virtual int GetCameraFollowType() const
    {
        return mCameraFollowType;
    }
    
    virtual void SetClearFlag(int flag)
    {
        mClearFlag = flag;
    }
    
    virtual int GetClearFlag() const
    {
        return mClearFlag;
    }
    
    virtual void SetClearColor(const vec4& color)
    {
        mClearColor = color;
    }
    
    virtual vec4 GetClearColor() const
    {
        return mClearColor;
    }
    
    
    virtual void OnPrepareRender()
    {
        if (!mRenderTarget.IsNull())
        {
            int FBO = mRenderTarget->GetFBO();
            
            mLastFBO = piGetFramebuffer();
            piBindFramebuffer(eFramebuffer_DrawRead, FBO);
        }
        
        switch(GetClearFlag())
        {
            case eClearFlag_DepthOnly:
                piClear(eBufferBit_Depth|eBufferBit_Stencil);
                break;
            case eClearFlag_Color:
                piClearColor(mClearColor);
                piClear(eBufferBit_Color|eBufferBit_Depth|eBufferBit_Stencil);
                break;
        }
    }
    
    virtual void OnFinishRender()
    {
        if (!mRenderTarget.IsNull())
        {
            piBindFramebuffer(eFramebuffer_DrawRead, mLastFBO);
        }
    }
};

iCamera* CreateCamera()
{
    return new Camera();
}


NSPI_END()


















