/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.12.2   1.0     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()

class GameEntity : public iEntity
{
private:
    string mName;
    
    int mTag;
    
    bool mIsActive;
    
    SmartPtr<iComponentArray> mComps;
    SmartPtr<iClassArray> mCompClassArray;
    
    iScene* mScene;
    
    SmartPtr<iEntityArray> mChildren;
    
    iEntity* mParent;
    
public:
    GameEntity():
    mScene(nullptr)
    , mParent(nullptr)
    , mTag(eTag_Default)
    , mIsActive(true)
    {
        mComps = CreateComponentArray();
        mCompClassArray = CreateClassArray();
        mChildren = CreateEntityArray();
    }
    
    virtual ~GameEntity()
    {
    }
    
    virtual void SetScene(iScene* scene)
    {
        mScene = scene;
    }
    
    virtual iScene* GetScene() const
    {
        return mScene;
    }
    
    virtual void SetName(const string& name)
    {
        mName = name;
    }
    
    virtual string GetName() const
    {
        return mName;
    }
    
    virtual int GetTag() const
    {
        return mTag;
    }
    
    virtual void SetTag(int tag)
    {
        mTag = tag;
    }
    
    virtual bool IsActive() const
    {
        return mIsActive;
    }
    
    virtual void SetActive(bool flag)
    {
        mIsActive = flag;
    }
    
    virtual bool IsActiveRecursively() const
    {
        if (!mIsActive)
        {
            return false;
        }
        
        return mParent ? mParent->IsActiveRecursively() : true;
    }

    virtual iEntity* GetParent() const
    {
        return mParent;
    }
    
    virtual void SetParent(iEntity* parent)
    {
        piCheck(parent != this, ;);
        
        if (mParent != nullptr)
        {
            mParent->RemoveChild(this);
        }
        
        mParent = parent;
        mParent->AddChild(this);
    }
    
    // This will set parent point only, do not add to children array.
    virtual void SetParent_Only(iEntity* parent)
    {
        mParent = parent;
    }
    
    virtual iEntityArray* GetChildren() const
    {
        return mChildren;
    }
    
    virtual void SetChildren(iEntityArray* array)
    {
        piAssert(array, ;);
        
        int32_t count = array->GetCount();
        
        for (int32_t i = 0; i < count; ++i)
        {
            array->GetItem(i)->SetParent_Only(this);
        }
        mChildren = array;
    }
    
    virtual void AddChild( iEntity* entity )
    {
        piAssert(entity != nullptr, ;);
        
        piCheck(entity->GetParent() != this, ;);
        
        entity->SetParent(this);
        mChildren->PushBack(entity);
    }
    
    virtual void RemoveChild( iEntity* entity )
    {
        piCheck(entity->GetParent() == this, ;);

        int32_t count = mChildren->GetCount();
        for (int32_t i = 0; i < count; ++i)
        {
            if (mChildren->GetItem(i) == entity)
            {
                mChildren->Remove(i);
                break;
            }
        }
    }
    
    virtual void AddComp(iComponent* component)
    {
        piAssert(component != nullptr, ;);
        
        SmartPtr<iClass> klass = component->GetClass();
        
        if (klass != iTable::StaticClass())
        {
            
            auto index = piIndexOf<iComponentArray, iComponent*>(mComps,
                                                                 [klass](iComponent* c) {
                                                                     return c->GetClass() == klass;
                                                                 });
            piAssert(index < 0, ;);
        }
        mCompClassArray->PushBack(klass);
        
        component->SetEntity(this);
        
        mComps->PushBack(component);
    }
    
    virtual void RemoveComp(iComponent* component)
    {
        piAssert(component != nullptr, ;);
        
        auto index = piIndexOf<iComponentArray, iComponent*>(mComps, component);
        if (index >= 0)
        {
            mComps->Remove(index);
            component->SetEntity(nullptr);
            
            SmartPtr<iClass> klass = component->GetClass();
            if (klass != iTable::StaticClass())
            {
                auto classIndex = piIndexOf<iClassArray, iClass*>(mCompClassArray,
                                                                  [klass](iClass* c) {
                                                                      return c == klass;
                                                                  });
                piCheck(classIndex < 0, ;);
                
                mCompClassArray->Remove(classIndex);
            }
        }
    }
    
    virtual iComponent* GetCompByClassName(const std::string& className) const
    {
        piAssert(!className.empty(), nullptr);
        
        SmartPtr<iClass> klass = GetClass()->GetClassLoader()->FindClass(className);
        piCheck(!klass.IsNull(), nullptr);
        
        return GetCompByClass(klass);
    }
    
    virtual iComponentArray* GetCompsByClassName(const std::string& className) const
    {
        SmartPtr<iClass> klass = GetClass()->GetClassLoader()->FindClass(className);
        piCheck(!klass.IsNull(), nullptr);
        
        return GetCompsByClass(klass);
    }
    
    virtual iTable* FindDynamicComp(const std::string& name) const
    {
        piAssert(!name.empty(), nullptr);
        
        for (int32_t i = 0; i < mComps->GetCount(); ++i)
        {
            SmartPtr<iComponent> c = mComps->GetItem(i);
            if (!c->GetClass()->SubclassOf(iTable::StaticClass()))
            {
                continue;
            }
            
            SmartPtr<iTable> table = dynamic_cast<iTable*>(c.Ptr());
            
            Var type = table->Get("Type");
            if (type.GetString().compare(name) == 0)
                return table;
        }
        
        return nullptr;
    }
    
    virtual iComponent* GetCompByClass(iClass* klass) const
    {
        for (int32_t i = 0; i < mComps->GetCount(); ++i)
        {
            SmartPtr<iComponent> c = mComps->GetItem(i);

            if (klass->SubclassOf(c->GetClass()))
            {
                return c;
            }
        }
        
        return nullptr;
    }
    
    virtual iComponentArray* GetCompsByClass(iClass* klass) const
    {
        SmartPtr<iComponentArray> comps = CreateComponentArray();
        
        for (int32_t i = 0; i < mComps->GetCount(); ++i)
        {
            SmartPtr<iComponent> c = mComps->GetItem(i);
            if (c->GetClass()->SubclassOf(klass))
            {
                comps->PushBack(c);
            }
        }
        
        return comps.PtrAndSetNull();
    }
    
    virtual iComponentArray* GetComps() const
    {
        return mComps;
    }
    
    virtual iClassArray* GetCompClassArray() const
    {
        return mCompClassArray;
    }
    
    virtual mat4 EvaluateGlobalTransform()
    {
        mat4 matrix;
        
        SmartPtr<iSceneParent> sceneParent = piGetEntityComponent<iSceneParent>(this);
        if (!sceneParent.IsNull())
        {
            string name = sceneParent->GetEntityName();
            SmartPtr<iEntity> entity = mScene->FindEntity(name);
            if (!entity.IsNull())
            {
                matrix = entity->EvaluateGlobalTransform();
            }
        }
        
        SmartPtr<iTransform> local = piGetEntityComponent<iTransform>(this);
        if (!local.IsNull())
        {
            matrix *= local->GetMatrix();
        }
        
        // If use scene parent component, entity tree will not work.
        piCheck(sceneParent == nullptr, matrix;);
    
        
        iEntity* parentEnity = GetParent();
        
        while (parentEnity != nullptr)
        {
            SmartPtr<iTransform> parentTransform = piGetEntityComponent<iTransform>(parentEnity);
            if (!parentTransform.IsNull())
            {
                matrix *= parentTransform->GetMatrix();
            }
            parentEnity = parentEnity->GetParent();
        }
        
        return matrix;
    }
    
    virtual void OnLoad()
    {
        piEach<iComponentArray, iComponent*>(mComps,
                                             [](iComponent* c) {
                                                 c->OnLoad();
                                             });
    }
    
    virtual void OnUnload()
    {
        piEach<iComponentArray, iComponent*>(mComps,
                                             [](iComponent* c) {
                                                 c->OnLoad();
                                             });
    }
    
    virtual void OnResize(const rect& bounds)
    {
        piEach<iComponentArray, iComponent*>(mComps,
                                             [bounds](iComponent* c) {
                                                 c->OnResize(bounds);
                                             });
    }
    
    virtual bool OnMessage(iMessage* message)
    {
        for (int32_t i = 0; i < mComps->GetCount(); ++i)
        {
            SmartPtr<iComponent> c = mComps->GetItem(i);
            if (c->OnMessage(message))
            {
                return true;
            }
        }
        
        return false;
    }
    
    virtual bool OnHIDEvent(iHIDEvent* event)
    {
        for (int32_t i = 0; i < mComps->GetCount(); ++i)
        {
            SmartPtr<iComponent> c = mComps->GetItem(i);
            if (c->OnHIDEvent(event))
            {
                return true;
            }
        }
        
        return false;
    }
    
    virtual void OnUpdate(float delta)
    {
        piEach<iComponentArray, iComponent*>(mComps,
                                             [delta](iComponent* c) {
                                                 if (c->IsEnabled())
                                                 {
                                                     c->OnUpdate(delta);
                                                 }
                                             });
    }
};


iEntity* CreateEntity()
{
    return new GameEntity();
}


NSPI_END()





















