/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 Sword     2017.7.13   1     Create
 ******************************************************************************/
#include <pi/game/impl/ComponentImpl.h>

#include <pi/lua/LuaScript.h>

using namespace std;

NSPI_BEGIN()
class ScriptComp : public ComponentImpl<iScriptComp>
{

private:
    SmartPtr<iScriptRef> mRef;
    string mUri;
    
public:
    ScriptComp()
    {
    }
    
    virtual ~ScriptComp()
    {
    }
    
    virtual void SetUri(const string& uri)
    {
        mUri = uri;
    }
    
    virtual string GetUri() const
    {
        return mUri;
    }
    
    virtual void OnResize(const rect& bounds)
    {
        piCheck(!mRef.IsNull(), ;);
        
        mRef->Call("OnResize", this, bounds);
    }
    
    virtual void OnUpdate(float delta)
    {
        piCheck(!mRef.IsNull(), ;);
        mRef->Call("OnUpdate", this, delta);
    }
    
    virtual bool OnHIDEvent(iHIDEvent* event)
    {
        piCheck(!mRef.IsNull(), false);
        return mRef->Call("OnHIDEvent", this, event);
    }
    
    virtual bool OnMessage(iMessage* message)
    {
        piCheck(!mRef.IsNull(), false);
        return mRef->Call("OnMessage", this, message);
    }
    
    virtual void OnLoad()
    {
        mRef = LoadScript(mUri);
        
        piCheck(!mRef.IsNull(), ;);
        
        mRef->Call("OnLoad", this);
    }
    
    virtual void OnUnload()
    {
        piCheck(!mRef.IsNull(), ;);
        mRef->Call("OnUnload", this);
    }
    
    virtual void OnEnabledChanged(bool value)
    {
        piCheck(!mRef.IsNull(), ;);
        mRef->Call("OnEnabledChanged", this, value);
    }
    
protected:
    iScriptRef* LoadScript(const string& uri)
    {
        SmartPtr<iEntity> entity = GetEntity();
        piAssert(!entity.IsNull(), nullptr);
        
        SmartPtr<iScene> scene = entity->GetScene();
        piAssert(!scene.IsNull(), nullptr);
        
        SmartPtr<iScript> script = scene->GetScript();
        piAssert(!script.IsNull(), nullptr);
        
        return script->EvaluateFile(uri);
    }
};

iScriptComp* CreateScriptComp()
{
    return new ScriptComp;
}

NSPI_END()

















