/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.13   0.1     Create
 ******************************************************************************/
#include <pi/game/impl/SystemImpl.h>

#include <pi/game/particle/ParticleShader.h>

using namespace std;

NSPI_BEGIN()

size_t maxParticleNum = 10000;


class ParticleSystem : public SystemImpl<iParticleSystem>
{
private:
    float mDelta;
    
    SmartPtr<iCamera>       mCamera;
    SmartPtr<iTransform>    mCameraTrans;

    
    int32_t mProgram;
    
    
    
    
    void LoadShader()
    {        
        mProgram = piCreateProgram();
        piCompileProgram(mProgram, g_Particle_VS_GLES2, g_Particle_FS_GLES2);
        
        piBindVertexAttr(mProgram, 0, "position");
        piBindVertexAttr(mProgram, 1, "uv");
        piBindVertexAttr(mProgram, 2, "jointIndices"); // reserve
        piBindVertexAttr(mProgram, 3, "jointWeights"); // reserve
        piBindVertexAttr(mProgram, 4, "normal");
        
        piLinkProgram(mProgram);
    }
    
    void UnloadShader()
    {
        piReleaseGraphicsObject(mProgram);
    }
public:
    ParticleSystem()
    : mDelta(0.0f)
    , mProgram(0)
    {
        
    }
    
    virtual ~ParticleSystem()
    {
    }
    
    virtual void OnLoad()
    {
        SmartPtr<iEntity> cameraEntity = GetScene()->GetFirstActiveCameraEntity();
        
        if (cameraEntity.IsNull())
            return;
        
        
        
        mCamera = piGetEntityComponent<iCamera>(cameraEntity);
        
        mCameraTrans = piGetEntityComponent<iTransform>(cameraEntity);
        
        LoadShader();

    }
    
    virtual void OnUnload()
    {
        UnloadShader();
    }
    
    virtual void OnUpdate(float delta)
    {
        piCheck(!mEntities->IsEmpty(), ;);
        
        mDelta = delta;
        
        
        piPushGroupMarker("ParticleSystem");
        
        piUseProgram(mProgram);
        
        mat4 matCamera;// = mCameraTrans->EvaluateMatrix(); // Some bug in this function.
        
        mat4 translation = piglm::translate(mCameraTrans->GetTranslation());
        
        quat orient = piglm::angleAxis(mCameraTrans->GetOrientation().x, vec3(1, 0, 0));
        orient = piglm::angleAxis(mCameraTrans->GetOrientation().y, vec3(0, 1, 0))*orient;
        orient = piglm::angleAxis(mCameraTrans->GetOrientation().z, vec3(0, 0, 1))*orient;
        
        mat4 scale = piglm::scale(mCameraTrans->GetScale());
        
        matCamera = translation * piglm::toMat4(orient) * scale;
        
        
        mat4 matCameraInv = piglm::inverse(matCamera);
        
        mat4 Proj;
        
        rect rt = GetScene()->GetBounds();
        
        float ratio = rt.width / rt.height;
        float screenXSize = ratio/2;
        float screenYSize = 0.5f;
        
        switch (mCamera->GetMode())
        {
            case eCameraMode_Ortho:
                {
                    Proj = piglm::ortho(-screenXSize,screenXSize,-screenYSize,screenYSize,mCamera->GetNear(), mCamera->GetFar());
                }
                break;
            case eCameraMode_Persp:
                {
                    Proj = piglm::frustum(-screenXSize,screenXSize,-screenYSize,screenYSize,mCamera->GetNear(), mCamera->GetFar());
                }
                break;
        }
        
        piUniformMat4f(mProgram, "MVPMatrix", Proj*matCameraInv);
        
        piEach<iEntityArray, iEntity*>(mEntities,
                                       [this](iEntity* e)
                                       {
                                           
                                           
                                           OnUpdateEntity(e, mDelta);
                                       });

        
        piUseProgram(0);
        
        piPopGroupMarker();
    }
    
    virtual bool Accept(iEntity* entity)
    {
        auto index = piIndexOf<iComponentArray, iComponent*>(entity->GetComps(),
                                                             [entity](iComponent* c)
                                                             {
                                                                 SmartPtr<iParticleEmitter> p = dynamic_cast<iParticleEmitter*>(c);
                                                                 
                                                                 if (!p.IsNull())
                                                                 {
                                                                     p->Init();
                                                                 }
                                                                 return !p.IsNull();
                                                             });
        
        
        return index >= 0;
    }
    
private:
    void OnLoadEntityRendererInfo(iEntity* entity)
    {
    }
    
    void OnUnloadEntityRendererInfo(iEntity* entity)
    {
    }
    
    void OnUpdateEntity(iEntity* entity, float detla)
    {
        
        auto func = [this, detla](iComponent* c)
        {
            
            
            SmartPtr<iParticleEmitter> emitter = dynamic_cast<iParticleEmitter*>(c);
            piCheck(!emitter.IsNull(), ;);
            
            emitter->OnUpdate(detla);
            
            emitter->DoRender(mCamera, mCameraTrans);
            
            
        };
        
        piEach<iComponentArray, iComponent*>(entity->GetComps(), func);
    }
};

iParticleSystem* CreateParticleSystem()
{
    return new ParticleSystem();
}

NSPI_END()
