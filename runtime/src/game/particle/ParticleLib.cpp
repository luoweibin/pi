/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 Sword 2017.5.16   0.1     Create
 ******************************************************************************/

#include <pi/Game.h>

#include "../../asset/AssetImpl.h"
#include <pi/game/impl/ParticleImpl.h>

NSPI_BEGIN()

class ParticleEmitterLib : public AssetImpl<iParticleEmitterLib>
{
private:
    
    int     mParticleType;
    int     mParticlePlayType;
    int     mParticleBlend;
    
    float   mEmissionRate; // The number of emission particles per second.
    
    int     mMaxNum;
    
    float   mWidth;
    float   mHeight;
    
    std::string mTextureUri;
    
    SmartPtr<iTexture2D> mTexture2D;
    
    int     mFrameSizeX;
    int     mFrameSizeY;
    int     mFrameCount;
    
    float   mFrameDuration;
    
    int     mBillboardType;
    
    vec3    mBornBasePosition;
    bool    mPositionOffsetAbs;
    vec3    mBornPositionMinOffset;
    vec3    mBornPositionMaxOffset;
    
    vec3    mBornBaseVelocity;
    bool    mVelocityOffsetAbs;
    vec3    mBornVelocityMinOffset;
    vec3    mBornVelocityMaxOffset;
    
    vec3    mBaseAcceleration;
    bool    mAccelerationOffsetAbs;
    vec3    mAccelerationMinOffset;
    vec3    mAccelerationMaxOffset;
    
    vec3    mBornBaseRotation;
    bool    mRotationOffsetAbs;
    vec3    mBornRotationMinOffset;
    vec3    mBornRotationMaxOffset;
    
    vec3    mBornBaseRotationRate;
    bool    mRotationRateOffsetAbs;
    vec3    mBornRotationRateMinOffset;
    vec3    mBornRotationRateMaxOffset;
    
    vec3    mBaseRotationalAcceleration;
    bool    mRotationalAccelerationOffsetAbs;
    vec3    mRotationalAccelerationMinOffset;
    vec3    mRotationalAccelerationMaxOffset;
    
    float   mBaseLife;
    float   mLifeMinOffset;
    float   mLifeMaxOffset;
    
    
public:
    ParticleEmitterLib()
    
    : mParticleType(eParticleType_Quad)
    , mParticlePlayType(eParticlePlayType_ByFrame)
    , mParticleBlend(eParticleBlend_Add)
    
    , mEmissionRate(0.0f)
    
    , mMaxNum(100)
    
    , mWidth(0.0f)
    , mHeight(0.0f)
    
    , mBillboardType(eBillboardType_Y)
    
    , mPositionOffsetAbs(false)
    , mVelocityOffsetAbs(false)
    , mAccelerationOffsetAbs(false)
    
    , mRotationOffsetAbs(false)
    , mRotationRateOffsetAbs(false)
    , mRotationalAccelerationOffsetAbs(false)
    
    , mFrameSizeX(0)
    , mFrameSizeY(0)
    , mFrameCount(0)
    
    , mFrameDuration(0.0f)
    
    , mBaseLife(0.0f)
    , mLifeMinOffset(0.0f)
    , mLifeMaxOffset(0.0f)
    
    {
    }
    
    virtual ~ParticleEmitterLib()
    {

    }
    
    virtual void SetParticleType( int type)
    {
        mParticleType = type;
    }
    
    virtual int GetParticleType() const
    {
        return mParticleType;
    }
    
    virtual void SetParticlePlayType(int type)
    {
        mParticlePlayType = type;
    }
    
    virtual int GetParticlePlayType() const
    {
        return mParticlePlayType;
    }
    
    virtual void SetParticleBlend(int type)
    {
        mParticleBlend = type;
    }
    
    virtual int GetParticleBlend() const
    {
        return mParticleBlend;
    }
    
    virtual void SetEmissionRate(float rate)
    {
        mEmissionRate = rate;
    }
    
    virtual float GetEmissionRate() const
    {
        return mEmissionRate;
    }
    
    virtual void SetMaxNum(int MaxNum)
    {
        mMaxNum = MaxNum;
    }
    
    virtual int GetMaxNum() const
    {
        return mMaxNum;
    }
    
    virtual void SetWidth(float width)
    {
        mWidth = width;
    }
    
    virtual float GetWidth() const
    {
        return mWidth;
    }
    
    virtual void SetHeight(float height)
    {
        mHeight = height;
    }
    
    virtual float GetHeight() const
    {
        return mHeight;
    }
    
    virtual void SetTextureUri(const std::string& uri)
    {
        mTextureUri = uri;
    }
    
    virtual std::string GetTextureUri() const
    {
        return mTextureUri;
    }
    
    virtual void SetTexture2D(iTexture2D* tex)
    {
        mTexture2D = tex;
    }
    
    virtual iTexture2D* GetTexture2D() const
    {
        return mTexture2D;
    }
    
    virtual void SetFrameSizeX(int pitch)
    {
        mFrameSizeX = pitch;
    }
    
    virtual int GetFrameSizeX() const
    {
        return mFrameSizeX;
    }
    
    virtual void SetFrameSizeY(int pitch)
    {
        mFrameSizeY = pitch;
    }
    
    virtual int GetFrameSizeY() const
    {
        return mFrameSizeY;
    }
    
    virtual void SetFrameCount(int count)
    {
        mFrameCount = count;
    }
    
    virtual int GetFrameCount() const
    {
        return mFrameCount;
    }
    
    virtual void SetFrameDuration(float duration)
    {
        mFrameDuration = duration;
    }
    
    virtual float GetFrameDuration() const
    {
        return mFrameDuration;
    }
    
    virtual void SetBillboardType(int type)
    {
        mBillboardType = type;
    }
    
    virtual int  GetBillboardType() const
    {
        return mBillboardType;
    }
    
    virtual void SetBornBasePosition(const vec3& pos)
    {
        mBornBasePosition = pos;
    }
    
    virtual vec3 GetBornBasePosition() const
    {
        return mBornBasePosition;
    }
    
    virtual void SetPositionOffsetAbs(bool flag)
    {
        mPositionOffsetAbs = flag;
    }
    
    virtual bool IsPositionOffsetAbs() const
    {
        return mPositionOffsetAbs;
    }
    
    virtual void SetBornPositionMinOffset(const vec3& pos)
    {
        mBornPositionMinOffset = pos;
    }
    
    virtual vec3 GetBornPositionMinOffset() const
    {
        return mBornPositionMinOffset;
    }
    
    virtual void SetBornPositionMaxOffset(const vec3& pos)
    {
        mBornPositionMaxOffset = pos;
    }
    
    virtual vec3 GetBornPositionMaxOffset() const
    {
        return mBornPositionMaxOffset;
    }
    
    virtual void SetBornBaseVelocity(const vec3& v)
    {
        mBornBaseVelocity = v;
    }
    
    virtual vec3 GetBornBaseVelocity() const
    {
        return mBornBaseVelocity;
    }
    
    virtual void SetVelocityOffsetAbs(bool flag)
    {
        mVelocityOffsetAbs = flag;
    }
    
    virtual bool IsVelocityOffsetAbs() const
    {
        return mVelocityOffsetAbs;
    }
    
    virtual void SetBornVelocityMinOffset(const vec3& v)
    {
        mBornVelocityMinOffset = v;
    }
    
    virtual vec3 GetBornVelocityMinOffset() const
    {
        return mBornVelocityMinOffset;
    }
    
    virtual void SetBornVelocityMaxOffset(const vec3& v)
    {
        mBornVelocityMaxOffset = v;
    }
    
    virtual vec3 GetBornVelocityMaxOffset() const
    {
        return mBornVelocityMaxOffset;
    }
    
    virtual void SetBaseAcceleration(const vec3& acc)
    {
        mBaseAcceleration = acc;
    }
    
    virtual vec3 GetBaseAcceleration() const
    {
        return mBaseAcceleration;
    }
    
    virtual void SetAccelerationOffsetAbs(bool flag)
    {
        mAccelerationOffsetAbs = flag;
    }
    
    virtual bool IsAccelerationOffsetAbs() const
    {
        return mAccelerationOffsetAbs;
    }
    
    virtual void SetAccelerationMinOffset(const vec3& acc)
    {
        mAccelerationMinOffset = acc;
    }
    
    virtual vec3 GetAccelerationMinOffset() const
    {
        return mAccelerationMinOffset;
    }
    
    virtual void SetAccelerationMaxOffset(const vec3& acc)
    {
        mAccelerationMaxOffset = acc;
    }
    
    virtual vec3 GetAccelerationMaxOffset() const
    {
        return mAccelerationMaxOffset;
    }
    
    virtual void SetBornBaseRotation(const vec3& rotation)
    {
        mBornBaseRotation = rotation;
    }
    
    virtual vec3 GetBornBaseRotation() const
    {
        return mBornBaseRotation;
    }
    
    virtual void SetRotationOffsetAbs(bool flag)
    {
        mRotationOffsetAbs = flag;
    }
    
    virtual bool IsRotationOffsetAbs() const
    {
        return mRotationOffsetAbs;
    }
    
    virtual void SetBornRotationMinOffset(const vec3& rotation)
    {
        mBornRotationMinOffset = rotation;
    }
    
    virtual vec3 GetBornRotationMinOffset() const
    {
        return mBornRotationMinOffset;
    }
    
    virtual void SetBornRotationMaxOffset(const vec3& rotation)
    {
        mBornRotationMaxOffset = rotation;
    }
    
    virtual vec3 GetBornRotationMaxOffset() const
    {
        return mBornRotationMaxOffset;
    }
    
    virtual void SetBornBaseRotationRate(const vec3& rotationRate)
    {
        mBornBaseRotationRate = rotationRate;
    }
    
    virtual vec3 GetBornBaseRotationRate() const
    {
        return mBornBaseRotationRate;
    }
    
    virtual void SetRotationRateOffsetAbs(bool flag)
    {
        mRotationRateOffsetAbs = flag;
    }
    
    virtual bool IsRotationRateOffsetAbs() const
    {
        return mRotationRateOffsetAbs;
    }
    
    virtual void SetBornRotationRateMinOffset(const vec3& rotationRate)
    {
        mBornRotationRateMinOffset = rotationRate;
    }
    
    virtual vec3 GetBornRotationRateMinOffset() const
    {
        return mBornRotationRateMinOffset;
    }
    
    virtual void SetBornRotationRateMaxOffset(const vec3& rotationRate)
    {
        mBornRotationRateMaxOffset = rotationRate;
    }
    
    virtual vec3 GetBornRotationRateMaxOffset() const
    {
        return mBornRotationRateMaxOffset;
    }
    
    virtual void SetBaseRotationalAcceleration(const vec3& rotationalAcceleration)
    {
        mBaseRotationalAcceleration = rotationalAcceleration;
    }
    
    virtual vec3 GetBaseRotationalAcceleration() const
    {
        return mBaseRotationalAcceleration;
    }
    
    virtual void SetRotationalAccelerationOffsetAbs(bool flag)
    {
        mRotationalAccelerationOffsetAbs = flag;
    }
    
    virtual bool IsRotationalAccelerationOffsetAbs() const
    {
        return mRotationalAccelerationOffsetAbs;
    }
    
    virtual void SetRotationalAccelerationMinOffset(const vec3& rotationalAcceleration)
    {
        mRotationalAccelerationMinOffset = rotationalAcceleration;
    }
    
    virtual vec3 GetRotationalAccelerationMinOffset() const
    {
        return mRotationalAccelerationMinOffset;
    }
    
    virtual void SetRotationalAccelerationMaxOffset(const vec3& rotationalAcceleration)
    {
        mRotationalAccelerationMaxOffset = rotationalAcceleration;
    }
    
    virtual vec3 GetRotationalAccelerationMaxOffset() const
    {
        return mRotationalAccelerationMaxOffset;
    }
    
    virtual void SetBaseLife(float sec)
    {
        mBaseLife = sec;
    }
    
    virtual float GetBaseLife() const
    {
        return mBaseLife;
    }
    
    virtual void SetLifeMinOffset(float sec)
    {
        mLifeMinOffset = sec;
    }
    
    virtual float GetLifeMinOffset() const
    {
        return mLifeMinOffset;
    }
    
    virtual void SetLifeMaxOffset(float sec)
    {
        mLifeMaxOffset = sec;
    }
    
    virtual float GetLifeMaxOffset() const
    {
        return mLifeMaxOffset;
    }
    
    virtual bool Init()
    {
        CheckOffsetValue();
        
        BuildMaterial();
        
        return true;
    }
    
private:
    void CheckAndSwap(float& min, float& max)
    {
        float temp;
        if (min > max)
        {
            temp = min;
            min = max;
            max = temp;
        }
    }
    
    void CheckAndSwap(vec3& min, vec3& max)
    {
        CheckAndSwap(min.x,max.x);
        CheckAndSwap(min.y,max.y);
        CheckAndSwap(min.z,max.z);
    }
    
    void CheckOffsetValue()
    {
        CheckAndSwap(mLifeMinOffset,mLifeMaxOffset);
        
        CheckAndSwap(mBornPositionMinOffset,mBornPositionMaxOffset);
        CheckAndSwap(mBornVelocityMinOffset,mBornVelocityMaxOffset);
        CheckAndSwap(mAccelerationMinOffset,mAccelerationMaxOffset);
        
    }
    
    void BuildMaterial()
    {
        
    }
};

iParticleEmitterLib* CreateParticleEmitterLib()
{
    return new ParticleEmitterLib();
}

class QuadParticle : public ParticleImpl<iQuadParticle>
{
private:
    int     mFrameNum;
    float   mDuration;
    
public:
    virtual ~QuadParticle()
    {
        //PILOGI(PI_GAME_TAG, "~QuadParticle");
    }
    
    virtual int GetFrameNum() const
    {
        return mFrameNum;
    }
    
    virtual void SetFrameNum(int frameNum)
    {
        mFrameNum = frameNum;
    }
    
    virtual float GetDuration() const
    {
        return mDuration;
    }
    
    virtual void SetDuration(float duration)
    {
        mDuration = duration;
    }
};

iQuadParticle* CreateQuadParticle()
{
    return new QuadParticle();
}


NSPI_END()
