/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 Sword 2017.5.16   0.1     Create
 ******************************************************************************/

#include <pi/Game.h>
#include <pi/game/impl/ComponentImpl.h>
#include "../../asset/serialzation/JsonUtil.h"


using namespace std;

NSPI_BEGIN()

struct RenderParticleInfo
{
    SmartPtr<iParticle> particle;
    float Distance;
};

//static int MakeType(int32_t count)
//{
//    if (count <= std::numeric_limits<uint8_t>::max())
//    {
//        return eType_U8;
//    }
//    else if (count <= std::numeric_limits<uint16_t>::max())
//    {
//        return eType_U16;
//    }
//    else
//    {
//        return eType_U32;
//    }
//}

class ParticleEmitter : public ComponentImpl<iParticleEmitter>
{
private:
    float mPassTime;
    
    std::string mUri;
    
    SmartPtr<iParticleEmitterLib> mLib;
    
    vector<SmartPtr<iParticle>> mParticleTable;
    
    vector<SmartPtr<iParticle>> mRecycleParticle; // Base by ParticleTable
    
    vector<SmartPtr<iMemory>> mVertexBufferList;
    
    int32_t mVAO;
    int32_t mVBO;
    int32_t mIBO;
    
    int mCurParticleNum;
    
    static int32_t CreateIBO(int32_t particleCount)
    {
        SmartPtr<iMemory> data;
        
        vector<uint32_t> buffer(particleCount*6);
        
        size_t bufIndex = 0;
        for (auto i = 0; i < particleCount; ++i)
        {
            buffer[bufIndex++] = 4*i + 0;
            buffer[bufIndex++] = 4*i + 1;
            buffer[bufIndex++] = 4*i + 2;
            buffer[bufIndex++] = 4*i + 1;
            buffer[bufIndex++] = 4*i + 3;
            buffer[bufIndex++] = 4*i + 2;
        }
        
        data = CreateMemoryCopy(buffer.data(), buffer.size() * sizeof(uint32_t));
        
        int32_t ibo = piCreateBuffer();
        piBindBuffer(eGraphicsBuffer_Index, ibo);
        piBufferData(eGraphicsBuffer_Index, ibo, data->Size(), data);
        piBindBuffer(eGraphicsBuffer_Index, 0);
        
        return ibo;
    }
    
    static int CreateVBO( int32_t particleCount)
    {
        SmartPtr<iMemory> data;
        
        data = CreateMemory(particleCount * 4 * sizeof(MeshVertex));
        
        memset( data->Ptr(), 0, (size_t)data->Size());
        
        int32_t vbo = piCreateBuffer();
        piBindBuffer(eGraphicsBuffer_Vertex, vbo);
        piBufferData(eGraphicsBuffer_Vertex, vbo, data->Size(), data);
        piBindBuffer(eGraphicsBuffer_Vertex, 0);
        
        return vbo;
    }
    
    float RandFloat(float base,float minOffset,float maxOffset, bool IsAbs)
    {
        if (IsAbs)
        {
            float isMinus = rand()%2? 1.0f : -1.0f;
            return base + isMinus * abs(minOffset + (maxOffset - minOffset) * (rand()%100000)/100000.0f);
        }
        else
            return base + minOffset + (maxOffset - minOffset) * (rand()%100000)/100000.0f;
    }
    
    vec3 RandVec3(const vec3& base,const vec3& minOffset,const vec3& maxOffset, bool IsAbs)
    {
        vec3 rt;
        rt.x = RandFloat(base.x,minOffset.x,maxOffset.x, IsAbs);
        rt.y = RandFloat(base.y,minOffset.y,maxOffset.y, IsAbs);
        rt.z = RandFloat(base.z,minOffset.z,maxOffset.z, IsAbs);
        
        return rt;
    }
    
    void NewParticle()
    {
        SmartPtr<iParticle> particle;
        
        if (mRecycleParticle.empty())
        {
            switch(mLib->GetParticleType())
            {
                case eParticleType_Quad:
                    particle = CreateQuadParticle();
                    break;
            }
            
            mParticleTable.push_back(particle);
        }
        else
        {
            particle = mRecycleParticle[mRecycleParticle.size() - 1];
            mRecycleParticle.pop_back();
            
            piAssert(!particle.IsNull(), ;);
        }
        
        if (particle.IsNull())
            return;
        
        InitParticleData(particle);
        ++mCurParticleNum;
    }
    
    void InitParticleData(iParticle* particle)
    {
        if (NULL == particle)
            return;
        
        particle->SetAlive(true);
        
        particle->SetLifeTime( RandFloat(mLib->GetBaseLife(), mLib->GetLifeMinOffset(), mLib->GetLifeMaxOffset(), false));
        particle->SetPassTime(0.0f);
        
        particle->SetPosition( RandVec3( mLib->GetBornBasePosition(), mLib->GetBornPositionMinOffset(), mLib->GetBornPositionMaxOffset(), mLib->IsPositionOffsetAbs() ) );
        
        particle->SetVelocity( RandVec3( mLib->GetBornBaseVelocity(), mLib->GetBornVelocityMinOffset(), mLib->GetBornVelocityMaxOffset(), mLib->IsVelocityOffsetAbs() ) );
        
        particle->SetAcceleration( RandVec3( mLib->GetBaseAcceleration(), mLib->GetAccelerationMinOffset(), mLib->GetAccelerationMaxOffset(), mLib->IsAccelerationOffsetAbs() ) );
        
        particle->SetRotation( RandVec3( mLib->GetBornBaseRotation(), mLib->GetBornRotationMinOffset(), mLib->GetBornRotationMaxOffset(), mLib->IsRotationOffsetAbs() ) );
        
        particle->SetRotationRate( RandVec3( mLib->GetBornBaseRotationRate(), mLib->GetBornRotationRateMinOffset(), mLib->GetBornRotationRateMaxOffset(), mLib->IsRotationRateOffsetAbs() ) );
        
        particle->SetRotationalAcceleration( RandVec3( mLib->GetBaseRotationalAcceleration(), mLib->GetRotationalAccelerationMinOffset(), mLib->GetRotationalAccelerationMaxOffset(), mLib->IsRotationalAccelerationOffsetAbs() ) );
        
        switch (mLib->GetParticleType())
        {
            case eParticleType_Quad:
                InitQuadParticleData(particle);
                break;
        }
    }
    
    void InitQuadParticleData(iParticle* particle)
    {
        iQuadParticle* quadParticle = dynamic_cast<iQuadParticle*>(particle);
        
        if (NULL == quadParticle)
            return;
        
        quadParticle->SetFrameNum(0);
        quadParticle->SetDuration(mLib->GetFrameDuration());
        
        if (mLib->GetParticlePlayType() == eParticlePlayType_ByFrame)
        {
            quadParticle->SetLifeTime( mLib->GetFrameCount()*quadParticle->GetDuration());
        }
    }
    
    void RecyclePartice(iParticle* particle)
    {
        particle->SetAlive(false);
        mRecycleParticle.push_back(particle);
        --mCurParticleNum;
    }
    
    void UpdateParticeInfo(float detla, iParticle* particle)
    {
        if (NULL == particle)
            return;
        
        vec3 position = particle->GetPosition() + particle->GetVelocity()*detla + 0.5f * particle->GetAcceleration() * detla * detla;
 
        vec3 velocity = particle->GetVelocity() + detla * particle->GetAcceleration();
        
        particle->SetPosition(position);
        particle->SetVelocity(velocity);
        
        vec3 rotation = particle->GetRotation() + particle->GetRotationRate()*detla + 0.5f * particle->GetRotationalAcceleration() * detla * detla;
        
        vec3 rotationRate = particle->GetRotationRate() + detla * particle->GetRotationalAcceleration();
        
        particle->SetRotation(rotation);
        particle->SetRotationRate(rotationRate);
        
        particle->SetPassTime( particle->GetPassTime() + detla );
        
        UpdateQuadParticleData(particle);
        
        if (particle->GetLifeTime() > 0.0f && particle->GetPassTime() >= particle->GetLifeTime())
        {
            RecyclePartice(particle);
        }
    }
    
    void UpdateQuadParticleData(iParticle* particle)
    {
        iQuadParticle* quadParticle = dynamic_cast<iQuadParticle*>(particle);
        
        if (NULL == quadParticle)
            return;
        
        int FrameNum = (int)(quadParticle->GetPassTime() / quadParticle->GetDuration());
        
        quadParticle->SetFrameNum( FrameNum % mLib->GetFrameCount() );
    }
    
    iMemory* GetUnusedVertexBuffer(size_t bufferSize)
    {
        SmartPtr<iMemory> unusedBuffer;
        for (size_t i = 0; i < mVertexBufferList.size(); ++i)
        {
            if (mVertexBufferList[i]->GetRefCount() > 1)
                continue;
            
            unusedBuffer = mVertexBufferList[i];
            break;
        }
        
        if (unusedBuffer.IsNull())
        {
            unusedBuffer = CreateMemory( mLib->GetMaxNum()*4*sizeof(MeshVertex) );
            mVertexBufferList.push_back(unusedBuffer);
            
            piAssert(bufferSize <= unusedBuffer->Size(), nullptr);
        }
        
        return unusedBuffer.PtrAndSetNull();
    }
    
    iMemory* BuildVertexTable( iCamera* camera, iTransform* camTransform,  int& Size)
    {
        Size = 0;
        
        SmartPtr<iMemory> pMemory;
        
        mat4 matCamera = camTransform->GetMatrix();
        
        mat4 matCameraInv = piglm::inverse(matCamera);
        
        std::list< RenderParticleInfo > renderobjectList;
        
        // Sort particles from far to near the camera.
        for (size_t i = 0; i < mParticleTable.size(); ++i)
        {
            iParticle* particle = mParticleTable[i];
            if (!particle->IsAlive())
                continue;
            
            vec3 position = particle->GetPosition();
            
            
            vec4 v4pos;
            v4pos.x = position.x;
            v4pos.y = position.y;
            v4pos.z = position.z;
            
            v4pos = matCameraInv * v4pos;
            
            position = vec3(v4pos);
            
            // cull the particle after the camera simplely.
//            if (position.z > 0)
//                continue;
            
            RenderParticleInfo info;
            
            info.particle = particle;
            info.Distance = abs(position.z);
            
            bool hasInsert = false;
            
            auto Iter = renderobjectList.begin();
            
            for(;Iter != renderobjectList.end(); ++Iter)
            {
                if (info.Distance > Iter->Distance)
                {
                    renderobjectList.insert( Iter, info);
                    hasInsert = true;
                    break;
                }
                
            }
            
//            // Test
//            std::list<int> listL;
//            
//            listL.push_back(4);
//            listL.push_back(2);
//            
//            int t = 2;
//            std::list<int>::iterator iter = listL.begin();
//            for(;iter != listL.end(); ++iter)
//            {
//                if (t > *iter)
//                {
//                    listL.insert( iter, t);
//                    break;
//                }
//            }
//            
//            t = 3;
//            iter = listL.begin();
//            for(;iter != listL.end(); ++iter)
//            {
//                if (t > *iter)
//                {
//                    listL.insert( iter, t);
//                    break;
//                }
//            }
//            
//            t = 5;
//            iter = listL.begin();
//            for(;iter != listL.end(); ++iter)
//            {
//                if (t > *iter)
//                {
//                    listL.insert( iter, t);
//                    break;
//                }
//            }
//            
//            iter = listL.begin();
//            for(;iter != listL.end(); ++iter)
//            {
//                PILOGI("test","%d,",*iter);
//            }
            

            
            
            
            if (!hasInsert)
            {
                renderobjectList.push_back(info);
            }
            
        }
        
        if (renderobjectList.size() <= 0)
            return pMemory.PtrAndSetNull();
        
        switch (mLib->GetParticleType() )
        {
            case eParticleType_Mesh:
                break;
            case eParticleType_Quad:
            default:
            {
                size_t VertexBufSize = renderobjectList.size()*4* sizeof(MeshVertex);
                pMemory = GetUnusedVertexBuffer(VertexBufSize);
                
                if (VertexBufSize > pMemory->Size())
                {
                    pMemory->Resize(VertexBufSize);
                }
                
                MeshVertex* vertexes = (MeshVertex*)pMemory->Ptr();
                
                switch(mLib->GetBillboardType())
                {
                    case eBillboardType_All:
                        BuildBillboardAllQuadArray(vertexes,renderobjectList, camTransform, Size);
                        break;
                    case eBillboardType_None:
                        BuildBillboardNoneQuadArray(vertexes,renderobjectList, camTransform, Size);
                        break;
                    case eBillboardType_Y:
                    default:
                        BuildBillboardYQuadArray(vertexes,renderobjectList, camTransform, Size);
                        break;
                }
                
                
            }
                break;
        }
        
        return pMemory.PtrAndSetNull();
    }
    
/*
0 - 1
| / |
2 - 3
*/
#define BUILDBILLBOARDQUAD() \
    /*----------- point 1 -----------------*/\
    positionTemp = matRotation * vec4( -mLib->GetWidth()*0.5f,mLib->GetHeight()*0.5f,0.0f, 1.0f);\
    vertexes[Size].pos[0] = positionTemp.x + center.x;\
    vertexes[Size].pos[1] = positionTemp.y + center.y;\
    vertexes[Size].pos[2] = positionTemp.z + center.z;\
    \
    vertexes[Size].uv[0] = frameX*quadUVSize.x;\
    vertexes[Size].uv[1] = 1.0f - (frameY)*quadUVSize.y;\
    \
    memset( vertexes[Size].jindex, 0, sizeof(vertexes[Size].jindex));\
    memset( vertexes[Size].jweight, 0, sizeof(vertexes[Size].jweight));\
    \
    vertexes[Size].normal[0] = 0.0f;\
    vertexes[Size].normal[1] = 0.0f;\
    vertexes[Size].normal[2] = -1.0f;\
    \
    ++Size;\
    \
    /*----------- point 1 -----------------*/\
    positionTemp = matRotation * vec4( mLib->GetWidth()*0.5f,mLib->GetHeight()*0.5f,0.0f, 1.0f);\
    \
    vertexes[Size].pos[0] = positionTemp.x + center.x;\
    vertexes[Size].pos[1] = positionTemp.y + center.y;\
    vertexes[Size].pos[2] = positionTemp.z + center.z;\
    \
    vertexes[Size].uv[0] = (frameX+1)*quadUVSize.x;\
    vertexes[Size].uv[1] = 1.0f - (frameY)*quadUVSize.y;\
    \
    memset( vertexes[Size].jindex, 0, sizeof(vertexes[Size].jindex));\
    memset( vertexes[Size].jweight, 0, sizeof(vertexes[Size].jweight));\
    \
    vertexes[Size].normal[0] = 0.0f;\
    vertexes[Size].normal[1] = 0.0f;\
    vertexes[Size].normal[2] = -1.0f;\
    \
    ++Size;\
    \
    /*----------- point 2 -----------------*/\
    positionTemp = matRotation * vec4( -mLib->GetWidth()*0.5f,-mLib->GetHeight()*0.5f,0.0f, 1.0f);\
    \
    vertexes[Size].pos[0] = positionTemp.x + center.x;\
    vertexes[Size].pos[1] = positionTemp.y + center.y;\
    vertexes[Size].pos[2] = positionTemp.z + center.z;\
    \
    vertexes[Size].uv[0] = frameX*quadUVSize.x;\
    vertexes[Size].uv[1] = 1.0f - (frameY+1)*quadUVSize.y;\
    \
    memset( vertexes[Size].jindex, 0, sizeof(vertexes[Size].jindex));\
    memset( vertexes[Size].jweight, 0, sizeof(vertexes[Size].jweight));\
    \
    vertexes[Size].normal[0] = 0.0f;\
    vertexes[Size].normal[1] = 0.0f;\
    vertexes[Size].normal[2] = -1.0f;\
    \
    ++Size;\
    \
    /*----------- point 3 -----------------*/\
    positionTemp = matRotation * vec4( mLib->GetWidth()*0.5f,-mLib->GetHeight()*0.5f,0.0f, 1.0f);\
    \
    vertexes[Size].pos[0] = positionTemp.x + center.x;\
    vertexes[Size].pos[1] = positionTemp.y + center.y;\
    vertexes[Size].pos[2] = positionTemp.z + center.z;\
    \
    vertexes[Size].uv[0] = (frameX+1)*quadUVSize.x;\
    vertexes[Size].uv[1] = 1.0f - (frameY+1)*quadUVSize.y;\
    memset( vertexes[Size].jindex, 0, sizeof(vertexes[Size].jindex));\
    memset( vertexes[Size].jweight, 0, sizeof(vertexes[Size].jweight));\
    vertexes[Size].normal[0] = 0.0f;\
    vertexes[Size].normal[1] = 0.0f;\
    vertexes[Size].normal[2] = -1.0f;\
    \
    ++Size;
    
#define BUILDBILLBOARDQUAD_REVERTV() \
    /*----------- point 1 -----------------*/\
    positionTemp = matRotation * vec4( -mLib->GetWidth()*0.5f,mLib->GetHeight()*0.5f,0.0f, 1.0f);\
    vertexes[Size].pos[0] = positionTemp.x + center.x;\
    vertexes[Size].pos[1] = positionTemp.y + center.y;\
    vertexes[Size].pos[2] = positionTemp.z + center.z;\
    \
    vertexes[Size].uv[0] = frameX*quadUVSize.x;\
    vertexes[Size].uv[1] = 1.0f - (frameY+1)*quadUVSize.y;\
    \
    memset( vertexes[Size].jindex, 0, sizeof(vertexes[Size].jindex));\
    memset( vertexes[Size].jweight, 0, sizeof(vertexes[Size].jweight));\
    \
    vertexes[Size].normal[0] = 0.0f;\
    vertexes[Size].normal[1] = 0.0f;\
    vertexes[Size].normal[2] = -1.0f;\
    \
    ++Size;\
    \
    /*----------- point 1 -----------------*/\
    positionTemp = matRotation * vec4( mLib->GetWidth()*0.5f,mLib->GetHeight()*0.5f,0.0f, 1.0f);\
    \
    vertexes[Size].pos[0] = positionTemp.x + center.x;\
    vertexes[Size].pos[1] = positionTemp.y + center.y;\
    vertexes[Size].pos[2] = positionTemp.z + center.z;\
    \
    vertexes[Size].uv[0] = (frameX+1)*quadUVSize.x;\
    vertexes[Size].uv[1] = 1.0f - (frameY+1)*quadUVSize.y;\
    \
    memset( vertexes[Size].jindex, 0, sizeof(vertexes[Size].jindex));\
    memset( vertexes[Size].jweight, 0, sizeof(vertexes[Size].jweight));\
    \
    vertexes[Size].normal[0] = 0.0f;\
    vertexes[Size].normal[1] = 0.0f;\
    vertexes[Size].normal[2] = -1.0f;\
    \
    ++Size;\
    \
    /*----------- point 2 -----------------*/\
    positionTemp = matRotation * vec4( -mLib->GetWidth()*0.5f,-mLib->GetHeight()*0.5f,0.0f, 1.0f);\
    \
    vertexes[Size].pos[0] = positionTemp.x + center.x;\
    vertexes[Size].pos[1] = positionTemp.y + center.y;\
    vertexes[Size].pos[2] = positionTemp.z + center.z;\
    \
    vertexes[Size].uv[0] = frameX*quadUVSize.x;\
    vertexes[Size].uv[1] = 1.0f - (frameY)*quadUVSize.y;\
    \
    memset( vertexes[Size].jindex, 0, sizeof(vertexes[Size].jindex));\
    memset( vertexes[Size].jweight, 0, sizeof(vertexes[Size].jweight));\
    \
    vertexes[Size].normal[0] = 0.0f;\
    vertexes[Size].normal[1] = 0.0f;\
    vertexes[Size].normal[2] = -1.0f;\
    \
    ++Size;\
    \
    /*----------- point 3 -----------------*/\
    positionTemp = matRotation * vec4( mLib->GetWidth()*0.5f,-mLib->GetHeight()*0.5f,0.0f, 1.0f);\
    \
    vertexes[Size].pos[0] = positionTemp.x + center.x;\
    vertexes[Size].pos[1] = positionTemp.y + center.y;\
    vertexes[Size].pos[2] = positionTemp.z + center.z;\
    \
    vertexes[Size].uv[0] = (frameX+1)*quadUVSize.x;\
    vertexes[Size].uv[1] = 1.0f - (frameY)*quadUVSize.y;\
    memset( vertexes[Size].jindex, 0, sizeof(vertexes[Size].jindex));\
    memset( vertexes[Size].jweight, 0, sizeof(vertexes[Size].jweight));\
    vertexes[Size].normal[0] = 0.0f;\
    vertexes[Size].normal[1] = 0.0f;\
    vertexes[Size].normal[2] = -1.0f;\
    \
    ++Size;

    
    
    void BuildBillboardYQuadArray(MeshVertex* vertexes, std::list< RenderParticleInfo >& renderobjectList, iTransform* camTransform,int& Size)
    {
        vec2 quadUVSize;
        quadUVSize.x = (float)mLib->GetFrameSizeX()/mLib->GetTexture2D()->GetWidth();
        quadUVSize.y = (float)mLib->GetFrameSizeY()/mLib->GetTexture2D()->GetHeight();
        
        vec4 positionTemp;
        
        int PitchX = mLib->GetTexture2D()->GetWidth()/mLib->GetFrameSizeX();
        
        // Buile Vertex List
        auto Iter = renderobjectList.begin();
        for(;Iter != renderobjectList.end();++Iter)
        {
            SmartPtr<iQuadParticle> quadParticle = dynamic_cast<iQuadParticle*>(Iter->particle.Ptr());
            
            if (quadParticle.IsNull())
                continue;
            
            int frameX = quadParticle->GetFrameNum() % PitchX;
            int frameY = quadParticle->GetFrameNum() / PitchX;
            
            vec3 center = quadParticle->GetPosition();
            
            vec3 camPos = camTransform->GetTranslation();
            
            mat4 matRotation;
            
            vec3 forward;
            forward.x = (center.x - camPos.x);
            forward.z = (center.z - camPos.z);
            forward = piglm::normalize(forward);
            
            matRotation[2] = vec4(forward,1.0f);
            vec3 right = piglm::normalize(piglm::cross( forward, vec3(0,1,0) ));
            matRotation[1] = vec4(vec3(0,1,0),1.0f);
            matRotation[0] = vec4(right,1.0f);
            
            vec3 rotation = quadParticle->GetRotation();
            
            quat quatRotation = piglm::angleAxis(rotation.x, vec3(1,0,0));
            quatRotation = piglm::angleAxis(rotation.y, vec3(0,1,0)) * quatRotation;
            quatRotation = piglm::angleAxis(rotation.z, vec3(0,0,1)) * quatRotation;
            
            matRotation *= piglm::toMat4(quatRotation);
            
            BUILDBILLBOARDQUAD();
        }
    }
    
    void BuildBillboardAllQuadArray(MeshVertex* vertexes, std::list< RenderParticleInfo >& renderobjectList, iTransform* camTransform, int& Size)
    {
        vec2 quadUVSize;
        quadUVSize.x = (float)mLib->GetFrameSizeX()/mLib->GetTexture2D()->GetWidth();
        quadUVSize.y = (float)mLib->GetFrameSizeY()/mLib->GetTexture2D()->GetHeight();
        
        vec4 positionTemp;
        
        int PitchX = mLib->GetTexture2D()->GetWidth()/mLib->GetFrameSizeX();
        
        // Buile Vertex List
        auto Iter = renderobjectList.begin();
        for(;Iter != renderobjectList.end();++Iter)
        {
            SmartPtr<iQuadParticle> quadParticle = dynamic_cast<iQuadParticle*>(Iter->particle.Ptr());
            
            if (quadParticle.IsNull())
                continue;
            
            int frameX = quadParticle->GetFrameNum() % PitchX;
            int frameY = quadParticle->GetFrameNum() / PitchX;
            
            vec3 center = Iter->particle->GetPosition();
            
            vec3 camPos = camTransform->GetTranslation();
            
            mat4 matRotation;
            
            vec3 forward = piglm::normalize(center - camPos);
            matRotation[2] = vec4(forward,1.0f);
            vec3 right = piglm::cross( forward, vec3(0,1,0) );
            matRotation[0] = vec4(right,1.0f);
            //matRotation[1] = vec4(piglm::cross( forward, right ),1.0f);
            
            vec3 rotation = quadParticle->GetRotation();
            
            quat quatRotation = piglm::angleAxis(rotation.x, vec3(1,0,0));
            quatRotation = piglm::angleAxis(rotation.y, vec3(0,1,0)) * quatRotation;
            quatRotation = piglm::angleAxis(rotation.z, vec3(0,0,1)) * quatRotation;
            
            matRotation *= piglm::toMat4(quatRotation);
            
            BUILDBILLBOARDQUAD();
        }
    }
  
    void BuildBillboardNoneQuadArray(MeshVertex* vertexes, std::list< RenderParticleInfo >& renderobjectList, iTransform* camTransform,int& Size)
    {
        vec2 quadUVSize;
        quadUVSize.x = (float)mLib->GetFrameSizeX()/mLib->GetTexture2D()->GetWidth();
        quadUVSize.y = (float)mLib->GetFrameSizeY()/mLib->GetTexture2D()->GetHeight();
        
        vec4 positionTemp;
        
        int PitchX = mLib->GetTexture2D()->GetWidth()/mLib->GetFrameSizeX();
        
        // Buile Vertex List
        auto Iter = renderobjectList.begin();
        for(;Iter != renderobjectList.end();++Iter)
        {
            SmartPtr<iQuadParticle> quadParticle = dynamic_cast<iQuadParticle*>(Iter->particle.Ptr());
            
            if (quadParticle.IsNull())
                continue;
            
            int frameX = quadParticle->GetFrameNum() % PitchX;
            int frameY = quadParticle->GetFrameNum() / PitchX;
            
            vec3 center = Iter->particle->GetPosition();
            
            vec3 rotation = quadParticle->GetRotation();
            
            quat quatRotation = piglm::angleAxis(rotation.x, vec3(1,0,0));
            quatRotation = piglm::angleAxis(rotation.y, vec3(0,1,0)) * quatRotation;
            quatRotation = piglm::angleAxis(rotation.z, vec3(0,0,1)) * quatRotation;
            
            mat4 matRotation = piglm::toMat4(quatRotation);
            
            BUILDBILLBOARDQUAD();
        }
    }
    
#undef BUILDBILLBOARDQUAD
#undef BUILDBILLBOARDQUAD_REVERTV
    
public:
    ParticleEmitter()
    : mCurParticleNum(0)
    , mPassTime(0)
    
    , mVAO(0)
    , mVBO(0)
    , mIBO(0)
    {
    }
    
    virtual ~ParticleEmitter()
    {
        mRecycleParticle.clear();
        mParticleTable.clear();
        
        piReleaseGraphicsObject(mVAO);
        
        piReleaseGraphicsObject(mVBO);
        
        piReleaseGraphicsObject(mIBO);
    }
    
    void SetUri(const std::string& uri)
    {
        mUri = uri;
    }
    
    virtual std::string GetUri() const
    {
        return mUri;
    }
    
    virtual void SetLibrary(iParticleEmitterLib* lib)
    {
        mLib = lib;
        
        mParticleTable.reserve(mLib->GetEmissionRate()*(mLib->GetBaseLife() + abs(mLib->GetLifeMaxOffset())));
        
        mRecycleParticle.reserve(mParticleTable.size());
    }
    
    virtual iParticleEmitterLib* GetLibrary() const
    {
        return mLib;
    }
    
    virtual void Init()
    {
        if (!mLib.IsNull())
            mLib->Init();
    
        mVAO = piCreateVertexArray();
        mVBO = CreateVBO( mLib->GetMaxNum());
        mIBO = CreateIBO( mLib->GetMaxNum() );
        
        piBindVertexArray(mVAO);
        
        piBindBuffer(eGraphicsBuffer_Vertex, mVBO);
        piBindBuffer(eGraphicsBuffer_Index, mIBO);
        
        piEnableVertexAttr(0);
        piEnableVertexAttr(1);
        piEnableVertexAttr(2);
        piEnableVertexAttr(3);
        piEnableVertexAttr(4);
        
        piVertexAttr(0, 3, eType_F32, sizeof(MeshVertex), offsetof(MeshVertex, pos));
        piVertexAttr(1, 2, eType_F32, sizeof(MeshVertex), offsetof(MeshVertex, uv));
        piVertexAttr(2, 4, eType_F32, sizeof(MeshVertex), offsetof(MeshVertex, jindex));
        piVertexAttr(3, 3, eType_F32, sizeof(MeshVertex), offsetof(MeshVertex, jweight));
        piVertexAttr(4, 3, eType_F32, sizeof(MeshVertex), offsetof(MeshVertex, normal));
        
        piBindVertexArray(0);
        piBindBuffer(eGraphicsBuffer_Vertex, 0);
        piBindBuffer(eGraphicsBuffer_Index, 0);
        
        
        
        piBindVertexArray(0);
    }
    
    virtual void OnUpdate(float delta)
    {
        for (size_t i = 0; i < mParticleTable.size(); ++i)
        {
            if (!mParticleTable[i]->IsAlive())
                continue;
            
            UpdateParticeInfo( delta, mParticleTable[i]);
        }
        
        mPassTime += delta;
        
        float EmissionTime = 1.0f / mLib->GetEmissionRate();
        
        while (mPassTime > EmissionTime)
        {
            if ( mCurParticleNum >= mLib->GetMaxNum())
                break;
            NewParticle();
            mPassTime -= EmissionTime;
        }
    }

    virtual void DoRender( iCamera* camera, iTransform* camTransform)
    {
        int VertexSize = 0;
        SmartPtr<iMemory> vertexes = BuildVertexTable(camera,camTransform,VertexSize);
        
        if (vertexes.IsNull())
            return;
        
        piBindBuffer(eGraphicsBuffer_Vertex, mVBO);
        piBufferSubData(eGraphicsBuffer_Vertex, 0, VertexSize*sizeof(MeshVertex), vertexes);
        piBindBuffer(eGraphicsBuffer_Vertex, 0);
        
        piBindVertexArray(mVAO);
        
        switch (mLib->GetParticleBlend())
        {
            case eParticleBlend_Add:
                piBlendFunc(eBlendFunc_SrcAlpha, eBlendFunc_One);
                break;
            case eParticleBlend_Blend:
                piBlendFunc(eBlendFunc_SrcAlpha, eBlendFunc_OneMinusSrcAlpha);
                break;
        }

        
        
        int enableFeatures = eGraphicsFeature_Blend | eGraphicsFeature_DepthTest;
        piEnableFeatures(enableFeatures);
        piDepthMask(false);
        piCullFace(eFace_Both);
        
        piActiveTexture(0);
        piBindTexture(eTexTarget_2D, GetLibrary()->GetTexture2D()->GetGraphicsName());
        
        piDrawElements( eGraphicsDraw_Triangles, VertexSize/4*6, eType_U32);
  
        piBindTexture(eTexTarget_2D, 0);
        
        piBindVertexArray(0);
        piCullFace(eFace_Front);
        piDepthMask(true);
        piDisableFeatures(enableFeatures);
        piBlendFunc(eBlendFunc_SrcAlpha, eBlendFunc_OneMinusSrcAlpha);
        
    }
    
    
};

iParticleEmitter* CreateParticleEmitter()
{
    return new ParticleEmitter();
}


NSPI_END()
