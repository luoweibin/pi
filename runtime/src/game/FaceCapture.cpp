/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.17   1.0     Create
 ******************************************************************************/
#include <pi/game/impl/ComponentImpl.h>

using namespace std;

NSPI_BEGIN()

class FaceCapture : public ComponentImpl<iFaceCapture>
{
private:
    int32_t mFaceID;
    bool    mIsOrth;
    
public:
    FaceCapture()
    : mFaceID(-1)
    , mIsOrth(false)
    {
    }
    
    virtual ~FaceCapture()
    {
    }
    
    virtual int32_t GetFaceID() const
    {
        return mFaceID;
    }
    
    virtual void SetFaceID(int32_t id)
    {
        mFaceID = id;
    }
    
    virtual bool IsOrth() const
    {
        return mIsOrth;
    }
    
    virtual void SetOrth(bool flag)
    {
        mIsOrth = flag;
    }
};


iFaceCapture* CreateFaceCapture()
{
    return new FaceCapture();
}




class FaceClone : public ComponentImpl<iFaceClone>
{
private:
    int32_t mFrom;
    int32_t mTo;
    string mMaskUri;
    
public:
    FaceClone():
    mFrom(0), mTo(0)
    {
    }
    
    virtual ~FaceClone()
    {
    }
    
    virtual int32_t GetFrom() const
    {
        return mFrom;
    }
    
    virtual void SetFrom(int32_t id)
    {
        mFrom = id;
    }
    
    virtual int32_t GetTo() const
    {
        return mTo;
    }
    
    virtual void SetTo(int32_t id)
    {
        mTo = id;
    }
    
    virtual std::string GetMaskUri() const
    {
        return mMaskUri;
    }
    
    virtual void SetMaskUri(const std::string& uri)
    {
        mMaskUri = uri;
    }
};

iFaceClone* CreateFaceClone()
{
    return new FaceClone();
}


NSPI_END()
















