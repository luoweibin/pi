/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin 2017.3.27   1.0     Create
 ******************************************************************************/
#include <pi/game/impl/ComponentImpl.h>

using namespace std;

NSPI_BEGIN()


class Material2D : public ComponentImpl<iMaterial2D>
{
private:
    vec4 mColor;
    string mScript;
    string mColorTexUri;
    string mMaskTexUri;
    mat4 mTexMatrix;
    
    SmartPtr<iGraphicsObject> mColorTex;
    SmartPtr<iGraphicsObject> mMaskTex;
    
    mutable int32_t mIndex;

public:
    Material2D():
    mIndex(-1)
    {
    }
    
    virtual ~Material2D()
    {
    }
    
    virtual void SetTexTransform(const mat4& matrix)
    {
        mTexMatrix = matrix;
    }
    
    virtual mat4 GetTexTransform() const
    {
        return mTexMatrix;
    }
    
    virtual string GetColorTexUri() const
    {
        return mColorTexUri;
    }
    
    virtual void SetColorTexUri(const string& uri)
    {
        mColorTexUri = uri;
    }
    
    virtual string GetMaskTexUri() const
    {
        return mMaskTexUri;
    }
    
    virtual void SetMaskTexUri(const string& uri)
    {
        mMaskTexUri = uri;
    }
    
    virtual vec4 GetColor() const
    {
        return mColor;
    }
    
    virtual void SetColor(const vec4& color)
    {
        mColor = color;
    }
    
    virtual iGraphicsObject* GetColorTex() const
    {
        SmartPtr<iEntity> entity = GetEntity();
        if (!entity.IsNull() && !mColorTex.IsNull())
        {
            SmartPtr<iFrameAnim2D> frame2d = piGetEntityComponent<iFrameAnim2D>(entity);
            if(!frame2d.IsNull() && frame2d->GetLibrary() && !frame2d->GetLibrary()->IsCache())
            {
                if(mIndex != frame2d->GetFrame())
                {
                    mIndex = frame2d->GetFrame();

                    SmartPtr<iScene> scene = GetEntity()->GetScene();
                    SmartPtr<iAssetManager> manager =  scene->GetAssetManager();
                    SmartPtr<iClassLoader> classLoader =  scene->GetClassLoader();
                    string uri = frame2d->GetLibrary()->GetFrames()->GetItem(mIndex)->GetColorTexUri();
                    SmartPtr<iBitmapAsset> bitmapAsset = piLoadAsset<iBitmapAsset>(manager, classLoader, uri, false);
                    SmartPtr<iBitmap>  bitmap = bitmapAsset->GetBitmap();
                    piBindTexture(eTexTarget_2D, mColorTex->GetName());
                    piTexSubImage2D(eTexTarget_2D, 0, 0, 0, bitmap->GetWidth(), bitmap->GetHeight(), bitmap, 0);
                    piBindTexture(eTexTarget_2D, 0);
                }
            }
        }
        
        return mColorTex;
    }
    
    virtual void SetColorTex(iGraphicsObject* texture)
    {
        mColorTex = texture;
    }
    
    virtual iGraphicsObject* GetMaskTex() const
    {
        return mMaskTex;
    }
    
    virtual void SetMaskTex(iGraphicsObject* texture)
    {
        mMaskTex = texture;
    }
    
    virtual std::string GetScript() const
    {
        return mScript;
    }
    
    virtual void SetScript(const std::string& uri)
    {
        mScript = uri;
    }
};

iMaterial2D* CreateMaterial2D()
{
    return new Material2D();
}

NSPI_END()















