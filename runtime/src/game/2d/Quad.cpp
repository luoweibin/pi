/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 zhoujiaqi 2017.3.16   1.0     Create
 ******************************************************************************/
#include <pi/game/impl/ComponentImpl.h>

using namespace std;

NSPI_BEGIN()

class Quad : public ComponentImpl<iQuad>
{
private:
    string mUri;
    
public:
    Quad()
    {
    }
    
    virtual ~Quad()
    {
    }
    
    virtual string GetUri() const
    {
        return mUri;
    }
    
    virtual void SetUri(const string& uri)
    {
        mUri = uri;
    }
};

iQuad* CreateQuad()
{
    return new Quad();
}


NSPI_END()














