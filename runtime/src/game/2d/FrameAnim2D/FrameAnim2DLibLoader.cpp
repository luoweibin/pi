/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 archie zhou    2017.3.21   1.0     Create
 ******************************************************************************/
#include <pi/Game.h>
#include "../../../asset/serialzation/JsonUtil.h"

using namespace std;

NSPI_BEGIN()

class FrameAnim2DLoader: public iAssetLoader
{
public:
    FrameAnim2DLoader()
    {
    }
    
    ~FrameAnim2DLoader()
    {
    }
    
    virtual iAsset* Load(iAssetManager* manager, iClassLoader* classLoader, iStream* stream)
    {
        json v = ReadJsonFromStream(stream);
        piAssert(v.is_object(), nullptr);
        piCheck(v.find("Frames") != v.end(), nullptr);
        
        SmartPtr<iFrameAnim2DLib> lib = CreateFrameAnim2DLib();
        SmartPtr<iMaterial2DArray> frameArray = lib->GetFrames();
        
        lib->SetFPS(v["FPS"].get<int32_t>());
        lib->SetCache(v["Cache"].get<bool>());
        
        string uri = stream->GetUri();
        string dir = piGetDirectory(uri);
        
        bool need_frush_data = true;
        
        const auto& jsonFrames = v["Frames"];
        for (auto& jsonFrame : jsonFrames)
        {
            SmartPtr<iMaterial2D> frame2D = CreateMaterial2D();
            SmartPtr<iGraphicsObject> object = CreateGraphicsObject();
            string path = dir + jsonFrame.get<string>();
            static int32_t name = 0;
        
            if (need_frush_data)
            {
                SmartPtr<iBitmapAsset> bmpAsset = piLoadAsset<iBitmapAsset>(manager, classLoader, path, lib->IsCache());
                piAssert(!bmpAsset.IsNull(), nullptr);
                SmartPtr<iBitmap> bmp = bmpAsset->GetBitmap();
                piAssert(!bmp.IsNull(), nullptr);
                
                name = piCreateTexture();
                piBindTexture(eTexTarget_2D, name);
                piTexImage2D(eTexTarget_2D, 0, bmp->GetPixelFormat()->GetName(), bmp, 0);
                piTexParam(eTexTarget_2D, eTexConfig_WrapS, eTexValue_Clamp);
                piTexParam(eTexTarget_2D, eTexConfig_WrapT, eTexValue_Clamp);
                piTexParam(eTexTarget_2D, eTexConfig_MinFilter, eTexValue_Linear);
                piTexParam(eTexTarget_2D, eTexConfig_MagFilter, eTexValue_Linear);
                if(!lib->IsCache() && name)
                {
                    need_frush_data = false;
                }
            }

            object->SetName(name);
            frame2D->SetColorTex(object);
            frame2D->SetColorTexUri(path);
            frameArray->PushBack(frame2D);

            piReleaseGraphicsObject(name);
        }
        
        return lib.PtrAndSetNull();
    }
};

class FrameAnim2DLoaderFactory : public iAssetLoaderFactory
{
private:
    SmartPtr<iAssetLoader> mLoader;
    
public:
    FrameAnim2DLoaderFactory()
    {
        mLoader = new FrameAnim2DLoader();
    }
    
    virtual ~FrameAnim2DLoaderFactory()
    {
    }
    
    virtual iAssetLoader* CreateLoader(const std::string& fileExt, iStream* stream)
    {
        return mLoader;
    }
};


iAssetLoaderFactory* CreateFrameAnim2DLoaderFactory()
{
    return new FrameAnim2DLoaderFactory();
}


NSPI_END()






















