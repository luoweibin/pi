/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.24    1.0    Create
 ******************************************************************************/
#include <pi/Game.h>
#include "../../../asset/AssetImpl.h"
#include "../../../asset/serialzation/JsonUtil.h"

using namespace std;


NSPI_BEGIN()

class FrameAnim2DLib : public AssetImpl<iFrameAnim2DLib>
{
private:
    bool mCache;
    int32_t mFPS;
    SmartPtr<iMaterial2DArray> mFrames;
    
public:
    FrameAnim2DLib()
    {
        mFrames = CreateMaterial2DArray();
    }
    
    virtual ~FrameAnim2DLib()
    {
    }
    
    virtual int32_t GetFPS() const
    {
        return mFPS;
    }

    virtual void SetFPS(int32_t fps)
    {
        mFPS = fps;
    }

    virtual iMaterial2DArray* GetFrames() const
    {
        return mFrames;
    }
    
    virtual void SetFrames(iMaterial2DArray* frames)
    {
        piAssert(frames != nullptr, ;);
        mFrames = frames;
    }
    
    virtual bool IsCache() const
    {
        return mCache;
    }
    
    virtual void SetCache(bool value)
    {
        mCache = value;
    }
};

iFrameAnim2DLib* CreateFrameAnim2DLib()
{
    return new FrameAnim2DLib();
}



NSPI_END()























