/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.23   1     Create
 ******************************************************************************/
#include <pi/game/impl/ComponentImpl.h>

using namespace std;

NSPI_BEGIN()


class FrameAnim2D : public ComponentImpl<iFrameAnim2D>
{
private:
    int32_t mFrame;
    string mUri;
    int32_t mFPS;
    SmartPtr<iFrameAnim2DLib> mLib;
    bool mCache;
    
    SmartPtr<iModelAnim> mModelAnim;
    
    SmartPtr<iAnimProp> mAnimProp;
    SmartPtr<iAnimProp> mMatProp;
    
public:
    FrameAnim2D():
    mFrame(0), mFPS(30)
    {
    }
    
    virtual ~FrameAnim2D()
    {
    }
    
    virtual int32_t GetFrame() const
    {
        return mFrame;
    }
    
    virtual void SetFrame(int32_t index)
    {
        piAssert(index >= 0, ;);
        mFrame = index;
        
        if (!mMatProp.IsNull())
        {
            SmartPtr<iMaterial2D> frame = mLib->GetFrames()->GetItem(index);
            if (!frame.IsNull())
            {
                mMatProp->SetValue(frame->GetColorTex());
            }
        }
    }
    
    virtual iAnimProp* GetProperty(const std::string& uri) const
    {
        return mAnimProp;
    }
    
    virtual void SetUri(const string& uri)
    {
        mUri = uri;
    }
    
    virtual string GetUri() const
    {
        return mUri;
    }
    
    virtual int32_t GetFPS() const
    {
        return mFPS;
    }
    
    virtual void SetFPS(int32_t value)
    {
        piAssert(value > 0, ;);
        mFPS = value;
    }
    
    virtual void SetLibrary(iFrameAnim2DLib* lib)
    {
        piAssert(lib != nullptr, ;);
        mLib = lib;
    }
    
    virtual iFrameAnim2DLib* GetLibrary() const
    {
        return mLib;
    }
    
    virtual void OnUpdate()
    {
    }
    
    virtual iModelAnim* GetAnimation() const
    {
        return mModelAnim;
    }
    
    virtual void OnUpdate(float delta)
    {
        if (mModelAnim.IsNull())
        {
            SmartPtr<iMaterial2D> material = piGetEntityComponent<iMaterial2D>(GetEntity());
            if (!material.IsNull())
            {
                SmartPtr<iProperty> prop = iMaterial2D::StaticClass()->GetProperty("ColorTex");
                mMatProp = CreateAnimProp(material, prop);
            }
            
            SmartPtr<iMaterial2DArray> frameArray = mLib->GetFrames();
            piAssert(!frameArray.IsNull(), ;);
            
            SmartPtr<iAnimCurveKey> key0 = CreateAnimCurveKey();
            key0->SetTime(0);
            key0->SetValue(0);
            
            SmartPtr<iAnimCurveKey> key1 = CreateAnimCurveKey();
            key1->SetTime((frameArray->GetCount() - 1) * (1.0f / mLib->GetFPS() * 1000));
            key1->SetValue(frameArray->GetCount() - 1);
            
            SmartPtr<iAnimChannel> channel = CreateAnimChannel();
            channel->SetTarget(this);
            channel->SetProperty(GetClass()->GetProperty("Frame"));
            
            SmartPtr<iAnimCurveKeyArray> keys = channel->GetKeys();
            keys->PushBack(key0);
            keys->PushBack(key1);
            
            SmartPtr<iAnimLayer> layer = CreateAnimLayer();
            layer->GetChannels()->PushBack(channel);
            
            mModelAnim = CreateModelAnim();
            mModelAnim->GetLayers()->PushBack(layer);
        }
        
        mModelAnim->OnUpdate(delta);
        
    }
};

iFrameAnim2D* CreateFrameAnim2D()
{
    return new FrameAnim2D();
}





NSPI_END()

















