/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 Sword     2017.7.5   0.1     Create
 *******************************************************************************/

#include <pi/Game.h>
#include <pi/game/impl/SystemImpl.h>

using namespace std;

NSPI_BEGIN()

class CaptureBackRemovalSystem : public SystemImpl<iCaptureBackRemovalSystem>
{
    
    virtual bool Accept( iEntity* entity )
    {
        SmartPtr<iUIMesh> uiMesh =  piGetEntityComponent<iUIMesh>(entity);
        if( uiMesh != nullptr)
        {
            return entity->FindDynamicComp("@BackRemoval") != nullptr;
        }
    
        return false;
    }
    
    virtual void OnUpdate( float delta)
    {
        SmartPtr<iEntityArray> entities = GetEntities();
    
        SmartPtr<iScene> scene = GetScene();
        if ( scene == nullptr)
        {
            return;
        }
    
    
        vec2 DesignResolution = scene->GetDesignSize();
    
        SmartPtr<iHID> HID = scene->GetHID();
        piCheck(!HID.IsNull(), ;);
        
        SmartPtr<iFaceTrackerResult> faceTrackerResult =  HID->GetFaceTrackerResult();
        piCheck(!faceTrackerResult.IsNull(), ;);
    
    
        if (faceTrackerResult->GetFaceCount() <= 0 )
        {
            return;
        }
    
    
        SmartPtr<iFaceInfo> faceInfo = faceTrackerResult->GetFace(0);
    
        rect rt = faceInfo->GetRect();
    
        float XExternScale = 0.4;
        float YExternScale = 1.5;
    
        rect uvRect = rect(rt.x- rt.width * XExternScale*0.5, 1.0 - rt.y, rt.width*(1+XExternScale), -rt.height*(1+YExternScale));
    
        float widthDivHeight = uvRect.width / -uvRect.height * DesignResolution.x/DesignResolution.y;
    
    
        for( int i = 0; i < entities->GetCount(); ++i)
        {
            SmartPtr<iEntity> entity = entities->GetItem(i);
    
            SmartPtr<iUIMesh> uiMesh = piGetEntityComponent<iUIMesh>(entity);
    
            SmartPtr<iTable> backRemoval = entity->FindDynamicComp("@BackRemoval");
    
            bool IsRotatorCapture = false;
            bool IsFixedUV        = false;
            bool IsFixedSize      = false;
            bool IsBaseByX        = false;
    
            if (!backRemoval.IsNull())
            {
                IsRotatorCapture    = backRemoval->Get("IsRotatorCapture");
                IsFixedUV           = backRemoval->Get("IsFixedUV");
                IsFixedSize         = backRemoval->Get("IsFixedSize");
                IsBaseByX           = backRemoval->Get("IsBaseByX");
            }
    
            SmartPtr<iMaterial> mat = uiMesh->GetMaterial();
    
            if ( IsFixedUV )
            {
                uiMesh->SetUVRect(uvRect);
                mat->SetProperty("uvRect",uvRect);
            }
    
            if ( IsFixedSize )
            {
                rect curPosRect = uiMesh->GetPosRect();
    
                if (IsBaseByX )
                {
                    float centerY = curPosRect.y + curPosRect.height*0.5;
                    curPosRect.height = curPosRect.width / widthDivHeight;
                    curPosRect.y = centerY - curPosRect.height*0.5;
                }
                else
                {
                    float centerX = curPosRect.x + curPosRect.width*0.5;
                    curPosRect.width = curPosRect.height * widthDivHeight;
                    curPosRect.x = centerX - curPosRect.width*0.5;
                }
        
                uiMesh->SetPosRect(curPosRect);
            }
        
            if ( IsRotatorCapture )
            {
            
                //vec3 anchorOffset = vec3(-uvRect.x-uvRect.width * 0.5f, -uvRect.y -uvRect.height* 0.5f, 0.0f);
                vec3 anchorOffset = vec3(-rt.x-rt.width * 0.5f, -rt.y-rt.height* 0.5f, 0.0f);
            
                mat4 matAnchor = piglm::translate(anchorOffset);
                mat4 matAnchorBack = piglm::translate(-anchorOffset);
            
            
                mat4 matScale = piglm::scale(vec3(1.0f, DesignResolution.y/DesignResolution.x, 1.0f));
                mat4 matScaleBack = piglm::scale(vec3( 1.0f, DesignResolution.x/DesignResolution.y, 1.0f));
            
            
                quat quatRotator = piglm::angleAxis( piglm::degrees(-faceInfo->GetRoll()) , vec3(0, 0, 1) );
                
                mat4 matRotator = piglm::toMat4(quatRotator);
            
                mat->SetProperty("UVMatrix",matAnchorBack*matScaleBack*matRotator*matScale*matAnchor);
            }
        }
    }
};

iCaptureBackRemovalSystem* CreateCaptureBackRemovalSystem()
{
    return new CaptureBackRemovalSystem();
}

NSPI_END()
