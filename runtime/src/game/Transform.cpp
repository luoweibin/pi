/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2015.3.26   1     Create
 ******************************************************************************/
#include <pi/game/impl/TransformImpl.h>

using namespace std;

NSPI_BEGIN()

class Transform : public TransformImpl<iTransform>
{
public:
    Transform()
    {
    }
    
    virtual ~Transform()
    {
    }
    
    
    virtual void SetTranslation(const vec3& value)
    {
        piAssert(!IsReadonly(), ;);
        TransformImpl::SetTranslation(value);
    }
    
    virtual void SetOrientation(const vec3& value)
    {
        piAssert(!IsReadonly(), ;);
        TransformImpl::SetOrientation(value);
    }
    
    virtual void SetScale(const vec3& value)
    {
        piAssert(!IsReadonly(), ;);
        TransformImpl::SetScale(value);
    }
    
};

iTransform* CreateTransform()
{
    return new Transform();
}

class Transform2D : public TransformImpl<iTransform2D>
{
private:
    vec2 mAnchor;
    int mAlignmentH;
    int mAlignmentV;
    vec2 mSize;
    
    float mRenderScale;
    vec3 mAnchorTranslate;
    
    mutable bool mNeedIntiPos;
public:
    Transform2D():
    mAnchor(0.5, 0.5),
    mAlignmentH(eAlignment_Center),
    mAlignmentV(eAlignment_Center),
    mRenderScale(-1),
    mNeedIntiPos(true)
    {
    }
    
    virtual ~Transform2D()
    {
    }
    
    virtual void SetTranslation(const vec3& value)
    {
        piAssert(!IsReadonly(), ;);
        TransformImpl::SetTranslation(value);
    }
    
    virtual void SetOrientation(const vec3& value)
    {
        piAssert(!IsReadonly(), ;);
        TransformImpl::SetOrientation(value);
    }
    
    virtual void SetScale(const vec3& value)
    {
        piAssert(!IsReadonly(), ;);
        TransformImpl::SetScale(value);
        CalculateAnchorTanslaion();
    }
    
    virtual void Scale(const vec3& value)
    {
        TransformImpl::Scale(value);
        CalculateAnchorTanslaion();
        mDirty = true;
    }
    
    virtual vec2 GetSize() const
    {
        return mSize;
    }
    
    virtual void SetSize(const vec2& size)
    {
        mSize = size;
    }
    
    virtual vec2 GetAnchor() const
    {
        return mAnchor;
    }
    
    virtual void SetAnchor(const vec2& value)
    {
        piAssert(!IsReadonly(), ;);
        mAnchor = value;
        mDirty = true;
    }
    
    virtual int GetAlignmentH() const
    {
        return mAlignmentH;
    }
    
    virtual void SetAlignmentH(int value)
    {
        piAssert(!IsReadonly(), ;);
        mAlignmentH = value;
        mDirty = true;
    }
    
    virtual int GetAlignmentV() const
    {
        return mAlignmentV;
    }
    
    virtual void SetAlignmentV(int value)
    {
        piAssert(!IsReadonly(), ;);
        mAlignmentV = value;
        mDirty = true;
    }
    
    virtual void SetRenderScale(float value)
    {
        if (!piEqual(value, mRenderScale))
        {
            mRenderScale = value;
            CalculateAnchorTanslaion();
        }
        
        if(mNeedIntiPos)
        {
            piAssert(!IsReadonly(), ;);
            
            SmartPtr<iEntity> entity = GetEntity();
            piCheck(!entity.IsNull(), ;);
            
            vec2 mParentSize = vec2(0.0f);
            SmartPtr<iSceneParent> parent = piGetEntityComponent<iSceneParent>(entity);
            
            if(!parent.IsNull())
            {
                SmartPtr<iEntity> parentEntity = entity->GetScene()->FindEntity(parent->GetEntityName());
                SmartPtr<iTransform2D> parentTrans = piGetEntityComponent<iTransform2D>(parentEntity);
                mParentSize =  parentTrans->GetSize() * vec2(mScale.x , mScale.y) * mRenderScale;
            }
            else
            {
                SmartPtr<iScene> scene = entity->GetScene();
                piCheck(!scene.IsNull(), ;);
                
                rect bound = scene->GetBounds();
                mParentSize = vec2(bound.width, bound.height);
            }
            vec2 alignTranslate = EvaluateAlignmentTranslate(mParentSize);
            mTranslation += vec3(alignTranslate.x, alignTranslate.y, 0);
        }
        
        mDirty = true;
    }
    
    virtual mat4 GetMatrix() const
    {
        mNeedIntiPos = false;
        
        mMatrix = mat4(1.0);
        
        SmartPtr<iEntity> entity = GetEntity();
        piCheck(!entity.IsNull(), mMatrix);
        
        SmartPtr<iSceneParent> parent = piGetEntityComponent<iSceneParent>(entity);
        
        if(!parent.IsNull())
        {
            SmartPtr<iEntity> parentEntity = entity->GetScene()->FindEntity(parent->GetEntityName());
            SmartPtr<iTransform2D> parentTrans = piGetEntityComponent<iTransform2D>(parentEntity);
            mMatrix = parentTrans->GetMatrix();
        }
        
        mat4 translation = piglm::translate(mTranslation + mAnchorTranslate);
        
        float x = piglm::radians(mOrientation.x);
        float y = piglm::radians(mOrientation.y);
        float z = piglm::radians(mOrientation.z);
        mat4 orient = piglm::yawPitchRoll(y, x, z);

        mat4 scale = piglm::scale(mScale);
        
        mMatrix *= orient * scale * translation;
        
        mDirty = false;
        
        return mMatrix;
    }
    
    virtual vec2 GetAlignment() const
    {
        return vec2(mAlignmentH, mAlignmentV);
    }
    
    virtual void SetAlignment(const vec2& value)
    {
        mAlignmentH = value.x;
        mAlignmentV = value.y;
    }
    
private:
    void CalculateAnchorTanslaion()
    {
        vec2 factor =  vec2(mScale.x , mScale.y) * mRenderScale;
        vec2 anchorTranslate = 0.0f - mAnchor * mSize * factor;
        mAnchorTranslate = vec3(anchorTranslate.x, anchorTranslate.y, 0.0f);
    }
    
    vec2 EvaluateAlignmentTranslate(const vec2& parentSize) const
    {
        vec2 factor = parentSize;
        
        switch (int32_t(mAlignmentH)) {
            default:
            case eAlignment_Left:
                factor.x *= 0.0f;
                break;
            case eAlignment_Center:
                factor.x *= 0.5f;
                break;
            case eAlignment_Right:
                factor.x *= 1.0f;
                break;
        }
        
        switch (int32_t(mAlignmentV)) {
            default:
            case eAlignment_Bottom:
                factor.y *= 0.0f;
                break;
            case eAlignment_Center:
                factor.y *= 0.5f;
                break;
            case eAlignment_Top:
                factor.y *= 1.0f;
                break;
        }
        
        return factor;
        
    }
    
    
};

iTransform2D* CreateTransform2D()
{
    return new Transform2D();
}

NSPI_END()




















