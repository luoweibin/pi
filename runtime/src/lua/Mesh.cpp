/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 wilburluo     2017.4.28   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>
#include <lua.hpp>
#include "LuaValue.h"
#include "LuaArg.h"

using namespace std;

NSPI_BEGIN()

void Mesh_Init(lua_State* L)
{
    lua_pushinteger(L, offsetof(MeshVertex, pos));
    lua_setfield(L, -2, "eAttrOffset_Position");
    
    lua_pushinteger(L, offsetof(MeshVertex, uv));
    lua_setfield(L, -2, "eAttrOffset_UV");
    
    lua_pushinteger(L, offsetof(MeshVertex, jindex));
    lua_setfield(L, -2, "eAttrOffset_JIndex");
    
    lua_pushinteger(L, offsetof(MeshVertex, jweight));
    lua_setfield(L, -2, "eAttrOffset_JWeight");
    
    lua_pushinteger(L, offsetof(MeshVertex, normal));
    lua_setfield(L, -2, "eAttrOffset_Normal");
    
    lua_pushinteger(L, 3);
    lua_setfield(L, -2, "eAttrSize_Position");
    
    lua_pushinteger(L, 2);
    lua_setfield(L, -2, "eAttrSize_UV");
    
    lua_pushinteger(L, 4);
    lua_setfield(L, -2, "eAttrSize_JIndex");
    
    lua_pushinteger(L, 3);
    lua_setfield(L, -2, "eAttrSize_JWeight");
    
    lua_pushinteger(L, 3);
    lua_setfield(L, -2, "eAttrSize_Normal");
    
    lua_pushinteger(L, sizeof(MeshVertex));
    lua_setfield(L, -2, "eAttrSize_Stride");
}


NSPI_END()
























