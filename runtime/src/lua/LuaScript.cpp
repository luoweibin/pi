/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 wilburluo     2017.5.12   0.1     Create
 ******************************************************************************/
#include <pi/lua/LuaScript.h>
#include <lua.hpp>
#include "LuaValue.h"
#include "LuaArg.h"

using namespace std;

NSPI_BEGIN()

int luaopen_pi(lua_State *L);

static void luaL_requiref (lua_State *L, const char *modname, lua_CFunction openf, int glb)
{
    lua_getglobal(L, "package");
    lua_getfield(L, -1, "loaded");
    if (!lua_istable(L, -1))
    {
        PILOGE(PI_LUA_TAG, "failed to load module '%s'. package.loaded is not a table.");
        lua_pop(L, 2);
        return;
    }
    
    lua_getfield(L, -1, modname);
    
    if (lua_istable(L, -1))
    {
        lua_remove(L, -2);
        return;
    }
    
    lua_pop(L, 1); // pop the last value
    
    lua_pushcfunction(L, openf);
    lua_pushstring(L, modname);
    if (lua_pcall(L, 1, 1, 0) != 0)
    {
        PILOGE(PI_LUA_TAG, "failed to open module '%s':%s", modname, lua_tostring(L, -1));
        lua_pop(L, 2);
        return;
    }
    
    if (lua_istable(L, -1))
    {
        lua_pushvalue(L, -1);
        lua_setfield(L, -3, modname);
        
        lua_remove(L, -2);
        
        if (glb)
        {
            lua_pushvalue(L, -1);
            lua_setglobal(L, modname);
        }
        
        return;
    }
    else
    {
        lua_pop(L, 2);
        return;
    }
}

static int Lua_Loader(lua_State* L);

static bool PCall(lua_State* L, int32_t nargs, int32_t nresults, const string& uri)
{
    if (lua_pcall(L, nargs, nresults, 0) == 0)
    {
        return true;
    }
    else
    {
        const char* err = lua_tostring(L, -1);
        PILOGE(PI_LUA_TAG, "error:%s\n%s", uri.c_str(), err);
        lua_pop(L, 1);
        return false;
    }
}

class LuaScriptRef : public iScriptRef
{
private:
    lua_State* mL;
    string mUri;
    int mRef;
    
public:
    LuaScriptRef(lua_State* L, int ref, const string& uri):
    mL(L), mRef(ref), mUri(uri)
    {
    }
    
    virtual Var Call(const std::string& name,
                     const Var& arg1 = Var(),
                     const Var& arg2 = Var(),
                     const Var& arg3 = Var(),
                     const Var& arg4 = Var(),
                     const Var& arg5 = Var())
    {
        piCheck(mRef != LUA_REFNIL, Var());
        lua_rawgeti(mL, LUA_REGISTRYINDEX, mRef);
        
        Var ret;
        
        lua_getfield(mL, -1, name.c_str());
        if (lua_isfunction(mL, -1))
        {
            lua_rawgeti(mL, LUA_REGISTRYINDEX, mRef);
            
            LuaValue<Var>::Push(mL, arg1);
            LuaValue<Var>::Push(mL, arg2);
            LuaValue<Var>::Push(mL, arg3);
            LuaValue<Var>::Push(mL, arg4);
            LuaValue<Var>::Push(mL, arg5);
            if (PCall(mL, 6, 1, mUri.c_str()))
            {
                ret = LuaArg<Var>::Value(mL, -1);
                lua_pop(mL, 1);
            }
        }
        else
        {
            PILOGV(PI_LUA_TAG, "error:%s\n'%s' is not a function.", mUri.c_str(), name.c_str());
            lua_pop(mL, 1);
        }
        
        // pop table
        lua_pop(mL, 1);
        
        return ret;
    }
};

class LuaScript : public iLuaScript
{
private:
    string mUri;
    lua_State* mL;
    typedef vector<SmartPtr<iScriptLoader>> LoaderVector;
    LoaderVector mLoaders;
    
public:
    LuaScript()
    {
        mL = luaL_newstate();
        luaL_openlibs(mL);
        luaL_requiref(mL, "pi", luaopen_pi, 1);
        luaL_requiref(mL, "bit", luaopen_bit, 0);
        
        RegisterLuaLoader();
    }
    
    virtual ~LuaScript()
    {
        lua_close(mL);
        mL = nullptr;
    }
    
    virtual lua_State* GetState() const
    {
        return mL;
    }
    
    virtual void OnUnload()
    {
        lua_pushnil(mL);
        lua_setglobal(mL, "pi_script");
    }
    
    virtual bool SetGlobal(const std::string& name, const Var& value)
    {
        LuaValue<Var>::Push(mL, value);
        lua_setglobal(mL, name.c_str());
        return true;
    }
    
    virtual Var GetGlobal(const std::string& name) const
    {
        lua_getglobal(mL, name.c_str());
        Var value = LuaArg<Var>::Value(mL, -1);
        lua_pop(mL, 1);
        return value;
    }
    
    virtual void GarbageCollect()
    {
        lua_gc(mL, LUA_GCCOLLECT, 0);
    }
    
    virtual void RegisterLoader(iScriptLoader* loader)
    {
        piAssert(loader != nullptr, ;);
        mLoaders.push_back(loader);
    }
    
    virtual Var Call(const std::string& name,
                     const Var& arg1 = Var(),
                     const Var& arg2 = Var(),
                     const Var& arg3 = Var(),
                     const Var& arg4 = Var(),
                     const Var& arg5 = Var())
    {
        piCheck(mL != nullptr, Var());
        
        Var ret;
        
        lua_getfield(mL, -1, name.c_str());
        if (lua_isfunction(mL, -1))
        {
            LuaValue<Var>::Push(mL, arg1);
            LuaValue<Var>::Push(mL, arg2);
            LuaValue<Var>::Push(mL, arg3);
            LuaValue<Var>::Push(mL, arg4);
            LuaValue<Var>::Push(mL, arg5);
            if (PCall(mL, 5, 1, mUri))
            {
                ret = LuaArg<Var>::Value(mL, -1);
                lua_pop(mL, 1);
            }
        }
        else
        {
            PILOGV(PI_LUA_TAG, "'%s' is not a function.", name.c_str());
            lua_pop(mL, 1);
        }
        
        // pop table
        lua_pop(mL, 1);
        
        return ret;
    }
    
    
    virtual iScriptRef* EvaluateMemory(iMemory* content)
    {
        mUri.clear();
        return DoEvaluate("", content);
    }
    
    virtual iScriptRef* EvaluateFile(const std::string& uri)
    {
        SmartPtr<iMemory> mem = ReadFile(uri);
        SmartPtr<iScriptRef> ret = DoEvaluate(uri, mem);
        if (!ret.IsNull())
        {
            mUri = uri;
        }
        else
        {
            PILOGE(PI_LUA_TAG, "File not found:%s", uri.c_str());
        }
        return ret.PtrAndSetNull();
    }
    
    iMemory* ReadFile(const string& uri)
    {
        for (auto loader : mLoaders)
        {
            SmartPtr<iMemory> mem = loader->Load(uri);
            if (!mem.IsNull())
            {
                return mem.PtrAndSetNull();
            }
        }
        
        return nullptr;
    }
    
private:
    
    iScriptRef* DoEvaluate(const string& uri, iMemory* content)
    {
        piAssert(content != nullptr, nullptr);
        
        if (luaL_dostring(mL, (char*)content->Ptr()) != 0)
        {
            string err = lua_tostring(mL, -1);
            lua_pop(mL, 1);
            
            PILOGE(PI_LUA_TAG, "error:%s:\n%s", uri.c_str(), err.c_str());
            return nullptr;
        }
        
        if (!lua_istable(mL, -1))
        {
            lua_pop(mL, 1);
            return nullptr;
        }
        
        int ref = luaL_ref(mL, LUA_REGISTRYINDEX);
        return new LuaScriptRef(mL, ref, uri);
    }
    
    void RegisterLuaLoader()
    {
        lua_getglobal(mL, "package");
        lua_getfield(mL, -1, "loaders");			    // push "package.loaders"
        lua_remove(mL, -2);                             // remove "package"
        
        // Count the number of entries in package.loaders.
        // Table is now at index -2, since 'nil' is right on top of it.
        // lua_next pushes a key and a value onto the stack.
        
        lua_pushnil(mL);
        int numLoaders = 0;
        while (lua_next(mL, -2) != 0)
        {
            lua_pop(mL, 1);
            numLoaders++;
        }
        
        lua_pushinteger(mL, numLoaders + 1);
        lua_pushcfunction(mL, Lua_Loader);
        lua_rawset(mL, -3);
        
        // Table is still on the stack.  Get rid of it now.
        lua_pop(mL, 1);
        
        // set asset manager as global variable
        LuaValue<LuaScript*>::Push(mL, this);
        lua_setglobal(mL, "pi_script");
    }
};


static int Lua_Loader(lua_State* L)
{
    string name = lua_tostring(L, 1);
    string uri = piFormat("%s.pis", name.c_str());
    
    lua_getglobal(L, "pi_script");
    SmartPtr<LuaScript> script = LuaArg<LuaScript*>::Value(L, -1);
    lua_pop(L, 1);
    
    SmartPtr<iMemory> mem = script->ReadFile(uri);
    if (mem.IsNull())
    {
        lua_pushstring(L, piFormat("no file in pi:%s", uri.c_str()).c_str());
        return 1;
    }
    
    // string size has to remove the '\0' ending.
    luaL_loadbuffer(L, mem->Ptr(), (int32_t)(mem->Size() - 1), name.c_str());
    
    return 1;
}


iLuaScript* CreateLuaScript()
{
    return new LuaScript();
}



NSPI_END()



















