/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 wilburluo     2016.10.11   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>
#include <lua.hpp>
#include "LuaValue.h"
#include "LuaArg.h"

using namespace std;

NSPI_BEGIN()

static int quat_New(lua_State* L)
{
    int top = lua_gettop(L);
    
    quat v;
    
    if (lua_isuserdata(L, 1))
    {
        vec3* ro = (vec3*)lua_touserdata(L, 1);
        v = quat(*ro);
    }
    else
    {
        float x = top >= 1 ? lua_tonumber(L, 1) : 0;
        float y = top >= 2 ? lua_tonumber(L, 2) : 0;
        float z = top >= 3 ? lua_tonumber(L, 3) : 0;
        float w = top >= 4 ? lua_tonumber(L, 4) : 1;
        
        v = quat(w, x, y, z);
    }
    
    LuaValue<quat>::Push(L, v);
    
    return 1;
}

static int quat_Add(lua_State* L)
{
    quat v;
    quat* left = (quat*)lua_touserdata(L, 1);
    
    quat* right = (quat*)lua_touserdata(L, 2);
    v = (*left) + (*right);
    
    LuaValue<quat>::Push(L, v);
    
    return 1;
}

static int quat_Mul(lua_State* L)
{
    quat v;
    quat* left = (quat*)lua_touserdata(L, 1);
    
    quat* right = (quat*)lua_touserdata(L, 2);
    v = (*left) * (*right);
    
    LuaValue<quat>::Push(L, v);
    
    return 1;
}

static int quat_Eq(lua_State* L)
{
    quat* left = (quat*)lua_touserdata(L, 1);
    quat* right = (quat*)lua_touserdata(L, 2);
    bool ret = (left == right);
    lua_pushboolean(L, ret);
    
    return 1;
}

static int quat_GetX(lua_State* L)
{
    quat* v = (quat*)lua_touserdata(L, 1);
    lua_pushnumber(L, v->x);
    return 1;
}

static int quat_SetX(lua_State* L)
{
    quat* v = (quat*)lua_touserdata(L, 1);
    float value = lua_tonumber(L, 2);
    v->x = value;
    return 0;
}

static int quat_GetY(lua_State* L)
{
    quat* v = (quat*)lua_touserdata(L, 1);
    lua_pushnumber(L, v->y);
    return 1;
}

static int quat_SetY(lua_State* L)
{
    quat* v = (quat*)lua_touserdata(L, 1);
    float value = lua_tonumber(L, 2);
    v->y = value;
    return 0;
}

static int quat_GetZ(lua_State* L)
{
    quat* v = (quat*)lua_touserdata(L, 1);
    lua_pushnumber(L, v->z);
    return 1;
}

static int quat_SetZ(lua_State* L)
{
    quat* v = (quat*)lua_touserdata(L, 1);
    float value = lua_tonumber(L, 2);
    v->z = value;
    return 0;
}

static int quat_GetW(lua_State* L)
{
    quat* v = (quat*)lua_touserdata(L, 1);
    lua_pushnumber(L, v->w);
    return 1;
}

static int quat_SetW(lua_State* L)
{
    quat* v = (quat*)lua_touserdata(L, 1);
    float value = lua_tonumber(L, 2);
    v->w = value;
    return 0;
}

static int quat_ToEulerAngle(lua_State* L)
{
    quat* q = (quat*)lua_touserdata(L, 1);
    vec3 eulerAngle = piglm::eulerAngles(*q);
    LuaValue<vec3>::Push(L, eulerAngle);
    return 1;
}

static int quat_ToMat4(lua_State* L)
{
    quat* v = (quat*)lua_touserdata(L, 1);
    mat4 matrix = piglm::toMat4(*v);
    LuaValue<mat4>::Push(L, matrix);
    return 1;
}

static int quat_ToString(lua_State* L)
{
    quat* v = (quat*)lua_touserdata(L, 1);
    string str = piToString(*v);
    lua_pushstring(L, str.c_str());
    return 1;
}

void quat_Init(lua_State* L)
{
    luaL_newmetatable(L, "quat");
    
    lua_pushlightuserdata(L, PrimitiveClass::Quat());
    lua_setfield(L, -2, "_class");
    
    lua_pushcfunction(L, quat_Add);
    lua_setfield(L, -2, "__add");
    
    lua_pushcfunction(L, quat_Mul);
    lua_setfield(L, -2, "__mul");
    
    lua_pushcfunction(L, quat_Eq);
    lua_setfield(L, -2, "__eq");
    
    lua_pushcfunction(L, quat_ToString);
    lua_setfield(L, -2, "__tostring");
    
    //__index table
    
    lua_newtable(L);
    
    lua_pushcfunction(L, quat_GetX);
    lua_setfield(L, -2, "GetX");
    
    lua_pushcfunction(L, quat_SetX);
    lua_setfield(L, -2, "SetX");
    
    lua_pushcfunction(L, quat_GetY);
    lua_setfield(L, -2, "GetY");
    
    lua_pushcfunction(L, quat_SetY);
    lua_setfield(L, -2, "SetY");
    
    lua_pushcfunction(L, quat_GetZ);
    lua_setfield(L, -2, "GetZ");
    
    lua_pushcfunction(L, quat_SetZ);
    lua_setfield(L, -2, "SetZ");
    
    lua_pushcfunction(L, quat_GetW);
    lua_setfield(L, -2, "GetW");
    
    lua_pushcfunction(L, quat_SetW);
    lua_setfield(L, -2, "SetW");
    
    lua_pushcfunction(L, quat_ToMat4);
    lua_setfield(L, -2, "ToMat4");
    
    lua_pushcfunction(L, quat_ToEulerAngle);
    lua_setfield(L, -2, "ToEulerAngle");
    
    lua_setfield(L, -2, "__index");
    
    lua_pop(L, 1);
    
    // register construtor
    lua_pushcfunction(L, quat_New);
    lua_setfield(L, -2, "quat");
}


NSPI_END()














