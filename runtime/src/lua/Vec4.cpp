/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 wilburluo     2016.10.11   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>
#include <lua.hpp>
#include "LuaValue.h"
#include "LuaArg.h"

using namespace std;

NSPI_BEGIN()

static int vec4_New(lua_State* L)
{
    int top = lua_gettop(L);
    
    vec4 v;
    
    switch (top)
    {
        case 1:
            v.x = lua_tonumber(L, 1);
            v.y = v.x;
            v.z = v.x;
            v.w = 1;
            break;
        case 2:
            v.x = lua_tonumber(L, 1);
            v.y = lua_tonumber(L, 2);
            v.z = 0;
            v.w = 1;
            break;
        case 3:
            v.x = lua_tonumber(L, 1);
            v.y = lua_tonumber(L, 2);
            v.z = lua_tonumber(L, 3);
            v.w = 1;
        default:
            v.x = lua_tonumber(L, 1);
            v.y = lua_tonumber(L, 2);
            v.z = lua_tonumber(L, 3);
            v.w = lua_tonumber(L, 4);
            break;
    }
    
    LuaValue<vec4>::Push(L, v);
    
    return 1;
}

static int vec4_Add(lua_State* L)
{
    vec4 v;
    
    vec4* left = (vec4*)lua_touserdata(L, 1);
    
    if (lua_isuserdata(L, 2))
    {
        vec4* right = (vec4*)lua_touserdata(L, 2);
        v = (*left) + (*right);
    }
    else if (lua_isnumber(L, 2))
    {
        float right = lua_tonumber(L, 2);
        v = (*left) + right;
    }
    
    LuaValue<vec4>::Push(L, v);
    
    return 1;
}

static int vec4_Sub(lua_State* L)
{
    vec4 v;
    
    vec4* left = (vec4*)lua_touserdata(L, 1);
    
    if (lua_isuserdata(L, 2))
    {
        vec4* right = (vec4*)lua_touserdata(L, 2);
        v = (*left) - (*right);
    }
    else if (lua_isnumber(L, 2))
    {
        float right = lua_tonumber(L, 2);
        v = (*left) - right;
    }
    
    LuaValue<vec4>::Push(L, v);
    
    return 1;
}

static int vec4_Mul(lua_State* L)
{
    vec4 v;
    
    vec4* left = (vec4*)lua_touserdata(L, 1);
    
    if (lua_isuserdata(L, 2))
    {
        vec4* right = (vec4*)lua_touserdata(L, 2);
        v = (*left) * (*right);
    }
    else if (lua_isnumber(L, 2))
    {
        float right = lua_tonumber(L, 2);
        v = (*left) * right;
    }
    
    LuaValue<vec4>::Push(L, v);
    
    return 1;
}

static int vec4_Div(lua_State* L)
{
    vec4 v;
    
    vec4* left = (vec4*)lua_touserdata(L, 1);
    
    if (lua_isuserdata(L, 2))
    {
        vec4* right = (vec4*)lua_touserdata(L, 2);
        v = (*left) / (*right);
    }
    else if (lua_isnumber(L, 2))
    {
        float right = lua_tonumber(L, 2);
        v = (*left) / right;
    }
    
    LuaValue<vec4>::Push(L, v);
    
    return 1;
}

static int vec4_Pow(lua_State* L)
{
    vec4* v = (vec4*)lua_touserdata(L, 1);
    vec4 ret(v->x * v->x, v->y * v->y, v->z * v->z, v->w * v->w);
    
    LuaValue<vec4>::Push(L, ret);
    
    return 1;
}

static int vec4_Unm(lua_State* L)
{
    vec4* v = (vec4*)lua_touserdata(L, 1);
    vec4 ret(-*v);
    
    LuaValue<vec4>::Push(L, ret);
    
    return 1;
}

static int vec4_Eq(lua_State* L)
{
    vec4* left = (vec4*)lua_touserdata(L, 1);
    vec4* right = (vec4*)lua_touserdata(L, 2);
    bool ret = (left == right);
    lua_pushboolean(L, ret);
    
    return 1;
}

static int vec4_GetX(lua_State* L)
{
    vec4* v = (vec4*)lua_touserdata(L, 1);
    lua_pushnumber(L, v->x);
    return 1;
}

static int vec4_SetX(lua_State* L)
{
    vec4* v = (vec4*)lua_touserdata(L, 1);
    float value = lua_tonumber(L, 2);
    v->x = value;
    return 0;
}

static int vec4_GetY(lua_State* L)
{
    vec4* v = (vec4*)lua_touserdata(L, 1);
    lua_pushnumber(L, v->y);
    return 1;
}

static int vec4_SetY(lua_State* L)
{
    vec4* v = (vec4*)lua_touserdata(L, 1);
    float value = lua_tonumber(L, 2);
    v->y = value;
    return 0;
}

static int vec4_GetZ(lua_State* L)
{
    vec4* v = (vec4*)lua_touserdata(L, 1);
    lua_pushnumber(L, v->z);
    return 1;
}

static int vec4_SetZ(lua_State* L)
{
    vec4* v = (vec4*)lua_touserdata(L, 1);
    float value = lua_tonumber(L, 2);
    v->z = value;
    return 0;
}

static int vec4_GetW(lua_State* L)
{
    vec4* v = (vec4*)lua_touserdata(L, 1);
    lua_pushnumber(L, v->w);
    return 1;
}

static int vec4_SetW(lua_State* L)
{
    vec4* v = (vec4*)lua_touserdata(L, 1);
    float value = lua_tonumber(L, 2);
    v->w = value;
    return 0;
}

static int vec4_ToString(lua_State* L)
{
    vec4* v = (vec4*)lua_touserdata(L, 1);
    string str = piToString(*v);
    lua_pushstring(L, str.c_str());
    return 1;
}

void vec4_Init(lua_State* L)
{
    luaL_newmetatable(L, "vec4");
    
    lua_pushlightuserdata(L, PrimitiveClass::Vec4());
    lua_setfield(L, -2, "_class");
    
    lua_pushcfunction(L, vec4_Add);
    lua_setfield(L, -2, "__add");
    
    lua_pushcfunction(L, vec4_Sub);
    lua_setfield(L, -2, "__sub");
    
    lua_pushcfunction(L, vec4_Mul);
    lua_setfield(L, -2, "__mul");
    
    lua_pushcfunction(L, vec4_Div);
    lua_setfield(L, -2, "__div");
    
    lua_pushcfunction(L, vec4_Pow);
    lua_setfield(L, -2, "__pow");
    
    lua_pushcfunction(L, vec4_Unm);
    lua_setfield(L, -2, "__unm");
    
    lua_pushcfunction(L, vec4_Eq);
    lua_setfield(L, -2, "__eq");
    
    lua_pushcfunction(L, vec4_ToString);
    lua_setfield(L, -2, "__tostring");
    
    // __index table
    lua_newtable(L);
    
    lua_pushcfunction(L, vec4_GetX);
    lua_setfield(L, -2, "GetX");
    
    lua_pushcfunction(L, vec4_SetX);
    lua_setfield(L, -2, "SetX");
    
    lua_pushcfunction(L, vec4_GetY);
    lua_setfield(L, -2, "GetY");
    
    lua_pushcfunction(L, vec4_SetY);
    lua_setfield(L, -2, "SetY");
    
    lua_pushcfunction(L, vec4_GetZ);
    lua_setfield(L, -2, "GetZ");
    
    lua_pushcfunction(L, vec4_SetZ);
    lua_setfield(L, -2, "SetZ");
    
    lua_pushcfunction(L, vec4_GetW);
    lua_setfield(L, -2, "GetW");
    
    lua_pushcfunction(L, vec4_SetW);
    lua_setfield(L, -2, "SetW");
    
    lua_setfield(L, -2, "__index");
    
    lua_pop(L, 1);
    
    // register construtor
    lua_pushcfunction(L, vec4_New);
    lua_setfield(L, -2, "vec4");
}



NSPI_END()


























