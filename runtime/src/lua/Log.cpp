/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 wilburluo     2016.10.20   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>
#include <lua.hpp>
#include "LuaValue.h"
#include "LuaArg.h"

using namespace std;

NSPI_BEGIN()

template <int level>
static int pi_Log(lua_State* L)
{
    int top = lua_gettop(L);
    piCheck(top >= 2, 0);
    
    piAssert(lua_isstring(L, 1), 0);
    string tag = lua_tostring(L, 1);
    
    piAssert(lua_isstring(L, 2), 0);
    string message = lua_tostring(L, 2);
    
    _piLogT("script", 0, level, tag.c_str(), "%s", message.c_str());
    
    return 0;
}

void Log_Init(lua_State* L)
{
    lua_pushcfunction(L, pi_Log<LOG_INFO>);
    lua_setfield(L, -2, "LogI");
    
    lua_pushcfunction(L, pi_Log<LOG_DEBUG>);
    lua_setfield(L, -2, "LogD");
    
    lua_pushcfunction(L, pi_Log<LOG_WARNING>);
    lua_setfield(L, -2, "LogW");
    
    lua_pushcfunction(L, pi_Log<LOG_ERROR>);
    lua_setfield(L, -2, "LogE");
    
    lua_pushcfunction(L, pi_Log<LOG_VERBOSE>);
    lua_setfield(L, -2, "LogV");
    
    lua_pushcfunction(L, pi_Log<LOG_SYSTEM>);
    lua_setfield(L, -2, "LogS");
}


NSPI_END()






















