/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.7   0.1     Create
 ******************************************************************************/
#ifndef PI_LUA_LUAARG_H
#define PI_LUA_LUAARG_H

namespace nspi
{
    template <typename T>
    struct LuaArg
    {
        static T Value(lua_State* L, int index)
        {
            return 0;
        }
    };
    
    
    template <typename T>
    struct LuaArg<T*>
    {
        static T* Value(lua_State* L, int index)
        {
            piCheck(!lua_isnil(L, index) && index <= lua_gettop(L), nullptr);
            LuaWrapper<T>* wrapper = (LuaWrapper<T>*)lua_touserdata(L, index);
            return wrapper->ref;
        }
    };
    
    
    template <typename T>
    struct LuaArg<const T&>
    {
        static T Value(lua_State* L, int index)
        {
            piCheck(!lua_isnil(L, index) && index <= lua_gettop(L), T());
            return *dynamic_cast<T*>(lua_touserdata(L, index));
        }
    };
    
    
    template <>
    struct LuaArg<bool>
    {
        static bool Value(lua_State* L, int index)
        {
            piCheck(!lua_isnil(L, index) && index <= lua_gettop(L), false);
            return lua_toboolean(L, index);
        }
    };
    
    template <>
    struct LuaArg<int8_t>
    {
        static int8_t Value(lua_State* L, int index)
        {
            piCheck(!lua_isnil(L, index) && index <= lua_gettop(L), 0);
            return (int8_t)lua_tointeger(L, index);
        }
    };
    
    
    template <>
    struct LuaArg<uint8_t>
    {
        static uint8_t Value(lua_State* L, int index)
        {
            piCheck(!lua_isnil(L, index) && index <= lua_gettop(L), 0);
            return (uint8_t)lua_tointeger(L, index);
        }
    };
    
    
    template <>
    struct LuaArg<int16_t>
    {
        static int16_t Value(lua_State* L, int index)
        {
            piCheck(!lua_isnil(L, index) && index <= lua_gettop(L), 0);
            return (int16_t)lua_tointeger(L, index);
        }
    };
    
    
    template <>
    struct LuaArg<uint16_t>
    {
        static uint16_t Value(lua_State* L, int index)
        {
            piCheck(!lua_isnil(L, index) && index <= lua_gettop(L), 0);
            return (uint16_t)lua_tointeger(L, index);
        }
    };
    
    
    template <>
    struct LuaArg<int32_t>
    {
        static int32_t Value(lua_State* L, int index)
        {
            piCheck(!lua_isnil(L, index) && index <= lua_gettop(L), 0);
            return (int32_t)lua_tointeger(L, index);
        }
    };
    
    
    template <>
    struct LuaArg<uint32_t>
    {
        static uint32_t Value(lua_State* L, int index)
        {
            piCheck(!lua_isnil(L, index) && index <= lua_gettop(L), 0);
            return (uint32_t)lua_tointeger(L, index);
        }
    };
    
    
    template <>
    struct LuaArg<int64_t>
    {
        static int64_t Value(lua_State* L, int index)
        {
            piCheck(!lua_isnil(L, index) && index <= lua_gettop(L), 0);
            return (int64_t)lua_tointeger(L, index);
        }
    };
    
    
    template <>
    struct LuaArg<uint64_t>
    {
        static uint64_t Value(lua_State* L, int index)
        {
            piCheck(!lua_isnil(L, index) && index <= lua_gettop(L), 0);
            return (uint64_t)lua_tointeger(L, index);
        }
    };
    
    
    template <>
    struct LuaArg<float>
    {
        static float Value(lua_State* L, int index)
        {
            piCheck(!lua_isnil(L, index) && index <= lua_gettop(L), 0);
            return (float)lua_tonumber(L, index);
        }
    };
    
    
    template <>
    struct LuaArg<double>
    {
        static double Value(lua_State* L, int index)
        {
            piCheck(!lua_isnil(L, index) && index <= lua_gettop(L), 0);
            return lua_tonumber(L, index);
        }
    };
    
    
    template <>
    struct LuaArg<std::string>
    {
        static std::string Value(lua_State* L, int index)
        {
            piCheck(!lua_isnil(L, index) && index <= lua_gettop(L), "");
            return lua_tostring(L, index);
        }
    };
    
    
    template <>
    struct LuaArg<vec2>
    {
        static vec2 Value(lua_State* L, int index)
        {
            piCheck(!lua_isnil(L, index) && index <= lua_gettop(L), vec2());
            return *(vec2*)lua_touserdata(L, index);
        }
    };
    
    
    template <>
    struct LuaArg<vec3>
    {
        static vec3 Value(lua_State* L, int index)
        {
            piCheck(!lua_isnil(L, index) && index <= lua_gettop(L), vec3());
            return *(vec3*)lua_touserdata(L, index);
        }
    };
    
    
    template <>
    struct LuaArg<vec4>
    {
        static vec4 Value(lua_State* L, int index)
        {
            piCheck(!lua_isnil(L, index) && index <= lua_gettop(L), vec4());
            return *(vec4*)lua_touserdata(L, index);
        }
    };
    
    
    template <>
    struct LuaArg<mat4>
    {
        static mat4 Value(lua_State* L, int index)
        {
            piCheck(!lua_isnil(L, index) && index <= lua_gettop(L), mat4());
            return *(mat4*)lua_touserdata(L, index);
        }
    };
    
    
    template <>
    struct LuaArg<quat>
    {
        static quat Value(lua_State* L, int index)
        {
            piCheck(!lua_isnil(L, index) && index <= lua_gettop(L), quat());
            return *(quat*)lua_touserdata(L, index);
        }
    };
    
    
    template <>
    struct LuaArg<rect>
    {
        static rect Value(lua_State* L, int index)
        {
            piCheck(!lua_isnil(L, index) && index <= lua_gettop(L), rect());
            return *(rect*)lua_touserdata(L, index);
        }
    };
    
    
    template <>
    struct LuaArg<Var>
    {
        static Var Value(lua_State* L, int index)
        {
            int type = lua_type(L, index);
            switch (type)
            {
                case LUA_TBOOLEAN:
                    return LuaArg<bool>::Value(L, index);
                case LUA_TNUMBER:
                    return LuaArg<double>::Value(L, index);
                case LUA_TSTRING:
                    return LuaArg<std::string>::Value(L, index);
                case LUA_TUSERDATA:
                {
                    if (luaL_getmetafield(L, index, "_class") != LUA_TNIL)
                    {
                        SmartPtr<iClass> klass = (iClass*)lua_touserdata(L, -1);
                        lua_pop(L, 1);
                        
                        if (klass.Ptr() == PrimitiveClass::Mat4())
                        {
                            return LuaArg<mat4>::Value(L, index);
                        }
                        else if (klass.Ptr() == PrimitiveClass::Vec2())
                        {
                            return LuaArg<vec2>::Value(L, index);
                        }
                        else if (klass.Ptr() == PrimitiveClass::Vec3())
                        {
                            return LuaArg<vec3>::Value(L, index);
                        }
                        else if (klass.Ptr() == PrimitiveClass::Vec4())
                        {
                            return LuaArg<vec4>::Value(L, index);
                        }
                        else if (klass.Ptr() == PrimitiveClass::Quat())
                        {
                            return LuaArg<quat>::Value(L, index);
                        }
                        else if (klass.Ptr() == PrimitiveClass::Rect())
                        {
                            return LuaArg<rect>::Value(L, index);
                        }
                        else if (klass.Ptr() == iI8Array::StaticClass())
                        {
                            return LuaArg<iI8Array*>::Value(L, index);
                        }
                        else if (klass.Ptr() == iI16Array::StaticClass())
                        {
                            return LuaArg<iI16Array*>::Value(L, index);
                        }
                        else if (klass.Ptr() == iI32Array::StaticClass())
                        {
                            return LuaArg<iI32Array*>::Value(L, index);
                        }
                        else if (klass.Ptr() == iI64Array::StaticClass())
                        {
                            return LuaArg<iI64Array*>::Value(L, index);
                        }
                        else if (klass.Ptr() == iU8Array::StaticClass())
                        {
                            return LuaArg<iU8Array*>::Value(L, index);
                        }
                        else if (klass.Ptr() == iU16Array::StaticClass())
                        {
                            return LuaArg<iU16Array*>::Value(L, index);
                        }
                        else if (klass.Ptr() == iU32Array::StaticClass())
                        {
                            return LuaArg<iU32Array*>::Value(L, index);
                        }
                        else if (klass.Ptr() == iU64Array::StaticClass())
                        {
                            return LuaArg<iU64Array*>::Value(L, index);
                        }
                        else if (klass.Ptr() == iVec2Array::StaticClass())
                        {
                            return LuaArg<iVec2Array*>::Value(L, index);
                        }
                        else if (klass.Ptr() == iVec3Array::StaticClass())
                        {
                            return LuaArg<iVec3Array*>::Value(L, index);
                        }
                        else if (klass.Ptr() == iVec4Array::StaticClass())
                        {
                            return LuaArg<iVec4Array*>::Value(L, index);
                        }
                        else if (klass.Ptr() == iMat4Array::StaticClass())
                        {
                            return LuaArg<iMat4Array*>::Value(L, index);
                        }
                        else if (klass.Ptr() == iQuatArray::StaticClass())
                        {
                            return LuaArg<iQuatArray*>::Value(L, index);
                        }
                        else if (klass.Ptr() == iRectArray::StaticClass())
                        {
                            return LuaArg<iRectArray*>::Value(L, index);
                        }
                        else
                        {
                            return LuaArg<iRefObject*>::Value(L, index);
                        }
                    }
                    
                    return Var();
                }
                case LUA_TNIL:
                default:
                    return Var();
            }
        }
    };
}

#endif





















