/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 wilburluo     2016.10.11   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>
#include <lua.hpp>
#include "LuaValue.h"
#include "LuaArg.h"

using namespace std;

NSPI_BEGIN()

static int rect_New(lua_State* L)
{
    int top = lua_gettop(L);

    float x = top >= 1 ? lua_tonumber(L, 1) : 0;
    float y = top >= 2 ? lua_tonumber(L, 2) : 0;
    float width = top >= 3 ? lua_tonumber(L, 3) : 0;
    float height = top >= 4 ? lua_tonumber(L, 4) : 1;
    
    rect v(x, y, width, height);
    
    LuaValue<rect>::Push(L, v);
    
    return 1;
}

static int rect_Eq(lua_State* L)
{
    rect* left = (rect*)lua_touserdata(L, 1);
    rect* right = (rect*)lua_touserdata(L, 2);
    bool ret = (left == right);
    lua_pushboolean(L, ret);
    
    return 1;
}

static int rect_GetX(lua_State* L)
{
    rect* v = (rect*)lua_touserdata(L, 1);
    lua_pushnumber(L, v->x);
    return 1;
}

static int rect_SetX(lua_State* L)
{
    rect* v = (rect*)lua_touserdata(L, 1);
    float value = lua_tonumber(L, 2);
    v->x = value;
    return 0;
}

static int rect_GetY(lua_State* L)
{
    rect* v = (rect*)lua_touserdata(L, 1);
    lua_pushnumber(L, v->y);
    return 1;
}

static int rect_SetY(lua_State* L)
{
    rect* v = (rect*)lua_touserdata(L, 1);
    float value = lua_tonumber(L, 2);
    v->y = value;
    return 0;
}

static int rect_GetWidth(lua_State* L)
{
    rect* v = (rect*)lua_touserdata(L, 1);
    lua_pushnumber(L, v->width);
    return 1;
}

static int rect_SetWidth(lua_State* L)
{
    rect* v = (rect*)lua_touserdata(L, 1);
    float value = lua_tonumber(L, 2);
    v->width = value;
    return 0;
}

static int rect_GetHeight(lua_State* L)
{
    rect* v = (rect*)lua_touserdata(L, 1);
    lua_pushnumber(L, v->height);
    return 1;
}

static int rect_SetHeight(lua_State* L)
{
    rect* v = (rect*)lua_touserdata(L, 1);
    float value = lua_tonumber(L, 2);
    v->height = value;
    return 0;
}

static int rect_ToString(lua_State* L)
{
    rect* v = (rect*)lua_touserdata(L, 1);
    string str = piToString(*v);
    lua_pushstring(L, str.c_str());
    return 1;
}

void rect_Init(lua_State* L)
{
    luaL_newmetatable(L, "rect");
    
    lua_pushlightuserdata(L, PrimitiveClass::Rect());
    lua_setfield(L, -2, "_class");
    
    lua_pushcfunction(L, rect_ToString);
    lua_setfield(L, -2, "__tostring");
    
    lua_pushcfunction(L, rect_Eq);
    lua_setfield(L, -2, "__eq");
    
    //__index table
    lua_newtable(L);
    
    lua_pushcfunction(L, rect_GetX);
    lua_setfield(L, -2, "GetX");
    
    lua_pushcfunction(L, rect_SetX);
    lua_setfield(L, -2, "SetX");
    
    lua_pushcfunction(L, rect_GetY);
    lua_setfield(L, -2, "GetY");
    
    lua_pushcfunction(L, rect_SetY);
    lua_setfield(L, -2, "SetY");
    
    lua_pushcfunction(L, rect_GetWidth);
    lua_setfield(L, -2, "GetWidth");
    
    lua_pushcfunction(L, rect_SetWidth);
    lua_setfield(L, -2, "SetWidth");
    
    lua_pushcfunction(L, rect_GetHeight);
    lua_setfield(L, -2, "GetHeight");
    
    lua_pushcfunction(L, rect_SetHeight);
    lua_setfield(L, -2, "SetHeight");
    
    lua_setfield(L, -2, "__index");
    
    lua_pop(L, 1);
    
    // register construtor
    lua_pushcfunction(L, rect_New);
    lua_setfield(L, -2, "rect");
}


NSPI_END()














