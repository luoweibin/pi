/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 wilburluo     2016.10.24   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>
#include <lua.hpp>
#include "LuaValue.h"
#include "LuaArg.h"

using namespace std;

void lua_pi_Init(lua_State* L);

NSPI_BEGIN()

void vec2_Init(lua_State* L);
void vec3_Init(lua_State* L);
void vec4_Init(lua_State* L);
void quat_Init(lua_State* L);
void mat4_Init(lua_State* L);
void rect_Init(lua_State* L);
void Log_Init(lua_State* L);
void Math_Init(lua_State* L);
void Mesh_Init(lua_State* L);

int luaopen_pi(lua_State *L)
{
    piAssert(L != nullptr, 0);
    
    lua_newtable(L);
        
    vec2_Init(L);
    vec3_Init(L);
    vec4_Init(L);
    mat4_Init(L);
    quat_Init(L);
    rect_Init(L);
    Log_Init(L);
    mat4_Init(L);
    Math_Init(L);
    Mesh_Init(L);

    lua_pi_Init(L);

    lua_pushstring(L, PI_GAME_TAG);
    lua_setfield(L, -2, "GAME_TAG");
    
    return 1;
}


NSPI_END()

















