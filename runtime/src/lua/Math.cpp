/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 wilburluo     2016.10.24   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>
#include <lua.hpp>
#include "LuaValue.h"
#include "LuaArg.h"

using namespace std;

NSPI_BEGIN()

void Math_Vector_Init(lua_State* L);


static int Math_Translate(lua_State* L)
{
    mat4 matrix;
    
    if (lua_isuserdata(L, 1))
    {
        vec3* v = (vec3*)lua_touserdata(L, 1);
        matrix = piglm::translate(*v);
    }
    else
    {
        int top = lua_gettop(L);
        vec3 v;
        v.x = top >= 1 ? lua_tonumber(L, 1) : 0;
        v.y = top >= 2 ? lua_tonumber(L, 2) : 0;
        v.z = top >= 3 ? lua_tonumber(L, 3) : 0;
        matrix = piglm::translate(v);
    }
    
    LuaValue<mat4>::Push(L, matrix);
    
    return 1;
}

static int Math_Scale(lua_State* L)
{
    mat4 matrix;
    
    if (lua_isuserdata(L, 1))
    {
        vec3* v = (vec3*)lua_touserdata(L, 1);
        matrix = piglm::scale(*v);
    }
    else
    {
        int top = lua_gettop(L);
        vec3 v;
        v.x = top >= 1 ? lua_tonumber(L, 1) : 0;
        v.y = top >= 2 ? lua_tonumber(L, 2) : 0;
        v.z = top >= 3 ? lua_tonumber(L, 3) : 0;
        matrix = piglm::scale(v);
    }
    
    LuaValue<mat4>::Push(L, matrix);
    
    return 1;
}

static int Math_AngleAxis(lua_State* L)
{
    float angle = LuaArg<float>::Value(L, 1);
    vec3 axis = LuaArg<vec3>::Value(L, 2);
    quat q = piglm::angleAxis(angle, axis);
    
    LuaValue<quat>::Push(L, q);
    
    return 1;
}

template <typename T>
static int Math_MixTpl(lua_State* L)
{
    T left = LuaArg<T>::Value(L, 1);
    T right = LuaArg<T>::Value(L, 2);
    float factor = LuaArg<float>::Value(L, 3);
    
    T v = piglm::mix(left, right, factor);
    LuaValue<T>::Push(L, v);
    
    return 1;
}

static int Math_Mix(lua_State* L)
{
    if (lua_isnumber(L, 1))
    {
        return Math_MixTpl<float>(L);
    }
    else if (lua_isuserdata(L, 1) && luaL_getmetafield(L, -1, "_class") != LUA_TNIL)
    {
        SmartPtr<iClass> klass = (iClass*)lua_touserdata(L, -1);
        lua_pop(L, 1);
        
        if (klass == PrimitiveClass::Vec2())
        {
            return Math_MixTpl<vec2>(L);
        }
        else if (klass == PrimitiveClass::Vec3())
        {
            return Math_MixTpl<vec3>(L);
        }
        else if (klass == PrimitiveClass::Vec4())
        {
            return Math_MixTpl<vec4>(L);
        }
        else if (klass == PrimitiveClass::Quat())
        {
            return Math_MixTpl<quat>(L);
        }
    }
    
    return 0;
}

static int Math_Slerp(lua_State* L)
{
    quat left = LuaArg<quat>::Value(L, 1);
    quat right = LuaArg<quat>::Value(L, 2);
    float factor = LuaArg<float>::Value(L, 3);
    
    quat v = piglm::slerp(left, right, factor);
    LuaValue<quat>::Push(L, v);
    
    return 1;
}


template <typename T>
static int Math_NormalizeTpl(lua_State* L, int index)
{
    T v = piglm::normalize(LuaArg<T>::Value(L, index));
    LuaValue<T>::Push(L, v);
    return 1;
}

static int Math_Normalize(lua_State* L)
{
    if (lua_isuserdata(L, 1) && luaL_getmetafield(L, 1, "_class") != LUA_TNIL)
    {
        SmartPtr<iClass> klass = (iClass*)lua_touserdata(L, -1);
        lua_pop(L, 1);
        
        if (klass == PrimitiveClass::Vec2())
        {
            return Math_NormalizeTpl<vec2>(L, 1);
        }
        else if (klass == PrimitiveClass::Vec3())
        {
            return Math_NormalizeTpl<vec3>(L, 1);
        }
        else if (klass == PrimitiveClass::Vec4())
        {
            return Math_NormalizeTpl<vec4>(L, 1);
        }
        else if (klass == PrimitiveClass::Quat())
        {
            return Math_NormalizeTpl<quat>(L, 1);
        }
    }
    
    return 0;
}

static int Math_Ortho(lua_State* L)
{
    int stackTop = lua_gettop(L);
    
    float left = stackTop >= 1 ? lua_tonumber(L, 1) : 0;
    float right = stackTop >= 2 ? lua_tonumber(L, 2) : 0;
    float bottom = stackTop >= 3 ? lua_tonumber(L, 3) : 0;
    float top = stackTop >= 4 ? lua_tonumber(L, 4) : 0;
    float near = stackTop >= 5 ? lua_tonumber(L, 5) : 0;
    float far = stackTop >= 6 ? lua_tonumber(L, 6) : 0;
    
    mat4 m = piglm::ortho(left, right, bottom, top, near, far);
    LuaValue<mat4>::Push(L, m);
    
    return 1;
}

static int Math_Frustum(lua_State* L)
{
    int stackTop = lua_gettop(L);
    
    float left = stackTop >= 1 ? lua_tonumber(L, 1) : 0;
    float right = stackTop >= 2 ? lua_tonumber(L, 2) : 0;
    float bottom = stackTop >= 3 ? lua_tonumber(L, 3) : 0;
    float top = stackTop >= 4 ? lua_tonumber(L, 4) : 0;
    float near = stackTop >= 5 ? lua_tonumber(L, 5) : 0;
    float far = stackTop >= 6 ? lua_tonumber(L, 6) : 0;
    
    mat4 m = piglm::frustum(left, right, bottom, top, near, far);
    LuaValue<mat4>::Push(L, m);
    
    return 1;
}

static int Math_LookAt(lua_State* L)
{
    vec3 eye = LuaArg<vec3>::Value(L, 1);
    vec3 center = LuaArg<vec3>::Value(L, 2);
    vec3 up = LuaArg<vec3>::Value(L, 3);
    
    mat4 m = piglm::lookAt(eye, center, up);
    LuaValue<mat4>::Push(L, m);
    
    return 1;
}

void Math_Init(lua_State* L)
{
    lua_pushcfunction(L, Math_Translate);
    lua_setfield(L, -2, "Translate");
    
    lua_pushcfunction(L, Math_Scale);
    lua_setfield(L, -2, "Scale");
    
    lua_pushcfunction(L, Math_AngleAxis);
    lua_setfield(L, -2, "AngleAxis");
    
    lua_pushcfunction(L, Math_Mix);
    lua_setfield(L, -2, "Mix");
    
    lua_pushcfunction(L, Math_Normalize);
    lua_setfield(L, -2, "Normailze");
    
    lua_pushcfunction(L, Math_Slerp);
    lua_setfield(L, -2, "Slerp");
    
    lua_pushcfunction(L, Math_Ortho);
    lua_setfield(L, -2, "Ortho");
    
    lua_pushcfunction(L, Math_Frustum);
    lua_setfield(L, -2, "Frustum");
    
    lua_pushcfunction(L, Math_LookAt);
    lua_setfield(L, -2, "LookAt");
    
    lua_pushnumber(L, piglm::pi<double>());
    lua_setfield(L, -2, "PI");
    
    
    lua_pushinteger(L, std::numeric_limits<int8_t>::max());
    lua_setfield(L, -2, "I8_MAX");
    
    lua_pushinteger(L, std::numeric_limits<uint8_t>::max());
    lua_setfield(L, -2, "U8_MAX");
    
    lua_pushinteger(L, std::numeric_limits<int16_t>::max());
    lua_setfield(L, -2, "I16_MAX");
    
    lua_pushinteger(L, std::numeric_limits<uint16_t>::max());
    lua_setfield(L, -2, "U16_MAX");
    
    lua_pushinteger(L, std::numeric_limits<int32_t>::max());
    lua_setfield(L, -2, "I32_MAX");
    
    lua_pushinteger(L, std::numeric_limits<uint32_t>::max());
    lua_setfield(L, -2, "U32_MAX");
    
    lua_pushinteger(L, (lua_Integer)std::numeric_limits<int64_t>::max());
    lua_setfield(L, -2, "I64_MAX");
    
    lua_pushinteger(L, (lua_Integer)std::numeric_limits<uint64_t>::max());
    lua_setfield(L, -2, "U64_MAX");
    
    lua_pushinteger(L, std::numeric_limits<int8_t>::min());
    lua_setfield(L, -2, "I8_MIN");
    
    lua_pushinteger(L, std::numeric_limits<uint8_t>::min());
    lua_setfield(L, -2, "U8_MIN");
    
    lua_pushinteger(L, std::numeric_limits<int16_t>::min());
    lua_setfield(L, -2, "I16_MIN");
    
    lua_pushinteger(L, std::numeric_limits<uint16_t>::min());
    lua_setfield(L, -2, "U16_MIN");
    
    lua_pushinteger(L, std::numeric_limits<int32_t>::min());
    lua_setfield(L, -2, "I32_MIN");
    
    lua_pushinteger(L, std::numeric_limits<uint32_t>::min());
    lua_setfield(L, -2, "U32_MIN");
    
    lua_pushinteger(L, (lua_Integer)std::numeric_limits<int64_t>::min());
    lua_setfield(L, -2, "I64_MIN");
    
    lua_pushinteger(L, (lua_Integer)std::numeric_limits<uint64_t>::min());
    lua_setfield(L, -2, "U64_MIN");
    
    
    Math_Vector_Init(L);
}




NSPI_END()























