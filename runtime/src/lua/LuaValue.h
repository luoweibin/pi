/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.7   0.1     Create
 ******************************************************************************/
#ifndef PI_LUA_LUAVALUE_H
#define PI_LUA_LUAVALUE_H

namespace nspi
{
    template <class T>
    struct LuaWrapper
    {
        T* ref;
    };
    
    
    template <typename T>
    struct LuaValue
    {
        static void Push(lua_State* L, T value)
        {
            lua_pushnil(L);
        }
    };
    
    
    template <typename T>
    struct LuaValue<T*>
    {
        static void Push(lua_State* L, T* value)
        {
            if (value != nullptr)
            {
                value->Retain();
                
                void* mem = lua_newuserdata(L, sizeof(LuaWrapper<T>));
                LuaWrapper<T>* o = new(mem) LuaWrapper<T>();
                o->ref = value;
                std::string type = value->GetClass()->GetFullName();
                luaL_getmetatable(L, type.c_str());
                lua_setmetatable(L, -2);
            }
            else
            {
                lua_pushnil(L);
            }
        }
    };
    
    
    template <>
    struct LuaValue<bool>
    {
        static void Push(lua_State* L, bool value)
        {
            lua_pushboolean(L, value);
        }
    };
    
    
    template <>
    struct LuaValue<int8_t>
    {
        static void Push(lua_State* L, int8_t value)
        {
            lua_pushinteger(L, value);
        }
    };
    
    
    template <>
    struct LuaValue<uint8_t>
    {
        static void Push(lua_State* L, uint8_t value)
        {
            lua_pushinteger(L, value);
        }
    };
    
    
    template <>
    struct LuaValue<int16_t>
    {
        static void Push(lua_State* L, int16_t value)
        {
            lua_pushinteger(L, value);
        }
    };
    
    
    template <>
    struct LuaValue<uint16_t>
    {
        static void Push(lua_State* L, uint16_t value)
        {
            lua_pushinteger(L, value);
        }
    };
    
    
    template <>
    struct LuaValue<int32_t>
    {
        static void Push(lua_State* L, int32_t value)
        {
            lua_pushinteger(L, value);
        }
    };
    
    
    template <>
    struct LuaValue<uint32_t>
    {
        static void Push(lua_State* L, uint32_t value)
        {
            lua_pushinteger(L, value);
        }
    };
    
    
    template <>
    struct LuaValue<int64_t>
    {
        static void Push(lua_State* L, int64_t value)
        {
            lua_pushinteger(L, (lua_Integer)value);
        }
    };
    
    
    template <>
    struct LuaValue<uint64_t>
    {
        static void Push(lua_State* L, uint64_t value)
        {
            lua_pushinteger(L, (lua_Integer)value);
        }
    };
    
    
    template <>
    struct LuaValue<float>
    {
        static void Push(lua_State* L, float value)
        {
            lua_pushnumber(L, value);
        }
    };
    
    
    template <>
    struct LuaValue<double>
    {
        static void Push(lua_State* L, double value)
        {
            lua_pushnumber(L, value);
        }
    };
    
    
    template <>
    struct LuaValue<std::string>
    {
        static void Push(lua_State* L, const std::string& value)
        {
            lua_pushstring(L, value.c_str());
        }
    };
    
    
    template <>
    struct LuaValue<vec2>
    {
        static void Push(lua_State* L, const vec2& value)
        {
            vec2* mem = (vec2*)lua_newuserdata(L, sizeof(vec2));
            new(mem) vec2(value);
            luaL_getmetatable(L, "vec2");
            lua_setmetatable(L, -2);
        }
    };
    
    
    template <>
    struct LuaValue<vec3>
    {
        static void Push(lua_State* L, const vec3& value)
        {
            vec3* mem = (vec3*)lua_newuserdata(L, sizeof(vec3));
            new(mem) vec3(value);
            luaL_getmetatable(L, "vec3");
            lua_setmetatable(L, -2);
        }
    };
    
    
    template <>
    struct LuaValue<vec4>
    {
        static void Push(lua_State* L, const vec4& value)
        {
            vec4* mem = (vec4*)lua_newuserdata(L, sizeof(vec4));
            new(mem) vec4(value);
            luaL_getmetatable(L, "vec4");
            lua_setmetatable(L, -2);
        }
    };
    
    
    template <>
    struct LuaValue<mat4>
    {
        static void Push(lua_State* L, const mat4& value)
        {
            mat4* mem = (mat4*)lua_newuserdata(L, sizeof(mat4));
            new(mem) mat4(value);
            luaL_getmetatable(L, "mat4");
            lua_setmetatable(L, -2);
        }
    };
    
    
    template <>
    struct LuaValue<rect>
    {
        static void Push(lua_State* L, const rect& value)
        {
            rect* mem = (rect*)lua_newuserdata(L, sizeof(rect));
            new(mem) rect(value);
            luaL_getmetatable(L, "rect");
            lua_setmetatable(L, -2);
        }
    };
    
    
    template <>
    struct LuaValue<quat>
    {
        static void Push(lua_State* L, const quat& value)
        {
            quat* mem = (quat*)lua_newuserdata(L, sizeof(quat));
            new(mem) quat(value);
            luaL_getmetatable(L, "quat");
            lua_setmetatable(L, -2);
        }
    };
    
    template <>
    struct LuaValue<Var>
    {
        static void Push(lua_State* L, const Var& value)
        {
            switch (value.GetType())
            {
                case eType_Null:
                    lua_pushnil(L);
                    break;
                case eType_Boolean:
                    lua_pushboolean(L, value);
                    break;
                case eType_I8:
                case eType_U8:
                case eType_I16:
                case eType_U16:
                case eType_I32:
                case eType_U32:
                case eType_I64:
                case eType_U64:
                    lua_pushinteger(L, (lua_Integer)value.GetI64());
                    break;
                case eType_F32:
                case eType_F64:
                    lua_pushnumber(L, value);
                    break;
                case eType_String:
                    lua_pushstring(L, value.GetString().c_str());
                    break;
                case eType_Vec2:
                    LuaValue<vec2>::Push(L, value);
                    break;
                case eType_Vec3:
                    LuaValue<vec3>::Push(L, value);
                    break;
                case eType_Vec4:
                    LuaValue<vec4>::Push(L, value);
                    break;
                case eType_Quat:
                    LuaValue<quat>::Push(L, value);
                    break;
                case eType_Rect:
                    LuaValue<rect>::Push(L, value);
                    break;
                case eType_Mat4:
                    LuaValue<mat4>::Push(L, value);
                    break;
                case eType_I8Array:
                    LuaValue<iI8Array*>::Push(L, value);
                    break;
                case eType_I16Array:
                    LuaValue<iI16Array*>::Push(L, value);
                    break;
                case eType_I32Array:
                    LuaValue<iI32Array*>::Push(L, value);
                    break;
                case eType_I64Array:
                    LuaValue<iI64Array*>::Push(L, value);
                    break;
                case eType_U8Array:
                    LuaValue<iU8Array*>::Push(L, value);
                    break;
                case eType_U16Array:
                    LuaValue<iU16Array*>::Push(L, value);
                    break;
                case eType_U32Array:
                    LuaValue<iU32Array*>::Push(L, value);
                    break;
                case eType_U64Array:
                    LuaValue<iU64Array*>::Push(L, value);
                    break;
                case eType_RectArray:
                    LuaValue<iRectArray*>::Push(L, value);
                    break;
                case eType_QuatArray:
                    LuaValue<iQuatArray*>::Push(L, value);
                    break;
                case eType_Mat4Array:
                    LuaValue<iMat4Array*>::Push(L, value);
                    break;
                case eType_Vec2Array:
                    LuaValue<iVec2Array*>::Push(L, value);
                    break;
                case eType_Vec3Array:
                    LuaValue<iVec3Array*>::Push(L, value);
                    break;
                case eType_Vec4Array:
                    LuaValue<iVec4Array*>::Push(L, value);
                    break;
                default:
                    LuaValue<iRefObject*>::Push(L, value);
                    break;
            }
        }
    };
}


#endif

























