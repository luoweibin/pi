/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 wilburluo     2017.3.23   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>
#include <lua.hpp>
#include "LuaValue.h"
#include "LuaArg.h"

using namespace std;

NSPI_BEGIN()


static int Math_Cross(lua_State* L)
{
    vec3 left = LuaArg<vec3>::Value(L, 1);
    vec3 right = LuaArg<vec3>::Value(L, 2);
    vec3 v = piglm::cross(left, right);
    LuaValue<vec3>::Push(L, v);
    return 1;
}


template <typename T>
static int Math_DotTpl(lua_State* L)
{
    T left = LuaArg<T>::Value(L, 1);
    T right = LuaArg<T>::Value(L, 2);
    float v = piglm::dot(left, right);
    LuaValue<float>::Push(L, v);
    return 1;
}

static int Math_Dot(lua_State* L)
{
    if (luaL_getmetafield(L, 1, "_class") != LUA_TNIL)
    {
        SmartPtr<iClass> klass = (iClass*)lua_touserdata(L, -1);
        lua_pop(L, 1);
        
        if (klass == PrimitiveClass::Vec2())
        {
            return Math_DotTpl<vec2>(L);
        }
        else if (klass == PrimitiveClass::Vec3())
        {
            return Math_DotTpl<vec3>(L);
        }
        else if (klass == PrimitiveClass::Vec4())
        {
            return Math_DotTpl<vec4>(L);
        }
    }
    
    return 0;
}



template <typename T>
static int Math_DistanceTpl(lua_State* L)
{
    T left = LuaArg<T>::Value(L, 1);
    T right = LuaArg<T>::Value(L, 2);
    float v = piglm::distance(left, right);
    LuaValue<float>::Push(L, v);
    return 1;
}

static int Math_Distance(lua_State* L)
{
    if (luaL_getmetafield(L, 1, "_class") != LUA_TNIL)
    {
        SmartPtr<iClass> klass = (iClass*)lua_touserdata(L, -1);
        lua_pop(L, 1);
        
        if (klass == PrimitiveClass::Vec2())
        {
            return Math_DistanceTpl<vec2>(L);
        }
        else if (klass == PrimitiveClass::Vec3())
        {
            return Math_DistanceTpl<vec3>(L);
        }
        else if (klass == PrimitiveClass::Vec4())
        {
            return Math_DistanceTpl<vec4>(L);
        }
    }
    
    return 0;
}



template <typename T>
static int Math_LengthTpl(lua_State* L)
{
    T left = LuaArg<T>::Value(L, 1);
    float v = piglm::length(left);
    LuaValue<float>::Push(L, v);
    return 1;
}


static int Math_Length(lua_State* L)
{
    if (luaL_getmetafield(L, 1, "_class") != LUA_TNIL)
    {
        SmartPtr<iClass> klass = (iClass*)lua_touserdata(L, -1);
        lua_pop(L, 1);
        
        if (klass == PrimitiveClass::Vec2())
        {
            return Math_LengthTpl<vec2>(L);
        }
        else if (klass == PrimitiveClass::Vec3())
        {
            return Math_LengthTpl<vec3>(L);
        }
        else if (klass == PrimitiveClass::Vec4())
        {
            return Math_LengthTpl<vec4>(L);
        }
    }
    
    return 0;
}


template <typename T>
static int Math_ReflectTpl(lua_State* L)
{
    T left = LuaArg<T>::Value(L, 1);
    T right = LuaArg<T>::Value(L, 2);
    T v = piglm::reflect(left, right);
    LuaValue<T>::Push(L, v);
    return 1;
}

static int Math_Reflect(lua_State* L)
{
    if (luaL_getmetafield(L, 1, "_class") != LUA_TNIL)
    {
        SmartPtr<iClass> klass = (iClass*)lua_touserdata(L, -1);
        lua_pop(L, 1);
        
        if (klass == PrimitiveClass::Vec2())
        {
            return Math_ReflectTpl<vec2>(L);
        }
        else if (klass == PrimitiveClass::Vec3())
        {
            return Math_ReflectTpl<vec3>(L);
        }
        else if (klass == PrimitiveClass::Vec4())
        {
            return Math_ReflectTpl<vec4>(L);
        }
    }
    
    return 0;
}


template <typename T>
static int Math_RefractTpl(lua_State* L)
{
    T left = LuaArg<T>::Value(L, 1);
    T right = LuaArg<T>::Value(L, 2);
    float eta = LuaArg<float>::Value(L, 3);
    T v = piglm::refract(left, right, eta);
    LuaValue<T>::Push(L, v);
    return 1;
}

static int Math_Refract(lua_State* L)
{
    if (luaL_getmetafield(L, 1, "_class") != LUA_TNIL)
    {
        SmartPtr<iClass> klass = (iClass*)lua_touserdata(L, -1);
        lua_pop(L, 1);
        
        if (klass == PrimitiveClass::Vec2())
        {
            return Math_RefractTpl<vec2>(L);
        }
        else if (klass == PrimitiveClass::Vec3())
        {
            return Math_RefractTpl<vec3>(L);
        }
        else if (klass == PrimitiveClass::Vec4())
        {
            return Math_RefractTpl<vec4>(L);
        }
    }
    
    return 0;
}


template <typename T>
static int Math_FaceForwardTpl(lua_State* L)
{
    T N = LuaArg<T>::Value(L, 1);
    T I = LuaArg<T>::Value(L, 2);
    T Nref = LuaArg<T>::Value(L, 3);
    T v = piglm::faceforward(N, I, Nref);
    LuaValue<T>::Push(L, v);
    return 1;
}

static int Math_FaceForward(lua_State* L)
{
    if (luaL_getmetafield(L, 1, "_class") != LUA_TNIL)
    {
        SmartPtr<iClass> klass = (iClass*)lua_touserdata(L, -1);
        lua_pop(L, 1);
        
        if (klass == PrimitiveClass::Vec2())
        {
            return Math_FaceForwardTpl<vec2>(L);
        }
        else if (klass == PrimitiveClass::Vec3())
        {
            return Math_FaceForwardTpl<vec3>(L);
        }
        else if (klass == PrimitiveClass::Vec4())
        {
            return Math_FaceForwardTpl<vec4>(L);
        }
    }
    
    return 0;
}


void Math_Vector_Init(lua_State* L)
{
    lua_pushcfunction(L, Math_Cross);
    lua_setfield(L, -2, "Cross");
    
    lua_pushcfunction(L, Math_Dot);
    lua_setfield(L, -2, "Dot");
    
    lua_pushcfunction(L, Math_Distance);
    lua_setfield(L, -2, "Distance");
    
    lua_pushcfunction(L, Math_Length);
    lua_setfield(L, -2, "Length");
    
    lua_pushcfunction(L, Math_Refract);
    lua_setfield(L, -2, "Refract");
    
    lua_pushcfunction(L, Math_Reflect);
    lua_setfield(L, -2, "Refract");
    
    lua_pushcfunction(L, Math_FaceForward);
    lua_setfield(L, -2, "FaceForward");
}





NSPI_END()






















