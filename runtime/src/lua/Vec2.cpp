/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 wilburluo     2016.10.11   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>
#include <lua.hpp>
#include "LuaValue.h"
#include "LuaArg.h"

using namespace std;

NSPI_BEGIN()

static int vec2_New(lua_State* L)
{
    int top = lua_gettop(L);
    
    vec2 v;
    
    switch (top)
    {
        case 1:
            v.x = lua_tonumber(L, 1);
            v.y = v.x;
            break;
        case 2:
        default:
            v.x = lua_tonumber(L, 1);
            v.y = lua_tonumber(L, 2);
            break;
    }
    
    LuaValue<vec2>::Push(L, v);
    
    return 1;
}

static int vec2_Add(lua_State* L)
{
    vec2 v;
    
    vec2* left = (vec2*)lua_touserdata(L, 1);
    
    if (lua_isuserdata(L, 2))
    {
        vec2* right = (vec2*)lua_touserdata(L, 2);
        v = (*left) + (*right);
    }
    else if (lua_isnumber(L, 2))
    {
        float right = lua_tonumber(L, 2);
        v = (*left) + right;
    }
    
    LuaValue<vec2>::Push(L, v);
    
    return 1;
}

static int vec2_Sub(lua_State* L)
{
    vec2 v;
    
    vec2* left = (vec2*)lua_touserdata(L, 1);
    
    if (lua_isuserdata(L, 2))
    {
        vec2* right = (vec2*)lua_touserdata(L, 2);
        v = (*left) - (*right);
    }
    else if (lua_isnumber(L, 2))
    {
        float right = lua_tonumber(L, 2);
        v = (*left) - right;
    }
    
    LuaValue<vec2>::Push(L, v);
    
    return 1;
}

static int vec2_Mul(lua_State* L)
{
    vec2 v;
    
    vec2* left = (vec2*)lua_touserdata(L, 1);
    
    if (lua_isuserdata(L, 2))
    {
        vec2* right = (vec2*)lua_touserdata(L, 2);
        v = (*left) * (*right);
    }
    else if (lua_isnumber(L, 2))
    {
        float right = lua_tonumber(L, 2);
        v = (*left) * right;
    }
    
    LuaValue<vec2>::Push(L, v);
    
    return 1;
}

static int vec2_Div(lua_State* L)
{
    vec2 v;
    
    vec2* left = (vec2*)lua_touserdata(L, 1);
    
    if (lua_isuserdata(L, 2))
    {
        vec2* right = (vec2*)lua_touserdata(L, 2);
        v = (*left) / (*right);
    }
    else if (lua_isnumber(L, 2))
    {
        float right = lua_tonumber(L, 2);
        v = (*left) / right;
    }
    
    LuaValue<vec2>::Push(L, v);
    
    return 1;
}

static int vec2_Pow(lua_State* L)
{
    vec2* v = (vec2*)lua_touserdata(L, 1);
    vec2 ret(v->x * v->x, v->y * v->y);
    
    LuaValue<vec2>::Push(L, ret);
    
    return 1;
}

static int vec2_Unm(lua_State* L)
{
    vec2* v = (vec2*)lua_touserdata(L, 1);
    vec2 ret(-*v);
    
    LuaValue<vec2>::Push(L, ret);
    
    return 1;
}

static int vec2_Eq(lua_State* L)
{
    vec2* left = (vec2*)lua_touserdata(L, 1);
    vec2* right = (vec2*)lua_touserdata(L, 2);
    bool ret = (left == right);
    lua_pushboolean(L, ret);
    
    return 1;
}

static int vec2_GetX(lua_State* L)
{
    vec2* v = (vec2*)lua_touserdata(L, 1);
    lua_pushnumber(L, v->x);
    return 1;
}

static int vec2_SetX(lua_State* L)
{
    vec2* v = (vec2*)lua_touserdata(L, 1);
    float value = lua_tonumber(L, 2);
    v->x = value;
    return 0;
}

static int vec2_GetY(lua_State* L)
{
    vec2* v = (vec2*)lua_touserdata(L, 1);
    lua_pushnumber(L, v->y);
    return 1;
}

static int vec2_SetY(lua_State* L)
{
    vec2* v = (vec2*)lua_touserdata(L, 1);
    float value = lua_tonumber(L, 2);
    v->y = value;
    return 0;
}

static int vec2_ToString(lua_State* L)
{
    vec2* v = (vec2*)lua_touserdata(L, 1);
    string str = piToString(*v);
    lua_pushstring(L, str.c_str());
    return 1;
}

void vec2_Init(lua_State* L)
{
    luaL_newmetatable(L, "vec2");
    
    lua_pushlightuserdata(L, PrimitiveClass::Vec2());
    lua_setfield(L, -2, "_class");
    
    lua_pushcfunction(L, vec2_Add);
    lua_setfield(L, -2, "__add");
    
    lua_pushcfunction(L, vec2_Sub);
    lua_setfield(L, -2, "__sub");
    
    lua_pushcfunction(L, vec2_Mul);
    lua_setfield(L, -2, "__mul");
    
    lua_pushcfunction(L, vec2_Div);
    lua_setfield(L, -2, "__div");
    
    lua_pushcfunction(L, vec2_Pow);
    lua_setfield(L, -2, "__pow");
    
    lua_pushcfunction(L, vec2_Unm);
    lua_setfield(L, -2, "__unm");
    
    lua_pushcfunction(L, vec2_Eq);
    lua_setfield(L, -2, "__eq");
    
    lua_pushcfunction(L, vec2_ToString);
    lua_setfield(L, -2, "__tostring");
    
    // __index table
    lua_newtable(L);
    
    lua_pushcfunction(L, vec2_GetX);
    lua_setfield(L, -2, "GetX");
    
    lua_pushcfunction(L, vec2_SetX);
    lua_setfield(L, -2, "SetX");
    
    lua_pushcfunction(L, vec2_GetY);
    lua_setfield(L, -2, "GetY");
    
    lua_pushcfunction(L, vec2_SetY);
    lua_setfield(L, -2, "SetY");
    
    lua_setfield(L, -2, "__index");
    
    lua_pop(L, 1);
    
    // register construtor
    lua_pushcfunction(L, vec2_New);
    lua_setfield(L, -2, "vec2");
}


NSPI_END()



























