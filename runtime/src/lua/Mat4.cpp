/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 wilburluo     2016.10.11   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>
#include <lua.hpp>
#include "LuaValue.h"
#include "LuaArg.h"

using namespace std;

NSPI_BEGIN()

static int mat4_New(lua_State* L)
{
    int top = lua_gettop(L);
    
    float v[] = {
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1,
    };
    
    for (int32_t i = 0; i < 16; ++i)
    {
        if (top > i && lua_isnumber(L, i + 1))
        {
            v[i] = lua_tonumber(L, i + 1);
        }
    }
    
    mat4 m(v[0],  v[1],  v[2],  v[3],
           v[4],  v[5],  v[6],  v[7],
           v[8],  v[9],  v[10], v[11],
           v[12], v[13], v[14], v[15]);
    
    LuaValue<mat4>::Push(L, m);
    
    return 1;
}

static int mat4_Add(lua_State* L)
{
    mat4 v;
    mat4* left = (mat4*)lua_touserdata(L, 1);
    
    mat4* right = (mat4*)lua_touserdata(L, 2);
    v = (*left) + (*right);
    
    LuaValue<mat4>::Push(L, v);
    
    return 1;
}

static int mat4_Sub(lua_State* L)
{
    mat4 v;
    mat4* left = (mat4*)lua_touserdata(L, 1);
    
    mat4* right = (mat4*)lua_touserdata(L, 2);
    v = (*left) - (*right);
    
    LuaValue<mat4>::Push(L, v);
    
    return 1;
}

static int mat4_Mul(lua_State* L)
{
    mat4 v;
    mat4* left = (mat4*)lua_touserdata(L, 1);
    
    mat4* right = (mat4*)lua_touserdata(L, 2);
    v = (*left) * (*right);
    
    LuaValue<mat4>::Push(L, v);
    
    return 1;
}

static int mat4_Div(lua_State* L)
{
    mat4 v;
    mat4* left = (mat4*)lua_touserdata(L, 1);
    
    mat4* right = (mat4*)lua_touserdata(L, 2);
    v = (*left) / (*right);
    
    LuaValue<mat4>::Push(L, v);
    
    return 1;
}

static int mat4_Eq(lua_State* L)
{
    mat4* left = (mat4*)lua_touserdata(L, 1);
    mat4* right = (mat4*)lua_touserdata(L, 2);
    bool ret = (left == right);
    lua_pushboolean(L, ret);
    
    return 1;
}

static int mat4_Index(lua_State* L)
{
    mat4* v = (mat4*)lua_touserdata(L, 1);
    
    int32_t index = (int32_t)lua_tointeger(L, 2);
    if (index >= 0 && index < 16)
    {
        lua_pushnumber(L, (*v)[index / 4][index % 4]);
    }
    else
    {
        lua_pushnumber(L, 0);
    }
    
    return 1;
}

static int mat4_Newindex(lua_State* L)
{
    mat4* v = (mat4*)lua_touserdata(L, 1);
    
    float value = 0;
    value = lua_tonumber(L, 3);
    
    int32_t index = (int32_t)lua_tointeger(L, 2);
    if (index >= 0 && index < 2)
    {
        (*v)[index / 4][index % 4] = value;
    }
    
    return 0;
}

static int mat4_ToString(lua_State* L)
{
    mat4* v = (mat4*)lua_touserdata(L, 1);
    string str = piToString(*v);
    lua_pushstring(L, str.c_str());
    return 1;
}


static int mat4_Inverse(lua_State* L)
{
    mat4* v = (mat4*)lua_touserdata(L, 1);
    mat4 ret = piglm::inverse(*v);
    LuaValue<mat4>::Push(L, ret);
    return 1;
}


void mat4_Init(lua_State* L)
{
    luaL_newmetatable(L, "mat4");
    
    lua_pushlightuserdata(L, PrimitiveClass::Mat4());
    lua_setfield(L, -2, "_class");
    
    lua_pushcfunction(L, mat4_Add);
    lua_setfield(L, -2, "__add");
    
    lua_pushcfunction(L, mat4_Sub);
    lua_setfield(L, -2, "__sub");
    
    lua_pushcfunction(L, mat4_Mul);
    lua_setfield(L, -2, "__mul");
    
    lua_pushcfunction(L, mat4_Div);
    lua_setfield(L, -2, "__div");
    
    lua_pushcfunction(L, mat4_Eq);
    lua_setfield(L, -2, "__eq");
    
    lua_pushcfunction(L, mat4_ToString);
    lua_setfield(L, -2, "__tostring");
    
    lua_pushcfunction(L, mat4_Index);
    lua_setfield(L, -2, "__index");
    
    lua_pushcfunction(L, mat4_Newindex);
    lua_setfield(L, -2, "__newindex");
    
    lua_pop(L, 1);
    
    // register construtor
    lua_pushcfunction(L, mat4_New);
    lua_setfield(L, -2, "mat4");
    
    lua_pushcfunction(L, mat4_Inverse);
    lua_setfield(L, -2, "Inverse");
}



NSPI_END()













