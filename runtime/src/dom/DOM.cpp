/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2012.2.27   0.1     Create
 ******************************************************************************/
#include <pi/DOM.h>

using namespace std;

NSPI_BEGIN()

template <class T>
class DOMNode : public T
{
public:
    string mNodeValue;
    string mNodeName;
    iDOMNode* mParentNode;
    iDOMDocument* mDocument;
    iDOMNode* mPreviousSibling;
    iDOMNode* mNextSibling;
    SmartPtr<iDOMNode> mFirstChild;
    
    vector<SmartPtr<iDOMNode>> mChildrens;
    
public:
    DOMNode():mDocument(nullptr), mParentNode(nullptr)
    {
        mPreviousSibling = this;
        mNextSibling = this;
    }
    
    virtual ~DOMNode()
    {
    }
    
    virtual string GetText() const
    {
        SmartPtr<iDOMText> text = dynamic_cast<iDOMText*>(GetFirstChild());
        return !text.IsNull() ? text->GetData() : "";
    }
    
    virtual string GetNodeValue() const
    {
        
        return mNodeName;
    }

    virtual void SetNodeValue(const string &value)
    {
        mNodeName = value;
    }
    
    virtual string GetNodeName() const
    {
        return mNodeName;
    }
    
    virtual void SetNodeName(const string &name)
    {
        mNodeName = name;
    }
    
    virtual iDOMNode* GetFirstChild() const
    {
        return mFirstChild->GetNextSibling();
    }
    
    virtual iDOMNode* GetLastChild() const
    {
        return mFirstChild->GetPreviousSibling();
    }
    
    virtual iDOMNodeArray* GetChildNodes() const
    {
        SmartPtr<iDOMNodeArray> childNodes = CreateDOMNodeArray();
        SmartPtr<iDOMNode> it = GetFirstChildNode();
        while (it != NullNode())
        {
            childNodes->PushBack(it);
            it = dynamic_cast<iDOMNode*>(it->GetNextSibling());
        }
        return childNodes.PtrAndSetNull();
    }

    virtual iDOMNode* GetParent() const
    {
        return mParentNode;
    }

    virtual iDOMNode* GetNextSibling() const
    {
        return mNextSibling;
    }

    virtual iDOMNode* GetPreviousSibling() const
    {
        return mPreviousSibling;
    }

    virtual iDOMNode* InsertBefore(iDOMNode *newChild, iDOMNode *refChild)
    {
        piAssert(newChild != nullptr, nullptr);
        
        SmartPtr<iDOMNode> newNode = dynamic_cast<iDOMNode*>(newChild);
        newNode->SetParent(this);
        
        mChildrens.push_back(newNode);
        
        if (refChild == nullptr)
        {
            SmartPtr<iDOMNode> lastNode = GetLastChildNode();
            lastNode->SetNextSibling(newNode);
            newNode->SetPreviousSibling(lastNode);
            newNode->SetNextSibling(mFirstChild);
            mFirstChild->SetPreviousSibling(newNode);
        }
        else
        {
            SmartPtr<iDOMNode> refNode = dynamic_cast<DOMNode*>(refChild);
            SmartPtr<iDOMNode> prevNode = refNode->GetPreviousSibling();
            
            prevNode->SetNextSibling(newNode);
            refNode->SetPreviousSibling(newNode);
            
            newNode->SetPreviousSibling(prevNode);
            newNode->SetNextSibling(refNode);
        }
        
        return newNode;
    }
    
    virtual iDOMNode* ReplaceChild(iDOMNode *newChild, iDOMNode *oldChild)
    {
        piAssert(newChild != nullptr, nullptr);
        piAssert(oldChild != nullptr, nullptr);
        
        SmartPtr<iDOMNode> newNode = dynamic_cast<iDOMNode*>(newChild);
        SmartPtr<iDOMNode> oldNode = dynamic_cast<iDOMNode*>(oldChild);
        SmartPtr<iDOMNode> prevNode = oldNode->GetPreviousSibling();
        SmartPtr<iDOMNode> nextNode = oldNode->GetNextSibling();
        
        prevNode->SetNextSibling(newNode);
        nextNode->SetPreviousSibling(newNode);
        
        newNode->SetPreviousSibling(prevNode);
        newNode->SetNextSibling(nextNode);
        
        mChildrens.push_back(newNode);
        UnrefChild(oldNode);
        
        return newNode.PtrAndSetNull();
    }
    

    virtual iDOMNode* RemoveChild(iDOMNode *oldChild)
    {
        piAssert(oldChild != nullptr, nullptr);
        
        SmartPtr<iDOMNode> oldNode = dynamic_cast<iDOMNode*>(oldChild);
        SmartPtr<iDOMNode> prevNode = oldNode->GetPreviousSibling();
        SmartPtr<iDOMNode> nextNode = oldNode->GetNextSibling();
        
        piAssert(!prevNode.IsNull(), nullptr);
        piAssert(!nextNode.IsNull(), nullptr);
        
        prevNode->SetNextSibling(nextNode);
        nextNode->SetPreviousSibling(prevNode);
        
        oldNode->SetPreviousSibling(nullptr);
        oldNode->SetNextSibling(nullptr);
        
        UnrefChild(oldNode);
        
        return oldNode.PtrAndSetNull();
    }
    
    virtual iDOMNode* AppendChild(iDOMNode *newChild)
    {
        return InsertBefore(newChild, nullptr);
    }

    virtual bool HasChildNodes() const
    {
        return GetFirstChild() != NullNode();
    }
    
    virtual iDOMDocument* GetDocument() const
    {
        return mDocument;
    }
    
    virtual iDOMNode* NullNode() const
    {
        return mFirstChild;
    }
    
    virtual iDOMElementArray* GetElementsByTagName(const string &name, bool recursively) const
    {
        SmartPtr<iDOMElementArray> elements = CreateDOMElementArray();
        FindElementsByTagName(name, elements, recursively);
        return elements.PtrAndSetNull();
    }
    
    virtual iDOMElement* GetElementByTagName(const string& name, bool recursively) const
    {
        SmartPtr<iDOMElementArray> elements = GetElementsByTagName(name, recursively);
        if (!elements->IsEmpty())
        {
            SmartPtr<iDOMElement> ret = elements->GetFront();
            elements->Clear();
            return ret.PtrAndSetNull();
        }
        else
        {
            return nullptr;
        }
    }
    
    virtual iDOMElement* GetElementByTagName(const string &name) const
    {
        SmartPtr<iDOMElementArray> elements = CreateDOMElementArray();
        FindElementsByTagName(name, elements, false);
        if (!elements->IsEmpty())
        {
            SmartPtr<iDOMElement> ret = elements->GetFront();
            elements->Clear();
            return ret.PtrAndSetNull();
        }
        else
        {
            return nullptr;
        }
    }
    
    virtual void SetParent(iDOMNode *parent)
    {
        mParentNode = parent;
    }
    
    virtual void SetNextSibling(iDOMNode *node)
    {
        mNextSibling = node;
    }
    
    virtual void SetPreviousSibling(iDOMNode *node)
    {
        mPreviousSibling = node;
    }
    
    virtual void FindElementsByTagName(const string &name,
                                       iDOMElementArray* elements,
                                       bool recursively) const
    {
        SmartPtr<iDOMNode> node = GetFirstChildNode();
        while (node != NullNode())
        {
            if (node->GetNodeType() == eDOMNode_Element)
            {
                SmartPtr<iDOMElement> el = dynamic_cast<iDOMElement*>(node.Ptr());
                if (el->GetTagName() == name)
                {
                    elements->PushBack(el);
                }
            }
            
            if (recursively)
            {
                node->FindElementsByTagName(name, elements, true);
            }
            
            node = node->GetNextSibling();
        }
    }
    
private:
    iDOMNode* GetLastChildNode() const
    {
        return mFirstChild->GetPreviousSibling();
    }
    
    iDOMNode* GetFirstChildNode() const
    {
        return mFirstChild->GetNextSibling();
    }
    
    void UnrefChild(iDOMNode* node)
    {
        mChildrens.erase(std::remove(mChildrens.begin(), mChildrens.end(), node), mChildrens.end());
    }
};


class DOMAttr : public DOMNode<iDOMAttr>
{
private:
    string mName;
    string mValue;
    
public:
    DOMAttr(const string &name, const string &value = string())
    {
        mName = name;
        mValue = value;
    }
    
    virtual ~DOMAttr()
    {
    }
    
    void Init()
    {
        mFirstChild = new DOMAttr("");
    }
    
    virtual string GetName() const
    {
        return mName;
    }
    
    virtual string GetValue() const
    {
        return mValue;
    }
    
    virtual void SetValue(const string &value)
    {
        mValue = mName;
    }
    
    virtual int GetNodeType() const
    {
        return eDOMNode_Attr;
    }
};


class DOMText : public DOMNode<iDOMText>
{
private:
    string mData;
    
public:
    DOMText(const string &data)
    {
        mData = data;
    }
    
    virtual ~DOMText()
    {
    }
    
    void Init()
    {
        mFirstChild = new DOMText("");
    }
    
    virtual string GetData() const
    {
        return mData;
    }
    
    virtual void SetData(const string &data)
    {
        mData = data;
    }
    
    virtual int GetNodeType() const
    {
        return eDOMNode_Text;
    }
};


class DOMCDATA : public DOMNode<iDOMCDATA>
{
private:
    string mData;
    
public:
    DOMCDATA(const string &data)
    {
        mData = data;
    }
    
    virtual ~DOMCDATA()
    {
    }
    
    void Init()
    {
        mFirstChild = new DOMCDATA("");
    }
    
    virtual string GetData() const
    {
        return mData;
    }
    
    virtual void SetData(const string &data)
    {
        mData = data;
    }
    
    virtual int GetNodeType() const
    {
        return eDOMNode_CDATA;
    }
};


class DOMElement : public DOMNode<iDOMElement>
{
private:
    string mTagName;
    
    typedef map<string, SmartPtr<iDOMAttr>> AttrMap;
    AttrMap mAttrs;
    
public:
    DOMElement(const string &tagName)
    {
        mTagName = tagName;
    }
    
    virtual ~DOMElement()
    {
    }
    
    void Init()
    {
        mFirstChild = new DOMElement("");
    }

    virtual string GetTagName() const
    {
        return mTagName;
    }

    virtual string GetAttr(const string &name) const
    {
        piAssert(!name.empty(), string());
        
        auto it = mAttrs.find(name);
        return it != mAttrs.end() ? it->second->GetValue() : string();
    }

    virtual void SetAttr(const string &name, const string &value)
    {
        piAssert(!name.empty(), ;);
        if (mAttrs.find(name) != mAttrs.end())
        {
            mAttrs[name]->SetValue(value);
        }
        else
        {
            SmartPtr<DOMAttr> attr = new DOMAttr(name, value);
            mAttrs[name] = attr;
        }
    }

    virtual void RemoveAttr(const string &name)
    {
        mAttrs.erase(name);
    }

    virtual iDOMAttr* GetAttrNode(const string &name) const
    {
        auto it = mAttrs.find(name);
        return it != mAttrs.end() ? it->second : nullptr;
    }

    virtual iDOMAttr* SetAttrNode(iDOMAttr *attr)
    {
        piAssert(attr != nullptr, nullptr);
        mAttrs[attr->GetName()] = dynamic_cast<DOMAttr*>(attr);
        return attr;
    }

    virtual iDOMAttrArray* GetAttrs() const
    {
        SmartPtr<iDOMAttrArray> attrs = CreateDOMAttrArray();
        for (auto p : mAttrs)
        {
            attrs->PushBack(p.second);
        }
        return attrs.PtrAndSetNull();
    }

    virtual iDOMAttr* RemoveAttrNode(iDOMAttr *attr)
    {
        piAssert(attr != nullptr, nullptr);
        map<string, SmartPtr<iDOMAttr>>::iterator it = mAttrs.find(attr->GetName());
        if (it != mAttrs.end())
        {
            SmartPtr<iDOMAttr> attr = it->second;
            return attr.PtrAndSetNull();
        }
        else
        {
            return nullptr;
        }
    }
    
    virtual int GetNodeType() const
    {
        return eDOMNode_Element;
    }
};


class DOMDocument : public DOMNode<iDOMDocument>
{
private:
    SmartPtr<iDOMElement> mDocElement;
    
public:
    DOMDocument()
    {
    }
    
    virtual ~DOMDocument()
    {
    }
    
    void Init()
    {
        mFirstChild = new DOMDocument();
    }

    virtual iDOMElement* GetDocumentElement() const
    {
        return mDocElement;
    }
    
    virtual void SetDocumentElement(iDOMElement* element)
    {
        mDocElement = element;
    }

    virtual iDOMText* CreateText(const string &data)
    {
        SmartPtr<DOMText> text = new DOMText(data);
        text->Init();
        text->mDocument = this;
        return text.PtrAndSetNull();
    }

    virtual iDOMCDATA* CreateCDATA(const string &data)
    {
        SmartPtr<DOMCDATA> cdata = new DOMCDATA(data);
        cdata->Init();
        cdata->mDocument = this;
        return cdata.PtrAndSetNull();
    }

    virtual iDOMAttr* CreateAttr(const string &name, const string &value)
    {
        piAssert(!name.empty(), nullptr);
        
        SmartPtr<DOMAttr> attr = new DOMAttr(name, value);
        attr->Init();
        attr->mDocument = this;
        return attr.PtrAndSetNull();
    }

    virtual iDOMElement* CreateElement(const string &tagName)
    {
        piAssert(!tagName.empty(), nullptr);
        
        SmartPtr<DOMElement> el = new DOMElement(tagName);
        el->Init();
        el->mDocument = this;
        return el.PtrAndSetNull();
    }
    
    virtual int GetNodeType() const
    {
        return eDOMNode_Document;
    }
};


iDOMDocument* CreateDOMDocument()
{
    SmartPtr<DOMDocument> doc = new DOMDocument();
    doc->Init();
    return doc.PtrAndSetNull();
}


NSPI_END()

















