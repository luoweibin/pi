/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2012.3.1   0.1     Create
 ******************************************************************************/
#include <pi/DOM.h>

#include <tinyxml2.h>

using namespace std;
using namespace tinyxml2;

NSPI_BEGIN()

class ElementVisitor : public XMLVisitor
{
private:
    SmartPtr<iDOMDocument> mDoc;
    list<const XMLElement*> mElStack;
    list<SmartPtr<iDOMElement>> mNodeStack;
    
public:
    ElementVisitor()
    {
        mDoc = CreateDOMDocument();
    }
    
    virtual ~ElementVisitor()
    {
    }
    
    iDOMDocument *PopDocument()
    {
        return mDoc.PtrAndSetNull();
    }
    
    /// Visit an element.
    virtual bool VisitEnter( const XMLElement& el, const XMLAttribute* firstAttr)
    {
        SmartPtr<iDOMElement> newEl = mDoc->CreateElement(el.Name());
        
        const XMLAttribute* attr = firstAttr;
        while (attr != nullptr)
        {
            newEl->SetAttr(attr->Name(), attr->Value());
            attr = attr->Next();
        }
        
        if (!mElStack.empty())
        {
            if (el.Parent() == mElStack.back())
            {
                PILOGD(TT("XML"),
                       TT("%s >> %s"),
                       mNodeStack.back()->GetTagName().c_str(),
                       newEl->GetTagName().c_str());
                mNodeStack.back()->AppendChild(newEl);
            }
            else
            {
                PILOGD(TT("XML"),
                       TT("%s >> %s"),
                       dynamic_cast<iDOMElement*>(mNodeStack.back()->GetParent())->GetTagName().c_str(),
                       newEl->GetTagName().c_str());
                mNodeStack.back()->GetParent()->AppendChild(newEl);
            }
        }
        else
        {
            mDoc->SetDocumentElement(newEl);
            PILOGD(TT("XML"), ">> %s", newEl->GetTagName().c_str());
        }
        
        mElStack.push_back(&el);
        mNodeStack.push_back(newEl);
        
        return true;
    }
    
    virtual bool Visit( const XMLText& text)
    {
        SmartPtr<iDOMText> newText;
        if (text.CData())
        {
            newText = mDoc->CreateCDATA(text.Value());
        }
        else
        {
            newText = mDoc->CreateText(text.Value());
        }
        mNodeStack.back()->AppendChild(newText);
        return true;
    }
    
    /// Visit an element.
    virtual bool VisitExit( const XMLElement& el)
    {
        if (!mElStack.empty() && &el == mElStack.back())
        {
            mElStack.pop_back();
            mNodeStack.pop_back();
        }
        return true;
    }
};

#if 1

iDOMDocument* piParseXML(iStream *input)
{
    piAssert(input != nullptr, nullptr);
    
    int64_t size = input->GetSize();
    piCheck(size > 0, nullptr);
    
    SmartPtr<iMemory> buffer = CreateMemory(size);
    piCheck(!buffer.IsNull(), nullptr);
    input->Read(buffer->Ptr(), buffer->Size());
    
    XMLDocument doc;
    piCheck(doc.Parse(buffer->Ptr(), (size_t)buffer->Size()) == XML_SUCCESS, nullptr);
    
    const XMLElement *docEl = doc.RootElement();
    piCheck(docEl != nullptr, nullptr);
    
    ElementVisitor visitor;
    doc.Accept(&visitor);
    
    return visitor.PopDocument();
}

#endif


//==============================================================================

static void WriteIndent(iStream *output, int32_t level)
{
    for (int32_t i = 0; i < level; ++i)
    {
        output->WriteString("    ");
    }
}

static void SerializeCDATA(iDOMCDATA *cdata, iStream *output)
{
    output->WriteString(piFormat("<![CDATA[%s]]>", cdata->GetData().c_str()));
}

static void SerializeText(iDOMText *text, iStream *output)
{
    output->WriteString(text->GetData());
}

static void SerializeElementXML(iDOMElement *el, iStream *output, int32_t level)
{
    WriteIndent(output, level);
    output->WriteString(piFormat("<%s", el->GetTagName().c_str()));
    
    auto attrs = el->GetAttrs();
    for (int32_t i = 0; i < attrs->GetCount(); ++i)
    {
        SmartPtr<iDOMAttr> attr = attrs->GetItem(i);
        output->WriteString(piFormat(" %s=\"%s\"", attr->GetName().c_str(), attr->GetValue().c_str()));
    }
    
    auto childNodes = el->GetChildNodes();
    if (childNodes->IsEmpty())
    {
        output->WriteString(" />\n");
    }
    else
    {
        output->WriteString(">");
        
        for (auto i = 0; i < childNodes->GetCount(); ++i)
        {
            SmartPtr<iDOMNode> node = childNodes->GetItem(i);
            if (i == 0)
            {
                switch (node->GetNodeType())
                {
                    case eDOMNode_Text:
                    case eDOMNode_CDATA:
                        break;
                    default:
                        output->WriteString("\n");
                        break;
                }
            }
            
            switch (node->GetNodeType())
            {
                case eDOMNode_Element:
                    SerializeElementXML(dynamic_cast<iDOMElement*>(node.Ptr()), output, level + 1);
                    break;
                case eDOMNode_CDATA:
                    SerializeCDATA(dynamic_cast<iDOMCDATA*>(node.Ptr()), output);
                    break;
                case eDOMNode_Text:
                    SerializeText(dynamic_cast<iDOMText*>(node.Ptr()), output);
                    break;
                default:
                    break;
            }
        }
        
        SmartPtr<iDOMNode> node = childNodes->GetFront();
        switch (node->GetNodeType())
        {
            case eDOMNode_Text:
            case eDOMNode_CDATA:
                break;
            default:
                WriteIndent(output, level);
                break;
        }
        
        output->WriteString(piFormat("</%s>\n", el->GetTagName().c_str()));
    }
}

void piSerializeXML(iDOMDocument *doc, iStream *output)
{
    piAssert(doc != nullptr, ;);
    piAssert(output != nullptr, ;);
    
    SmartPtr<iDOMElement> docEl = doc->GetDocumentElement();
    piCheck(!docEl.IsNull(), ;);
    
    SerializeElementXML(docEl, output, 0);
}


NSPI_END()





















