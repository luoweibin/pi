/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.23   1     Create
 ******************************************************************************/
#include <pi/wrapper/GameBuffer.h>

using namespace std;
using namespace nspi;


static int32_t CreateTextureForBuffer(iBitmap* bitmap)
{
    int32_t name = piCreateTexture();
    piBindTexture(eTexTarget_2D, name);
    piTexParam(eTexTarget_2D, eTexConfig_MinFilter, eTexValue_Nearest);
    piTexParam(eTexTarget_2D, eTexConfig_MagFilter, eTexValue_Nearest);
    piTexParam(eTexTarget_2D, eTexConfig_WrapS, eTexValue_Clamp);
    piTexParam(eTexTarget_2D, eTexConfig_WrapT, eTexValue_Clamp);
    piTexImage2D(eTexTarget_2D, 0, ePixelFormat_RGBA, bitmap, 0);
    piBindTexture(eTexTarget_2D, 0);
    return name;
}

GameBuffer::GameBuffer():
mFB(0), mDepthBuffer(0), mWidth(0), mHeight(0), mSizeChanged(true)
{
    mTexs[0] = 0;
    mTexs[1] = 0;
}


GameBuffer::~GameBuffer()
{
}

int32_t GameBuffer::GetFramebuffer()
{
    if (mSizeChanged)
    {
        SmartPtr<iBitmap> bitmap = CreateBitmapEmpty(ePixelFormat_RGBA, mWidth, mHeight);

        if (mTexs[0] == 0)
        {
            mTexs[0] = CreateTextureForBuffer(bitmap);
            mTexs[1] = CreateTextureForBuffer(bitmap);
        }
        else
        {
            piBindTexture(eTexTarget_2D, mTexs[0]);
            piTexImage2D(eTexTarget_2D, 0, ePixelFormat_RGBA, bitmap, 0);
            
            piBindTexture(eTexTarget_2D, mTexs[1]);
            piTexImage2D(eTexTarget_2D, 0, ePixelFormat_RGBA, bitmap, 0);
            
            piBindTexture(eTexTarget_2D, 0);
        }
        
        if (mDepthBuffer == 0)
        {
            mDepthBuffer = piCreateRenderbuffer();
        }
        
        piBindRenderbuffer(mDepthBuffer);
        piRenderbufferStorage(ePixelFormat_Depth24Stencil8, mWidth, mHeight);
        piBindRenderbuffer(0);
        
        if (mFB == 0)
        {
            mFB = piCreateFramebuffer();
        }
        
        piBindFramebuffer(eFramebuffer_DrawRead, mFB);
        piFramebufferTexture2D(eFramebuffer_DrawRead, eAttachment_Color0, eTexTarget_2D, mTexs[0], 0);
        piFramebufferRenderbuffer(eFramebuffer_DrawRead, eAttachment_Depth, mDepthBuffer);
        
#if defined(PI_DEBUG)
        piVerifyFramebufferState(eFramebuffer_DrawRead);
#endif
        
        piBindFramebuffer(eFramebuffer_DrawRead, 0);
        
        mSizeChanged = false;
    }
    
    return mFB;
}

void GameBuffer::OnLoad()
{
}

void GameBuffer::OnUnload()
{
    piReleaseGraphicsObject(mFB);
    mFB = 0;

    piReleaseGraphicsObject(mTexs[0]);
    mTexs[0] = 0;
    
    piReleaseGraphicsObject(mTexs[1]);
    mTexs[1] = 0;
    
    piReleaseGraphicsObject(mDepthBuffer);
    mDepthBuffer = 0;
    
    mSizeChanged = true;
}

void GameBuffer::Swap()
{
    int32_t tmp = mTexs[0];
    mTexs[0] = mTexs[1];
    mTexs[1] = tmp;
 
    piFramebufferTexture2D(eFramebuffer_DrawRead, eAttachment_Color0, eTexTarget_2D, mTexs[0], 0);
}

int32_t GameBuffer::GetBackTexture() const
{
    return mTexs[0];
}

int32_t GameBuffer::GetFrontTexture() const
{
    return mTexs[1];
}

void GameBuffer::OnResize(int32_t width, int32_t height)
{
    if (mWidth != width || mHeight != height)
    {
        mWidth = width;
        mHeight = height;
        mSizeChanged = true;
    }
}






















