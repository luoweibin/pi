/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.23   1     Create
 ******************************************************************************/
#include <pi/wrapper/QuadRenderer.h>

using namespace std;
using namespace nspi;


struct QuadVertex
{
    float pos[2];
    float uv[2];
};

QuadRenderer::QuadRenderer():
mProgram(0)
{
}

QuadRenderer::~QuadRenderer()
{
}

void QuadRenderer::OnLoad()
{
    InitProgram();
    InitVBO();
    
    int32_t vao = piCreateVertexArray();
    PILOGI(PI_GAME_TAG, "QuadRenderer VAO:[%d]", vao);
    
    piBindVertexArray(vao);
    
    piBindBuffer(eGraphicsBuffer_Vertex, mVBO);
    piBindBuffer(eGraphicsBuffer_Index, 0);
    
    piEnableVertexAttr(0);
    piEnableVertexAttr(1);
    
    piVertexAttr(0, 2, eType_F32, sizeof(QuadVertex), offsetof(QuadVertex, pos));
    piVertexAttr(1, 2, eType_F32, sizeof(QuadVertex), offsetof(QuadVertex, uv));
    
    piBindVertexArray(0);
    piBindBuffer(eGraphicsBuffer_Vertex, 0);
    piBindBuffer(eGraphicsBuffer_Index, 0);
    
    mVAO = vao;
}

void QuadRenderer::OnUnload()
{
    piReleaseGraphicsObject(mProgram);
    piReleaseGraphicsObject(mVBO);
    piReleaseGraphicsObject(mVAO);
}

void QuadRenderer::Present(int32_t texture, const mat4& matrix)
{
    piPushGroupMarker("QuadRenderer::PresentBuffer");
 
    int32_t features = eGraphicsFeature_Blend;
    piEnableFeatures(features);
    
    piBlendFunc(eBlendFunc_SrcAlpha, eBlendFunc_OneMinusSrcAlpha);
    
    piUseProgram(mProgram);
    
    piBindVertexArray(mVAO);
  
    piActiveTexture(0);
    piBindTexture(eTexTarget_2D, texture);
    piUniform1i(mProgram, "frame", 0);
    
    piUniformMat4f(mProgram, "UVMatrix", matrix);
    
    piDrawArrays(eGraphicsDraw_TriangleStrip, 0, 4);
    
    piUseProgram(0);
    piBindVertexArray(0);
    piBindTexture(eTexTarget_2D, 0);
    piDisableFeatures(features);
    
    piPopGroupMarker();
}

void QuadRenderer::InitVBO()
{
    static const QuadVertex vertices[] =
    {
        {-1, -1, 0, 0},
        { 1, -1, 1, 0},
        {-1,  1, 0, 1},
        { 1,  1, 1, 1},
    };
    
    SmartPtr<iMemory> mem = CreateMemoryStatic(vertices, sizeof(vertices));
    
    int32_t vbo = piCreateBuffer();
    piBindBuffer(eGraphicsBuffer_Vertex, vbo);
    piBufferData(eGraphicsBuffer_Vertex, vbo, mem->Size(), mem);
    piBindBuffer(eGraphicsBuffer_Vertex, 0);
    
    mVBO = vbo;
}

void QuadRenderer::InitProgram()
{
    mProgram = piCreateProgram();

#if defined(PI_OPENGL_ES)
    static const char VS_ES2[] =
    "precision mediump float;\n"
    "attribute vec4 position;\n"
    "attribute vec4 uv;\n"
    "uniform mat4 UVMatrix;\n"
    "varying vec2 uv0;\n"
    "void main(void)\n"
    "{\n"
    "    gl_Position = position;\n"
    "    uv0 = (UVMatrix * uv).st;\n"
    "}\n";
    
    static const char FS_ES2[] =
    "precision mediump float;\n"
    "varying vec2 uv0;\n"
    "uniform sampler2D frame;\n"
    "void main(void) {\n"
    "    gl_FragColor = texture2D(frame, uv0);\n"
    "}\n";
    
    static const char VS_ES3[] =
    "#version 300 es\n"
    "precision mediump float;\n"
    "layout(location=0) in vec4 position;\n"
    "layout(location=1) in vec4 uv;\n"
    "uniform mat4 UVMatrix;\n"
    "out vec2 uv0;\n"
    "void main(void)\n"
    "{\n"
    "    gl_Position = position;\n"
    "    uv0 = (UVMatrix * uv).st;\n"
    "}\n";
    
    static const char FS_ES3[] =
    "#version 300 es\n"
    "precision mediump float;\n"
    "in vec2 uv0;\n"
    "uniform sampler2D frame;\n"
    "layout(location=0) out vec4 fragColor;"
    "void main(void) {\n"
    "    fragColor = texture(frame, uv0);\n"
    "}\n";
    
    int gType = piGetGraphicsType();
    if (gType == eGraphicsBackend_OpenGL_ES2)
    {
        piCompileProgram(mProgram, VS_ES2, FS_ES2);
        piBindVertexAttr(mProgram, 0, "position");
        piBindVertexAttr(mProgram, 1, "uv");
    }
    else
    {
        piCompileProgram(mProgram, VS_ES3, FS_ES3);
    }
    
#elif defined(PI_OPENGL)
    static const char VS[] =
    "#version 400\n"
    "precision mediump float;\n"
    "layout(location=0) in vec4 position;\n"
    "layout(location=1) in vec4 uv;\n"
    "out vec2 uv0;\n"
    "uniform mat4 UVMatrix;\n"
    "void main(void)\n"
    "{\n"
    "    gl_Position = position;\n"
    "    uv0 = (UVMatrix * uv).st;\n"
    "}\n";
    
    static const char FS[] =
    "#version 400\n"
    "precision mediump float;\n"
    "in vec2 uv0;\n"
    "uniform sampler2D frame;\n"
    "layout(location=0) out vec4 fragColor;"
    "void main(void) {\n"
    "    fragColor = texture(frame, uv0);\n"
    "}\n";
    
    piCompileProgram(mProgram, VS, FS);
#endif
    
    piLinkProgram(mProgram);
}































