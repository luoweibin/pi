/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2012.12.23   0.1     Create
 ******************************************************************************/
#import <pi/wrapper/macOS/PIGameView.h>
#import <OpenGL/gl.h>
#import <CoreVideo/CVDisplayLink.h>
#import <pi/Game.h>
#import <pi/Input.h>

using namespace nspi;

@interface PIGameView ()
{
@private
    CVDisplayLinkRef mpDisplayLink;
    SmartPtr<iGame> mGame;
    SmartPtr<iGraphicsVM> mGraphicsVM;
}

- (iGame*) game;

@end

static CVReturn OpenGLDisplayLinkCallback(CVDisplayLinkRef displayLink,
                                          const CVTimeStamp* now,
                                          const CVTimeStamp* outputTime,
                                          CVOptionFlags flagsIn,
                                          CVOptionFlags* flagsOut,
                                          void* displayLinkContext)
{
    PIGameView *view = (__bridge PIGameView*)displayLinkContext;
    [view setNeedsDisplay:YES];
    
    [view game]->PostMessage(nullptr, eGameMsgPri_Normal, eGameMsg_Update);
    return kCVReturnSuccess;
}


@implementation PIGameView

- (iGame*)game
{
    return mGame;
}

- (void) dealloc
{
    [self stop];
}

- (void) prepareOpenGL
{
    [super prepareOpenGL];
    
    // Make all the OpenGL calls to setup rendering
    //  and build the necessary rendering objects
    [self initGL];
    
    // Create a display link capable of being used with all active displays
    CVDisplayLinkCreateWithActiveCGDisplays(&mpDisplayLink);
    
    // Set the renderer output callback function
    CVDisplayLinkSetOutputCallback(mpDisplayLink, &OpenGLDisplayLinkCallback, (__bridge void*)self);
    
    // Set the display link for the current renderer
    CGLContextObj cglContext = [[self openGLContext] CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = [[self pixelFormat] CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(mpDisplayLink, cglContext, cglPixelFormat);
    
    // Activate the display link
    CVDisplayLinkStart(mpDisplayLink);
    
    // Register to be notified when the window closes so we can stop the displaylink
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(windowWillClose:)
                                                 name:NSWindowWillCloseNotification
                                               object:[self window]];
}

- (void) initGL
{
    // The reshape function may have changed the thread to which our OpenGL
    // context is attached before prepareOpenGL and initGL are called.  So call
    // makeCurrentContext to ensure that our OpenGL context current to this
    // thread (i.e. makeCurrentContext directs all OpenGL calls on this thread
    // to [self openGLContext])
    [[self openGLContext] makeCurrentContext];
    
    // Synchronize buffer swaps with vertical refresh rate
    GLint swapInt = 2;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
}

- (void)renewGState
{
    // Called whenever graphics state updated (such as window resize)
    
    // OpenGL rendering is not synchronous with other rendering on the OSX.
    // Therefore, call disableScreenUpdatesUntilFlush so the window server
    // doesn't render non-OpenGL content in the window asynchronously from
    // OpenGL content, which could cause flickering.  (non-OpenGL content
    // includes the title bar and drawing done by the app with other APIs)
    [[self window] disableScreenUpdatesUntilFlush];
    
    [super renewGState];
}

- (id) initWithFrame:(NSRect)frameRect
      andPixelFormat:(NSOpenGLPixelFormat*)pixelFormat
             andGame:(iGame*)game
       andGraphicsVM:(nspi::iGraphicsVM *)graphicsVM
{
    self = [super initWithFrame:frameRect pixelFormat:pixelFormat];
    
    NSRect bounds = [self convertRectToBacking:self.bounds];
    
    mGame = game;
    mGraphicsVM = graphicsVM;
    
    rect b(bounds.origin.x, bounds.origin.y, bounds.size.width, bounds.size.height);
    mGame->PostMessage(nullptr, eGameMsgPri_High, eGameMsg_Resize, b);
    
    return self;
}

- (void) start
{
    // Activate the display link
    if (mpDisplayLink != nil)
    {
        CVDisplayLinkStart(mpDisplayLink);
    }
}

- (void) stop
{
    if (mpDisplayLink != nil)
    {
        CVDisplayLinkRelease(mpDisplayLink);
        mpDisplayLink = nil;
    }
}

- (void)drawRect:(NSRect)dirtyRect
{
    [self lockOpenGLContext];
    
    if (!mGraphicsVM.IsNull())
    {
        mGraphicsVM->Run();
    }
    
    [[self openGLContext] flushBuffer];
    [self unlockOpenGLContext];
}

- (void)reshape
{
    NSRect bounds = [self convertRectToBacking:self.frame];
    rect b(bounds.origin.x, bounds.origin.y, bounds.size.width, bounds.size.height);
    mGame->PostMessage(nullptr, eGameMsgPri_High, eGameMsg_Resize, b);
}

- (iKeyEvent*)makeKeyEventFromNSEvent:(NSEvent*)theEvent
                             withType:(int)type
{
    SmartPtr<iKeyEvent> event = CreateKeyEvent();
    event->SetID(type);
    event->SetCode(piMapKeyCode([theEvent keyCode]));
    event->SetFlags(piMapEventFlags((int)[theEvent modifierFlags]));
    return event.PtrAndSetNull();
}

- (void) keyDown:(NSEvent *)theEvent
{
    [super keyDown:theEvent];
    
    SmartPtr<iKeyEvent> event = [self makeKeyEventFromNSEvent:theEvent
                                                     withType:eHIDEvent_KeyDown];
    mGame->PostMessage(nullptr, eGameMsgPri_High, eGameMsg_HIDEvent, event.Ptr());
}

- (void) keyUp:(NSEvent *)theEvent
{
    [super keyUp:theEvent];
    
    SmartPtr<iKeyEvent> event = [self makeKeyEventFromNSEvent:theEvent
                                                     withType:eHIDEvent_KeyUp];
    mGame->PostMessage(nullptr, eGameMsgPri_High, eGameMsg_HIDEvent, event.Ptr());
}

- (void) flagsChanged:(NSEvent *)theEvent
{
    [super flagsChanged:theEvent];
    
    SmartPtr<iKeyEvent> event = [self makeKeyEventFromNSEvent:theEvent
                                                     withType:eHIDEvent_KeyModifierChanged];
    mGame->PostMessage(nullptr, eGameMsgPri_High, eGameMsg_HIDEvent, event.Ptr());
}

- (iTouchEvent*) makeTouchEventFromNSEvent:(NSEvent *)ev withType:(int)type
{
    SmartPtr<iTouchEvent> event = CreateTouchEvent();
    event->SetID(type);

    NSPoint eventLocation = [ev locationInWindow];
    NSPoint center = [self convertPoint:eventLocation fromView:nil];
    NSSize centerBacking = [self convertSizeToBacking:NSMakeSize(center.x, center.y)];
    event->AddLocation(vec3(centerBacking.width, centerBacking.height, 0));

    return event.PtrAndSetNull();
}

- (void) mouseDown:(NSEvent *)theEvent
{
    [super mouseDown:theEvent];
    
    SmartPtr<iTouchEvent> event = [self makeTouchEventFromNSEvent:theEvent
                                                         withType:eHIDEvent_LeftMouseDown];
    mGame->PostMessage(nullptr, eGameMsgPri_High, eGameMsg_HIDEvent, event.Ptr());
}

- (void) mouseUp:(NSEvent *)theEvent
{
    [super mouseUp:theEvent];
    
    SmartPtr<iTouchEvent> event = [self makeTouchEventFromNSEvent:theEvent
                                                         withType:eHIDEvent_LeftMouseUp];
    mGame->PostMessage(nullptr, eGameMsgPri_High, eGameMsg_HIDEvent, event.Ptr());
}

- (void) mouseDragged:(NSEvent *)theEvent
{
    [super mouseDragged:theEvent];
    
    SmartPtr<iTouchEvent> event = [self makeTouchEventFromNSEvent:theEvent
                                                         withType:eHIDEvent_LeftMouseDragged];
    mGame->PostMessage(nullptr, eGameMsgPri_High, eGameMsg_HIDEvent, event.Ptr());
}

- (void) lockOpenGLContext
{
    [[self openGLContext] makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (void) unlockOpenGLContext
{
    CGLUnlockContext((CGLContextObj)[[self openGLContext] CGLContextObj]);
}

- (BOOL)acceptsFirstResponder
{
    return YES;
}

- (void)windowWillClose:(NSNotification *)notification
{
}

@end




























