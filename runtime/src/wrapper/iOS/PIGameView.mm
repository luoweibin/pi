//
//  PIGameView.m
//  pi
//
//  Created by luoweibin on 9/4/16.
//  Copyright © 2016 Baxian. All rights reserved.
//

#import <pi/wrapper/iOS/PIGameView.h>

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

using namespace std;
using namespace nspi;

@interface PIGameView () {
    EAGLContext* _glContext;
    CADisplayLink* _displayLink;
    GLuint _colorBuffer;
    GLuint _depthBuffer;
    GLuint _frameBuffer;
    float _width;
    float _height;
    SmartPtr<iGame> _game;
    SmartPtr<iGraphicsVM> _vm;
}

@end

@implementation PIGameView

- (id)initWithFrame:(CGRect)frame
       andGLContext:(EAGLContext *)glContext
           andScale:(float)scale
{
    if (self = [super initWithFrame:frame]) {
        self.contentScaleFactor = scale;
        _glContext = glContext;
        [self initLayer];
        [self createBuffers];
        [self initDisplayLink];
        
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(handleSingleTap:)];
        [self addGestureRecognizer:singleFingerTap];
    }
    return self;
}

- (void)initLayer
{
    CAEAGLLayer* aglLayer = (CAEAGLLayer*)self.layer;
    aglLayer.drawableProperties = @{
                                    kEAGLDrawablePropertyRetainedBacking: [NSNumber numberWithBool:NO],
                                    kEAGLDrawablePropertyColorFormat: kEAGLColorFormatRGBA8,
                                    };
    aglLayer.opaque = YES;
    aglLayer.contentsScale = self.contentScaleFactor;
}

- (void)createBuffers
{
    piAssert([EAGLContext setCurrentContext:_glContext], ;);
    
    glGenFramebuffers(1, &_frameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);
    
    glGenRenderbuffers(1, &_colorBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorBuffer);
    
    CAEAGLLayer* aglLayer = (CAEAGLLayer*)self.layer;
    if (![_glContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:aglLayer])
    {
        glDeleteBuffers(1, &_colorBuffer);
        _colorBuffer = -1;
        return;
    }
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _colorBuffer);
    
    GLint width = 0;
    GLint height = 0;
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &_depthBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _depthBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _depthBuffer);
    
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    piAssert(status == GL_FRAMEBUFFER_COMPLETE, ;);    
}

- (void)destroyBuffers
{
    if (_colorBuffer != -1)
    {
        glDeleteRenderbuffers(1, &_colorBuffer);
        _colorBuffer = -1;
    }
    
    if (_depthBuffer != -1)
    {
        glDeleteRenderbuffers(1, &_depthBuffer);
        _depthBuffer = -1;
    }
    
    if (_frameBuffer != -1)
    {
        glDeleteFramebuffers(1, &_frameBuffer);
        _frameBuffer = -1;
    }
}

- (void)initDisplayLink
{
    _displayLink = [CADisplayLink displayLinkWithTarget:self
                                              selector:@selector(onDisplay:)];
    [_displayLink addToRunLoop:[NSRunLoop mainRunLoop]
                      forMode:NSDefaultRunLoopMode];
    _displayLink.frameInterval = 1;
    _displayLink.paused = YES;
}

- (void)start
{
    _displayLink.paused = NO;
}

- (void)stop
{
    _displayLink.paused = YES;
}

- (void)onDisplay:(CADisplayLink*)displayLink
{
    piAssert([EAGLContext setCurrentContext:_glContext], ;);
    
    glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);
    
    SmartPtr<iGame> game;
    @synchronized (self)
    {
        game = _game;
    }
    if (!game.IsNull())
    {
        game->PostMessage(nullptr, eGameMsgPri_Normal, eGameMsg_Update);
        if (!_vm.IsNull())
        {
            _vm->Run();
        }
    }
    
    glFlush();
    
    glBindRenderbuffer(GL_RENDERBUFFER, _colorBuffer);
    piAssert([_glContext presentRenderbuffer:GL_RENDERBUFFER], ;);
}

- (void)layoutSubviews
{
    CGRect frame = self.bounds;
    if (!piEqual(frame.size.width, _width) || piEqual(frame.size.height, _height))
    {
        _width = frame.size.width;
        _height = frame.size.height;
        
        piAssert([EAGLContext setCurrentContext:_glContext], ;);
        [self destroyBuffers];
        [self createBuffers];
        
        [self postResizeMessage];
    }
}

- (void)setGame:(iGame*)game {
    @synchronized (self) {
        _game = game;
    }
    
    CGRect frame = self.bounds;
    _width = frame.size.width;
    _height = frame.size.height;
    
    [self postResizeMessage];
}

- (void)setGraphicsVM:(iGraphicsVM *)vm
{
    _vm = vm;
}

- (void)postResizeMessage {
    SmartPtr<iGame> game;
    @synchronized (self) {
        game = _game;
    }
    if (!game.IsNull()) {
        float scale = self.layer.contentsScale;
        float width = _width * scale;
        float height = _height * scale;
        rect bounds(0, 0, width, height);
        game->PostMessage(nullptr, eGameMsgPri_High, eGameMsg_Resize, bounds);
    }
}

- (BOOL)isFirstResponder {
    return YES;
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateEnded)
    {
        SmartPtr<iGame> game;
        @synchronized (self) {
            game = _game;
        }
        
        if (!game.IsNull())
        {
            float scale = self.layer.contentsScale;
            int32_t height = self.bounds.size.height;
            
            SmartPtr<iTouchEvent> event = CreateTouchEvent();
            event->SetID(eHIDEvent_Tap);
            
            for (int32_t i = 0; i < [recognizer numberOfTouches]; ++i)
            {
                CGPoint point = [recognizer locationOfTouch:i
                                                     inView:self];
                event->AddLocation(vec3(point.x * scale, (height - point.y) * scale, 0));
            }
            
            event->SetReadonly();
            
            game->PostMessage(nullptr, eGameMsgPri_High, eGameMsg_HIDEvent, event.Ptr());
        }
    }
}

+ (Class)layerClass
{
    return [CAEAGLLayer class];
}

@end




















