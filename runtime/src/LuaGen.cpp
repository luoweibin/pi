/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 wilburluo     2017.3.20   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>
#include <pi/lua/LuaScript.h>
#include <lua.hpp>
#include "lua/LuaValue.h"
#include "lua/LuaArg.h"

using namespace nspi;
using namespace std;

#if PI_LUA_ENABLED
#include "../gen/lua/Init.cpp"
#endif
