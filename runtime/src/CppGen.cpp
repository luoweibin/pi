/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.28   1     Create
 ******************************************************************************/
#include <pi/Core.h>
#include <pi/DOM.h>
#include <pi/Crypto.h>
#include <pi/CV.h>
#include <pi/Graphics.h>
#include <pi/Input.h>
#include <pi/Media.h>
#include <pi/Scripting.h>
#include <pi/Game.h>
#include <pi/lua/LuaScript.h>

#if PI_REFLECTION_ENABLED

#include "../gen/cpp/All.cpp"

#endif

