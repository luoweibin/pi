/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.16   1.0     Create
 ******************************************************************************/
#include <pi/CV.h>

using namespace std;

NSPI_BEGIN()


class Calibrate : public iCalibrate
{
public:
    Calibrate()
    {
    }
    
    virtual ~Calibrate()
    {
    }
    
    virtual void Update(iFaceTrackerResult* result)
    {
        PILOGE(PI_TAG, "iCalibrate::Update not implemented on macOS.");
    }
    
    virtual void SetViewportSize(const vec2& size)
    {
        PILOGE(PI_TAG, "iCalibrate::SetViewportSize not implemented on macOS.");
    }
    
    virtual mat4 GetViewMatrix(int32_t index) const
    {
        PILOGE(PI_TAG, "iCalibrate::GetViewMatrix not implemented on macOS.");
        return mat4();
    }
};


iCalibrate* CreateCalibrate()
{
    return new Calibrate();
}


mat4 piProject(double left, double right, double bottom, double top, double near, double far)
{
    PILOGE(PI_TAG, "piProject not implemented on macOS.");
    return mat4();
}


NSPI_END()























