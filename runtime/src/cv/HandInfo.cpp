/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.5   1.0     Create
 ******************************************************************************/
#include <pi/CV.h>

using namespace std;

NSPI_BEGIN()

class HandInfo : public iHandInfo
{
private:
    rect mRect;
    vec3 mPoint;
    int32_t mActionFlags;
    float mScore;
    float mActionScore;
    
public:
    HandInfo():
    mActionFlags(0), mScore(0), mActionScore(0)
    {
    }
    
    virtual ~HandInfo()
    {
    }
    
    virtual rect GetRect() const
    {
        return mRect;
    }
    
    virtual void SetRect(const rect& rect)
    {
        mRect = rect;
    }
    
    virtual vec3 GetPoint() const
    {
        return mPoint;
    }
    
    virtual void SetPoint(const vec3& point)
    {
        mPoint = point;
    }
    
    virtual int32_t GetActionFlags() const
    {
        return mActionFlags;
    }
    
    virtual bool ActionFlagsIs(int32_t actions) const
    {
        return piFlagIs(mActionFlags, actions);
    }
    
    virtual void SetActionFlags(int32_t value)
    {
        mActionFlags = value;
    }
    
    virtual float GetScore() const
    {
        return mScore;
    }
    
    virtual void SetScore(float value)
    {
        mScore = value;
    }
    
    virtual float GetActionScore() const
    {
        return mActionScore;
    }
    
    virtual void SetActionScore(float value)
    {
        mActionScore = value;
    }
};


iHandInfo* CreateHandInfo()
{
    return new HandInfo();
}


NSPI_END()

























