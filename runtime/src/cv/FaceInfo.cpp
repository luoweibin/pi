/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.16   1.0     Create
 ******************************************************************************/
#include <pi/CV.h>

using namespace std;

NSPI_BEGIN()

class FaceInfo : public iFaceInfo
{
private:
    SmartPtr<iVec3Array> mPoints;
    rect mRect;
    float mYaw;
    float mRoll;
    float mPitch;
    int32_t mEyeDistance;
    int32_t mTrackID;
    
    int32_t mActionFlags;
    
public:
    FaceInfo():
    mYaw(0),
    mRoll(0),
    mPitch(0),
    mEyeDistance(0),
    mTrackID(-1),
    mActionFlags(0)
    {
    }
    
    virtual ~FaceInfo()
    {
    }
    
    virtual void SetTrackID(int32_t trackID)
    {
        piAssert(!IsReadonly(), ;);
        mTrackID = trackID;
    }
    
    virtual int32_t GetTrackID() const
    {
        return mTrackID;
    }
    
    // 人脸106关键点的数组
    virtual iVec3Array* GetPoints() const
    {
        return mPoints;
    }
    
    virtual void SetPoints(iVec3Array* points)
    {
        piAssert(!IsReadonly(), ;);
        mPoints = points;
    }
    
    // 代表面部的矩形区域
    virtual rect GetRect() const
    {
        return mRect;
    }
    
    virtual void SetRect(const rect& value)
    {
        piAssert(!IsReadonly(), ;);
        mRect = value;
    }
    
    // 水平转角，真实度量的左负右正
    virtual float GetYaw() const
    {
        return mYaw;
    }
    
    virtual void SetYaw(float value)
    {
        piAssert(!IsReadonly(), ;);
        mYaw = value;
    }
    
    // 俯仰角，真实度量的上负下正
    virtual float GetPitch() const
    {
        return mPitch;
    }
    
    virtual void SetPitch(float value)
    {
        piAssert(!IsReadonly(), ;);
        mPitch = value;
    }
    
    // 旋转角，真实度量的左负右正
    virtual float GetRoll() const
    {
        return mRoll;
    }
    
    virtual void SetRoll(float value)
    {
        piAssert(!IsReadonly(), ;);
        mRoll = value;
    }
    
    // 两眼间距
    virtual float GetEyeDistance() const
    {
        return mEyeDistance;
    }
    
    virtual void SetEyeDistance(float value)
    {
        piAssert(!IsReadonly(), ;);
        mEyeDistance = value;
    }
    
    virtual void SetActionFlags(int32_t value)
    {
        piAssert(!IsReadonly(), ;);
        mActionFlags = value;
    }
    
    virtual int32_t GetActionFlags() const
    {
        return mActionFlags;
    }
    
    virtual bool ActionFlagsIs(int32_t actions) const
    {
        return piFlagIs(mActionFlags, actions);
    }
};

iFaceInfo* CreateFaceInfo()
{
    return new FaceInfo();
}


NSPI_END()






















