/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.16   1.0     Create
 ******************************************************************************/
#include <pi/CV.h>

using namespace std;

NSPI_BEGIN()

class FaceTrackerResult : public iFaceTrackerResult
{
private:
    vector<SmartPtr<iFaceInfo>> mFaces;
    vector<SmartPtr<iHandInfo>> mHands;
    int32_t mImageWidth;
    int32_t mImageHeight;
    
    SmartPtr<iGraphicsObject> mBgMask;
    SmartPtr<iTexture2D>      mBgMaskTex2D;
    bool mBgMaskAvail;
    mat4 mBgMaskMatrix;
    
public:
    FaceTrackerResult():
    mImageWidth(0),
    mImageHeight(0),
    mBgMaskAvail(false)
    {
        mBgMaskTex2D = CreateTexture2D();
    }
    
    virtual ~FaceTrackerResult()
    {
        
    }
    
    virtual int32_t GetFaceCount() const
    {
        return (int32_t)mFaces.size();
    }
    
    virtual iFaceInfo* GetFace(int32_t index) const
    {
        piAssert(index >= 0 && index < mFaces.size(), nullptr);
        return mFaces[index];
    }
    
    virtual iFaceInfo* FindFaceByTrackID(int32_t trackID) const
    {
        for (auto it = mFaces.begin(); it != mFaces.end(); ++it)
        {
            if ((*it)->GetTrackID() == trackID)
            {
                return *it;
            }
        }
        
        return nullptr;
    }
    
    virtual void AddFace(iFaceInfo* face)
    {
        piAssert(face != nullptr, ;);
        piAssert(!IsReadonly(), ;);
        mFaces.push_back(face);
    }
    
    virtual int32_t GetHandCount() const
    {
        return (int32_t)mHands.size();
    }
    
    virtual iHandInfo* GetHand(int32_t index) const
    {
        piAssert(index >= 0 && index < mHands.size(), nullptr);
        return mHands[index];
    }
    
    virtual void AddHand(iHandInfo* hand)
    {
        piAssert(hand != nullptr, ;);
        piAssert(!IsReadonly(), ;);
        mHands.push_back(hand);
    }
    
    virtual int32_t GetImageWidth() const
    {
        return mImageWidth;
    }
    
    virtual void SetImageWidth(int32_t width)
    {
        piAssert(width >= 0, ;);
        piAssert(!IsReadonly(), ;);
        mImageWidth = width;
    }
    
    virtual int32_t GetImageHeight() const
    {
        return mImageHeight;
    }
    
    virtual void SetImageHeight(int32_t height)
    {
        piAssert(height >= 0, ;);
        piAssert(!IsReadonly(), ;);
        mImageHeight = height;
    }
    
    virtual string ToString() const
    {
        return piFormat("iFaceTrackerResult {count:%d}", GetFaceCount());
    }
    
    virtual void SetBgMask(iGraphicsObject* object)
    {
        piAssert(!IsReadonly(), ;);
        mBgMask = object;
        
        if (!mBgMask.IsNull())
        {
            mBgMaskTex2D->SetGraphicsName(mBgMask->GetName());
        }
    }
    
    virtual iGraphicsObject* GetBgMask() const
    {
        return mBgMask;
    }
    
    virtual iTexture2D* GetBgMaskTexture() const
    {
        return mBgMaskTex2D;
    }
    
    virtual bool HasBgMask() const
    {
        return mBgMaskAvail;
    }
    
    virtual void SetBgMaskAvailable(bool value)
    {
        piAssert(!IsReadonly(), ;);
        mBgMaskAvail = value;
    }
    
    virtual mat4 GetBgMaskMatrix() const
    {
        return mBgMaskMatrix;
    }
    
    virtual void SetBgMaskMatrix(const mat4& matrix)
    {
        mBgMaskMatrix = matrix;
    }
};

iFaceTrackerResult* CreateFaceTrackerResult()
{
    return new FaceTrackerResult();
}



NSPI_END()















