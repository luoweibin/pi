/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.30   1.0     Create
 ******************************************************************************/
#include <pi/CV.h>

extern "C"
{
#if PI_FACETRACKER_ENABLED
#if defined(PI_MACOS)
#   include <st_mobile_face.h>
#   include <st_mobile_license.h>
#   include <st_mobile_hand.h>
#else
#   include <st_mobile_human_action.h>
#   include <st_mobile_license.h>
#endif
#endif
}

using namespace std;

NSPI_BEGIN()

#if PI_FACETRACKER_ENABLED


static st_rotate_type ToSenseTimeOrient(int value)
{
    switch (value)
    {
        case eOrient_90:
            return ST_CLOCKWISE_ROTATE_90;
        case eOrient_180:
            return ST_CLOCKWISE_ROTATE_180;
        case eOrient_270:
            return ST_CLOCKWISE_ROTATE_270;
        default:
        case eOrient_0:
            return ST_CLOCKWISE_ROTATE_0;
    }
}

static st_pixel_format ToSenseTimePixelFormat(int value)
{
    switch (value)
    {
        case ePixelFormat_Gray:
            return ST_PIX_FMT_GRAY8;
        case ePixelFormat_RGBA:
            return ST_PIX_FMT_RGBA8888;
        case ePixelFormat_BGRA:
        default:
            return ST_PIX_FMT_BGRA8888;
    }
}

static int32_t ToPIFaceAction(unsigned int actions)
{
    int32_t value = 0;
    
    if (piFlagIs(actions, ST_MOBILE_BROW_JUMP))
    {
        value |= eHumanAction_BrowJump;
    }
    
    if (piFlagIs(actions, ST_MOBILE_HEAD_YAW))
    {
        value |= eHumanAction_HeadYaw;
    }
    
    if (piFlagIs(actions, ST_MOBILE_MOUTH_AH))
    {
        value |= eHumanAction_MouthOpen;
    }
    
    if (piFlagIs(actions, ST_MOBILE_EYE_BLINK))
    {
        value |= eHumanAction_EyeBlink;
    }
    
    if (piFlagIs(actions, ST_MOBILE_HEAD_PITCH))
    {
        value |= eHumanAction_HeadPitch;
    }
    
    return value;
}

static int32_t ToPIHandAction(unsigned int actions)
{
    int32_t value = 0;
    
    if (piFlagIs(actions, ST_MOBILE_HAND_GOOD))
    {
        value |= eHumanAction_HandGood;
    }
    
    if (piFlagIs(actions, ST_MOBILE_HAND_LOVE))
    {
        value |= eHumanAction_HandLove;
    }
    
    if (piFlagIs(actions, ST_MOBILE_HAND_PALM))
    {
        value |= eHumanAction_HandPalm;
    }
    
    if (piFlagIs(actions, ST_MOBILE_HAND_CONGRATULATE))
    {
        value |= eHumanAction_HandCong;
    }
    
    //    if (piFlagIs(actions, ST_MOBILE_HAND_FINGER_HEART))
    //    {
    //        value |= eHumanAction_FingerHeart;
    //    }
    
    if (piFlagIs(actions, ST_MOBILE_HAND_HOLDUP))
    {
        value |= eHumanAction_HandHoldup;
    }
    
    return value;
}

static iFaceInfo* ToPIFaceInfo(st_mobile_face_action_t* action, int orientation, float imageWidth, float imageHeight)
{
    st_mobile_106_t* face = &action->face;
    
    SmartPtr<iVec3Array> points = CreateVec3Array();
    points->Resize(106);
    
    for (int32_t j = 0; j < piArrayCount(face->points_array); ++j)
    {
        st_pointf_t point = face->points_array[j];
        vec3 pos(0, 0, 0);
        
        switch (orientation)
        {
            case eOrient_0:
                pos.x = point.x / imageWidth;
                pos.y = (imageHeight - point.y) / imageHeight;
                points->SetItem(j, pos);
                break;
            default:
                break;
        }
    }
    
    float pitch = (face->pitch / 180.0f) * M_PI;
    float yaw = (face->yaw / 180.f) * M_PI;
    float roll = (face->roll / 180.f) * M_PI;
    
    st_rect_t faceRect = face->rect;
    rect rectObj;
    rectObj.x = faceRect.left / imageWidth;
    rectObj.y = (imageHeight - faceRect.bottom) / imageHeight;
    rectObj.width = abs(faceRect.right - faceRect.left) / imageWidth;
    rectObj.height = abs(faceRect.top - faceRect.bottom) / imageHeight;
    
    SmartPtr<iFaceInfo> faceObj = CreateFaceInfo();
    faceObj->SetPoints(points);
    faceObj->SetRect(rectObj);
    faceObj->SetYaw(yaw);
    faceObj->SetRoll(roll);
    faceObj->SetPitch(pitch);
    faceObj->SetEyeDistance(face->eye_dist);
    faceObj->SetTrackID(face->ID);
    
    int32_t actions = ToPIFaceAction(action->face_action);
    faceObj->SetActionFlags(actions);
    
    faceObj->SetReadonly();
    
    return faceObj.PtrAndSetNull();
}

static iHandInfo* ToPIHandInfo(st_mobile_hand_action_t* action, int orientation, float imageWidth, float imageHeight)
{
    SmartPtr<iHandInfo> info = CreateHandInfo();
    
    st_rect_t handRect = action->hand;
    rect rectObj;
    rectObj.x = handRect.left / imageWidth;
    rectObj.y = (imageHeight - handRect.bottom) / imageHeight;
    rectObj.width = abs(handRect.right - handRect.left) / imageWidth;
    rectObj.height = abs(handRect.bottom - handRect.top) / imageHeight;
    info->SetRect(rectObj);
    
#if defined(PI_IOS)
    st_pointi_t handPoint = action->key_point;
    vec3 pointObj(handPoint.x / imageWidth, handPoint.y / imageHeight, 0);
    info->SetPoint(pointObj);
#endif
    
    info->SetScore(action->hand_score);
    info->SetActionScore(action->hand_action_score);
    info->SetActionFlags(ToPIHandAction(action->hand_action));
    
    return info.PtrAndSetNull();
}

class FaceTrackerSenseTime : public iFaceTracker
{
private:
    st_handle_t mHandle;
    SmartPtr<iGraphicsObject> mBgMask;
    int mFeatures;
    
public:
    FaceTrackerSenseTime():
    mHandle(nullptr), mFeatures(0)
    {
    }
    
    virtual ~FaceTrackerSenseTime()
    {
        if (mHandle != nullptr)
        {
#if defined(PI_MACOS)
            st_mobile_tracker_106_destroy(mHandle);
#else
            st_mobile_human_action_destroy(mHandle);
#endif
            mHandle = nullptr;
        }
    }
    
    virtual void SetFeatures(int flags)
    {
        mFeatures = flags;
    }
    
    virtual int GetFeatures() const
    {
        return mFeatures;
    }
    
    bool Init(const string& path)
    {
#if defined(PI_MACOS)
        st_result_t result = st_mobile_tracker_106_create(path.c_str(),
                                                          ST_MOBILE_TRACKING_DEFAULT_CONFIG,
                                                          &mHandle);
        piAssert(result == ST_OK, false);
        
        st_mobile_tracker_106_set_facelimit(mHandle, 5);
        
        return true;
#else
        st_result_t result = st_mobile_human_action_create(path.c_str(),
                                                           ST_MOBILE_HUMAN_ACTION_DEFAULT_CONFIG_VIDEO,
                                                           &mHandle);
        return result == ST_OK;
#endif
    }
    
    virtual void Reset()
    {
        piAssert(mHandle != nullptr, ;);
        
#if defined(PI_MACOS)
        st_mobile_tracker_106_reset(mHandle);
#else
        st_mobile_human_action_reset(mHandle);
#endif
    }
    
    virtual iFaceTrackerResult* Track(iBitmap *bitmap, int32_t orientation)
    {
        piAssert(mHandle != nullptr, nullptr);
        piAssert(bitmap != nullptr, nullptr);
        piCheck(mFeatures != 0, nullptr);
        
        bitmap->Map(eMemMap_ReadOnly);
        
        SmartPtr<iMemory> mem = bitmap->GetData(0);
        
        int imageWidth = bitmap->GetWidth();
        int imageHeight = bitmap->GetHeight();
        int stride = (int)bitmap->GetBytesPerRow(0);
        
        st_pixel_format format = ToSenseTimePixelFormat(bitmap->GetPixelFormat()->GetName());
        
#if defined(PI_MACOS)
        int faceCount = 0;
        st_mobile_face_action_t* action = nullptr;
        st_result_t result = st_mobile_tracker_106_track_face_action(mHandle,
                                                                     (uint8_t*)mem->Ptr(),
                                                                     format,
                                                                     imageWidth,
                                                                     imageHeight,
                                                                     stride,
                                                                     ToSenseTimeOrient(orientation),
                                                                     &action,
                                                                     &faceCount);
#else
        unsigned int config = 0;
        
        if (piFlagIs(mFeatures, eFaceTracker_Face))
        {
            config |= ST_MOBILE_FACE_DETECT;
        }
        
        if (piFlagIs(mFeatures, eFaceTracker_HandAction))
        {
            config |= ST_MOBILE_HAND_GOOD | ST_MOBILE_HAND_PALM | ST_MOBILE_HAND_LOVE | ST_MOBILE_HAND_HOLDUP |ST_MOBILE_HAND_CONGRATULATE | ST_MOBILE_HAND_FINGER_HEART;
        }
        
        if (piFlagIs(mFeatures, eFaceTracker_FaceAction))
        {
            config |= ST_MOBILE_EYE_BLINK | ST_MOBILE_MOUTH_AH | ST_MOBILE_HEAD_YAW | ST_MOBILE_HEAD_PITCH | ST_MOBILE_BROW_JUMP;
        }

        if (piFlagIs(mFeatures, eFaceTracker_Bg))
        {
            config |= ST_MOBILE_SEG_BACKGROUND;
        }
        
        st_mobile_human_action_t action;
        st_result_t result = st_mobile_human_action_detect(mHandle,
                                                           (uint8_t*)mem->Ptr(),
                                                           format,
                                                           imageWidth,
                                                           imageHeight,
                                                           stride,
                                                           ToSenseTimeOrient(orientation),
                                                           config,
                                                           &action);
        int faceCount = action.face_count;
        int handCount = action.hand_count;
#endif
        
        bitmap->Unmap();
        
        piAssert(result == ST_OK, nullptr);
        
        SmartPtr<iFaceTrackerResult> faceResult = CreateFaceTrackerResult();
        
        for (int32_t i = 0; i < faceCount; ++i)
        {
#if defined(PI_MACOS)
            st_mobile_face_action_t *faceAction = &action[i];
#else
            st_mobile_face_action_t *faceAction = &action.faces[i];
#endif
            
            SmartPtr<iFaceInfo> faceObj = ToPIFaceInfo(faceAction, orientation, imageWidth, imageHeight);
            faceResult->AddFace(faceObj);
        }
        
#if defined(PI_IOS)
        for (auto i = 0; i < handCount; ++i)
        {
            st_mobile_hand_action_t *handAction = &action.hands[i];
            SmartPtr<iHandInfo> handObj = ToPIHandInfo(handAction, orientation, imageWidth, imageHeight);
            faceResult->AddHand(handObj);
        }
#endif
        SmartPtr<iGraphicsObject> bgMask = GetBgMask();
        
#if defined(PI_IOS)
        bool hasBgMask = action.background_result != 0;
#else
        bool hasBgMask = false;
#endif
        if (hasBgMask)
        {
#if defined(PI_MACOS)
            st_image_t* bgBmp = nullptr;
#else
            st_image_t* bgBmp = &action.background;
#endif
            SmartPtr<iBitmap> bitmap = CreateBitmap(ePixelFormat_Gray, bgBmp->width, bgBmp->height);
            SmartPtr<iMemory> mem = bitmap->GetData(0);
            
            int32_t stride = bgBmp->stride;
            piAssert(stride == bgBmp->width, nullptr);
            
            memcpy(mem->Ptr(), bgBmp->data, (size_t)mem->Size());
            
            piBindTexture(eTexTarget_2D, bgMask->GetName());
            piTexImage2D(eTexTarget_2D, 0, ePixelFormat_R, bitmap, 0);
            piBindTexture(eTexTarget_2D, 0);
        }
        
        faceResult->SetBgMaskAvailable(hasBgMask);
        faceResult->SetBgMask(bgMask);
        
        faceResult->SetImageWidth(imageWidth);
        faceResult->SetImageHeight(imageHeight);
        
        return faceResult.PtrAndSetNull();
    }
    
private:
    
    iGraphicsObject* GetBgMask()
    {
        if (mBgMask.IsNull())
        {
            mBgMask = CreateGraphicsObject();
            
            int32_t name = piCreateTexture();
            piBindTexture(eTexTarget_2D, name);
            piTexParam(eTexTarget_2D, eTexConfig_WrapS, eTexValue_Clamp);
            piTexParam(eTexTarget_2D, eTexConfig_WrapT, eTexValue_Clamp);
            piTexParam(eTexTarget_2D, eTexConfig_MinFilter, eTexValue_Linear);
            piTexParam(eTexTarget_2D, eTexConfig_MagFilter, eTexValue_Linear);
            piBindTexture(eTexTarget_2D, 0);
            
            mBgMask->SetName(name);
            piReleaseGraphicsObject(name);
        }
        
        return mBgMask;
    }
};

iFaceTracker* CreateFaceTrackerSenseTime(const string &root)
{
    SmartPtr<FaceTrackerSenseTime> tracker = new FaceTrackerSenseTime();
    if (tracker->Init(root))
    {
        return tracker.PtrAndSetNull();
    }
    else
    {
        return nullptr;
    }
}

#else

iFaceTracker* CreateFaceTrackerSenseTime(const string &root)
{
    return nullptr;
}

#endif

NSPI_END()
























