/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.16   1.0     Create
 ******************************************************************************/
#include <pi/CV.h>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

NSPI_BEGIN()

static const double fx = 1057.456338905779;
static const double fy = 1057.794340727126;

static const float g_ObjectPoints[] = {
   -0.534543812275,  0.413008123636, -0.444587618113, //29
   -0.184372961521,  0.393059551716, -0.385646045208, //24
    0.188392043114,  0.391447782516, -0.387026339769, //59
    0.538276195526,  0.408368229866, -0.448561161757, //87
   -0.183154970407,  0.086407102644, -0.145170152187, //361
    0.002636999823,  0.149028271437,  0.066903710365,  //317
    0.186302036047,  0.084809340537, -0.146538257599, //981
   -0.278057992458, -0.208431661129, -0.181415677071, //355
    0.278380990028, -0.210838854313, -0.183476209641  //828
};

static const int32_t g_PointIndices[] = {
    52, 55, 58, 61, 82, 46, 83, 84, 90
};

static float g_distCoeff[] = {
    0.1341890169782395, -0.1039701628043858, 0.01878435425172195, -0.0003149254554303547,
    -0.3025893952674054
};

//绕X轴旋转180度，从OpenCV坐标系变换为OpenGL坐标系
static double CvToGL[] =
{
    1, 0, 0,
    0, -1, 0,
    0, 0, -1
};

static const Mat ConvertMatrix = Mat(3, 3, CV_64FC1, CvToGL);

static mat4 BuildViewMatrix(const Mat& rvec, const Mat& tvec)
{
    Mat R;
    Rodrigues(rvec, R);
    
    Mat glRotation = ConvertMatrix * R;
    Mat glTranslation = ConvertMatrix * tvec;
    
    mat4 mat;
    
    mat[0][0] = glRotation.at<double>(0, 0);
    mat[0][1] = glRotation.at<double>(1, 0);
    mat[0][2] = glRotation.at<double>(2, 0);
    mat[0][3] = 0.0f;
    
    mat[1][0] = glRotation.at<double>(0, 1);
    mat[1][1] = glRotation.at<double>(1, 1);
    mat[1][2] = glRotation.at<double>(2, 1);
    mat[1][3] = 0.0f;
    
    mat[2][0] = glRotation.at<double>(0, 2);
    mat[2][1] = glRotation.at<double>(1, 2);
    mat[2][2] = glRotation.at<double>(2, 2);
    mat[2][3] = 0.0f;
    
    mat[3][0] = glTranslation.at<double>(0, 0);
    mat[3][1] = glTranslation.at<double>(1, 0);
    mat[3][2] = glTranslation.at<double>(2, 0);
    mat[3][3] = 1.0f;
    
    return mat;
}

struct FaceState : public iRefObject
{
    Mat camIntrisic;
    Mat rvec;
    Mat tvec;
    mat4 viewMatrix;
    int32_t trackID;
    bool init;
    
    SmartPtr<iVec3Array> points;
    
    FaceState():
    init(false), trackID(-1)
    {
        points = CreateVec3Array();
        
        rvec = Mat::zeros(3, 1, CV_64FC1);
        tvec = Mat::zeros(3, 1, CV_64FC1);
    }
};


class Calibrate : public iCalibrate
{
private:
    vector<Point3f> mObjectPoints;
    
    typedef vector<SmartPtr<FaceState>> StateVector;
    StateVector mStates;
    
    Mat mCamMatrix;
    Mat mDistCoeffs;
    
    vec2 mViewportSize;
    
public:
    Calibrate()
    {
        mDistCoeffs = Mat(1, piArrayCount(g_distCoeff), CV_32FC1, g_distCoeff).clone();

        SetViewportSize(vec2(720, 1280));
        
        int32_t count = piArrayCount(g_ObjectPoints);
        for (auto i = 0; i < count; i += 3)
        {
            mObjectPoints.push_back(Point3f(g_ObjectPoints[i], g_ObjectPoints[i+1], g_ObjectPoints[i+2]));
        }
    }
    
    virtual ~Calibrate()
    {
    }
    
    virtual void Update(iFaceTrackerResult* result)
    {
        piCheck(result != nullptr, ;);
        
        for (auto it = mStates.begin(); it != mStates.end();)
        {
            int32_t trackID = (*it)->trackID;
            if (result->FindFaceByTrackID(trackID) == nullptr)
            {
                it = mStates.erase(it);
            }
            else
            {
                ++it;
            }
        }
        
        float width = mViewportSize.x;
        float height = mViewportSize.y;
        
        for (auto i = 0; i < result->GetFaceCount(); ++i)
        {
            SmartPtr<iFaceInfo> face = result->GetFace(i);
            
            int32_t trackID = face->GetTrackID();
            SmartPtr<FaceState> state = FindState(trackID);
            if (state.IsNull())
            {
                state = new FaceState();
                state->trackID = trackID;
                
                SmartPtr<iVec3Array> imagePoints = state->points;
                imagePoints->Resize((int32_t)mObjectPoints.size());
                
                mStates.push_back(state);
            }
            
            SmartPtr<iVec3Array> imagePoints = state->points;
            
            SmartPtr<iVec3Array> points = face->GetPoints();
            for (auto j = 0; j < piArrayCount(g_PointIndices); ++j)
            {
                int32_t index = g_PointIndices[j];
                vec3 p = points->GetItem(index);
                p.x *= width;
                p.y = (1 - p.y) * height;
                imagePoints->SetItem(j, p);
            }
            
            Solve(state);
        }
    }
    
    virtual void SetViewportSize(const vec2& size)
    {
        mViewportSize = size;
        SetImageOffset(size.x / 2, size.y / 2);
        
        float scale = 1;
        
        if (mViewportSize.x > 0)
        {
            scale = size.x / mViewportSize.x;
        }
        
        for (auto s : mStates)
        {
            s->rvec *= scale;
            s->tvec *= scale;
        }

//        mStates.clear();
    }
    
    virtual mat4 GetViewMatrix(int32_t index) const
    {
        piCheck(index >= 0 && index < mStates.size(), mat4());
        return mStates[index]->viewMatrix;
    }
    
private:
    
    void SetImageOffset(double cx, double cy)
    {
        mCamMatrix = Mat::zeros(3, 3, CV_64FC1);     // intrinsic camera parameters
        mCamMatrix.at<double>(0, 0) = fx;  // [ fx   0  cx ]
        mCamMatrix.at<double>(1, 1) = fy;  // [  0  fy  cy ]
        mCamMatrix.at<double>(0, 2) = cx;  // [  0   0   1 ]
        mCamMatrix.at<double>(1, 2) = cy;
        mCamMatrix.at<double>(2, 2) = 1;
    }
    
    FaceState* FindState(int32_t trackID)
    {
        for (auto s : mStates)
        {
            if (s->trackID == trackID)
            {
                return s;
            }
        }
        
        return nullptr;
    }
    
    bool Solve(FaceState* state)
    {
        vector<Point2f> imagePoints;

        for (int32_t i = 0; i < state->points->GetCount(); ++i)
        {
            vec3 p = state->points->GetItem(i);
            imagePoints.push_back(Point2f(p.x, p.y));
        }
        
        // Pose estimation
        bool correspondence = false;
        
        if (!state->init)
        {
            state->rvec = Mat::zeros(3, 1, CV_64FC1);
            state->tvec = Mat::zeros(3, 1, CV_64FC1);
            correspondence = solvePnPRansac(mObjectPoints,
                                            imagePoints,
                                            mCamMatrix,
                                            mDistCoeffs,
                                            state->rvec,
                                            state->tvec,
                                            false);
            state->init = correspondence;
        }
        else
        {
            correspondence = solvePnP(mObjectPoints,
                                      imagePoints,
                                      mCamMatrix,
                                      mDistCoeffs,
                                      state->rvec,
                                      state->tvec,
                                      true);
        }
        
        state->viewMatrix = BuildViewMatrix(state->rvec, state->tvec);
        
        return correspondence;
    }
};


iCalibrate* CreateCalibrate()
{
    return new Calibrate();
}

/**
 * =================================================================================================
 */

mat4 piProject(double left, double right, double bottom, double top, double near, double far)
{
    float width = abs(left - right);
    float height = abs(top - bottom);
    
    float cx = (right - left) / 2;
    float cy = (top - bottom) / 2;
    
    mat4 mat;
    
    mat[0][0] = 2.0f * fx / width;
    mat[0][1] = 0.0f;
    mat[0][2] = 0.0f;
    mat[0][3] = 0.0f;
    
    mat[1][0] = 0.0f;
    mat[1][1] = 2.0f * fy / height;
    mat[1][2] = 0.0f;
    mat[1][3] = 0.0f;
    
    mat[2][0] = 1.0f - 2.0f * cx / width;
    mat[2][1] = 2.0f * cy / height - 1.0f;
    mat[2][2] = -(far + near) / (far - near);
    mat[2][3] = -1.0f;
    
    mat[3][0] = 0.0f;
    mat[3][1] = 0.0f;
    mat[3][2] = -2.0f * far * near / (far - near);
    mat[3][3] = 0.0f;
    
    return mat;
}




NSPI_END()























