/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.30   1.0     Create
 ******************************************************************************/
#include <pi/CV.h>

extern "C"
{
#if PI_FACETRACKER_ENABLED
#   include <st_mobile_license.h>
#endif
}

using namespace std;

NSPI_BEGIN()

#if PI_FACETRACKER_ENABLED

#if defined(PI_MACOS)

bool InitSenseTimeSDK(const std::string& certificate)
{
    string appDir = piGetAppDirectory();
    string licDir = appDir + "SenseTimeSDK/";
    string licFile = licDir + "device.lic";
    
    if (!piFileExist(licFile))
    {
        int len = 10000;
    
        SmartPtr<iMemory> mem = CreateMemory(len);
        piAssert(!mem.IsNull(), false);
        
        st_result_t result = st_mobile_generate_license_online(certificate.c_str(), mem->Ptr(), &len);
        piAssert(result == ST_OK, false);
        
        piCheck(CreateDirectory(licDir, true), true);
        
        SmartPtr<iStream> stream = CreateFileStream(licFile, "w");
        piCheck(!stream.IsNull(), true);
        
        stream->Write(mem->Ptr(), len);
        
        return true;
    }
    else
    {
        return st_mobile_check_license(licFile.c_str()) == ST_OK;
    }
}

#else

bool InitSenseTimeSDK(const std::string& license)
{
    char activeCode[1024];
    int activeCodeLen = 1024;
    st_result_t ret = st_mobile_generate_activecode(license.c_str(), activeCode, &activeCodeLen);
    return ret == ST_OK;
}

#endif

#else

bool InitSenseTimeSDK(const std::string& license)
{
    return false;
}

#endif

NSPI_END()



















