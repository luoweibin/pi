/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.10   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>

using namespace std;

NSPI_BEGIN()

class RefObjectArray : public ObjectArrayImpl<iRefObject, iRefObjectArray>
{
public:
    RefObjectArray()
    {
    }
    
    virtual ~RefObjectArray()
    {
    }
    
    virtual string ToString() const
    {
        return piFormat("iRefObjectArray[%d]", GetCount());
    }
};


iRefObjectArray* CreateRefObjectArray()
{
    return new RefObjectArray();
}


NSPI_END()



















