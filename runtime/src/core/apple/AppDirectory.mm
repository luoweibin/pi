/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.10.20   0.1     Create
 ******************************************************************************/
#import <pi/Core.h>
#import <Foundation/Foundation.h>

NSPI_BEGIN()

using namespace std;

string piGetAppDirectory()
{
    NSArray<NSURL*>* dirs = [[NSFileManager defaultManager] URLsForDirectory:NSApplicationSupportDirectory
                                                                   inDomains:NSUserDomainMask];
    piAssert(dirs != nil && [dirs count] > 0, string());
    
    NSURL* url = dirs[0];
    NSString* path = url.path;
    
    NSString* appId = [[NSBundle mainBundle] bundleIdentifier];
    path = [path stringByAppendingPathComponent:appId];
    
    return [path cStringUsingEncoding:NSUTF8StringEncoding];
}

NSPI_END()

