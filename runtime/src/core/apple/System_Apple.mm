/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2015.4.23   0.1     Create
 ******************************************************************************/
#import <Foundation/Foundation.h>
#import <pi/Core.h>

using namespace std;

NSPI_BEGIN()

string piFindResource(const string& filename, const string& extension)
{
    piAssert(!filename.empty(), string());
    
    NSString* name = [NSString stringWithUTF8String:filename.c_str()];
    NSString* type = [NSString stringWithUTF8String:extension.c_str()];
    
    NSString* path = [[NSBundle mainBundle] pathForResource:name ofType:type];
    return [path UTF8String];
}


NSPI_END()




























