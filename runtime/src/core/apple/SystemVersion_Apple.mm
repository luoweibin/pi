/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.4.10   0.1     Create
 ******************************************************************************/
#import <Foundation/Foundation.h>
#import <pi/Core.h>

using namespace std;

NSPI_BEGIN()


int32_t piGetSystemVersionCode()
{
#if defined(PI_MACOS)
    NSOperatingSystemVersion v = [[NSProcessInfo processInfo] operatingSystemVersion];
    return (int32_t)(v.majorVersion * 10000 + v.minorVersion * 100 + v.patchVersion);
#elif defined(PI_IOS)
    NSOperatingSystemVersion v = [[NSProcessInfo processInfo] operatingSystemVersion];
    return (int32_t)(v.majorVersion * 10000 + v.minorVersion * 100 + v.patchVersion);
#else
    return 0;
#endif
}

int32_t piGetSystemMajorCode()
{
    NSOperatingSystemVersion v = [[NSProcessInfo processInfo] operatingSystemVersion];
    return (int32_t)v.majorVersion;
}

int32_t piGetSystemMinorCode()
{
    NSOperatingSystemVersion v = [[NSProcessInfo processInfo] operatingSystemVersion];
    return (int32_t)v.minorVersion;
}

int32_t piGetSystemPatchCode()
{
    NSOperatingSystemVersion v = [[NSProcessInfo processInfo] operatingSystemVersion];
    return (int32_t)v.patchVersion;
}

string piGetSystemVersionString()
{
#if defined(PI_MACOS)
    NSOperatingSystemVersion v = [[NSProcessInfo processInfo] operatingSystemVersion];
    return piFormat("macOS %d.%d.%d", v.majorVersion, v.minorVersion, v.patchVersion);
#elif defined(PI_IOS)
    NSOperatingSystemVersion v = [[NSProcessInfo processInfo] operatingSystemVersion];
    return piFormat("iOS %d.%d.%d", v.majorVersion, v.minorVersion, v.patchVersion);
#else
    return "";
#endif
}

NSPI_END()



















