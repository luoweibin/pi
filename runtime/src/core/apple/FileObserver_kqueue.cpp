/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2015.5.25   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>

#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>
#include <fcntl.h>
#include <thread>
#include <atomic>

using namespace std;

NSPI_BEGIN()

enum
{
    eMsg_Watch,
    eMsg_Quit,
};

struct PipeMsg
{
    int32_t msgId;
    int32_t file;
    int32_t events;
    int32_t eventId;
};

bool SetNonBlock(int fd)
{
    int flag = fcntl(fd, F_GETFL, 0);
    piAssert(flag == -1, false);
    fcntl(fd, F_SETFL, flag | O_NONBLOCK);
    return true;
}

struct FileEvent
{
    int fd;
    string path;
    
    FileEvent():fd(-1)
    {
    }
    
    FileEvent(const FileEvent &event)
    {
        fd = event.fd;
        path = event.path;
    }
};

class FileObserverKqueue : public iFileObserver
{
private:
    int mKQueue;
    
    int mPipe[2];
    
    atomic_int mFreeEventId;
    
    //map<event id, file handle>
    map<int32_t, FileEvent> mFileEvents;
    
    thread mThread;
    
    mutex mMutex;
    
    list<SmartPtr<iFileListener>> mListeners;
    
public:
    FileObserverKqueue()
    {
        mFreeEventId = 1;
        mKQueue = kqueue();
    }
    
    virtual ~FileObserverKqueue()
    {
        Quit();
        
        if (mThread.joinable())
        {
            mThread.join();
        }
        
        close(mKQueue);
        mKQueue = -1;
        
        if (mPipe[0] >= 0)
        {
            close(mPipe[0]);
            mPipe[0] = -1;
        }
        
        if (mPipe[1] >= 0)
        {
            close(mPipe[1]);
            mPipe[1] = -1;
        }
    }
    
    bool Init()
    {
        piAssert(pipe(mPipe) == 0, false);
        
        struct kevent event;
        EV_SET(&event, mPipe[0], EVFILT_READ, EV_ADD|EV_CLEAR, 0, 0, 0);
        kevent(mKQueue, &event, 1, nullptr, 0, nullptr);
        
        mThread = thread([this]{ Run(); });
        
        return true;
    }
    
    virtual int32_t Watch(const std::string &path, int32_t events)
    {
        piAssert(!path.empty(), -1);
        piAssert(events != 0, -1);
        
        int file = open(path.c_str(), O_RDONLY);
        piCheck(file >= 0, -1);
        
        int32_t eventId = mFreeEventId.fetch_add(1);
        
        {
            lock_guard<mutex> lock(mMutex);
            FileEvent event;
            event.fd = file;
            event.path = path;
            mFileEvents[eventId] = event;
        }
        
        PipeMsg msg;
        msg.msgId = eMsg_Watch;
        msg.file = file;
        msg.events = events;
        msg.eventId = eventId;
        
        SendMessage(msg);
        
        return eventId;
    }
    
    virtual void Remove(int32_t eventId)
    {
        piAssert(eventId > 0, ;);
        
        lock_guard<mutex> lock(mMutex);
        
        map<int32_t, FileEvent>::iterator it = mFileEvents.find(eventId);
        if (it != mFileEvents.end())
        {
            mFileEvents.erase(it);
            close(it->second.fd);
        }
    }
    
    virtual void RegisterListener(iFileListener *listener)
    {
        piAssert(listener != nullptr, ;);
        
        lock_guard<mutex> lock(mMutex);
        mListeners.push_back(listener);
    }
    
    virtual void UnregisterListener(iFileListener *listener)
    {
        piAssert(listener != nullptr, ;);
        lock_guard<mutex> lock(mMutex);
        mListeners.remove(listener);
    }
    
private:
    
    void Run()
    {
        while (true)
        {
            struct kevent events[32] = {0};
            int count = kevent(mKQueue, nullptr, 0, events, 32, nullptr);
            
            for (int i = 0; i < count; ++i)
            {
                struct kevent *ev = &events[i];
                int file = (int)ev->ident;
                if (file == mPipe[0])
                {
                    PipeMsg msg;
                    ReadMessage(msg);
                    switch (msg.msgId)
                    {
                        case eMsg_Watch:
                            HandleWatch(msg);
                            break;
                        case eMsg_Quit:
                            goto quit;
                        default:
                            break;
                    }
                }
                else
                {
                    int64_t eventId = (int64_t)ev->udata;
                    
                    FileEvent event;
                    list<SmartPtr<iFileListener>> listeners;
                    {
                        lock_guard<mutex> lock(mMutex);
                        listeners = mListeners;
                        
                        map<int32_t, FileEvent>::iterator it = mFileEvents.find((int32_t)eventId);
                        if (it != mFileEvents.end())
                        {
                            event = it->second;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    
                    int32_t events = 0;
                    
                    if (piFlagIs(ev->fflags, NOTE_WRITE))
                    {
                        piFlagOn(events, eFileEvent_Modify);
                    }
                    
                    if (piFlagIs(ev->fflags, NOTE_DELETE))
                    {
                        piFlagOn(events, eFileEvent_Delete);
                    }
                    
                    if (piFlagIs(ev->fflags, NOTE_RENAME))
                    {
                        piFlagOn(events, eFileEvent_Move);
                    }
                    
                    if (event.fd >= 0)
                    {
                        for (SmartPtr<iFileListener> listener : listeners)
                        {
                            listener->OnFileEvent(event.path, events);
                        }
                    }
                }
            }
            
            
        }
    quit:
        return;
    }
    
    void HandleWatch(const PipeMsg &msg)
    {
        uint32_t fflags = 0;
        if (piFlagIs(msg.events, eFileEvent_Modify))
        {
            fflags |= NOTE_WRITE;
        }
        if (piFlagIs(msg.events, eFileEvent_Delete))
        {
            fflags |= NOTE_DELETE;
        }
        if (piFlagIs(msg.events, eFileEvent_Move))
        {
            fflags |= NOTE_RENAME;
        }
        
        int64_t eventId = msg.eventId;
        struct kevent event;
        EV_SET(&event,
               msg.file,
               EVFILT_VNODE,
               EV_ADD|EV_CLEAR,
               fflags,
               0,
               (char*)eventId);
        
        kevent(mKQueue, &event, 1, nullptr, 0, nullptr);
    }
    
    void SendMessage(const PipeMsg &msg)
    {
        lock_guard<mutex> lock(mMutex);
        
        size_t sent = 0;
        while (sent < sizeof(msg))
        {
            int64_t size = write(mPipe[1], ((char*)&msg) + sent, sizeof(msg) - sent);
            if (size > 0)
            {
                sent += size;
            }
        }
    }
    
    void ReadMessage(PipeMsg &msg)
    {
        lock_guard<mutex> lock(mMutex);
        
        size_t readSize = 0;
        while (readSize < sizeof(msg))
        {
            int64_t size = read(mPipe[0], ((char*)&msg) + readSize, sizeof(msg) - readSize);
            if (size > 0)
            {
                readSize += size;
            }
        }
    }
    
    void Quit()
    {
        PipeMsg msg;
        msg.msgId = eMsg_Quit;
        msg.file = -1;
        msg.events = 0;
        
        SendMessage(msg);
    }
};


iFileObserver* CreateFileObserver()
{
    SmartPtr<FileObserverKqueue> observer = new FileObserverKqueue();
    if (observer->Init())
    {
        return observer.PtrAndSetNull();
    }
    else
    {
        return nullptr;
    }
}



NSPI_END()

























