/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2013.6.11   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>

using namespace std;

NSPI_BEGIN()

class Memory : public iMemory
{
public:
    Memory()
    {
        mBase = nullptr;
        mSize = 0;
    }
    
    virtual ~Memory()
    {
        if (mBase != nullptr)
        {
            free(mBase);
            mBase = nullptr;
        }
    }
    
    bool Init(int64_t size)
    {
        piAssert(size > 0, false);
        
        mBase = (char*) calloc((size_t)size, sizeof(uint8_t));
        piAssert(mBase != nullptr, false);
        mSize = size;
        
        return true;
    }
    
    virtual char* Ptr() const
    {
        return mBase;
    }
    
    virtual char* Buffer() const
    {
        return mBase;
    }
    
    virtual int64_t Size() const
    {
        return mSize;
    }
    
    virtual bool Resize(int64_t size)
    {
        char* pMem = (char*) realloc(mBase, (size_t)size);
        if (pMem != nullptr)
        {
            mBase = pMem;
            mSize = size;
            return true;
        }
        else
        {
            return false;
        }
    }
    
    virtual string ToString() const
    {
        return piFormat("iMemory(%p){size:%lld}", this, mSize);
    }
    
private:
    char* mBase;
    int64_t mSize;
};

iMemory* CreateMemory(int64_t size)
{
    SmartPtr<Memory> mem = new Memory();
    if (mem->Init(size))
    {
        return mem.PtrAndSetNull();
    }
    else
    {
        return nullptr;
    }
}

iMemory* CreateMemoryCopy(const void *mem, int64_t size)
{
    piAssert(mem != nullptr, nullptr);
    piAssert(size > 0, nullptr);
    
    SmartPtr<iMemory> ret = CreateMemory(size);
    piAssert(!ret.IsNull(), nullptr);
    
    memcpy(ret->Ptr(), mem, (size_t)size);
    
    return ret.PtrAndSetNull();
}

//==============================================================================


class StaticMemory : public iMemory
{
public:
    StaticMemory(const void* base, int64_t size)
    {
        mBase = (const char*)base;
        mSize = size;
    }
    
    virtual ~StaticMemory()
    {
        mBase = nullptr;
        mSize = 0;
    }
    
    virtual char* Ptr() const
    {
        return const_cast<char*>(mBase);
    }
    
    virtual char* Buffer() const
    {
        return const_cast<char*>(mBase);
    }
    
    virtual int64_t Size() const
    {
        return mSize;
    }
    
    virtual bool Resize(int64_t size)
    {
        return false;
    }
    
    virtual string ToString() const
    {
        return piFormat("iMemory(%p){size:%lld}", this, mSize);
    }
    
private:
    const char* mBase;
    int64_t mSize;
};

iMemory* CreateMemoryStatic(const void* mem, int64_t size)
{
    return new StaticMemory(mem, size);
}




NSPI_END()























