/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.4.10   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>

using namespace std;

NSPI_BEGIN()

static int32_t g_VerMajor = 0;
static int32_t g_VerMinor = 0;
static int32_t g_VerPatch = 0;
static int32_t g_VerCode = 0;

static string g_VerString = "";

void piSetSystemVersionCode(int32_t sdkLevel)
{
    g_VerMajor = sdkLevel;
    g_VerMinor = 0;
    g_VerPatch = 0;
    
    g_VerCode = eOS_Android * 1000000 + g_VerMajor * 10000;
}

void piSetSystemVersionString(const string& str)
{
    g_VerString = str;
}

int32_t piGetSystemVersionCode()
{
    return g_VerCode;
}

int32_t piGetSystemMajorCode()
{
    return g_VerMinor;
}

int32_t piGetSystemMinorCode()
{
    return g_VerMinor;
}

int32_t piGetSystemPatchCode()
{
    return g_VerPatch;
}

string piGetSystemVersionString()
{
    return g_VerString;
}

NSPI_END()



















