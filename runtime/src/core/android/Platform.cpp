/*******************************************************************************
 See copyright notice in LICENSE.

 History:
 caiwenqiang     2017.7.12   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>

#ifdef PI_ANDROID

NSPI_BEGIN()

static JavaVM* g_JavaVM = nullptr;
static pthread_key_t g_key;

static void _detachCurrentThread(void* a)
{
    g_JavaVM->DetachCurrentThread();
}

bool piInitJNI(JavaVM* pJVM)
{
    g_JavaVM = pJVM;
    pthread_key_create(&g_key, _detachCurrentThread);
    return true;
}

void piDeinitJNI(JavaVM* pJVM)
{
    g_JavaVM = nullptr;
}

static JNIEnv* cacheEnv(JavaVM* jvm)
{
    JNIEnv* _env = nullptr;
    // get jni environment
    jint ret = jvm->GetEnv((void**)&_env, JNI_VERSION_1_4);

    switch (ret)
    {
        case JNI_OK :
            // Success!
            pthread_setspecific(g_key, _env);
            return _env;

        case JNI_EDETACHED :
            // Thread not attached
            if (jvm->AttachCurrentThread(&_env, nullptr) < 0)
            {
                PILOGE(PI_TAG, "Failed to get the environment using AttachCurrentThread()");

                return nullptr;
            }
            else
            {
                // Success : Attached and obtained JNIEnv!
                pthread_setspecific(g_key, _env);
                return _env;
            }

        case JNI_EVERSION :
            // Cannot recover from this error
            PILOGE(PI_TAG, "JNI interface version 1.4 not supported");
        default :
            PILOGE(PI_TAG, "Failed to get the environment using GetEnv()");
            return nullptr;
    }
}

JNIEnv* piAttachJVM()
{
    JNIEnv *_env = (JNIEnv *)pthread_getspecific(g_key);
    if (_env == nullptr)
        _env = cacheEnv(g_JavaVM);
    return _env;
}

NSPI_END()

#endif
