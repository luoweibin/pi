/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2012.9.7   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>

#include <wchar.h>
#include <ctype.h>

#include <locale>
#include <algorithm>

#if !defined(PI_ANDROID)
#include <codecvt>
#endif

using namespace std;

NSPI_BEGIN()

bool piIsStringUTF8Empty(const char *pszValue)
{
    return pszValue == NULL || strlen(pszValue) == 0;
}

string piBinToHexStringUTF8(const void* pData, int64_t luSize, bool bUpper)
{
    piAssert(pData != NULL, string());
    piAssert(luSize > 0, string());
    
    uint8_t* pszCur = (uint8_t*)pData;
    string strResult;
    for (int64_t i = 0; i < luSize; ++i)
    {
        char buffer[3];
        if (bUpper)
        {
            snprintf(buffer, 3, "%02X", pszCur[i]);
        }
        else
        {
            snprintf(buffer, 3, "%02x", pszCur[i]);
        }
        strResult.append(1, buffer[0]);
        strResult.append(1, buffer[1]);
    }
    
    return strResult;
}
//=============================================================================
// print function

#if defined (PI_WINDOWS)

int32_t piVsnprintfOS(char_t* pBuffer, int32_t luSize, const char_t* pFormat, va_list ap)
{
    piAssert(pBuffer != NULL, 0);
    piCheck(luSize > 0, 0);
    
    sint32_t ldRet = _vsntprintf_s(pBuffer, luSize-1, _TRUNCATE, pFormat, ap);
    if (ldRet >= 0)
    {
        return ldRet;
    }
    else
    {
        pBuffer[luSize-1] = '\0';
        return luSize;
    }
}

int32_t piSnprintfOS(char_t* pBuffer, int32_t luSize, const char_t* pFormat, ...)
{
    va_list ap;
    va_start(ap, pFormat);
    int32_t luRet = piVsnprintfOS(pBuffer, luSize, pFormat, ap);
    va_end(ap);
    return luRet;
}

#endif

#if !defined(PI_ANDROID)

u16string piU8ToU16(const string& text)
{
    wstring_convert<codecvt_utf8_utf16<char16_t>, char16_t> conv;
    return conv.from_bytes(text);
}


string piU16ToU8(const u16string& text)
{
    wstring_convert<codecvt_utf8_utf16<char16_t>, char16_t> conv;
    return conv.to_bytes(text);
}

#endif

string piTrimLeft(const string& str, const string& chars)
{
    string ret(str);
    auto it = find_if(ret.begin(), ret.end(), [&chars](char ch){ return chars.find(ch) == string::npos; });
    ret.erase(ret.begin(), it);
    return ret;
}

string piTrimRight(const string& str, const string& chars)
{
    string ret(str);
    auto it = find_if(ret.rbegin(), ret.rend(), [&chars](char ch){ return chars.find(ch) == string::npos; });
    ret.erase(it.base(), ret.end());
    return ret;
}

string piTrim(const string& str, const string& chars)
{
    string ret = piTrimLeft(str, chars);
    return piTrimRight(ret, chars);
}


NSPI_END()

















