/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2012.6.25   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>

#include <future>
#include <mutex>
#include <thread>
#include <time.h>

#if defined (PI_ANDROID)
#   include <android/log.h>
#endif

using namespace std;

NSPI_BEGIN()

////////////////////////////////////////////////////////////////////////////////

static const char_t* piLogLevelName(int dLevel)
{
    switch (dLevel) 
    {
        case LOG_ERROR:
            return TT("ERROR");
            
        case LOG_WARNING:
            return TT("WARNING");
            
        case LOG_SYSTEM:
            return TT("SYSTEM");
            
        case LOG_DEBUG:
            return TT("DEBUG");
            
        case LOG_INFO:
            return TT("INFO");
            
        case LOG_VERBOSE:
            return TT("VERBOSE");
            
        default:
            return TT("?");
    }
}

static std::string FormatMessage(const char_t* pszMsg,
                            int64_t size,
                            int dLevel,
                            const char_t* pszFile,
                            int dLine,
                            const char_t* pszTag)
{
    time_t now = time(NULL);
    struct tm ltime;
    localtime_r(&now, &ltime);
    
    static const int64_t s_luBufSize = 128;
    char_t buffer[s_luBufSize];
    
#if defined(PI_UNIX)
    int64_t luWriteSize = strftime(buffer, s_luBufSize, "## %T.", &ltime);
//    int64_t luWriteSize = strftime(buffer, s_luBufSize, "## %F %T.", &ltime);
#else
	int64_t luWriteSize = 0;
#endif
    buffer[luWriteSize] = '\0';
    
    std::string strFile;
    if (pszFile != NULL)
    {
        strFile = piGetBaseName(pszFile);
    }
    else
    {
        strFile = TT("-");
    }
    
    std::string strTag;
    if (pszTag != NULL)
    {
        strTag += pszTag;
    }
    if (strTag.empty())
    {
        strTag = TT("-");
    }
    
    std::string message = piFormat(TT("%s%03llu [%u,%s:%d] %s ## [%s]"),
                              buffer,
                              piGetSystemTimeMS() % piSec2Ms(1),
                              this_thread::get_id(),
                              strFile.c_str(),
                              dLine,
                              piLogLevelName(dLevel),
                              strTag.c_str());
    
    message.append(pszMsg, (size_t)size);
    message.append(1, '\n');
    return message;
}

////////////////////////////////////////////////////////////////////////////////
// Backend

static void PrintConsole(int dLevel, const char_t* pszTag, const char_t* pInfo)
{
    piAssert(pInfo != NULL, ;);
    
#if defined(PI_ANDROID) 
    switch (dLevel) {
        case LOG_ERROR:
            __android_log_write(ANDROID_LOG_ERROR, pszTag, pInfo);
            break;
            
        case LOG_SYSTEM:
        case LOG_WARNING:
            __android_log_write(ANDROID_LOG_WARN, pszTag, pInfo);
            break;

        case LOG_INFO:
            __android_log_write(ANDROID_LOG_INFO, pszTag, pInfo);
            break;
            
        case LOG_DEBUG:
            __android_log_write(ANDROID_LOG_DEBUG, pszTag, pInfo);
            break;

        case LOG_VERBOSE:
        default:
            __android_log_write(ANDROID_LOG_VERBOSE, pszTag, pInfo);
            break;
    }
#else
    printf("%s", pInfo);
#endif    
}

//==============================================================================

class cRobinFileLogBackend : public iRefObject
{
private:
    SmartPtr<iStream> mptrFile;
    int32_t mFileCount;
    int64_t mFileSize;
    std::string mPath;
    
public:
    cRobinFileLogBackend()
    {
    }
    
    virtual ~cRobinFileLogBackend()
    {
        mptrFile = NULL;
    }
    
    bool Init(int64_t fileSize,
              int32_t fileCount,
              const std::string &path)
    {
        mFileCount = fileCount;
        mFileSize = fileSize;
        mPath = path;
        piAssert(CreateDirectory(path, true), false);
        return true;
    }
    
    virtual void Write(int dLevel, const char_t* pszTag, const char_t* pInfo, int32_t luSize)
    {
        if (mptrFile.IsNull())
        {
            mptrFile = OpenFile(GetLastFileIndex());
        }
        
        if (!mptrFile.IsNull())
        {
            mptrFile->Write(pInfo, luSize);
            
            if (mptrFile->GetSize() >= mFileSize)
            {
                DeleteFiles();
                mptrFile = OpenFile(GetLastFileIndex() + 1);
            }
        }
    }
    
    virtual void Close()
    {
        mptrFile = NULL;
    }
    
private:
    iStream* OpenFile(int64_t dIndex)
    {
        std::string strPath = GetFilePath(dIndex);
        SmartPtr<iStream> ptrFile = CreateFileStream(strPath.c_str(), "a");
        return ptrFile.PtrAndSetNull();
    }
    
    int64_t GetLastFileIndex() const
    {
        int64_t dIndex = 0;
        SmartPtr<iFileFinder> ptrFinder = CreateFileFinder();
        if (ptrFinder->Search(mPath.c_str(), eFileFinderFlag_Descending))
        {
            std::string strFileName = ptrFinder->Name();
            std::string::size_type ldIndex = strFileName.find_last_of('.');
            if (ldIndex != std::string::npos && ldIndex + 1 < mPath.size())
            {
                std::string strIndex = strFileName.substr(ldIndex + 1, strFileName.size());
                dIndex = atoi(strIndex.c_str());
            }
        }
        
        return dIndex;
    }
    
    std::string GetFilePath(int64_t dIndex)
    {
        std::string strFilePath = mPath;
        strFilePath = strFilePath + PI_PATHDEL;
        return piFormat(TT("%s%06d.log"), strFilePath.c_str(), dIndex);
    }
    
    void DeleteFiles()
    {
        std::string strFilePath = mPath;
        SmartPtr<iFileFinder> ptrFinder = CreateFileFinder();
        if (!ptrFinder->Search(strFilePath.c_str(), eFileFinderFlag_Ascending))
        {
            return;
        }
        
        int32_t luCount = ptrFinder->Size();
        std::string strFileName = ptrFinder->Name();
        if (luCount < mFileCount)
        {
            return;
        }
        
        luCount -= mFileCount - 1;
        while (luCount > 0)
        {
            std::string strFileName = ptrFinder->Name();
            std::string strPath(strFilePath);
            strPath = strPath + PI_PATHDEL;
            strPath = strPath + strFileName;
            piDeleteFile(strPath.c_str());
            --luCount;
            ptrFinder->MoveNext();
        }
    }
};

////////////////////////////////////////////////////////////////////////////////
// Log

struct LogBlock : public iRefObject
{
    int32_t dLevel;
    uint16_t uTag;
    uint16_t uData;
    char_t data[PI_LOG_MESSAGE_MAXSIZE];
    
    virtual ~LogBlock() {}
};

class cLogClient : public iRefObject
{
private:
    enum eMessage
    {
        eMessage_Quit,
        eMessage_Log,
    };
    
    int64_t mLogSize;
    int64_t mLimit;
    int mGlobalLevel;
    int mConsoleLevel;
    int mFileLevel;
    
    string mFilter;
    
    SmartPtr<iMessageQueue> mMQ;
    SmartPtr<cRobinFileLogBackend> mFileBackend;
    
    mutex mMutex;
    thread mThread;
    
public:    
    cLogClient()
    {
        mLimit = 1024 * 1024 * 2;
        mLogSize = 0;
        mConsoleLevel = LOG_DISABLED;
        mFileLevel = LOG_DISABLED;
        mGlobalLevel = LOG_SYSTEM;
        mMQ = CreateMessageQueue();
        mThread = thread([this]{
            Run();
        });
    }
    
    virtual ~cLogClient()
    {
        mMQ->PostMessage(nullptr, eMessage_Quit);
        if (mThread.joinable())
        {
            mThread.join();
        }
    }
    
    void SetBackendLevel(int backend, int dLevel)
    {
        lock_guard<mutex> lock(mMutex);
        switch (backend)
        {
            case eLog_Console:
                mConsoleLevel = dLevel;
                break;
            case eLog_File:
                mFileLevel = dLevel;
                break;
            case eLog_Global:
                mGlobalLevel = dLevel;
                break;
            default:
                break;
        }
    }
    
    void SetFilter(const string& tag)
    {
        mFilter = tag;
    }
    
    bool Print(const char_t* pszFile,
               int dLine,
               int dLevel,
               const char_t* pszTag,
               const char_t* pszFormat,
               ...)
    {
        va_list args;
        va_start(args, pszFormat);
        bool bRet = PrintV(pszFile, dLine, dLevel, pszFormat, pszTag, args);
        va_end(args);
        
        return bRet;
    }
    
    bool PrintV(const char_t* pszFile,
                int dLine,
                int dLevel,
                const char_t* pszTag,
                const char_t* pszFormat,
                va_list args)
    {
        piCheck(!piIsStringEmpty(pszFormat), false);
        
        {
            lock_guard<mutex> lock(mMutex);
            if (dLevel > mGlobalLevel)
            {
                return true;
            }
            
            if (!mFilter.empty() && strcasecmp(mFilter.c_str(), pszTag) != 0)
            {
                return true;
            }
        }
        
        SmartPtr<LogBlock> block = new LogBlock();
        block->dLevel = dLevel;
        if (!piIsStringEmpty(pszTag))
        {
            block->uTag = piPrint(block->data,
                                   PI_LOG_MESSAGE_MAXSIZE,
                                   TT("%s"),
                                   pszTag) + 1;
        }
        else
        {
            block->data[0] = '\0';
            block->uTag = 1;
        }
        
        std::string strMsg = piFormatV(pszFormat, args);
        std::string strData = FormatMessage(strMsg.c_str(),
                                       strMsg.size(),
                                       dLevel,
                                       pszFile,
                                       dLine,
                                       pszTag);
        
        int64_t uLen = piMin(PI_LOG_MESSAGE_MAXSIZE - block->uTag - 1, strData.size());
        memcpy(block->data + block->uTag, strData.c_str(), (size_t)uLen);
        block->data[block->uTag + uLen + 1] = '\0';
        block->uData = uLen + 1;
        
        {
            lock_guard<mutex> lock(mMutex);
            piCheck(mLogSize + (int32_t)sizeof(LogBlock) <= mLimit, false);
            mLogSize += sizeof(LogBlock);
            mMQ->PostMessage(NULL, eMessage_Log, block.Ptr());
        }
        
        return true;
    }
    
    void Stop(int32_t timeoutMS)
    {
        mMQ->PostMessage(NULL, eMessage_Quit);
    }
    
private:
    void Run()
    {
        while (true)
        {
            SmartPtr<iMessage> message = mMQ->WaitForMessage();
            uint32_t msgId = message->GetID();
            if (msgId == eMessage_Quit)
            {
                break;
            }
            
            SmartPtr<LogBlock> block = piQueryVarObject<LogBlock>(message->GetArg1());
            FlushLogBlock(block->dLevel,
                          block->data,
                          block->data + block->uTag,
                          block->uData);
            
            lock_guard<mutex> lock(mMutex);
            mLogSize -= sizeof(LogBlock);
        }
    }
    
    void FlushLogBlock(int level, const char_t *tag, const char *info, int32_t size)
    {
        if (level <= mConsoleLevel)
        {
            PrintConsole(level, tag, info);
        }
        
        if (level <= mFileLevel && !mFileBackend.IsNull())
        {
            mFileBackend->Write(level, tag, info, size);
        }
    }
};

static SmartPtr<cLogClient> g_ptrLogClient;

void piInitLog()
{
    g_ptrLogClient = new cLogClient();
}

void piDeinitLog()
{
    g_ptrLogClient = nullptr;
}

void piSetLogLevel(int backend, int dLevel)
{
    if (!g_ptrLogClient.IsNull())
    {
        g_ptrLogClient->SetBackendLevel(backend, dLevel);
    }
}

void piSetLogFilter(const string tag)
{
    if (!g_ptrLogClient.IsNull())
    {
        g_ptrLogClient->SetFilter(tag);
    }
}

void _piLog(const char_t* pszFile,
                  int dLine,
                  int dLevel,
                  const char_t* pszFormat,
                  ...)
{
    piAssert(dLine >= 0, ;);
    piAssert(pszFormat != NULL, ;);
    
    if (!g_ptrLogClient.IsNull())
    {
        va_list args;
        va_start(args, pszFormat);
        g_ptrLogClient->PrintV(pszFile, dLine, dLevel, NULL, pszFormat, args);
        va_end(args);
    }
}

void _piLogT(const char_t* pszFile,
                   int dLine,
                   int dLevel,
                   const char_t* pszTag,
                   const char_t* pszFormat,
                   ...)
{
    piAssert(dLine >= 0, ;);
    piAssert(pszFormat != NULL, ;);
    
    if (!g_ptrLogClient.IsNull())
    {
        va_list args;
        va_start(args, pszFormat);
        g_ptrLogClient->PrintV(pszFile, dLine, dLevel, pszTag, pszFormat, args);
        va_end(args);
    }
}

void _piLogv(const char_t *pszFile,
                   int dLine,
                   int dLevel,
                   const char_t *pszFormat,
                   va_list args)
{
    if (!g_ptrLogClient.IsNull())
    {
        g_ptrLogClient->PrintV(pszFile, dLine, dLevel, NULL, pszFormat, args);
    }
}

void _piLogvT(const char_t* pszFile,
                    int dLine,
                    int dLevel,
                    const char_t* pszTag,
                    const char_t* pszFormat,
                    va_list args)
{
    if (!g_ptrLogClient.IsNull())
    {
        g_ptrLogClient->PrintV(pszFile, dLine, dLevel, pszTag, pszFormat, args);
    }
}



NSPI_END()




































