/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.10   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>

using namespace std;

NSPI_BEGIN()

class Enum : public iEnum
{
protected:
    string mSimpleName;
    string mFullName;
    
    typedef map<string, int> ValueMap;
    ValueMap mValueMap;
    
public:
    Enum()
    {
    }
    
    virtual ~Enum()
    {
    }
    
    virtual string GetName() const
    {
        return mSimpleName;
    }
    
    virtual void SetName(const string& name)
    {
        piAssert(!name.empty(), ;);
        piAssert(!IsReadonly(), ;);
        mSimpleName = name;
    }
    
    virtual string GetFullName() const
    {
        return mFullName;
    }
    
    virtual void SetFullName(const string& name)
    {
        piAssert(!name.empty(), ;);
        piAssert(!IsReadonly(), ;);
        mFullName = name;
    }
    
    virtual void RegisterValue(const std::string& name, int value)
    {
        piAssert(!name.empty(), ;);
        piAssert(!IsReadonly(), ;);
        
        auto it = mValueMap.find(name);
        piAssert(it == mValueMap.end(), ;);
        
        mValueMap[name] = value;
    }
    
    virtual int GetValue(const string& name, int defValue = -1) const
    {
        piAssert(!name.empty(), defValue);
        
        auto it = mValueMap.find(name);
        return it != mValueMap.end() ? it->second : defValue;
    }
    
    virtual bool HasValue(const string& name) const
    {
        piAssert(!name.empty(), false);
        
        auto it = mValueMap.find(name);
        return it != mValueMap.end();
    }
    
    virtual string GetString(int value) const
    {
        for (auto pair : mValueMap)
        {
            if (pair.second == value)
            {
                return pair.first;
            }
        }
        
        return piFormat("%s_Unknown", mSimpleName.c_str());
    }
};

iEnum* CreateEnum()
{
    return new Enum();
}


NSPI_END()
















