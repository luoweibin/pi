/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2013.6.11   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>

using namespace std;

NSPI_BEGIN()

Var::Var()
{
    Init();
}

Var::Var(float v)
{
    mdType = eType_F32;
    mValue.f32 = v;
}

Var::Var(int8_t v)
{
    mdType = eType_I8;
    mValue.i8 = v;
}

Var::Var(int16_t v)
{
    mdType = eType_I16;
    mValue.i16 = v;
}

Var::Var(int32_t v)
{
    mdType = eType_I32;
    mValue.i32 = v;
}

Var::Var(int64_t v)
{
    mdType = eType_I64;
    mValue.i64 = v;
}

Var::Var(uint8_t v)
{
    mdType = eType_U8;
    mValue.u8 = v;
}

Var::Var(uint16_t v)
{
    mdType = eType_U16;
    mValue.u16 = v;
}

Var::Var(uint32_t v)
{
    mdType = eType_U32;
    mValue.u32 = v;
}

Var::Var(uint64_t v)
{
    mdType = eType_U64;
    mValue.u64 = v;
}

Var::Var(double v)
{
    mdType = eType_F64;
    mValue.f64 = v;
}

Var::Var(bool v)
{
    mdType = eType_Boolean;
    mValue.b = v;
}

Var::Var(const char* v)
{
    mdType = eType_String;
    mValue.pStr = new string(v);
}

Var::Var(const string &v)
{
    mdType = eType_String;
    mValue.pStr = new string(v);
}

Var::Var(const vec2 &v)
{
    mdType = eType_Vec2;
    mValue.pVec2 = new vec2(v);
}

Var::Var(const vec3 &v)
{
    mdType = eType_Vec3;
    mValue.pVec3 = new vec3(v);
}

Var::Var(const vec4 &v)
{
    mdType = eType_Vec4;
    mValue.pVec4 = new vec4(v);
}

Var::Var(const mat4 &v)
{
    mdType = eType_Mat4;
    mValue.pMat4 = new mat4(v);
}

Var::Var(const quat &v)
{
    mdType = eType_Quat;
    mValue.pQuat = new quat(v);
}

Var::Var(const rect& v)
{
    mdType = eType_Rect;
    mValue.pRect = new rect(v);
}

Var::Var(iRefObject* v)
{
    if (v != NULL)
    {
        mdType = eType_Object;
        mValue.pObj = v;
        v->Retain();
    }
    else
    {
        Init();
    }
}

Var::Var(iI8Array* v)
{
    if (v != NULL)
    {
        mdType = eType_I8Array;
        mValue.pI8Array = v;
        v->Retain();
    }
    else
    {
        Init();
    }
}

Var::Var(iU8Array* v)
{
    if (v != NULL)
    {
        mdType = eType_U8Array;
        mValue.pU8Array = v;
        v->Retain();
    }
    else
    {
        Init();
    }
}

Var::Var(iI16Array* v)
{
    if (v != NULL)
    {
        mdType = eType_I16Array;
        mValue.pI16Array = v;
        v->Retain();
    }
    else
    {
        Init();
    }
}

Var::Var(iU16Array* v)
{
    if (v != NULL)
    {
        mdType = eType_U16Array;
        mValue.pU16Array = v;
        v->Retain();
    }
    else
    {
        Init();
    }
}

Var::Var(iI32Array* v)
{
    if (v != NULL)
    {
        mdType = eType_I32Array;
        mValue.pI32Array = v;
        v->Retain();
    }
    else
    {
        Init();
    }
}

Var::Var(iU32Array* v)
{
    if (v != NULL)
    {
        mdType = eType_U32Array;
        mValue.pU32Array = v;
        v->Retain();
    }
    else
    {
        Init();
    }
}

Var::Var(iI64Array* v)
{
    if (v != NULL)
    {
        mdType = eType_I64Array;
        mValue.pI64Array = v;
        v->Retain();
    }
    else
    {
        Init();
    }
}

Var::Var(iU64Array* v)
{
    if (v != NULL)
    {
        mdType = eType_U64Array;
        mValue.pU64Array = v;
        v->Retain();
    }
    else
    {
        Init();
    }
}

Var::Var(iF32Array* v)
{
    if (v != NULL)
    {
        mdType = eType_F32Array;
        mValue.pF32Array = v;
        v->Retain();
    }
    else
    {
        Init();
    }
}

Var::Var(iF64Array* v)
{
    if (v != NULL)
    {
        mdType = eType_F64Array;
        mValue.pF64Array = v;
        v->Retain();
    }
    else
    {
        Init();
    }
}

Var::Var(iVec2Array* v)
{
    if (v != NULL)
    {
        mdType = eType_Vec2Array;
        mValue.pVec2Array = v;
        v->Retain();
    }
    else
    {
        Init();
    }
}

Var::Var(iVec3Array* v)
{
    if (v != NULL)
    {
        mdType = eType_Vec3Array;
        mValue.pVec3Array = v;
        v->Retain();
    }
    else
    {
        Init();
    }
}

Var::Var(iVec4Array* v)
{
    if (v != NULL)
    {
        mdType = eType_Vec4Array;
        mValue.pVec4Array = v;
        v->Retain();
    }
    else
    {
        Init();
    }
}

Var::Var(iRectArray* v)
{
    if (v != NULL)
    {
        mdType = eType_RectArray;
        mValue.pRectArray = v;
        v->Retain();
    }
    else
    {
        Init();
    }
}

Var::Var(iQuatArray* v)
{
    if (v != NULL)
    {
        mdType = eType_QuatArray;
        mValue.pQuatArray = v;
        v->Retain();
    }
    else
    {
        Init();
    }
}

Var::Var(iMat4Array* v)
{
    if (v != NULL)
    {
        mdType = eType_Mat4Array;
        mValue.pMat4Array = v;
        v->Retain();
    }
    else
    {
        Init();
    }
}

Var::Var(const Var& a)
{
    Copy(a);
}

Var::~Var()
{
    ReleaseMemory();
}

void Var::ReleaseMemory()
{
    switch (mdType)
    {
        case eType_I8Array:
            mValue.pI8Array->Release();
            mValue.pI8Array = nullptr;
            break;
        case eType_U8Array:
            mValue.pU8Array->Release();
            mValue.pU8Array = nullptr;
            break;
        case eType_I16Array:
            mValue.pI16Array->Release();
            mValue.pI16Array = nullptr;
            break;
        case eType_U16Array:
            mValue.pU16Array->Release();
            mValue.pU16Array = nullptr;
            break;
        case eType_I32Array:
            mValue.pI32Array->Release();
            mValue.pI32Array = nullptr;
            break;
        case eType_U32Array:
            mValue.pU32Array->Release();
            mValue.pU32Array = nullptr;
            break;
        case eType_I64Array:
            mValue.pI64Array->Release();
            mValue.pI64Array = nullptr;
            break;
        case eType_U64Array:
            mValue.pU64Array->Release();
            mValue.pU64Array = nullptr;
            break;
        case eType_F32Array:
            mValue.pF32Array->Release();
            mValue.pF32Array = nullptr;
            break;
        case eType_F64Array:
            mValue.pF64Array->Release();
            mValue.pF64Array = nullptr;
            break;
        case eType_Vec2Array:
            mValue.pVec2Array->Release();
            mValue.pVec2Array = nullptr;
            break;
        case eType_Vec3Array:
            mValue.pVec3Array->Release();
            mValue.pVec3Array = nullptr;
            break;
        case eType_Vec4Array:
            mValue.pVec4Array->Release();
            mValue.pVec4Array = nullptr;
            break;
        case eType_QuatArray:
            mValue.pQuatArray->Release();
            mValue.pQuatArray = nullptr;
            break;
        case eType_RectArray:
            mValue.pRectArray->Release();
            mValue.pRectArray = nullptr;
            break;
        case eType_Mat4Array:
            mValue.pMat4Array->Release();
            mValue.pMat4Array = nullptr;
            break;
        case eType_Object:
            mValue.pObj->Release();
            mValue.pObj = nullptr;
            break;
        case eType_String:
            delete mValue.pStr;
            mValue.pStr = nullptr;
            break;
        case eType_Vec2:
            delete mValue.pVec2;
            mValue.pVec2 = nullptr;
            break;
        case eType_Vec3:
            delete mValue.pVec3;
            mValue.pVec3 = nullptr;
            break;
        case eType_Vec4:
            delete mValue.pVec4;
            mValue.pVec4 = nullptr;
            break;
        case eType_Mat4:
            delete mValue.pMat4;
            mValue.pMat4 = nullptr;
            break;
        case eType_Quat:
            delete mValue.pQuat;
            mValue.pQuat = nullptr;
            break;
        case eType_Rect:
            delete mValue.pRect;
            break;
        default:
            mValue.i64 = 0;
            break;
    }
    mdType = eType_Null;
}

void Var::Copy(const Var &a)
{
    switch (a.mdType)
    {
        case eType_I8Array:
            mValue.pI8Array = a.mValue.pI8Array;
            mValue.pI8Array->Retain();
            break;
        case eType_U8Array:
            mValue.pU8Array = a.mValue.pU8Array;
            mValue.pU8Array->Retain();
            break;
        case eType_I16Array:
            mValue.pI16Array = a.mValue.pI16Array;
            mValue.pI16Array->Retain();
            break;
        case eType_U16Array:
            mValue.pU16Array = a.mValue.pU16Array;
            mValue.pU16Array->Retain();
            break;
        case eType_I32Array:
            mValue.pI32Array = a.mValue.pI32Array;
            mValue.pI32Array->Retain();
            break;
        case eType_U32Array:
            mValue.pU32Array = a.mValue.pU32Array;
            mValue.pU32Array->Retain();
            break;
        case eType_I64Array:
            mValue.pI64Array = a.mValue.pI64Array;
            mValue.pI64Array->Retain();
            break;
        case eType_U64Array:
            mValue.pU64Array = a.mValue.pU64Array;
            mValue.pU64Array->Retain();
            break;
        case eType_F32Array:
            mValue.pF32Array = a.mValue.pF32Array;
            mValue.pF32Array->Retain();
            break;
        case eType_F64Array:
            mValue.pF64Array = a.mValue.pF64Array;
            mValue.pF64Array->Retain();
            break;
        case eType_RectArray:
            mValue.pRectArray = a.mValue.pRectArray;
            mValue.pRectArray->Retain();
            break;
        case eType_QuatArray:
            mValue.pQuatArray = a.mValue.pQuatArray;
            mValue.pQuatArray->Retain();
            break;
        case eType_Mat4Array:
            mValue.pMat4Array = a.mValue.pMat4Array;
            mValue.pMat4Array->Retain();
            break;
        case eType_Vec2Array:
            mValue.pVec2Array = a.mValue.pVec2Array;
            mValue.pVec2Array->Retain();
            break;
        case eType_Vec3Array:
            mValue.pVec3Array = a.mValue.pVec3Array;
            mValue.pVec3Array->Retain();
            break;
        case eType_Vec4Array:
            mValue.pVec4Array = a.mValue.pVec4Array;
            mValue.pVec4Array->Retain();
            break;
        case eType_Object:
            mValue.pObj = a.mValue.pObj;
            mValue.pObj->Retain();
            break;
        case eType_String:
            mValue.pStr = new string(*a.mValue.pStr);
            break;
        case eType_Vec2:
            mValue.pVec2 = new vec2(*a.mValue.pVec2);
            break;
        case eType_Vec3:
            mValue.pVec3 = new vec3(*a.mValue.pVec3);
            break;
        case eType_Vec4:
            mValue.pVec4 = new vec4(*a.mValue.pVec4);
            break;
        case eType_Mat4:
            mValue.pMat4 = new mat4(*a.mValue.pMat4);
            break;
        case eType_Quat:
            mValue.pQuat = new quat(*a.mValue.pQuat);
            break;
        case eType_Rect:
            mValue.pRect = new rect(*a.mValue.pRect);
            break;
        default:
            mValue.i64 = a.mValue.i64;
            break;
    }
    
    mdType = a.mdType;
}

void Var::Init()
{
    mdType = eType_Null;
    mValue.i64 = 0;
}

Var& Var::operator=(const Var& a)
{
    if (this == &a)
    {
        return *this;
    }
    
    ReleaseMemory();
    Copy(a);
    
    return *this;
}

void Var::SetNull()
{
    ReleaseMemory();
}

void Var::SetBoolean(bool v)
{
    ReleaseMemory();
    mdType = eType_Boolean;
    mValue.b = v;
}

void Var::SetI8(int8_t v)
{
    ReleaseMemory();
    mdType = eType_I8;
    mValue.i8 = v;
}

void Var::SetI16(int16_t v)
{
    ReleaseMemory();
    mdType = eType_I16;
    mValue.i16 = v;
}

void Var::SetI32(int32_t v)
{
    ReleaseMemory();
    mdType = eType_I32;
    mValue.i32 = v;
}

void Var::SetI64(int64_t v)
{
    ReleaseMemory();
    mdType = eType_I64;
    mValue.i64 = v;
}

void Var::SetU8(uint8_t v)
{
    ReleaseMemory();
    mdType = eType_U8;
    mValue.u8 = v;
}

void Var::SetU16(uint16_t v)
{
    ReleaseMemory();
    mdType = eType_U16;
    mValue.u16 = v;
}

void Var::SetU32(uint32_t v)
{
    ReleaseMemory();
    mdType = eType_U32;
    mValue.u32 = v;
}

void Var::SetU64(uint64_t v)
{
    ReleaseMemory();
    mdType = eType_U64;
    mValue.u64 = v;
}

void Var::SetF32(float v)
{
    ReleaseMemory();
    mdType = eType_F32;
    mValue.f32 = v;
}

void Var::SetF64(double v)
{
    ReleaseMemory();
    mdType = eType_F64;
    mValue.f64 = v;
}

void Var::SetObject(iRefObject* v)
{
    ReleaseMemory();
    if (v != NULL)
    {
        mdType = eType_Object;
        mValue.pObj = v;
        v->Retain();
    }
    else
    {
        mdType = eType_Null;
        mValue.pObj = NULL;
    }
}

void Var::SetI8Array(iI8Array* v)
{
    ReleaseMemory();
    if (v != NULL)
    {
        mdType = eType_I8Array;
        mValue.pObj = v;
        v->Retain();
    }
    else
    {
        mdType = eType_Null;
        mValue.pObj = NULL;
    }
}

void Var::SetI16Array(iI16Array* v)
{
    ReleaseMemory();
    if (v != NULL)
    {
        mdType = eType_I16Array;
        mValue.pObj = v;
        v->Retain();
    }
    else
    {
        mdType = eType_Null;
        mValue.pObj = NULL;
    }
}

void Var::SetI32Array(iI32Array* v)
{
    ReleaseMemory();
    if (v != NULL)
    {
        mdType = eType_I32Array;
        mValue.pObj = v;
        v->Retain();
    }
    else
    {
        mdType = eType_Null;
        mValue.pObj = NULL;
    }
}

void Var::SetI64Array(iI64Array* v)
{
    ReleaseMemory();
    if (v != NULL)
    {
        mdType = eType_I64Array;
        mValue.pObj = v;
        v->Retain();
    }
    else
    {
        mdType = eType_Null;
        mValue.pObj = NULL;
    }
}

void Var::SetU8Array(iU8Array* v)
{
    ReleaseMemory();
    if (v != NULL)
    {
        mdType = eType_U8Array;
        mValue.pObj = v;
        v->Retain();
    }
    else
    {
        mdType = eType_Null;
        mValue.pObj = NULL;
    }
}

void Var::SetU16Array(iU16Array* v)
{
    ReleaseMemory();
    if (v != NULL)
    {
        mdType = eType_U16Array;
        mValue.pObj = v;
        v->Retain();
    }
    else
    {
        mdType = eType_Null;
        mValue.pObj = NULL;
    }
}

void Var::SetU32Array(iU32Array* v)
{
    ReleaseMemory();
    if (v != NULL)
    {
        mdType = eType_U32Array;
        mValue.pObj = v;
        v->Retain();
    }
    else
    {
        mdType = eType_Null;
        mValue.pObj = NULL;
    }
}

void Var::SetU64Array(iU64Array* v)
{
    ReleaseMemory();
    if (v != NULL)
    {
        mdType = eType_U64Array;
        mValue.pObj = v;
        v->Retain();
    }
    else
    {
        mdType = eType_Null;
        mValue.pObj = NULL;
    }
}

void Var::SetString(const string &v)
{
    ReleaseMemory();
    mdType = eType_String;
    mValue.pStr = new string(v);
}

bool Var::IsNull() const
{
    return mdType == eType_Null;
}

int Var::GetType() const
{
    return mdType;
}

bool Var::GetBoolean() const
{
    switch (mdType)
    {
        case eType_Boolean:
            return mValue.b;
        case eType_I32:
            return mValue.i32 != 0;
        case eType_I64:
            return mValue.i64 != 0;
        case eType_F32:
            return mValue.f32 != 0.0f;
        case eType_F64:
            return mValue.f64 != 0.0;
        case eType_Null:
            return false;
        default:
            return true;
    }
}

int8_t Var::GetI8(int8_t dDefault) const
{
    switch (mdType)
    {
        case eType_Boolean:
            return mValue.b;
        case eType_F32:
            return (int8_t)mValue.f32;
        case eType_F64:
            return (int8_t)mValue.f64;
        case eType_I8:
            return mValue.i8;
        case eType_I16:
            return (int8_t)mValue.i16;
        case eType_I32:
            return (int8_t)mValue.i32;
        case eType_I64:
            return (int8_t)mValue.i64;
        case eType_U8:
            return (int8_t)mValue.u8;
        case eType_U16:
            return (int8_t)mValue.u16;
        case eType_U32:
            return (int8_t)mValue.u32;
        case eType_U64:
            return (int8_t)mValue.u64;
        case eType_String:
#if defined(PI_MBS)
            return (int8_t)atoi(mValue.pStr->c_str());
#else
            return (int8_t)wcstol(mValue.pStr->c_str(), NULL, 0);
#endif
        default:
            return dDefault;
    }
}

int16_t Var::GetI16(int16_t dDefault) const
{
    switch (mdType)
    {
        case eType_Boolean:
            return mValue.b;
        case eType_F32:
            return (int16_t)mValue.f32;
        case eType_F64:
            return (int16_t)mValue.f64;
        case eType_I8:
            return mValue.i8;
        case eType_I16:
            return mValue.i16;
        case eType_I32:
            return (int16_t)mValue.i32;
        case eType_I64:
            return (int16_t)mValue.i64;
        case eType_U8:
            return (int16_t)mValue.u8;
        case eType_U16:
            return (int16_t)mValue.u16;
        case eType_U32:
            return (int16_t)mValue.u32;
        case eType_U64:
            return (int16_t)mValue.u64;
        case eType_String:
#if defined(PI_MBS)
            return (int16_t)atoi(mValue.pStr->c_str());
#else
            return (int16_t)wcstol(mValue.pStr->c_str(), NULL, 0);
#endif
        default:
            return dDefault;
    }
}

int32_t Var::GetI32(int32_t dDefault) const
{
    switch (mdType)
    {
        case eType_Boolean:
            return mValue.b;
        case eType_F32:
            return (int32_t)mValue.f32;
        case eType_F64:
            return (int32_t)mValue.f64;
        case eType_I8:
            return mValue.i8;
        case eType_I16:
            return mValue.i16;
        case eType_I32:
            return mValue.i32;
        case eType_I64:
            return (int32_t)mValue.i64;
        case eType_U8:
            return (int32_t)mValue.u8;
        case eType_U16:
            return (int32_t)mValue.u16;
        case eType_U32:
            return (int32_t)mValue.u32;
        case eType_U64:
            return (int32_t)mValue.u64;
        case eType_String:
#if defined(PI_MBS)
            return atoi(mValue.pStr->c_str());
#else
            return (int32_t)wcstol(mValue.pStr->c_str(), NULL, 0);
#endif
        default:
            return dDefault;
    }
}

int64_t Var::GetI64(int64_t lldDefault) const
{
    switch (mdType)
    {
        case eType_Boolean:
            return mValue.b;
        case eType_F32:
            return (int64_t)mValue.f32;
        case eType_F64:
            return (int64_t)mValue.f64;
        case eType_I8:
            return mValue.i8;
        case eType_I16:
            return mValue.i16;
        case eType_I32:
            return mValue.i32;
        case eType_I64:
            return mValue.i64;
        case eType_U8:
            return mValue.u8;
        case eType_U16:
            return mValue.u16;
        case eType_U32:
            return mValue.u32;
        case eType_U64:
            return (int64_t)mValue.u64;
        case eType_String:
#if defined(PI_MBS)
            return strtoll(mValue.pStr->c_str(), NULL, 10);
#else
            return wcstoll(mValue.pStr->c_str(), NULL, 10);
#endif
        default:
            return lldDefault;
    }
}

uint8_t Var::GetU8(uint8_t dDefault) const
{
    switch (mdType)
    {
        case eType_Boolean:
            return mValue.b;
        case eType_F32:
            return (uint8_t)mValue.f32;
        case eType_F64:
            return (uint8_t)mValue.f64;
        case eType_I8:
            return (uint8_t)mValue.i8;
        case eType_I16:
            return (uint8_t)mValue.i16;
        case eType_I32:
            return (uint8_t)mValue.i32;
        case eType_I64:
            return (uint8_t)mValue.i64;
        case eType_U8:
            return mValue.u8;
        case eType_U16:
            return (uint8_t)mValue.u16;
        case eType_U32:
            return (uint8_t)mValue.u32;
        case eType_U64:
            return (uint8_t)mValue.u64;
        case eType_String:
#if defined(PI_MBS)
            return (uint8_t)atoi(mValue.pStr->c_str());
#else
            return (uint8_t)wcstol(mValue.pStr->c_str(), NULL, 0);
#endif
        default:
            return dDefault;
    }
}

uint16_t Var::GetU16(uint16_t dDefault) const
{
    switch (mdType)
    {
        case eType_Boolean:
            return mValue.b;
        case eType_F32:
            return (uint16_t)mValue.f32;
        case eType_F64:
            return (uint16_t)mValue.f64;
        case eType_I8:
            return (uint16_t)mValue.i8;
        case eType_I16:
            return (uint16_t)mValue.i16;
        case eType_I32:
            return (uint16_t)mValue.i32;
        case eType_I64:
            return (uint16_t)mValue.i64;
        case eType_U8:
            return mValue.u8;
        case eType_U16:
            return mValue.u16;
        case eType_U32:
            return (uint16_t)mValue.u32;
        case eType_U64:
            return (uint16_t)mValue.u64;
        case eType_String:
#if defined(PI_MBS)
            return (uint16_t)atoi(mValue.pStr->c_str());
#else
            return (uint16_t)wcstol(mValue.pStr->c_str(), NULL, 0);
#endif
        default:
            return dDefault;
    }
}

uint32_t Var::GetU32(uint32_t dDefault) const
{
    switch (mdType)
    {
        case eType_Boolean:
            return mValue.b;
        case eType_F32:
            return (uint32_t)mValue.f32;
        case eType_F64:
            return (uint32_t)mValue.f64;
        case eType_I8:
            return (uint32_t)mValue.i8;
        case eType_I16:
            return (uint32_t)mValue.i16;
        case eType_I32:
            return (uint32_t)mValue.i32;
        case eType_I64:
            return (uint32_t)mValue.i64;
        case eType_U8:
            return mValue.u8;
        case eType_U16:
            return mValue.u16;
        case eType_U32:
            return mValue.u32;
        case eType_U64:
            return (uint32_t)mValue.u64;
        case eType_String:
#if defined(PI_MBS)
            return (uint32_t)atoi(mValue.pStr->c_str());
#else
            return (uint32_t)wcstol(mValue.pStr->c_str(), NULL, 0);
#endif
        default:
            return dDefault;
    }
}

uint64_t Var::GetU64(uint64_t lldDefault) const
{
    switch (mdType)
    {
        case eType_Boolean:
            return mValue.b;
        case eType_F32:
            return (uint64_t)mValue.f32;
        case eType_F64:
            return (uint64_t)mValue.f64;
        case eType_I8:
            return (uint64_t)mValue.i8;
        case eType_I16:
            return (uint64_t)mValue.i16;
        case eType_I32:
            return (uint64_t)mValue.i32;
        case eType_I64:
            return (uint64_t)mValue.i64;
        case eType_U8:
            return mValue.u8;
        case eType_U16:
            return mValue.u16;
        case eType_U32:
            return mValue.u32;
        case eType_U64:
            return mValue.u64;
        case eType_String:
#if defined(PI_MBS)
            return (uint64_t)strtoll(mValue.pStr->c_str(), NULL, 10);
#else
            return (uint64_t)wcstoll(mValue.pStr->c_str(), NULL, 10);
#endif
        default:
            return lldDefault;
    }
}

float Var::GetF32(float fDefault) const
{
    switch (mdType)
    {
        case eType_Boolean:
            return mValue.b;
        case eType_F32:
            return mValue.f32;
        case eType_F64:
            return (float)mValue.f64;
        case eType_I8:
            return (float)mValue.i8;
        case eType_I16:
            return (float)mValue.i16;
        case eType_I32:
            return (float)mValue.i32;
        case eType_I64:
            return (float)mValue.i64;
        case eType_U8:
            return (float)mValue.u8;
        case eType_U16:
            return (float)mValue.u16;
        case eType_U32:
            return (float)mValue.u32;
        case eType_U64:
            return (float)mValue.u64;
        case eType_String:
#if defined(PI_MBS)
            return strtof(mValue.pStr->c_str(), NULL);
#else
            return (float)wcstod(mValue.pStr->c_str(), NULL);
#endif
        default:
            return fDefault;
    }
}

double Var::GetF64(double dbDefault) const
{
    switch (mdType)
    {
        case eType_Boolean:
            return mValue.b;
        case eType_F32:
            return mValue.f32;
        case eType_F64:
            return mValue.f64;
        case eType_I8:
            return (double)mValue.i8;
        case eType_I16:
            return (double)mValue.i16;
        case eType_I32:
            return (double)mValue.i32;
        case eType_I64:
            return (double)mValue.i64;
        case eType_U8:
            return (double)mValue.u8;
        case eType_U16:
            return (double)mValue.u16;
        case eType_U32:
            return (double)mValue.u32;
        case eType_U64:
            return (double)mValue.u64;
        case eType_String:
#if defined(PI_MBS)
            return strtod(mValue.pStr->c_str(), NULL);
#else
            return wcstod(mValue.pStr->c_str(), NULL);
#endif
        default:
            return dbDefault;
    }
}

iRefObject* Var::GetObject() const
{
    switch (mdType)
    {
        case eType_Object:
            return mValue.pObj;
        default:
            return nullptr;
    }
}

iI8Array* Var::GetI8Array() const
{
    switch (mdType)
    {
        case eType_I8Array:
            return mValue.pI8Array;
        default:
            return nullptr;
    }
}

iI16Array* Var::GetI16Array() const
{
    switch (mdType)
    {
        case eType_I16Array:
            return mValue.pI16Array;
        default:
            return nullptr;
    }
}

iI32Array* Var::GetI32Array() const
{
    switch (mdType)
    {
        case eType_I32Array:
            return mValue.pI32Array;
        default:
            return nullptr;
    }
}

iI64Array* Var::GetI64Array() const
{
    switch (mdType)
    {
        case eType_I64Array:
            return mValue.pI64Array;
        default:
            return nullptr;
    }
}

iU8Array* Var::GetU8Array() const
{
    switch (mdType)
    {
        case eType_U8Array:
            return mValue.pU8Array;
        default:
            return nullptr;
    }
}

iU16Array* Var::GetU16Array() const
{
    switch (mdType)
    {
        case eType_U16Array:
            return mValue.pU16Array;
        default:
            return nullptr;
    }
}

iU32Array* Var::GetU32Array() const
{
    switch (mdType)
    {
        case eType_U32Array:
            return mValue.pU32Array;
        default:
            return nullptr;
    }
}

iU64Array* Var::GetU64Array() const
{
    switch (mdType)
    {
        case eType_U64Array:
            return mValue.pU64Array;
        default:
            return nullptr;
    }
}

iF32Array* Var::GetF32Array() const
{
    switch (mdType)
    {
        case eType_F32Array:
            return mValue.pF32Array;
        default:
            return nullptr;
    }
}

iF64Array* Var::GetF64Array() const
{
    switch (mdType)
    {
        case eType_F64Array:
            return mValue.pF64Array;
        default:
            return nullptr;
    }
}

iRectArray* Var::GetRectArray() const
{
    switch (mdType)
    {
        case eType_RectArray:
            return mValue.pRectArray;
        default:
            return nullptr;
    }
}

iQuatArray* Var::GetQuatArray() const
{
    switch (mdType)
    {
        case eType_QuatArray:
            return mValue.pQuatArray;
        default:
            return nullptr;
    }
}

iMat4Array* Var::GetMat4Array() const
{
    switch (mdType)
    {
        case eType_Mat4Array:
            return mValue.pMat4Array;
        default:
            return nullptr;
    }
}

iVec2Array* Var::GetVec2Array() const
{
    switch (mdType)
    {
        case eType_Vec2Array:
            return mValue.pVec2Array;
        default:
            return nullptr;
    }
}

iVec3Array* Var::GetVec3Array() const
{
    switch (mdType)
    {
        case eType_Vec3Array:
            return mValue.pVec3Array;
        default:
            return nullptr;
    }
}

iVec4Array* Var::GetVec4Array() const
{
    switch (mdType)
    {
        case eType_Vec4Array:
            return mValue.pVec4Array;
        default:
            return nullptr;
    }
}

string Var::GetString(const string &def) const
{
    switch (mdType)
    {
        case eType_F32:
        {
            return piFormat(TT("%f"), mValue.f32);
        }
        case eType_F64:
        {
            return piFormat(TT("%f"), mValue.f64);
        }
        case eType_I8:
        {
            return piFormat(TT("%d"), mValue.i8);
        }
        case eType_I16:
        {
            return piFormat(TT("%d"), mValue.i16);
        }
        case eType_I32:
        {
            return piFormat(TT("%d"), mValue.i32);
        }
        case eType_I64:
        {
            return piFormat(TT("%lld"), mValue.i64);
        }
        case eType_U8:
        {
            return piFormat(TT("%u"), mValue.u8);
        }
        case eType_U16:
        {
            return piFormat(TT("%u"), mValue.u16);
        }
        case eType_U32:
        {
            return piFormat(TT("%u"), mValue.u32);
        }
        case eType_U64:
        {
            return piFormat(TT("%llu"), mValue.u64);
        }
        case eType_Vec2:
            return piToString(*mValue.pVec2);
        case eType_Vec3:
            return piToString(*mValue.pVec3);
        case eType_Vec4:
            return piToString(*mValue.pVec4);
        case eType_Mat4:
            return piToString(*mValue.pMat4);
        case eType_Quat:
            return piToString(*mValue.pQuat);
        case eType_Rect:
            return piToString(*mValue.pRect);
        case eType_String:
        {
            if (!mValue.pStr->empty())
            {
                return string(*mValue.pStr);
            }
            else
            {
                return def;
            }
        }
        case eType_Object:
            return mValue.pObj->ToString();
        case eType_I8Array:
            return mValue.pI8Array->ToString();
        case eType_I16Array:
            return mValue.pI16Array->ToString();
        case eType_I32Array:
            return mValue.pI32Array->ToString();
        case eType_I64Array:
            return mValue.pI64Array->ToString();
        case eType_U8Array:
            return mValue.pU8Array->ToString();
        case eType_U16Array:
            return mValue.pU16Array->ToString();
        case eType_U32Array:
            return mValue.pU32Array->ToString();
        case eType_U64Array:
            return mValue.pU64Array->ToString();
        case eType_F32Array:
            return mValue.pF32Array->ToString();
        case eType_F64Array:
            return mValue.pF64Array->ToString();
        case eType_Vec2Array:
            return mValue.pVec2Array->ToString();
        case eType_Vec3Array:
            return mValue.pVec3Array->ToString();
        case eType_Vec4Array:
            return mValue.pVec4Array->ToString();
        case eType_RectArray:
            return mValue.pRectArray->ToString();
        case eType_Mat4Array:
            return mValue.pMat4Array->ToString();
        case eType_QuatArray:
            return mValue.pQuatArray->ToString();
        default:
            return def;
    }
}


vec2 Var::GetVec2(const vec2 &def) const
{
    if (mdType == eType_Vec2)
    {
        return *mValue.pVec2;
    }
    else
    {
        return def;
    }
}

void Var::SetVec2(const vec2 &v)
{
    ReleaseMemory();
    mdType = eType_Vec2;
    mValue.pVec2 = new vec2(v);
}

vec3 Var::GetVec3(const vec3 &def) const
{
    if (mdType == eType_Vec3)
    {
        return *mValue.pVec3;
    }
    else
    {
        return def;
    }
}

void Var::SetVec3(const vec3 &v)
{
    ReleaseMemory();
    mdType = eType_Vec3;
    mValue.pVec3 = new vec3(v);
}

vec4 Var::GetVec4(const vec4 &def) const
{
    if (mdType == eType_Vec4)
    {
        return *mValue.pVec4;
    }
    else
    {
        return def;
    }
}


void Var::SetVec4(const vec4 &v)
{
    ReleaseMemory();
    mdType = eType_Vec4;
    mValue.pVec4 = new vec4(v);
}

mat4 Var::GetMat4(const mat4 &def) const
{
    if (mdType == eType_Mat4)
    {
        return *mValue.pMat4;
    }
    else
    {
        return def;
    }
}

void Var::SetMat4(const mat4 &v)
{
    ReleaseMemory();
    mdType = eType_Mat4;
    mValue.pMat4 = new mat4(v);
}

quat Var::GetQuat(const quat &def) const
{
    if (mdType == eType_Quat)
    {
        return *mValue.pQuat;
    }
    else
    {
        return def;
    }
}

void Var::SetQuat(const quat &v)
{
    ReleaseMemory();
    mdType = eType_Quat;
    mValue.pQuat = new quat(v);
}

rect Var::GetRect(const rect& def) const
{
    if (mdType == eType_Rect)
    {
        return *mValue.pRect;
    }
    else
    {
        return def;
    }
}

void Var::SetRect(const rect &v)
{
    ReleaseMemory();
    mdType = eType_Rect;
    mValue.pRect = new rect(v);
}

Var::operator bool() const
{
    return GetBoolean();
}

Var::operator int8_t() const
{
    return GetI8();
}

Var::operator uint8_t() const
{
    return GetU8();
}

Var::operator int16_t() const
{
    return GetI16();
}

Var::operator uint16_t() const
{
    return GetU16();
}

Var::operator int32_t() const
{
    return GetI32();
}

Var::operator uint32_t() const
{
    return GetU32();
}

Var::operator int64_t() const
{
    return GetI64();
}

Var::operator uint64_t() const
{
    return GetU64();
}

Var::operator float() const
{
    return GetF32();
}

Var::operator double() const
{
    return GetF64();
}

Var::operator string() const
{
    return GetString();
}

Var::operator vec2() const
{
    return GetVec2();
}

Var::operator vec3() const
{
    return GetVec3();
}

Var::operator vec4() const
{
    return GetVec4();
}

Var::operator mat4() const
{
    return GetMat4();
}

Var::operator quat() const
{
    return GetQuat();
}

Var::operator rect() const
{
    return GetRect();
}

Var::operator iRefObject*() const
{
    return GetObject();
}

Var::operator iI8Array*() const
{
    return GetI8Array();
}

Var::operator iI16Array*() const
{
    return GetI16Array();
}

Var::operator iI32Array*() const
{
    return GetI32Array();
}

Var::operator iI64Array*() const
{
    return GetI64Array();
}

Var::operator iU8Array*() const
{
    return GetU8Array();
}

Var::operator iU16Array*() const
{
    return GetU16Array();
}

Var::operator iU32Array*() const
{
    return GetU32Array();
}

Var::operator iU64Array*() const
{
    return GetU64Array();
}

Var::operator iF32Array*() const
{
    return GetF32Array();
}

Var::operator iF64Array*()const
{
    return GetF64Array();
}

Var::operator iRectArray*() const
{
    return GetRectArray();
}

Var::operator iQuatArray*() const
{
    return GetQuatArray();
}

Var::operator iMat4Array*() const
{
    return GetMat4Array();
}

Var::operator iVec2Array*() const
{
    return GetVec2Array();
}

Var::operator iVec3Array*() const
{
    return GetVec3Array();
}

Var::operator iVec4Array*() const
{
    return GetVec4Array();
}

string piTypeName(int type)
{
    switch (type) {
        case eType_Null:
            return "Null";
        case eType_Boolean:
            return "Boolean";
        case eType_I8:
            return "I8";
        case eType_U8:
            return "U8";
        case eType_I16:
            return "I16";
        case eType_U16:
            return "U16";
        case eType_I32:
            return "I32";
        case eType_U32:
            return "U32";
        case eType_I64:
            return "I64";
        case eType_U64:
            return "U64";
        case eType_F32:
            return "F32";
        case eType_F64:
            return "F64";
        case eType_Object:
            return "Object";
        case eType_String:
            return "string";
        case eType_Rect:
            return "rect";
        case eType_Vec2:
            return "vec2";
        case eType_Vec3:
            return "vec3";
        case eType_Vec4:
            return "vec4";
        case eType_Mat4:
            return "mat4";
        case eType_Enum:
            return "Enum";
        case eType_Quat:
            return "quat";
        case eType_Var:
            return "Var";
        case eType_Void:
            return "void";
        case eType_I8Array:
            return "I8Array";
        case eType_I16Array:
            return "I16Array";
        case eType_I32Array:
            return "I32Array";
        case eType_I64Array:
            return "I64Array";
        case eType_U8Array:
            return "U8Array";
        case eType_U16Array:
            return "U16Array";
        case eType_U32Array:
            return "U32Array";
        case eType_U64Array:
            return "U64Array";
        case eType_F32Array:
            return "F32Array";
        case eType_F64Array:
            return "F64Array";
        case eType_RectArray:
            return "RectArray";
        case eType_QuatArray:
            return "QuatArray";
        case eType_Mat4Array:
            return "Mat4Array";
        default:
            return "eType_Unknown";
    }
}

class VarArray : public StructArrayImpl<Var, iVarArray>
{
public:
    VarArray()
    {
    }
    
    VarArray(const Var* values, int32_t count):
    StructArrayImpl(values, count)
    {
    }
    
    virtual ~VarArray()
    {
    }
    
    virtual string ToString() const
    {
        return piFormat("VarArray[%d]", GetCount());
    }
};


iVarArray* CreateVarArray()
{
    return new VarArray();
}


iI8Array* VarArrayToI8Array(iVarArray* array)
{
    SmartPtr<iI8Array> ret = CreateI8Array();
    
    for (auto i = 0; i < array->GetCount(); ++i)
    {
        auto v = array->GetItem(i);
        ret->PushBack(v);
    }
    
    return ret.PtrAndSetNull();
}

iU8Array* VarArrayToU8Array(iVarArray* array)
{
    SmartPtr<iU8Array> ret = CreateU8Array();
    
    for (auto i = 0; i < array->GetCount(); ++i)
    {
        auto v = array->GetItem(i);
        ret->PushBack(v);
    }
    
    return ret.PtrAndSetNull();
}

iI16Array* VarArrayToI16Array(iVarArray* array)
{
    SmartPtr<iI16Array> ret = CreateI16Array();
    
    for (auto i = 0; i < array->GetCount(); ++i)
    {
        auto v = array->GetItem(i);
        ret->PushBack(v);
    }
    
    return ret.PtrAndSetNull();
}

iU16Array* VarArrayToU16Array(iVarArray* array)
{
    SmartPtr<iU16Array> ret = CreateU16Array();
    
    for (auto i = 0; i < array->GetCount(); ++i)
    {
        auto v = array->GetItem(i);
        ret->PushBack(v);
    }
    
    return ret.PtrAndSetNull();
}

iI32Array* VarArrayToI32Array(iVarArray* array)
{
    SmartPtr<iI32Array> ret = CreateI32Array();
    
    for (auto i = 0; i < array->GetCount(); ++i)
    {
        auto v = array->GetItem(i);
        ret->PushBack(v);
    }
    
    return ret.PtrAndSetNull();
}


iU32Array* VarArrayToU32Array(iVarArray* array)
{
    SmartPtr<iU32Array> ret = CreateU32Array();
    
    for (auto i = 0; i < array->GetCount(); ++i)
    {
        auto v = array->GetItem(i);
        ret->PushBack(v);
    }
    
    return ret.PtrAndSetNull();
}


iI64Array* VarArrayToI64Array(iVarArray* array)
{
    SmartPtr<iI64Array> ret = CreateI64Array();
    
    for (auto i = 0; i < array->GetCount(); ++i)
    {
        auto v = array->GetItem(i);
        ret->PushBack(v);
    }
    
    return ret.PtrAndSetNull();
}


iU64Array* VarArrayToU64Array(iVarArray* array)
{
    SmartPtr<iU64Array> ret = CreateU64Array();
    
    for (auto i = 0; i < array->GetCount(); ++i)
    {
        auto v = array->GetItem(i);
        ret->PushBack(v);
    }
    
    return ret.PtrAndSetNull();
}


iF32Array* VarArrayToF32Array(iVarArray* array)
{
    SmartPtr<iF32Array> ret = CreateF32Array();
    
    for (auto i = 0; i < array->GetCount(); ++i)
    {
        auto v = array->GetItem(i);
        ret->PushBack(v);
    }
    
    return ret.PtrAndSetNull();
}


iF64Array* VarArrayToF64Array(iVarArray* array)
{
    SmartPtr<iF64Array> ret = CreateF64Array();
    
    for (auto i = 0; i < array->GetCount(); ++i)
    {
        auto v = array->GetItem(i);
        ret->PushBack(v);
    }
    
    return ret.PtrAndSetNull();
}










NSPI_END()













