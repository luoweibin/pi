/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2012.9.12   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>
#include <mutex>
#include <condition_variable>

using namespace std;

NSPI_BEGIN()

class cMessage : public iMessage
{
private:
    int32_t mId;
    Var mArg1;
    Var mArg2;
    SmartPtr<iRefObject> mptrSender;

public:
    cMessage(int32_t messageId, const Var& arg1, const Var& arg2):
    mId(messageId), mArg1(arg1), mArg2(arg2)
    {
    }
    
    virtual ~cMessage()
    {
        mptrSender = NULL;
    }
    
    virtual int32_t GetID() const
    {
        return mId;
    }
    
    virtual Var GetArg1() const
    {
        return mArg1;
    }
    
    virtual Var GetArg2() const
    {
        return mArg2;
    }
    
    virtual iRefObject* GetSender() const
    {
        return mptrSender;
    }
    
    virtual void SetSender(iRefObject* pSender)
    {
        mptrSender = pSender;
    }
    
    virtual iMessage* Clone() const
    {
        return new cMessage(mId, mArg1, mArg2);
    }
    
    virtual string ToString() const
    {
        return piFormat("iMessage {id:%d}", mId);
    }
};

iMessage* CreateMessage(uint32_t uMessageId, const Var& arg1, const Var& arg2)
{
    return new cMessage(uMessageId, arg1, arg2);
}

class cMessageQueue : public iMessageQueue
{
private:
    typedef list<SmartPtr<iMessage>> MessageQueue;
    MessageQueue mMessageQueue;
   
    mutable mutex mMutex;
    mutable condition_variable mCond;
    
public:
    cMessageQueue()
    {
    }
    
    virtual ~cMessageQueue()
    {
        Clear();
    }
    
    virtual bool Empty() const
    {
        lock_guard<mutex> lock(mMutex);
        return mMessageQueue.empty();
    }
    
    virtual void SendMessage(iRefObject* pSender,
                             int32_t message,
                             const Var& arg1 = Var(),
                             const Var& arg2 = Var())
    {
    }
    
    virtual void PostMessage(iRefObject* pSender,
                             int32_t message,
                             const Var& arg1 = Var(),
                             const Var& arg2 = Var())
    {
        unique_lock<mutex> lock(mMutex);
        SmartPtr<iMessage> ptrMsg = CreateMessage(message, arg1, arg2);
        ptrMsg->SetSender(pSender);
        mMessageQueue.push_back(ptrMsg);
        mCond.notify_one();
    }
    
    virtual iMessage* PopMessage()
    {
        lock_guard<mutex> lock(mMutex);
        if (!mMessageQueue.empty())
        {
            SmartPtr<iMessage> ptrMsg = mMessageQueue.front();
            mMessageQueue.pop_front();
            return ptrMsg.PtrAndSetNull();
        }
        else
        {
            return NULL;
        }
    }
    
    virtual iMessage* PeekMessage()
    {
        lock_guard<mutex> lock(mMutex);
        if (!mMessageQueue.empty())
        {
            SmartPtr<iMessage> ptrMsg = mMessageQueue.front();
            return ptrMsg.PtrAndSetNull();
        }
        else
        {
            return NULL;
        }
    }
    
    virtual iMessage *WaitForMessage()
    {
        unique_lock<mutex> lock(mMutex);
        if (!mMessageQueue.empty())
        {
            SmartPtr<iMessage> ptrMsg = mMessageQueue.front();
            mMessageQueue.pop_front();
            return ptrMsg.PtrAndSetNull();
        }
        mCond.wait(lock);
        
        SmartPtr<iMessage> ptrMsg = mMessageQueue.front();
        mMessageQueue.pop_front();
        return ptrMsg.PtrAndSetNull();
    }
    
    virtual void Clear()
    {
        lock_guard<mutex> lock(mMutex);
        mMessageQueue.clear();
    }
    
    virtual int32_t GetMessageCount() const
    {
        lock_guard<mutex> lock(mMutex);
        return (int32_t)mMessageQueue.size();
    }
};

iMessageQueue* CreateMessageQueue()
{
    return new cMessageQueue();
}



NSPI_END()






























































