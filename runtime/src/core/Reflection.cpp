/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.5   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>
#include <mutex>

using namespace std;

NSPI_BEGIN()

class ClassLoader : public iClassLoader
{
private:
    typedef map<string, SmartPtr<iClass>> ClassMap;
    ClassMap mClassMap;
    
    typedef map<string, SmartPtr<iEnum>> EnumMap;
    EnumMap mEnumMap;
    
    mutable mutex mMutex;
    iClassLoader* mParent;
    
public:
    ClassLoader():
    mParent(nullptr)
    {
    }
    
    virtual ~ClassLoader()
    {
    }
    
    virtual void SetParent(iClassLoader* loader)
    {
        mParent = loader;
    }
    
    virtual iClassLoader* GetParent() const
    {
        return mParent;
    }
    
    virtual iClass* FindClass(const std::string& name) const
    {
        piAssert(!name.empty(), nullptr);
        
        lock_guard<mutex> lock(mMutex);

        ClassMap::const_iterator it = mClassMap.find(name);
        if (it != mClassMap.end())
        {
            return it->second;
        }
        
        return mParent ? mParent->FindClass(name) : nullptr;
    }
    
    virtual void RegisterClass(const string& name, iClass* klass)
    {
        piAssert(klass != nullptr, ;);
        
        piAssert(!name.empty(), ;);
        
        lock_guard<mutex> lock(mMutex);

        ClassMap::iterator it = mClassMap.find(name);
        piAssert(it == mClassMap.end() || it->second == klass, ;);
        
        mClassMap[name] = klass;
    }
    
    virtual void UnregisterClass(iClass* klass)
    {
        piAssert(klass != nullptr, ;);
        
        lock_guard<mutex> lock(mMutex);
        
        auto it = mClassMap.begin();
        while (it != mClassMap.end())
        {
            if (it->second == klass)
            {
                it = mClassMap.erase(it);
            }
            else
            {
                ++it;
            }
        }
    }
    
    virtual iEnum* FindEnum(const string& name) const
    {
        piAssert(!name.empty(), nullptr);
        
        lock_guard<mutex> lock(mMutex);
        EnumMap::const_iterator it = mEnumMap.find(name);
        
        if (it != mEnumMap.end())
        {
            return it->second;
        }
        
        return mParent ? mParent->FindEnum(name) : nullptr;
    }
    
    virtual void RegisterEnum(const string& name, iEnum* enumClass)
    {
        piAssert(!name.empty(), ;);
        piAssert(enumClass != nullptr, ;);
        
        lock_guard<mutex> lock(mMutex);
        EnumMap::iterator it = mEnumMap.find(name);
        piAssert(it == mEnumMap.end(), ;);
        
        mEnumMap[name] = enumClass;
    }
    
    virtual void UnregisterEnum(const string& name)
    {
        piAssert(!name.empty(), ;);
        
        lock_guard<mutex> lock(mMutex);
        mEnumMap.erase(name);
    }
};

iClassLoader* piGetRootClassLoader()
{
    static SmartPtr<iClassLoader> g_RootClassLoader;
    if (g_RootClassLoader.IsNull())
    {
        g_RootClassLoader = new ClassLoader();
    }
    return g_RootClassLoader;
}


NSPI_END()
























