/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2015.3.27   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>

#define PI_VERSION 163

NSPI_BEGIN()

using namespace std;

void piInitLog();
void piDeinitLog();
void piInitMedia();

void piInit()
{
    piInitLog();
    piInitMedia();
}

void piDeinit()
{
    piDeinitLog();
}


int32_t piGetVersionCode()
{
    return PI_VERSION;
}


string piGetVersionString()
{
#if PI_DEBUG
    return piFormat("%d DEBUG", PI_VERSION);
#else
    return piFormat("%d RELEASE", PI_VERSION);
#endif
}


int piGetSystemCode()
{
#if defined(PI_MACOS)
    return eOS_macOS;
#elif defined(PI_IOS)
    return eOS_iOS;
#elif defined(PI_ANDROID)
    return eOS_Android;
#elif defined(PI_WINDOWS)
    return eOS_Windows;
#else
    return eOS_Unknown;
#endif
}

NSPI_END()

















