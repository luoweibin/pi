/*******************************************************************************
 See copyright notice in LICENSE.

 
 History:
 luo weibin     2015.1.12   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>

#include <mutex>
#include <condition_variable>
#include <chrono>

NSPI_BEGIN()

using namespace std;
using namespace chrono;

class Event : public iEvent
{
private:
    int32_t mSignals;
    int32_t mCurSignals;
    condition_variable mCond;
    mutable mutex mMutex;
    unique_lock<mutex> mLock;
    
public:
    Event(int32_t count):
    mSignals(count), mCurSignals(count)
    {
    }
    
    virtual ~Event()
    {
    }
    
    virtual void Wait()
    {
        mCond.wait(mLock, [this](){return mCurSignals <= 0;});
        piAssert(mLock.owns_lock(), ;);
    }
    
    virtual void Lock()
    {
        unique_lock<mutex> lock(mMutex);
        mLock.swap(lock);
        piAssert(mLock.owns_lock(), ;);
    }
    
    virtual void Unlock()
    {
        mLock.unlock();
        mLock.release();
    }
    
    virtual bool TimedWait(int64_t ms)
    {
        if (ms < 0)
        {
            Wait();
            return true;
        }
        
        return mCond.wait_for(mLock, milliseconds(ms), [this](){return mCurSignals <= 0;});
    }
    
    virtual void Fire()
    {
        unique_lock<mutex> lock(mMutex);
        if (mCurSignals > 0)
        {
            --mCurSignals;
            mCond.notify_one();
        }
    }
    
    virtual void FireAll()
    {
        unique_lock<mutex> lock(mMutex);
        if (mCurSignals > 0)
        {
            --mCurSignals;
            mCond.notify_all();
        }
    }
    
    virtual void Trigger()
    {
        unique_lock<mutex> lock(mMutex);
        if (mCurSignals > 0)
        {
            mCurSignals = 0;
            mCond.notify_one();
        }
    }
    
    virtual void TriggerAll()
    {
        unique_lock<mutex> lock(mMutex);
        if (mCurSignals > 0)
        {
            mCurSignals = 0;
            mCond.notify_all();
        }
    }
    
    virtual int32_t GetSignals() const
    {
        unique_lock<mutex> lock(mMutex);
        return mCurSignals;
    }
    
    virtual void Reset()
    {
        mCurSignals = mSignals;
    }
    
private:
    bool pred()
    {
        return mCurSignals <= 0;
    }
};


iEvent* CreateEvent(int32_t signals)
{
    return new Event(signals);
}




class Future : public iFuture
{
private:
    bool mIsValid;
    Var mValue;
    SmartPtr<iEvent> mEvent;
    
public:
    Future():
    mIsValid(false)
    {
        mEvent = CreateEvent(1);
    }
    
    virtual ~Future()
    {
    }
    
    virtual void Reset()
    {
        mEvent->Lock();
        
        mIsValid = false;
        mValue = Var();
        
        mEvent->Reset();
        mEvent->Unlock();
    }
    
    virtual void Wait()
    {
        mEvent->Lock();
        mEvent->Wait();
        mEvent->Unlock();
    }
    
    virtual bool TimedWait(int64_t ms)
    {
        mEvent->Lock();
        bool ret = mEvent->TimedWait(ms);
        mEvent->Unlock();
        return ret;
    }
    
    virtual void SetValue(const Var& value)
    {
        mEvent->Lock();
        mIsValid = true;
        mValue = value;
        mEvent->FireAll();
        mEvent->Unlock();
    }
    
    virtual bool IsValid() const
    {
        mEvent->Lock();
        bool ret = mIsValid;
        mEvent->Unlock();
        return ret;
    }
    
    virtual Var GetValue() const
    {
        mEvent->Lock();
        Var ret = mValue;
        mEvent->Unlock();
        return ret;
    }
};


iFuture* CreateFuture()
{
    return new Future();
}



NSPI_END()

























