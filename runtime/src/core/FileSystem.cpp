/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2012.9.1   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>

#include <stdio.h>
#include <string>

#if defined (PI_UNIX)
#   include <sys/stat.h>
#   include <sys/types.h>
#   include <unistd.h>
#   include <fcntl.h>
#   include <dirent.h>
#   include <sys/param.h>
#   include <sys/mount.h>
#endif

using namespace std;

NSPI_BEGIN()

//==============================================================================

class FileStream : public cStreamImpl<iStream>
{
private:
    FILE *mFile;
    
public:
    FileStream():cStreamImpl<iStream>()
    {
    }
    
    virtual ~FileStream()
    {
        if (mFile != nullptr)
        {
            fclose(mFile);
            mFile = nullptr;
        }
    }
    
    virtual string GetUri() const
    {
        return mUri;
    }
    
    bool Init(const std::string &path, const std::string &mode)
    {
        mFile = fopen(path.c_str(), mode.c_str());
        return mFile != nullptr;
    }
    
    virtual int64_t GetSize() const
    {
        int64_t old = ftell(mFile);
        fseek(mFile, 0, SEEK_END);
        int64_t size = ftell(mFile);
        fseek(mFile, (size_t)old, SEEK_SET);
        return size;
    }
    
    virtual int64_t Seek(int64_t offset, int dSeek)
    {
        switch (dSeek)
        {
            case eSeek_Set:
                fseek(mFile, (size_t)offset, SEEK_SET);
                break;
            case eSeek_Current:
                fseek(mFile, (size_t)offset, SEEK_CUR);
                break;
            case eSeek_End:
                fseek(mFile, (size_t)offset, SEEK_END);
                break;
            default:
                return -1;
        }
        
        return ftell(mFile);
    }
    
    virtual int64_t GetOffset() const
    {
        return ftell(mFile);
    }
    
    virtual int64_t Read(void* pBuffer, int64_t luSize)
    {
        return fread(pBuffer, 1, (size_t)luSize, mFile);
    }
    
    virtual int64_t Write(const void* pData, int64_t luSize)
    {
        return fwrite(pData, 1, (size_t)luSize, mFile);
    }
    
    virtual bool End() const
    {
        return GetOffset() >= GetSize();
    }
    
    virtual bool Flush()
    {
        return fflush(mFile);
    }
};

iStream* CreateFileStream(const std::string &path, const std::string &mode)
{
    SmartPtr<FileStream> stream = new FileStream();
    if (stream->Init(path, mode))
    {
        return stream.PtrAndSetNull();
    }
    else
    {
        return NULL;
    }
}

//==============================================================================

bool piMoveFile(const std::string &originPath, const std::string &newPath)
{
    piAssert(!originPath.empty(), false);
    piAssert(!newPath.empty(), false);
    
#if defined(PI_POSIX)
    if (rename(originPath.c_str(), newPath.c_str()) == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
#else
    piSetErrno(ENOIMPL);
    return false;
#endif
}

bool piDeleteFile(const std::string &path)
{
    piAssert(!path.empty(), false);
    
#if defined(PI_UNIX)
    
    if (remove(path.c_str()) == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
    
#elif defined(PI_WINDOWS)
    
    if (::DeleteFile(path.c_str()))
    {
        return true;
    }
    else
    {
        return false;
    }
    
#else
    
    return false;
    
#endif
}

bool CreateDirectory(const std::string &path, bool recursively)
{
    piAssert(!path.empty(), false);
    
    piCheck(!piDirectoryExist(path.c_str()), true);
    
    std::string strAbsPath = piAbsPath(path.c_str());
    
    std::string strParent = piGetDirectory(strAbsPath.c_str());
    if (!piDirectoryExist(strParent.c_str()))
    {
        piCheck(recursively, false);
        piAssert(CreateDirectory(strParent.c_str(), recursively), false);
    }
    
#if defined (PI_UNIX)
    piCheck(mkdir(path.c_str(), 0755) == 0, false);
    return true;
#elif defined(PI_WINDOWS)
	piCheck(::CreateDirectory(path.c_str(), NULL), false);
	return true;
#else
    return true;
#endif
}

bool piDeleteDirectory(const std::string &path, bool recursively)
{
    piAssert(!path.empty(), false);
    piCheck(piDirectoryExist(path), true);
    
    SmartPtr<iFileFinder> ptrFinder = CreateFileFinder();
    ptrFinder->Search(path, eFileFinderFlag_Directory);
    
    while (!ptrFinder->End())
    {
        std::string strPath(path);
        strPath = strPath + PI_PATHDEL;
        strPath = strPath + ptrFinder->Name();
        
        if (ptrFinder->IsDirectory())
        {
            if (recursively && !piDeleteDirectory(strPath, recursively))
            {
                return false;
            }
        }
        else
        {
            //piPrintConsole(LOG_DEBUG, TT("delete file:%s"), strPath.c_str());
            if (!piDeleteFile(strPath))
            {
                return false;
            }
        }
        
        ptrFinder->MoveNext();
    }
    
#if defined(PI_UNIX)
    if (rmdir(path.c_str()) != 0)
    {
        return false;
    }
#else
    return false;
#endif
    
    return true;
}

std::string piGetDirectory(const std::string &path)
{
    piAssert(!path.empty(), std::string());
    
    std::string tmpPath(path);
    if (tmpPath.back() == PI_PATHDEL)
    {
        tmpPath.pop_back();
    }
    
    std::string::size_type pos = tmpPath.find_last_of(PI_PATHDEL);
    if (pos != std::string::npos)
    {
        return tmpPath.substr(0, pos + 1);
    }
    else
    {
        return std::string();
    }
}

std::string piGetBaseName(const std::string &path)
{
    piCheck(!path.empty(), std::string());
    
    std::string strPath(path);
    std::string::size_type pos = strPath.find_last_of(PI_PATHDEL);
    if (pos != std::string::npos)
    {
        return strPath.substr(pos + 1, strPath.size() - (pos + 1));
    }
    else
    {
        return path;
    }
}

std::string piGetExtendName(const std::string &path)
{
    piCheck(!path.empty(), std::string());
    
    const char_t *end = path.c_str() + path.size();
    const char_t *current = end;
    
    do
    {
        current = StringInterface<char_t>::Move(current, path.c_str() - 1, -1);
        char_t ch = StringInterface<char_t>::Decode(current);
        switch (ch)
        {
            case '.':
                return std::string(StringInterface<char_t>::Move(current, end, 1),
                              (int32_t)(end - current - 1));
            case PI_PATHDEL:
                return std::string();
            default:
                break;
        }
    }
    while (current >= path.c_str());
    
    return std::string();
}

bool piFileExist(const std::string &path)
{
    piCheck(!path.empty(), false);
    
#if defined(PI_WINDOWS)
	DWORD dwAttr = GetFileAttributes(path.c_str());
	piCheck((dwAttr != INVALID_FILE_ATTRIBUTES
            && piFlagIsNot(dwAttr, FILE_ATTRIBUTE_DIRECTORY)), false);
    return true;
#elif defined(PI_POSIX)
	struct stat stat;
	piCheck(lstat(path.c_str(), &stat) == 0, false);
	return S_ISREG(stat.st_mode);
#else
    return false;
#endif
}

int64_t piGetFileSize(const std::string &path)
{
    piAssert(!path.empty(), -1);
    
#if defined(PI_UNIX)
    struct stat state;
    if (stat(path.c_str(), &state) == 0)
    {
        return state.st_size;
    }
    else
    {
        return -1;
    }
#else
    return -1;
#endif
}

bool piDirectoryExist(const std::string &path)
{
    piAssert(!path.empty(), false);
    
#if defined (PI_POSIX)
    struct stat stat;
    piCheck(lstat(path.c_str(), &stat) == 0, false);
    return S_ISDIR(stat.st_mode);
#elif defined(PI_WINDOWS)
	DWORD dwAttr = GetFileAttributes(pszPath);
	piCheck((dwAttr != INVALID_FILE_ATTRIBUTES
             && piFlagIs(dwAttr, FILE_ATTRIBUTE_DIRECTORY)), false);
    return true;
#else
    return false;
#endif
}

std::string piGetWorkingDirectory()
{
#if defined (PI_UNIX)
    char_t* pszDir = getcwd(NULL, 0);
#elif defined(PI_WINDOWS)
    char_t* pszDir = _tgetcwd(NULL, 0);
#endif
    std::string strDir(pszDir);
    strDir = strDir + PI_PATHDEL;
    delete pszDir;
    
    return strDir;
}

std::string piAbsPath(const std::string &path)
{
    piAssert(!path.empty(), std::string());
    
    if (path[0] == PI_PATHDEL)
    {
        return std::string(path);
    }

    std::string fullPath = piGetWorkingDirectory() + path;
    
    vector<std::string> segments = piStrSplit(fullPath.c_str(), fullPath.size(), PI_PATHDEL);
    vector<std::string> absPath;
    for (size_t i = 0; i < segments.size(); ++i)
    {
        std::string seg = segments[i];
        if (!seg.empty())
        {
            if (seg.size() == 2 && seg[0] == '.' && seg[1] == '.')
            {
                absPath.pop_back();
            }
            else if (seg.size() == 1 && seg[0] == '.')
            {
                // nothing to do
            }
            else
            {
                absPath.push_back(seg.c_str());
            }
        }
    }
    
    std::string result(1, PI_PATHDEL);
    
    for (int32_t i = 0; i < absPath.size(); ++i)
    {
        std::string seg = absPath[i];
        result = result + seg;
        if (i < absPath.size() - 1)
        {
            result = result + PI_PATHDEL;
        }
    }
    
    return result;
}


//==============================================================================
// FileFinder

#if defined(PI_LINUX)

static int EnumDirFilter(const struct dirent* pDirent)
{
    return strncmp(pDirent->d_name, ".", 1) != 0 && strncmp(pDirent->d_name, "..", 2) != 0;
}

static int EnumDirSortAscending(const struct dirent** a, const struct dirent** b)
{
    return strcmp((*a)->d_name, (*b)->d_name);
}

static int EnumDirSortDescending(const struct dirent** a, const struct dirent** b)
{
    return -strcmp((*a)->d_name, (*b)->d_name);
}

#elif defined (PI_APPLE)
#   if !defined(__MAC_10_8)

static int EnumDirFilter(struct dirent* pDirent)
{
    return strncmp(pDirent->d_name, ".", 1) != 0 && strncmp(pDirent->d_name, "..", 2) != 0;
}

static int EnumDirSortAscending(const void* a, const void* b)
{
    return strcmp((*(const struct dirent**)a)->d_name, (*(const struct dirent**)b)->d_name);
}

static int EnumDirSortDescending(const void* a, const void* b)
{
    return -strcmp((*(const struct dirent**)a)->d_name, (*(const struct dirent**)b)->d_name);
}

#   else

static int EnumDirFilter(const struct dirent* pDirent)
{
    return strncmp(pDirent->d_name, ".", 1) != 0 && strncmp(pDirent->d_name, "..", 2) != 0;
}

static int EnumDirSortAscending(const struct dirent** a, const struct dirent** b)
{
    return strcmp((*a)->d_name, (*b)->d_name);
}

static int EnumDirSortDescending(const struct dirent** a, const struct dirent** b)
{
    return -strcmp((*a)->d_name, (*b)->d_name);
}

#   endif
#endif

struct cFileEntry
{
    cFileEntry()
    {
        bIsDir = false;
    }
    
    cFileEntry(const cFileEntry& a)
    {
        bIsDir = a.bIsDir;
        strPath = a.strPath;
    }
    
    std::string strPath;
    bool bIsDir;
};

class cFileFinder : public iFileFinder
{
private:
    typedef list<cFileEntry> EntryList;
    list<cFileEntry>::const_iterator mIt;
    EntryList mFiles;
    int32_t mluSize;
    std::string mstrDir;
    
public:
    cFileFinder()
    {
        mluSize = 0;
    }
    
    virtual ~cFileFinder()
    {
    }
    
    virtual int32_t Search(const std::string &path, uint32_t uFlags)
    {
        piAssert(!path.empty(), 0);
        
        mFiles.clear();
        
        struct dirent** pList = NULL;
        
        int dCount = 0;
        if (piFlagIs(uFlags, eFileFinderFlag_Descending))
        {
            dCount = scandir(path.c_str(), &pList, EnumDirFilter, EnumDirSortDescending);
        }
        else
        {
            dCount = scandir(path.c_str(), &pList, EnumDirFilter, EnumDirSortAscending);
        }
        
        if (dCount < 0)
        {
            if (pList != NULL)
            {
                delete pList;
                pList = NULL;
            }
            return 0;
        }
        mluSize = dCount;
        
        for (int i = 0; i < dCount; ++i)
        {
            struct dirent* pDirent = pList[i];
            
            if (pDirent->d_type == DT_DIR)
            {
                if (piFlagIs(uFlags, eFileFinderFlag_Directory))
                {
                    cFileEntry item;
                    item.strPath = pDirent->d_name;
                    item.bIsDir = pDirent->d_type == DT_DIR;
                    mFiles.push_back(item);
                }
            }
            else
            {
                cFileEntry item;
                item.strPath = pDirent->d_name;
                item.bIsDir = pDirent->d_type == DT_DIR;
                mFiles.push_back(item);
            }
            
            free(pDirent);
        }
        
        if (pList != NULL)
        {
            free(pList);
            pList = NULL;
        }
        
        Reset();
        
        mstrDir = path;
        
        return dCount;
    }
    
    virtual void Reset()
    {
        mIt = mFiles.begin();
    }
    
    virtual std::string Path() const
    {
        std::string path(mstrDir);
        path = path + PI_PATHDEL;
        path = path + mIt->strPath;
        return path;
    }
    
    virtual std::string Name() const
    {
        if (mIt != mFiles.end())
        {
            return mIt->strPath;
        }
        else
        {
            return std::string();
        }
    }
    
    virtual iStream* OpenStream(const char *mode) const
    {
        if (mIt != mFiles.end())
        {
            std::string path = Path();
            return CreateFileStream(path.c_str(), mode);
        }
        else
        {
            return NULL;
        }
    }
    
    virtual bool IsDirectory() const
    {
        if (mIt != mFiles.end())
        {
            return mIt->bIsDir;
        }
        else
        {
            return false;
        }
    }
    
    virtual bool End() const
    {
        return mIt == mFiles.end();
    }
    
    virtual bool MoveNext()
    {
        if (mIt != mFiles.end())
        {
            ++mIt;
            return true;
        }
        else
        {
            return false;
        }
    }
    
    virtual bool MovePrevious()
    {
        if (mIt != mFiles.begin())
        {
            --mIt;
            return true;
        }
        else
        {
            return false;
        }
    }
    
    virtual int32_t Size() const
    {
        return mluSize;
    }
};

iFileFinder* CreateFileFinder()
{
    return new cFileFinder();
}


NSPI_END()








