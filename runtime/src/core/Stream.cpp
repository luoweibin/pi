/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2013.1.28   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>

using namespace std;

NSPI_BEGIN()

class cStreamRegion : public cStreamImpl<iStream>
{
private:
    int64_t mlldStart;
    int64_t mlldEnd;
    SmartPtr<iStream> mptrOrigin;
    
public:
    cStreamRegion()
    :cStreamImpl<iStream>()
    {
        mlldStart = 0;
        mlldEnd = 0;
    }
    
    virtual ~cStreamRegion()
    {
        mptrOrigin = NULL;
    }
    
    virtual string GetUri() const
    {
        return mptrOrigin->GetUri();
    }
    
    bool Init(iStream* pOrigin, int64_t lldStart, int64_t lldSize)
    {
        int64_t lldRealSize = pOrigin->GetSize();
        piAssert(lldStart < lldRealSize, false);
        piAssert(lldSize <= lldRealSize - lldStart, false);
        
        mptrOrigin = pOrigin;
        mlldEnd = lldStart + lldSize;
        mlldStart = lldStart;
        return true;
    }
    
    virtual int64_t GetSize() const
    {
        return mlldEnd - mlldStart;
    }
    
    virtual int64_t Seek(int64_t lldOffset, int dSeek)
    {
        piAssert(lldOffset >= 0 && lldOffset < GetSize(), -1);
        
        int64_t lldRet = mptrOrigin->Seek(mlldStart + lldOffset, dSeek);
        if (lldRet >= 0)
        {
            mlldOffset = lldOffset;
            return lldRet - mlldStart;
        }
        else
        {
            return lldRet;
        }
    }
    
    virtual int64_t Read(void* pBuffer, int64_t size)
    {
        piAssert(pBuffer != NULL, -1);
        piCheck(size > 0, 0);
        
        int64_t lldOffset = mlldOffset + mlldStart;
        int64_t lldOldOffset = mptrOrigin->GetOffset();
        if (mptrOrigin->Seek(lldOffset, eSeek_Set) >= 0)
        {
            int64_t ldRet = mptrOrigin->Read(pBuffer, size);
            if (ldRet > 0)
            {
                mlldOffset += ldRet;
            }
            mptrOrigin->Seek(lldOldOffset, eSeek_Set);
            return ldRet;
        }
        else
        {
            return -1;
        }
    }
    
    virtual int64_t Write(const void* pData, int64_t luSize)
    {
        piAssert(pData != NULL, -1);
        piCheck(luSize > 0, 0);
        
        int64_t lldOffset = mlldOffset + mlldStart;
        int64_t lldOldOffset = mptrOrigin->GetOffset();
        if (mptrOrigin->Seek(lldOffset, eSeek_Set) >= 0)
        {
            int64_t ldRet = mptrOrigin->Write(pData, luSize);
            if (ldRet > 0)
            {
                mlldOffset += ldRet;
            }
            mptrOrigin->Seek(lldOldOffset, eSeek_Set);
            return ldRet;
        }
        else
        {
            return -1;
        }
    }
    
    virtual bool Flush()
    {
        return mptrOrigin->Flush();
    }
};

iStream* CreateStreamRegion(iStream* pStream, int64_t lldStart, int64_t lldSize)
{
    piAssert(pStream != NULL, NULL);
    piAssert(lldStart >= 0, NULL);
    piAssert(lldSize > 0, NULL);
    
    SmartPtr<cStreamRegion> ptrRegion = new cStreamRegion();
    if (ptrRegion->Init(pStream, lldStart, lldSize))
    {
        return ptrRegion.PtrAndSetNull();
    }
    else
    {
        return NULL;
    }
}


//==============================================================================

class cStreamGroup : public cStreamImpl<iStreamGroup>
{
public:
    cStreamGroup():
    cStreamImpl<iStreamGroup>()
    {
        mlldSize = 0;
    }
    
    virtual ~cStreamGroup()
    {
        Clear();
    }
    
    virtual string GetUri() const
    {
        return string();
    }
    
    virtual int64_t GetSize() const
    {
        return mlldSize;
    }
    
    virtual int64_t Read(void* pBuffer, int64_t luSize)
    {
        piAssert(pBuffer != NULL, -1);
        piCheck(luSize > 0, 0);
        
        int64_t luSizeToRead = piMin(luSize, GetSize() - GetOffset());
        int64_t lldOffset = mlldOffset;
        StreamSet::const_iterator ptrIt = FindStream(&lldOffset);
        piCheck(ptrIt == mStreams.end(), -1);
        
        char* pRead = (char*)pBuffer;
        int64_t luRead = 0;
        while (luRead < luSizeToRead)
        {
            SmartPtr<iStream> ptrStream = *ptrIt;
            int64_t lldOldOffset = ptrStream->GetOffset();
            if (ptrStream->Seek(lldOffset, eSeek_Set) < 0)
            {
                return -1;
            }
            int64_t ldTemp = ptrStream->Read(pRead + luRead, luSize - luRead);
            if (ldTemp >= 0)
            {
                luRead += ldTemp;
                lldOffset = 0;
                ptrStream->Seek(lldOldOffset, eSeek_Set);
                ++ptrIt;
            }
            else
            {
                return -1;
            }
        }
        
        mlldOffset += luRead;
        return luRead;
    }
    
    virtual int64_t Write(const void* pData, int64_t luSize)
    {
        piAssert(pData != NULL, -1);
        piCheck(luSize > 0, 0);
        
        int64_t lldOffset = mlldOffset;
        StreamSet::const_iterator ptrIt = FindStream(&lldOffset);
        piCheck(ptrIt == mStreams.end(), -1);
        
        const char* pWrite = (const char*)pData;
        int64_t luWritten = 0;
        while (luWritten < luSize)
        {
            SmartPtr<iStream> ptrStream = *ptrIt;
            int64_t lldOldOffset = ptrStream->GetOffset();
            if (ptrStream->Seek(lldOffset, eSeek_Set) < 0)
            {
                return -1;
            }
            int64_t ldTemp = ptrStream->Write(pWrite + luWritten, luSize - luWritten);
            if (ldTemp >= 0)
            {
                luWritten += ldTemp;
                lldOffset = 0;
                ptrStream->Seek(lldOldOffset, eSeek_Set);
                ++ptrIt;
            }
            else
            {
                return -1;
            }
        }
        
        mlldOffset += luWritten;
        return luWritten;
    }
    
    virtual void Push(iStream* pStream)
    {
        piAssert(pStream != NULL, ;);
        mlldSize += pStream->GetSize();
        mStreams.push_back(pStream);
    }
    
    virtual void Unshift(iStream* pStream)
    {
        piAssert(pStream != NULL, ;);
        mlldSize += pStream->GetSize();
        mStreams.push_front(pStream);
    }
    
    virtual void Remove(iStream* pStream)
    {
        piAssert(pStream != NULL, ;);
        
        for (StreamSet::iterator it = mStreams.begin();
             it != mStreams.end();
             ++it)
        {
            if ((*it).Ptr() == pStream)
            {
                mlldSize -= pStream->GetSize();
                mStreams.erase(it);
                return;
            }
        }
    }
    
    virtual void Clear()
    {
        mStreams.clear();
        mlldOffset = 0;
        mlldSize = 0;
    }
    
    virtual bool Flush()
    {
        for (StreamSet::iterator it = mStreams.begin();
             it != mStreams.end();
             ++it)
        {
            piCheck((*it)->Flush(), false);
        }
        return true;
    }
    
private:
    typedef list<SmartPtr<iStream> > StreamSet;
    
    StreamSet::const_iterator FindStream(int64_t* plldOffset) const
    {
        int64_t lldCurrent = 0;
        for (StreamSet::const_iterator it = mStreams.begin();
             it != mStreams.end();
             ++it)
        {
            int64_t lldSize = (*it)->GetSize();
            if (lldCurrent + lldSize > *plldOffset)
            {
                *plldOffset -= lldCurrent;
                return it;
            }
            lldCurrent += lldSize;
        }
        
        return mStreams.end();
    }
    
private:
    StreamSet mStreams;
    int64_t mlldSize;
};

iStreamGroup* CreateStreamGroup()
{
    return new cStreamGroup();
}

//==============================================================================

class cMemoryStream : public cStreamImpl<iMemoryStream>
{
public:
    cMemoryStream(iMemory* pMem, int64_t luStart, int64_t luSize):
    cStreamImpl()
    {
        mlldOffset = 0;
        mlldStart = luStart;
        mlldEnd = luStart + luSize;
        mMem = pMem;
    }
    
    virtual ~cMemoryStream()
    {
        mMem = NULL;
    }
    
    virtual string GetUri() const
    {
        return string();
    }
    
    virtual int64_t GetSize() const
    {
        return mlldEnd - mlldStart;
    }
    
    virtual int64_t Read(void* pBuffer, int64_t luSize)
    {
        piAssert(pBuffer != NULL, -1);
        piCheck(luSize > 0, 0);
        
        int64_t luReadSize = piMin(luSize, GetSize() - mlldOffset);
        if (luReadSize > 0)
        {
            memcpy(pBuffer, mMem->Ptr() + mlldOffset + mlldStart, (size_t)luReadSize);
            mlldOffset += luReadSize;
            return luReadSize;
        }
        else
        {
            return 0;
        }
    }
    
    virtual int64_t Write(const void* pData, int64_t luSize)
    {
        int64_t luWriteSize = piMin(luSize, GetSize() - mlldOffset);
        if (luWriteSize > 0)
        {
            memcpy(mMem->Ptr() + mlldOffset + mlldStart, pData, (size_t)luWriteSize);
            mlldOffset += luWriteSize;
            return luWriteSize;
        }
        else
        {
            return -1;
        }
    }
    
    virtual bool Flush()
    {
        return true;
    }
    
    virtual iMemory* GetMemory() const
    {
        return mMem;
    }

private:
    int64_t mlldStart;
    int64_t mlldEnd;
    SmartPtr<iMemory> mMem;
};

iMemoryStream* CreateMemoryStream(int64_t size)
{
    piAssert(size > 0, NULL);
    
    SmartPtr<iMemory> ptrMem = CreateMemory(size);
    piAssert(!ptrMem.IsNull(), NULL);
    return new cMemoryStream(ptrMem, 0, size);
}

iMemoryStream* CreateMemoryStreamEx(iMemory* pMem, int64_t luStart, int64_t luSize)
{
    return new cMemoryStream(pMem, luStart, luSize - luStart);
}

iStream* CreateMemoryStreamCopy(const void* data, int64_t size)
{
    piAssert(data != NULL, NULL);
    piAssert(size > 0, NULL);
    SmartPtr<iMemory> ptrMem = CreateMemory(size);
    memcpy(ptrMem->Ptr(), data, (size_t)size);
    return CreateMemoryStreamEx(ptrMem, 0, size);
}

iStream* CreateMemoryStreamStatic(const void *data, int64_t size)
{
    piAssert(data != NULL, NULL);
    piAssert(size > 0, NULL);
    SmartPtr<iMemory> ptrMem = CreateMemoryStatic(data, size);
    return CreateMemoryStreamEx(ptrMem, 0, size);
}

//==============================================================================


class ReaderUTF8 : public iReaderUTF8
{
private:
    SmartPtr<iStream> mData;
    SmartPtr<iMemory> mBuffer;
    const char *mCurrent;
    const char *mEnd;
    const char *mLineEnd;
    
public:
    ReaderUTF8(iStream *data)
    {
        mCurrent = NULL;
        mEnd = NULL;
        mLineEnd = NULL;
        mData = data;
    }
    
    virtual ~ReaderUTF8()
    {
    }
    
    bool Init(int64_t bufferSize)
    {
        mBuffer = CreateMemory(bufferSize);
        piAssert(!mBuffer.IsNull(), false);
        
        return true;
    }
    
    virtual const char *GetLine() const
    {
        return mCurrent;
    }
    
    virtual int64_t GetSize() const
    {
        return mLineEnd - mCurrent;
    }
    
    virtual bool ReadLine()
    {
        if (mCurrent != mEnd)
        {
            mCurrent = MoveToNextLine(mCurrent, mEnd);
        }
        
        if (mCurrent == mEnd)
        {
            int64_t size = mData->Read(mBuffer->Ptr(), mBuffer->Size());
            if (size <= 0)
            {
                return false;
            }
        
            mCurrent = mBuffer->Ptr();
            mEnd = mCurrent + size;
            const char *lineEnd = FindLine(mCurrent, mEnd);
            if (lineEnd != mEnd)
            {
                mLineEnd = lineEnd;
                return true;
            }
            else
            {
                if (mData->End())
                {
                    mLineEnd = lineEnd;
                    return true;
                }
                else
                {
                    PILOGE(TT("CORE"), TT("Buffer is too small."));
                    return false;
                }
            }
        }
        else
        {
            const char *lineEnd = FindLine(mCurrent, mEnd);
            if (lineEnd != mEnd)
            {
                mLineEnd = lineEnd;
                return true;
            }
            else
            {
                if (mCurrent == mBuffer->Ptr())
                {
                    PILOGE(TT("CORE"), TT("Buffer is too small."));
                    return false;
                }
                
                int64_t dataSize = mEnd - mCurrent;
                memmove(mBuffer->Ptr(), mCurrent, (size_t)dataSize);
                mCurrent = mBuffer->Ptr();
                mEnd = mCurrent + dataSize;
                
                int64_t size = mData->Read(mBuffer->Ptr() + dataSize,
                                           mBuffer->Size() - dataSize);
                if (size <= 0)
                {
                    lineEnd = mEnd;
                    mLineEnd = lineEnd;
                    return true;
                }
                else
                {
                    mEnd += size;
                }
                
                lineEnd = FindLine(mCurrent, mEnd);
                if (lineEnd != mEnd)
                {
                    mLineEnd = lineEnd;
                    return true;
                }
                else
                {
                    PILOGE(TT("CORE"), TT("Buffer is too small."));
                    return false;
                }
            }
        }
    }
    
private:
    const char *FindLine(const char *start, const char *end)
    {
        const char *current = start;
        
        while (current != end)
        {
            char ch = *current;
            if (ch == '\r' || ch == '\n')
            {
                return current;
            }
            else
            {
                ++current;
            }
        }
        
        return end;
    }
    
    const char *MoveToNextLine(const char *start, const char *end)
    {
        const char *current = start;
        while (current != end)
        {
            char ch = *current;
            if (ch != '\r' && ch != '\n')
            {
                ++current;
            }
            else
            {
                break;
            }
        }
        
        piCheck(current != end, end);
        
        char ch = *current;
        if (ch == '\r')
        {
            ++current;
            if (current != end)
            {
                ch = *current;
                if (ch == '\n')
                {
                    ++current;
                }
            }
        }
        else if (ch == '\n')
        {
            ++current;
        }
        
        return current;
    }
};

iReaderUTF8 *CreateReaderUTF8(iStream *data, int64_t bufferSize)
{
    piAssert(data != NULL, NULL);
    piAssert(bufferSize > 0, NULL);
    
    SmartPtr<ReaderUTF8> reader = new ReaderUTF8(data);
    if (reader->Init(bufferSize))
    {
        return reader.PtrAndSetNull();
    }
    else
    {
        return NULL;
    }
}


NSPI_END()























