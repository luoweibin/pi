/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2012.5.13   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>

#if defined (PI_UNIX)
#   include <time.h>
#   include <sys/time.h>
#endif

#if defined(PI_APPLE)
#   include <mach/mach_time.h>
#endif

#if defined(PI_WINDOWS)
#	include <Mmsystem.h>
#	pragma comment(lib, "Winmm.lib")
#endif

using namespace std;

NSPI_BEGIN()

int64_t piGetUpTimeUS()
{
#if defined(PI_APPLE)
	int64_t absTime = mach_absolute_time();
    
    mach_timebase_info_data_t info;
    mach_timebase_info(&info);
    absTime *= info.numer;
    absTime /= info.denom;
    
    return piNs2Us(absTime);
#endif
    
#if defined(PI_LINUX)
	struct timespec time;
	piAssert(clock_gettime(CLOCK_MONOTONIC, &time) == 0, 0);
    piAssert(time.tv_sec > 0, 0);
	return piSec2Us((uint64_t)time.tv_sec) + (uint64_t)piNs2Us(time.tv_nsec);
#endif
    
#if defined(PI_WINDOWS)
	LARGE_INTEGER freq;
	memset(&freq,0,sizeof(freq));
	piAssert(::QueryPerformanceFrequency(&freq),0);
    
	LARGE_INTEGER counter;
	memset(&counter,0,sizeof(counter));
	piAssert(::QueryPerformanceCounter(&counter),0);
    
	return piSec2Us(counter.QuadPart)/freq.QuadPart;
#endif 
}

iTime* piGetCurrentTime()
{
#if defined(PI_APPLE)
    int64_t absTime = mach_absolute_time();
    
    mach_timebase_info_data_t info;
    mach_timebase_info(&info);
    absTime *= info.numer;
    absTime /= info.denom;
    
    return CreateTime(absTime, piSec2Ns(1));
#endif
    
#if defined(PI_LINUX)
    struct timespec time;
    piAssert(clock_gettime(CLOCK_MONOTONIC, &time) == 0, 0);
    piAssert(time.tv_sec > 0, 0);
    int64_t us = piSec2Us((uint64_t)time.tv_sec) + (uint64_t)piNs2Us(time.tv_nsec);
    return CreateTime(us, piSec2Us(1));
#endif
    
#if defined(PI_WINDOWS)
    LARGE_INTEGER freq;
    memset(&freq,0,sizeof(freq));
    piAssert(::QueryPerformanceFrequency(&freq),0);
    
    LARGE_INTEGER counter;
    memset(&counter,0,sizeof(counter));
    piAssert(::QueryPerformanceCounter(&counter),0);
    
    return CreateTime(couner.QuadPart, freq.QuadPart);
#endif
}

void piSleepMS(int64_t timeout)
{
#if defined (PI_POSIX)
	struct timespec time;
	time.tv_sec = (size_t)piMs2Sec(timeout);
	time.tv_nsec = (size_t)piMs2Ns(timeout - piSec2Ms(time.tv_sec));
    if (time.tv_nsec >= 1000000000)
    {
        time.tv_nsec %= 1000000000;
        ++time.tv_sec;
    }
    
	struct timespec rem;
	rem.tv_sec = 0;
	rem.tv_nsec = 0;
    
	while (nanosleep(&time, &rem) != 0)
	{
		time.tv_sec = rem.tv_sec;
		time.tv_nsec = rem.tv_nsec;
	}
#elif defined(PI_WINDOWS)
	uint64_t lluTimeMS = piMax(1, piUs2Ms(lluTimeoutUS));
	::Sleep((DWORD)lluTimeMS);
#endif
}

int64_t piGetSystemTimeMS()
{
#if defined (PI_WINDOWS)
    return timeGetTime();
#elif defined (PI_UNIX)
    struct timeval now;
    piAssert(gettimeofday(&now, NULL)==0, 0);
    return piSec2Ms((int64_t)now.tv_sec) + piUs2Ms(now.tv_usec);
#endif
}



//==============================================================================
// TimeStamp

class Time : public iTime
{
private:
    int64_t mTimeScale;
    int64_t mValue;
    
public:
    Time(int64_t value, int64_t timeScale):
    mTimeScale(timeScale),
    mValue(value)
    {
    }
    
    virtual ~Time()
    {
    }
    
    virtual int64_t GetValue() const
    {
        return mValue;
    }
    
    virtual void SetValue(int64_t value)
    {
        mValue = value;
    }
    
    virtual int64_t GetTimeScale() const
    {
        return mTimeScale;
    }
    
    virtual void SetTimeScale(int64_t value)
    {
        piAssert(value > 0, ;);
        mTimeScale = value;
    }
    
    virtual void Rescale(int64_t timeScale)
    {
        piAssert(timeScale > 0, ;);
        
        mValue = double(timeScale) / double(mTimeScale) * mValue;
    }
    
    virtual iTime* Clone() const
    {
        return CreateTime(mValue, mTimeScale);
    }
    
    virtual string ToString() const
    {
        return piFormat("iTime{value=%lld, timeScale=%d}", mValue, mTimeScale);
    }
    
    virtual int64_t Timestamp() const
    {
        return piSec2Ms(1.0)/double(mTimeScale) * mValue;
    }
};

iTime* CreateTime(int64_t value, int64_t timeScale)
{
    piAssert(timeScale > 0, nullptr);
    return new Time(value, timeScale);
}



class TimeStamp : public iTimeStamp
{
private:
    SmartPtr<iTime> mHostTime;
    SmartPtr<iTime> mFrameTime;
    
public:
    TimeStamp(iTime *frameTime, iTime *hostTime):
    mHostTime(hostTime),
    mFrameTime(frameTime)
    {
    }
    
    virtual ~TimeStamp()
    {
    }
    
    virtual iTime* GetHostTime() const
    {
        return mHostTime;
    }
    
    virtual void SetHostTime(iTime *time)
    {
        mHostTime = time;
    }
    
    virtual iTime* GetFrameTime() const
    {
        return mFrameTime;
    }
    
    virtual void SetFrameTime(iTime *time)
    {
        mFrameTime = time;
    }
};

iTimeStamp* CreateTimeStamp(iTime *frameTime, iTime *hostTime)
{
    return new TimeStamp(frameTime, hostTime);
}


NSPI_END()




















