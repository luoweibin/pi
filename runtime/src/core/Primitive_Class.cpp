/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.10   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>

using namespace std;

NSPI_BEGIN()


template <int T>
class PrimitiveClassImpl : public RefObjectClassImpl
{
private:
    Var mDefValue;
    
public:
    PrimitiveClassImpl(const string& name, const Var& defValue, iClassLoader* classLoader):
    RefObjectClassImpl(name, name, classLoader), mDefValue(defValue)
    {
    }
    
    virtual bool IsArray() const
    {
        return false;
    }
    
    virtual iClass* GetParent() const
    {
        return nullptr;
    }
    
    virtual int GetType() const
    {
        return T;
    }
    
    virtual iClass* GetElementClass() const
    {
        return nullptr;
    }
    
    virtual bool InstanceOf(const Var& value)
    {
        return T == value.GetType();
    }
    
    virtual Var CreateInstance()
    {
        return mDefValue;
    }
};


static SmartPtr<iClass> g_nspi_Boolean_Class;
static SmartPtr<iClass> g_nspi_I8_Class;
static SmartPtr<iClass> g_nspi_U8_Class;
static SmartPtr<iClass> g_nspi_I16_Class;
static SmartPtr<iClass> g_nspi_U16_Class;
static SmartPtr<iClass> g_nspi_I32_Class;
static SmartPtr<iClass> g_nspi_U32_Class;
static SmartPtr<iClass> g_nspi_I64_Class;
static SmartPtr<iClass> g_nspi_U64_Class;
static SmartPtr<iClass> g_nspi_F32_Class;
static SmartPtr<iClass> g_nspi_F64_Class;
static SmartPtr<iClass> g_nspi_Vec2_Class;
static SmartPtr<iClass> g_nspi_Vec3_Class;
static SmartPtr<iClass> g_nspi_Vec4_Class;
static SmartPtr<iClass> g_nspi_Rect_Class;
static SmartPtr<iClass> g_nspi_Quat_Class;
static SmartPtr<iClass> g_nspi_Mat4_Class;
static SmartPtr<iClass> g_nspi_String_Class;
static SmartPtr<iClass> g_nspi_Var_Class;


iClass* PrimitiveClass::Boolean()
{
    return g_nspi_Boolean_Class;
}

iClass* PrimitiveClass::I8()
{
    return g_nspi_I8_Class;
}

iClass* PrimitiveClass::U8()
{
    return g_nspi_U8_Class;
}

iClass* PrimitiveClass::I16()
{
    return g_nspi_I16_Class;
}

iClass* PrimitiveClass::U16()
{
    return g_nspi_U16_Class;
}

iClass* PrimitiveClass::I32()
{
    return g_nspi_I32_Class;
}

iClass* PrimitiveClass::U32()
{
    return g_nspi_U32_Class;
}

iClass* PrimitiveClass::I64()
{
    return g_nspi_I64_Class;
}

iClass* PrimitiveClass::U64()
{
    return g_nspi_U64_Class;
}

iClass* PrimitiveClass::F32()
{
    return g_nspi_F32_Class;
}

iClass* PrimitiveClass::F64()
{
    return g_nspi_F64_Class;
}

iClass* PrimitiveClass::Rect()
{
    return g_nspi_Rect_Class;
}

iClass* PrimitiveClass::Quat()
{
    return g_nspi_Quat_Class;
}

iClass* PrimitiveClass::Vec2()
{
    return g_nspi_Vec2_Class;
}

iClass* PrimitiveClass::Vec3()
{
    return g_nspi_Vec3_Class;
}

iClass* PrimitiveClass::Vec4()
{
    return g_nspi_Vec4_Class;
}

iClass* PrimitiveClass::String()
{
    return g_nspi_String_Class;
}

iClass* PrimitiveClass::Var()
{
    return g_nspi_Var_Class;
}

iClass* PrimitiveClass::Mat4()
{
    return g_nspi_Mat4_Class;
}




iClass* PrimitiveClass::Get(int type)
{
    switch (type)
    {
        case eType_Boolean:
            return g_nspi_Boolean_Class;
        case eType_I8:
            return g_nspi_I8_Class;
        case eType_U8:
            return g_nspi_U8_Class;
        case eType_I16:
            return g_nspi_I16_Class;
        case eType_U16:
            return g_nspi_U16_Class;
        case eType_I32:
            return g_nspi_I32_Class;
        case eType_U32:
            return g_nspi_U32_Class;
        case eType_I64:
            return g_nspi_I64_Class;
        case eType_U64:
            return g_nspi_U64_Class;
        case eType_F32:
            return g_nspi_F32_Class;
        case eType_F64:
            return g_nspi_F64_Class;
        case eType_Vec2:
            return g_nspi_Vec2_Class;
        case eType_Vec3:
            return g_nspi_Vec3_Class;
        case eType_Vec4:
            return g_nspi_Vec4_Class;
        case eType_Rect:
            return g_nspi_Rect_Class;
        case eType_Quat:
            return g_nspi_Quat_Class;
        case eType_Mat4:
            return g_nspi_Mat4_Class;
        case eType_String:
            return g_nspi_String_Class;
        case eType_Var:
            return g_nspi_Var_Class;
        default:
            return nullptr;
    }
}



template <int T>
static void InitPrimitiveClass(const string& name, SmartPtr<iClass>& klass, const Var& defvalue)
{
    SmartPtr<iClassLoader> classLoader = piGetRootClassLoader();
    klass = new PrimitiveClassImpl<T>(name, defvalue, classLoader);
    klass->SetReadonly();
    classLoader->RegisterClass(name, klass);
}

static struct nspi_Reflection_Init
{
    nspi_Reflection_Init()
    {
        InitPrimitiveClass<eType_Boolean>("bool", g_nspi_Boolean_Class, false);
        InitPrimitiveClass<eType_I8>("int8_t", g_nspi_I8_Class, 0);
        InitPrimitiveClass<eType_U8>("uint8_t", g_nspi_U8_Class, 0);
        InitPrimitiveClass<eType_I16>("int16_t", g_nspi_I16_Class, 0);
        InitPrimitiveClass<eType_U16>("uint16_t", g_nspi_U16_Class, 0);
        InitPrimitiveClass<eType_I32>("int32_t", g_nspi_I32_Class, 0);
        InitPrimitiveClass<eType_U32>("uint32_t", g_nspi_U32_Class, 0);
        InitPrimitiveClass<eType_I64>("int64_t", g_nspi_I64_Class, 0);
        InitPrimitiveClass<eType_U64>("uint64_t", g_nspi_U64_Class, 0);
        InitPrimitiveClass<eType_F32>("float", g_nspi_F32_Class, 0);
        InitPrimitiveClass<eType_F64>("double", g_nspi_F64_Class, 0);
        InitPrimitiveClass<eType_Vec2>("vec2", g_nspi_Vec2_Class, vec2());
        InitPrimitiveClass<eType_Vec3>("vec3", g_nspi_Vec3_Class, vec3());
        InitPrimitiveClass<eType_Vec4>("vec4", g_nspi_Vec4_Class, vec4());
        InitPrimitiveClass<eType_Quat>("quat", g_nspi_Quat_Class, quat());
        InitPrimitiveClass<eType_Mat4>("mat4", g_nspi_Mat4_Class, mat4());
        InitPrimitiveClass<eType_Rect>("rect", g_nspi_Rect_Class, rect());
        InitPrimitiveClass<eType_String>("string", g_nspi_String_Class, "");
        InitPrimitiveClass<eType_Var>("Var", g_nspi_Var_Class, Var());
    }
} g_nspi_Reflection_Init;

NSPI_END()















