/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.10.11   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>

using namespace std;

NSPI_BEGIN()

class F32Array : public PrimitiveArrayImpl<float, iF32Array>
{
public:
    F32Array()
    {
    }
    
    F32Array(const float* values, int32_t count):
    PrimitiveArrayImpl(values, count)
    {
    }
    
    virtual ~F32Array()
    {
    }
    
    virtual int GetType() const
    {
        return eType_F32Array;
    }
    
    virtual string ToString() const
    {
        return piFormat("F32[%d]", GetCount());
    }
};

iF32Array* CreateF32Array()
{
    return new F32Array();
}

iF32Array* CreateF32ArrayRaw(const float *values, int32_t count)
{
    return CreatePrimitiveArray<float, iF32Array, F32Array>(values, count);
}


class F64Array : public PrimitiveArrayImpl<double, iF64Array>
{
public:
    F64Array()
    {
    }
    
    F64Array(const double* values, int32_t count):
    PrimitiveArrayImpl(values, count)
    {
    }
    
    virtual ~F64Array()
    {
    }
    
    virtual int GetType() const
    {
        return eType_F64Array;
    }
    
    virtual string ToString() const
    {
        return piFormat("F64[%d]", GetCount());
    }
};

iF64Array* CreateF64Array()
{
    return new F64Array();
}

iF64Array* CreateF64ArrayRaw(const double *values, int32_t count)
{
    return CreatePrimitiveArray<double, iF64Array, F64Array>(values, count);
}


class I8Array : public PrimitiveArrayImpl<int8_t, iI8Array>
{
public:
    I8Array()
    {
    }
    
    I8Array(const int8_t* values, int32_t count):
    PrimitiveArrayImpl(values, count)
    {
    }
    
    virtual ~I8Array()
    {
    }
    
    virtual int GetType() const
    {
        return eType_I8Array;
    }
    
    virtual string ToString() const
    {
        return piFormat("I8[%d]", GetCount());
    }
};

iI8Array* CreateI8Array()
{
    return new I8Array();
}

iI8Array* CreateI8ArrayRaw(const int8_t *values, int32_t count)
{
    return CreatePrimitiveArray<int8_t, iI8Array, I8Array>(values, count);
}


class U8Array : public PrimitiveArrayImpl<uint8_t, iU8Array>
{
public:
    U8Array()
    {
    }
    
    U8Array(const uint8_t* values, int32_t count):
    PrimitiveArrayImpl(values, count)
    {
    }
    
    virtual ~U8Array()
    {
    }
    
    virtual int GetType() const
    {
        return eType_U8Array;
    }
    
    virtual string ToString() const
    {
        return piFormat("U8[%d]", GetCount());
    }
};

iU8Array* CreateU8Array()
{
    return new U8Array();
}

iU8Array* CreateU8ArrayRaw(const uint8_t *values, int32_t count)
{
    return CreatePrimitiveArray<uint8_t, iU8Array, U8Array>(values, count);
}


class I16Array : public PrimitiveArrayImpl<int16_t, iI16Array>
{
public:
    I16Array()
    {
    }
    
    I16Array(const int16_t* values, int32_t count):
    PrimitiveArrayImpl(values, count)
    {
    }
    
    virtual ~I16Array()
    {
    }
    
    virtual int GetType() const
    {
        return eType_I16Array;
    }
    
    virtual string ToString() const
    {
        return piFormat("I16[%d]", GetCount());
    }
};

iI16Array* CreateI16Array()
{
    return new I16Array();
}

iI16Array* CreateI16ArrayRaw(const int16_t *values, int32_t count)
{
    return CreatePrimitiveArray<int16_t, iI16Array, I16Array>(values, count);
}

class U16Array : public PrimitiveArrayImpl<uint16_t, iU16Array>
{
public:
    U16Array()
    {
    }
    
    U16Array(const uint16_t* values, int32_t count):
    PrimitiveArrayImpl(values, count)
    {
    }
    
    virtual ~U16Array()
    {
    }
    
    virtual int GetType() const
    {
        return eType_U16Array;
    }
    
    virtual string ToString() const
    {
        return piFormat("U16[%d]", GetCount());
    }
};

iU16Array* CreateU16Array()
{
    return new U16Array();
}

iU16Array* CreateU16ArrayRaw(const uint16_t *values, int32_t count)
{
    return CreatePrimitiveArray<uint16_t, iU16Array, U16Array>(values, count);
}


class I32Array : public PrimitiveArrayImpl<int32_t, iI32Array>
{
public:
    I32Array()
    {
    }
    
    I32Array(const int32_t* values, int32_t count):
    PrimitiveArrayImpl(values, count)
    {
    }
    
    virtual ~I32Array()
    {
    }
    
    virtual int GetType() const
    {
        return eType_I32Array;
    }
    
    virtual string ToString() const
    {
        return piFormat("I32[%d]", GetCount());
    }
};

iI32Array* CreateI32Array()
{
    return new I32Array();
}

iI32Array* CreateI32ArrayRaw(const int32_t *values, int32_t count)
{
    return CreatePrimitiveArray<int32_t, iI32Array, I32Array>(values, count);
}


class U32Array : public PrimitiveArrayImpl<uint32_t, iU32Array>
{
public:
    U32Array()
    {
    }
    
    U32Array(const uint32_t* values, int32_t count):
    PrimitiveArrayImpl(values, count)
    {
    }
    
    virtual ~U32Array()
    {
    }
    
    virtual int GetType() const
    {
        return eType_U32Array;
    }
    
    virtual string ToString() const
    {
        return piFormat("U32[%d]", GetCount());
    }
};

iU32Array* CreateU32Array()
{
    return new U32Array();
}

iU32Array* CreateU32ArrayRaw(const uint32_t *values, int32_t count)
{
    return CreatePrimitiveArray<uint32_t, iU32Array, U32Array>(values, count);
}

class I64Array : public PrimitiveArrayImpl<int64_t, iI64Array>
{
public:
    I64Array()
    {
    }
    
    I64Array(const int64_t* values, int32_t count):
    PrimitiveArrayImpl(values, count)
    {
    }
    
    virtual ~I64Array()
    {
    }
    
    virtual int GetType() const
    {
        return eType_I64Array;
    }
    
    virtual string ToString() const
    {
        return piFormat("I64[%d]", GetCount());
    }
};

iI64Array* CreateI64Array()
{
    return new I64Array();
}

iI64Array* CreateI64ArrayRaw(const int64_t *values, int32_t count)
{
    return CreatePrimitiveArray<int64_t, iI64Array, I64Array>(values, count);
}


class U64Array : public PrimitiveArrayImpl<uint64_t, iU64Array>
{
public:
    U64Array()
    {
    }
    
    U64Array(const uint64_t* values, int32_t count):
    PrimitiveArrayImpl(values, count)
    {
    }
    
    virtual ~U64Array()
    {
    }
    
    virtual int GetType() const
    {
        return eType_U64Array;
    }
    
    virtual string ToString() const
    {
        return piFormat("U64[%d]", GetCount());
    }
};

iU64Array* CreateU64Array()
{
    return new U64Array();
}

iU64Array* CreateU64ArrayRaw(const uint64_t *values, int32_t count)
{
    return CreatePrimitiveArray<uint64_t, iU64Array, U64Array>(values, count);
}


//==================================================================================================

class StringArray : public StructArrayImpl<string, iStringArray>
{
public:
    StringArray()
    {
    }
    
    virtual ~StringArray()
    {
    }
    
    virtual int GetType() const
    {
        return eType_StringArray;
    }
    
    virtual string ToString() const
    {
        return piFormat("String[%d]", GetCount());
    }
};

iStringArray* CreateStringArray()
{
    return new StringArray();
}

class Vec2Array : public StructArrayImpl<vec2, iVec2Array>
{
public:
    Vec2Array()
    {
    }
    
    Vec2Array(const vec2* values, int32_t count):
    StructArrayImpl(values, count)
    {
    }
    
    virtual ~Vec2Array()
    {
    }
    
    virtual int GetType() const
    {
        return eType_Vec2Array;
    }
    
    virtual string ToString() const
    {
        return piFormat("vec2[%d]", GetCount());
    }
};


iVec2Array* CreateVec2Array()
{
    return new Vec2Array();
}

iVec2Array* CreateVec2ArrayRaw(const vec2 *values, int32_t count)
{
    return CreateStructArray<vec2, iVec2Array, Vec2Array>(values, count);
}

class Vec3Array : public StructArrayImpl<vec3, iVec3Array>
{
public:
    Vec3Array()
    {
    }
    
    Vec3Array(const vec3* values, int32_t count):
    StructArrayImpl(values, count)
    {
    }
    
    virtual ~Vec3Array()
    {
    }
    
    virtual int GetType() const
    {
        return eType_Vec3Array;
    }
    
    virtual string ToString() const
    {
        return piFormat("vec3[%d]", GetCount());
    }
};

iVec3Array* CreateVec3Array()
{
    return new Vec3Array();
}

iVec3Array* CreateVec3ArrayRaw(const vec3 *values, int32_t count)
{
    return CreateStructArray<vec3, iVec3Array, Vec3Array>(values, count);
}


class Vec4Array : public StructArrayImpl<vec4, iVec4Array>
{
public:
    Vec4Array()
    {
    }
    
    Vec4Array(const vec4* values, int32_t count):
    StructArrayImpl(values, count)
    {
    }
    
    virtual ~Vec4Array()
    {
    }
    
    virtual int GetType() const
    {
        return eType_Vec4Array;
    }
    
    virtual string ToString() const
    {
        return piFormat("vec4[%d]", GetCount());
    }
};

iVec4Array* CreateVec4Array()
{
    return new Vec4Array();
}

iVec4Array* CreateVec4ArrayRaw(const vec4* values, int32_t count)
{
    return CreateStructArray<vec4, iVec4Array, Vec4Array>(values, count);
}


class Mat4Array : public StructArrayImpl<mat4, iMat4Array>
{
public:
    Mat4Array()
    {
    }
    
    Mat4Array(const mat4* values, int32_t count):
    StructArrayImpl(values, count)
    {
    }
    
    virtual ~Mat4Array()
    {
    }
    
    virtual int GetType() const
    {
        return eType_Mat4Array;
    }
    
    virtual string ToString() const
    {
        return piFormat("mat4[%d]", GetCount());
    }
};

iMat4Array* CreateMat4Array()
{
    return new Mat4Array();
}

iMat4Array* CreateMat4ArrayRaw(const mat4 *values, int32_t count)
{
    return CreateStructArray<mat4, iMat4Array, Mat4Array>(values, count);
}



class QuatArray : public StructArrayImpl<quat, iQuatArray>
{
public:
    QuatArray()
    {
    }
    
    QuatArray(const quat* values, int32_t count):
    StructArrayImpl(values, count)
    {
    }
    
    virtual ~QuatArray()
    {
    }
    
    virtual int GetType() const
    {
        return eType_QuatArray;
    }
    
    virtual string ToString() const
    {
        return piFormat("quat[%d]", GetCount());
    }
};

iQuatArray* CreateQuatArray()
{
    return new QuatArray();
}

iQuatArray* CreateQuatArrayRaw(const quat *values, int32_t count)
{
    return CreateStructArray<quat, iQuatArray, QuatArray>(values, count);
}


class RectArray : public StructArrayImpl<rect, iRectArray>
{
public:
    RectArray()
    {
    }
    
    RectArray(const rect* values, int32_t count):
    StructArrayImpl(values, count)
    {
    }
    
    virtual ~RectArray()
    {
    }
    
    virtual int GetType() const
    {
        return eType_RectArray;
    }
    
    virtual string ToString() const
    {
        return piFormat("rect[%d]", GetCount());
    }
};

iRectArray* CreateRectArray()
{
    return new RectArray();
}

iRectArray* CreateRectArrayRaw(const rect *values, int32_t count)
{
    return CreateStructArray<rect, iRectArray, RectArray>(values, count);
}

NSPI_END()

























