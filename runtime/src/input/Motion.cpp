/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.22   1.0     Create
 ******************************************************************************/
#include <pi/input/impl/HIDEventImpl.h>

using namespace std;

NSPI_BEGIN()


class DeviceMotion : public iDeviceMotion
{
private:
    vec3 mAtt;
    quat mAttQuat;
    vec3 mRot;
    vec3 mMagField;
    int mMagAccuracy;
    vec3 mGravity;
    vec3 mUserAccel;
    
public:
    DeviceMotion():
    mMagAccuracy(eAccuracy_None)
    {
    }
    
    virtual ~DeviceMotion()
    {
    }
    
    virtual quat GetAttitudeQuat() const
    {
        return mAttQuat;
    }
    
    virtual void SetAttitudeQuat(const quat& value)
    {
        mAttQuat = value;
    }
    
    virtual vec3 GetAttitude() const
    {
        return mAtt;
    }
    
    virtual void SetAttitude(const vec3& value)
    {
        piAssert(!IsReadonly(), ;);
        mAtt = value;
    }
    
    virtual vec3 GetRotationRate() const
    {
        return mRot;
    }
    
    virtual void SetRotationRate(const vec3& rate)
    {
        piAssert(!IsReadonly(), ;);
        mRot = rate;
    }
    
    virtual void SetGravity(const vec3& value)
    {
        piAssert(!IsReadonly(), ;);
        mGravity = value;
    }
    
    virtual vec3 GetGravity() const
    {
        return mGravity;
    }
    
    virtual void SetUserAccel(const vec3& value)
    {
        piAssert(!IsReadonly(), ;);
        mUserAccel = value;
    }
    
    virtual vec3 GetUserAccel() const
    {
        return mUserAccel;
    }
    
    virtual void SetMagField(const vec3& value)
    {
        piAssert(!IsReadonly(), ;);
        mMagField = value;
    }
    
    virtual vec3 GetMagField() const
    {
        return mMagField;
    }
    
    virtual void SetMagAccuracy(int value)
    {
        piAssert(!IsReadonly(), ;);
        mMagAccuracy = value;
    }
    
    virtual int GetMagAccuracy() const
    {
        return mMagAccuracy;
    }
    
    virtual string ToString() const
    {
        string buffer = piFormat("iDeviceMotion[%p] \n{\n", this);
        buffer += piFormat("    Attitude:%s\n", piToString(mAtt).c_str());
        buffer += piFormat("    RotationRate:%s\n", piToString(mRot).c_str());
        buffer += piFormat("    gravity:%s\n", piToString(mGravity).c_str());
        buffer += "}\n";
        return buffer;
    }
};

iDeviceMotion* CreateDeviceMotion()
{
    return new DeviceMotion();
}

NSPI_END()























