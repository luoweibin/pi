/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.21   1.0     Create
 ******************************************************************************/
#include <pi/Input.h>

using namespace std;


NSPI_BEGIN()


class HID : public iHID
{
private:
    int32_t mFlags;
    SmartPtr<iKeyEventArray> mKeyEvents;
    SmartPtr<iCalibrate> mCalib;
    
    double mAudioLevel;
    SmartPtr<iNativeTexture> mOriginFrame;
    SmartPtr<iNativeTexture> mFilteredFrame;
    
    SmartPtr<iFaceTracker> mFaceTracker;
    SmartPtr<iFaceTrackerResult> mFaceResult;
    
    SmartPtr<iBitmap> mOriginBitmap;
    
    SmartPtr<iTouchEvent> mTapEv;
    
    bool mDirty;
    
public:
    HID():
    mFlags(0), mAudioLevel(-100), mDirty(true)
    {
        mKeyEvents = CreateKeyEventArray();
        mCalib = CreateCalibrate();
        mFaceResult = CreateFaceTrackerResult();
    }
    
    virtual ~HID()
    {
    }
    
    virtual void SetFaceTracker(iFaceTracker* tracker)
    {
        mFaceTracker = tracker;
    }
    
    /**
     * -----------------------------------------------------------------------------------------
     * Input State
     */
    
    virtual void Reset()
    {
        mTapEv = nullptr;
        mKeyEvents->Clear();
    }
    
    virtual iTouchEvent* GetTapEvent() const
    {
        return mTapEv;
    }
    
    /**
     * -----------------------------------------------------------------------------------------
     * Keyboard
     */
    
    virtual iKeyEventArray* GetKeyEvents() const
    {
        return mKeyEvents;
    }
    
    virtual bool IsKeyDown(int keyCode) const
    {
        auto ret = piLastIndexOf<iKeyEventArray, iKeyEvent*>(mKeyEvents,
                                                             [keyCode](iKeyEvent* e) {
                                                                 return e->GetCode() == keyCode
                                                                 && e->GetID() == eHIDEvent_KeyDown;
                                                             });
        return ret >= 0;
    }
    
    virtual bool IsKeyUp(int keyCode) const
    {
        auto ret = piLastIndexOf<iKeyEventArray, iKeyEvent*>(mKeyEvents,
                                                             [keyCode](iKeyEvent* e) {
                                                                 return e->GetCode() == keyCode
                                                                 && e->GetID() == eHIDEvent_KeyUp;
                                                             });
        return ret >= 0;
    }
    
    virtual bool IsFlagsOn(int32_t flags) const
    {
        return piFlagIs(mFlags, flags);
    }
    
    
    /**
     * -----------------------------------------------------------------------------------------
     * Camera Calibration
     */
    
    virtual mat4 GetFaceViewMatrix(int32_t faceIndex) const
    {
        return mCalib->GetViewMatrix(faceIndex);
    }
    
    virtual iFaceTrackerResult* GetFaceTrackerResult() const
    {
        return mFaceResult;
    }
    
    
    /**
     * -----------------------------------------------------------------------------------------
     * AVCapture
     */
    
    virtual double GetAudioLevel() const
    {
        return mAudioLevel;
    }
    
    virtual iNativeTexture* GetOriginFrame() const
    {
        return mOriginFrame;
    }
    
    virtual iNativeTexture* GetFilteredFrame() const
    {
        return mFilteredFrame;
    }
    
    virtual iBitmap* GetOriginBitmap() const
    {
        return mOriginBitmap;
    }
    
    /**
     * ---------------------------------------------------------------------------------------------
     * Callback
     */
    
    virtual void OnLoad()
    {
        SmartPtr<iNativeTexture> frame = CreateNativeTexture();
        int32_t name = piCreateTexture();
        piBindTexture(eTexTarget_2D, name);
        piTexParam(eTexTarget_2D, eTexConfig_WrapS, eTexValue_Clamp);
        piTexParam(eTexTarget_2D, eTexConfig_WrapT, eTexValue_Clamp);
        piTexParam(eTexTarget_2D, eTexConfig_MinFilter, eTexValue_Linear);
        piTexParam(eTexTarget_2D, eTexConfig_MagFilter, eTexValue_Linear);
        piBindTexture(eTexTarget_2D, 0);
        
        frame->SetName(name);
        
        mOriginFrame = frame;
    }
    
    virtual void OnUnload()
    {
        mOriginBitmap = nullptr;
        mOriginFrame = nullptr;
        mOriginBitmap = nullptr;
    }
    
    virtual void OnUpdate()
    {
        mKeyEvents->Clear();
        
        if (mDirty)
        {
            mCalib->Update(mFaceResult);
            mDirty = false;
        }
    }
    
    virtual void OnEvent(iHIDEvent* event)
    {
        switch (event->GetID())
        {
            case eHIDEvent_Tap:
                OnTap(event);
                break;
            case eHIDEvent_KeyDown:
            case eHIDEvent_KeyUp:
                OnKey(event);
                break;
            case eHIDEvent_FaceTrackerChanged:
                OnFaceTrackerChanged(event);
                break;
            case eHIDevent_AudioLevelChanged:
                OnAudioLevelChanged(event);
                break;
            case eHIDEvent_VideoFrameChanged:
                OnVideoFrameChanged(event);
                break;
            case eHIDEvent_FilteredFrameChanged:
                OnFilteredFrameChanged(event);
                break;
            default:
                PILOGW(PI_TAG, "HID event not handled, type:%d.", event->GetID());
                break;
        }
    }
    
    virtual void OnResize(const rect& bounds)
    {
        mCalib->SetViewportSize(vec2(bounds.width, bounds.height));
    }
    
private:
    
    void OnTap(iHIDEvent* event)
    {
        mTapEv = dynamic_cast<iTouchEvent*>(event);
    }
    
    void OnKey(iHIDEvent* event)
    {
        SmartPtr<iKeyEvent> keyEvent = dynamic_cast<iKeyEvent*>(event);
        mKeyEvents->PushBack(keyEvent);
    }
    
    void OnFilteredFrameChanged(iHIDEvent* event)
    {
        SmartPtr<iFilteredFrameEvent> frameEv = dynamic_cast<iFilteredFrameEvent*>(event);
        mFilteredFrame = frameEv->GetTexture();
    }
    
    void OnFaceTrackerChanged(iHIDEvent* event)
    {
        SmartPtr<iFaceTrackerEvent> ev = dynamic_cast<iFaceTrackerEvent*>(event);
        mFaceResult = ev->GetResult();
        mDirty = true;
    }
    
    void OnAudioLevelChanged(iHIDEvent* event)
    {
        SmartPtr<iAVCaptureEvent> ev = dynamic_cast<iAVCaptureEvent*>(event);
        mAudioLevel = ev->GetAudioLevel();
    }
    
    void OnVideoFrameChanged(iHIDEvent* event)
    {
        SmartPtr<iAVCaptureEvent> ev = dynamic_cast<iAVCaptureEvent*>(event);
        SmartPtr<iVideoFrame> frame = ev->GetVideoFrame();
        piAssert(!frame.IsNull(), ;);
        
        SmartPtr<iBitmap> bitmap = frame->GetBitmap();
        int format = bitmap->GetPixelFormat()->GetName();
        mOriginFrame->SetFormat(format);
        mOriginFrame->SetWidth(bitmap->GetWidth());
        mOriginFrame->SetHeight(bitmap->GetHeight());
        
        piBindTexture(eTexTarget_2D, mOriginFrame->GetName());
        // NOTE: Set fixed internalFormat GL_RGBA
        piTexImage2D(eTexTarget_2D, 0, ePixelFormat_RGBA, bitmap, 0);
        piBindTexture(eTexTarget_2D, 0);
        
        mFilteredFrame = mOriginFrame;
        
        if (!mFaceTracker.IsNull())
        {
            int orient = ev->GetVideoOrient();
            
            SmartPtr<iFaceTrackerResult> result = mFaceTracker->Track(bitmap, orient);
            mFaceResult = result;
            mDirty = true;
        }
        
        mOriginBitmap = bitmap;
    }
};


iHID* CreateHID()
{
    return new HID();
}


NSPI_END()












