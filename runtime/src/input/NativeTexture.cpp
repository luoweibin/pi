/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.4.10   1.0     Create
 ******************************************************************************/
#include <pi/Input.h>

using namespace std;

NSPI_BEGIN()

class NativeTexture : public iNativeTexture
{
private:
    int32_t mName;
    int mFormat;
    int32_t mWidth;
    int32_t mHeight;
    int mOrient;
    
public:
    NativeTexture():
    mName(0), mFormat(ePixelFormat_Unknown), mWidth(0), mHeight(0), mOrient(eOrient_0)
    {
    }
    
    virtual ~NativeTexture()
    {
        piReleaseGraphicsObject(mName);
        mName = 0;
    }
    
    virtual int32_t GetName() const
    {
        return mName;
    }
    
    virtual void SetName(int32_t name)
    {
        piAssert(!IsReadonly(), ;);
        piRetainGraphicsObject(name);
        piReleaseGraphicsObject(mName);
        mName = name;
    }
    
    virtual int32_t GetWidth() const
    {
        return mWidth;
    }
    
    virtual void SetWidth(int32_t width)
    {
        piAssert(width >= 0, ;);
        piAssert(!IsReadonly(), ;);
        mWidth = width;
    }
    
    virtual int32_t GetHeight() const
    {
        return mHeight;
    }
    
    virtual void SetHeight(int32_t height)
    {
        piAssert(height > 0, ;);
        piAssert(!IsReadonly(), ;);
        mHeight = height;
    }
    
    virtual int GetFormat() const
    {
        return mFormat;
    }
    
    virtual void SetFormat(int format)
    {
        piAssert(!IsReadonly(), ;);
        mFormat = format;
    }
    
    virtual int GetOrient() const
    {
        return mOrient;
    }
    
    virtual void SetOrient(int orient)
    {
        piAssert(!IsReadonly(), ;);
        mOrient = orient;
    }
};


iNativeTexture* CreateNativeTexture()
{
    return new NativeTexture();
}


NSPI_END()






















