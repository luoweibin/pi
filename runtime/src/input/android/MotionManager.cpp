/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.8   1.0     Create
 ******************************************************************************/
#include <pi/Input.h>
#include "pi/Core.h"
#include <mutex>
#include <android/sensor.h>

using namespace std;

#define SENSOR_TYPE_ROTATION_VECTOR 11

static const int LOOPER_ID_USER = 1;
static int SENSOR_REFRESH_RATE_HZ = 100;
static int32_t SENSOR_REFRESH_PERIOD_US = int32_t(1000000 / SENSOR_REFRESH_RATE_HZ);
static const nspi::quat ATT_CONV = piglm::angleAxis(-90.0f, nspi::vec3(1, 0, 0));

NSPI_BEGIN()

class MotionManager : public iMotionManager
{
private:
    int32_t mStartCount;
    mutex mMutex;
    ASensorManager *mSensorManager;
    ASensorEventQueue *mSensorEventQueue;
    const ASensor *mAccelerometer;
    const ASensor *mRotationVector;
    const ASensor *mMagneticField;
    SmartPtr<iDeviceMotion> mDeviceMotion;

    int64_t mLastGyroscopeTimestamp;
public:
    MotionManager():
    mStartCount(0)
    ,mSensorManager(nullptr)
    ,mAccelerometer(nullptr)
    ,mRotationVector(nullptr)
    ,mMagneticField(nullptr)
    ,mSensorEventQueue(nullptr)
    ,mLastGyroscopeTimestamp(0)
    {
    }

    virtual ~MotionManager()
    {
        releaseSensor();
    }

    virtual void Start()
    {
        lock_guard<mutex> lock(mMutex);
        ++mStartCount;

        if (mStartCount == 1)
        {
            mSensorManager = ASensorManager_getInstance();
            mDeviceMotion = CreateDeviceMotion();
            ALooper *aLooper = ALooper_prepare(ALOOPER_PREPARE_ALLOW_NON_CALLBACKS);
            mSensorEventQueue = ASensorManager_createEventQueue(mSensorManager, aLooper, LOOPER_ID_USER,
                                                                            nullptr, nullptr);
            initAccelerometer();
            initRotationVector();
            initMagneticField();
        }
    }

    virtual void Stop()
    {
        lock_guard<mutex> lock(mMutex);
        --mStartCount;

        if (mStartCount <= 0)
        {
            releaseSensor();
        }
    }

    virtual bool IsActive() const
    {
        return nullptr != mSensorManager;
    }

    virtual bool IsAvailable() const
    {
        return nullptr != mSensorManager;
    }

    virtual iDeviceMotion* GetMotion() const
    {
        return mDeviceMotion;
    }

    virtual void OnUpdate()
    {
        piCheck(IsActive(), ;);

        ALooper_pollAll(0, NULL, NULL, NULL);
        if (mDeviceMotion.IsNull()) {
            return;
        }

        ASensorEvent event;
        while(ASensorEventQueue_getEvents(mSensorEventQueue, &event, 1) > 0) {
            if (ASENSOR_TYPE_ACCELEROMETER == event.type) {
                mDeviceMotion->SetUserAccel(vec3(event.acceleration.x, event.acceleration.y, event.acceleration.z));
            } else if (SENSOR_TYPE_ROTATION_VECTOR == event.type) {
                mDeviceMotion->SetRotationRate(vec3(0, 0, 0));

                if (0 != mLastGyroscopeTimestamp) {
                    float sinRho2 = event.data[0] * event.data[0] + event.data[1] * event.data[1] + event.data[2] * event.data[2];
                    float w = (sinRho2 < 1.f) ? sqrt(1.f - sinRho2) : 0.f;

                    quat quatRotator(w, event.data[0], event.data[1], event.data[2]);
                    quatRotator *= ATT_CONV;

                    float tmp = quatRotator.y;
                    quatRotator.y = quatRotator.z;
                    quatRotator.z = -tmp;

                    mDeviceMotion->SetAttitudeQuat(quatRotator);

                    vec3 atti = piglm::eulerAngles(quatRotator);
                    atti.x = fmodf(atti.x + 360.0f, 360.0f);
                    atti.y = fmodf(atti.y + 360.0f, 360.0f);
                    atti.z = fmodf(atti.z + 360.0f, 360.0f);
                    mDeviceMotion->SetAttitude(atti);
                }

                mLastGyroscopeTimestamp = event.timestamp;
            } else if (ASENSOR_TYPE_MAGNETIC_FIELD == event.type) {
                mDeviceMotion->SetMagField(vec3(event.magnetic.x, event.magnetic.y, event.magnetic.z));
            }
        }
    }

private:
    void initAccelerometer() {
        mAccelerometer = ASensorManager_getDefaultSensor(mSensorManager, ASENSOR_TYPE_ACCELEROMETER);
        if (!mAccelerometer) {
            return;
        }

        int ret = ASensorEventQueue_enableSensor(mSensorEventQueue, mAccelerometer);
        if (0 > ret) {
            return;
        }

        int delay = ASensor_getMinDelay(mAccelerometer);
        delay = (0 != delay ? (delay > SENSOR_REFRESH_PERIOD_US ? delay : SENSOR_REFRESH_PERIOD_US) : delay);

        ASensorEventQueue_setEventRate(mSensorEventQueue, mAccelerometer, delay);
    }

    void initRotationVector() {
        mLastGyroscopeTimestamp = 0;

        mRotationVector = ASensorManager_getDefaultSensor(mSensorManager, SENSOR_TYPE_ROTATION_VECTOR);
        if (!mRotationVector) {
            return;
        }

        int ret = ASensorEventQueue_enableSensor(mSensorEventQueue, mRotationVector);
        if (0 > ret) {
            return;
        }

        int delay = ASensor_getMinDelay(mRotationVector);
        delay = (0 != delay ? (delay > SENSOR_REFRESH_PERIOD_US ? delay : SENSOR_REFRESH_PERIOD_US) : delay);

        ASensorEventQueue_setEventRate(mSensorEventQueue, mRotationVector, delay);
    }

    void initMagneticField() {
        mMagneticField = ASensorManager_getDefaultSensor(mSensorManager, ASENSOR_TYPE_MAGNETIC_FIELD);
        if (!mMagneticField) {
            return;
        }

        int ret = ASensorEventQueue_enableSensor(mSensorEventQueue, mMagneticField);
        if (0 > ret) {
            return;
        }

        int delay = ASensor_getMinDelay(mMagneticField);
        delay = (0 != delay ? (delay > SENSOR_REFRESH_PERIOD_US ? delay : SENSOR_REFRESH_PERIOD_US) : delay);

        ASensorEventQueue_setEventRate(mSensorEventQueue, mMagneticField, delay);
    }

    void releaseSensor() {
        if (!mSensorManager) {
            return;
        }

        if (mAccelerometer) {
            ASensorEventQueue_disableSensor(mSensorEventQueue, mAccelerometer);
            mAccelerometer = nullptr;
        }

        if (mRotationVector) {
            ASensorEventQueue_disableSensor(mSensorEventQueue, mRotationVector);
            mRotationVector = nullptr;
            mLastGyroscopeTimestamp = 0;
        }

        if (mMagneticField) {
            ASensorEventQueue_disableSensor(mSensorEventQueue, mMagneticField);
            mMagneticField = nullptr;
        }

        ASensorManager_destroyEventQueue(mSensorManager, mSensorEventQueue);
        mSensorManager = nullptr;
        mSensorEventQueue = nullptr;

        mDeviceMotion = nullptr;
    }
};

iMotionManager* CreateMotionManager()
{
    return new MotionManager();
}

NSPI_END()



















