/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.22   1.0     Create
 ******************************************************************************/
#include <pi/input/impl/HIDEventImpl.h>

using namespace std;

NSPI_BEGIN()


class AVCaptureEvent : public HIDEventImpl<iAVCaptureEvent>
{
private:
    double mAudioLevel;
    SmartPtr<iVideoFrame> mVideoFrame;
    int mOrient;
    
public:
    AVCaptureEvent():
    mAudioLevel(-100), mOrient(eOrient_0)
    {
    }
    
    virtual ~AVCaptureEvent()
    {
    }
    
    virtual void SetAudioLevel(double level)
    {
        mAudioLevel = level;
    }
    
    virtual double GetAudioLevel() const
    {
        return mAudioLevel;
    }
    
    virtual iVideoFrame* GetVideoFrame() const
    {
        return mVideoFrame;
    }
    
    virtual void SetVideoFrame(iVideoFrame* frame)
    {
        piAssert(!IsReadonly(), ;);
        mVideoFrame = frame;
    }
    
    virtual int GetVideoOrient() const
    {
        return mOrient;
    }
    
    virtual void SetVideoOrient(int orient)
    {
        piAssert(!IsReadonly(), ;);
        mOrient = orient;
    }
};

iAVCaptureEvent* CreateAVCaptureEvent()
{
    return new AVCaptureEvent();
}




class FilteredFrameEvent : public HIDEventImpl<iFilteredFrameEvent>
{
private:
    SmartPtr<iNativeTexture> mTex;
    
public:
    FilteredFrameEvent()
    {
    }
    
    virtual ~FilteredFrameEvent()
    {
    }
    
    virtual void SetTexture(iNativeTexture* texture)
    {
        piAssert(texture != nullptr, ;);
        mTex = texture;
    }
    
    virtual iNativeTexture* GetTexture() const
    {
        return mTex;
    }
};


iFilteredFrameEvent* CreateFilteredFrameEvent()
{
    return new FilteredFrameEvent();
}


NSPI_END()




















