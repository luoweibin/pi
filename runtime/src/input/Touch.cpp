/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.22   1.0     Create
 ******************************************************************************/
#include <pi/input/impl/HIDEventImpl.h>

using namespace std;

NSPI_BEGIN()

class TouchEvent : public HIDEventImpl<iTouchEvent>
{
private:
    vector<vec3> mLocations;
    
public:
    TouchEvent()
    {
    }
    
    virtual ~TouchEvent()
    {
    }
    
    virtual int32_t GetCount() const
    {
        return (int32_t)mLocations.size();
    }
    
    virtual vec3 GetLocation() const
    {
        return !mLocations.empty() ? mLocations[0] : vec3();
    }
    
    virtual void AddLocation(const vec3& point)
    {
        piAssert(!IsReadonly(), ;);
        mLocations.push_back(point);
    }
    
    virtual vec3 GetLocationOfTouch(int32_t index) const
    {
        piAssert(index >= 0 && index < mLocations.size(), vec3());
        return mLocations[index];
    }
    
    virtual string ToString() const
    {
        string content = piFormat("TouchEvent[Tap], Count:%d, Locations:", GetCount());
        
        for (int32_t i = 0; i < mLocations.size(); ++i)
        {
            if (i > 0)
            {
                content += ",";
            }
            
            content += piToString(mLocations[i]);
        }
        
        return content;
    }
};


iTouchEvent* CreateTouchEvent()
{
    return new TouchEvent();
}


NSPI_END()



























