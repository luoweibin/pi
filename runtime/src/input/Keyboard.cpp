/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.22   1.0     Create
 ******************************************************************************/
#include <pi/input/impl/HIDEventImpl.h>

using namespace std;

NSPI_BEGIN()

class KeyEvent : public HIDEventImpl<iKeyEvent>
{
private:
    int mCode;
    string mText;
    
public:
    KeyEvent():
    mCode(-1)
    {
    }
    
    virtual ~KeyEvent()
    {
    }
    
    virtual int GetCode() const
    {
        return mCode;
    }
    
    virtual void SetCode(int code)
    {
        piAssert(!IsReadonly(), ;);
        mCode = code;
    }
    
    virtual void SetText(const string& text)
    {
        piAssert(!IsReadonly(), ;);
        mText = text;
    }
    
    virtual string GetText() const
    {
        return mText;
    }
};


iKeyEvent* CreateKeyEvent()
{
    return new KeyEvent();
}



NSPI_END()



























