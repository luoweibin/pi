/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.22   1.0     Create
 ******************************************************************************/
#include <pi/input/impl/HIDEventImpl.h>

using namespace std;

NSPI_BEGIN()


class FaceTrackerEvent : public HIDEventImpl<iFaceTrackerEvent>
{
private:
    SmartPtr<iFaceTrackerResult> mResult;
    
public:
    FaceTrackerEvent()
    {
    }
    
    virtual ~FaceTrackerEvent()
    {
        
    }
    
    virtual void SetResult(iFaceTrackerResult* result)
    {
        piAssert(!IsReadonly(), ;);
        piAssert(result != nullptr, ;);
        mResult = result;
    }
    
    virtual iFaceTrackerResult* GetResult() const
    {
        return mResult;
    }
    
};


iFaceTrackerEvent* CreateFaceTrackerEvent()
{
    return new FaceTrackerEvent();
}


NSPI_END()

















