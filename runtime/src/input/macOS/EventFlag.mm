/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.22   1.0     Create
 ******************************************************************************/
#import <pi/Input.h>
#import <AppKit/AppKit.h>

using namespace std;

NSPI_BEGIN()


int piMapEventFlags(int systemFlags)
{
    int32_t flags = 0;
    
    if (piFlagIs(systemFlags, NSEventModifierFlagCommand))
    {
        piFlagOn(flags, eEventFlag_WindowCommand);
    }
    
    return flags;
}



NSPI_END()





















