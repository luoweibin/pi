/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.22   1.0     Create
 ******************************************************************************/
#import <pi/Input.h>

using namespace std;

NSPI_BEGIN()

class MotionManager : public iMotionManager
{
public:
    MotionManager()
    {
    }
    
    virtual ~MotionManager()
    {
    }
    
    virtual void Start()
    {
    }
    
    virtual void Stop()
    {
    }
    
    virtual bool IsActive() const
    {
        return false;
    }
    
    virtual bool IsAvailable() const
    {
        return false;
    }
    
    virtual iDeviceMotion* GetMotion() const
    {
        return nullptr;
    }
    
    virtual void OnUpdate()
    {
    }
};

iMotionManager* CreateMotionManager()
{
    return new MotionManager();
}

NSPI_END()




















