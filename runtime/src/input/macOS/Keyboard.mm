/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.22   1.0     Create
 ******************************************************************************/
#import <pi/Input.h>
#import <Carbon/Carbon.h>

using namespace std;

NSPI_BEGIN()

int piMapKeyCode(int systemCode)
{
    switch (systemCode)
    {
        case kVK_ANSI_A:
            return eKey_A;
        case kVK_ANSI_S:
            return eKey_S;
        case kVK_ANSI_D:
            return eKey_D;
        case kVK_ANSI_F:
            return eKey_F;
        case kVK_ANSI_H:
            return eKey_H;
        case kVK_ANSI_G:
            return eKey_G;
        case kVK_ANSI_Z:
            return eKey_Z;
        case kVK_ANSI_X:
            return eKey_X;
        case kVK_ANSI_C:
            return eKey_C;
        case kVK_ANSI_V:
            return eKey_V;
        case kVK_ANSI_B:
            return eKey_B;
        case kVK_ANSI_Q:
            return eKey_Q;
        case kVK_ANSI_W:
            return eKey_W;
        case kVK_ANSI_E:
            return eKey_E;
        case kVK_ANSI_R:
            return eKey_R;
        case kVK_ANSI_Y:
            return eKey_Y;
        case kVK_ANSI_T:
            return eKey_T;
        case kVK_ANSI_1:
            return eKey_1;
        case kVK_ANSI_2:
            return eKey_2;
        case kVK_ANSI_3:
            return eKey_3;
        case kVK_ANSI_4:
            return eKey_4;
        case kVK_ANSI_5:
            return eKey_5;
        case kVK_ANSI_6:
            return eKey_6;
        case kVK_ANSI_7:
            return eKey_7;
        case kVK_ANSI_8:
            return eKey_8;
        case kVK_ANSI_9:
            return eKey_9;
        case kVK_ANSI_0:
            return eKey_0;
        case kVK_ANSI_Equal:
            return eKey_Equals;
        case kVK_ANSI_Minus:
            return eKey_Minus;
        case kVK_ANSI_RightBracket:
            return eKey_RightBracket;
        case kVK_ANSI_O:
            return eKey_O;
        case kVK_ANSI_U:
            return eKey_U;
        case kVK_ANSI_LeftBracket:
            return eKey_LeftBracket;
        case kVK_ANSI_I:
            return eKey_I;
        case kVK_ANSI_P:
            return eKey_P;
        case kVK_ANSI_L:
            return eKey_L;
        case kVK_ANSI_J:
            return eKey_J;
        case kVK_ANSI_Quote:
            return eKey_Quote;
        case kVK_ANSI_K:
            return eKey_K;
        case kVK_ANSI_Semicolon:
            return eKey_Semicolon;
        case kVK_ANSI_Backslash:
            return eKey_Backslash;
        case kVK_ANSI_Comma:
            return eKey_Comma;
        case kVK_ANSI_Slash:
            return eKey_Slash;
        case kVK_ANSI_N:
            return eKey_N;
        case kVK_ANSI_M:
            return eKey_M;
        case kVK_ANSI_Period:
            return eKey_Period;
        case kVK_ANSI_Grave:
            return eKey_Grave;
        case kVK_ANSI_Keypad0:
            return eKey_Keypad0;
        case kVK_ANSI_Keypad1:
            return eKey_Keypad1;
        case kVK_ANSI_Keypad2:
            return eKey_Keypad2;
        case kVK_ANSI_Keypad3:
            return eKey_Keypad3;
        case kVK_ANSI_Keypad4:
            return eKey_Keypad4;
        case kVK_ANSI_Keypad5:
            return eKey_Keypad5;
        case kVK_ANSI_Keypad6:
            return eKey_Keypad6;
        case kVK_ANSI_Keypad7:
            return eKey_Keypad7;
        case kVK_ANSI_Keypad8:
            return eKey_Keypad8;
        case kVK_ANSI_Keypad9:
            return eKey_Keypad9;
        case kVK_ANSI_KeypadDecimal:
            return eKey_KeypadDecimal;
        case kVK_ANSI_KeypadMultiply:
            return eKey_KeypadMultiply;
        case kVK_ANSI_KeypadPlus:
            return eKey_KeypadPlus;
        case kVK_ANSI_KeypadClear:
            return eKey_KeypadClear;
        case kVK_ANSI_KeypadDivide:
            return eKey_KeypadDivide;
        case kVK_ANSI_KeypadEnter:
            return eKey_KeypadEnter;
        case kVK_ANSI_KeypadMinus:
            return eKey_KeypadMinus;
        case kVK_ANSI_KeypadEquals:
            return eKey_KeypadEquals;
        case kVK_Return:
            return eKey_Return;
        case kVK_Tab:
            return eKey_Tab;
        case kVK_Space:
            return eKey_Space;
        case kVK_Delete:
            return eKey_Delete;
        case kVK_Escape:
            return eKey_Escape;
        case kVK_Command:
            return eKey_Command;
        case kVK_Shift:
            return eKey_Shift;
        case kVK_CapsLock:
            return eKey_CapsLock;
        case kVK_Option:
            return eKey_Option;
        case kVK_Control:
            return eKey_Control;
        case kVK_RightShift:
            return eKey_RightShift;
        case kVK_RightOption:
            return eKey_RightOption;
        case kVK_RightControl:
            return eKey_RightControl;
        case kVK_Function:
            return eKey_Function;
        case kVK_F17:
            return eKey_F17;
        case kVK_VolumeUp:
            return eKey_VolumeUp;
        case kVK_VolumeDown:
            return eKey_VolumeDown;
        case kVK_Mute:
            return eKey_Mute;
        case kVK_F18:
            return eKey_F18;
        case kVK_F19:
            return eKey_F19;
        case kVK_F20:
            return eKey_F20;
        case kVK_F5:
            return eKey_F5;
        case kVK_F6:
            return eKey_F6;
        case kVK_F7:
            return eKey_F7;
        case kVK_F8:
            return eKey_F8;
        case kVK_F9:
            return eKey_F9;
        case kVK_F11:
            return eKey_F11;
        case kVK_F13:
            return eKey_F13;
        case kVK_F16:
            return eKey_F16;
        case kVK_F14:
            return eKey_F14;
        case kVK_F10:
            return eKey_F10;
        case kVK_F12:
            return eKey_F12;
        case kVK_F15:
            return eKey_F15;
        case kVK_Help:
            return eKey_Help;
        case kVK_Home:
            return eKey_Home;
        case kVK_PageUp:
            return eKey_PageUp;
        case kVK_PageDown:
            return eKey_PageDown;
        case kVK_ForwardDelete:
            return eKey_ForwardDelete;
        case kVK_F4:
            return eKey_F4;
        case kVK_End:
            return eKey_End;
        case kVK_F2:
            return eKey_F2;
        case kVK_F1:
            return eKey_F1;
        case kVK_LeftArrow:
            return eKey_LeftArrow;
        case kVK_RightArrow:
            return eKey_RightArrow;
        case kVK_DownArrow:
            return eKey_DownArrow;
        case kVK_UpArrow:
            return eKey_UpArrow;
        case kVK_ISO_Section:
            return eKey_ISO_Section;
        case kVK_JIS_Yen:
            return eKey_JIS_Yen;
        case kVK_JIS_Underscore:
            return eKey_JIS_Underscore;
        case kVK_JIS_KeypadComma:
            return eKey_JIS_KeypadComma;
        case kVK_JIS_Eisu:
            return eKey_JIS_Eisu;
        case kVK_JIS_Kana:
            return eKey_JIS_Kana;
        default:
            return eKey_Unknown;
    }
}





NSPI_END()




















