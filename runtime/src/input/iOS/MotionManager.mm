/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.22   1.0     Create
 ******************************************************************************/
#import <pi/Input.h>
#import <CoreMotion/CoreMotion.h>
#import <mutex>

using namespace std;

NSPI_BEGIN()

static int ToPIAccuracy(CMMagneticFieldCalibrationAccuracy value)
{
    switch (value)
    {
        case CMMagneticFieldCalibrationAccuracyLow:
            return eAccuracy_Low;
        case CMMagneticFieldCalibrationAccuracyMedium:
            return eAccuracy_Medium;
        case CMMagneticFieldCalibrationAccuracyHigh:
            return eAccuracy_High;
        case CMMagneticFieldCalibrationAccuracyUncalibrated:
        default:
            return eAccuracy_None;
    }
}

static quat g_AttConv(0.707107, 0.000000, 0.000000, 0.707107);
static mat4 g_CoordConv(1.000000,  0.000000, 0.000000, 0.000000,
                        0.000000, -0.000000, 1.000000, 0.000000,
                        0.000000, -1.000000, 0.000000, 0.000000,
                        0.000000,  0.000000, 0.000000, 1.000000);

static vec3 RadianToDegree(const vec3& v)
{
    return vec3(v.x / M_PI * 180, v.y / M_PI * 180, v.z / M_PI * 180);
}

static vec3 NormalizeDegree(const vec3& v)
{
    return vec3(fmod(v.x + 360, 360), fmod(v.y + 360, 360), fmod(v.z + 360, 360));
}

static vec3 ToPICoord(const vec3& v)
{
    vec4 p(v.x, v.y, v.z, 1);
    p = g_CoordConv * p;
    return vec3(p);
}

class MotionManager : public iMotionManager
{
private:
    mutable SmartPtr<iDeviceMotion> mMotion;
    
    CMMotionManager* mManager;
    int32_t mStartCount;
    mutex mMutex;
    
public:
    MotionManager():
    mStartCount(0)
    {
        mManager = [[CMMotionManager alloc] init];
        
        g_AttConv = piglm::angleAxis(-90.0f, vec3(1, 0, 0));
    }
    
    virtual ~MotionManager()
    {
    }
    
    virtual void Start()
    {
        lock_guard<mutex> lock(mMutex);
        ++mStartCount;
        
        if (mStartCount == 1)
        {
            [mManager startDeviceMotionUpdatesUsingReferenceFrame:CMAttitudeReferenceFrameXArbitraryCorrectedZVertical];
        }
    }
    
    virtual void Stop()
    {
        lock_guard<mutex> lock(mMutex);
        --mStartCount;
        
        if (mStartCount <= 0)
        {
            [mManager stopDeviceMotionUpdates];
        }
    }
    
    virtual bool IsActive() const
    {
        return mManager.deviceMotionActive;
    }
    
    virtual bool IsAvailable() const
    {
        return mManager.deviceMotionAvailable;
    }
    
    virtual iDeviceMotion* GetMotion() const
    {
        return mMotion;
    }
    
    void MakePositive (vec3& euler)
    {
        const float negativeFlip = -0.0001F;
        const float positiveFlip = (360) - 0.0001F;
        
        if (euler.x < negativeFlip)
            euler.x += 360;
        else if (euler.x > positiveFlip)
            euler.x -= 360;
        
        if (euler.y < negativeFlip)
            euler.y += 360;
        else if (euler.y > positiveFlip)
            euler.y -= 360;
        
        if (euler.z < negativeFlip)
            euler.z += 360;
        else if (euler.z > positiveFlip)
            euler.z -= 360;
    }
    
    /// This is YXZ euler conversion
    bool MatrixToEuler (const mat3& matrix, vec3& v)
    {
        // from http://www.geometrictools.com/Documentation/EulerAngles.pdf
        // YXZ order
        if ( matrix[1].z < 0.999F ) // some fudge for imprecision
        {
            if ( matrix[1].z > -0.999F ) // some fudge for imprecision
            {
                v.x = piglm::degrees( asin(-matrix[1].z ) );
                v.y = piglm::degrees( atan2(matrix[0].z, matrix[2].z));
                v.z = piglm::degrees( atan2(matrix[1].x, matrix[1].y));

                return true;
            }
            else
            {
                // WARNING.  Not unique.  YA - ZA = atan2(r01,r00)
                v.x = piglm::degrees( 3.1415926 * 0.5F);
                v.y = piglm::degrees( atan2(matrix[0].y, matrix[0].x));
                v.z = piglm::degrees( 0.0F);
                
                return false;
            }
        }
        else
        {
            // WARNING.  Not unique.  YA + ZA = atan2(-r01,r00)
            v.x = piglm::degrees( -3.1415926 * 0.5F);
            v.y = piglm::degrees( atan2(-matrix[0].y ,matrix[0].x ));
            v.z = piglm::degrees( 0.0F);

            return false;
        }
    }
    
    virtual void OnUpdate()
    {
        piCheck(IsActive(), ;);
        
        SmartPtr<iDeviceMotion> m = CreateDeviceMotion();
        
        CMDeviceMotion* motion = mManager.deviceMotion;
        piCheck(motion != nil, ;);
        
        quat attiQuat(motion.attitude.quaternion.w,
                      motion.attitude.quaternion.x,
                      motion.attitude.quaternion.y,
                      motion.attitude.quaternion.z);
        attiQuat *= g_AttConv;
        
        float Temp = attiQuat.y;
        attiQuat.y = attiQuat.z;
        attiQuat.z = -Temp;
        
        m->SetAttitudeQuat(attiQuat);
        
        vec3 gravity = vec3(motion.gravity.x, motion.gravity.y, motion.gravity.z);
        m->SetGravity(ToPICoord(gravity));

        vec3 atti;
        /* 
        // This magic code make me sick, use quaternion to eulerangle instead!
        atti = RadianToDegree(vec3(motion.attitude.pitch,
                                        motion.attitude.roll,
                                        motion.attitude.yaw));

        
        vec3 g = piglm::normalize(gravity);
        vec3 up(0, 0, 1);
        float cosTheta = piglm::dot(g, up);
        if (cosTheta > 0)
        {
            atti.x = 90 - atti.x;
        }
        else
        {
            atti.x -= 90;
        }
        */
        
        // This make atti as y,x,z but atti should be use as x,y,z
        
        //MatrixToEuler(piglm::toMat3(attiQuat),atti);
        
        atti = piglm::eulerAngles(attiQuat);
        
        //atti = vec3( piglm::roll(attiQuat), piglm::yaw(attiQuat), piglm::pitch(attiQuat));
        atti = NormalizeDegree(atti);
        m->SetAttitude(atti);
    
        vec3 magField = ToPICoord(vec3(motion.magneticField.field.x,
                                       motion.magneticField.field.y,
                                       motion.magneticField.field.z));
        m->SetMagField(magField);
        
        m->SetMagAccuracy(ToPIAccuracy(motion.magneticField.accuracy));
        
        vec3 userAccel = ToPICoord(vec3(motion.userAcceleration.x,
                                        motion.userAcceleration.y,
                                        motion.userAcceleration.z));
        m->SetUserAccel(userAccel);
        
        vec3 rate = ToPICoord(vec3(motion.rotationRate.x,
                                   motion.rotationRate.y,
                                   motion.rotationRate.z));
        rate = RadianToDegree(rate);
        m->SetRotationRate(rate);
        
        mMotion = m;
    }
};

iMotionManager* CreateMotionManager()
{
    return new MotionManager();
}

NSPI_END()




















