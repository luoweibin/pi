/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2015.4.20   0.1     Create
 ******************************************************************************/
#include <pi/Crypto.h>

#if defined(PI_APPLE)

#include <CommonCrypto/CommonDigest.h>
#ifdef MD5_DIGEST_LENGTH
#undef MD5_DIGEST_LENGTH
#endif

#define MD5_Init            CC_MD5_Init
#define MD5_Update          CC_MD5_Update
#define MD5_Final           CC_MD5_Final
#define MD5_DIGEST_LENGTH   CC_MD5_DIGEST_LENGTH
#define MD5_CTX             CC_MD5_CTX

#else

#include <openssl/md5.h>

#endif

using namespace std;


NSPI_BEGIN()

class MD5 : public iHash
{
private:
    MD5_CTX mCtx;
    
public:
    MD5()
    {
        MD5_Init(&mCtx);
    }
    
    virtual ~MD5()
    {
    }
    
    virtual void Update(const void* data, int64_t size)
    {
        MD5_Update(&mCtx, data, (int32_t)size);
    }
    
    virtual int64_t Final(void* buffer, int64_t size)
    {
        MD5_Final((uint8_t*)buffer, &mCtx);
        return GetBytes();
    }
    
    virtual std::string FinalHex()
    {
        uint8_t digist[16];
        MD5_Final(digist, &mCtx);
        return piBinToHexStringUTF8(digist, 16, false);
    }
    
    virtual void Reset()
    {
        MD5_Init(&mCtx);
    }
    
    virtual int GetName() const
    {
        return eHash_MD5;
    }
    
    virtual int64_t GetBytes() const
    {
        return 16;
    }
};



iHash* CreateMD5()
{
    return new MD5();
}



NSPI_END()


















































