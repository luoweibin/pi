/*******************************************************************************
 See copyright notice in LICENSE.

 History:
 CYY     2017.4.13   1.0     Create
 ******************************************************************************/
#include "pi/Crypto.h"

using namespace std;

NSPI_BEGIN()

iStream* CreateBase64EncoderStream(iStream* source);
iStream* CreateBase64DecoderStream(iStream* source);
iStream* CreateBlowfishEncryptStream(iStream* source, iMemory* key);
iStream* CreateBlowfishDecryptStream(iStream* source, iMemory* key);

iStream* CreateCipherStream(iStream* source, int name, iMemory* key, iMemory* iv)
{
    switch (name)
    {
        case eEncoder_Base64:
            return CreateBase64EncoderStream(source);
        case eDecoder_Base64:
            return CreateBase64DecoderStream(source);
        case eEncoder_Blowfish:
            return CreateBlowfishEncryptStream(source, key);
        case eDecoder_Blowfish:
            return CreateBlowfishDecryptStream(source, key);
        default:
            return nullptr;
    }
}

NSPI_END()













