/*******************************************************************************
 See copyright notice in LICENSE.

 History:
 CYY     2017.4.13   1.0     Create
 ******************************************************************************/

#include <pi/Crypto.h>

#if defined(PI_APPLE)
#   include <CommonCrypto/CommonCrypto.h>
#else
#   include <openssl/blowfish.h>
#endif

using namespace std;

NSPI_BEGIN()

// Buffer Size
#define BS 512

// Block Size
#define kBlockSize 8


/**
 * ---------------------------------------------------------------------------------------
 * Decrypt
 */


class BlowfishDecryptStream : public cStreamImpl<iStream>
{
private:
    
#if defined(PI_APPLE)
    CCCryptorRef mCryptor;
#elif defined(PI_ANDROID)
    BF_KEY mBFKey;
#endif
    
    SmartPtr<iStream> mSrc;
    
    char mBuf[BS];
    
    int64_t mBOff; // offset of mBuf in stream
    int64_t mBSize; // size of mBuf
    int64_t mBStart;
    int64_t mPadding;
    
public:
    BlowfishDecryptStream(iStream* source):
    mBOff(0), mBSize(0), mSrc(source), mBStart(0), mPadding(0)
#if defined(PI_APPLE)
        ,mCryptor(nullptr)
#endif
    {
    }

    virtual ~BlowfishDecryptStream()
    {
#if defined(PI_APPLE)
        if (mCryptor != nullptr)
        {
            CCCryptorRelease(mCryptor);
            mCryptor = nullptr;
        }
#endif
    }
    
    bool Init(iMemory* key)
    {
        piAssert(mSrc->Seek(0, eSeek_Set) >= 0, false);
        
#if defined(PI_APPLE)
        piAssert(CCCryptorCreate(kCCDecrypt,
                                 kCCAlgorithmBlowfish,
                                 kCCOptionECBMode,
                                 key->Ptr(),
                                 32,
                                 nullptr,
                                 &mCryptor) == kCCSuccess, false);
        
        piAssert(mSrc->Seek(-512, eSeek_End) > 0, false);
        piAssert(ReadBlock(), false);
        
        int len = mBuf[BS - 1];
        if (len > 0)
        {
            for (auto i = 0; i < len; ++i)
            {
                int c = mBuf[BS - i - 1];
                if (c != len)
                {
                    break;
                }
            }
            
            mPadding = len;
        }
        
        return true;
#elif defined(PI_ANDROID)
        BF_set_key(&mBFKey, key->Size(), (const unsigned char*)key->Ptr());
        return true;
#endif
    }
    
    virtual int64_t GetSize() const
    {
        return mSrc->GetSize();
    }
    
    virtual int64_t Seek(int64_t offset, int seek)
    {
        int64_t off = 0;
        switch (seek)
        {
            case eSeek_Set:
                off = piMin(offset, GetSize());
                break;
            case eSeek_Current:
                off = piMin(offset + mlldOffset, GetSize());
                break;
            case eSeek_End:
                off = GetSize() + offset - mPadding;
                break;
            default:
                return -1;
        }
        mlldOffset = off;
        
        if (off < mBStart || off >= mBStart + mBSize)
        {
            mBSize = 0;
            mBStart = off - off % BS;
            mSrc->Seek(mBStart, eSeek_Set);

#if defined(PI_APPLE)
            piAssert(CCCryptorReset(mCryptor, nullptr) == kCCSuccess, -1);
#endif
        }
        
        mBOff = mlldOffset - mBStart;

        return off;
    }
    
    virtual int64_t GetOffset() const
    {
        return mlldOffset;
    }
    
    virtual int64_t Write(const void* pData, int64_t size)
    {
        return -1;
    }

    virtual int64_t Read(void* buffer, int64_t size)
    {
        int64_t total = 0;
        
        char* p = (char*)buffer;
        
        while (total < size)
        {
            if (mBOff >= mBSize && !ReadBlock())
            {
                break;
            }
            
            mBOff %= BS;
            // copy data into buffer
            int64_t cpSize = piMin(mBSize - mBOff, size - total);
            
            memcpy(p + total, mBuf + mBOff, (size_t)cpSize);
            
            mBOff += cpSize;
            total += cpSize;
        }
        
        mlldOffset += total;
        return total;
    }
    
    virtual string GetUri() const
    {
        return mSrc->GetUri();
    }
    
private:
    
    // mSrc must seek to the proper position before calling this method
    bool ReadBlock()
    {
        char buf[sizeof(mBuf)];
        auto read = mSrc->Read(buf, sizeof(buf));
        if (read < 0)
        {
            return false;
        }
        else if (read == 0)
        {
            return read > 0;
        }
        
#if defined(PI_APPLE)
        size_t ret = 0;
        auto status = CCCryptorUpdate(mCryptor, buf, (size_t)read, mBuf, sizeof(mBuf), &ret);
        piCheck(status == kCCSuccess, false);

        if (mSrc->End())
        {
            ret -= mPadding;
        }
        mBSize = ret;
        return true;
#elif defined(PI_ANDROID)
        for (int i = 0; i < read; i += 8) {
            BF_ecb_encrypt((const unsigned char *) (buf + i), (unsigned char *) (mBuf + i),
                           &mBFKey,
                           BF_DECRYPT);
        }

        mBSize = read;
        return true;
#endif
    }
};

iStream* CreateBlowfishDecryptStream(iStream* source, iMemory* key)
{
    SmartPtr<BlowfishDecryptStream> s = new BlowfishDecryptStream(source);
    if (s->Init(key))
    {
        return s.PtrAndSetNull();
    }
    else
    {
        return nullptr;
    }
}



/**
 * ---------------------------------------------------------------------------------------
 * Encrypt
 */


class BlowfishEncryptStream : public cStreamImpl<iStream>
{
private:
    
#if defined(PI_APPLE)
    CCCryptorRef mCryptor;
#elif defined(PI_ANDROID)
    BF_KEY mBFKey;
#endif
    
    SmartPtr<iStream> mSrc;
    
    char mBuf[BS + kBlockSize];
    int64_t mBSize; // size of mBuf
    int64_t mBStart;
    
public:
    BlowfishEncryptStream(iStream* source):
    mBSize(0), mSrc(source), mBStart(0)
#if defined(PI_APPLE)
        ,mCryptor(nullptr)
#endif
    {
    }
    
    virtual ~BlowfishEncryptStream()
    {
        Flush();

#if defined(PI_APPLE)
        if (mCryptor != nullptr)
        {
            CCCryptorRelease(mCryptor);
            mCryptor = nullptr;
        }
#endif
    }
    
    bool Init(iMemory* key)
    {
#if defined(PI_APPLE)
        auto status = CCCryptorCreate(kCCEncrypt,
                                      kCCAlgorithmBlowfish,
                                      kCCOptionECBMode,
                                      key->Ptr(),
                                      32,
                                      nullptr,
                                      &mCryptor);
        return status == kCCSuccess;
#elif defined(PI_ANDROID)
        BF_set_key(&mBFKey, key->Size(), (const unsigned char *) key->Ptr());
        return true;
#endif
    }
    
    virtual int64_t GetSize() const
    {
        return mSrc->GetSize();
    }
    
    virtual int64_t Seek(int64_t offset, int seek)
    {
        return -1;
    }
    
    virtual int64_t GetOffset() const
    {
        return mlldOffset;
    }
    
    virtual int64_t Write(const void* pData, int64_t size)
    {
        piCheck(pData != nullptr, 0);
        piCheck(size > 0, 0);
        
        const char* p = (const char*)pData;
        
        int64_t total = 0;
        while (total < size)
        {
            int64_t wSize = piMin(size, BS - mBSize);
            memcpy(mBuf + mBSize, p + total, (size_t)wSize);
            
            mBSize += wSize;
            total += wSize;
            mlldOffset += wSize;

            if (mBSize == BS && WriteBlock() != BS)
            {
                break;
            }
        }
        
        return total;
    }
    
    virtual int64_t Read(void* buffer, int64_t size)
    {
        PILOGE(PI_TAG, "Seek is unsupported in blowfinsh encoder stream. Path:%s", GetUri().c_str());
        return -1;
    }
    
    virtual string GetUri() const
    {
        return mSrc->GetUri();
    }
    
    virtual bool Flush()
    {
        piCheck(mBSize > 0, true);
        
        piCheck(WriteBlock() >= 0, false);
        
        return mSrc->Flush();
    }
    
private:
    
    int64_t WriteBlock()
    {
        piCheck(mBSize > 0, 0);
        
        auto align = mBSize;
        auto len = 0;
        if (align % kBlockSize != 0)
        {
            len = kBlockSize - align % kBlockSize;
        }
        if (len > 0)
        {
            for (auto i = 0; i < len; ++i)
            {
                mBuf[align + i] = len;
            }
            align += len;
        }

        size_t ret = 0;
        char buf[BS];
#if defined(PI_APPLE)
        auto status = CCCryptorUpdate(mCryptor, mBuf, (size_t)align, buf, sizeof(mBuf), &ret);
        piAssert(status == kCCSuccess, -1);

        auto write = mSrc->Write(buf, ret);
#elif defined(PI_ANDROID)
        for (int i = 0; i < align; i += 8) {
            BF_ecb_encrypt((const unsigned char *) (mBuf + i), (unsigned char *) (buf + i), &mBFKey,
                           BF_ENCRYPT);
        }

        auto write = mSrc->Write(buf, align);
#endif

        mBSize = 0;
        
        return write;
    }
};


iStream* CreateBlowfishEncryptStream(iStream* source, iMemory* key)
{
    piAssert(source != nullptr, nullptr);
    
    SmartPtr<BlowfishEncryptStream> s = new BlowfishEncryptStream(source);
    if (s->Init(key))
    {
        return s.PtrAndSetNull();
    }
    else
    {
        return nullptr;
    }
}


NSPI_END()


















