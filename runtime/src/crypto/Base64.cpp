/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2015.3.29   0.1     Create
 ******************************************************************************/
#include <pi/Crypto.h>

using namespace std;

NSPI_BEGIN()

static const char g_Basic64[] =
"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

class Base64EncoderStream : public cStreamImpl<iStream>
{
private:
    uint8_t mBuffer[3];
    int64_t mSize;
    
    SmartPtr<iStream> mSrc;
    
public:
    Base64EncoderStream(iStream* source):
    mSize(0), mSrc(source)
    {
    }
    
    virtual ~Base64EncoderStream()
    {
    }
    
    virtual int64_t GetSize() const
    {
        return mSrc->GetSize();
    }
    
    virtual int64_t Seek(int64_t offset, int seek)
    {
        PILOGE(PI_TAG, "Seek is unsupported in base encoder stream. Path:%s", GetUri().c_str());
        return -1;
    }
    
    virtual int64_t GetOffset() const
    {
        return mlldOffset;
    }
    
    virtual int64_t Write(const void* data, int64_t size)
    {
        piCheck(data != nullptr, 0);
        piCheck(size > 0, 0);
        
        const uint8_t* s = (const uint8_t*)data;
        int64_t srcSize = size;
        
        int64_t total = 0;
        
        if (mSize > 0)
        {
            int64_t left = 3 - mSize;
            for (int64_t i = 0; i < left; ++i)
            {
                mBuffer[mSize + i] = s[i];
            }
            mSize += left;
            srcSize -= left;
            s += left;
            
            if (mSize == 3)
            {
                char buf[4];
                Encode(mBuffer, buf);
                mSize = 0;
                
                auto w = mSrc->Write(buf, 4);
                if (w > 0)
                {
                    total += 3;
                }
                else
                {
                    return total;
                }
            }
        }
        
        int64_t sizeToWrite = size - size % 3;
        
        for (int64_t i = 0; i < sizeToWrite; i += 3)
        {
            char buf[4];
            Encode(s + i * 3, buf);
            
            auto w = mSrc->Write(buf, 4);
            if (w > 0)
            {
                total += 3;
            }
            else
            {
                return total;
            }
        }
        
        int64_t left = size - sizeToWrite;
        if (left > 0)
        {
            memcpy(mBuffer, s + sizeToWrite, (size_t)left);
            mSize = left;
        }
        
        return total;
    }
    
    virtual int64_t Read(void* buffer, int64_t size)
    {
        return -1;
    }
    
    virtual string GetUri() const
    {
        return mSrc->GetUri();
    }
    
    virtual bool Flush()
    {
        piCheck(mSize > 0, true);
        
        char buf[4];
        EncodeWithPadding(mBuffer, mSize, buf);
        piCheck(mSrc->Write(buf, 4) == 4, false);
        
        return mSrc->Flush();
    }
    
private:
    void Encode(const uint8_t* src, char* dest)
    {
        *dest++ = g_Basic64[(src[0] >> 2) & 0x3F];
        *dest++ = g_Basic64[((src[0] & 0x3) << 4) |
                        ((int) (src[1] & 0xF0) >> 4)];
        *dest++ = g_Basic64[((src[1] & 0xF) << 2) |
                        ((int) (src[2] & 0xC0) >> 6)];
        *dest++ = g_Basic64[src[2] & 0x3F];
    }
    
    void EncodeWithPadding(const uint8_t* src, int64_t srcSize, char* dest)
    {
        *dest++ = g_Basic64[(src[0] >> 2) & 0x3F];
        if (srcSize == 1)
        {
            *dest++ = g_Basic64[((src[0] & 0x3) << 4)];
            *dest++ = '=';
        }
        else
        {
            *dest++ = g_Basic64[((src[0] & 0x3) << 4) |
                            ((int) (src[1] & 0xF0) >> 4)];
            *dest++ = g_Basic64[((src[1] & 0xF) << 2)];
        }
        *dest++ = '=';
    }
};


//==============================================================================

/* aaaack but it's fast and const should make it shared text page. */
static const unsigned char pr2six[256] =
{
    /* ASCII table */
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 62, 64, 64, 64, 63,
    52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 64, 64, 64, 64, 64, 64,
    64,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
    15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 64, 64, 64, 64, 64,
    64, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
    41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
    64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64
};

class Base64DecoderStream : public cStreamImpl<iStream>
{
private:
    uint8_t mBuf[4];
    int64_t mBSize;
    int64_t mBStart;
    SmartPtr<iStream> mSrc;
    int64_t mPadding;
    int64_t mBOff;
    
    int64_t mSize;
    
public:
    Base64DecoderStream(iStream* src):
    mSize(0), mSrc(src), mPadding(0), mBSize(0), mBStart(0), mBOff(0)
    {
    }
    
    virtual ~Base64DecoderStream()
    {
    }
    
    bool Init()
    {
        piAssert(mSrc->Seek(-4, eSeek_Set) >= 0, false);

        char buf[4];
        piAssert(mSrc->Read(buf, 4) == 4, false);
        
        for (int64_t i = 0; i < 4; ++i)
        {
            if (buf[4 - i - 1] == '=')
            {
                ++mPadding;
            }
            else
            {
                break;
            }
        }
        
        mSize = mSrc->GetSize() - mPadding;
        
        return true;
    }
    
    virtual int64_t GetSize() const
    {
        return mSize;
    }
    
    virtual int64_t Seek(int64_t offset, int seek)
    {
        int64_t off = 0;
        switch (seek)
        {
            case eSeek_Set:
                off = piMin(offset, GetSize());
                break;
            case eSeek_Current:
                off = piMin(offset + mlldOffset, GetSize());
                break;
            case eSeek_End:
                off = GetSize() + offset - mPadding;
                break;
            default:
                return -1;
        }
        mlldOffset = off;
        
        if (off < mBStart || off >= mBStart + mBSize)
        {
            mBSize = 0;
            mBStart = off - off % 4;
            mSrc->Seek(mBStart, eSeek_Set);
        }
        
        mBOff = mlldOffset - mBStart;
        
        return off;
    }
    
    virtual int64_t GetOffset() const
    {
        return mlldOffset;
    }
    
    virtual int64_t Write(const void* pData, int64_t size)
    {
        return -1;
    }
    
    virtual int64_t Read(void* buffer, int64_t size)
    {
        int64_t total = 0;
        
        char* p = (char*)buffer;
        
        while (total < size)
        {
            if (mBOff >= mBSize && !ReadBlock())
            {
                break;
            }
            
            mBOff %= 4;
            // copy data into buffer
            int64_t cpSize = piMin(mBSize - mBOff, size - total);
            
            memcpy(p + total, mBuf + mBOff, (size_t)cpSize);
            
            mBOff += cpSize;
            total += cpSize;
        }
        
        mlldOffset += total;
        return total;
    }
    
    virtual string GetUri() const
    {
        return mSrc->GetUri();
    }
    
    virtual int64_t Final(void* buffer, int64_t size)
    {
        //丢弃mBuffer中的数据，因为不够解码。
        return 0;
    }
    
    virtual void Reset()
    {
        mSize = 0;
    }
    
    virtual int64_t EvaluateSpace(int64_t sourceSize)
    {
        return sourceSize / 4 * 3;
    }
    
    virtual int GetName() const
    {
        return eDecoder_Base64;
    }
    
private:
    void Decode(const char* src, uint8_t* dest)
    {
        *dest++ = uint8_t(pr2six[src[0]] << 2 | pr2six[src[1]] >> 4);
        *dest++ = uint8_t(pr2six[src[1]] << 4 | pr2six[src[2]] >> 2);
        *dest++ = uint8_t(pr2six[src[2]] << 6 | pr2six[src[3]]);
    }
    
    // mSrc must seek to the proper position before calling this method
    bool ReadBlock()
    {
        char buf[sizeof(mBuf)];
        auto read = mSrc->Read(buf, sizeof(buf));
        
        if (read < 0)
        {
            return false;
        }
        else if (read == 0)
        {
            return read > 0;
        }
        else
        {
            int64_t ret = 3;

            Decode(buf, mBuf);
            
            if (mSrc->End())
            {
                ret -= mPadding;
            }
            
            mBSize = ret;
            
            return true;
        }
    }
};

iStream* CreateBase64EncoderStream(iStream* source)
{
    return new Base64EncoderStream(source);
}

iStream* CreateBase64DecoderStream(iStream* source)
{
    return new Base64DecoderStream(source);
}

NSPI_END()




















