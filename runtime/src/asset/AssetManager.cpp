/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.23   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>
#include <mutex>
#include <thread>

using namespace std;

NSPI_BEGIN()

enum
{
    eAssetMsg_Load      = 1,
    eAssetMsg_Quit      = 2,
    eAssetMsg_Start     = 3,
};

struct AssetTask : public iRefObject
{
    AssetTask():
    cache(true)
    {
    }
    
    virtual ~AssetTask()
    {
    }
    
    SmartPtr<iAssetLoader> assetLoader;
    SmartPtr<iClassLoader> classLoader;
    SmartPtr<iStream> source;
    SmartPtr<iAssetManager> manager;
    SmartPtr<iFuture> future;
    bool cache;
};

class AssetManagerServer : public iRefObject
{
private:
    mutex mMutex;
    thread mThread;
    
    SmartPtr<iMessageQueue> mMQ;
    
public:
    AssetManagerServer()
    {
        mMQ = CreateMessageQueue();
        
        SmartPtr<iEvent> event = CreateEvent(1);
        
        mThread = std::thread([this, event]{Run(event);});
        
        event->Lock();
        event->Wait();
        event->Unlock();
    }
    
    virtual ~AssetManagerServer()
    {
        SmartPtr<iEvent> event = CreateEvent(1);
        
        mMQ->Clear();
        mMQ->PostMessage(nullptr, eAssetMsg_Quit, event.Ptr());
        
        event->Lock();
        event->Wait();
        event->Unlock();
        
        if (mThread.joinable())
        {
            mThread.join();
        }
    }
    
    void Load(AssetTask* task)
    {
        mMQ->PostMessage(nullptr, eAssetMsg_Load, task);
    }
    
private:
    
    void Run(iEvent* event)
    {
        event->FireAll();
        
        while (true)
        {
            SmartPtr<iMessage> msg = mMQ->WaitForMessage();
            
            int32_t msgId = msg->GetID();
            if (msgId == eAssetMsg_Quit)
            {
                SmartPtr<iEvent> evQuit = piGetMessageArg1<iEvent>(msg);
                evQuit->FireAll();
                break;
            }
            else if (msgId == eAssetMsg_Load)
            {
                OnLoad(msg);
            }
        }
    }
    
    void OnLoad(iMessage* msg)
    {
        SmartPtr<AssetTask> task = piQueryVarObject<AssetTask>(msg->GetArg1());
        SmartPtr<iFuture> future = task->future;
        SmartPtr<iRefObject> res = task->assetLoader->Load(task->manager, task->classLoader, task->source);
        if (task->cache)
        {
            // TODO how to cache?
        }
        
        future->SetValue(res.Ptr());
    }
};


//static SmartPtr<AssetManagerServer> g_Server = new AssetManagerServer();


class AssetManager : public iAssetManager
{
private:
    typedef vector<SmartPtr<iUriResolver>> ResolverVector;
    ResolverVector mResolvers;
    
    typedef map<string, iAsset*> ResMap;
    ResMap mResMap;
    
    AssetManager* mParent;
    
    typedef vector<SmartPtr<iAssetLoaderFactory>> FactoryVector;
    FactoryVector mLoaderFactories;
    
public:
    AssetManager():
    mParent(nullptr)
    {
    }
    
    virtual ~AssetManager()
    {
    }
    
    virtual void OnLoad()
    {
    }
    
    virtual void OnUnload()
    {
        mResMap.clear();
        
        if (mParent)
        {
            mParent->OnUnload();
        }
    }
    
    virtual void SetParent(iAssetManager* parent)
    {
        mParent = dynamic_cast<AssetManager*>(parent);
    }
    
    virtual iAssetManager* GetParent() const
    {
        return mParent;
    }
    
    virtual string Resolve(const string& uri)
    {
        for (auto& resolver : mResolvers)
        {
            string ret = resolver->Resolve(uri);
            if (!ret.empty())
            {
                return ret;
            }
        }
        
        return mParent ? mParent->Resolve(uri) : "";
    }
    
    virtual iAsset* SyncLoad(iClassLoader* classLoader,
                             const std::string& aUri,
                             bool cache = true)
    {
        piCheck(!aUri.empty(), nullptr);
        
        string uri = NormalizeUri(aUri);
        
        ResMap::iterator it = mResMap.find(uri);
        piCheck(it == mResMap.end(), it->second);
        
        SmartPtr<iStream> stream = Open(uri);
        if (!stream.IsNull())
        {
            stream->SetUri(uri);
            
            SmartPtr<iAssetLoader> loader = FindLoader(uri, stream);
            piAssert(!loader.IsNull(), nullptr);
            
            SmartPtr<iAsset> asset = loader->Load(this, classLoader, stream);
            piCheck(!asset.IsNull(), nullptr);
            
            asset->SetUri(uri);
            asset->SetReadonly();
            
            if (cache)
            {
                mResMap[uri] = asset;
                asset->SetManager(this);
            }
            
            return asset.PtrAndSetNull();
        }
        else
        {
            if (mParent)
            {
                return mParent->SyncLoad(classLoader, uri, cache);
            }
            else
            {
                PILOGE(PI_GAME_TAG, "Asset '%s' not found.", uri.c_str());
                return nullptr;
            }
        }
    }
    
    virtual iFuture* AsyncLoad(iClassLoader* classLoader,
                               const std::string& uri,
                               bool cache)
    {
        PILOGE(PI_TAG, "iAssetManager::AsyncLoad not implemented.");
        piAssert(false, nullptr);
        return nullptr;
#if 0
        SmartPtr<iFuture> future = CreateFuture();
        
        if (uri.empty())
        {
            future->SetValue(Var());
            return future.PtrAndSetNull();
        }
        
        ResMap::iterator it = mResMap.find(uri);
        if (it != mResMap.end())
        {
            future->SetValue(it->second.Ptr());
            return future.PtrAndSetNull();
        }
        
        SmartPtr<iStream> stream = Open(uri);
        if (!stream.IsNull())
        {
            stream->SetUri(uri);
            
            SmartPtr<iAssetLoader> assetLoader = FindLoader(uri, stream);
            piAssert(!assetLoader.IsNull(), future.PtrAndSetNull());
            
            SmartPtr<AssetTask> task = new AssetTask();
            task->classLoader = classLoader;
            task->assetLoader = assetLoader;
            task->manager = this;
            task->source = stream;
            task->future = future;
            
            //            g_Server->Load(task);
            
            return future.PtrAndSetNull();
        }
        else
        {
            return mParent ? mParent->AsyncLoad(classLoader, uri, cache) : future.PtrAndSetNull();
        }
#endif
    }
    
    virtual bool SetAsset(const std::string& uri, const Var& var)
    {
        ResMap::iterator it = mResMap.find(uri);
        piCheck(it == mResMap.end(), false);
        
        mResMap[uri] = piQueryVarObject<iAsset>(var);
        
        return true;
    }
    
    virtual void Unload(const std::string& uri)
    {
        mResMap.erase(uri);
    }
    
    virtual void AddUriResolver(iUriResolver* resolver)
    {
        mResolvers.push_back(resolver);
    }
    
    virtual void RemoveUriResolver(iUriResolver* resolver)
    {
        mResolvers.erase(std::remove(mResolvers.begin(), mResolvers.end(), resolver));
    }
    
    virtual void AddLoaderFactory(iAssetLoaderFactory* factory)
    {
        piAssert(factory != nullptr, ;);
        mLoaderFactories.push_back(factory);
    }
    
private:
    
    iStream* Open(const string& uri)
    {
        for (auto& resolver : mResolvers)
        {
            SmartPtr<iStream> stream = resolver->Open(uri);
            if (!stream.IsNull())
            {
                return stream.PtrAndSetNull();
            }
        }
        
        return nullptr;
    }
    
    iAssetLoader* FindLoader(const string& uri, iStream* source)
    {
        if (!mLoaderFactories.empty())
        {
            string fileExt = piGetExtendName(uri);
            SmartPtr<iAssetLoaderFactory> factory;
            for (auto factory : mLoaderFactories)
            {
                SmartPtr<iAssetLoader> loader = factory->CreateLoader(fileExt, source);
                if (!loader.IsNull())
                {
                    return loader.PtrAndSetNull();
                }
            }
        }
        
        return mParent ? mParent->FindLoader(uri, source) : nullptr;
    }
    
    static string NormalizeUri(const string& uri)
    {
        string protocol;
        string tmp = uri;
        
        size_t protoPos = uri.find("://");
        if (protoPos != string::npos)
        {
            protocol = uri.substr(0, protoPos + 3);
            tmp = uri.substr(protoPos + 3);
        }
        else if (uri[0] == '/')
        {
            protocol = "/";
            tmp = uri.substr(1);
        }
        
        vector<string> segs = piStrSplit(tmp.data(), tmp.size(), '/');
        
        vector<string> stack;
        
        for (const auto& s : segs)
        {
            if (s == "..")
            {
                stack.pop_back();
            }
            else if (s == ".")
            {
                // nothing to do
            }
            else
            {
                stack.push_back(s);
            }
        }
        
        string ret;
        
        if (!protocol.empty())
        {
            ret += protocol;
        }
        
        for (int i = 0; i < stack.size(); ++i)
        {
            const auto& s = stack[i];
            if (i > 0)
            {
                ret += "/";
            }
            ret += s;
        }
        
        return ret;
    }
};

iAssetManager* CreateAssetManager()
{
    return new AssetManager();
}




NSPI_END()












