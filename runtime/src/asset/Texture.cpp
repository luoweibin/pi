/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.18   0.1     Create
 ******************************************************************************/
#include "../asset/AssetImpl.h"

using namespace std;

NSPI_BEGIN()

template <class I>
class TextureImpl : public GraphicsAssetImpl<I>
{
private:
    int mWrapS;
    int mWrapT;
    int mWrapR;
    int mMinFilter;
    int mMagFilter;
    int mFormat;
    
protected:
    using I::IsReadonly;
    
public:
    TextureImpl():
    mWrapS(eTexValue_Linear), mWrapT(eTexValue_Linear), mWrapR(eTexValue_Linear),
    mMinFilter(eTexValue_Clamp), mMagFilter(eTexValue_Clamp), mFormat(ePixelFormat_Unknown)
    {
    }
    
    virtual ~TextureImpl()
    {
    }
    
    virtual int GetWrapS() const
    {
        return mWrapS;
    }
    
    virtual void SetWrapS(int value)
    {
        piAssert(!IsReadonly(), ;);
        mWrapS = value;
    }
    
    virtual int GetWrapT() const
    {
        return mWrapT;
    }
    
    virtual void SetWrapT(int value)
    {
        piAssert(!IsReadonly(), ;);
        mWrapT = value;
    }
    
    virtual int GetWrapR() const
    {
        return mWrapR;
    }
    
    virtual void SetWrapR(int value)
    {
        piAssert(!IsReadonly(), ;);
        mWrapR = value;
    }
    
    virtual int GetMinFilter() const
    {
        return mMinFilter;
    }
    
    virtual void SetMinFilter(int value)
    {
        piAssert(!IsReadonly(), ;);
        mMinFilter = value;
    }
    
    virtual int GetMagFilter() const
    {
        return mMagFilter;
    }
    
    virtual void SetMagFilter(int value)
    {
        piAssert(!IsReadonly(), ;);
        mMagFilter = value;
    }
    
    virtual void SetFormat(int format)
    {
        piAssert(!IsReadonly(), ;);
        mFormat = format;
    }
    
    virtual int GetFormat() const
    {
        return mFormat;
    }
};


class Texture2D : public TextureImpl<iTexture2D>
{
private:
    string mSource;
    
    int32_t mWidth;
    int32_t mHeight;
public:
    Texture2D()
    : mWidth(0)
    , mHeight(0)
    {
    }
    
    virtual ~Texture2D()
    {
    }
    
    virtual std::string GetSource() const
    {
        return mSource;
    }
    
    virtual void SetSource(const std::string& uri)
    {
        piAssert(!IsReadonly(), ;);
        mSource = uri;
    }
    
    virtual int GetWidth() const
    {
        return mWidth;
    }
    
    virtual void SetWidth(int32_t width)
    {
        mWidth = width;
    }
    
    virtual int GetHeight() const
    {
        return mHeight;
    }
    
    virtual void SetHeight(int32_t height)
    {
        mHeight = height;
    }
};


iTexture2D* CreateTexture2D()
{
    return new Texture2D();
}


class CubeMap : public TextureImpl<iCubeMap>
{
private:
    string mPosX;
    string mNegX;
    string mPosY;
    string mNegY;
    string mPosZ;
    string mNegZ;
        
public:
    CubeMap()
    {
    }
    
    virtual ~CubeMap()
    {
    }
    
    virtual string GetPositiveX() const
    {
        return mPosX;
    }
    
    virtual void SetPositiveX(const string& name)
    {
        piAssert(!IsReadonly(), ;);
        mPosX = name;
    }
    
    virtual string GetNegativeX() const
    {
        return mNegX;
    }
    
    virtual void SetNegativeX(const string& name)
    {
        piAssert(!IsReadonly(), ;);
        mNegX = name;
    }
    
    
    virtual string GetPositiveY() const
    {
        return mPosY;
    }
    
    virtual void SetPositiveY(const string& name)
    {
        piAssert(!IsReadonly(), ;);
        mPosY = name;
    }
    
    virtual string GetNegativeY() const
    {
        return mNegY;
    }
    
    virtual void SetNegativeY(const string& name)
    {
        piAssert(!IsReadonly(), ;);
        mNegY = name;
    }
    
    
    virtual string GetPositiveZ() const
    {
        return mPosZ;
    }
    
    virtual void SetPositiveZ(const string& name)
    {
        mPosZ = name;
    }
    
    virtual string GetNegativeZ() const
    {
        return mNegZ;
    }
    
    virtual void SetNegativeZ(const string& name)
    {
        piAssert(!IsReadonly(), ;);
        mNegZ = name;
    }
};


iCubeMap* CreateCubeMap()
{
    return new CubeMap();
}


class RenderTexture : public TextureImpl<iRenderTexture>
{
    bool mFixScreenBounds;
    
    int32_t mWidth;
    int32_t mHeight;
    int32_t mDepth;

    int32_t mDepthFormat;
    
    int32_t mDimession;
    
    int32_t mFBO;
    
    int32_t mDepthBuffer;
    
public:
    RenderTexture()
    : mFBO(0)
    , mDepthBuffer(0)
    , mFixScreenBounds(false)
    , mWidth(0)
    , mHeight(0)
    , mDepth(0)
    , mDepthFormat(eRenderTextureDepthFormat_D16)
    , mDimession(eRenderTextureDimession_2D)
    {
    }
    
    virtual ~RenderTexture()
    {
        if (mFBO != 0)
            piReleaseGraphicsObject(mFBO);
        
        if (mDepthBuffer != 0)
            piReleaseGraphicsObject(mDepthBuffer);
    }

    virtual bool IsFixScreenBounds() const
    {
        return mFixScreenBounds;
    }
    
    virtual void SetFixScreenBounds(bool flag)
    {
        mFixScreenBounds = flag;
    }
    
    virtual void Resize()
    {
    }
    
    virtual int32_t GetWidth() const
    {
        return mWidth;
    }
    
    virtual void SetWidth(int32_t width)
    {
        mWidth = width;
    }
    
    virtual int32_t GetHeight() const
    {
        return mHeight;
    }
    
    virtual void SetHeight(int32_t height)
    {
        mHeight = height;
    }
    
    virtual int32_t GetDepth() const
    {
        return mDepth;
    }
    
    virtual void SetDepth(int32_t depth)
    {
        mDepth = depth;
    }
    
    virtual int32_t GetDepthFormat() const
    {
        return mDepthFormat;
    }
    
    virtual void SetDepthFormat(int32_t format)
    {
        mDepthFormat = format;
    }
    
    virtual int32_t GetDimession() const 
    {
        return mDimession;
    }
    
    virtual void SetDimession(int32_t dimession)
    {
        mDimession = dimession;
    }
    
    virtual bool DoCreate()
    {
        if (mFBO == 0)
            mFBO = piCreateFramebuffer();
        
        piBindFramebuffer(eFramebuffer_DrawRead, mFBO);
        
        int32_t name = 0;
        
        switch(mDimession)
        {
            case eRenderTextureDimession_2D:
            {
                name = CreateColorTextureEx(vec4(0,0,0,0),mWidth,mHeight,GetGraphicsName());
                
                piFramebufferTexture2D(eFramebuffer_DrawRead, eAttachment_Color0, eTexTarget_2D, name, 0);
                
                SetGraphicsName(name);
                
                piReleaseGraphicsObject(name);
            }
            break;
            case eRenderTextureDimession_Cube:
            {
                PILOGE(PI_GAME_TAG,"RenderTexture_Cube still not supported yet.");
                
                piAssert( mWidth == mHeight, false;);
                
                piAssert(!(piIsPow2(mWidth)&&piIsPow2(mHeight)),  false;);
                
//                piBindTexture(eTexTarget_CubeMap, name);
//                
//                piTexParam(eTexTarget_CubeMap, eTexConfig_WrapS, GetWrapS());
//                piTexParam(eTexTarget_CubeMap, eTexConfig_WrapT, GetWrapT());
//                piTexParam(eTexTarget_CubeMap, eTexConfig_WrapR, GetWrapR());
//                piTexParam(eTexTarget_CubeMap, eTexConfig_MinFilter, GetMinFilter());
//                piTexParam(eTexTarget_CubeMap, eTexConfig_MagFilter, GetMagFilter());
            }
            break;
            case eRenderTextureDimession_3D:
            {
                PILOGE(PI_GAME_TAG,"RenderTexture_3D still not supported yet.");
                //piBindTexture(eTexTarget_3D, name);
            }
            break;
        }
        
        switch(mDepthFormat)
        {
            case eRenderTextureDepthFormat_D16:
            {
                mDepthBuffer = piCreateRenderbuffer();
                piBindRenderbuffer(mDepthBuffer);
                piRenderbufferStorage(ePixelFormat_Depth16, mWidth, mHeight);
                piBindRenderbuffer(0);
            }
            break;
            case eRenderTextureDepthFormat_D24S8:
            {
                mDepthBuffer = piCreateRenderbuffer();
                piBindRenderbuffer(mDepthBuffer);
                piRenderbufferStorage(ePixelFormat_Depth24Stencil8, mWidth, mHeight);
                piBindRenderbuffer(0);
            }
            break;
        }
        
        if (mDepthBuffer )
        {
            piFramebufferRenderbuffer(eFramebuffer_DrawRead, eAttachment_Depth, mDepthBuffer);
        }
        
        piBindFramebuffer(eFramebuffer_DrawRead, 0);
        
        return true;
    }
    
    virtual bool Resize(int32_t width, int32_t height)
    {
        if (GetWidth() == width
            && GetHeight() == height)
            return true;
        
        if (mFBO == 0)
            return false;
        
        SetWidth(width);
        SetHeight(height);
        
        piBindFramebuffer(eFramebuffer_DrawRead, mFBO);
        switch(mDimession)
        {
            case eRenderTextureDimession_2D:
            {
                int32_t name = CreateColorTextureEx(vec4(0,0,0,0),mWidth,mHeight,GetGraphicsName());
                
                piFramebufferTexture2D(eFramebuffer_DrawRead, eAttachment_Color0, eTexTarget_2D, name, 0);
            }
            break;
            default:
                piBindFramebuffer(eFramebuffer_DrawRead, 0);
                return false;
        }
        
        switch(mDepthFormat)
        {
            case eRenderTextureDepthFormat_D16:
            {
                if (mDepthBuffer == 0)
                    mDepthBuffer = piCreateRenderbuffer();
                piBindRenderbuffer(mDepthBuffer);
                piRenderbufferStorage(ePixelFormat_Depth16, mWidth, mHeight);
                piBindRenderbuffer(0);
            }
                break;
            case eRenderTextureDepthFormat_D24S8:
            {
                if (mDepthBuffer == 0)
                    mDepthBuffer = piCreateRenderbuffer();
                piBindRenderbuffer(mDepthBuffer);
                piRenderbufferStorage(ePixelFormat_Depth24Stencil8, mWidth, mHeight);
                piBindRenderbuffer(0);
            }
                break;
        }
        
        if (mDepthBuffer )
        {
            piFramebufferRenderbuffer(eFramebuffer_DrawRead, eAttachment_Depth, mDepthBuffer);
        }
        
        piBindFramebuffer(eFramebuffer_DrawRead, 0);
        
        return true;
    }
    
    virtual int32_t GetFBO()
    {
        return mFBO;
    }
    
    static std::map< std::string, SmartPtr<iRenderTexture>> ms_SrcRenderTextureTable;
};

std::map< std::string, SmartPtr<iRenderTexture>> RenderTexture::ms_SrcRenderTextureTable;

iRenderTexture* CreateRenderTexture()
{
    return new RenderTexture();
}

void AddToSourceRenderTexture(const std::string& uri, iRenderTexture* texture)
{
    auto Iter = RenderTexture::ms_SrcRenderTextureTable.find(uri);
    if (Iter != RenderTexture::ms_SrcRenderTextureTable.end())
    {
        PILOGW(PI_GAME_TAG,"%s has already been loaded.",uri.c_str());
        return;
    }
    
    RenderTexture::ms_SrcRenderTextureTable.insert(std::make_pair(uri, texture));
}

iRenderTexture* GetRenderTexture(const std::string& uri)
{
    auto Iter = RenderTexture::ms_SrcRenderTextureTable.find(uri);
    if (Iter == RenderTexture::ms_SrcRenderTextureTable.end())
    {
        return nullptr;
    }
    
    return Iter->second.Ptr();
}

void ClearRenderTextureTable()
{
    RenderTexture::ms_SrcRenderTextureTable.clear();
}

int32_t CreateColorTexture(const vec4& color)
{
    int32_t texture = piCreateTexture();
    
    SmartPtr<iBitmap> bitmap = CreateBitmap(ePixelFormat_RGBA, 2, 2);
    piAssert(!bitmap.IsNull(), 0);
    
    SmartPtr<iMemory> buffer = bitmap->GetData(0);
    piAssert(!buffer.IsNull(), 0);
    
    uint8_t *p = (uint8_t*)buffer->Ptr();
    for (int32_t i = 0; i < buffer->Size(); i += 4)
    {
        p[i] = color.r * 255.0;
        p[i+1] = color.g * 255.0;
        p[i+2] = color.b * 255.0;
        p[i+3] = color.a * 255.0;
    }
    
    piActiveTexture(0);
    piBindTexture(eTexTarget_2D, texture);
    piTexImage2D(eTexTarget_2D, 0, ePixelFormat_RGBA, bitmap, 0);
    piTexParam(eTexTarget_2D, eTexConfig_MagFilter, eTexValue_Linear);
    piTexParam(eTexTarget_2D, eTexConfig_MinFilter, eTexValue_Linear);
    piTexParam(eTexTarget_2D, eTexConfig_WrapS, eTexValue_Clamp);
    piTexParam(eTexTarget_2D, eTexConfig_WrapT, eTexValue_Clamp);
    
    return texture;
}

int32_t CreateColorTextureEx(const vec4& color, int width, int height, int32_t texture /*= 0*/)
{
    if (texture == 0)
    {
        texture = piCreateTexture();
    }
    
    SmartPtr<iBitmap> bitmap = CreateBitmap(ePixelFormat_RGBA, width, height);
    piCheck(!bitmap.IsNull(), 0);
    
    SmartPtr<iMemory> buffer = bitmap->GetData(0);
    piCheck(!buffer.IsNull(), 0);
    
    uint32_t color32 = 0;
    
    uint8_t* pColor = (uint8_t*)&color32;
    pColor[0] = color.r * 255.0;
    pColor[1] = color.g * 255.0;
    pColor[2] = color.b * 255.0;
    pColor[3] = color.a * 255.0;
    
//    uint32_t *p = (uint32_t*);
//    for (int32_t i = 0; i < buffer->Size()>>2; ++i)
//    {
//        p[i] = color32;
//    }
    
    memset(buffer->Ptr(), color32, buffer->Size());
    
    piActiveTexture(0);
    piBindTexture(eTexTarget_2D, texture);
    piTexImage2D(eTexTarget_2D, 0, ePixelFormat_RGBA, bitmap, 0);
    piTexParam(eTexTarget_2D, eTexConfig_MagFilter, eTexValue_Linear);
    piTexParam(eTexTarget_2D, eTexConfig_MinFilter, eTexValue_Linear);
    piTexParam(eTexTarget_2D, eTexConfig_WrapS, eTexValue_Clamp);
    piTexParam(eTexTarget_2D, eTexConfig_WrapT, eTexValue_Clamp);
    piBindTexture(eTexTarget_2D, 0);
    
    return texture;
}

NSPI_END()





















