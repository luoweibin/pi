/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.19   0.1     Create
 ******************************************************************************/
#include "AssetImpl.h"

using namespace std;

NSPI_BEGIN()

class ShaderProgram : public GraphicsAssetImpl<iShaderProgram>
{
private:
    SmartPtr<iTable> mAttrs;
    string mVertexShader;
    string mFragShader;
    
public:
    ShaderProgram()
    {
        mAttrs = CreateTable();
    }
    
    virtual ~ShaderProgram()
    {
    }
    
    virtual std::string GetVertexShader() const
    {
        return mVertexShader;
    }
    
    virtual void SetVertexShader(const std::string& uri)
    {
        mVertexShader = uri;
    }
    
    virtual std::string GetFragShader() const
    {
        return mFragShader;
    }
    
    virtual void SetFragShader(const std::string& uri)
    {
        mFragShader = uri;
    }
    
    virtual iTable* GetAttrs() const
    {
        return mAttrs;
    }
    
    virtual void SetAttrs(iTable* attrs)
    {
        piAssert(attrs != nullptr, ;);
        mAttrs = attrs;
    }
};


iShaderProgram* CreateShaderProgram()
{
    return new ShaderProgram();
}

iShaderProgram* CreateDefaultShaderProgram()
{
    ShaderProgram* program = new ShaderProgram();
    
    int gType = piGetGraphicsType();
    
    const char defaultVS_ES2[] = "  precision highp float;\n\
                                attribute vec4 position;\n\
                                attribute vec2 uv;\n\
                                attribute vec4 jointIndices;\n\
                                attribute vec4 jointWeights;\n\
                                attribute vec3 normal;\n\
                                \n\
                                varying vec2 uv0;\n\
                                \n\
                                uniform mat4 MVPMatrix;\n\
                                void main(void)\n\
                                {\n\
                                    gl_Position = MVPMatrix * position;\n\
                                    uv0 = uv.st;\n\
                                }";
    
    const char defaultFS_ES2[] = "precision highp float;\n\
                                varying vec2 uv0;\n\
                                \n\
                                uniform sampler2D s_diffuse;\n\
                                uniform sampler2D s_transparent;\n\
                                \n\
                                float luminance(vec4 color)\n\
                                {\n\
                                    return color.r * 0.212671 + color.g * 0.715160 + color.b * 0.072169;\n\
                                }\n\
                                void main(void)\n\
                                {\n\
                                    gl_FragColor = vec4(uv0,1,1);\
                                }";
    
    const char defaultVS_GL[] = "#version 400\n\
    precision highp float;\n\
    layout(location=0) in vec4 position;\n\
    layout(location=1) in vec2 uv;\n\
    layout(location=2) in vec4 jointIndices;\n\
    layout(location=3) in vec4 jointWeights;\n\
    layout(location=4) in vec3 normal;\n\
    \n\
    out vec2 uv0;\n\
    \n\
    uniform mat4 MVPMatrix;\n\
    void main(void)\n\
    {\n\
    gl_Position = MVPMatrix * position;\n\
    uv0 = uv.st;\n\
    }";
    
    const char defaultFS_GL[] = " #version 400\n\
    precision highp float;\n\
    in vec2 uv0;\n\
    layout(location=0) out vec4 fragColor;\n\
    \n\
    uniform sampler2D s_diffuse;\n\
    uniform sampler2D s_transparent;\n\
    \n\
    float luminance(vec4 color)\n\
    {\n\
    return color.r * 0.212671 + color.g * 0.715160 + color.b * 0.072169;\n\
    }\n\
    void main(void)\n\
    {\n\
    fragColor = vec4(uv0,1,1);\
    }";

    
    int32_t name = piCreateProgram();
    
    switch(gType)
    {
        case eGraphicsBackend_OpenGL_ES2:
            piCompileProgram(name, defaultVS_ES2, defaultFS_ES2);
            break;
        case eGraphicsBackend_OpenGL3:
        case eGraphicsBackend_OpenGL4:
            piCompileProgram(name, defaultVS_GL, defaultFS_GL);
            break;
    }
    
    
    SmartPtr<iTable> params = CreateTable();
    
    params->Set("position",     0);
    params->Set("uv",           1);
    params->Set("jointIndices", 2);
    params->Set("jointWeights", 3);
    params->Set("normal",       4);
    
    program->SetAttrs(params);
    
    if (gType == eGraphicsBackend_OpenGL_ES2)
    {
        SmartPtr<iTable> attrs = program->GetAttrs();
        SmartPtr<iStringArray> keys = attrs->GetKeys();
        piEach<iStringArray, const string&>(keys, [attrs, name](const string& key) {
            int32_t index = attrs->Get(key);
            piBindVertexAttr(name, index, key);
        });
    }
    
    piLinkProgram(name);
    
    program->SetGraphicsName(name);
    
    piReleaseGraphicsObject(name);
    
    return program;
}


NSPI_END()
























