/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.18   1.0     Create
 ******************************************************************************/
#ifndef PI_SRC_GAME_ASSET_ASSETIMPL_H
#define PI_SRC_GAME_ASSET_ASSETIMPL_H

#include <pi/Game.h>

namespace nspi
{
    
    template <class I>
    class AssetImpl : public I
    {
    private:
        std::string mUri;
        iAssetManager* mMgr;
        int32_t mVersion;
        
    public:
        AssetImpl():
        mMgr(nullptr),
        mVersion(1)
        {
        }
        
        virtual ~AssetImpl()
        {
            if (mMgr != nullptr)
            {
                mMgr->Unload(mUri);
            }
        }
        
        virtual std::string GetUri() const
        {
            return mUri;
        }
        
        virtual void SetUri(const std::string& uri)
        {
            mUri = uri;
        }
        
        virtual iAssetManager* GetManager() const
        {
            return mMgr;
        }
        
        virtual void SetManager(iAssetManager* manager)
        {
            piAssert(manager != nullptr, ;);
            mMgr = manager;
        }
        
        virtual void SetVersion(int32_t version)
        {
            piAssert(version > 0, ;);
            mVersion = version;
        }
        
        virtual int32_t GetVersion() const
        {
            return mVersion;
        }
    };
    
    
    template <class I>
    class GraphicsAssetImpl : public AssetImpl<I>
    {
    private:
        int32_t mName;
        
    public:
        GraphicsAssetImpl():
        mName(0)
        {
        }
        
        virtual ~GraphicsAssetImpl()
        {
            piReleaseGraphicsObject(mName);
            mName = 0;
        }
        
        virtual int32_t GetGraphicsName() const
        {
            return mName;
        }
        
        virtual void SetGraphicsName(int32_t name)
        {
            piAssert(name > 0, ;);
            
            piRetainGraphicsObject(name);
            piReleaseGraphicsObject(mName);
            
            mName = name;
        }
    };
}


#endif 





























