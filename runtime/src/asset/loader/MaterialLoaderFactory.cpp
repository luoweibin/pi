//
//  MaterialLoaderFactory.cpp
//  pi
//
//  Created by Sword on 2017/6/26.
//  Copyright © 2017 pi. All rights reserved.
//

#include <stdio.h>
#include <pi/Game.h>
#include "../serialzation/JsonUtil.h"

using namespace std;

NSPI_BEGIN()


class MaterialLoader : public iAssetLoader
{
public:
    MaterialLoader()
    {
    }
    
    virtual ~MaterialLoader()
    {
    }
    
    virtual iAsset* Load(iAssetManager* manager, iClassLoader* classLoader, iStream* stream)
    {
        SmartPtr<iUnserializer> unser = CreateJsonUnserializer(manager, classLoader);
  
        string dir = piGetDirectory(stream->GetUri());
        
        json o = ReadJsonFromStream(stream);

        piCheck(o.find("Type") != o.end(), nullptr);
        
        piCheck(o["Type"].get<string>().compare("Material") == 0, nullptr);
        
        SmartPtr<iMaterial> mtl = CreateMaterial();
        
        if (o.find("Name") != o.end())
        {
            mtl->SetName(o["Name"].get<string>());
        }
        
        if (o.find("RenderType") != o.end())
        {
            SmartPtr<iEnum> enumClass = classLoader->FindEnum("eRenderType");
            if (!enumClass.IsNull())
            {
                int v = Json2Enum(enumClass, o["RenderType"].get<string>());
                mtl->SetRenderType(v);
            }
        }
        
        if (o.find("RenderQueue") != o.end())
        {
            mtl->SetRenderQueue(o["RenderQueue"].get<int32_t>());
        }
        
        if (o.find("Shader") != o.end())
        {
            string shaderUri = o["Shader"].get<string>();
            mtl->SetShaderUri(shaderUri);
            
            if (shaderUri.find("share://") != 0)
            {
                shaderUri = dir + shaderUri;
            }
            
            SmartPtr<iShaderProgram> program = piLoadAsset<iShaderProgram>(manager, classLoader, shaderUri);
            mtl->SetShader(program);
        }
        
        if (o.find("State") != o.end())
        {
            string stateUri = o["State"].get<string>();
            mtl->SetShaderUri(stateUri);
            
            if (stateUri.find("share://") != 0)
            {
                stateUri = dir + stateUri;
            }
            
            SmartPtr<iRenderState> state = piLoadAsset<iRenderState>(manager, classLoader, stateUri);
            mtl->SetRenderState(state);
        }
        else
        {
            mtl->SetRenderState(CreateRenderState());
        }
        
        if (o.find("Props") != o.end() && o["Props"].is_array())
        {
            SmartPtr<iMaterialPropArray> propArray = mtl->GetProps();
            
            json props = o["Props"];
            for (auto p : props)
            {
                if (p.find("Name") == p.end() || p.find("Type") == p.end())
                {
                    continue;
                }
                
                string name = p["Name"];
                SmartPtr<iMaterialProp> prop = CreateMaterialProp();
                prop->SetName(name);
                
                string className = p["Type"].get<string>();
                SmartPtr<iClass> klass = classLoader->FindClass(className);
                if (klass.IsNull())
                {
                    continue;
                }
                
                if (p.find("Value") != p.end())
                {
                    Var propValue = Json2VarHint(manager, classLoader, klass, p["Value"]);
                    prop->SetValue(propValue);
                }
                else if (p.find("Uri") != p.end())
                {
                    string uri = dir + p["Uri"].get<string>();
                    SmartPtr<iAsset> asset = piLoadAsset<iAsset>(manager, classLoader, uri);
                    
                    piAssert(!asset.IsNull(), nullptr;);
                    
                    prop->SetValue(asset.Ptr());
                }
                else
                {
                    prop->SetValue(klass->CreateInstance());
                }
                
                propArray->PushBack(prop);
            }
        }
        
        return mtl.PtrAndSetNull();
    }
};


class MaterialLoaderFactory : public iAssetLoaderFactory
{
private:
    SmartPtr<iAssetLoader> mLoader;
    
public:
    MaterialLoaderFactory()
    {
        mLoader = new MaterialLoader();
    }
    
    virtual ~MaterialLoaderFactory()
    {
    }
    
    virtual iAssetLoader* CreateLoader(const string& fileExt, iStream* stream)
    {
        return mLoader;
    }
};


iAssetLoaderFactory* CreateMaterialLoaderFactory()
{
    return new MaterialLoaderFactory();
}

NSPI_END()
