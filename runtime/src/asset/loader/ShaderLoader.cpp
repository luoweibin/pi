/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.19   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>
#include "../AssetBuilder.h"

using namespace std;

NSPI_BEGIN()

static const char* Prefix(int type)
{
    switch (type)
    {
        case eGraphicsBackend_OpenGL_ES2:
            return "es2";
        case eGraphicsBackend_OpenGL_ES3:
            return "es3";
        case eGraphicsBackend_OpenGL3:
        case eGraphicsBackend_OpenGL4:
            return "gl";
        case eGraphicsBackend_Unknown:
        default:
            return "";
    }
}

static string MatchUri(int type, const string& uri)
{
    auto pos = uri.find_last_of(".");
    if (pos != string::npos)
    {
        return uri.substr(0, pos + 1) + Prefix(type) + ".glsl";
    }
    else
    {
        return uri + "." + Prefix(type);
    }
}

static iMemory* LoadShader(int type, const string& uri, iAssetManager* assetManager, iClassLoader* classLoader)
{
    string matchedUri = MatchUri(type, uri);
    
    SmartPtr<iMemoryAsset> asset = piLoadAsset<iMemoryAsset>(assetManager, classLoader, matchedUri);
    piCheck(!asset.IsNull(), nullptr);
    
    SmartPtr<iMemory> mem = asset->GetMemory();
    asset.PtrAndSetNull();
    
    return mem.PtrAndSetNull();
}

bool BuildShader(const std::string& uri, iAsset* asset, iAssetManager* assetManager, iClassLoader* classLoader)
{
    SmartPtr<iShaderProgram> program = dynamic_cast<iShaderProgram*>(asset);
    piAssert(!program.IsNull(), false);
    
    string vs = program->GetVertexShader();
    piCheck(!vs.empty(), false);
    
    string fs = program->GetFragShader();
    piCheck(!fs.empty(), false);
    
    string dir = piGetDirectory(uri);

    int gType = piGetGraphicsType();
    char* memVS = nullptr;
    char* memFS = nullptr;
    
    {
        string shaderUri = dir + vs;
        SmartPtr<iMemory> mem = LoadShader(gType, shaderUri, assetManager, classLoader);
        piCheck(!mem.IsNull(), false);
        
        memVS = mem->Ptr();
    }
    
    {
        string shaderUri = dir + fs;
        SmartPtr<iMemory> mem = LoadShader(gType, shaderUri, assetManager, classLoader);
        piCheck(!mem.IsNull(), false);
        
        memFS = mem->Ptr();
    }
    
    int32_t name = piCreateProgram();
    piCompileProgram(name, memVS, memFS);
    
    if (gType == eGraphicsBackend_OpenGL_ES2)
    {
        SmartPtr<iTable> attrs = program->GetAttrs();
        SmartPtr<iStringArray> keys = attrs->GetKeys();
        piEach<iStringArray, const string&>(keys, [attrs, name](const string& key) {
            int32_t index = attrs->Get(key);
            piBindVertexAttr(name, index, key);
        });
    }

    piLinkProgram(name);

    program->SetGraphicsName(name);
    
    piReleaseGraphicsObject(name);
    return true;
}

bool BuildRenderState(const std::string& uri, iAsset* asset, iAssetManager* assetManager, iClassLoader* classLoader)
{
    SmartPtr<iRenderState> renderState = dynamic_cast<iRenderState*>(asset);
    piAssert(!renderState.IsNull(), false);
    
    return true;
}

NSPI_END()


























