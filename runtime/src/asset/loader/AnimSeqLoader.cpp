/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.30   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>
#include "../serialzation/JsonUtil.h"

using namespace std;

NSPI_BEGIN()


class AnimSeqLoader : public iAssetLoader
{
public:
    AnimSeqLoader()
    {
    }
    
    virtual ~AnimSeqLoader()
    {
    }
    
    virtual iAsset* Load(iAssetManager* manager, iClassLoader* classLoader, iStream* stream)
    {
        SmartPtr<iAnimSeq> seq = CreateAnimSeq();
        
        json o = ReadJsonFromStream(stream);
        
        if (o.find("Frames") != o.end() && o["Frames"].is_array())
        {
            json array = o["Frames"];
            
            SmartPtr<iTexture2DArray> frames = seq->GetFrames();
            
            string dir = piGetDirectory(stream->GetUri());
            
            for (auto item : array)
            {
                string uri = dir + item.get<string>();
                SmartPtr<iTexture2D> frame = piLoadAsset<iTexture2D>(manager, classLoader, uri);
                if (!frame.IsNull())
                {
                    frames->PushBack(frame);
                }
            }
            
            piCheck(!frames->IsEmpty(), nullptr);
        }
        
        if (o.find("FPS") != o.end())
        {
            int32_t fps = o["FPS"].get<int32_t>();
            seq->SetFPS(fps);
        }
        
        return seq.PtrAndSetNull();
    }
};


class AnimSeqLoaderFactory : public iAssetLoaderFactory
{
private:
    SmartPtr<iAssetLoader> mLoader;
    
public:
    AnimSeqLoaderFactory()
    {
        mLoader = new AnimSeqLoader();
    }
    
    virtual ~AnimSeqLoaderFactory()
    {
    }
    
    virtual iAssetLoader* CreateLoader(const string& fileExt, iStream* stream)
    {
        return mLoader;
    }
};

iAssetLoaderFactory* CreateAnimSeqLoaderFactory()
{
    return new AnimSeqLoaderFactory();
}


NSPI_END()























