/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.11   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()

class PngLoader : public iAssetLoader
{
public:
    PngLoader()
    {
    }
    
    virtual ~PngLoader()
    {
    }
    
    virtual iAsset* Load(iAssetManager* manager, iClassLoader* classLoader, iStream* stream)
    {
        SmartPtr<iBitmap> bitmap = piDecodePNG(stream);
        piCheck(!bitmap.IsNull(), nullptr);
        
        SmartPtr<iBitmapAsset> asset = CreateBitmapAsset();
        asset->SetBitmap(bitmap);
        return asset.PtrAndSetNull();
    }
};


class PngLoaderFactory : public iAssetLoaderFactory
{
private:
    SmartPtr<iAssetLoader> mLoader;
public:
    
    PngLoaderFactory()
    {
        mLoader = new PngLoader();
    }
    
    virtual ~PngLoaderFactory()
    {
    }
    
    virtual iAssetLoader* CreateLoader(const std::string& fileExt, iStream* stream)
    {
        return mLoader;
    }
};

iAssetLoaderFactory* CreatePngLoaderFactory()
{
    return new PngLoaderFactory();
}


NSPI_END()



















