/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 sword     2017.6.30   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>
#include "../serialzation/JsonUtil.h"

using namespace std;

NSPI_BEGIN()


class RenderTextureLoader : public iAssetLoader
{
public:
    RenderTextureLoader()
    {
    }
    
    virtual ~RenderTextureLoader()
    {
    }
    
    virtual iAsset* Load(iAssetManager* manager, iClassLoader* classLoader, iStream* stream)
    {
        json o = ReadJsonFromStream(stream);
        
        string absPath = piAbsPath(stream->GetUri());
        
        SmartPtr<iRenderTexture> texture = GetRenderTexture(absPath);
        
        if (!texture.IsNull())
        {
            return texture.PtrAndSetNull();
        }
        
        SmartPtr<iUnserializer> unser = CreateJsonUnserializer(manager, classLoader);
        
        texture = dynamic_cast<iRenderTexture*>(Json2Object(manager, classLoader, o));
        
        if (!texture.IsNull())
        {
            texture->DoCreate();
            AddToSourceRenderTexture(absPath, texture);
        }
        
        return texture.PtrAndSetNull();
    }
};


class RenderTextureLoaderFactory : public iAssetLoaderFactory
{
private:
    SmartPtr<iAssetLoader> mLoader;
    
public:
    RenderTextureLoaderFactory()
    {
        mLoader = new RenderTextureLoader();
    }
    
    virtual ~RenderTextureLoaderFactory()
    {
    }
    
    virtual iAssetLoader* CreateLoader(const string& fileExt, iStream* stream)
    {
        return mLoader;
    }
};


iAssetLoaderFactory* CreateRenderTextureLoaderFactory()
{
    return new RenderTextureLoaderFactory();
}

NSPI_END()


























