/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.13   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()

bool BuildParticleEmitterLib(const std::string& uri, iAsset* asset, iAssetManager* assetManager, iClassLoader* classLoader)
{
    iParticleEmitterLib* piParticleEmitterLib = dynamic_cast<iParticleEmitterLib*>(asset);
    return piParticleEmitterLib != NULL;
}

NSPI_END()
