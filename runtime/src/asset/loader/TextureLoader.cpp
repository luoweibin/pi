/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.18   0.1     Create
 ******************************************************************************/
#include <pi/Asset.h>

using namespace std;

NSPI_BEGIN()

bool BuildTexture2D(const std::string& uri, iAsset* asset, iAssetManager* assetManager, iClassLoader* classLoader)
{
    SmartPtr<iTexture2D> texture = dynamic_cast<iTexture2D*>(asset);
    piAssert(!texture.IsNull(), false);
    
    string dir = piGetDirectory(uri);

    string src = dir + texture->GetSource();
    SmartPtr<iBitmapAsset> bmpAsset = piLoadAsset<iBitmapAsset>(assetManager, classLoader, src, false);
    piAssert(!bmpAsset.IsNull(), false);
    
    SmartPtr<iBitmap> bitmap = bmpAsset->GetBitmap();
    piAssert(!bitmap.IsNull(), false);
    
    if (texture->GetFormat() == ePixelFormat_Unknown)
    {
        texture->SetFormat(bitmap->GetPixelFormat()->GetName());
    }
    
    int32_t name = piCreateTexture();
    piBindTexture(eTexTarget_2D, name);
    
    piTexParam(eTexTarget_2D, eTexConfig_WrapS, texture->GetWrapS());
    piTexParam(eTexTarget_2D, eTexConfig_WrapT, texture->GetWrapT());
    piTexParam(eTexTarget_2D, eTexConfig_WrapR, texture->GetWrapR());
    piTexParam(eTexTarget_2D, eTexConfig_MinFilter, texture->GetMinFilter());
    piTexParam(eTexTarget_2D, eTexConfig_MagFilter, texture->GetMagFilter());
    piTexImage2D(eTexTarget_2D, 0, texture->GetFormat(), bitmap, 0);
    
    piBindTexture(eTexTarget_2D, 0);
    
    texture->SetGraphicsName(name);
    texture->SetWidth(bitmap->GetWidth());
    texture->SetHeight(bitmap->GetHeight());
    
    piReleaseGraphicsObject(name);
    
    PILOGI(PI_GRAPHICS_TAG, "==Texture:[%d]%s", name, src.c_str());
    
    return true;
}


bool BuildCubMap(const std::string& uri, iAsset* asset, iAssetManager* assetManager, iClassLoader* classLoader)
{
    SmartPtr<iCubeMap> cubeMap = dynamic_cast<iCubeMap*>(asset);
    piAssert(!cubeMap.IsNull(), false);
    
    int32_t name = piCreateTexture();
    piBindTexture(eTexTarget_CubeMap, name);
    
    piTexParam(eTexTarget_CubeMap, eTexConfig_WrapS, cubeMap->GetWrapS());
    piTexParam(eTexTarget_CubeMap, eTexConfig_WrapT, cubeMap->GetWrapT());
    piTexParam(eTexTarget_CubeMap, eTexConfig_WrapR, cubeMap->GetWrapR());
    piTexParam(eTexTarget_CubeMap, eTexConfig_MinFilter, cubeMap->GetMinFilter());
    piTexParam(eTexTarget_CubeMap, eTexConfig_MagFilter, cubeMap->GetMagFilter());
    
    string dir = piGetDirectory(uri);
    
    SmartPtr<iBitmapAsset> bmpPX;
    SmartPtr<iBitmapAsset> bmpNX;
    SmartPtr<iBitmapAsset> bmpPY;
    SmartPtr<iBitmapAsset> bmpNY;
    SmartPtr<iBitmapAsset> bmpPZ;
    SmartPtr<iBitmapAsset> bmpNZ;
    
    {
        string src = dir + cubeMap->GetPositiveX();
        bmpPX = piLoadAsset<iBitmapAsset>(assetManager, classLoader, src);
        if (bmpPX.IsNull())
        {
            goto err;
        }
    }
    
    {
        string src = dir + cubeMap->GetNegativeX();
        bmpNX = piLoadAsset<iBitmapAsset>(assetManager, classLoader, src);
        if (bmpNX.IsNull())
        {
            goto err;
        }
    }
    
    
    {
        string src = dir + cubeMap->GetPositiveY();
        bmpPY = piLoadAsset<iBitmapAsset>(assetManager, classLoader, src);
        if (bmpPY.IsNull())
        {
            goto err;
        }
    }
    
    {
        string src = dir + cubeMap->GetNegativeY();
        bmpNY = piLoadAsset<iBitmapAsset>(assetManager, classLoader, src);
        if (bmpNY.IsNull())
        {
            goto err;
        }
    }
    
    {
        string src = dir + cubeMap->GetPositiveZ();
        bmpPZ = piLoadAsset<iBitmapAsset>(assetManager, classLoader, src);
        if (bmpPZ.IsNull())
        {
            goto err;
        }
    }
    
    {
        string src = dir + cubeMap->GetNegativeZ();
        bmpNZ = piLoadAsset<iBitmapAsset>(assetManager, classLoader, src);
        if (bmpNZ.IsNull())
        {
            goto err;
        }
    }
    
    
    piTexImage2D(eTexTarget_CubeMapPositiveX, 0, cubeMap->GetFormat(), bmpPX->GetBitmap(), 0);
    piTexImage2D(eTexTarget_CubeMapNegativeX, 0, cubeMap->GetFormat(), bmpNX->GetBitmap(), 0);
    piTexImage2D(eTexTarget_CubeMapPositiveY, 0, cubeMap->GetFormat(), bmpPY->GetBitmap(), 0);
    piTexImage2D(eTexTarget_CubeMapNegativeY, 0, cubeMap->GetFormat(), bmpNY->GetBitmap(), 0);
    piTexImage2D(eTexTarget_CubeMapPositiveZ, 0, cubeMap->GetFormat(), bmpPZ->GetBitmap(), 0);
    piTexImage2D(eTexTarget_CubeMapNegativeZ, 0, cubeMap->GetFormat(), bmpNZ->GetBitmap(), 0);

    piBindTexture(eTexTarget_CubeMap, 0);
    
    cubeMap->SetGraphicsName(name);
    
    return true;
    
err:
    piReleaseGraphicsObject(name);
    return false;
}

bool BuildRenderTexture(const std::string& uri, iAsset* asset, iAssetManager* assetManager, iClassLoader* classLoader)
{
    if (GetRenderTexture(uri) != nullptr)
        return true;
    
    SmartPtr<iRenderTexture> renderTexture = dynamic_cast<iRenderTexture*>(asset);
    piAssert(!renderTexture.IsNull(), false);
    
    renderTexture->DoCreate();
    
    AddToSourceRenderTexture(uri, renderTexture);
    
    return true;
}

NSPI_END()



























