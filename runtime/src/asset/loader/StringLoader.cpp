/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.20   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()


class StringLoader : public iAssetLoader
{
public:
    StringLoader()
    {
    }
    
    virtual ~StringLoader()
    {
    }
    
    virtual iAsset* Load(iAssetManager* manager, iClassLoader* classLoader, iStream* stream)
    {
        int32_t size = (int32_t)stream->GetSize();
        
        // add a '\0' to the end of the buffer.
        vector<char> buffer(size + 1);
        buffer[size] = '\0';
        
        stream->Read(buffer.data(), size);
        
        iMemory* mem = CreateMemoryCopy(buffer.data(), buffer.size());
        
        SmartPtr<iMemoryAsset> asset = CreateMemoryAsset();
        asset->SetMemory(mem);
        return asset.PtrAndSetNull();
    }

};


class StringLoaderFactory : public iAssetLoaderFactory
{
private:
    SmartPtr<iAssetLoader> mLoader;
    
public:
    StringLoaderFactory()
    {
        mLoader = new StringLoader();
    }
    
    virtual ~StringLoaderFactory()
    {
    }
    
    virtual iAssetLoader* CreateLoader(const string& fileExt, iStream* stream)
    {
        return mLoader;
    }
};


iAssetLoaderFactory* CreateStringLoaderFactory()
{
    return new StringLoaderFactory();
}












NSPI_END()








