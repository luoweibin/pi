/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.8   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>
#include "../serialzation/JsonUtil.h"

using namespace std;

NSPI_BEGIN()


class ModelLoader : public iAssetLoader
{
public:
    ModelLoader()
    {
    }
    
    virtual ~ModelLoader()
    {
    }
    
    virtual iAsset* Load(iAssetManager* manager, iClassLoader* classLoader, iStream* stream)
    {
        SmartPtr<iUnserializer> unser = CreateJsonUnserializer(manager, classLoader);
        
        
        json o = ReadJsonFromStream(stream);
        
        piCheck(o.find("Model") != o.end(), nullptr);
        
        string dir = piGetDirectory(stream->GetUri());
        
        string modelUri = dir + o["Model"].get<string>();
        SmartPtr<iModelScene> modelScene = piLoadAsset<iModelScene>(manager, classLoader, modelUri);
        piCheck(!modelScene.IsNull(), nullptr);

        if (o.find("Materials") != o.end() && o["Materials"].is_array())
        {
            json mtls = o["Materials"];
            
            SmartPtr<iMaterialArray> mtlArray = CreateMaterialArray();
            
            for (auto it = mtls.begin(); it != mtls.end(); ++it)
            {
                json value = it.value();
                
                SmartPtr<iMaterial> mtl = CreateMaterial();

                if (value.find("Name") != value.end())
                {
                    mtl->SetName(value["Name"].get<string>());
                }
                
                string mesh;
                if (value.find("Mesh") != value.end())
                {
                    mesh = value["Mesh"].get<string>();
                }
                
                if (value.find("RenderType") != value.end())
                {
                    SmartPtr<iEnum> enumClass = classLoader->FindEnum("eRenderType");
                    if (!enumClass.IsNull())
                    {
                        int v = Json2Enum(enumClass, value["RenderType"].get<string>());
                        mtl->SetRenderType(v);
                    }
                }
                
                if (value.find("RenderQueue") != value.end())
                {
                    mtl->SetRenderQueue(value["RenderQueue"].get<int32_t>());
                }
                
                if (value.find("Shader") != value.end())
                {
                    string shaderUri = value["Shader"].get<string>();
                    mtl->SetShaderUri(shaderUri);
                    
                    if (shaderUri.find("share://") != 0)
                    {
                        shaderUri = dir + shaderUri;
                    }
                    
                    SmartPtr<iShaderProgram> program = piLoadAsset<iShaderProgram>(manager, classLoader, shaderUri);
                    mtl->SetShader(program);
                }
                
                if (value.find("State") != value.end())
                {
                    string stateUri = value["State"].get<string>();
                    mtl->SetShaderUri(stateUri);
                    
                    if (stateUri.find("share://") != 0)
                    {
                        stateUri = dir + stateUri;
                    }
                    
                    SmartPtr<iRenderState> state = piLoadAsset<iRenderState>(manager, classLoader, stateUri);
                    mtl->SetRenderState(state);
                }
                else
                {
                    mtl->SetRenderState(CreateRenderState());
                }
                
                if (value.find("Props") != value.end() && value["Props"].is_array())
                {
                    SmartPtr<iMaterialPropArray> propArray = mtl->GetProps();
                    
                    json props = value["Props"];
                    for (auto p : props)
                    {
                        if (p.find("Name") == p.end() || p.find("Type") == p.end())
                        {
                            continue;
                        }
                        
                        string name = p["Name"];
                        SmartPtr<iMaterialProp> prop = CreateMaterialProp();
                        prop->SetName(name);
                        
                        string className = p["Type"].get<string>();
                        SmartPtr<iClass> klass = classLoader->FindClass(className);
                        if (klass.IsNull())
                        {
                            continue;
                        }
                        
                        if (p.find("Value") != p.end())
                        {
                            Var propValue = Json2VarHint(manager, classLoader, klass, p["Value"]);
                            prop->SetValue(propValue);
                        }
                        else if (p.find("Uri") != p.end())
                        {
                            string uri = dir + p["Uri"].get<string>();
                            SmartPtr<iAsset> asset = piLoadAsset<iAsset>(manager, classLoader, uri);
                            prop->SetValue(asset.Ptr());
                        }
                        else
                        {
                            prop->SetValue(klass->CreateInstance());
                        }
                        
                        propArray->PushBack(prop);
                    }
                }
                
                if (!mtl.IsNull())
                {
                    string name = mtl->GetName();
                    SmartPtr<iMaterialArray> modelMtls = modelScene->GetMaterials();
                    
                    SmartPtr<iMaterial> m = modelScene->FindMaterial(name);
                    if (!m.IsNull())
                    {
                        piRemoveOne<iMaterialArray, iMaterial*>(modelMtls, m);
                    }
                    
                    ReplaceMaterial(modelScene->GetRootNode(), m, mtl);
                
                    modelMtls->PushBack(mtl);
                }
            }
        }
        
        if (o.find("Meshes") != o.end() && o["Meshes"].is_array())
        {
            json meshes = o["Meshes"];
            
            for (auto it = meshes.begin(); it != meshes.end(); ++it)
            {
                json& o = *it;
                
                if (o.find("Name") == o.end() || o.find("Material") == o.end())
                {
                    continue;
                }
                
                string meshName = o["Name"].get<string>();
                SmartPtr<iModelMesh> mesh = modelScene->FindMesh(meshName);
                if (mesh.IsNull())
                {
                    continue;
                }
                
                string mattName = o["Material"];
                SmartPtr<iMaterial> matt = modelScene->FindMaterial(mattName);
                if (matt.IsNull())
                {
                    continue;
                }
                
                mesh->SetMaterial(matt);
            }
        }
        
        return modelScene.PtrAndSetNull();
    }
    
private:
    
    void ReplaceMaterial(iModelNode* node, iMaterial* origin, iMaterial* target)
    {
        SmartPtr<iModelMeshArray> meshes = node->GetMeshes();
        
        for (auto i = 0; i < meshes->GetCount(); ++i)
        {
            SmartPtr<iModelMesh> mesh = meshes->GetItem(i);
            SmartPtr<iMaterial> matt = mesh->GetMaterial();
            if (matt == origin)
            {
                mesh->SetMaterial(target);
            }
        }
        
        SmartPtr<iModelNodeArray> children = node->GetChildren();
        for (auto i = 0; i < children->GetCount(); ++i)
        {
            SmartPtr<iModelNode> child = children->GetItem(i);
            ReplaceMaterial(child, origin, target);
        }
    }
};


class ModelLoaderFactory : public iAssetLoaderFactory
{
private:
    SmartPtr<iAssetLoader> mLoader;
    
public:
    ModelLoaderFactory()
    {
        mLoader = new ModelLoader();
    }
    
    virtual ~ModelLoaderFactory()
    {
    }
    
    virtual iAssetLoader* CreateLoader(const string& fileExt, iStream* stream)
    {
        return mLoader;
    }
};


iAssetLoaderFactory* CreateModelLoaderFactory()
{
    return new ModelLoaderFactory();
}

NSPI_END()

























