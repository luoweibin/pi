/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.8   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>
#include "../../asset/serialzation/JsonUtil.h"

using namespace std;

NSPI_BEGIN()

static int InterpretCaps(const std::string& cap)
{
    if (cap == "FacialLandmarks")
    {
        return eSceneCap_FacialLandmarks;
    }
    else if (cap == "Calibrate")
    {
        return eSceneCap_Calibrate;
    }
    else
    {
        return -1;
    }
}

static iEntity* ParseEntity(iAssetManager* manager, iClassLoader* loader, const json& e)
{
    SmartPtr<iEntity> entity = CreateEntity();
    
    if (e.find("Name") != e.end())
    {
        entity->SetName(e["Name"].get<string>());
    }
    
    if (e.find("Tag") != e.end())
    {
        SmartPtr<iEnum> enumClass = loader->FindEnum("eTag");
        if (!enumClass.IsNull())
        {
            int tag = Json2Enum(enumClass, e["Tag"]);
            entity->SetTag(tag);
        }
    }
    
    if (e.find("Active") != e.end())
    {
        entity->SetActive(e["Active"].get<bool>());
    }
    
    if (e.find("Children") != e.end())
    {
        for (auto child : e["Children"])
        {
            SmartPtr<iEntity> childEntity = ParseEntity(manager, loader, child);
            if (!childEntity.IsNull())
            {
                entity->AddChild(childEntity);
            }
        }
    }
    
    piCheck(e.find("Comps") != e.end(), entity.PtrAndSetNull());
    
    const auto& comps = e["Comps"];
    piCheck(comps.is_array(), entity.PtrAndSetNull());
    
    for (auto it = comps.begin(); it != comps.end(); ++it)
    {
        const auto& v = it.value();
        if (!v.is_object())
        {
            continue;
        }
        
        SmartPtr<iComponent> comp = dynamic_cast<iComponent*>(Json2Object(manager, loader, v));
        if (!comp.IsNull())
        {
            entity->AddComp(comp);
        }
    }
    
    return entity.PtrAndSetNull();
}

class SceneLoader : public iAssetLoader
{    
public:
    SceneLoader()
    {
    }
    
    virtual ~SceneLoader()
    {
    }
    
    virtual iAsset* Load(iAssetManager* manager, iClassLoader* classLoader, iStream* stream)
    {
        int64_t begin = piGetSystemTimeMS();
        
        json o = ReadJsonFromStream(stream);
        SmartPtr<iScene> scene = CreateScene();
        
        if (o.find("DesignSize") != o.end())
        {
            json v = o["DesignSize"];
            vec2 size = JsonArray2Vec2(v);
            scene->SetDesignSize(size);
        }
        
        if (o.find("XScaling") != o.end())
        {
            json v = o["XScaling"];
            scene->SetXScaling(v.get<float>());
        }
        
        ParseDatabase(manager, classLoader, o);
        
        ParseCaps(scene, classLoader, o);
        
        ParseSystems(scene, manager, classLoader, o);
        
        ParseEntities(scene, manager, classLoader, o);
        
        PILOGI(PI_GAME_TAG, "Timecost for loading scene:%lldMS", piGetSystemTimeMS() - begin);
        
        return scene.PtrAndSetNull();
    }
    
private:
    
    void ParseDatabase(iAssetManager* manager, iClassLoader* loader, const json& o)
    {
        piCheck(o.find("Database") != o.end(), ;);
        
        const auto& database = o["Database"];
        piCheck(database.is_object(), ;);
        
        for (auto it = database.begin(); it != database.end(); ++it)
        {
            string uri = it.key();
            
            SmartPtr<iRefObject> value = Json2Object(manager, loader, it.value());
            if (!value.IsNull())
            {
                manager->SetAsset(uri, value.Ptr());
            }
        }
    }
    
    void ParseCaps(iScene* scene, iClassLoader* loader, const json& o)
    {
        piCheck(o.find("Caps") != o.end(), ;);
        
        const auto& caps = o["Caps"];
        piCheck(caps.is_array(), ;);
        
        SmartPtr<iI32Array> capArray = scene->GetCaps();
        
        for (auto it = caps.begin(); it != caps.end(); ++it)
        {
            auto v = it.value();
            capArray->PushBack(InterpretCaps(v));
        }
    }
    
    void ParseSystems(iScene* scene, iAssetManager* manager, iClassLoader* loader, const json& o)
    {
        piCheck(o.find("Systems") != o.end(), ;);
        const auto& systems = o["Systems"];
        piCheck(systems.is_array(), ;);
        
        for (auto it = systems.begin(); it != systems.end(); ++it)
        {
            const auto& s = it.value();
            if (!s.is_object())
            {
                continue;
            }
            
            SmartPtr<iSystem> system = dynamic_cast<iSystem*>(Json2Object(manager, loader, s));
            if (!system.IsNull())
            {
                scene->AddSystem(system);
            }
        }
    }
    
    void ParseEntities(iScene* scene, iAssetManager* manager, iClassLoader* loader, const json& o)
    {
        piCheck(o.find("Entities") != o.end(), ;);
        const auto& entities = o["Entities"];
        piCheck(entities.is_array(), ;);
        
        SmartPtr<iEntity> rootEntity = CreateEntity();
        
        rootEntity->SetName("Root");
        
        for (auto it = entities.begin(); it != entities.end(); ++it)
        {
            const auto& e = it.value();
            if (!e.is_object())
            {
                continue;
            }
            
            SmartPtr<iEntity> entity = ParseEntity(manager, loader, e);
            if (!entity.IsNull())
            {
                rootEntity->AddChild(entity);
            }
        }
        
        scene->SetRootEntity(rootEntity);
        scene->AddEntityRecursively(rootEntity);
    }
};

class SceneLoaderFactory : public iAssetLoaderFactory
{
private:
    SmartPtr<iAssetLoader> mLoader;
    
public:
    SceneLoaderFactory()
    {
        mLoader = new SceneLoader();
    }
    
    virtual ~SceneLoaderFactory()
    {
    }
    
    iAssetLoader* CreateLoader(const string& fileExt, iStream* stream)
    {
        return mLoader;
    }
};


iAssetLoaderFactory* CreateSceneLoaderFactory()
{
    return new SceneLoaderFactory();
}


NSPI_END()







