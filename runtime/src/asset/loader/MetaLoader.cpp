/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.18   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>
#include "../AssetBuilder.h"

using namespace std;

NSPI_BEGIN()


class MetaLoader : public iAssetLoader
{
private:
    typedef map<iClass*, AssetBuilder> BuilderMap;
    BuilderMap mBuilderMap;
    
public:
    MetaLoader()
    {
        mBuilderMap[iTexture2D::StaticClass()] = BuildTexture2D;
        mBuilderMap[iCubeMap::StaticClass()] = BuildCubMap;
        mBuilderMap[iRenderTexture::StaticClass()] = BuildRenderTexture;
        mBuilderMap[iShaderProgram::StaticClass()] = BuildShader;
        mBuilderMap[iRenderState::StaticClass()] = BuildRenderState;
        mBuilderMap[iParticleEmitterLib::StaticClass()] = BuildParticleEmitterLib;
    }
    
    virtual ~MetaLoader()
    {
    }
    
    virtual iAsset* Load(iAssetManager* manager, iClassLoader* classLoader, iStream* stream)
    {
        SmartPtr<iUnserializer> unser = CreateJsonUnserializer(manager, classLoader);
        SmartPtr<iRefObject> obj = unser->Read(stream).GetObject();
        piAssert(!obj.IsNull(), nullptr);
        
        SmartPtr<iAsset> asset = dynamic_cast<iAsset*>(obj.Ptr());
        piAssert(!asset.IsNull(), nullptr);
        
        SmartPtr<iClass> klass = obj->GetClass();
        auto it = mBuilderMap.find(klass);
        if (it != mBuilderMap.end())
        {
            auto func = it->second;
            piAssert(func(stream->GetUri(), asset, manager, classLoader), nullptr);
        }

        obj.PtrAndSetNull();
        return asset.PtrAndSetNull();
    }
};


class MetaLoaderFactory : public iAssetLoaderFactory
{
private:
    SmartPtr<iAssetLoader> mLoader;
    
public:
    MetaLoaderFactory()
    {
        mLoader = new MetaLoader();
    }
    
    virtual ~MetaLoaderFactory()
    {
    }
    
    virtual iAssetLoader* CreateLoader(const std::string& fileExt, iStream* stream)
    {
        piCheck(fileExt == "meta", nullptr);
        return mLoader;
    }
};

iAssetLoaderFactory* CreateMetaLoaderFactory()
{
    return new MetaLoaderFactory();
}


NSPI_END()
























