/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.30   0.1     Create
 ******************************************************************************/
#include "AssetImpl.h"

using namespace std;

NSPI_BEGIN()


class AnimSeq : public AssetImpl<iAnimSeq>
{
private:
    int32_t mFPS;
    SmartPtr<iTexture2DArray> mFrames;
    
public:
    AnimSeq():
    mFPS(30)
    {
        mFrames = CreateTexture2DArray();
    }
    
    virtual ~AnimSeq()
    {
    }
    
    virtual int32_t GetFPS() const
    {
        return mFPS;
    }
    
    virtual void SetFPS(int32_t value)
    {
        piAssert(value > 0, ;);
        mFPS = value;
    }
    
    virtual void SetFrames(iTexture2DArray* frames)
    {
        piAssert(frames != nullptr, ;);
        mFrames = frames;
    }
    
    virtual iTexture2DArray* GetFrames() const
    {
        return mFrames;
    }
    
    virtual iTexture2D* GetFrame(int32_t index) const
    {
        return mFrames->GetItem(index);
    }
};


iAnimSeq* CreateAnimSeq()
{
    return new AnimSeq();
}


NSPI_END()






























