/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.19   0.1     Create
 ******************************************************************************/
#include "AssetImpl.h"

using namespace std;

NSPI_BEGIN()

class BitmapAsset : public AssetImpl<iBitmapAsset>
{
private:
    SmartPtr<iBitmap> mBmp;
    
public:
    BitmapAsset()
    {
    }
    
    virtual ~BitmapAsset()
    {
    }
    
    virtual iBitmap* GetBitmap() const
    {
        return mBmp;
    }
    
    virtual void SetBitmap(iBitmap* bitmap)
    {
        piAssert(bitmap != nullptr, ;);
        piAssert(!IsReadonly(), ;);
        mBmp = bitmap;
    }
};

iBitmapAsset* CreateBitmapAsset()
{
    return new BitmapAsset();
}

NSPI_END()






















