/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.18   1.0     Create
 ******************************************************************************/
#ifndef PI_SRC_GAME_ASSET_ASSETBUILDER_H
#define PI_SRC_GAME_ASSET_ASSETBUILDER_H

#include <pi/Game.h>

namespace nspi
{
    
    typedef bool (*AssetBuilder)(const std::string&, iAsset*, iAssetManager*, iClassLoader*);
    
    bool BuildTexture2D(const std::string& uri, iAsset* asset, iAssetManager* assetManager, iClassLoader* classLoader);
    bool BuildCubMap(const std::string& uri, iAsset* asset, iAssetManager* assetManager, iClassLoader* classLoader);
    bool BuildRenderTexture(const std::string& uri, iAsset* asset, iAssetManager* assetManager, iClassLoader* classLoader);
    bool BuildShader(const std::string& uri, iAsset* asset, iAssetManager* assetManager, iClassLoader* classLoader);
    bool BuildRenderState(const std::string& uri, iAsset* asset, iAssetManager* assetManager, iClassLoader* classLoader);
    bool BuildParticleEmitterLib(const std::string& uri, iAsset* asset, iAssetManager* assetManager, iClassLoader* classLoader);
    
}


#endif
