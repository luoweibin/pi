/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.4.1   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()

class ShareResolver : public iUriResolver
{
private:
    string mRoot;
    
public:
    ShareResolver(const string& root)
    {
        mRoot = piTrimRight(root, "/");
        if (!mRoot.empty())
        {
            mRoot += "/";
        }
    }
    
    virtual ~ShareResolver()
    {
    }
    
    virtual iStream* Open(const std::string& uri)
    {
        piCheck(!uri.empty(), nullptr);
        
        piCheck(uri.find("share://") == 0, nullptr);
        
        string absPath = Resolve(uri);
        return CreateFileStream(absPath, "r");
    }
    
    virtual string Resolve(const string& path)
    {
        piAssert(!path.empty(), string());
        piCheck(path.find("share://") == 0, string());
        string absPath = mRoot + path.substr(sizeof("share://") - 1);
        return piFileExist(absPath) ? absPath : "";
    }
    
    virtual string ToString() const
    {
        return piFormat("ShareResolver{root:\"%s\"}", mRoot.substr(0, mRoot.length() - 1).c_str());
    }
};


iUriResolver* CreateShareResolver(const string& dir)
{
    return new ShareResolver(dir);
}



NSPI_END()


















