/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.4.13   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>

extern "C"
{
    #include <unzip.h>
}

using namespace std;

NSPI_BEGIN()


static void* open64_file_func(void* opaque, const void* filename, int mode)
{
    iStream* s = (iStream*)opaque;
    piAssert(s != nullptr, nullptr);
    return s;
}

static int close_file_func(void* opaque, void* stream)
{
    return 0;
}

static uLong read_file_func(void* opaque, void* stream, void* buf, uLong size)
{
    iStream* s = (iStream*)stream;
    piAssert(s != nullptr, -1);
    return (uLong)s->Read(buf, size);
}

static uLong write_file_func(void* opaque, void* stream, const void* buf, uLong size)
{
    iStream* s = (iStream*)stream;
    piAssert(s != nullptr, -1);
    return (uLong)s->Write(buf, size);
}

static int testerror_file_func(void* opaque, void* stream)
{
    return 0;
}


static long seek64_file_func(void* opaque, void* stream, ZPOS64_T offset, int origin)
{
    iStream* s = (iStream*)stream;
    piAssert(s != nullptr, -1);
    
    int mode = eSeek_Current;
    switch (origin)
    {
        case ZLIB_FILEFUNC_SEEK_END:
            mode = eSeek_End;
            break;
        case ZLIB_FILEFUNC_SEEK_SET:
            mode = eSeek_Set;
            break;
        case ZLIB_FILEFUNC_SEEK_CUR:
        default:
            mode = eSeek_Current;
            break;
    }
    
    return s->Seek(offset, mode) != -1 ? 0 : -1;
}

ZPOS64_T tell64_file_func(void* opaque, void* stream)
{
    iStream* s = (iStream*)stream;
    piAssert(s != nullptr, -1);
    return s->GetOffset();
}


class ZipStream : public cStreamImpl<iStream>
{
private:
    unzFile mFile;
    string mUri;
    int64_t mSize;
    
public:
    ZipStream(unzFile file, const string& uri, int64_t size):
    mFile(file), mUri(uri), mSize(size)
    {
    }
    
    virtual ~ZipStream()
    {
        unzCloseCurrentFile(mFile);
        mFile = nullptr;
    }
    
    virtual string GetUri() const
    {
        return mUri;
    }
    
    virtual int64_t GetSize() const
    {
        return mSize;
    }
    
    virtual int64_t Seek(int64_t offset, int seek)
    {
        PILOGW(PI_TAG, "Seeking in commpressed zip is unsupported. Path:%s", mUri.c_str());
        return -1;
    }
    
    virtual int64_t GetOffset() const
    {
        return unzseek64(mFile, 0, ZLIB_FILEFUNC_SEEK_END);
    }
    
    virtual bool End() const
    {
        return unzeof(mFile);
    }
    
    virtual int64_t Read(void* pBuffer, int64_t size)
    {
        return unzReadCurrentFile(mFile, pBuffer, (unsigned int)size);
    }
    
    virtual int64_t Write(const void* pData, int64_t size)
    {
        return -1;
    }
};


class ZipResolver : public iUriResolver
{
private:
    SmartPtr<iStream> mSrc;
    unzFile mFile;
    
    typedef map<string, bool> FileMap;
    FileMap mFileMap;
    
public:
    ZipResolver(iStream* source):
    mSrc(source), mFile(nullptr)
    {
    }
    
    virtual ~ZipResolver()
    {
        if (mFile != nullptr)
        {
            unzClose(mFile);
            mFile = nullptr;
        }
    }
    
    bool Init()
    {
        zlib_filefunc64_def io;
        io.zopen64_file = open64_file_func;
        io.zclose_file = close_file_func;
        io.zerror_file = testerror_file_func;
        io.zread_file = read_file_func;
        io.zwrite_file = write_file_func;
        io.zseek64_file = seek64_file_func;
        io.ztell64_file = tell64_file_func;
        io.opaque = mSrc.Ptr();
        
        mFile = unzOpen2_64(nullptr, &io);
        piAssert(mFile != nullptr, false);
        
        int ret = unzGoToFirstFile(mFile);
        piCheck(ret == UNZ_OK, false);
        
        while (ret == UNZ_OK)
        {
            // get zipped file info
            unz_file_info info;
            char fileName[512];
            if (unzGetCurrentFileInfo(mFile, &info, fileName, 512, NULL, 0, NULL, 0) != UNZ_OK)
            {
                break;
            }
            
            if (info.flag == 8)
            {
                mFileMap[fileName] = true;
            }
            
            ret = unzGoToNextFile(mFile);
        }
        
        return true;
    }
    
    virtual iStream* Open(const std::string& uri)
    {
        piCheck(!uri.empty(), nullptr);

        piCheck(mFileMap.find(uri) != mFileMap.end(), nullptr);
        
        piCheck(unzLocateFile(mFile, uri.c_str(), nullptr) == UNZ_OK, nullptr);

        piCheck(unzOpenCurrentFile(mFile) == UNZ_OK, nullptr);
        
        unz_file_info info;
        char fileName[512];
        piCheck(unzGetCurrentFileInfo(mFile, &info, fileName, 512, NULL, 0, NULL, 0) == UNZ_OK, nullptr);
        
        return new ZipStream(mFile, uri, info.uncompressed_size);
    }
    
    virtual string Resolve(const string& path)
    {
        return "";
    }
    
    virtual string ToString() const
    {
        return piFormat("ZipResolver{%p}", this);
    }
};


iUriResolver* CreateZipResolver(iStream* source)
{
    SmartPtr<ZipResolver> resolver = new ZipResolver(source);
    if (resolver->Init())
    {
        return resolver.PtrAndSetNull();
    }
    else
    {
        return nullptr;
    }
}



NSPI_END()


















