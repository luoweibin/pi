/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.23   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()

class DirectoryResolver : public iUriResolver
{
private:
    string mRoot;
    
public:
    DirectoryResolver(const string& root)
    {
        mRoot = piTrimRight(root, "/");
        if (!mRoot.empty())
        {
            mRoot += "/";
        }
    }
    
    virtual ~DirectoryResolver()
    {
    }
    
    virtual string Resolve(const string& path)
    {
        piAssert(!path.empty(), string());
        string ret = mRoot + path;
        return piFileExist(ret) ? ret : "";
    }
    
    virtual iStream* Open(const std::string& uri)
    {
        piCheck(!uri.empty(), nullptr);
        string absPath = Resolve(uri);
        return CreateFileStream(absPath, "r");
    }
    
    virtual string ToString() const
    {
        return piFormat("DirectoryResolver{root:\"%s\"}", mRoot.substr(0, mRoot.length() - 1).c_str());
    }
};


iUriResolver* CreateDirectoryResolver(const string& dir)
{
    return new DirectoryResolver(dir);
}

NSPI_END()


















