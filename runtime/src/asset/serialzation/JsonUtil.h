/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.9   1.0     Create
 ******************************************************************************/
#ifndef PI_SRC_GAME_ASSET_JSONUTIL_H
#define PI_SRC_GAME_ASSET_JSONUTIL_H

#include <pi/Game.h>

#include <json.h>

using namespace std;
using namespace nlohmann;

namespace nspi
{
    Var Json2VarGuess(iAssetManager* assetMgr, iClassLoader* loader, const json& v);

    Var Json2VarHint(iAssetManager* assetMgr, iClassLoader* loader, iClass* klass, const json& v);
    
    iRefObject* Json2Object(iAssetManager* assetMgr, iClassLoader* classLoader, const json& v);
    
    iRefObject* Json2ObjectHint(iAssetManager* assetMgr, iClassLoader* classLoader, iClass* klass, const json& v);
    
    int Json2Enum(iEnum* enumClass, const string& v);
    
    vec2 JsonArray2Vec2(const json& v);
    vec3 JsonArray2Vec3(const json& v);
    vec4 JsonArray2Vec4(const json& v);
    rect JsonArray2Rect(const json& v);
    quat JsonArray2Quat(const json& v);
    mat4 JsonArray2Mat4(const json& v);
    
    json ReadJsonFromStream(iStream* stream);
}

#endif
