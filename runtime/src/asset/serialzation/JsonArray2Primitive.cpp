/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.13   1.0     Create
 ******************************************************************************/
#include "JsonUtil.h"

NSPI_BEGIN()

vec2 JsonArray2Vec2(const json& v)
{
    float v0 = v.size() > 0 ? v[0].get<float>() : 0;
    float v1 = v.size() > 1 ? v[1].get<float>() : 0;
    return vec2(v0, v1);
}

vec3 JsonArray2Vec3(const json& v)
{
    float v0 = v.size() > 0 ? v[0].get<float>() : 0;
    float v1 = v.size() > 1 ? v[1].get<float>() : 0;
    float v2 = v.size() > 2 ? v[2].get<float>() : 0;
    return vec3(v0, v1, v2);
}

vec4 JsonArray2Vec4(const json& v)
{
    float v0 = v.size() > 0 ? v[0].get<float>() : 0;
    float v1 = v.size() > 1 ? v[1].get<float>() : 0;
    float v2 = v.size() > 2 ? v[2].get<float>() : 0;
    float v3 = v.size() > 3 ? v[3].get<float>() : 0;
    return vec4(v0, v1, v2, v3);
}

rect JsonArray2Rect(const json& v)
{
    float v0 = v.size() > 0 ? v[0].get<float>() : 0;
    float v1 = v.size() > 1 ? v[1].get<float>() : 0;
    float v2 = v.size() > 2 ? v[2].get<float>() : 0;
    float v3 = v.size() > 3 ? v[3].get<float>() : 0;
    return rect(v0, v1, v2, v3);
}

quat JsonArray2Quat(const json& v)
{
    float v0 = v.size() > 0 ? v[0].get<float>() : 0;
    float v1 = v.size() > 1 ? v[1].get<float>() : 0;
    float v2 = v.size() > 2 ? v[2].get<float>() : 0;
    float v3 = v.size() > 3 ? v[3].get<float>() : 0;
    return quat(v0, v1, v2, v3);
}

mat4 JsonArray2Mat4(const json& v)
{
    float v0 = v.size() > 0 ? v[0].get<float>() : 0;
    float v1 = v.size() > 1 ? v[1].get<float>() : 0;
    float v2 = v.size() > 2 ? v[2].get<float>() : 0;
    float v3 = v.size() > 3 ? v[3].get<float>() : 0;
    float v4 = v.size() > 4 ? v[4].get<float>() : 0;
    float v5 = v.size() > 5 ? v[5].get<float>() : 0;
    float v6 = v.size() > 6 ? v[6].get<float>() : 0;
    float v7 = v.size() > 7 ? v[7].get<float>() : 0;
    float v8 = v.size() > 8 ? v[8].get<float>() : 0;
    float v9 = v.size() > 9 ? v[9].get<float>() : 0;
    float v10 = v.size() > 10 ? v[10].get<float>() : 0;
    float v11 = v.size() > 11 ? v[11].get<float>() : 0;
    float v12 = v.size() > 12 ? v[12].get<float>() : 0;
    float v13 = v.size() > 13 ? v[13].get<float>() : 0;
    float v14 = v.size() > 14 ? v[14].get<float>() : 0;
    float v15 = v.size() > 15 ? v[15].get<float>() : 0;
    return mat4(v0,  v1,  v2,  v3,
                v4,  v5,  v6,  v7,
                v8,  v9,  v10, v11,
                v12, v13, v14, v15);
}

NSPI_END()













