/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.13   1.0     Create
 ******************************************************************************/
#include "JsonUtil.h"

NSPI_BEGIN()


int Json2Enum(iEnum* enumClass, const string& literial)
{
    int ret = 0;
    
    auto segs = piStrSplit(literial.c_str(), literial.size(), '|');
    for (const auto& s : segs)
    {
        int v = enumClass->GetValue(s, 0);
        ret |= v;
    }
    
    return ret;
}


static iVarArray* Json2VarArray(iAssetManager* assetMgr, iClassLoader* loader, const json& v)
{
    SmartPtr<iVarArray> array = CreateVarArray();
    for (auto i = 0; i < v.size(); ++i)
    {
        Var var = Json2VarGuess(assetMgr, loader, v[i]);
        array->PushBack(var);
    }
    return array.PtrAndSetNull();
}

static iI8Array* Json2I8Array(const json& v)
{
    SmartPtr<iI8Array> array = CreateI8Array();
    for (auto i = 0; i < v.size(); ++i)
    {
        array->PushBack(v.get<int8_t>());
    }
    return array.PtrAndSetNull();
}

static iU8Array* Json2U8Array(const json& v)
{
    SmartPtr<iU8Array> array = CreateU8Array();
    for (auto i = 0; i < v.size(); ++i)
    {
        array->PushBack(v[i].get<uint8_t>());
    }
    return array.PtrAndSetNull();
}

static iI16Array* Json2I16Array(const json& v)
{
    SmartPtr<iI16Array> array = CreateI16Array();
    for (auto i = 0; i < v.size(); ++i)
    {
        array->PushBack(v[i].get<int16_t>());
    }
    return array.PtrAndSetNull();
}

static iU16Array* Json2U16Array(const json& v)
{
    SmartPtr<iU16Array> array = CreateU16Array();
    for (auto i = 0; i < v.size(); ++i)
    {
        array->PushBack(v[i].get<uint16_t>());
    }
    return array.PtrAndSetNull();
}

static iI32Array* Json2I32Array(const json& v)
{
    SmartPtr<iI32Array> array = CreateI32Array();
    for (auto i = 0; i < v.size(); ++i)
    {
        array->PushBack(v[i].get<int32_t>());
    }
    return array.PtrAndSetNull();
}

static iU32Array* Json2U32Array(const json& v)
{
    SmartPtr<iU32Array> array = CreateU32Array();
    for (auto i = 0; i < v.size(); ++i)
    {
        array->PushBack(v[i].get<uint32_t>());
    }
    return array.PtrAndSetNull();
}

static iI64Array* Json2I64Array(const json& v)
{
    SmartPtr<iI64Array> array = CreateI64Array();
    for (auto i = 0; i < v.size(); ++i)
    {
        array->PushBack(v[i].get<int64_t>());
    }
    return array.PtrAndSetNull();
}

static iU64Array* Json2U64Array(const json& v)
{
    SmartPtr<iU64Array> array = CreateU64Array();
    for (auto i = 0; i < v.size(); ++i)
    {
        array->PushBack(v[i].get<uint64_t>());
    }
    return array.PtrAndSetNull();
}

static iF32Array* Json2F32Array(const json& v)
{
    SmartPtr<iF32Array> array = CreateF32Array();
    for (auto i = 0; i < v.size(); ++i)
    {
        array->PushBack(v[i].get<float>());
    }
    return array.PtrAndSetNull();
}

static iF64Array* Json2F64Array(const json& v)
{
    SmartPtr<iF64Array> array = CreateF64Array();
    for (auto i = 0; i < v.size(); ++i)
    {
        array->PushBack(v[i].get<double>());
    }
    return array.PtrAndSetNull();
}

static Var Json2VarObjectGuess(iAssetManager* assetMgr, iClassLoader* classLoader, const json& v)
{
    SmartPtr<iClass> klass;
    string className;
    if (v.find("Type") != v.end())
    {
        className = v["Type"];
        klass = classLoader->FindClass(className);
    }
    
    if (!klass.IsNull())
    {
        if (klass == PrimitiveClass::Vec2())
        {
            vec2 value;
            
            if (v.find("X") != v.end())
            {
                value.x = v["X"].get<float>();
            }
            
            if (v.find("Y") != v.end())
            {
                value.y = v["Y"].get<float>();
            }
            
            return value;
        }
        else if (klass == PrimitiveClass::Vec3())
        {
            vec3 value;
            
            if (v.find("X") != v.end())
            {
                value.x = v["X"].get<float>();
            }
            
            if (v.find("Y") != v.end())
            {
                value.y = v["Y"].get<float>();
            }
            
            if (v.find("Z") != v.end())
            {
                value.z = v["Z"].get<float>();
            }
            
            return value;
        }
        else if (klass == PrimitiveClass::Vec4())
        {
            vec4 value;
            
            if (v.find("X") != v.end())
            {
                value.x = v["X"].get<float>();
            }
            
            if (v.find("Y") != v.end())
            {
                value.y = v["Y"].get<float>();
            }
            
            if (v.find("Z") != v.end())
            {
                value.z = v["Z"].get<float>();
            }
            
            if (v.find("W") != v.end())
            {
                value.w = v["W"].get<float>();
            }
            
            return value;
        }
        else if (klass == PrimitiveClass::Rect())
        {
            rect value;
            
            if (v.find("X") != v.end())
            {
                value.x = v["X"].get<float>();
            }
            
            if (v.find("Y") != v.end())
            {
                value.y = v["Y"].get<float>();
            }
            
            if (v.find("Width") != v.end())
            {
                value.width = v["Width"].get<float>();
            }
            
            if (v.find("Height") != v.end())
            {
                value.height = v["Height"].get<float>();
            }
            
            return value;
        }
        else if (klass == PrimitiveClass::Quat())
        {
            quat value;
            
            if (v.find("X") != v.end())
            {
                value.x = v["X"].get<float>();
            }
            
            if (v.find("Y") != v.end())
            {
                value.y = v["Y"].get<float>();
            }
            
            if (v.find("Z") != v.end())
            {
                value.z = v["Z"].get<float>();
            }
            
            if (v.find("W") != v.end())
            {
                value.w = v["W"].get<float>();
            }
            
            return value;
        }
        else if (klass == PrimitiveClass::Mat4())
        {
            if (v.is_array())
            {
                float values[16] = {
                    1, 0, 0, 0,
                    0, 1, 0, 0,
                    0, 0, 0, 1,
                    0, 0, 0, 1,
                };
                
                for (int i = 0; i < piMin(16, v.size()); ++i)
                {
                    values[i] = v[i].get<float>();
                }
                
                return mat4(values[0],  values[1],  values[2],  values[3],
                            values[4],  values[5],  values[6],  values[7],
                            values[8],  values[9],  values[10], values[11],
                            values[12], values[13], values[14], values[15]);
            }
            else
            {
                return mat4();
            }
        }
        else
        {
            return Json2ObjectHint(assetMgr, classLoader, klass, v);
        }
    }
    else
    {
        SmartPtr<iTable> t = CreateTable();
        for (auto it = v.begin(); it != v.end(); ++it)
        {
            string key = it.key();
            Var value = Json2VarGuess(assetMgr, classLoader, it.value());
            t->Set(key, value);
        }
        return t.PtrAndSetNull();
    }
}

Var Json2VarGuess(iAssetManager* assetMgr, iClassLoader* loader, const json& v)
{
    switch (v.type())
    {
        case json::value_t::boolean:
            return v.get<bool>();
        case json::value_t::number_float:
            return v.get<double>();
        case json::value_t::number_integer:
            return v.get<int64_t>();
        case json::value_t::number_unsigned:
            return v.get<uint64_t>();
        case json::value_t::string:
            return v.get<string>();
        case json::value_t::array:
            return Json2VarArray(assetMgr, loader, v);
        case json::value_t::object:
            return Json2VarObjectGuess(assetMgr, loader, v);
        case json::value_t::null:
        default:
            return Var();
    }
}


Var Json2VarHint(iAssetManager* assetMgr, iClassLoader* loader, iClass* klass, const json& v)
{
    switch (klass->GetType())
    {
        case eType_Boolean:
            return v.get<bool>();
        case eType_I8:
            return v.get<int8_t>();
        case eType_I16:
            return v.get<int16_t>();
        case eType_I32:
        case eType_Enum:
            return v.get<int32_t>();
        case eType_U8:
            return v.get<uint8_t>();
        case eType_U16:
            return v.get<uint16_t>();
        case eType_U32:
            return v.get<uint32_t>();
        case eType_I64:
            return v.get<int64_t>();
        case eType_U64:
            return v.get<uint64_t>();
        case eType_F32:
            return v.get<float>();
        case eType_F64:
            return v.get<double>();
        case eType_String:
            return v.get<string>();
        case eType_Vec2:
            return JsonArray2Vec2(v);
        case eType_Vec3:
            return JsonArray2Vec3(v);
        case eType_Vec4:
            return JsonArray2Vec4(v);
        case eType_Rect:
            return JsonArray2Rect(v);
        case eType_Quat:
            return JsonArray2Quat(v);
        case eType_Mat4:
            return JsonArray2Mat4(v);
        case eType_I8Array:
            return Json2I8Array(v);
        case eType_U8Array:
            return Json2U8Array(v);
        case eType_I16Array:
            return Json2I16Array(v);
        case eType_U16Array:
            return Json2U16Array(v);
        case eType_I32Array:
            return Json2I32Array(v);
        case eType_U32Array:
            return Json2U32Array(v);
        case eType_I64Array:
            return Json2I64Array(v);
        case eType_U64Array:
            return Json2U64Array(v);
        case eType_F32Array:
            return Json2F32Array(v);
        case eType_F64Array:
            return Json2F64Array(v);
        case eType_Object:
            return Json2Object(assetMgr, loader, v);
        default:
            return Var();
    }
}



NSPI_END()






























