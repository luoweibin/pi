/*******************************************************************************
 See copyright notice in LICENSE.
 
 
 History:
 luo weibin     2017.6.12   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>
#include "JsonUtil.h"

using namespace std;
using namespace nlohmann;

NSPI_BEGIN()

class JsonUnserializer : public iUnserializer
{
private:
    SmartPtr<iAssetManager> mAssetMgr;
    SmartPtr<iClassLoader> mClassLoader;
    
public:
    JsonUnserializer(iAssetManager* manager, iClassLoader* loader):
    mAssetMgr(manager), mClassLoader(loader)
    {
    }
    
    virtual ~JsonUnserializer()
    {
    }
    
    virtual Var Read(iStream* stream)
    {
        piAssert(stream != nullptr, Var());
        
        try
        {
            json value = ReadJsonFromStream(stream);
            
            if (value.is_object())
            {
                return Json2Object(mAssetMgr, mClassLoader, value);
            }
            else if (value.is_array())
            {
                return Var();
            }
            else
            {
                return ReadPrimitive(value);
            }
        }
        catch (std::invalid_argument e)
        {
            return Var();
        }
    }
    
    virtual Var ReadClass(iStream* stream, iClass* klass)
    {
        piAssert(stream != nullptr, Var());
        piAssert(klass != nullptr, Var());
        
        try
        {
            json value = ReadJsonFromStream(stream);
            return Json2ObjectHint(mAssetMgr, mClassLoader, klass, value);
        }
        catch (std::invalid_argument e)
        {
            return Var();
        }
    }
    
private:
    
    static Var ReadPrimitive(const json& v)
    {
        switch (v.type())
        {
            case json::value_t::boolean:
                return v.get<bool>();
            case json::value_t::number_float:
                return v.get<double>();
            case json::value_t::number_integer:
                return v.get<int64_t>();
            case json::value_t::number_unsigned:
                return v.get<uint64_t>();
            case json::value_t::string:
                return v.get<string>();
            case json::value_t::null:
            default:
                return Var();
        }
    }
};

iUnserializer* CreateJsonUnserializer(iAssetManager* assetManager, iClassLoader* classLoader)
{
    return new JsonUnserializer(assetManager, classLoader);
}










NSPI_END()

























