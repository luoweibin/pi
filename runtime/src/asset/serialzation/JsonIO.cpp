/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.13   1.0     Create
 ******************************************************************************/
#include "JsonUtil.h"

using namespace std;
using namespace nlohmann;

NSPI_BEGIN()

json ReadJsonFromStream(iStream* stream)
{
    vector<char> buffer;
    buffer.resize((size_t)stream->GetSize() + 1, 0);
    stream->Read(buffer.data(), stream->GetSize());
    return json::parse(buffer.begin(), buffer.end());
}

NSPI_END()




















