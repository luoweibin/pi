/*******************************************************************************
 See copyright notice in LICENSE.
 
 
 History:
 luo weibin     2017.6.12   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>


NSPI_BEGIN()

class JsonSerializer : public iSerializer
{
public:
    JsonSerializer()
    {
    }
    
    virtual ~JsonSerializer()
    {
    }
    
    virtual bool Write(iStream* stream, const Var& value)
    {
        return false;
    }
};

iSerializer* CreateJsonSerializer()
{
    return new JsonSerializer();
}

NSPI_END()

























