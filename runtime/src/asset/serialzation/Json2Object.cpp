/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.13   1.0     Create
 ******************************************************************************/
#include "JsonUtil.h"


NSPI_BEGIN()


iRefObject* Json2Object(iAssetManager* assetMgr, iClassLoader* classLoader, const json& v)
{
    SmartPtr<iClass> klass;
    string className;
    if (v.find("Type") != v.end())
    {
        className = v["Type"];
        klass = classLoader->FindClass(className);
    }
    
    if (!klass.IsNull())
    {
        return Json2ObjectHint(assetMgr, classLoader, klass, v);
    }
    else
    {
        SmartPtr<iTable> t = CreateTable();
        for (auto it = v.begin(); it != v.end(); ++it)
        {
            string key = it.key();
            Var value = Json2VarGuess(assetMgr, classLoader, it.value());
            t->Set(key, value);
        }
        return t.PtrAndSetNull();
    }
}

iRefObject* Json2ObjectHint(iAssetManager* assetMgr, iClassLoader* classLoader, iClass* klass, const json& v)
{
    SmartPtr<iRefObject> o = klass->CreateInstance().GetObject();
    
    for (auto it = v.begin(); it != v.end(); ++it)
    {
        string name = it.key();
        if (name == "Type")
        {
            continue;
        }
        
        SmartPtr<iProperty> prop = klass->GetProperty(name);
        if (prop.IsNull())
        {
            continue;
        }
        
        const auto& value = it.value();
        
        SmartPtr<iClass> propClass = prop->GetTypeClass();
        if (propClass.IsNull())
        {
            continue;
        }
        
        int type = propClass->GetType();
        if ((type == eType_I32 || type == eType_Enum) && value.is_string())
        {
            string className = prop->GetConfig("Class");
            SmartPtr<iEnum> enumClass = classLoader->FindEnum(className);
            if (!enumClass.IsNull())
            {
                int v = Json2Enum(enumClass, value.get<string>());
                prop->SetValue(o, v);
            }
            else
            {
                prop->SetValue(o, -1);
            }
        }
        else
        {
            if (propClass->IsArray())
            {
                SmartPtr<iRefObject> array = propClass->CreateInstance().GetObject();
                
                SmartPtr<iMethod> method = propClass->GetMethod("PushBack");
                SmartPtr<iClass> elClass = propClass->GetElementClass();
                
                for (auto item : value)
                {
                    Var ret = Json2VarHint(assetMgr, classLoader, elClass, item);
                    method->Call(array, ret);
                }
                
                prop->SetValue(o, array.Ptr());
            }
            else
            {
                if (propClass != PrimitiveClass::Var())
                {
                    Var ret = Json2VarHint(assetMgr, classLoader, propClass, value);
                    prop->SetValue(o, ret);
                }
                else
                {
                    Var ret = Json2VarGuess(assetMgr, classLoader, value);
                    prop->SetValue(o, ret);
                }
            }
        }
    }
    
    SmartPtr<iClass> current = klass;
    while (!current.IsNull())
    {
        SmartPtr<iPropertyArray> props = current->GetProperties();
        for (auto i = 0; i < props->GetCount(); ++i)
        {
            SmartPtr<iProperty> p = props->GetItem(i);
            
            string assetUri = p->GetConfig("AssetUri");
            if (assetUri.empty())
            {
                continue;
            }
            
            SmartPtr<iProperty> prop = current->GetProperty(assetUri);
            if (prop.IsNull())
            {
                continue;
            }
            
            string uri = prop->GetValue(o);
            SmartPtr<iRefObject> asset = assetMgr->SyncLoad(classLoader, uri);
            p->SetValue(o, asset.Ptr());
        }
        
        current = current->GetParent();
    }
    
    return o.PtrAndSetNull();
}


NSPI_END()























