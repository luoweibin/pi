/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.23   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>

using namespace std;

NSPI_BEGIN()

iAssetLoaderFactory* CreateSceneLoaderFactory();
iAssetLoaderFactory* CreateMeshLoaderFactory();
iAssetLoaderFactory* CreatePngLoaderFactory();
iAssetLoaderFactory* CreateStringLoaderFactory();
iAssetLoaderFactory* CreateFrameAnim2DLoaderFactory();
iAssetLoaderFactory* CreateColladaLoaderFactory();
iAssetLoaderFactory* CreateMetaLoaderFactory();
iAssetLoaderFactory* CreateModelLoaderFactory();
iAssetLoaderFactory* CreateRenderTextureLoaderFactory();
iAssetLoaderFactory* CreateMaterialLoaderFactory();
iAssetLoaderFactory* CreateAnimSeqLoaderFactory();

class DefaultLoaderFactory : public iAssetLoaderFactory
{
private:
    typedef map<string, SmartPtr<iAssetLoaderFactory>> FactoryMap;
    FactoryMap mFactoryMap;
    
public:
    DefaultLoaderFactory()
    {
        SmartPtr<iAssetLoaderFactory> strFactory = CreateStringLoaderFactory();
        
        mFactoryMap["pis"] = strFactory;
        mFactoryMap["glsl"] = strFactory;
        
        mFactoryMap["sce"] = CreateSceneLoaderFactory();
        mFactoryMap["msh"] = CreateMeshLoaderFactory();
        mFactoryMap["png"] = CreatePngLoaderFactory();
        mFactoryMap["f2d"] = CreateFrameAnim2DLoaderFactory();
        mFactoryMap["dae"] = CreateColladaLoaderFactory();
        mFactoryMap["meta"] = CreateMetaLoaderFactory();
        mFactoryMap["pim"] = CreateModelLoaderFactory();
        mFactoryMap["mat"] = CreateMaterialLoaderFactory();
        mFactoryMap["rtex"] = CreateRenderTextureLoaderFactory();
        mFactoryMap["seq"] = CreateAnimSeqLoaderFactory();
    }
    
    virtual ~DefaultLoaderFactory()
    {
    }
    
    virtual iAssetLoader* CreateLoader(const string& fileExt, iStream* stream)
    {
        FactoryMap::iterator it = mFactoryMap.find(fileExt);
        piAssert(it != mFactoryMap.end(), nullptr);
        
        return it->second->CreateLoader(fileExt, stream);
    }
};


static SmartPtr<iAssetLoaderFactory> g_DefaultLoaderFactory = new DefaultLoaderFactory();
iAssetLoaderFactory* piGetDefaultAssetLoaderFactory()
{
    return g_DefaultLoaderFactory;
}


NSPI_END()

























