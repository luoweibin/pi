/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.19   0.1     Create
 ******************************************************************************/
#include "AssetImpl.h"

using namespace std;

NSPI_BEGIN()


class MemoryAsset : public AssetImpl<iMemoryAsset>
{
private:
    SmartPtr<iMemory> mMem;
    
public:
    MemoryAsset()
    {
    }
    
    virtual ~MemoryAsset()
    {
    }
    
    virtual iMemory* GetMemory() const
    {
        return mMem;
    }
    
    virtual void SetMemory(iMemory* memory)
    {
        piAssert(!IsReadonly(), ;);
        mMem = memory;
    }
    
    virtual char* Ptr() const
    {
        return mMem->Ptr();
    }
    
    virtual int64_t Size() const
    {
        return mMem->Size();
    }
};


iMemoryAsset* CreateMemoryAsset()
{
    return new MemoryAsset();
}



NSPI_END()




















