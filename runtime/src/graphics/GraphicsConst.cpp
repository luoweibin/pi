/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2015.4.15   0.1     Create
 ******************************************************************************/
#include <pi/Graphics.h>

using namespace std;

NSPI_BEGIN()

string piGraphicsBackendName(int backend)
{
    SmartPtr<iEnum> e = piGetRootClassLoader()->FindEnum("nspi::eGraphicsBackend");
    return e->GetString(backend);
}


string piGraphicsFeatureNames(int32_t features)
{
    string str;
    
    if (piFlagIs(features, eGraphicsFeature_DepthTest))
    {
        str += str.empty() ? "eGraphicsFeature_DepthTest" : "|eGraphicsFeature_DepthTest";
    }
    
    if (piFlagIs(features, eGraphicsFeature_CullFace))
    {
        str += str.empty() ? "eGraphicsFeature_CullFace" : "|eGraphicsFeature_CullFace";
    }
    
    if (piFlagIs(features, eGraphicsFeature_Blend))
    {
        str += str.empty() ? "eGraphicsFeature_Blend" : "|eGraphicsFeature_Blend";
    }
    
    if (piFlagIs(features, eGraphicsFeature_PolygonOffset_Fill))
    {
        str += str.empty() ? "eGraphicsFeature_PolygonOffset_Fill" : "|eGraphicsFeature_PolygonOffset_Fill";
    }
    
    if (piFlagIs(features, eGraphicsFeature_PolygonOffset_Line))
    {
        str += str.empty() ? "eGraphicsFeature_PolygonOffset_Line" : "|eGraphicsFeature_PolygonOffset_Line";
    }
    
    if (piFlagIs(features, eGraphicsFeature_PolygonOffset_Point))
    {
        str += str.empty() ? "eGraphicsFeature_PolygonOffset_Point" : "|eGraphicsFeature_PolygonOffset_Point";
    }
    
    if (piFlagIs(features, eGraphicsFeature_MultiSample))
    {
        str += str.empty() ? "eGraphicsFeature_MultiSample" : "|eGraphicsFeature_MultiSample";
    }
    
    if (piFlagIs(features, eGraphicsFeature_StencilTest))
    {
        str += str.empty() ? "eGraphicsFeature_StencilTest" : "|eGraphicsFeature_StencilTest";
    }
    
    if (piFlagIs(features, eGraphicsFeature_LineSmooth))
    {
        str += str.empty() ? "eGraphicsFeature_LineSmooth" : "|eGraphicsFeature_LineSmooth";
    }
    
    if (piFlagIs(features, eGraphicsFeature_Texture1D))
    {
        str += str.empty() ? "eGraphicsFeature_Texture1D" : "|eGraphicsFeature_Texture1D";
    }
    
    if (piFlagIs(features, eGraphicsFeature_Texture2D))
    {
        str += str.empty() ? "eGraphicsFeature_Texture2D" : "|eGraphicsFeature_Texture2D";
    }
    
    if (piFlagIs(features, eGraphicsFeature_Texture3D))
    {
        str += str.empty() ? "eGraphicsFeature_Texture3D" : "|eGraphicsFeature_Texture3D";
    }
    
    return str;
}

string piHintTargetName(int value)
{
    SmartPtr<iEnum> e = piGetRootClassLoader()->FindEnum("nspi::eHintTarget");
    return e->GetString(value);
}

string piHintName(int value)
{
    SmartPtr<iEnum> e = piGetRootClassLoader()->FindEnum("nspi::eHint");
    return e->GetString(value);
}

string piBlendFuncName(int value)
{
    SmartPtr<iEnum> e = piGetRootClassLoader()->FindEnum("nspi::eBlendFunc");
    return e->GetString(value);
}

string piWindingName(int value)
{
    SmartPtr<iEnum> e = piGetRootClassLoader()->FindEnum("nspi::eWinding");
    return e->GetString(value);
}


string piFaceName(int value)
{
    SmartPtr<iEnum> e = piGetRootClassLoader()->FindEnum("nspi::eFace");
    return e->GetString(value);
}

string piFramebufferBitName(int bits)
{
    string ret;

    if (piFlagIs(bits, eBufferBit_Color))
    {
        ret += "eBufferBit_Color";
    }
    
    if (piFlagIs(bits, eBufferBit_Depth))
    {
        if (!ret.empty())
        {
            ret += "|";
        }
        ret += "eBufferBit_Depth";
    }
    
    if (piFlagIs(bits, eBufferBit_Stencil))
    {
        if (!ret.empty())
        {
            ret += "|";
        }
        ret += "eBufferBit_Stencil";
    }
    
    return ret;
}


string piFramebufferName(int value)
{
    SmartPtr<iEnum> e = piGetRootClassLoader()->FindEnum("nspi::eFramebuffer");
    return e->GetString(value);
}


string piTexConfigName(int name)
{
    SmartPtr<iEnum> e = piGetRootClassLoader()->FindEnum("nspi::eTexConfig");
    return e->GetString(name);
}


string piTexValueName(int name)
{
    SmartPtr<iEnum> e = piGetRootClassLoader()->FindEnum("nspi::eTexValue");
    return e->GetString(name);
}


string piTexTargetName(int target)
{
    SmartPtr<iEnum> e = piGetRootClassLoader()->FindEnum("nspi::eTexTarget");
    return e->GetString(target);
}


string piAttachmentName(int target)
{
    SmartPtr<iEnum> e = piGetRootClassLoader()->FindEnum("nspi::eAttachment");
    return e->GetString(target);
}



string piGraphicsBufferName(int name)
{
    SmartPtr<iEnum> e = piGetRootClassLoader()->FindEnum("nspi::eGraphicsBuffer");
    return e->GetString(name);
}


string piGraphicsDrawName(int name)
{
    SmartPtr<iEnum> e = piGetRootClassLoader()->FindEnum("nspi::eGraphicsDraw");
    return e->GetString(name);
}

string piPolygonModeName(int name)
{
    SmartPtr<iEnum> e = piGetRootClassLoader()->FindEnum("nspi::ePolygonMode");
    return e->GetString(name);
}

string piFuncName(int func)
{
    SmartPtr<iEnum> e = piGetRootClassLoader()->FindEnum("nspi::eFunc");
    return e->GetString(func);
}

string piStencilOpName(int op)
{
    SmartPtr<iEnum> e = piGetRootClassLoader()->FindEnum("nspi::eStencilOp");
    return e->GetString(op);
}

string piEqFuncName(int value)
{
    SmartPtr<iEnum> e = piGetRootClassLoader()->FindEnum("nspi::eEqFunc");
    return e->GetString(value);
}

NSPI_END()

























