/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2015.4.2   0.1     Create
 ******************************************************************************/
#include <pi/Graphics.h>
#include <pi/core/impl/ThreadLocalImpl.h>

#include <mutex>
#include <thread>

using namespace std;

NSPI_BEGIN()

//==============================================================================


#if PI_THREAD_LOCAL || defined(PI_SIMULATOR) || defined(PI_ANDROID)

static ThreadLocal<iGraphicsVM*> gThreadGraphicsVM;

static iGraphicsVM* GetThreadGraphicsVM()
{
    return gThreadGraphicsVM.Get();
}

void piSetGraphicsVM(iGraphicsVM* vm)
{
    gThreadGraphicsVM.Set(vm);
}

#else

thread_local iGraphicsVM* gThreadGraphicsVM;

static iGraphicsVM* GetThreadGraphicsVM()
{
    return gThreadGraphicsVM;
}

void piSetGraphicsVM(iGraphicsVM* vm)
{
    gThreadGraphicsVM = vm;
}

#endif


int piGetGraphicsType()
{
    return GetThreadGraphicsVM()->GetType();
}

bool piAddGraphicsAlias(const string &alias, int32_t name)
{
    return GetThreadGraphicsVM()->AddAlias(alias, name);
}

void piRemoveGraphicsAlias(const string &alias)
{
    return GetThreadGraphicsVM()->RemoveAlias(alias);
}

int32_t piGetGraphicsObject(const string &alias)
{
    return GetThreadGraphicsVM()->GetObject(alias);
}

int32_t piCreateNativeGraphicsObject(int64_t nativeHandle)
{
    return GetThreadGraphicsVM()->CreateNativeGraphicsObject(nativeHandle);
}

//==============================================================================

void piPushGroupMarker(const string &marker)
{
    GetThreadGraphicsVM()->PushGroupMarker(marker);
}

void piPopGroupMarker()
{
    GetThreadGraphicsVM()->PopGroupMarker();
}

void piInsertEventMarker(const string &marker)
{
    GetThreadGraphicsVM()->InsertEventMarker(marker);
}

void piClear(int32_t fields)
{
    GetThreadGraphicsVM()->Clear(fields);
}

void piCullFace(int face)
{
    GetThreadGraphicsVM()->CullFace(face);
}

void piViewport(int32_t x, int32_t y, int32_t width, int32_t height)
{
    GetThreadGraphicsVM()->Viewport(x, y, width, height);
}

rect piGetViewport()
{
    return GetThreadGraphicsVM()->GetViewport();
}

void piWinding(int32_t winding)
{
    GetThreadGraphicsVM()->Winding(winding);
}

void piEnableFeatures(int features)
{
    GetThreadGraphicsVM()->EnableFeatures(features);
}

void piDisableFeatures(int features)
{
    GetThreadGraphicsVM()->DisableFeatures(features);
}

void piDrawElements(int mode, int32_t count, int type)
{
    GetThreadGraphicsVM()->DrawElements(mode, count, type);
}

void piDrawElementsOffset(int mode, int32_t count, int type, int32_t offset)
{
    GetThreadGraphicsVM()->DrawElementsOffset(mode, count, type, offset);
}

void piDrawElementsInstanced(int mode, int32_t count, int type, int32_t instanceCount)
{
    GetThreadGraphicsVM()->DrawArraysInstanced(mode, count, type, instanceCount);
}

void piDrawArrays(int mode, int32_t first, int32_t count)
{
    GetThreadGraphicsVM()->DrawArrays(mode, first, count);
}

void piDrawArraysInstanced(int mode, int32_t first, int32_t count, int instanceCount)
{
    GetThreadGraphicsVM()->DrawArraysInstanced(mode, first, count, instanceCount);
}

void piRetainGraphicsObject(int32_t name)
{
    GetThreadGraphicsVM()->RetainObject(name);
}

void piReleaseGraphicsObject(int32_t name)
{
    GetThreadGraphicsVM()->ReleaseObejct(name);
}

void piFlush()
{
    GetThreadGraphicsVM()->Flush();
}

//----------------------------------------------------------------------

void piPolygonOffset(float factor, float units)
{
    GetThreadGraphicsVM()->PolygonOffset(factor, units);
}

void piPolygonMode(int face, int mode)
{
    GetThreadGraphicsVM()->PolygonMode(face, mode);
}

void piDepthFunction(int func)
{
    GetThreadGraphicsVM()->DepthFunction(func);
}

void piClearDepth(float value)
{
    GetThreadGraphicsVM()->ClearDepth(value);
}

void piDepthMask(bool flag)
{
    GetThreadGraphicsVM()->DepthMask(flag);
}

void piClearColor(const vec4 &color)
{
    GetThreadGraphicsVM()->ClearColor(color);
}

void piColorMask(bool red, bool green, bool blue, bool alpha)
{
    GetThreadGraphicsVM()->ColorMask(red, green, blue, alpha);
}

void piClearStencil(int value)
{
    GetThreadGraphicsVM()->ClearStencil(value);
}

void piStencilFunc(int func, int32_t ref, int64_t mask)
{
    GetThreadGraphicsVM()->StencilFunc(func, ref, mask);
}

void piStencilOp(int sfail, int dpfail, int dppass)
{
    GetThreadGraphicsVM()->StencilOp(sfail, dpfail, dppass);
}

void piStencilOpSeparate(int face, int sfail, int dpfail, int dppass)
{
    GetThreadGraphicsVM()->StencilOpSeparate(face, sfail, dpfail, dppass);
}


void piStencilMask(int64_t mask)
{
    GetThreadGraphicsVM()->StencilMask(mask);
}

void piLineWidth(float width)
{
    GetThreadGraphicsVM()->LineWidth(width);
}

void piHint(int target, int value)
{
    GetThreadGraphicsVM()->Hint(target, value);
}

void piBlendColor(const vec4& color)
{
    GetThreadGraphicsVM()->BlendColor(color);
}

void piBlendEquation(int mode)
{
    GetThreadGraphicsVM()->BlendEquation(mode);
}

void piBlendFunc(int sfactor, int dfactor)
{
    GetThreadGraphicsVM()->BlendFunc(sfactor, dfactor);
}

void piBlendFuncSeparate(int32_t index, int sfactor, int dfactor)
{
    GetThreadGraphicsVM()->BlendFuncSeparate(index, sfactor, dfactor);
}

//----------------------------------------------------------------------

int32_t piCreateProgram()
{
    return GetThreadGraphicsVM()->CreateProgram();
}

void piCompileProgram(int32_t program,
                      const string& vertex,
                      const string& fragment)
{
    GetThreadGraphicsVM()->CompileProgram(program, vertex, fragment);
}

void piLinkProgram(int32_t program)
{
    GetThreadGraphicsVM()->LinkProgram(program);
}

void piUseProgram(int32_t program)
{
    GetThreadGraphicsVM()->UseProgram(program);
}

void piBindVertexAttr(int32_t program, int32_t index, const string& name)
{
    GetThreadGraphicsVM()->BindVertexAttr(program, index, name);
}

void piEnableVertexAttr(int32_t name)
{
    GetThreadGraphicsVM()->EnableVertexAttr(name);
}

void piDisableVertexAttr(int32_t name)
{
    GetThreadGraphicsVM()->DisableVertexAttr(name);
}

void piVertexAttr(int32_t name,
                     int32_t size,
                     int32_t type,
                     int32_t stride,
                     int32_t offset)
{
    GetThreadGraphicsVM()->VertexAttr(name, size, type, stride, offset);
}

void piUniform1i(int32_t program,
                 const string& name,
                 int32_t v)
{
    GetThreadGraphicsVM()->Uniform1i(program, name, v);
}

void piUniform1iv(int32_t program,
                  const string& name,
                  iI32Array* values)
{
    GetThreadGraphicsVM()->Uniform1iv(program, name, values);
}

void piUniform2i(int32_t program,
                 const string& name, int32_t v1, int32_t v2)
{
    GetThreadGraphicsVM()->Uniform2i(program, name, v1, v2);
}

void piUniform2iv(int32_t program,
                  const string& name,
                  iI32Array* values)
{
    GetThreadGraphicsVM()->Uniform2iv(program, name, values);
}

void piUniform3i(int32_t program,
                 const string& name,
                 int32_t v1,
                 int32_t v2,
                 int32_t v3)
{
    GetThreadGraphicsVM()->Uniform3i(program, name, v1, v2, v3);
}

void piUniform3iv(int32_t program,
                  const string& name,
                  iI32Array* values)
{
    GetThreadGraphicsVM()->Uniform3iv(program, name, values);
}

void piUniform4i(int32_t program,
                 const string& name,
                 int32_t v1,
                 int32_t v2,
                 int32_t v3,
                 int32_t v4)
{
    GetThreadGraphicsVM()->Uniform4i(program, name, v1, v2, v3, v4);
}

void piUniform4iv(int32_t program,
                  const string& name,
                  iI32Array* values)
{
    GetThreadGraphicsVM()->Uniform4iv(program, name, values);
}

void piUniform1f(int32_t program,
                 const string& name,
                 float v)
{
    GetThreadGraphicsVM()->Uniform1f(program, name, v);
}

void piUniform1fv(int32_t program,
                  const string& name,
                  iF32Array* values)
{
    GetThreadGraphicsVM()->Uniform1fv(program, name, values);
}

void piUniform2f(int32_t program, const string& name, float v1, float v2)
{
    GetThreadGraphicsVM()->Uniform2f(program, name, v1, v2);
}

void piUniform2fv(int32_t program,
                  const string& name,
                  iF32Array* values)
{
    GetThreadGraphicsVM()->Uniform2fv(program, name, values);
}

void piUniform3f(int32_t program,
                 const string& name,
                 float v1,
                 float v2,
                 float v3)
{
    GetThreadGraphicsVM()->Uniform3f(program, name, v1, v2, v3);
}

void piUniform3fv(int32_t program,
                  const string& name,
                  iF32Array* values)
{
    GetThreadGraphicsVM()->Uniform3fv(program, name, values);
}

void piUniform4f(int32_t program,
                 const string& name,
                 float v1,
                 float v2,
                 float v3,
                 float v4)
{
    GetThreadGraphicsVM()->Uniform4f(program, name, v1, v2, v3, v4);
}

void piUniform4fv(int32_t program,
                  const string& name,
                  iF32Array* values)
{
    GetThreadGraphicsVM()->Uniform4fv(program, name, values);
}

void piUniformVec3v(int32_t program, const string&name, iVec3Array* values)
{
    GetThreadGraphicsVM()->UniformVec3v(program, name, values);
}

void piUniformVec3(int32_t program, const string&name, const vec3& value)
{
    GetThreadGraphicsVM()->UniformVec3(program, name, value);
}

void piUniformVec4(int32_t program, const string& name, const vec4& value)
{
    GetThreadGraphicsVM()->UniformVec4(program, name, value);
}

void piUniformRect(int32_t program, const string& name, const rect& value)
{
    GetThreadGraphicsVM()->UniformRect(program, name, value);
}

void piUniformMat4f(int32_t program, const string& name, const mat4& value)
{
    GetThreadGraphicsVM()->UniformMat4f(program, name, value);
}

void piUniformMat4fv(int32_t program, const string& name, iMat4Array* values)
{
    GetThreadGraphicsVM()->UniformMat4fv(program, name, values);
}

//----------------------------------------------------------------------

int32_t piCreateVertexArray()
{
    return GetThreadGraphicsVM()->CreateVertexArray();
}

void piBindVertexArray(int32_t name)
{
    GetThreadGraphicsVM()->BindVertexArray(name);
}

int32_t piCreateBuffer()
{
    return GetThreadGraphicsVM()->CreateBuffer();
}

void piBindBuffer(int target, int32_t name)
{
    GetThreadGraphicsVM()->BindBuffer(target, name);
}

void piBufferData(int target, int32_t name, int64_t size, iMemory* data)
{
    GetThreadGraphicsVM()->BufferData(target, name, size, data);
}

void piBufferSubData(int target, int64_t offset, int64_t size, iMemory* data)
{
    GetThreadGraphicsVM()->BufferSubData(target, offset, size, data);
}


//----------------------------------------------------------------------

int32_t piCreateTexture()
{
    return GetThreadGraphicsVM()->CreateTexture();
}

void piGenerateMipmap(int target)
{
    GetThreadGraphicsVM()->GenerateMipmap(target);
}

void piActiveTexture(int32_t unit)
{
    GetThreadGraphicsVM()->ActiveTexture(unit);
}

void piBindTexture(int32_t target, int32_t name)
{
    GetThreadGraphicsVM()->BindTexture(target, name);
}

void piTexParam(int32_t target, int32_t name, int32_t value)
{
    GetThreadGraphicsVM()->TexParam(target, name, value);
}

void piTexImage2D(int target,
                  int32_t level,
                  int32_t format,
                  iBitmap *bitmap,
                  int32_t planar)
{
    GetThreadGraphicsVM()->TexImage2D(target, level, format, bitmap, planar);
}

void piCompressedTexImage2D(int target,
                            int32_t level,
                            int32_t format,
                            iBitmap *bitmap,
                            int32_t planar)
{
    GetThreadGraphicsVM()->CompressedTexImage2D(target, level, format, bitmap, planar);
}

void piCopyTexImage2D(int target,
                      int32_t level,
                      int32_t format,
                      int32_t x,
                      int32_t y,
                      int32_t width,
                      int32_t height)
{
    GetThreadGraphicsVM()->CopyTexImage2D(target, level, format, x, y, width, height);
}

void piTexSubImage2D(int target,
                     int32_t level,
                     int32_t xOffset, int32_t yOffset,
                     int32_t width, int32_t height,
                     iBitmap *bitmap,
                     int32_t planar)
{
    GetThreadGraphicsVM()->TexSubImage2D(target, level, xOffset, yOffset, width, height, bitmap, planar);
}

void piCompressedTexSubImage2D(int target,
                               int32_t level,
                               int32_t xOffset, int32_t yOffset,
                               int32_t width, int32_t height,
                               iBitmap *bitmap,
                               int32_t planar)
{
    GetThreadGraphicsVM()->CompressedTexSubImage2D(target, level, xOffset, yOffset, width, height, bitmap, planar);
}

void piCopyTexSubImage2D(int target,
                         int32_t level,
                         int32_t format,
                         int32_t xOffset, int32_t yOffset,
                         int32_t x, int32_t y,
                         int32_t width, int32_t height)
{
    GetThreadGraphicsVM()->CopyTexSubImage2D(target, level, format, xOffset, yOffset, x, y, width, height);
}



void piPixelStorei(int name, int32_t value)
{
    GetThreadGraphicsVM()->PixelStorei(name, value);
}

//----------------------------------------------------------------------

int32_t piGetFramebuffer()
{
    return GetThreadGraphicsVM()->GetFramebuffer();
}

int32_t piCreateFramebuffer()
{
    return GetThreadGraphicsVM()->CreateFramebuffer();
}

void piBindFramebuffer(int target, int32_t name)
{
    GetThreadGraphicsVM()->BindFramebuffer(target, name);
}

void piFramebufferTexture2D(int framebufferTarget,
                            int attachment,
                            int textureTarget,
                            int texture,
                            int32_t level)
{
    GetThreadGraphicsVM()->FramebufferTexture2D(framebufferTarget, attachment, textureTarget, texture, level);
}

void piFramebufferRenderbuffer(int target, int attachment, int32_t renderbuffer)
{
    GetThreadGraphicsVM()->FramebufferRenderbuffer(target, attachment, renderbuffer);
}

void piVerifyFramebufferState(int target)
{
    GetThreadGraphicsVM()->VerifyFramebufferState(target);
}

//----------------------------------------------------------------------

int32_t piCreateRenderbuffer()
{
    return GetThreadGraphicsVM()->CreateRenderbuffer();
}

void piBindRenderbuffer(int32_t name)
{
    GetThreadGraphicsVM()->BindRenderbuffer(name);
}

void piRenderbufferStorage(int format, int32_t width, int32_t height)
{
    GetThreadGraphicsVM()->RenderbufferStorage(format, width, height);
}



NSPI_END()













