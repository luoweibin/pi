/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.10.13   0.1     Create
 ******************************************************************************/
#include "GraphicsVM_Impl.h"

NSPI_BEGIN()


int32_t GraphicsVM::GetFramebuffer() const
{
    lock_guard<mutex> lock(mMutex);
    return mFramebuffer;
}

int32_t GraphicsVM::CreateFramebuffer()
{
    lock_guard<mutex> lock(mMutex);
    int32_t name = AllocName();
    
    PushByteCode(eGraphicsOp_CreateFramebuffer, name);
    return name;
}

void GraphicsVM::BindFramebuffer(int target, int32_t name)
{
    piAssert(name >= 0, ;);
    
    lock_guard<mutex> lock(mMutex);
    mFramebuffer = name;
    
    PushByteCode(eGraphicsOp_BindFramebuffer, target, name);
}

void GraphicsVM::FramebufferTexture2D(int framebufferTarget,
                                      int attachment,
                                      int textureTarget,
                                      int texture,
                                      int32_t level)
{
    piAssert(texture > 0, ;);
    piAssert(level >= 0, ;);
    
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_FramebufferTexture2D,
                 framebufferTarget,
                 attachment,
                 textureTarget,
                 texture,
                 level);
}

void GraphicsVM::FramebufferRenderbuffer(int target,
                                         int attachment,
                                         int32_t renderbuffer)
{
    piAssert(renderbuffer > 0, ;);
    
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_FramebufferRenderbuffer,
                 target,
                 attachment,
                 renderbuffer);
}

void GraphicsVM::VerifyFramebufferState(int target)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_VerifyFramebufferState, target);
}

int32_t GraphicsVM::CreateRenderbuffer()
{
    lock_guard<mutex> lock(mMutex);
    int32_t name = AllocName();
    
    PushByteCode(eGraphicsOp_CreateRenderbuffer, name);
    
    return name;
}

void GraphicsVM::BindRenderbuffer(int32_t name)
{
    piAssert(name >= 0, ;);
    
    lock_guard<mutex> lock(mMutex);
    mRenderbuffer = name;
    
    PushByteCode(eGraphicsOp_BindRenderbuffer, name);
}

void GraphicsVM::RenderbufferStorage(int format, int32_t width, int32_t height)
{
    lock_guard<mutex> lock(mMutex);
    
    PushByteCode(eGraphicsOp_RenderbufferStorage, format, width, height);
}




/**
 * =================================================================================================
 * Dispatch
 */

void GraphicsVM::CallCreateFramebuffer(iGraphicsByteCode* byteCode)
{
    int32_t name = byteCode->GetArg(0);
    mBackend->CreateFramebuffer(name);

    PILOGV(PI_GRAPHICS_TAG, "CreateFramebuffer(name:%d)", name);
}

void GraphicsVM::CallBindFramebuffer(iGraphicsByteCode* byteCode)
{
    int target = byteCode->GetArg(0);
    int32_t name = byteCode->GetArg(1);
    mBackend->BindFramebuffer(target, name);

    PILOGV(PI_GRAPHICS_TAG,
           "BindFramebuffer(target:%s, name:%d)",
           piFramebufferName(target).c_str(),
           name);
}

void GraphicsVM::CallVerifyFramebufferState(iGraphicsByteCode *byteCode)
{
    int target = byteCode->GetArg(0);
    mBackend->VerifyFramebufferState(target);
    
    PILOGV(PI_GRAPHICS_TAG, "VerifyFramebufferState(target:%s)", piFramebufferName(target).c_str());
}

void GraphicsVM::CallFramebufferTexture2D(iGraphicsByteCode* byteCode)
{
    int fbTarget = byteCode->GetArg(0);
    int attachment = byteCode->GetArg(1);
    int texTarget = byteCode->GetArg(2);
    int32_t texture = byteCode->GetArg(3);
    int32_t level = byteCode->GetArg(4);
    mBackend->FramebufferTexture2D(fbTarget, attachment, texTarget, texture, level);

    PILOGV(PI_GRAPHICS_TAG,
           "FramebufferTexture2D(target:%s, attachment:%s, textureTarget:%s, texture:%d, level:%d)",
           piFramebufferName(fbTarget).c_str(),
           piAttachmentName(attachment).c_str(),
           piTexTargetName(texTarget).c_str(),
           texture,
           level);
}

void GraphicsVM::CallFramebufferRenderbuffer(iGraphicsByteCode* byteCode)
{
    int fbTarget = byteCode->GetArg(0);
    int attachment = byteCode->GetArg(1);
    int32_t renderbuffer = byteCode->GetArg(2);
    mBackend->FramebufferRenderbuffer(fbTarget, attachment, renderbuffer);

    PILOGV(PI_GRAPHICS_TAG,
           "CallFramebufferRenderbuffer(target:%s, attachment:%s, renderbuffer:%d)",
           piFramebufferName(fbTarget).c_str(),
           piAttachmentName(attachment).c_str(),
           renderbuffer);
}

void GraphicsVM::CallCreateRenderbuffer(iGraphicsByteCode* byteCode)
{
    int32_t name = byteCode->GetArg(0);
    mBackend->CreateRenderbuffer(name);

    PILOGV(PI_GRAPHICS_TAG, "CreateRenderbuffer(name:%d)", name);
}

void GraphicsVM::CallBindRenderbuffer(iGraphicsByteCode* byteCode)
{
    int32_t name = byteCode->GetArg(0);
    mBackend->BindRenderbuffer(name);

    PILOGV(PI_GRAPHICS_TAG, "BindRenderbuffer(name:%d)", name);
}

void GraphicsVM::CallRenderbufferStorage(iGraphicsByteCode* byteCode)
{
    int32_t format = byteCode->GetArg(0);
    int32_t width = byteCode->GetArg(1);
    int32_t height = byteCode->GetArg(2);
    mBackend->RenderbufferStorage(format, width, height);

    PILOGV(PI_GRAPHICS_TAG,
           "RenderbufferStorage(format:%s, width:%d, height:%d)",
           piPixelFormatName(format).c_str(),
           width,
           height);
}


NSPI_END()





















