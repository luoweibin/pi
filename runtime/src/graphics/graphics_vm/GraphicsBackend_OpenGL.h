/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.10.13   0.1     Create
 ******************************************************************************/
#ifndef PI_SRC_GRAPHICS_GRAPHICSVM_GRAPHICSBACKENDOPENGL_H
#define PI_SRC_GRAPHICS_GRAPHICSVM_GRAPHICSBACKENDOPENGL_H

#include "GraphicsOp.h"
#include "GraphicsBackend.h"

NSPI_BEGIN()

class TextureOpenGL : public iRefObject
{
private:
    GLuint mHandle;
    
public:
    TextureOpenGL()
    {
        glGenTextures(1, &mHandle);
    }
    
    virtual ~TextureOpenGL()
    {
        glDeleteTextures(1, &mHandle);
        mHandle = -1;
    }
    
    GLuint GetHandle() const
    {
        return mHandle;
    }
    
    virtual string ToString() const
    {
        return "Texture";
    }
};


class FramebufferOpenGL : public iRefObject
{
private:
    GLuint mHandle;
    
public:
    FramebufferOpenGL()
    {
        glGenFramebuffers(1, &mHandle);
    }
    
    virtual ~FramebufferOpenGL()
    {
        glDeleteFramebuffers(1, &mHandle);
        mHandle = -1;
    }
    
    GLuint GetHandle() const
    {
        return mHandle;
    }
    
    virtual string ToString() const
    {
        return "Framebuffer";
    }
};

class BufferOpenGL : public iRefObject
{
private:
    GLuint mHandle;
    
public:
    BufferOpenGL()
    {
        glGenBuffers(1, &mHandle);
    }
    
    virtual ~BufferOpenGL()
    {
        glDeleteBuffers(1, &mHandle);
        mHandle = -1;
    }
    
    GLuint GetHandle() const
    {
        return mHandle;
    }
    
    virtual string ToString() const
    {
        return "Buffer";
    }
};

//==============================================================================

class RenderbufferOpenGL : public iRefObject
{
private:
    GLuint mHandle;
    
public:
    RenderbufferOpenGL()
    {
        glGenRenderbuffers(1, &mHandle);
    }
    
    virtual ~RenderbufferOpenGL()
    {
        glDeleteRenderbuffers(1, &mHandle);
        mHandle = -1;
    }
    
    GLuint GetHandle() const
    {
        return mHandle;
    }
    
    virtual string ToString() const
    {
        return "Renderbuffer";
    }
};

//==============================================================================


class GraphicsBackend_OpenGL : public iGraphicsBackend
{
protected:
    typedef map<int32_t, SmartPtr<iRefObject>> ObjectMap;
    ObjectMap mObjects;
    
    typedef map<int32_t, GLint> NativeObjMap;
    NativeObjMap mNativeObjMap;
    
#if PI_DEBUG
    typedef map<int32_t, SmartPtr<iClass>> ObjectTypeMap;
    ObjectTypeMap mObjectTypes;
#endif
    
    GLint mDefaultFramebuffer;
    GLint mDefaultRenderbuffer;
    
    map<int, bool> mCaps;
    
public:
    GraphicsBackend_OpenGL():
    mDefaultFramebuffer(0), mDefaultRenderbuffer(0)
    {
        mCaps[eGraphicsCap_VAO] = true;
    }
    
    virtual ~GraphicsBackend_OpenGL()
    {
    }
    
    virtual void DumpDebugInfo()
    {
#if PI_DEBUG
        PILOGI(PI_GRAPHICS_TAG, "=========================== Graphics Debug Info ========================");
        for (auto type : mObjects)
        {
            PILOGI(PI_GRAPHICS_TAG, "[%d]type:%s", type.first, type.second->ToString().c_str());
        }
#endif
    }
    
    virtual void SetCapability(int cap, bool value)
    {
        mCaps[cap] = value;
    }
    
    virtual bool GetCapability(int cap) const
    {
        map<int, bool>::const_iterator it = mCaps.find(cap);
        return it != mCaps.end() ? it->second : false;
    }
    
    virtual void OnBeforeRun()
    {
        glGetIntegerv(GL_FRAMEBUFFER_BINDING, &mDefaultFramebuffer);
        glGetIntegerv(GL_RENDERBUFFER_BINDING, &mDefaultRenderbuffer);
    }
    
    virtual void OnAfterRun()
    {
    }
    
    virtual void CreateNativeGraphicsObject(int32_t name, int64_t handle)
    {
        auto it = mNativeObjMap.find(name);
        piAssert(it == mNativeObjMap.end(), ;);
        
        mNativeObjMap[name] = (GLint)handle;
    }
    
    virtual void PushGroupMarker(const std::string& marker)
    {
#if PI_GRAPHICS_MARKER_ENABLED && !defined(PI_ANDROID)
        glPushGroupMarkerEXT(0, marker.c_str());
#endif
    }
    
    virtual void PopGroupMarker()
    {
#if PI_GRAPHICS_MARKER_ENABLED && !defined(PI_ANDROID)
        glPopGroupMarkerEXT();
#endif
    }
    
    virtual void InsertEventMarker(const std::string& marker)
    {
#if PI_GRAPHICS_MARKER_ENABLED && !defined(PI_ANDROID)
        glInsertEventMarkerEXT(0, marker.c_str());
#endif
    }
    
    virtual void Hint(int target, int value)
    {
        glHint(ToGLHintTarget(target), ToGLHint(value));
    }
    
    virtual void Clear(int32_t inFields)
    {
        GLbitfield fields = 0;
        
        if (piFlagIs(inFields, eBufferBit_Color))
        {
            fields |= GL_COLOR_BUFFER_BIT;
        }
        
        if (piFlagIs(inFields, eBufferBit_Depth))
        {
            fields |= GL_DEPTH_BUFFER_BIT;
        }
        
        if (piFlagIs(inFields, eBufferBit_Stencil))
        {
            fields |= GL_STENCIL_BUFFER_BIT;
        }
        
        glClear(fields);
    }
    
    virtual void BlendColor(const vec4& color)
    {
        glBlendColor(color.r, color.g, color.b, color.a);
    }
    
    virtual void BlendEquation(int mode)
    {
        glBlendEquation(ToGLEqFunc(mode));
    }
    
    virtual void BlendFunc(int sfactor, int dfactor)
    {
        glBlendFunc(ToGLBlendFunc(sfactor), ToGLBlendFunc(dfactor));
    }
    
    virtual void DepthFunction(int func)
    {
        glDepthFunc(ToGLFunc(func));
    }
    
    virtual void DisableFeatures(int features)
    {
        if (piFlagIs(features, eGraphicsFeature_DepthTest))
        {
            glDisable(GL_DEPTH_TEST);
        }
        
        if (piFlagIs(features, eGraphicsFeature_Blend))
        {
            glDisable(GL_BLEND);
        }
        
        if (piFlagIs(features, eGraphicsFeature_CullFace))
        {
            glDisable(GL_CULL_FACE);
        }
        
        if (piFlagIs(features, eGraphicsFeature_PolygonOffset_Fill))
        {
            glDisable(GL_POLYGON_OFFSET_FILL);
        }
        
        if (piFlagIs(features, eGraphicsFeature_StencilTest))
        {
            glDisable(GL_STENCIL_TEST);
        }
        
        if (piFlagIs(features, eGraphicsFeature_Texture2D))
        {
            glDisable(GL_TEXTURE_2D);
        }
    }
    
    virtual void LineWidth(float width)
    {
        glLineWidth(width);
    }
    
    virtual void EnableFeatures(int features)
    {
        if (piFlagIs(features, eGraphicsFeature_DepthTest))
        {
            glEnable(GL_DEPTH_TEST);
        }
        
        if (piFlagIs(features, eGraphicsFeature_Blend))
        {
            glEnable(GL_BLEND);
        }
        
        if (piFlagIs(features, eGraphicsFeature_CullFace))
        {
            glEnable(GL_CULL_FACE);
        }
        
        if (piFlagIs(features, eGraphicsFeature_PolygonOffset_Fill))
        {
            glEnable(GL_POLYGON_OFFSET_FILL);
        }
        
        if (piFlagIs(features, eGraphicsFeature_StencilTest))
        {
            glEnable(GL_STENCIL_TEST);
        }
        
        if (piFlagIs(features, eGraphicsFeature_Texture2D))
        {
            glEnable(GL_TEXTURE_2D);
        }
    }
    
    virtual void CullFace(int face)
    {
        glCullFace(ToGLFace(face));
    }
    
    virtual void DepthMask(bool flag)
    {
        glDepthMask(flag);
    }
    
    virtual void ColorMask(bool red, bool green, bool blue, bool alpha)
    {
        glColorMask(red, green, blue, alpha);
    }
    
    virtual void ClearColor(const vec4 &color)
    {
        glClearColor(color.r, color.g, color.b, color.a);
    }
    
    virtual void Viewport(int32_t x, int32_t y, int32_t width, int32_t height)
    {
        glViewport(x, y, width, height);
    }
    
    virtual void Winding(int32_t winding)
    {
        switch (winding)
        {
            case eWinding_CCW:
                glFrontFace(GL_CCW);
                break;
                
            case eWinding_CW:
            default:
                glFrontFace(GL_CW);
                break;
        }
    }
    
    virtual void DrawElements(int mode, int32_t count, int type)
    {
        glDrawElements(ToGLDraw(mode), count, ToGLType(type), 0);
    }
    
    virtual void DrawElementsOffset(int mode, int32_t count, int type, int32_t offset)
    {
        intptr_t off = offset;
        glDrawElements(ToGLDraw(mode), count, ToGLType(type), (void*)off);
    }
    
    virtual void DrawArrays(int mode, int32_t first, int32_t count)
    {
        glDrawArrays(ToGLDraw(mode), first, count);
    }
    
    virtual void ReleaseObejct(int32_t name)
    {
        mObjects.erase(name);
        mNativeObjMap.erase(name);
        
#if PI_DEBUG
        mObjectTypes.erase(name);
#endif
    }
    
    virtual void Flush()
    {
        glFlush();
    }
    
    //----------------------------------------------------------------------
    
    virtual void PolygonOffset(float factor, float units)
    {
        glPolygonOffset(factor, units);
    }
    
    virtual void StencilMask(int64_t mask)
    {
        glStencilMask((GLuint)mask);
    }
    
    virtual void ClearStencil(int value)
    {
        glClearStencil(value);
    }
    
    virtual void StencilFunc(int func, int32_t ref, int64_t mask)
    {
        glStencilFunc(ToGLFunc(func), ref, (GLuint)mask);
    }
    
    virtual void StencilOp(int sfail, int dpfail, int dppass)
    {
        glStencilOp(ToGLStencilOp(sfail), ToGLStencilOp(dpfail), ToGLStencilOp(dppass));
    }
    
    virtual void StencilOpSeparate(int face, int sfail, int dpfail, int dppass)
    {
        glStencilOpSeparate(ToGLFace(face), ToGLStencilOp(sfail), ToGLStencilOp(dpfail), ToGLStencilOp(dppass));
    }
    
    //----------------------------------------------------------------------
    
    virtual void CreateProgram(int32_t name)
    {
        SmartPtr<GraphicsProgramOpenGL> object = new GraphicsProgramOpenGL();
        AddObject(name, object);
    }
    
    virtual void CompileProgram(int32_t name,
                                const string& vertex,
                                const string& fragment)
    {
        ObjectMap::iterator it = mObjects.find(name);
        piAssert(it != mObjects.end(), ;);
        
        SmartPtr<GraphicsProgramOpenGL> program = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        program->Compile(vertex, fragment);
    }
    
    virtual void LinkProgram(int32_t name)
    {
        ObjectMap::iterator it = mObjects.find(name);
        piAssert(it != mObjects.end(), ;);
        
        SmartPtr<GraphicsProgramOpenGL> program = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        program->Link();
    }
    
    virtual void UseProgram(int32_t name)
    {
        if (name > 0)
        {
            ObjectMap::iterator it = mObjects.find(name);
            piAssert(it != mObjects.end(), ;);
            
            SmartPtr<GraphicsProgramOpenGL> program = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
            program->Use();
        }
        else
        {
            glUseProgram(0);
        }
    }
    
    virtual void EnableVertexAttr(int32_t name)
    {
        glEnableVertexAttribArray(name);
    }
    
    virtual void BindVertexAttr(int32_t program, int32_t name, const string& strName)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->BindVertexAttr(name, strName);
    }
    
    virtual void DisableVertexAttr(int32_t name)
    {
        glDisableVertexAttribArray(name);
    }
    
    virtual void VertexAttr(int32_t name,
                            int32_t size,
                            int32_t type,
                            int32_t stride,
                            int32_t offset)
    {
        int64_t glOffset = offset;
        
        glVertexAttribPointer(name,
                              size,
                              ToGLType(type),
                              GL_FALSE,
                              stride,
                              (GLvoid*)glOffset);
    }
    
    virtual void Uniform1i(int32_t program,
                           const string& name, int32_t v)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->Uniform1i(name, v);
    }
    
    virtual void Uniform1iv(int32_t program,
                            const string& name,
                            iI32Array* values)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->Uniform1iv(name, values);
    }
    
    virtual void Uniform2i(int32_t program,
                           const string& name, int32_t v1, int32_t v2)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->Uniform2i(name, v1, v2);
    }
    
    virtual void Uniform2iv(int32_t program,
                            const string& name,
                            iI32Array* values)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->Uniform2iv(name, values);
    }
    
    virtual void Uniform3i(int32_t program,
                           const string& name,
                           int32_t v1,
                           int32_t v2,
                           int32_t v3)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->Uniform3i(name, v1, v2, v3);
    }
    
    virtual void Uniform3iv(int32_t program,
                            const string& name,
                            iI32Array* values)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->Uniform3iv(name, values);
    }
    
    virtual void Uniform4i(int32_t program,
                           const string& name,
                           int32_t v1,
                           int32_t v2,
                           int32_t v3,
                           int32_t v4)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->Uniform4i(name, v1, v2, v3, v4);
    }
    
    virtual void Uniform4iv(int32_t program,
                            const string& name,
                            iI32Array* values)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->Uniform4iv(name, values);
    }
    
    virtual void Uniform1f(int32_t program,
                           const string& name, float v)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->Uniform1f(name, v);
    }
    
    virtual void Uniform1fv(int32_t program,
                            const string& name,
                            iF32Array* values)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->Uniform1fv(name, values);
    }
    
    virtual void Uniform2f(int32_t program,
                           const string& name, float v1, float v2)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->Uniform2f(name, v1, v2);
    }
    
    virtual void Uniform2fv(int32_t program,
                            const string& name,
                            iF32Array* values)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->Uniform2fv(name, values);
    }
    
    virtual void Uniform3f(int32_t program,
                           const string& name,
                           float v1,
                           float v2,
                           float v3)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->Uniform3f(name, v1, v2, v3);
    }
    
    virtual void Uniform3fv(int32_t program,
                            const string& name,
                            iF32Array* values)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->Uniform3fv(name, values);
    }
    
    virtual void Uniform4f(int32_t program,
                           const string& name,
                           float v1,
                           float v2,
                           float v3,
                           float v4)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->Uniform4f(name, v1, v2, v3, v4);
    }
    
    virtual void Uniform4fv(int32_t program,
                            const string& name,
                            iF32Array* values)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->Uniform4fv(name, values);
    }
    
    virtual void UniformVec3(int32_t program,
                             const string& name,
                             const vec3& value)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->UniformVec3(name, value);
    }
    
    virtual void UniformVec3v(int32_t program, const std::string&name, iVec3Array* values)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->UniformVec3v(name, values);
    }
    
    virtual void UniformVec4(int32_t program,
                             const string& name,
                             const vec4& value)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->UniformVec4(name, value);
    }
    
    virtual void UniformRect(int32_t program,
                             const string& name,
                             const rect& value)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->UniformRect(name, value);
    }
    
    virtual void UniformQuat(int32_t program,
                             const string& name,
                             const quat& value)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->UniformQuat(name, value);
    }
    
    virtual void UniformMat4f(int32_t program, const string& name, const mat4& value)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->UniformMat4f(name, value);
    }
    
    virtual void UniformMat4fv(int32_t program, const string& name, iMat4Array* values)
    {
        ObjectMap::iterator it = mObjects.find(program);
        piAssert(it != mObjects.end(), ;);
        SmartPtr<GraphicsProgramOpenGL> p = dynamic_cast<GraphicsProgramOpenGL*>(it->second.Ptr());
        p->UniformMat4fv(name, values);
    }
    
    //--------------------------------------------------------------------------
    
    
    virtual void CreateBuffer(int32_t name)
    {
        SmartPtr<BufferOpenGL> object = new BufferOpenGL();
        AddObject(name, object);
    }
    
    virtual void BindBuffer(int target, int32_t name)
    {
        if (name == 0)
        {
            glBindBuffer(ToGLBufferTarget(target), 0);
            return;
        }
        
        ObjectMap::iterator it = mObjects.find(name);
        
        if (it != mObjects.end())
        {
            SmartPtr<BufferOpenGL> obj = dynamic_cast<BufferOpenGL*>(it->second.Ptr());
            glBindBuffer(ToGLBufferTarget(target), obj->GetHandle());
        }
        else
        {
            PILOGE(PI_GRAPHICS_TAG, "BindBuffer: buffer(%d) not found.", name);
        }
    }
    
    virtual void BufferSubData(int32_t target, int64_t offset, int64_t size, iMemory* data)
    {
        glBufferSubData(ToGLBufferTarget(target),
                        (GLintptr)offset,
                        (GLsizeiptr)size,
                        data != nullptr ? data->Ptr() : nullptr);
    }
    
    
    virtual void BufferData(int target, int32_t name, int64_t size, iMemory* data)
    {
        ObjectMap::iterator it = mObjects.find(name);
        
        if (it != mObjects.end())
        {
            SmartPtr<BufferOpenGL> obj = dynamic_cast<BufferOpenGL*>(it->second.Ptr());
            glBufferData(ToGLBufferTarget(target),
                         (size_t)size,
                         data ? data->Ptr() : nullptr,
                         GL_STATIC_DRAW);
        }
        else
        {
            PILOGE(PI_GRAPHICS_TAG, "BindBuffer: buffer(%d) not found.", name);
        }
    }
    
    
    //--------------------------------------------------------------------------
    
    
    virtual void ActiveTexture(int32_t unit)
    {
        glActiveTexture(GL_TEXTURE0 + unit);
    }
    
    virtual void BindTexture(int32_t target, int32_t name)
    {
        if (name == 0)
        {
            glBindTexture(ToGLTextureTarget(target), 0);
            return;
        }
        
        ObjectMap::iterator it = mObjects.find(name);
        if (it != mObjects.end())
        {
            SmartPtr<TextureOpenGL> obj = dynamic_cast<TextureOpenGL*>(it->second.Ptr());
            glBindTexture(ToGLTextureTarget(target), obj->GetHandle());
        }
        else
        {
            auto oIt = mNativeObjMap.find(name);
            if (oIt != mNativeObjMap.end())
            {
                glBindTexture(ToGLTextureTarget(target), oIt->second);
            }
            else
            {
                PILOGE(PI_GRAPHICS_TAG, "BindTexture: texture(%d) not found.", name);
            }
        }
    }
    
    virtual void GenerateMipmap(int target)
    {
        glGenerateMipmap(ToGLTextureTarget(target));
    }
    
    virtual void PixelStorei(int name, int32_t value)
    {
        glPixelStorei(ToGLTextureParamName(name), value);
    }
    
    virtual void CreateTexture(int32_t name)
    {
        SmartPtr<TextureOpenGL> object = new TextureOpenGL();
        AddObject(name, object);
    }
    
    //----------------------------------------------------------------------
    
    virtual void CreateFramebuffer(int32_t name)
    {
        SmartPtr<FramebufferOpenGL> object = new FramebufferOpenGL();
        AddObject(name, object);
    }
    
    virtual void BindFramebuffer(int target, int32_t name)
    {
        if (name > 0)
        {
            ObjectMap::iterator it = mObjects.find(name);
            
            if (it != mObjects.end())
            {
                SmartPtr<FramebufferOpenGL> obj = dynamic_cast<FramebufferOpenGL*>(it->second.Ptr());
                glBindFramebuffer(ToGLFramebufferTarget(target), obj->GetHandle());
            }
            else
            {
                PILOGE(PI_GRAPHICS_TAG, "BindFramebuffer: framebuffer(%d) not found.", name);
            }
        }
        else
        {
            glBindFramebuffer(ToGLFramebufferTarget(target), mDefaultFramebuffer);
        }
    }
    
    virtual void FramebufferTexture2D(int framebufferTarget,
                                      int attachment,
                                      int textureTarget,
                                      int texture,
                                      int32_t level)
    {
        ObjectMap::iterator it = mObjects.find(texture);
        
        if (it != mObjects.end())
        {
            SmartPtr<TextureOpenGL> obj = dynamic_cast<TextureOpenGL*>(it->second.Ptr());
            glFramebufferTexture2D(ToGLFramebufferTarget(framebufferTarget),
                                   ToGLAttachment(attachment),
                                   ToGLTextureTarget(textureTarget),
                                   obj->GetHandle(),
                                   level);
        }
        else
        {
            PILOGE(PI_GRAPHICS_TAG, "FramebufferTexture2D: texture(%d) not found.", texture);
        }
    }
    
    virtual void FramebufferRenderbuffer(int target,
                                         int attachment,
                                         int32_t renderbuffer)
    {
        ObjectMap::iterator it = mObjects.find(renderbuffer);
        
        if (it != mObjects.end())
        {
            SmartPtr<RenderbufferOpenGL> obj = dynamic_cast<RenderbufferOpenGL*>(it->second.Ptr());
            glFramebufferRenderbuffer(ToGLFramebufferTarget(target),
                                      ToGLAttachment(attachment),
                                      GL_RENDERBUFFER,
                                      obj->GetHandle());
        }
        else
        {
            PILOGE(PI_GRAPHICS_TAG, "FramebufferRenderbuffer: renderbuffer(%d) not found.", renderbuffer);
        }
    }
    
    virtual void VerifyFramebufferState(int target)
    {
        GLenum status = glCheckFramebufferStatus(ToGLFramebufferTarget(target));
        
        if (status != GL_FRAMEBUFFER_COMPLETE)
        {
            PILOGE(PI_GRAPHICS_TAG, "Invalid framebuffer status:0x%X", status);
        }
        
        piAssert(status == GL_FRAMEBUFFER_COMPLETE, ;);
    }
    
    //----------------------------------------------------------------------
    
    virtual void CreateRenderbuffer(int32_t name)
    {
        SmartPtr<RenderbufferOpenGL> object = new RenderbufferOpenGL();
        AddObject(name, object);
    }
    
    virtual void BindRenderbuffer(int32_t name)
    {
        if (name > 0)
        {
            ObjectMap::iterator it = mObjects.find(name);
            
            if (it != mObjects.end())
            {
                SmartPtr<RenderbufferOpenGL> obj = dynamic_cast<RenderbufferOpenGL*>(it->second.Ptr());
                glBindRenderbuffer(GL_RENDERBUFFER, obj->GetHandle());
            }
            else
            {
                PILOGE(PI_GRAPHICS_TAG, "BindRenderbuffer: renderbuffer(%d) not found.", name);
            }
        }
        else
        {
            glBindRenderbuffer(GL_RENDERBUFFER, mDefaultRenderbuffer);
        }
    }
    
    virtual void RenderbufferStorage(int format, int32_t width, int32_t height)
    {
        GLenum glFormat = ToGLFormat(format);
        glRenderbufferStorage(GL_RENDERBUFFER, glFormat, width, height);
    }
    
protected:
    
    void AddObject(int32_t name, iRefObject* object)
    {
        mObjects[name] = object;
        
#if PI_DEBUG
        mObjectTypes[name] = object->GetClass();
#endif
    }
};


NSPI_END()


#endif




















