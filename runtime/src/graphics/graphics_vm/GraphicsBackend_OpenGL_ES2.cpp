/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.10.13   0.1     Create
 ******************************************************************************/
#include <pi/Graphics.h>
#include "GraphicsOp.h"
#include "GraphicsBackend.h"

#if defined(PI_IOS)
#   include <OpenGLES/ES2/gl.h>
#   include <OpenGLES/ES2/glext.h>
#elif defined(PI_ANDROID)
#   include <GLES2/gl2.h>
#	include <GLES2/gl2ext.h>
#endif

#include "GraphicsVM_Impl.h"

using namespace std;
#include "GraphicsUtils_OpenGL_ES2.h"

#include "GraphicsProgram_OpenGL.h"
#include "GraphicsBackend_OpenGL_ES.h"

NSPI_BEGIN()

class VertexArrayOpenGL_ES2_Sim : public iRefObject
{
private:
    ByteCodeVector mStates;
    
public:
    VertexArrayOpenGL_ES2_Sim()
    {
    }
    
    virtual ~VertexArrayOpenGL_ES2_Sim()
    {
    }
    
    void PushState(iGraphicsByteCode* byteCode)
    {
        mStates.push_back(byteCode);
    }
    
    const ByteCodeVector& GetStates() const
    {
        return mStates;
    }
    
    virtual string ToString() const
    {
        return "VertexArrayOpenGL_ES2_Sim";
    }
};

class VertexArrayOpenGL_ES2 : public iRefObject
{
private:
    GLuint mHandle;
    
public:
    VertexArrayOpenGL_ES2()
    {
#if !defined(PI_ANDROID)
        glGenVertexArraysOES(1, &mHandle);
#endif
    }
    
    virtual ~VertexArrayOpenGL_ES2()
    {
#if !defined(PI_ANDROID)
        glDeleteVertexArraysOES(1, &mHandle);
#endif
        mHandle = -1;
    }
    
    GLuint GetHandle() const
    {
        return mHandle;
    }
    
    virtual string ToString() const
    {
        return "VertexArrayOpenGL_ES2";
    }
};

class GraphicsBackend_OpenGL_ES2 : public GraphicsBackend_OpenGL_ES
{
private:
    int32_t mCurrVAO;
    
public:
    GraphicsBackend_OpenGL_ES2()
    :mCurrVAO(0)
    {
#if defined(PI_ANDROID)
        mCaps[eGraphicsCap_VAO] = false;
#endif
    }
    
    virtual ~GraphicsBackend_OpenGL_ES2()
    {
    }
    
    virtual int GetType() const
    {
        return eGraphicsBackend_OpenGL_ES2;
    }
    
    virtual bool Support(int op) const
    {
        return true;
    }
    
    virtual void EnableFeatures(int32_t features)
    {
        GraphicsBackend_OpenGL_ES::EnableFeatures(features);
        
        if (piFlagIs(features, eGraphicsFeature_Texture3D))
        {
            PILOGE(PI_GRAPHICS_TAG,
                   "%s not supported in OpenGL ES2.",
                   piGraphicsFeatureNames(eGraphicsFeature_Texture3D).c_str());
        }
    }
    
    virtual void DisableFeatures(int32_t features)
    {
        GraphicsBackend_OpenGL_ES::DisableFeatures(features);
        
        if (piFlagIs(features, eGraphicsFeature_Texture3D))
        {
            PILOGE(PI_GRAPHICS_TAG,
                   "%s not supported in OpenGL ES2.",
                   piGraphicsFeatureNames(eGraphicsFeature_Texture3D).c_str());
        }
    }
    
    virtual void CreateVertexArray(int32_t name)
    {
        SmartPtr<iRefObject> object;
        if (GetCapability(eGraphicsCap_VAO))
        {
            object = new VertexArrayOpenGL_ES2();
        }
        else
        {
            object = new VertexArrayOpenGL_ES2_Sim();
        }
        
        AddObject(name, object);
    }
    
    void DoBindBuffer(int target, int32_t name)
    {
        if (name == 0)
        {
            glBindBuffer(ToGLBufferTarget(target), 0);
            return;
        }
        
        ObjectMap::iterator it = mObjects.find(name);
        
        if (it != mObjects.end())
        {
            SmartPtr<BufferOpenGL> obj = dynamic_cast<BufferOpenGL*>(it->second.Ptr());
            glBindBuffer(ToGLBufferTarget(target), obj->GetHandle());
        }
        else
        {
            PILOGE(PI_GRAPHICS_TAG, "BindBuffer: buffer(%d) not found.", name);
        }
    }
    
    virtual void BindBuffer(int target, int32_t name)
    {
        DoBindBuffer(target, name);
        
        if (!GetCapability(eGraphicsCap_VAO))
        {
            PushVertexArrayState(new GraphicsByteCode(eGraphicsOp_BindBuffer, target, name));
        }
    }
    
    virtual void EnableVertexAttr(int32_t name)
    {
        glEnableVertexAttribArray(name);
        
        if (!GetCapability(eGraphicsCap_VAO))
        {
            PushVertexArrayState(new GraphicsByteCode(eGraphicsOp_EnableVertexAttr, name));
        }
    }
    
    void DoSetVertexAttr(int32_t name,
                         int32_t size,
                         int32_t type,
                         int32_t stride,
                         int32_t offset)
    {
        int64_t glOffset = offset;
        
        glVertexAttribPointer(name,
                              size,
                              ToGLType(type),
                              GL_FALSE,
                              stride,
                              (GLvoid*)glOffset);
    }
    
    virtual void VertexAttr(int32_t name,
                            int32_t size,
                            int32_t type,
                            int32_t stride,
                            int32_t offset)
    {
        DoSetVertexAttr(name, size, type, stride, offset);
        
        if (!GetCapability(eGraphicsCap_VAO))
        {
            PushVertexArrayState(new GraphicsByteCode(eGraphicsOp_VertexAttr, name, size, type, stride, offset));
        }
    }
    
    virtual void BindVertexArray(int32_t name)
    {
        if (name == 0)
        {
            if (GetCapability(eGraphicsCap_VAO))
            {
#if !defined(PI_ANDROID)
                glBindVertexArrayOES(0);
#endif
            }
            else
            {
                UnbindVertexArray();
            }
            
            mCurrVAO = 0;
            return;
        }
        
        ObjectMap::iterator it = mObjects.find(name);
        
        if (it != mObjects.end())
        {
            if (GetCapability(eGraphicsCap_VAO))
            {
#if !defined(PI_ANDROID)
                SmartPtr<VertexArrayOpenGL_ES2> obj = dynamic_cast<VertexArrayOpenGL_ES2*>(it->second.Ptr());
                glBindVertexArrayOES(obj->GetHandle());
#endif
            }
            else
            {
                SmartPtr<VertexArrayOpenGL_ES2_Sim> obj = dynamic_cast<VertexArrayOpenGL_ES2_Sim*>(it->second.Ptr());
                BindVertexArray(obj);
                mCurrVAO = name;
            }
        }
        else
        {
            PILOGE(PI_GRAPHICS_TAG, "BindVertexArray: vertex array(%d) not found.", name);
        }
    }
    
    virtual void DrawElementsInstanced(int mode, int32_t count, int type, int32_t instanceCount)
    {
#if !defined(PI_ANDROID)
        glDrawElementsInstancedEXT(ToGLDraw(mode), count, type, 0, instanceCount);
#else
        piAssert(false, ;);
#endif
    }
    
    virtual void SetClearDepth(float value)
    {
        glClearDepthf(value);
    }
    
    virtual void DrawArraysInstanced(int mode, int32_t first, int32_t count, int instanceCount)
    {
#if !defined(PI_ANDROID)
        glDrawArraysInstancedEXT(ToGLDraw(mode), first, count, instanceCount);
#else
        piAssert(false, ;);
#endif
    }
    
    virtual void SetPolygonMode(int face, int mode)
    {
        PILOGE(PI_GRAPHICS_TAG, "SetPolygonMode not supported in OpenGL ES2.");
    }
    
    virtual void TexImage2D(int target,
                            int32_t level,
                            int format,
                            iBitmap *bitmap,
                            int32_t planar)
    {        
        SmartPtr<iPixelFormat> pixelFormat = bitmap->GetPixelFormat();
        
        bitmap->Map(eMemMap_ReadOnly);
        
        SmartPtr<iMemory> data = bitmap->GetData(planar);
        
        int32_t width = bitmap->GetWidthOfPlanar(planar);
        int32_t height = bitmap->GetHeightOfPlanar(planar);
        int type = pixelFormat->GetType(planar);
        int extFormat = pixelFormat->GetFormat(planar);
        
        glPixelStorei(GL_UNPACK_ALIGNMENT, bitmap->GetPixelAlignment(planar));
        
        glTexImage2D(ToGLTextureTarget(target),
                     level,
                     ToGLFormat(format),
                     width,
                     height,
                     0,
                     ToGLFormat(extFormat),
                     ToGLType(type),
                     !data.IsNull() ? data->Ptr() : nullptr);
        
        bitmap->Unmap();
    }
    
    virtual void CompressedTexImage2D(int target,
                                      int32_t level,
                                      int format,
                                      iBitmap *bitmap,
                                      int32_t planar)
    {
        SmartPtr<iPixelFormat> pixelFormat = bitmap->GetPixelFormat();
        
        bitmap->Map(eMemMap_ReadOnly);
        
        SmartPtr<iMemory> data = bitmap->GetData(planar);
        
        int32_t width = bitmap->GetWidthOfPlanar(planar);
        int32_t height = bitmap->GetHeightOfPlanar(planar);
        
        glCompressedTexImage2D(ToGLTextureTarget(target),
                               level,
                               format,
                               width, height,
                               0,
                               (GLsizei)data->Size(),
                               data->Ptr());
        
        bitmap->Unmap();
    }
    
    
    virtual void CopyTexImage2D(int target,
                                int32_t level,
                                int32_t format,
                                int32_t x,
                                int32_t y,
                                int32_t width,
                                int32_t height)
    {
        glCopyTexImage2D(ToGLTextureTarget(target), level, ToGLFormat(format), x, y, width, height, 0);
    }
    
    virtual void TexParam(int32_t target, int32_t name, int32_t value)
    {
        int param = ToGLTextureParamName(name);
        if (param >= 0)
        {
            glTexParameteri(ToGLTextureTarget(target),
                            param,
                            ToGLTextureParamValue(value));
        }
    }
    
    virtual void TexSubImage2D(int target,
                               int32_t level,
                               int32_t xOffset, int32_t yOffset,
                               int32_t width, int32_t height,
                               iBitmap *bitmap,
                               int32_t planar)
    {
        SmartPtr<iPixelFormat> pixelFormat = bitmap->GetPixelFormat();
        
        bitmap->Map(eMemMap_ReadOnly);
        
        SmartPtr<iMemory> data = bitmap->GetData(planar);
        
        glPixelStorei(GL_UNPACK_ALIGNMENT, bitmap->GetPixelAlignment(planar));
        
        glTexSubImage2D(ToGLTextureTarget(target),
                        level,
                        xOffset, yOffset,
                        width, height,
                        ToGLFormat(pixelFormat->GetFormat(planar)),
                        ToGLType(pixelFormat->GetType(planar)),
                        !data.IsNull() ? data->Ptr() : nullptr);
        
        bitmap->Unmap();
    }
    
    virtual void CompressedTexSubImage2D(int target,
                                         int32_t level,
                                         int32_t xOffset, int32_t yOffset,
                                         int32_t width, int32_t height,
                                         iBitmap *bitmap,
                                         int32_t planar)
    {
        SmartPtr<iPixelFormat> pixelFormat = bitmap->GetPixelFormat();
        
        bitmap->Map(eMemMap_ReadOnly);
        
        SmartPtr<iMemory> data = bitmap->GetData(planar);
        
        glCompressedTexSubImage2D(ToGLTextureTarget(target),
                                  level,
                                  xOffset, yOffset,
                                  width, height,
                                  ToGLFormat(pixelFormat->GetName()),
                                  (GLsizei)data->Size(),
                                  data->Ptr());
        
        bitmap->Unmap();
    }
    
    virtual void CopyTexSubImage2D(int target,
                                   int32_t level,
                                   int32_t format,
                                   int32_t xOffset, int32_t yOffset,
                                   int32_t x, int32_t y,
                                   int32_t width, int32_t height)
    {
        glCopyTexSubImage2D(ToGLTextureTarget(target),
                            level,
                            xOffset, yOffset,
                            x, y,
                            width, height);
    }
    
private:
    
    void PushVertexArrayState(iGraphicsByteCode* byteCode)
    {
        piCheck(mCurrVAO != 0, ;);
        
        ObjectMap::iterator it = mObjects.find(mCurrVAO);
        
        if (it != mObjects.end())
        {
            SmartPtr<VertexArrayOpenGL_ES2_Sim> obj = dynamic_cast<VertexArrayOpenGL_ES2_Sim*>(it->second.Ptr());
            obj->PushState(byteCode);
        }
    }
    
    void BindVertexArray(VertexArrayOpenGL_ES2_Sim* obj)
    {
        const ByteCodeVector& states = obj->GetStates();
        for (auto byteCode : states)
        {
            switch (byteCode->GetOperation())
            {
                case eGraphicsOp_BindBuffer:
                {
                    int target = byteCode->GetArg(0);
                    int32_t buffer = byteCode->GetArg(1);
                    DoBindBuffer(target, buffer);
                    
                    PILOGV(PI_GRAPHICS_TAG,
                           "[VAO]BindBuffer(%s, %d)",
                           piGraphicsBufferName(target).c_str(),
                           buffer);
                    break;
                }
                case eGraphicsOp_EnableVertexAttr:
                {
                    int32_t name = byteCode->GetArg(0);
                    glEnableVertexAttribArray(name);
                    PILOGV(PI_GRAPHICS_TAG, "[VAO]EnableVertexAttr(%d)", name);
                    break;
                }
                case eGraphicsOp_VertexAttr:
                {
                    int32_t name = byteCode->GetArg(0);
                    int32_t size = byteCode->GetArg(1);
                    int type = byteCode->GetArg(2);
                    int32_t stride = byteCode->GetArg(3);
                    int32_t offset = byteCode->GetArg(4);
                    DoSetVertexAttr(name, size, type, stride, offset);
                    PILOGV(PI_GRAPHICS_TAG,
                           "[VAO]SetVertexAttr(%d, %d, %s, %d, %d)",
                           name,
                           size,
                           piTypeName(type).c_str(),
                           stride,
                           offset);
                    break;
                }
                default:
                    break;
            }
        }
    }
    
    void UnbindVertexArray()
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        PILOGV(PI_GRAPHICS_TAG, "[VAO]BindBuffer(eGraphicsBuffer_Vertex, 0)");
        PILOGV(PI_GRAPHICS_TAG, "[VAO]BindBuffer(eGraphicsBuffer_Index, 0)");
        
        piCheck(mCurrVAO > 0, ;);
        
        ObjectMap::iterator it = mObjects.find(mCurrVAO);
        piCheck(it != mObjects.end(), ;);
        
        SmartPtr<VertexArrayOpenGL_ES2_Sim> obj = dynamic_cast<VertexArrayOpenGL_ES2_Sim*>(it->second.Ptr());
        
        const ByteCodeVector& states = obj->GetStates();
        for (auto byteCode : states)
        {
            if (byteCode->GetOperation() == eGraphicsOp_EnableVertexAttr)
            {
                int32_t name = byteCode->GetArg(0);
                glDisableVertexAttribArray(name);
                
                PILOGV(PI_GRAPHICS_TAG, "[VAO]DisableVertexAttr(%d)", name);
            }
        }
    }
};

iGraphicsBackend* CreateGraphicsBackend_OpenGL_ES2()
{
    return new GraphicsBackend_OpenGL_ES2();
}




NSPI_END()



















