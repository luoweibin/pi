/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.10.13   0.1     Create
 ******************************************************************************/
#include "GraphicsVM_Impl.h"

NSPI_BEGIN()

int32_t GraphicsVM::CreateProgram()
{
    lock_guard<mutex> lock(mMutex);
    int32_t name = AllocName();
    PushByteCode(eGraphicsOp_CreateProgram, name);
    return name;
}

void GraphicsVM::CompileProgram(int32_t name,
                                const string& vertex,
                                const string& fragment)
{
    piAssert(name > 0, ;);
    
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_CompileProgram, name, vertex, fragment);
}

void GraphicsVM::LinkProgram(int32_t name)
{
    piAssert(name > 0, ;);
    
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_LinkProgram, name);
}

void GraphicsVM::UseProgram(int32_t name)
{
    piAssert(name >= 0, ;);
    
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_UseProgram, name);
}

void GraphicsVM::Uniform1i(int32_t program, const string& name, int32_t v)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Uniform1i, program, name, v);
}

void GraphicsVM::Uniform1iv(int32_t program,
                            const string& name,
                            iI32Array* values)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Uniform1iv, program, name, values);
}

void GraphicsVM::Uniform2i(int32_t program, const string& name, int32_t v1, int32_t v2)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Uniform2i, program, name, v1, v2);
}

void GraphicsVM::Uniform2iv(int32_t program,
                            const string& name,
                            iI32Array* values)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Uniform2iv, program, name, values);
}

void GraphicsVM::Uniform3i(int32_t program,
                           const string& name,
                           int32_t v1,
                           int32_t v2,
                           int32_t v3)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Uniform3i, program, name, v1, v2, v3);
}

void GraphicsVM::Uniform3iv(int32_t program,
                            const string& name,
                            iI32Array* values)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Uniform3iv, program, name, values);
}

void GraphicsVM::Uniform4i(int32_t program,
                           const string& name,
                           int32_t v1,
                           int32_t v2,
                           int32_t v3,
                           int32_t v4)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Uniform4i, program, name, v1, v2, v3, v4);
}

void GraphicsVM::Uniform4iv(int32_t program,
                            const string& name,
                            iI32Array* values)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Uniform4iv, program, name, values);
}

void GraphicsVM::Uniform1f(int32_t program, const string& name, float v)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Uniform1f, program, name, v);
}

void GraphicsVM::Uniform1fv(int32_t program,
                            const string& name,
                            iF32Array* values)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Uniform1fv, program, name, values);
}

void GraphicsVM::Uniform2f(int32_t program, const string& name, float v1, float v2)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Uniform2f, program, name, v1, v2);
}

void GraphicsVM::Uniform2fv(int32_t program,
                            const string& name,
                            iF32Array* values)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Uniform2fv, program, name, values);
}

void GraphicsVM::Uniform3f(int32_t program,
                           const string& name,
                           float v1,
                           float v2,
                           float v3)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Uniform3f, program, name, v1, v2, v3);
}

void GraphicsVM::Uniform3fv(int32_t program,
                            const string& name,
                            iF32Array* values)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Uniform3fv, program, name, values);
}

void GraphicsVM::Uniform4f(int32_t program,
                           const string& name,
                           float v1,
                           float v2,
                           float v3,
                           float v4)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Uniform4f, program, name, v1, v2, v3, v4);
}

void GraphicsVM::Uniform4fv(int32_t program,
                            const string& name,
                            iF32Array* values)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Uniform4fv, program, name, values);
}

void GraphicsVM::UniformVec3v(int32_t program, const std::string&name, iVec3Array* values)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_UniformVec3v, program, name, values);
}

void GraphicsVM::UniformVec3(int32_t program, const std::string& name, const vec3& value)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_UniformVec3, program, name, value);
}

void GraphicsVM::UniformVec4(int32_t program, const std::string& name, const vec4& value)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_UniformVec4, program, name, value);
}

void GraphicsVM::UniformRect(int32_t program, const std::string& name, const rect& value)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_UniformRect, program, name, value);
}

void GraphicsVM::UniformQuat(int32_t program, const std::string& name, const quat& value)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_UniformQuat, program, name, value);
}

void GraphicsVM::UniformMat4f(int32_t program, const string& name, const mat4& value)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_UniformMat4f, program, name, value);
}

void GraphicsVM::UniformMat4fv(int32_t program, const string& name, iMat4Array* values)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_UniformMat4fv, program, name, values);
}



/**
 * =================================================================================================
 * Dispatch
 */

void GraphicsVM::CallCreateProgram(iGraphicsByteCode* byteCode)
{
    int32_t name = byteCode->GetArg(0);
    mBackend->CreateProgram(name);
    
    PILOGV(PI_GRAPHICS_TAG, "CreateProgram(progrom:%d)", name);
}

void GraphicsVM::CallCompileProgram(iGraphicsByteCode* byteCode)
{
    int32_t name = byteCode->GetArg(0);
    string vertex = byteCode->GetArg(1);
    string fragment = byteCode->GetArg(2);
    mBackend->CompileProgram(name, vertex, fragment);
    
    PILOGV(PI_GRAPHICS_TAG,
           "CompileProgram(program:%d, vertex:%s, fragment:%s)",
           name,
           piToString(vertex).c_str(),
           piToString(fragment).c_str());
}

void GraphicsVM::CallLinkProgram(iGraphicsByteCode* byteCode)
{
    int32_t name = byteCode->GetArg(0);
    mBackend->LinkProgram(name);

    PILOGV(PI_GRAPHICS_TAG, "LinkProgram(program:%d)", name);
}

void GraphicsVM::CallUseProgram(iGraphicsByteCode* byteCode)
{
    int32_t name = byteCode->GetArg(0);
    mBackend->UseProgram(name);
    
    PILOGV(PI_GRAPHICS_TAG, "UseProgram(program:%d)", name);
}

void GraphicsVM::CallUniform1i(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    int32_t v1 = byteCode->GetArg(2);
    mBackend->Uniform1i(program, name, v1);
    
    PILOGV(PI_GRAPHICS_TAG, "Uniform1i(program:%d, name:%s, v:%d)", program, name.c_str(), v1);
}

void GraphicsVM::CallUniform1iv(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    SmartPtr<iI32Array> values = byteCode->GetArg(2).GetI32Array();
    mBackend->Uniform1iv(program, name, values);
    
    PILOGV(PI_GRAPHICS_TAG,
           "Uniform1iv(program:%d, name:%s, values:%s)",
           program,
           name.c_str(),
           piToString(values).c_str());
}

void GraphicsVM::CallUniform2i(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    int32_t v1 = byteCode->GetArg(2);
    int32_t v2 = byteCode->GetArg(3);
    mBackend->Uniform2i(program, name, v1, v2);
    
    PILOGV(PI_GRAPHICS_TAG,
           "Uniform2i(program:%d, name:%s, v1:%d, v2:%d)",
           program, name.c_str(), v1, v2);
}

void GraphicsVM::CallUniform2iv(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    SmartPtr<iI32Array> values = byteCode->GetArg(2).GetI32Array();
    mBackend->Uniform2iv(program, name, values);
    
    PILOGV(PI_GRAPHICS_TAG,
           "Uniform2iv(program:%d, name:%s, values:%s)",
           program,
           name.c_str(),
           piToString(values).c_str());
}

void GraphicsVM::CallUniform3i(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    int32_t v1 = byteCode->GetArg(2);
    int32_t v2 = byteCode->GetArg(3);
    int32_t v3 = byteCode->GetArg(4);
    mBackend->Uniform3i(program, name, v1, v2, v3);
    
    PILOGV(PI_GRAPHICS_TAG,
           "Uniform3i(program:%d, name:%s, v1:%d, v2:%d, v3:%d)",
           program, name.c_str(), v1, v2, v3);
}


void GraphicsVM::CallUniform3iv(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    SmartPtr<iI32Array> values = byteCode->GetArg(2).GetI32Array();
    mBackend->Uniform3iv(program, name, values);
    
    PILOGV(PI_GRAPHICS_TAG,
           "Uniform3iv(program:%d, name:%s, values:%s)",
           program,
           name.c_str(),
           piToString(values).c_str());
}


void GraphicsVM::CallUniformVec3v(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    SmartPtr<iVec3Array> values = byteCode->GetArg(2).GetVec3Array();
    mBackend->UniformVec3v(program, name, values);
    
    PILOGV(PI_GRAPHICS_TAG,
           "UniformVec3v(program:%d, name:%s, values:%s)",
           program,
           name.c_str(),
           piToString(values).c_str());
}

void GraphicsVM::CallUniform4i(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    int32_t v1 = byteCode->GetArg(2);
    int32_t v2 = byteCode->GetArg(3);
    int32_t v3 = byteCode->GetArg(4);
    int32_t v4 = byteCode->GetArg(5);
    mBackend->Uniform4i(program, name, v1, v2, v3, v4);
    
    PILOGV(PI_GRAPHICS_TAG,
           "Uniform4i(program:%d, name:%s, v1:%d, v2:%d, v3:%d, v4:%d)",
           program, name.c_str(), v1, v2, v3, v4);
}

void GraphicsVM::CallUniform4iv(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    SmartPtr<iI32Array> values = byteCode->GetArg(2).GetI32Array();
    
    mBackend->Uniform4iv(program, name, values);
    
    PILOGV(PI_GRAPHICS_TAG,
           "Uniform4iv(program:%d, name:%s, values:%s)",
           program,
           name.c_str(),
           piToString(values).c_str());
}

void GraphicsVM::CallUniform1f(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    float v1 = byteCode->GetArg(2);
    mBackend->Uniform1f(program, name, v1);
    
    PILOGV(PI_GRAPHICS_TAG,
           "Uniform4f(program:%d, name:%s, v:%f)",
           program, name.c_str(), v1);
}

void GraphicsVM::CallUniform1fv(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    SmartPtr<iF32Array> values = byteCode->GetArg(2).GetF32Array();
    
    PILOGV(PI_GRAPHICS_TAG,
           "Uniform1fv(program:%d, name:%s, values:%s)",
           program,
           name.c_str(),
           piToString(values).c_str());
    
    mBackend->Uniform1fv(program, name, values);
}

void GraphicsVM::CallUniform2f(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    float v1 = byteCode->GetArg(2);
    float v2 = byteCode->GetArg(3);
    mBackend->Uniform2f(program, name, v1, v2);
    
    PILOGV(PI_GRAPHICS_TAG,
           "Uniform2f(program:%d, name:%s, v1:%f, v2:%f)",
           program, name.c_str(), v1, v2);
}

void GraphicsVM::CallUniform2fv(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    SmartPtr<iF32Array> values = byteCode->GetArg(2).GetF32Array();
    mBackend->Uniform2fv(program, name, values);
    
    PILOGV(PI_GRAPHICS_TAG,
           "Uniform2fv(program:%d, name:%s, values:%s)",
           program,
           name.c_str(),
           piToString(values).c_str());
}

void GraphicsVM::CallUniform3f(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    float v1 = byteCode->GetArg(2);
    float v2 = byteCode->GetArg(3);
    float v3 = byteCode->GetArg(4);
    mBackend->Uniform3f(program, name, v1, v2, v3);

    PILOGV(PI_GRAPHICS_TAG,
           "Uniform3f(program:%d, name:%s, v1:%f, v2:%f, v3:%f)",
           program, name.c_str(), v1, v2, v3);
}


void GraphicsVM::CallUniform3fv(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    SmartPtr<iF32Array> values = byteCode->GetArg(2).GetF32Array();
    mBackend->Uniform3fv(program, name, values);

    PILOGV(PI_GRAPHICS_TAG,
           "Uniform3fv(program:%d, name:%s, values:%s)",
           program,
           name.c_str(),
           piToString(values).c_str());
}

void GraphicsVM::CallUniform4f(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    float v1 = byteCode->GetArg(2);
    float v2 = byteCode->GetArg(3);
    float v3 = byteCode->GetArg(4);
    float v4 = byteCode->GetArg(5);
    mBackend->Uniform4f(program, name, v1, v2, v3, v4);

    PILOGV(PI_GRAPHICS_TAG,
           "Uniform4f(program:%d, name:%s, v1:%f, v2:%f, v3:%f, v4:%f)",
           program, name.c_str(), v1, v2, v3, v4);
}

void GraphicsVM::CallUniform4fv(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    SmartPtr<iF32Array> values = byteCode->GetArg(2).GetF32Array();
    mBackend->Uniform4fv(program, name, values);

    PILOGV(PI_GRAPHICS_TAG,
           "Uniform4fv(program:%d, name:%s, values:%s)",
           program,
           name.c_str(),
           piToString(values).c_str());
}

void GraphicsVM::CallUniformVec3(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    vec3 value = byteCode->GetArg(2);
    mBackend->UniformVec3(program, name, value);
    
    PILOGV(PI_GRAPHICS_TAG,
           "UniformVec3(program:%d, name:%s, value:%s)",
           program,
           name.c_str(),
           piToString(value).c_str());
}

void GraphicsVM::CallUniformVec4(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    vec4 value = byteCode->GetArg(2);
    mBackend->UniformVec4(program, name, value);

    PILOGV(PI_GRAPHICS_TAG,
           "UniformVec4(program:%d, name:%s, value:%s)",
           program,
           name.c_str(),
           piToString(value).c_str());
}

void GraphicsVM::CallUniformRect(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    rect value = byteCode->GetArg(2);
    mBackend->UniformRect(program, name, value);
    
    PILOGV(PI_GRAPHICS_TAG,
           "UniformRect(program:%d, name:%s, value:%s)",
           program,
           name.c_str(),
           piToString(value).c_str());
}

void GraphicsVM::CallUniformQuat(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    quat value = byteCode->GetArg(2);
    mBackend->UniformQuat(program, name, value);
    
    PILOGV(PI_GRAPHICS_TAG,
           "UniformQuat(program:%d, name:%s, value:%s)",
           program,
           name.c_str(),
           piToString(value).c_str());
}

void GraphicsVM::CallUniformMat4f(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    mat4 v = byteCode->GetArg(2);
    mBackend->UniformMat4f(program, name, v);

    PILOGV(PI_GRAPHICS_TAG,
           "UniformMat4f(program:%d, name:%s, value:%s)",
           program,
           name.c_str(),
           piToString(v).c_str());
}

void GraphicsVM::CallUniformMat4fv(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    string name = byteCode->GetArg(1);
    SmartPtr<iMat4Array> values = byteCode->GetArg(2).GetMat4Array();
    mBackend->UniformMat4fv(program, name, values);

    PILOGV(PI_GRAPHICS_TAG,
           "UniformMat4fv(program:%d, name:%s, values:%s)",
           program,
           name.c_str(),
           piToString(values).c_str());
}



NSPI_END()



































