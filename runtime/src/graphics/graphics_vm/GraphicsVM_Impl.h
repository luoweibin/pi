/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2015.4.10   0.1     Create
 ******************************************************************************/
#ifndef PI_SRC_GRAPHICS_GRAPHICSVM_GRAPHICSVMIMPL_H
#define PI_SRC_GRAPHICS_GRAPHICSVM_GRAPHICSVMIMPL_H

#include "GraphicsBackend.h"
#include "GraphicsOp.h"
#include <mutex>
#include <deque>

using namespace std;

NSPI_BEGIN()

struct iGraphicsByteCode : public iRefObject
{
    virtual ~iGraphicsByteCode() {}
    
    virtual int GetOperation() const = 0;
    
    virtual Var GetArg(int32_t i) const = 0;
};

class GraphicsByteCode : public iGraphicsByteCode
{
private:
    int op;
    Var mArgs[9];
    
public:
    GraphicsByteCode(int operation,
                     const Var &arg0 = Var(),
                     const Var &arg1 = Var(),
                     const Var &arg2 = Var(),
                     const Var &arg3 = Var(),
                     const Var &arg4 = Var(),
                     const Var &arg5 = Var(),
                     const Var &arg6 = Var(),
                     const Var &arg7 = Var(),
                     const Var &arg8 = Var())
    {
        op = operation;
        mArgs[0] = arg0;
        mArgs[1] = arg1;
        mArgs[2] = arg2;
        mArgs[3] = arg3;
        mArgs[4] = arg4;
        mArgs[5] = arg5;
        mArgs[6] = arg6;
        mArgs[7] = arg7;
        mArgs[8] = arg8;
    }
    
    virtual int GetOperation() const
    {
        return op;
    }
    
    Var GetArg(int32_t i) const
    {
        return mArgs[i];
    }
};

typedef vector<SmartPtr<iGraphicsByteCode>> ByteCodeVector;

class ByteCodeBatch : public iRefObject
{
private:
    int64_t mSubmitTime;
    ByteCodeVector mByteCodes;
    
public:
    ByteCodeBatch():
    mSubmitTime(0)
    {
    }
    
    virtual ~ByteCodeBatch()
    {
    }
    
    void Push(iGraphicsByteCode *byteCode)
    {
        mByteCodes.push_back(byteCode);
    }
    
    void SetSubmitTime(int64_t time)
    {
        mSubmitTime = time;
    }
    
    const ByteCodeVector& GetByteCodes() const
    {
        return mByteCodes;
    }
    
    int64_t GetSubmitTime() const
    {
        return mSubmitTime;
    }
};


class GraphicsVM : public iGraphicsVM
{
private:
    int32_t mFreeName;
    
    SmartPtr<iGraphicsBackend> mBackend;
    
    typedef deque<SmartPtr<ByteCodeBatch>> BatchQueue;
    
    BatchQueue mBatchQueue;
    SmartPtr<ByteCodeBatch> mPushBatch;
    
    mutable mutex mMutex;
    
    int32_t mFeatures;
    int32_t mFramebuffer;
    int32_t mRenderbuffer;
        
    rect mViewport;
    
    typedef map<string, int32_t> AliasMap;
    AliasMap mAlias;
    
    typedef map<int32_t, int32_t> RefCountMap;
    RefCountMap mRefCounts;
    
public:
    GraphicsVM(iGraphicsBackend* backend);
    
    virtual ~GraphicsVM();
    
    virtual void DumpDebugInfo();
    
    virtual int GetType() const;
    
    virtual int32_t CreateNativeGraphicsObject(int64_t nativeHandle);
    
    virtual void RetainObject(int32_t name);
    
    virtual void ReleaseObejct(int32_t name);
    
    virtual bool AddAlias(const std::string& alias, int32_t name);
    
    virtual void RemoveAlias(const std::string& alias);
    
    virtual int32_t GetObject(const std::string& alias) const;
    
    virtual void SetCapability(int cap, bool value);
    
    virtual bool GetCapability(int cap) const;
    
    virtual void FlushCurrentContext();
    
    virtual void PushGroupMarker(const std::string& marker);
    
    virtual void PopGroupMarker();
    
    virtual void InsertEventMarker(const std::string& marker);
    
    virtual void Viewport(int32_t x, int32_t y, int32_t width, int32_t height);
    
    virtual rect GetViewport() const;
    
    virtual void Clear(int32_t fields);
    
    virtual void DrawArrays(int mode, int32_t first, int32_t count);
    
    virtual void DrawArraysInstanced(int mode, int32_t first, int32_t count, int32_t instanceCount);
    
    virtual void DrawElements(int mode, int32_t count, int type);
    
    virtual void DrawElementsOffset(int mode, int32_t count, int type, int32_t offset);
    
    virtual void DrawElementsInstanced(int mode, int32_t count, int type, int32_t instanceCount);
    
    virtual void BeforeRun();
    
    virtual void AfterRun();
    
    virtual void Submit();
    
    virtual void Flush();
    
    /**
     * -----------------------------------------------------------------------------------------
     * State
     */
    
    virtual void BlendColor(const vec4& color);
    
    virtual void BlendEquation(int mode);
    
    virtual void BlendFunc(int sfactor, int dfactor);
    
    virtual void BlendFuncSeparate(int32_t index, int sfactor, int dfactor);
    
    virtual void CullFace(int face);
    
    virtual void ClearColor(const vec4 &color);
    
    virtual void ClearDepth(float value);
    
    virtual void Winding(int32_t winding);
    
    virtual void EnableFeatures(int features);
    
    virtual void DisableFeatures(int features);
    
    virtual bool IsFeatureEnabled(int32_t feature) const;
    
    virtual void StencilFunc(int func, int32_t ref, int64_t mask);
    
    virtual void ClearStencil(int value);
    
    virtual void StencilOp(int sfail, int dbfail, int dbpass);
    
    virtual void StencilOpSeparate(int face, int sfail, int dbfail, int dbpass);
    
    virtual void StencilMask(int64_t mask);
    
    virtual void LineWidth(float width);
    
    virtual void PolygonOffset(float factor, float units);
    
    virtual void PolygonMode(int face, int mode);
    
    virtual void DepthFunction(int func);
    
    virtual void DepthMask(bool flag);
    
    virtual void ColorMask(bool red, bool green, bool blue, bool alpha);
    
    virtual void Hint(int target, int value);
    
    /**
     * -----------------------------------------------------------------------------------------
     * Program
     */
    
    virtual int32_t CreateProgram();
    
    virtual void CompileProgram(int32_t name,
                                const string& vertex,
                                const string& fragment);
    
    virtual void LinkProgram(int32_t name);
    
    virtual void UseProgram(int32_t name);
    
    virtual void Uniform1i(int32_t program, const string& name, int32_t v);
    
    virtual void Uniform1iv(int32_t program,
                            const string& name,
                            iI32Array* values);
    
    virtual void Uniform2i(int32_t program, const string& name, int32_t v1, int32_t v2);
    
    virtual void Uniform2iv(int32_t program,
                            const string& name,
                            iI32Array* values);
    
    virtual void Uniform3i(int32_t program,
                           const string& name,
                           int32_t v1,
                           int32_t v2,
                           int32_t v3);
    
    virtual void Uniform3iv(int32_t program,
                            const string& name,
                            iI32Array* values);
    
    virtual void Uniform4i(int32_t program,
                           const string& name,
                           int32_t v1,
                           int32_t v2,
                           int32_t v3,
                           int32_t v4);
    
    virtual void Uniform4iv(int32_t program,
                            const string& name,
                            iI32Array* values);
    
    virtual void Uniform1f(int32_t program, const string& name, float v);
    
    virtual void Uniform1fv(int32_t program,
                            const string& name,
                            iF32Array* values);
    
    virtual void Uniform2f(int32_t program, const string& name, float v1, float v2);
    
    virtual void Uniform2fv(int32_t program,
                            const string& name,
                            iF32Array* values);
    
    virtual void Uniform3f(int32_t program,
                           const string& name,
                           float v1,
                           float v2,
                           float v3);
    
    virtual void Uniform3fv(int32_t program,
                            const string& name,
                            iF32Array* values);
    
    virtual void Uniform4f(int32_t program,
                           const string& name,
                           float v1,
                           float v2,
                           float v3,
                           float v4);
    
    virtual void Uniform4fv(int32_t program,
                            const string& name,
                            iF32Array* values);
    
    virtual void UniformVec3(int32_t program, const std::string& name, const vec3& value);

    virtual void UniformVec3v(int32_t program, const std::string&name, iVec3Array* values);
    
    virtual void UniformVec4(int32_t program, const std::string& name, const vec4& value);
    
    virtual void UniformMat4f(int32_t program, const string& name, const mat4& value);
    
    virtual void UniformMat4fv(int32_t program, const string& name, iMat4Array* values);
    
    virtual void UniformRect(int32_t program, const std::string& name, const rect& value);
    
    virtual void UniformQuat(int32_t program, const std::string& name, const quat& value);
    
    /**
     * -----------------------------------------------------------------------------------------
     * Object
     */
    
    virtual void EnableVertexAttr(int32_t name);
    
    virtual void BindVertexAttr(int32_t program, int32_t index, const std::string& name);
    
    virtual void DisableVertexAttr(int32_t name);
    
    virtual void VertexAttr(int32_t name,
                               int32_t size,
                               int32_t type,
                               int32_t stride,
                               int32_t offset);
    
    virtual int32_t CreateVertexArray();
    
    virtual void BindVertexArray(int32_t name);
    
    
    virtual int32_t CreateBuffer();
    
    virtual void BindBuffer(int target, int32_t name);
    
    virtual void BufferData(int target, int32_t name, int64_t size, iMemory* data);
    
    virtual void BufferSubData(int32_t target, int64_t offset, int64_t size, iMemory* data);
    
    /**
     * -----------------------------------------------------------------------------------------
     * Texture
     */
    
    virtual void GenerateMipmap(int target);
    
    virtual void ActiveTexture(int32_t unit);
    
    virtual void BindTexture(int32_t target, int32_t unit);
    
    virtual void TexParam(int32_t target, int32_t name, int32_t value);
    
    virtual int32_t CreateTexture();
    
    virtual void TexImage2D(int target,
                            int32_t level,
                            int format,
                            iBitmap *bitmap,
                            int32_t planar);
    
    virtual void CompressedTexImage2D(int target,
                                      int32_t level,
                                      int32_t format,
                                      iBitmap *bitmap,
                                      int32_t planar);
    
    virtual void CopyTexImage2D(int target,
                                int32_t level,
                                int32_t format,
                                int32_t x,
                                int32_t y,
                                int32_t width,
                                int32_t height);
    
    virtual void TexSubImage2D(int target,
                               int32_t level,
                               int32_t xOffset, int32_t yOffset,
                               int32_t width, int32_t height,
                               iBitmap *bitmap,
                               int32_t planar);
    
    virtual void CompressedTexSubImage2D(int target,
                                         int32_t level,
                                         int32_t xOffset, int32_t yOffset,
                                         int32_t width, int32_t height,
                                         iBitmap *bitmap,
                                         int32_t planar);
    
    virtual void CopyTexSubImage2D(int target,
                                   int32_t level,
                                   int32_t format,
                                   int32_t xOffset, int32_t yOffset,
                                   int32_t x, int32_t y,
                                   int32_t width, int32_t height);
    
    virtual void PixelStorei(int name, int32_t value);
    
    /**
     * -----------------------------------------------------------------------------------------
     * Framebuffer
     */
    
    virtual int32_t GetFramebuffer() const;
    
    virtual int32_t CreateFramebuffer();
    
    virtual void BindFramebuffer(int target, int32_t name);
    
    virtual void FramebufferTexture2D(int framebufferTarget,
                                      int attachment,
                                      int textureTarget,
                                      int texture,
                                      int32_t level);
    
    virtual void FramebufferRenderbuffer(int target,
                                         int attachment,
                                         int32_t renderbuffer);
    
    virtual void VerifyFramebufferState(int target);
    
    virtual int32_t CreateRenderbuffer();
    
    virtual void BindRenderbuffer(int32_t name);
    
    virtual void RenderbufferStorage(int format, int32_t width, int32_t height);
    
    virtual void Run();
    
private:
    
    /**
     * =========================================================================================
     * Misc
     */
    
    void Dispatch(iGraphicsByteCode* byteCode);
    
    void CallDumpDebugInfo(iGraphicsByteCode* byteCode);
    
    void CallCreateNativeGraphicsObject(iGraphicsByteCode* byteCode);
    
    void CallPushGroupMarker(iGraphicsByteCode* byteCode);
    
    void CallPopGroupMarker(iGraphicsByteCode* byteCode);
    
    void CallInsertEventMarker(iGraphicsByteCode* byteCode);
    
    void CallClear(iGraphicsByteCode* byteCode);
    
    void CallViewport(iGraphicsByteCode* byteCode);
    
    void CallDrawArrays(iGraphicsByteCode* byteCode);
    
    void CallDrawArraysInstanced(iGraphicsByteCode* byteCode);
    
    void CallDrawElements(iGraphicsByteCode* byteCode);
    
    void CallDrawElementsOffset(iGraphicsByteCode* byteCode);
    
    void CallDrawElementsInstanced(iGraphicsByteCode* byteCode);
    
    void CallRetain(iGraphicsByteCode* byteCode);
    
    void CallDestroy(iGraphicsByteCode* byteCode);
    
    void CallFlush(iGraphicsByteCode* byteCode);
    
    void CallWinding(iGraphicsByteCode* byteCode);
    
    
    /**
     * =========================================================================================
     * State
     */
    
    void CallBlendColor(iGraphicsByteCode* byteCode);

    void CallBlendEquation(iGraphicsByteCode* byteCode);

    void CallBlendFunc(iGraphicsByteCode* byteCode);
    
    void CallBlendFuncSeparate(iGraphicsByteCode* byteCode);
    
    void CallHint(iGraphicsByteCode* byteCode);
    
    void CallStencilMask(iGraphicsByteCode* byteCode);
    
    void CallLineWidth(iGraphicsByteCode* byteCode);
    
    void CallStencilOp(iGraphicsByteCode* byteCode);
    
    void CallStencilOpSeparate(iGraphicsByteCode* byteCode);
    
    void CallStencilFunc(iGraphicsByteCode* byteCode);
    
    void CallClearStencil(iGraphicsByteCode* byteCode);
    
    void CallClearDepth(iGraphicsByteCode* byteCode);
    
    void CallDepthMask(iGraphicsByteCode* byteCode);
    
    void CallColorMask(iGraphicsByteCode* byteCode);
        
    void CallCullFace(iGraphicsByteCode* byteCode);
    
    void CallClearColor(iGraphicsByteCode* byteCode);
    
    void CallEnableFeatures(iGraphicsByteCode* byteCode);
    
    void CallDisableFeatures(iGraphicsByteCode* byteCode);
    
    void CallPolygonOffset(iGraphicsByteCode* byteCode);
    
    void CallPolygonMode(iGraphicsByteCode* byteCode);
    
    void CallDepthFunction(iGraphicsByteCode *byteCode);
        
    
    /**
     * -----------------------------------------------------------------------------------------
     * Program
     */
    
    void CallCreateProgram(iGraphicsByteCode* byteCode);
    
    void CallCompileProgram(iGraphicsByteCode* byteCode);
    
    void CallLinkProgram(iGraphicsByteCode* byteCode);
    
    void CallUseProgram(iGraphicsByteCode* byteCode);
    
    void CallUniform1i(iGraphicsByteCode* byteCode);
    
    void CallUniform1iv(iGraphicsByteCode* byteCode);
    
    void CallUniform2i(iGraphicsByteCode* byteCode);
    
    void CallUniform2iv(iGraphicsByteCode* byteCode);
    
    void CallUniform3i(iGraphicsByteCode* byteCode);
    
    void CallUniform3iv(iGraphicsByteCode* byteCode);
    
    void CallUniformVec3v(iGraphicsByteCode* byteCode);
    
    void CallUniform4i(iGraphicsByteCode* byteCode);
    
    void CallUniform4iv(iGraphicsByteCode* byteCode);
    
    void CallUniform1f(iGraphicsByteCode* byteCode);
    
    void CallUniform1fv(iGraphicsByteCode* byteCode);
    
    void CallUniform2f(iGraphicsByteCode* byteCode);
    
    void CallUniform2fv(iGraphicsByteCode* byteCode);
    
    void CallUniform3f(iGraphicsByteCode* byteCode);
    
    void CallUniform3fv(iGraphicsByteCode* byteCode);
    
    void CallUniform4f(iGraphicsByteCode* byteCode);
    
    void CallUniform4fv(iGraphicsByteCode* byteCode);
    
    void CallUniformVec4(iGraphicsByteCode* byteCode);
    
    void CallUniformVec3(iGraphicsByteCode* byteCode);
    
    void CallUniformMat4f(iGraphicsByteCode* byteCode);
    
    void CallUniformMat4fv(iGraphicsByteCode* byteCode);
    
    void CallUniformRect(iGraphicsByteCode* byteCode);

    void CallUniformQuat(iGraphicsByteCode* byteCode);
    
    /**
     * -----------------------------------------------------------------------------------------
     * Texture
     */
    
    void CallGenerateMipmap(iGraphicsByteCode* byteCode);
    
    void CallCreateTexture(iGraphicsByteCode* byteCode);
    
    void CallActiveTexture(iGraphicsByteCode* byteCode);
    
    void CallBindTexture(iGraphicsByteCode* byteCode);
    
    void CallTexImage2D(iGraphicsByteCode* byteCode);
    
    void CallCompressedTexImage2D(iGraphicsByteCode* byteCode);
    
    void CallCopyTexImage2D(iGraphicsByteCode* byteCode);
    
    void CallTexSubImage2D(iGraphicsByteCode* byteCode);
    
    void CallCompressedTexSubImage2D(iGraphicsByteCode* byteCode);
    
    void CallCopyTexSubImage2D(iGraphicsByteCode* byteCode);

    void CallTexParam(iGraphicsByteCode* byteCode);
    
    void CallPixelStorei(iGraphicsByteCode* byteCode);
     
    
    /** ========================================================================
     *  Framebuffer
     */
    
    void CallVerifyFramebufferState(iGraphicsByteCode* byteCode);
    
    void CallCreateFramebuffer(iGraphicsByteCode* byteCode);
    
    void CallBindFramebuffer(iGraphicsByteCode* byteCode);
    
    void CallFramebufferTexture2D(iGraphicsByteCode* byteCode);
    
    void CallFramebufferRenderbuffer(iGraphicsByteCode* byteCode);
    
    void CallCreateRenderbuffer(iGraphicsByteCode* byteCode);
    
    void CallBindRenderbuffer(iGraphicsByteCode* byteCode);
    
    void CallRenderbufferStorage(iGraphicsByteCode* byteCode);    
    
    
    /**
     * =========================================================================================
     * Object
     */
    
    void CallEnableVertexAttr(iGraphicsByteCode* byteCode);
    
    void CallBindVertexAttr(iGraphicsByteCode* byteCode);
    
    void CallDisableVertexAttr(iGraphicsByteCode* byteCode);
    
    void CallVertexAttr(iGraphicsByteCode* byteCode);
    
    void CallCreateBuffer(iGraphicsByteCode* byteCode);
    
    void CallBindBuffer(iGraphicsByteCode* byteCode);
    
    void CallBufferData(iGraphicsByteCode* byteCode);
    
    void CallBufferSubData(iGraphicsByteCode* byteCode);
    
    void CallCreateVertexArray(iGraphicsByteCode* byteCode);
    
    void CallBindVertexArray(iGraphicsByteCode* byteCode);
  
    
    /**
     * =========================================================================================
     * Misc
     */
    
    static void DumpByteCodes(string title, const ByteCodeVector& byteCodes);
    
    int32_t AllocName();
    
    void PushByteCode(int operation,
                      const Var &arg0 = Var(),
                      const Var &arg1 = Var(),
                      const Var &arg2 = Var(),
                      const Var &arg3 = Var(),
                      const Var &arg4 = Var(),
                      const Var &arg5 = Var(),
                      const Var &arg6 = Var(),
                      const Var &arg7 = Var(),
                      const Var &arg8 = Var());
};


NSPI_END()

#endif
