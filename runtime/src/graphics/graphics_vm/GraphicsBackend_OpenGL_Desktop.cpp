/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2015.4.9   0.1     Create
 ******************************************************************************/
#include <pi/Graphics.h>

#if defined(PI_MACOS)
#   include <OpenGL/gl3.h>
#   include <OpenGL/gl3ext.h>
#endif

using namespace std;

#include "GraphicsUtils_OpenGL_Desktop.h"

#include "GraphicsProgram_OpenGL.h"
#include "GraphicsBackend_OpenGL.h"

NSPI_BEGIN()

class VertexArrayOpenGL : public iRefObject
{
private:
    GLuint mHandle;
    
public:
    VertexArrayOpenGL()
    {
        glGenVertexArrays(1, &mHandle);
    }
    
    virtual ~VertexArrayOpenGL()
    {
        glDeleteVertexArrays(1, &mHandle);
        mHandle = -1;
    }
    
    GLuint GetHandle() const
    {
        return mHandle;
    }
};

//==============================================================================


class GraphicsBackend_OpenGL_Desktop : public GraphicsBackend_OpenGL
{
public:
    GraphicsBackend_OpenGL_Desktop()
    {
    }
    
    virtual ~GraphicsBackend_OpenGL_Desktop()
    {
    }
    
    virtual void ClearDepth(float value)
    {
        glClearDepth(value);
    }
    
    virtual void DepthFunction(int func)
    {
        glDepthFunc(ToGLFunc(func));
    }
    
    virtual void DisableFeatures(int features)
    {
        GraphicsBackend_OpenGL::DisableFeatures(features);
        
        if (piFlagIs(features, eGraphicsFeature_Texture3D))
        {
            glDisable(GL_TEXTURE_3D);
        }
        
        if (piFlagIs(features, eGraphicsFeature_MultiSample))
        {
            glDisable(GL_MULTISAMPLE);
        }
        
        if (piFlagIs(features, eGraphicsFeature_PolygonOffset_Point))
        {
            glDisable(GL_POLYGON_OFFSET_POINT);
        }
        
        if (piFlagIs(features, eGraphicsFeature_PolygonOffset_Line))
        {
            glDisable(GL_POLYGON_OFFSET_LINE);
        }
        
        if (piFlagIs(features, eGraphicsFeature_LineSmooth))
        {
            glDisable(GL_LINE_SMOOTH);
        }
        
        if (piFlagIs(features, eGraphicsFeature_Texture1D))
        {
            glDisable(GL_TEXTURE_1D);
        }
    }
    
    virtual void LineWidth(float width)
    {
        glLineWidth(width);
    }
    
    virtual void EnableFeatures(int features)
    {
        GraphicsBackend_OpenGL::EnableFeatures(features);
        
        if (piFlagIs(features, eGraphicsFeature_Texture3D))
        {
            glEnable(GL_TEXTURE_3D);
        }
        
        if (piFlagIs(features, eGraphicsFeature_MultiSample))
        {
            glEnable(GL_MULTISAMPLE);
        }
        
        if (piFlagIs(features, eGraphicsFeature_PolygonOffset_Point))
        {
            glEnable(GL_POLYGON_OFFSET_POINT);
        }
        
        if (piFlagIs(features, eGraphicsFeature_PolygonOffset_Line))
        {
            glEnable(GL_POLYGON_OFFSET_LINE);
        }
        
        if (piFlagIs(features, eGraphicsFeature_LineSmooth))
        {
            glEnable(GL_LINE_SMOOTH);
        }
        
        if (piFlagIs(features, eGraphicsFeature_Texture1D))
        {
            glEnable(GL_TEXTURE_1D);
        }
    }
    
    virtual void BlendFuncSeparate(int32_t index, int sfactor, int dfactor)
    {
        glBlendFunci(index, ToGLBlendFunc(sfactor), ToGLBlendFunc(dfactor));
    }
    
    virtual void DrawElementsInstanced(int mode, int32_t count, int type, int32_t instanceCount)
    {
        glDrawElementsInstanced(ToGLDraw(mode), count, ToGLType(type), 0, instanceCount);
    }
    
    virtual void DrawArraysInstanced(int mode, int32_t first, int32_t count, int instanceCount)
    {
        glDrawArraysInstanced(ToGLDraw(mode), first, count, instanceCount);
    }
    
    virtual void PolygonMode(int face, int mode)
    {
        glPolygonMode(ToGLFace(face), ToGLPolygonMode(mode));
    }
    
    virtual void DisableVertexAttr(int32_t name)
    {
        glDisableVertexAttribArray(name);
    }
    
    virtual void TexParam(int32_t target, int32_t name, int32_t value)
    {
        int param = ToGLTextureParamName(name);
        if (param >= 0)
        {
            glTexParameteri(ToGLTextureTarget(target),
                            param,
                            ToGLTextureParamValue(value));
        }
    }
    
    virtual void TexImage2D(int target,
                            int32_t level,
                            int format,
                            iBitmap *bitmap,
                            int32_t planar)
    {
        bitmap->Map(eMemMap_ReadOnly);

        SmartPtr<iPixelFormat> pixelFormat = bitmap->GetPixelFormat();
        
        SmartPtr<iMemory> data = bitmap->GetData(planar);
        
        glPixelStorei(GL_UNPACK_ROW_LENGTH, bitmap->GetWidthOfPlanar(planar));
        
        int32_t width = bitmap->GetWidthOfPlanar(planar);
        int32_t height = bitmap->GetHeightOfPlanar(planar);
        int type = pixelFormat->GetType(planar);
        int extFormat = pixelFormat->GetFormat(planar);
        
        glTexImage2D(ToGLTextureTarget(target),
                     level,
                     ToGLFormat(format),
                     width,
                     height,
                     0,
                     ToGLFormat(extFormat),
                     ToGLType(type),
                     !data.IsNull() ? data->Ptr() : nullptr);
        
        glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
        
        bitmap->Unmap();
    }
    
    
    virtual void CompressedTexImage2D(int target,
                                      int32_t level,
                                      int format,
                                      iBitmap *bitmap,
                                      int32_t planar)
    {
        bitmap->Map(eMemMap_ReadOnly);

        SmartPtr<iPixelFormat> pixelFormat = bitmap->GetPixelFormat();
        
        SmartPtr<iMemory> data = bitmap->GetData(planar);
        
        glPixelStorei(GL_UNPACK_ROW_LENGTH, bitmap->GetWidthOfPlanar(planar));
        
        int32_t width = bitmap->GetWidthOfPlanar(planar);
        int32_t height = bitmap->GetHeightOfPlanar(planar);
        
        glCompressedTexImage2D(ToGLTextureTarget(target),
                               level,
                               format,
                               width, height,
                               0,
                               (GLsizei)data->Size(),
                               data->Ptr());
        
        glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
        
        bitmap->Unmap();
    }
    
    
    virtual void CopyTexImage2D(int target,
                                int32_t level,
                                int32_t format,
                                int32_t x,
                                int32_t y,
                                int32_t width,
                                int32_t height)
    {
        glCopyTexImage2D(ToGLTextureTarget(target), level, ToGLFormat(format), x, y, width, height, 0);
    }
    
    
    virtual void TexSubImage2D(int target,
                               int32_t level,
                               int32_t xOffset, int32_t yOffset,
                               int32_t width, int32_t height,
                               iBitmap *bitmap,
                               int32_t planar)
    {
        bitmap->Map(eMemMap_ReadOnly);
        
        SmartPtr<iPixelFormat> pixelFormat = bitmap->GetPixelFormat();
        
        SmartPtr<iMemory> data = bitmap->GetData(planar);
        
        glPixelStorei(GL_UNPACK_ROW_LENGTH, bitmap->GetWidthOfPlanar(planar));
        
        glTexSubImage2D(ToGLTextureTarget(target),
                        level,
                        xOffset, yOffset,
                        width, height,
                        ToGLFormat(pixelFormat->GetFormat(planar)),
                        ToGLType(pixelFormat->GetType(planar)),
                        !data.IsNull() ? data->Ptr() : nullptr);
        
        glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
        
        bitmap->Unmap();
    }
    
    virtual void CompressedTexSubImage2D(int target,
                                         int32_t level,
                                         int32_t xOffset, int32_t yOffset,
                                         int32_t width, int32_t height,
                                         iBitmap *bitmap,
                                         int32_t planar)
    {
        bitmap->Map(eMemMap_ReadOnly);

        SmartPtr<iPixelFormat> pixelFormat = bitmap->GetPixelFormat();
        
        SmartPtr<iMemory> data = bitmap->GetData(planar);
        
        glPixelStorei(GL_UNPACK_ROW_LENGTH, bitmap->GetWidthOfPlanar(planar));
        
        glCompressedTexSubImage2D(ToGLTextureTarget(target),
                                  level,
                                  xOffset, yOffset,
                                  width, height,
                                  ToGLFormat(pixelFormat->GetName()),
                                  (GLsizei)data->Size(),
                                  data->Ptr());
        
        glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
        
        bitmap->Unmap();
    }
    
    virtual void CopyTexSubImage2D(int target,
                                   int32_t level,
                                   int32_t format,
                                   int32_t xOffset, int32_t yOffset,
                                   int32_t x, int32_t y,
                                   int32_t width, int32_t height)
    {
        glCopyTexSubImage2D(ToGLTextureTarget(target),
                            level,
                            xOffset, yOffset,
                            x, y,
                            width, height);
    }
    
    virtual void PixelStorei(int name, int32_t value)
    {
        glPixelStorei(ToGLTextureParamName(name), value);
    }
    
    virtual void CreateTexture(int32_t name)
    {
        mObjects[name] = new TextureOpenGL();
    }
};

class GraphicsBackend_OpenGL34 : public GraphicsBackend_OpenGL_Desktop
{
public:
    GraphicsBackend_OpenGL34()
    {
    }
    
    virtual ~GraphicsBackend_OpenGL34()
    {
        
    }
    
    //--------------------------------------------------------------------------
    
    virtual void CreateVertexArray(int32_t name)
    {
        AddObject(name, new VertexArrayOpenGL());
    }
    
    virtual void BindVertexArray(int32_t name)
    {
        if (name == 0)
        {
            glBindVertexArray(0);
            return;
        }
        
        ObjectMap::iterator it = mObjects.find(name);
        
        if (it != mObjects.end())
        {
            SmartPtr<VertexArrayOpenGL> obj = dynamic_cast<VertexArrayOpenGL*>(it->second.Ptr());
            
            while (glGetError() != GL_NO_ERROR);
            glBindVertexArray(obj->GetHandle());
            piAssert(glGetError() == GL_NO_ERROR, ;);
        }
        else
        {
            PILOGE(PI_GRAPHICS_TAG, "BindVertexArray: vertex array(%d) not found.", name);
        }
    }
    
    virtual bool Support(int op) const
    {
        switch (op)
        {
            case eGraphicsOp_BindVertexAttr:
                return false;
            default:
                return true;
        }
    }
};

class GraphicsBackend_OpenGL3 : public GraphicsBackend_OpenGL34
{
public:
    GraphicsBackend_OpenGL3()
    {
    }
    
    virtual ~GraphicsBackend_OpenGL3()
    {
    }
    
    virtual int GetType() const
    {
        return eGraphicsBackend_OpenGL3;
    }
};

class GraphicsBackend_OpenGL4 : public GraphicsBackend_OpenGL34
{
public:
    GraphicsBackend_OpenGL4()
    {
    }
    
    virtual ~GraphicsBackend_OpenGL4()
    {
    }
    
    virtual int GetType() const
    {
        return eGraphicsBackend_OpenGL4;
    }
};

iGraphicsBackend* CreateGraphicsBackend_OpenGL3()
{
    return new GraphicsBackend_OpenGL3();
}

iGraphicsBackend* CreateGraphicsBackend_OpenGL4()
{
    return new GraphicsBackend_OpenGL4();
}

NSPI_END()










































