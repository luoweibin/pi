/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.10.13   0.1     Create
 ******************************************************************************/
#ifndef PI_SRC_GRAPHICS_GRAPHICSVM_GRAPHICSUTILOPENGL_H
#define PI_SRC_GRAPHICS_GRAPHICSVM_GRAPHICSUTILOPENGL_H

NSPI_BEGIN()

static GLenum ToGLEqFunc(int value)
{
    switch (value)
    {
        case eEqFunc_Add:
            return GL_FUNC_ADD;
        case eEqFunc_Sub:
            return GL_FUNC_SUBTRACT;
        case eEqFunc_RevSub:
            return GL_FUNC_REVERSE_SUBTRACT;
        default:
            return -1;
    }
}

static GLenum ToGLBlendFunc(int value)
{
    switch (value)
    {
        case eBlendFunc_One:
            return GL_ONE;
        case eBlendFunc_Zero:
            return GL_ZERO;
        case eBlendFunc_DstAlpha:
            return GL_DST_ALPHA;
        case eBlendFunc_DstColor:
            return GL_DST_COLOR;
        case eBlendFunc_SrcAlpha:
            return GL_SRC_ALPHA;
        case eBlendFunc_SrcColor:
            return GL_SRC_COLOR;
        case eBlendFunc_ConstAlpha:
            return GL_CONSTANT_ALPHA;
        case eBlendFunc_ConstColor:
            return GL_CONSTANT_COLOR;
        case eBlendFunc_OneMinusDstAlpha:
            return GL_ONE_MINUS_DST_ALPHA;
        case eBlendFunc_OneMinusDstColor:
            return GL_ONE_MINUS_DST_COLOR;
        case eBlendFunc_OneMinusSrcAlpha:
            return GL_ONE_MINUS_SRC_ALPHA;
        case eBlendFunc_OneMinusSrcColor:
            return GL_ONE_MINUS_SRC_COLOR;
        case eBlendFunc_OneMinusConstAlpha:
            return GL_ONE_MINUS_CONSTANT_ALPHA;
        case eBlendFunc_OneMinusConstColor:
            return GL_ONE_MINUS_CONSTANT_COLOR;
        default:
            return -1;
    }
}

static inline GLenum ToGLFace(int face)
{
    switch (face) {
        case eFace_Front:
            return GL_FRONT;
        case eFace_Back:
            return GL_BACK;
        case eFace_Both:
        default:
            return GL_FRONT_AND_BACK;
    }
}

static inline GLenum ToGLType(int type)
{
    switch (type)
    {
        case eType_I8:
            return GL_BYTE;
        case eType_U8:
            return GL_UNSIGNED_BYTE;
        case eType_I16:
            return GL_SHORT;
        case eType_U16:
            return GL_UNSIGNED_SHORT;
        case eType_I32:
            return GL_INT;
        case eType_U32:
            return GL_UNSIGNED_INT;
        case eType_F32:
            return GL_FLOAT;
        default:
            return -1;
    }
}


static inline GLenum ToGLHint(int value)
{
    switch (value)
    {
        case eHint_Fastest:
            return GL_FASTEST;
        case eHint_Nicest:
            return GL_NICEST;
        case eHint_DontCare:
            return GL_DONT_CARE;
        default:
            return 0;
    }
}



static inline GLenum ToGLFunc(int value)
{
    switch (value) {
        case eFunc_Equal:
            return GL_EQUAL;
        case eFunc_Never:
            return GL_NEVER;
        case eFunc_Always:
            return GL_ALWAYS;
        case eFunc_Greater:
            return GL_GREATER;
        case eFunc_NotEqual:
            return GL_NOTEQUAL;
        case eFunc_LessAndEqual:
            return GL_LEQUAL;
        case eFunc_GreaterAndEqual:
            return GL_GEQUAL;
        case eFunc_Less:
            return GL_LESS;
        default:
            return -1;
    }
}



static inline GLenum ToGLStencilOp(int op)
{
    switch (op)
    {
        case eStencilOp_IncreaseWrap:
            return GL_INCR_WRAP;
        case eStencilOp_DecreaseWrap:
            return GL_DECR_WRAP;
        case eStencilOp_Increase:
            return GL_INCR;
        case eStencilOp_Decrease:
            return GL_DECR;
        case eStencilOp_Replace:
            return GL_REPLACE;
        case eStencilOp_Invert:
            return GL_INVERT;
        case eStencilOp_Zero:
            return GL_ZERO;
        case eStencilOp_Keep:
        default:
            return GL_KEEP;
    }
}


static inline GLenum ToGLBufferTarget(int target)
{
    switch (target)
    {
        case eGraphicsBuffer_Vertex:
            return GL_ARRAY_BUFFER;
        case eGraphicsBuffer_Index:
            return GL_ELEMENT_ARRAY_BUFFER;
        default:
            return -1;
    }
}

#endif


NSPI_END()














