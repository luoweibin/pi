/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.10.13   0.1     Create
 ******************************************************************************/
#include <pi/Graphics.h>
#include "GraphicsOp.h"
#include "GraphicsBackend.h"

#if defined(PI_IOS)
#   include <OpenGLES/ES3/gl.h>
#   include <OpenGLES/ES3/glext.h>
#elif defined(PI_ANDROID)
#   include <GLES3/gl3.h>
#	include <GLES3/gl3ext.h>
#endif

using namespace std;

#include "GraphicsUtils_OpenGL_ES3.h"

#include "GraphicsProgram_OpenGL.h"
#include "GraphicsBackend_OpenGL_ES.h"

NSPI_BEGIN()

class VertexArrayOpenGL_ES3 : public iRefObject
{
private:
    GLuint mHandle;
    
public:
    VertexArrayOpenGL_ES3()
    {
        glGenVertexArrays(1, &mHandle);
    }
    
    virtual ~VertexArrayOpenGL_ES3()
    {
        glDeleteVertexArrays(1, &mHandle);
        mHandle = -1;
    }
    
    GLuint GetHandle() const
    {
        return mHandle;
    }
};

class GraphicsBackend_OpenGL_ES3 : public GraphicsBackend_OpenGL_ES
{
public:
    GraphicsBackend_OpenGL_ES3()
    {
    }
    
    virtual ~GraphicsBackend_OpenGL_ES3()
    {
    }
    
    virtual int GetType() const
    {
        return eGraphicsBackend_OpenGL_ES3;
    }
    
    virtual bool Support(int op) const
    {
        switch (op)
        {
            case eGraphicsOp_BindVertexAttr:
                return false;
            default:
                return true;
        }
    }
    
    virtual void EnableFeatures(int32_t features)
    {
        GraphicsBackend_OpenGL_ES::EnableFeatures(features);
        
        if (piFlagIs(features, eGraphicsFeature_Texture3D))
        {
            glEnable(GL_TEXTURE_3D);
        }
    }
    
    virtual void DisableFeatures(int32_t features)
    {
        GraphicsBackend_OpenGL_ES::DisableFeatures(features);
        
        if (piFlagIs(features, eGraphicsFeature_Texture3D))
        {
            glDisable(GL_TEXTURE_3D);
        }
    }
    
    virtual void CreateVertexArray(int32_t name)
    {
        SmartPtr<iRefObject> object = new VertexArrayOpenGL_ES3();
        AddObject(name, object);
    }
    
    virtual void BindVertexArray(int32_t name)
    {
        if (name == 0)
        {
            glBindVertexArray(0);
            return;
        }
        
        ObjectMap::iterator it = mObjects.find(name);
        
        if (it != mObjects.end())
        {
            SmartPtr<VertexArrayOpenGL_ES3> obj = dynamic_cast<VertexArrayOpenGL_ES3*>(it->second.Ptr());
            
            while (glGetError() != GL_NO_ERROR);
            glBindVertexArray(obj->GetHandle());
            piAssert(glGetError() == GL_NO_ERROR, ;);
        }
        else
        {
            PILOGE(PI_GRAPHICS_TAG, "BindVertexArray: vertex array(%d) not found.", name);
        }
    }
    
    virtual void DrawElementsInstanced(int mode, int32_t count, int type, int32_t instanceCount)
    {
        glDrawElementsInstanced(ToGLDraw(mode), count, type, 0, instanceCount);
    }
    
    virtual void DrawArraysInstanced(int mode, int32_t first, int32_t count, int instanceCount)
    {
        glDrawArraysInstanced(ToGLDraw(mode), first, count, instanceCount);
    }
    
    virtual void TexImage2D(int target,
                            int32_t level,
                            int format,
                            iBitmap *bitmap,
                            int32_t planar)
    {        
        SmartPtr<iPixelFormat> pixelFormat = bitmap->GetPixelFormat();
        
        bitmap->Map(eMemMap_ReadOnly);
        
        SmartPtr<iMemory> data = bitmap->GetData(planar);
        
        glPixelStorei(GL_UNPACK_ALIGNMENT, bitmap->GetPixelAlignment(planar));
        
        glPixelStorei(GL_UNPACK_ROW_LENGTH, bitmap->GetWidthOfPlanar(planar));
        
        int32_t width = bitmap->GetWidthOfPlanar(planar);
        int32_t height = bitmap->GetHeightOfPlanar(planar);
        int type = pixelFormat->GetType(planar);
        int extFormat = pixelFormat->GetFormat(planar);
        
        glTexImage2D(ToGLTextureTarget(target),
                     level,
                     ToGLFormat(format),
                     width,
                     height,
                     0,
                     ToGLFormat(extFormat),
                     ToGLType(type),
                     !data.IsNull() ? data->Ptr() : nullptr);
        
        glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
        
        bitmap->Unmap();
    }
    
    virtual void CompressedTexImage2D(int target,
                                      int32_t level,
                                      int format,
                                      iBitmap *bitmap,
                                      int32_t planar)
    {
        SmartPtr<iPixelFormat> pixelFormat = bitmap->GetPixelFormat();
        
        bitmap->Map(eMemMap_ReadOnly);
        
        SmartPtr<iMemory> data = bitmap->GetData(planar);
        
        glPixelStorei(GL_UNPACK_ROW_LENGTH, bitmap->GetWidthOfPlanar(planar));
        
        int32_t width = bitmap->GetWidthOfPlanar(planar);
        int32_t height = bitmap->GetHeightOfPlanar(planar);
        
        glCompressedTexImage2D(ToGLTextureTarget(target),
                               level,
                               format,
                               width, height,
                               0,
                               (GLsizei)data->Size(),
                               data->Ptr());
        
        glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
        
        bitmap->Unmap();
    }
    
    
    virtual void CopyTexImage2D(int target,
                                int32_t level,
                                int32_t format,
                                int32_t x,
                                int32_t y,
                                int32_t width,
                                int32_t height)
    {
        glCopyTexImage2D(ToGLTextureTarget(target), level, ToGLFormat(format), x, y, width, height, 0);
    }
    
    virtual void TexParam(int32_t target, int32_t name, int32_t value)
    {
        int param = ToGLTextureParamName(name);
        if (param >= 0)
        {
            glTexParameteri(ToGLTextureTarget(target),
                            param,
                            ToGLTextureParamValue(value));
        }
    }
    
    virtual void TexSubImage2D(int target,
                               int32_t level,
                               int32_t xOffset, int32_t yOffset,
                               int32_t width, int32_t height,
                               iBitmap *bitmap,
                               int32_t planar)
    {
        SmartPtr<iPixelFormat> pixelFormat = bitmap->GetPixelFormat();
        
        bitmap->Map(eMemMap_ReadOnly);
        
        SmartPtr<iMemory> data = bitmap->GetData(planar);
        
        glPixelStorei(GL_UNPACK_ALIGNMENT, bitmap->GetPixelAlignment(planar));
        
        glPixelStorei(GL_UNPACK_ROW_LENGTH, bitmap->GetWidthOfPlanar(planar));
        
        glTexSubImage2D(ToGLTextureTarget(target),
                        level,
                        xOffset, yOffset,
                        width, height,
                        ToGLFormat(pixelFormat->GetFormat(planar)),
                        ToGLType(pixelFormat->GetType(planar)),
                        !data.IsNull() ? data->Ptr() : nullptr);
        
        glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
        
        bitmap->Unmap();
    }
    
    virtual void CompressedTexSubImage2D(int target,
                                         int32_t level,
                                         int32_t xOffset, int32_t yOffset,
                                         int32_t width, int32_t height,
                                         iBitmap *bitmap,
                                         int32_t planar)
    {
        SmartPtr<iPixelFormat> pixelFormat = bitmap->GetPixelFormat();
        
        bitmap->Map(eMemMap_ReadOnly);
        
        SmartPtr<iMemory> data = bitmap->GetData(planar);
        
        glPixelStorei(GL_UNPACK_ROW_LENGTH, bitmap->GetWidthOfPlanar(planar));
        
        glCompressedTexSubImage2D(ToGLTextureTarget(target),
                                  level,
                                  xOffset, yOffset,
                                  width, height,
                                  ToGLFormat(pixelFormat->GetName()),
                                  (GLsizei)data->Size(),
                                  data->Ptr());
        
        glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
        
        bitmap->Unmap();
    }
    
    virtual void CopyTexSubImage2D(int target,
                                   int32_t level,
                                   int32_t format,
                                   int32_t xOffset, int32_t yOffset,
                                   int32_t x, int32_t y,
                                   int32_t width, int32_t height)
    {
        glCopyTexSubImage2D(ToGLTextureTarget(target),
                            level,
                            xOffset, yOffset,
                            x, y,
                            width, height);
    }
};

iGraphicsBackend* CreateGraphicsBackend_OpenGL_ES3()
{
    return new GraphicsBackend_OpenGL_ES3();
}

NSPI_END()





















