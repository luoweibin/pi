/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2015.4.10   0.1     Create
 ******************************************************************************/
#ifndef PI_SRC_GRAPHICS_GRAPHICSVM_GRAPHICSPROGRAM_H
#define PI_SRC_GRAPHICS_GRAPHICSVM_GRAPHICSPROGRAM_H

NSPI_BEGIN()

class GraphicsProgramOpenGL : public iRefObject
{
private:
    GLuint mProgramHandle;
    GLuint mVertexShader;
    GLuint mFragmnetShader;
    
    std::map<std::string, GLint> mAttrVars;
    std::map<std::string, GLint> mUniformVars;
    
public:
    GraphicsProgramOpenGL()
    :mProgramHandle(-1), mVertexShader(-1), mFragmnetShader(-1)
    {
        mProgramHandle = glCreateProgram();
        mVertexShader = glCreateShader(GL_VERTEX_SHADER);
        mFragmnetShader = glCreateShader(GL_FRAGMENT_SHADER);
    }
    
    virtual ~GraphicsProgramOpenGL()
    {
        glDeleteShader(mVertexShader);
        mVertexShader = -1;
        
        glDeleteShader(mFragmnetShader);
        mFragmnetShader = -1;
        
        glDeleteProgram(mProgramHandle);
        mProgramHandle = -1;
    }
    
    virtual void Use()
    {
        glUseProgram(mProgramHandle);
    }
    
    virtual string ToString() const
    {
        return "Program";
    }
    
    virtual void Compile(const std::string& vertex, const std::string& fragment)
    {
        piCheck(CompileShader(mVertexShader, vertex), ;);
        piCheck(CompileShader(mFragmnetShader, fragment), ;);
        
        PILOGV(PI_GRAPHICS_TAG, "program compiled.");
    }
    
    virtual void Link()
    {
        glAttachShader(mProgramHandle, mVertexShader);
        glAttachShader(mProgramHandle, mFragmnetShader);
        glLinkProgram(mProgramHandle);
        
        GLint status = 0;
        glGetProgramiv(mProgramHandle, GL_LINK_STATUS, &status);
        if (status != GL_TRUE)
        {
            char b[1025];
            GLsizei infoSize = 0;
            glGetProgramInfoLog(mProgramHandle, 1024, &infoSize, b);
            b[infoSize] = '\0';
            PILOGE(PI_GRAPHICS_TAG, "%s", b);
            return;
        }
        
        PILOGV(PI_GRAPHICS_TAG, "program linked.");
    }
    
    virtual void BindVertexAttr(int32_t index, const std::string& name)
    {
        glBindAttribLocation(mProgramHandle, index, name.c_str());
    }
    
    virtual void Uniform1i(const string &name, int32_t v)
    {
        GLint var = GetUniformVar(name);
        glUniform1i(var, v);
    }
    
    virtual void Uniform1iv(const string &name, iI32Array* values)
    {
        GLint var = GetUniformVar(name);
        glUniform1iv(var, values->GetCount(), values->GetData());
    }
    
    virtual void Uniform2i(const string &name, int32_t v1, int32_t v2)
    {
        GLint var = GetUniformVar(name);
        glUniform2i(var, v1, v2);
    }
    
    virtual void Uniform2iv(const string &name,
                            iI32Array* values)
    {
        GLint var = GetUniformVar(name);
        glUniform2iv(var, values->GetCount() / 2, values->GetData());
    }
    
    virtual void Uniform3i(const string &name,
                           int32_t v1,
                           int32_t v2,
                           int32_t v3)
    {
        GLint var = GetUniformVar(name);
        glUniform3i(var, v1, v2, v3);
    }
    
    virtual void Uniform3iv(const string &name,
                            iI32Array* values)
    {
        GLint var = GetUniformVar(name);
        glUniform3iv(var, values->GetCount() / 3, values->GetData());
    }
    
    virtual void Uniform4i(const string &name,
                           int32_t v1,
                           int32_t v2,
                           int32_t v3,
                           int32_t v4)
    {
        GLint var = GetUniformVar(name);
        glUniform4i(var, v1, v2, v3, v4);
    }
    
    virtual void Uniform4iv(const string &name,
                            iI32Array* values)
    {
        GLint var = GetUniformVar(name);
        glUniform4iv(var, values->GetCount() / 4, values->GetData());
    }
    
    virtual void Uniform1f(const string &name, float v)
    {
        glUniform1f(GetUniformVar(name), v);
    }
    
    virtual void Uniform1fv(const string &name, iF32Array* values)
    {
        glUniform1fv(GetUniformVar(name), values->GetCount(), values->GetData());
    }
    
    virtual void Uniform2f(const string &name, float v1, float v2)
    {
        glUniform2f(GetUniformVar(name), v1, v2);
    }
    
    virtual void Uniform2fv(const string &name,
                            iF32Array* values)
    {
        glUniform2fv(GetUniformVar(name), values->GetCount() / 2, values->GetData());
    }
    
    virtual void Uniform3f(const string &name,
                           float v1,
                           float v2,
                           float v3)
    {
        glUniform3f(GetUniformVar(name), v1, v2, v3);
    }
    
    virtual void Uniform3fv(const string &name,
                            iF32Array* values)
    {
        glUniform3fv(GetUniformVar(name), values->GetCount() / 3, values->GetData());
    }
    
    virtual void UniformVec3(const std::string &name, const vec3 &value)
    {
        glUniform3f(GetUniformVar(name), value[0], value[1], value[2]);
    }
    
    virtual void UniformVec3v(const std::string &name, iVec3Array* values)
    {
        glUniform3fv(GetUniformVar(name), values->GetCount(), (float*)values->GetData());
    }
    
    virtual void Uniform4f(const string &name,
                           float v1,
                           float v2,
                           float v3,
                           float v4)
    {
        glUniform4f(GetUniformVar(name), v1, v2, v3, v4);
    }
    
    virtual void Uniform4fv(const string &name, iF32Array* values)
    {
        glUniform4fv(GetUniformVar(name), values->GetCount() / 4, values->GetData());
    }
    
    virtual void UniformVec4(const std::string &name, const vec4 &value)
    {
        glUniform4f(GetUniformVar(name), value[0], value[1], value[2], value[3]);
    }
    
    virtual void UniformRect(const std::string &name, const rect &value)
    {
        glUniform4f(GetUniformVar(name), value.x, value.y, value.width, value.height);
    }
    
    virtual void UniformQuat(const std::string &name, const quat &value)
    {
        glUniform4f(GetUniformVar(name), value.x, value.y, value.z, value.w);
    }
    
    virtual void UniformMat4f(const string &name, const mat4& value)
    {
        glUniformMatrix4fv(GetUniformVar(name), 1, GL_FALSE, piglm::value_ptr(value));
    }
    
    virtual void UniformMat4fv(const string& name, iMat4Array* values)
    {
        glUniformMatrix4fv(GetUniformVar(name), values->GetCount(), GL_FALSE, (GLfloat*)values->GetData());
    }
    
private:
    GLint GetAttrVar(const string& name)
    {
        GLint var = -1;
        
        map<string, GLint>::iterator it = mAttrVars.find(name);
        if (it != mAttrVars.end())
        {
            var = it->second;
        }
        else
        {
            var = glGetAttribLocation(mProgramHandle, name.c_str());
            if (var != -1)
            {
                mAttrVars[name] = var;
            }
            else
            {
                PILOGW(PI_GRAPHICS_TAG, "vertex attribute variable(%s) not found.", name.c_str());
            }
        }
        
        return var;
    }
    
    GLint GetUniformVar(const string& name)
    {
        GLint var = -1;
        
        map<string, GLint>::iterator it = mUniformVars.find(name);
        if (it != mUniformVars.end())
        {
            var = it->second;
        }
        else
        {
            var = glGetUniformLocation(mProgramHandle, name.c_str());
            if (var != -1)
            {
                mUniformVars[name] = var;
            }
            else
            {
                PILOGD(PI_GRAPHICS_TAG, "uniform variable(%s) not found.", name.c_str());
            }
        }
        
        return var;
    }
    
    bool CompileShader(GLuint handle, const std::string& source)
    {
        piAssert(!source.empty(), false);
        
#if PI_DEBUG
        PILOGD(PI_GRAPHICS_TAG, "\n%s", source.c_str());
#endif
        
        const char* src = source.c_str();
        int len = (int)source.size();
        glShaderSource(handle, 1, &src, &len);
        glCompileShader(handle);
        GLint status = 0;
        glGetShaderiv(handle, GL_COMPILE_STATUS, &status);
        if (status == GL_TRUE)
        {
            return true;
        }
        else
        {
            char b[1025];
            GLsizei infoSize = 0;
            glGetShaderInfoLog(handle, 1024, &infoSize, b);
            b[infoSize] = '\0';
#if PI_DEBUG
            PILOGE(PI_GRAPHICS_TAG, "%s", b);
#endif
            return false;
        }
    }
    
    static void ClearGLError()
    {
        while (glGetError() != GL_NO_ERROR);
    }
    
    static void CheckGLError()
    {
        GLenum err = glGetError();
        piAssert(err == GL_NO_ERROR, ;);
    }
};

NSPI_END()

#endif































