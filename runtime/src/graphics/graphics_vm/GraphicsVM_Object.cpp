/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.10.13   0.1     Create
 ******************************************************************************/
#include "GraphicsVM_Impl.h"

NSPI_BEGIN()

void GraphicsVM::EnableVertexAttr(int32_t name)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_EnableVertexAttr, name);
}

void GraphicsVM::BindVertexAttr(int32_t program, int32_t index, const std::string& name)
{
    piCheck(mBackend->Support(eGraphicsOp_BindVertexAttr), ;);
    
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_BindVertexAttr, program, index, name);
}

void GraphicsVM::DisableVertexAttr(int32_t name)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_DisableVertexAttr, name);
}

void GraphicsVM::VertexAttr(int32_t name,
                            int32_t size,
                            int32_t type,
                            int32_t stride,
                            int32_t offset)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_VertexAttr, name, size, type, stride, offset);
}



int32_t GraphicsVM::CreateVertexArray()
{
    piCheck(mBackend->Support(eGraphicsOp_CreateVertexArray), 0);
    
    lock_guard<mutex> lock(mMutex);
    int32_t name = AllocName();
    PushByteCode(eGraphicsOp_CreateVertexArray, name);
    return name;
}

void GraphicsVM::BindVertexArray(int32_t name)
{
    piAssert(mBackend->Support(eGraphicsOp_BindVertexArray), ;);
    
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_BindVertexArray, name);
}


int32_t GraphicsVM::CreateBuffer()
{
    lock_guard<mutex> lock(mMutex);
    int32_t name = AllocName();
    PushByteCode(eGraphicsOp_CreateBuffer, name);
    return name;
}

void GraphicsVM::BindBuffer(int target, int32_t name)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_BindBuffer, target, name);
}

void GraphicsVM::BufferData(int target, int32_t name, int64_t size, iMemory* data)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_BufferData, target, name, size, data);
}

void GraphicsVM::BufferSubData(int32_t target, int64_t offset, int64_t size, iMemory *data)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_BufferSubData, target, offset, size, data);
}

/**
 * =================================================================================================
 * Dispatch
 */

void GraphicsVM::CallEnableVertexAttr(iGraphicsByteCode* byteCode)
{
    int32_t name = byteCode->GetArg(0);
    mBackend->EnableVertexAttr(name);
    
    PILOGV(PI_GRAPHICS_TAG, "EnableVertexAttr(name:%d)", name);
}

void GraphicsVM::CallBindVertexAttr(iGraphicsByteCode* byteCode)
{
    int32_t program = byteCode->GetArg(0);
    int32_t index = byteCode->GetArg(1);
    string name = byteCode->GetArg(2);
    mBackend->BindVertexAttr(program, index, name);

    PILOGV(PI_GRAPHICS_TAG, "BindVertexAttr(program:%d, index:%d, name:%s)", program, index, name.c_str());
}

void GraphicsVM::CallDisableVertexAttr(iGraphicsByteCode* byteCode)
{
    int32_t name = byteCode->GetArg(0);
    mBackend->DisableVertexAttr(name);
    
    PILOGV(PI_GRAPHICS_TAG, "DisableVertexAttr(name:%d)", name);
}

void GraphicsVM::CallVertexAttr(iGraphicsByteCode* byteCode)
{
    int32_t name = byteCode->GetArg(0);
    int32_t size = byteCode->GetArg(1);
    int type = byteCode->GetArg(2);
    int32_t stride = byteCode->GetArg(3);
    int32_t offset = byteCode->GetArg(4);
    mBackend->VertexAttr(name, size, type, stride, offset);

    PILOGV(PI_GRAPHICS_TAG,
           "SetVertexAttr(name:%d, size:%d, type:%s, stride:%d, offset:%d)",
           name,
           size,
           piTypeName(type).c_str(),
           stride,
           offset);
}

void GraphicsVM::CallCreateBuffer(iGraphicsByteCode* byteCode)
{
    int32_t name = byteCode->GetArg(0);
    mBackend->CreateBuffer(name);

    PILOGV(PI_GRAPHICS_TAG, "CreateBuffer(name:%d)", name);
}

void GraphicsVM::CallBindBuffer(iGraphicsByteCode* byteCode)
{
    int32_t target = byteCode->GetArg(0);
    int32_t name = byteCode->GetArg(1);
    mBackend->BindBuffer(target, name);

    PILOGV(PI_GRAPHICS_TAG,
           "BindBuffer(target:%s, name:%d)",
           piGraphicsBufferName(target).c_str(),
           name);
}

void GraphicsVM::CallBufferData(iGraphicsByteCode* byteCode)
{
    int target = byteCode->GetArg(0);
    int32_t name = byteCode->GetArg(1);
    int64_t size = byteCode->GetArg(2);
    SmartPtr<iMemory> data = piQueryVarObject<iMemory>(byteCode->GetArg(3));
    mBackend->BufferData(target, name, size, data);
    
    PILOGV(PI_GRAPHICS_TAG,
           "BufferData(target:%s, name:%d, size:%lld, data:%s)",
           piGraphicsBufferName(target).c_str(),
           name,
           size,
           piToString(data).c_str());
}

void GraphicsVM::CallBufferSubData(iGraphicsByteCode *byteCode)
{
    int target = byteCode->GetArg(0);
    int64_t offset = byteCode->GetArg(1);
    int64_t size = byteCode->GetArg(2);
    SmartPtr<iMemory> data = piQueryVarObject<iMemory>(byteCode->GetArg(3));
    mBackend->BufferSubData(target, offset, size, data);
    
    PILOGV(PI_GRAPHICS_TAG,
           "BufferSubData(target:%s, offset:%lld, size:%lld, data:%s)",
           piGraphicsBufferName(target).c_str(),
           offset,
           size,
           piToString(data).c_str());
}

void GraphicsVM::CallCreateVertexArray(iGraphicsByteCode* byteCode)
{
    int32_t name = byteCode->GetArg(0);
    mBackend->CreateVertexArray(name);

    PILOGV(PI_GRAPHICS_TAG, "CreateVertexArray(name:%d)", name);
}

void GraphicsVM::CallBindVertexArray(iGraphicsByteCode* byteCode)
{
    int32_t name = byteCode->GetArg(0);
    mBackend->BindVertexArray(name);

    PILOGV(PI_GRAPHICS_TAG,
           "BindVertexArray(name:%d)",
           name);
}


NSPI_END()





























