/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.10.13   0.1     Create
 ******************************************************************************/
#include "GraphicsVM_Impl.h"

NSPI_BEGIN()

void GraphicsVM::CullFace(int face)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_CullFace, face);
}

void GraphicsVM::ClearColor(const vec4 &color)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_ClearColor, color);
}

void GraphicsVM::ClearDepth(float value)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_ClearDepth, value);
}

void GraphicsVM::Winding(int32_t winding)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Winding, winding);
}

void GraphicsVM::EnableFeatures(int features)
{
    lock_guard<mutex> lock(mMutex);
    piFlagOn(mFeatures, features);
    PushByteCode(eGraphicsOp_EnableFeatures, features);
}

void GraphicsVM::DisableFeatures(int features)
{
    lock_guard<mutex> lock(mMutex);
    piFlagOff(mFeatures, features);
    PushByteCode(eGraphicsOp_DisableFeatures, features);
}

bool GraphicsVM::IsFeatureEnabled(int32_t feature) const
{
    lock_guard<mutex> lock(mMutex);
    return piFlagIs(mFeatures, feature);
}


void GraphicsVM::PolygonOffset(float factor, float units)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_PolygonOffset, factor, units);
}

void GraphicsVM::PolygonMode(int face, int mode)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_PolygonMode, face, mode);
}

void GraphicsVM::DepthFunction(int func)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_DepthFunction, func);
}

void GraphicsVM::DepthMask(bool flag)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_DepthMask, flag);
}

void GraphicsVM::ColorMask(bool red, bool green, bool blue, bool alpha)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_ColorMask, red, green, blue, alpha);
}

void GraphicsVM::Hint(int target, int value)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Hint, target, value);
}

void GraphicsVM::StencilFunc(int func, int32_t ref, int64_t mask)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_StencilFunc, func, ref, mask);
}

void GraphicsVM::ClearStencil(int value)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_ClearStencil, value);
}

void GraphicsVM::StencilOp(int sfail, int dbfail, int dbpass)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_StencilOp, sfail, dbfail, dbpass);
}

void GraphicsVM::StencilOpSeparate(int face, int sfail, int dbfail, int dbpass)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_StencilOpSeparate, face, sfail, dbfail, dbpass);
}

void GraphicsVM::StencilMask(int64_t mask)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_StencilMask, mask);
}

void GraphicsVM::LineWidth(float width)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_LineWidth, width);
}

void GraphicsVM::BlendColor(const vec4 &color)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_BlendColor, color);
}

void GraphicsVM::BlendEquation(int mode)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_BlendEquation, mode);
}

void GraphicsVM::BlendFunc(int sfactor, int dfactor)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_BlendFunc, sfactor, dfactor);
}

void GraphicsVM::BlendFuncSeparate(int32_t index, int sfactor, int dfactor)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_BlendFuncSeparate, index, sfactor, dfactor);
}


/**
 * =================================================================================================
 * Dispatch
 */

void GraphicsVM::CallBlendColor(iGraphicsByteCode *byteCode)
{
    vec4 color = byteCode->GetArg(0);
    mBackend->BlendColor(color);

    PILOGV(PI_GRAPHICS_TAG, "BlendColor(%s)", piToString(color).c_str());
}

void GraphicsVM::CallBlendEquation(iGraphicsByteCode *byteCode)
{
    int mode = byteCode->GetArg(0);
    mBackend->BlendEquation(mode);
    
    PILOGV(PI_GRAPHICS_TAG, "BlendEquation(%s)", piEqFuncName(mode).c_str());;
}

void GraphicsVM::CallBlendFunc(iGraphicsByteCode *byteCode)
{
    int sfactor = byteCode->GetArg(0);
    int dfactor = byteCode->GetArg(1);
    mBackend->BlendFunc(sfactor, dfactor);

    PILOGV(PI_GRAPHICS_TAG,
           "BlendFunc(%s, %s)",
           piBlendFuncName(sfactor).c_str(),
           piBlendFuncName(dfactor).c_str());
}

void GraphicsVM::CallBlendFuncSeparate(iGraphicsByteCode *byteCode)
{
    int32_t index = byteCode->GetArg(0);
    int sfactor = byteCode->GetArg(1);
    int dfactor = byteCode->GetArg(2);
    mBackend->BlendFuncSeparate(index, sfactor, dfactor);

    PILOGV(PI_GRAPHICS_TAG,
           "BlendFuncSeparate(%d, %s, %s)",
           index,
           piBlendFuncName(sfactor).c_str(),
           piBlendFuncName(dfactor).c_str());
}

void GraphicsVM::CallHint(iGraphicsByteCode* byteCode)
{
    int target = byteCode->GetArg(0);
    int value = byteCode->GetArg(1);
    mBackend->Hint(target, value);
    
    PILOGV(PI_GRAPHICS_TAG,
           "Hint(%s, %s)",
           piHintTargetName(target).c_str(),
           piHintName(value).c_str());
}

void GraphicsVM::CallStencilMask(iGraphicsByteCode* byteCode)
{
    int64_t mask = byteCode->GetArg(0);
    mBackend->StencilMask(mask);
    
    PILOGV(PI_GRAPHICS_TAG, "StencilMask(mask:%X)", mask);
}

void GraphicsVM::CallLineWidth(iGraphicsByteCode* byteCode)
{
    float width = byteCode->GetArg(0);
    mBackend->LineWidth(width);
    
    PILOGV(PI_GRAPHICS_TAG, "LineWidth(width:%0.2f)", width);
}

void GraphicsVM::CallStencilOp(iGraphicsByteCode* byteCode)
{
    int sfail = byteCode->GetArg(0);
    int dpfail = byteCode->GetArg(1);
    int dppass = byteCode->GetArg(2);
    mBackend->StencilOp(sfail, dpfail, dppass);
    
    PILOGV(PI_GRAPHICS_TAG,
           "StencilOp(sfail:%s, dpfail:%s, dppass:%s)",
           piStencilOpName(sfail).c_str(),
           piStencilOpName(dpfail).c_str(),
           piStencilOpName(dppass).c_str());
}

void GraphicsVM::CallStencilOpSeparate(iGraphicsByteCode* byteCode)
{
    int face = byteCode->GetArg(0);
    int sfail = byteCode->GetArg(1);
    int dpfail = byteCode->GetArg(2);
    int dppass = byteCode->GetArg(3);
    mBackend->StencilOpSeparate(face, sfail, dpfail, dppass);
    
    PILOGV(PI_GRAPHICS_TAG,
           "StencilOpSeparate(face:%s, sfail:%s, dpfail:%s, dppass:%s)",
           piFaceName(face).c_str(),
           piStencilOpName(sfail).c_str(),
           piStencilOpName(dpfail).c_str(),
           piStencilOpName(dppass).c_str());
}

void GraphicsVM::CallStencilFunc(iGraphicsByteCode* byteCode)
{
    int func = byteCode->GetArg(0);
    int32_t ref = byteCode->GetArg(1);
    int64_t mask = byteCode->GetArg(2);
    mBackend->StencilFunc(func, ref, mask);
    
    PILOGV(PI_GRAPHICS_TAG,
           "StencilFunc(func:%s, ref:%d, mask:%ld)",
           piFuncName(func).c_str(),
           ref,
           mask);
}

void GraphicsVM::CallClearStencil(iGraphicsByteCode* byteCode)
{
    int value = byteCode->GetArg(0);
    mBackend->ClearStencil(value);
    
    PILOGV(PI_GRAPHICS_TAG, "ClearStencil(%d)", value);
}

void GraphicsVM::CallClearDepth(iGraphicsByteCode* byteCode)
{
    float value = byteCode->GetArg(0);
    mBackend->ClearDepth(value);
    
    PILOGV(PI_GRAPHICS_TAG, "ClearDepth(%0.2f)", value);
}

void GraphicsVM::CallDepthMask(iGraphicsByteCode* byteCode)
{
    bool flag = byteCode->GetArg(0);
    mBackend->DepthMask(flag);
    
    PILOGV(PI_GRAPHICS_TAG, "DepthMask(%d)", flag);
}

void GraphicsVM::CallColorMask(iGraphicsByteCode* byteCode)
{
    bool red = byteCode->GetArg(0);
    bool green = byteCode->GetArg(1);
    bool blue = byteCode->GetArg(2);
    bool alpha = byteCode->GetArg(3);
    mBackend->ColorMask(red, green, blue, alpha);
    
    PILOGV(PI_GRAPHICS_TAG, "ColorMask(%d, %d, %d, %d)", red, green, blue, alpha);
}

void GraphicsVM::CallCullFace(iGraphicsByteCode* byteCode)
{
    int face = byteCode->GetArg(0);
    mBackend->CullFace(face);
    
    PILOGV(PI_GRAPHICS_TAG, "CullFace(mode:%s)", piFaceName(face).c_str());
}

void GraphicsVM::CallClearColor(iGraphicsByteCode* byteCode)
{
    vec4 color = byteCode->GetArg(0);
    mBackend->ClearColor(color);
    
    PILOGV(PI_GRAPHICS_TAG, "ClearColor(color:%s)", piToString(color).c_str());
}

void GraphicsVM::CallWinding(iGraphicsByteCode* byteCode)
{
    int winding = byteCode->GetArg(0);
    mBackend->Winding(winding);
    
    PILOGV(PI_GRAPHICS_TAG, "Winding(mode:%s)", piWindingName(winding).c_str());
}

void GraphicsVM::CallEnableFeatures(iGraphicsByteCode* byteCode)
{
    int features = byteCode->GetArg(0);
    mBackend->EnableFeatures(features);
    
    PILOGV(PI_GRAPHICS_TAG, "EnableFeatures(features:%s)", piGraphicsFeatureNames(features).c_str());
}

void GraphicsVM::CallDisableFeatures(iGraphicsByteCode* byteCode)
{
    int features = byteCode->GetArg(0);
    mBackend->DisableFeatures(features);
    
    PILOGV(PI_GRAPHICS_TAG, "DisableFeature(feature:%s)", piGraphicsFeatureNames(features).c_str());
}

void GraphicsVM::CallPolygonOffset(iGraphicsByteCode* byteCode)
{
    float factor = byteCode->GetArg(0);
    float units = byteCode->GetArg(1);
    mBackend->PolygonOffset(factor, units);
    
    PILOGV(PI_GRAPHICS_TAG, "PolygonOffset(%0.2f, %0.2f)", factor, units);
}

void GraphicsVM::CallPolygonMode(iGraphicsByteCode* byteCode)
{
    int face = byteCode->GetArg(0);
    int mode = byteCode->GetArg(1);
    mBackend->PolygonMode(face, mode);
    
    PILOGV(PI_GRAPHICS_TAG,
           "PolygonMode(%s, %s)",
           piFaceName(face).c_str(),
           piPolygonModeName(mode).c_str());
}

void GraphicsVM::CallDepthFunction(iGraphicsByteCode *byteCode)
{
    int func = byteCode->GetArg(0);
    mBackend->DepthFunction(func);
    
    PILOGV(PI_GRAPHICS_TAG, "DepthFunction(%s)", piFuncName(func).c_str());
}



NSPI_END()



























