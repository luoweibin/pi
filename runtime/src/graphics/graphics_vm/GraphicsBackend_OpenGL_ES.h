/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.10.13   0.1     Create
 ******************************************************************************/
#include <pi/Graphics.h>
#include "GraphicsOp.h"
#include "GraphicsBackend.h"

using namespace std;

#include "GraphicsUtils_OpenGL_ES.h"
#include "GraphicsProgram_OpenGL.h"
#include "GraphicsBackend_OpenGL.h"

NSPI_BEGIN()

class GraphicsBackend_OpenGL_ES : public GraphicsBackend_OpenGL
{
public:
    GraphicsBackend_OpenGL_ES()
    {
    }
    
    virtual ~GraphicsBackend_OpenGL_ES()
    {
    }
    
    virtual void EnableFeatures(int features)
    {
        GraphicsBackend_OpenGL::EnableFeatures(features);
        
        if (piFlagIs(features, eGraphicsFeature_MultiSample))
        {
            PILOGE(PI_GRAPHICS_TAG,
                   "%s not supported in OpenGL ES2.",
                   piGraphicsFeatureNames(eGraphicsFeature_MultiSample).c_str());
        }
        
        if (piFlagIs(features, eGraphicsFeature_PolygonOffset_Point))
        {
            PILOGE(PI_GRAPHICS_TAG,
                   "%s not supported in OpenGL ES2.",
                   piGraphicsFeatureNames(eGraphicsFeature_PolygonOffset_Point).c_str());
        }
        
        if (piFlagIs(features, eGraphicsFeature_PolygonOffset_Line))
        {
            PILOGE(PI_GRAPHICS_TAG,
                   "%s not supported in OpenGL ES2.",
                   piGraphicsFeatureNames(eGraphicsFeature_PolygonOffset_Line).c_str());
        }
        
        if (piFlagIs(features, eGraphicsFeature_LineSmooth))
        {
            PILOGE(PI_GRAPHICS_TAG,
                   "%s not supported in OpenGL ES2.",
                   piGraphicsFeatureNames(eGraphicsFeature_LineSmooth).c_str());
        }
        
        if (piFlagIs(features, eGraphicsFeature_Texture1D))
        {
            PILOGE(PI_GRAPHICS_TAG,
                   "%s not supported in OpenGL ES2.",
                   piGraphicsFeatureNames(eGraphicsFeature_Texture1D).c_str());
        }
    }
    
    virtual void DisableFeatures(int features)
    {
        GraphicsBackend_OpenGL::DisableFeatures(features);
        
        if (piFlagIs(features, eGraphicsFeature_MultiSample))
        {
            PILOGE(PI_GRAPHICS_TAG,
                   "%s not supported in OpenGL ES2.",
                   piGraphicsFeatureNames(eGraphicsFeature_MultiSample).c_str());
        }
        
        if (piFlagIs(features, eGraphicsFeature_PolygonOffset_Point))
        {
            PILOGE(PI_GRAPHICS_TAG,
                   "%s not supported in OpenGL ES2.",
                   piGraphicsFeatureNames(eGraphicsFeature_PolygonOffset_Point).c_str());
        }
        
        if (piFlagIs(features, eGraphicsFeature_PolygonOffset_Line))
        {
            PILOGE(PI_GRAPHICS_TAG,
                   "%s not supported in OpenGL ES2.",
                   piGraphicsFeatureNames(eGraphicsFeature_PolygonOffset_Line).c_str());
        }
        
        if (piFlagIs(features, eGraphicsFeature_LineSmooth))
        {
            PILOGE(PI_GRAPHICS_TAG,
                   "%s not supported in OpenGL ES2.",
                   piGraphicsFeatureNames(eGraphicsFeature_LineSmooth).c_str());
        }
        
        if (piFlagIs(features, eGraphicsFeature_Texture1D))
        {
            PILOGE(PI_GRAPHICS_TAG,
                   "%s not supported in OpenGL ES2.",
                   piGraphicsFeatureNames(eGraphicsFeature_Texture1D).c_str());
        }
    }
    
    virtual void BlendFuncSeparate(int32_t index, int sfactor, int dfactor)
    {
        PILOGE(PI_GRAPHICS_TAG, "BlendFuncSeparate not supported in OpenGL ES.");
    }
    
    virtual void ClearDepth(float value)
    {
        glClearDepthf(value);
    }
    
    virtual void PolygonMode(int face, int mode)
    {
        PILOGE(PI_GRAPHICS_TAG, "SetPolygonMode not supported in OpenGL ES.");
    }
};

NSPI_END()
























