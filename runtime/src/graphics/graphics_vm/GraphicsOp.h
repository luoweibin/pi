/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2016.9.16   0.1     Create
 ******************************************************************************/
#ifndef PI_SRC_GRAPHICS_GRAPHICSVM_GRAPHICSOP_H
#define PI_SRC_GRAPHICS_GRAPHICSVM_GRAPHICSOP_H

NSPI_BEGIN()

enum eGraphicsOp
{
    eGraphicsOp_DumpDebugInfo,
    
    eGraphicsOp_CreateNativeGraphicsObject,
    
    eGraphicsOp_PushGroupMarker,
    
    eGraphicsOp_PopGroupMarker,
    
    eGraphicsOp_InsertEventMarker,
    
    eGraphicsOp_Retain,
    
    eGraphicsOp_Release,
    
    eGraphicsOp_BlendColor,
    
    eGraphicsOp_BlendEquation,
    
    /**
     * 设置混合模式
     * \param int32_t blend mode
     */
    eGraphicsOp_BlendFunc,
    
    eGraphicsOp_BlendFuncSeparate,
    
    /**
     * 设置viewport。
     * \param int32_t x
     * \param int32_t y
     * \param int32_t width
     * \param int32_t height
     */
    eGraphicsOp_Viewport,
    
    /**
     * 设置clear color。
     * \param vec4 color
     */
    eGraphicsOp_ClearColor,
    
    /**
     * 设置clear depth
     * \param float value
     */
    eGraphicsOp_ClearDepth,
    
    /**
     * 设置depth mask
     * \param bool flag
     */
    eGraphicsOp_DepthMask,
    
    /**
     * 设置color mask
     * \param bool red
     * \param bool green
     * \param bool blue
     * \param bool alpha
     */
    eGraphicsOp_ColorMask,
    
    /**
     * 清楚缓冲区
     * \param int32_t fields
     */
    eGraphicsOp_Clear,
    
    /**
     * 启用特性
     * \param int32_t features
     */
    eGraphicsOp_EnableFeatures,
    
    /**
     * 禁用特性
     * \param int32_t features
     */
    eGraphicsOp_DisableFeatures,
    
    /**
     * 设置前向面方向
     * \param int32_t eWinding_CCW|eWinding_CW
     */
    eGraphicsOp_Winding,
    
    /**
     * \param int32_t eFace_Front|eFrace_Back
     */
    eGraphicsOp_CullFace,
    
    eGraphicsOp_DrawArrays,
    
    eGraphicsOp_DrawArraysInstanced,
    
    eGraphicsOp_DrawElements,
    
    eGraphicsOp_DrawElementsOffset,
    
    eGraphicsOp_DrawElementsInstanced,
    
    eGraphicsOp_Flush,
    
    //----------------------------------------------------------------------
    
    /**
     * \param float factor
     * \param float units
     */
    eGraphicsOp_PolygonOffset,
    
    /**
     * \param int face
     * \param int mode
     */
    eGraphicsOp_PolygonMode,
    
    /**
     * \param int func
     */
    eGraphicsOp_DepthFunction,
    
    /**
     * \param int func
     */
    eGraphicsOp_StencilFunc,
    
    /**
     * \param int sfail
     * \param int dpfail
     * \param int dppass
     */
    eGraphicsOp_StencilOp,
    
    eGraphicsOp_StencilOpSeparate,
    
    /**
     * \param float value
     */
    eGraphicsOp_ClearStencil,
    
    /**
     * \param int64_t mask
     */
    eGraphicsOp_StencilMask,
    
    /**
     * \param float pixel
     */
    eGraphicsOp_LineWidth,
    
    /**
     * \param int target
     * \param int value
     */
    eGraphicsOp_Hint,
    
    //----------------------------------------------------------------------
    
    
    eGraphicsOp_CreateProgram,
    
    eGraphicsOp_UseProgram,
    
    eGraphicsOp_CompileProgram,
    
    eGraphicsOp_LinkProgram,
    
    eGraphicsOp_EnableVertexAttr,
    
    eGraphicsOp_DisableVertexAttr,
    
    eGraphicsOp_BindVertexAttr,
    
    /**
     * 设置顶点属性
     * \param int32_t 属性名称，Shader中的location名
     * \param int32_t 大小
     * \param int32_t 属性类型
     * \param int32_t 顶点类型大小
     * \param int32_t 偏移量
     */
    eGraphicsOp_VertexAttr,
    
    eGraphicsOp_Uniform1i,
    
    eGraphicsOp_Uniform1iv,
    
    eGraphicsOp_Uniform2i,
    
    eGraphicsOp_Uniform2iv,
    
    eGraphicsOp_Uniform3i,
    
    eGraphicsOp_Uniform3iv,
    
    eGraphicsOp_Uniform4i,
    
    eGraphicsOp_Uniform4iv,
    
    eGraphicsOp_Uniform1f,
    
    eGraphicsOp_Uniform1fv,
    
    eGraphicsOp_Uniform2f,
    
    eGraphicsOp_Uniform2fv,
    
    eGraphicsOp_Uniform3f,
    
    eGraphicsOp_Uniform3fv,
    
    eGraphicsOp_Uniform4f,
    
    eGraphicsOp_Uniform4fv,
    
    eGraphicsOp_UniformVec3,
    
    eGraphicsOp_UniformVec3v,
    
    eGraphicsOp_UniformVec4,
    
    eGraphicsOp_UniformVec4v,
    
    eGraphicsOp_UniformMat4f,
    
    eGraphicsOp_UniformMat4fv,
    
    eGraphicsOp_UniformRect,
    
    eGraphicsOp_UniformQuat,
    
    //----------------------------------------------------------------------
    
    eGraphicsOp_CreateVertexArray,
    
    eGraphicsOp_BindVertexArray,
    
    //----------------------------------------------------------------------
    
    eGraphicsOp_CreateBuffer,
    
    eGraphicsOp_BindBuffer,
    
    eGraphicsOp_BufferData,
    
    eGraphicsOp_BufferSubData,
    
    //----------------------------------------------------------------------
    
    eGraphicsOp_GenerateMipmap,
    
    eGraphicsOp_ActiveTexture,
    
    eGraphicsOp_BindTexture,
    
    eGraphicsOp_TexParam,
    
    eGraphicsOp_CreateTexture,
        
    eGraphicsOp_TexImage2D,
    
    eGraphicsOp_CompressedTexImage2D,
    
    eGraphicsOp_CopyTexImage2D,
    
    eGraphicsOp_TexImage3D,
    
    eGraphicsOp_TexSubImage2D,
    
    eGraphicsOp_CompressedTexSubImage2D,
    
    eGraphicsOp_CopyTexSubImage2D,
    
    eGraphicsOp_PixelStorei,
    
    //----------------------------------------------------------------------
    
    
    eGraphicsOp_CreateFramebuffer,
    
    eGraphicsOp_BindFramebuffer,
    
    eGraphicsOp_FramebufferTexture2D,
    
    eGraphicsOp_FramebufferRenderbuffer,
    
    eGraphicsOp_VerifyFramebufferState,
    
    eGraphicsOp_CreateRenderbuffer,
    
    eGraphicsOp_BindRenderbuffer,
    
    eGraphicsOp_RenderbufferStorage,
};

NSPI_END()


#endif














