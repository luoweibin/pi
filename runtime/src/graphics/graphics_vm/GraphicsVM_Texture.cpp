/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.10.13   0.1     Create
 ******************************************************************************/
#include "GraphicsVM_Impl.h"

NSPI_BEGIN()

void GraphicsVM::ActiveTexture(int32_t unit)
{
    piAssert(unit >= 0, ;);
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_ActiveTexture, unit);
}

void GraphicsVM::GenerateMipmap(int target)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_GenerateMipmap, target);
}

void GraphicsVM::BindTexture(int32_t target, int32_t unit)
{
    piAssert(unit >= 0, ;);
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_BindTexture, target, unit);
}

void GraphicsVM::TexParam(int32_t target, int32_t name, int32_t value)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_TexParam, target, name, value);
}

int32_t GraphicsVM::CreateTexture()
{
    lock_guard<mutex> lock(mMutex);
    int32_t name = AllocName();
    PushByteCode(eGraphicsOp_CreateTexture, name);
    return name;
}

void GraphicsVM::TexImage2D(int target,
                            int32_t level,
                            int format,
                            iBitmap *bitmap,
                            int32_t planar)
{
    piAssert(bitmap != nullptr, ;);
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_TexImage2D, target, level, format, bitmap, planar);
}

void GraphicsVM::CompressedTexImage2D(int target,
                                      int32_t level,
                                      int32_t format,
                                      iBitmap *bitmap,
                                      int32_t planar)
{
    piAssert(bitmap != nullptr, ;);
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_CompressedTexImage2D, target, level, format, bitmap, planar);
}

void GraphicsVM::CopyTexImage2D(int target,
                                int32_t level,
                                int32_t format,
                                int32_t x, int32_t y,
                                int32_t width, int32_t height)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_CopyTexImage2D, target, level, format, x, y, width, height);
}

void GraphicsVM::TexSubImage2D(int target,
                               int32_t level,
                               int32_t xOffset, int32_t yOffset,
                               int32_t width, int32_t height,
                               iBitmap *bitmap,
                               int32_t planar)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_TexSubImage2D, target, level, xOffset, yOffset, width, height, bitmap, planar);
}

void GraphicsVM::CompressedTexSubImage2D(int target,
                                         int32_t level,
                                         int32_t xOffset, int32_t yOffset,
                                         int32_t width, int32_t height,
                                         iBitmap *bitmap,
                                         int32_t planar)
{
    piAssert(bitmap != nullptr, ;);
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_CompressedTexSubImage2D, target, level, xOffset, yOffset, width, height, bitmap, planar);
}

void GraphicsVM::CopyTexSubImage2D(int target,
                                   int32_t level,
                                   int32_t format,
                                   int32_t xOffset, int32_t yOffset,
                                   int32_t x, int32_t y,
                                   int32_t width, int32_t height)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_CopyTexSubImage2D, target, level, format, xOffset, yOffset, x, y, width, height);
}

void GraphicsVM::PixelStorei(int name, int32_t value)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_PixelStorei, name, value);
}


/**
 * =================================================================================================
 * Dispatch
 */

void GraphicsVM::CallGenerateMipmap(iGraphicsByteCode *byteCode)
{
    int target = byteCode->GetArg(0);
    mBackend->GenerateMipmap(target);
    
    PILOGV(PI_GRAPHICS_TAG, "GenerateMipmap(target:%s)", piTexTargetName(target).c_str());
}

void GraphicsVM::CallCreateTexture(iGraphicsByteCode* byteCode)
{
    int32_t name = byteCode->GetArg(0);
    mBackend->CreateTexture(name);
    
    PILOGV(PI_GRAPHICS_TAG, "CreateTexture(name:%d)", name);
}

void GraphicsVM::CallActiveTexture(iGraphicsByteCode* byteCode)
{
    int unit = byteCode->GetArg(0);
    mBackend->ActiveTexture(unit);

    PILOGV(PI_GRAPHICS_TAG, "ActiveTexture(unit:%d)", unit);
}

void GraphicsVM::CallBindTexture(iGraphicsByteCode* byteCode)
{
    int target = byteCode->GetArg(0);
    int32_t name = byteCode->GetArg(1);
    mBackend->BindTexture(target, name);
    
    PILOGV(PI_GRAPHICS_TAG,
           "BindTexture(target:%s, unit:%d)",
           piTexTargetName(target).c_str(),
           name);
}

void GraphicsVM::CallTexImage2D(iGraphicsByteCode* byteCode)
{
    int target = byteCode->GetArg(0);
    int32_t level = byteCode->GetArg(1);
    int format = byteCode->GetArg(2);
    SmartPtr<iBitmap> bitmap = piQueryVarObject<iBitmap>(byteCode->GetArg(3));
    int32_t planar = byteCode->GetArg(4);
    mBackend->TexImage2D(target, level, format, bitmap, planar);
    
    PILOGV(PI_GRAPHICS_TAG,
           "TexImage2D(target:%s, level:%d, format:%s, bitmap:%s, planar:%d)",
           piTexTargetName(target).c_str(),
           level,
           piPixelFormatName(format).c_str(),
           bitmap->ToString().c_str(),
           planar);
}

void GraphicsVM::CallCompressedTexImage2D(iGraphicsByteCode* byteCode)
{
    int target = byteCode->GetArg(0);
    int32_t level = byteCode->GetArg(1);
    int format = byteCode->GetArg(2);
    SmartPtr<iBitmap> bitmap = piQueryVarObject<iBitmap>(byteCode->GetArg(3));
    int32_t planar = byteCode->GetArg(4);
    mBackend->CompressedTexImage2D(target, level, format, bitmap, planar);

    PILOGV(PI_GRAPHICS_TAG,
           "CompressedTexImage2D(target:%s, level:%d, format:%s, bitmap:%s, planar:%d)",
           piTexTargetName(target).c_str(),
           level,
           piPixelFormatName(format).c_str(),
           bitmap->ToString().c_str(),
           planar);
}

void GraphicsVM::CallCopyTexImage2D(iGraphicsByteCode *byteCode)
{
    int target = byteCode->GetArg(0);
    int32_t level = byteCode->GetArg(1);
    int format = byteCode->GetArg(2);
    int32_t x = byteCode->GetArg(3);
    int32_t y = byteCode->GetArg(4);
    int32_t width = byteCode->GetArg(5);
    int32_t height = byteCode->GetArg(6);
    mBackend->CopyTexImage2D(target, level, format, x, y, width, height);
    
    PILOGV(PI_GRAPHICS_TAG,
           "CopyTexImage2D(target:%s, level:%d, format:%s, x:%d, y:%d, width:%d, height:%d)",
           piTexTargetName(target).c_str(),
           level,
           piPixelFormatName(format).c_str(),
           x,
           y,
           width,
           height);
}

void GraphicsVM::CallTexSubImage2D(iGraphicsByteCode* byteCode)
{
    int target = byteCode->GetArg(0);
    int32_t level = byteCode->GetArg(1);
    int32_t xOffset = byteCode->GetArg(2);
    int32_t yOffset = byteCode->GetArg(3);
    int32_t width = byteCode->GetArg(4);
    int32_t height = byteCode->GetArg(5);
    SmartPtr<iBitmap> bitmap = piQueryVarObject<iBitmap>(byteCode->GetArg(6));
    int32_t planar = byteCode->GetArg(7);
    
    mBackend->TexSubImage2D(target,
                            level,
                            xOffset, yOffset,
                            width, height,
                            bitmap,
                            planar);
    
    PILOGV(PI_GRAPHICS_TAG,
           "TexSubImage2D(target:%s, level:%d, xOffset:%d, yOffset:%d, width:%d, height:%d, bitmap:%s, planar:%d",
           piTexTargetName(target).c_str(),
           level,
           xOffset, yOffset,
           width, height,
           bitmap->ToString().c_str(),
           planar);
}

void GraphicsVM::CallCompressedTexSubImage2D(iGraphicsByteCode* byteCode)
{
    int target = byteCode->GetArg(0);
    int32_t level = byteCode->GetArg(1);
    int32_t xOffset = byteCode->GetArg(2);
    int32_t yOffset = byteCode->GetArg(3);
    int32_t width = byteCode->GetArg(4);
    int32_t height = byteCode->GetArg(5);
    SmartPtr<iBitmap> bitmap = piQueryVarObject<iBitmap>(byteCode->GetArg(6));
    int32_t planar = byteCode->GetArg(7);
    
    mBackend->CompressedTexSubImage2D(target,
                                      level,
                                      xOffset, yOffset,
                                      width, height,
                                      bitmap,
                                      planar);
    
    PILOGV(PI_GRAPHICS_TAG,
           "CompressedTexSubImage2D(target:%s, level:%d, xOffset:%d, yOffset:%d, width:%d, height:%d, bitmap:%s, planar:%d",
           piTexTargetName(target).c_str(),
           level,
           xOffset, yOffset,
           width, height,
           bitmap->ToString().c_str(),
           planar);
}

void GraphicsVM::CallCopyTexSubImage2D(iGraphicsByteCode *byteCode)
{
    int target = byteCode->GetArg(0);
    int32_t level = byteCode->GetArg(1);
    int format = byteCode->GetArg(2);
    int32_t xOffset = byteCode->GetArg(3);
    int32_t yOffset = byteCode->GetArg(4);
    int32_t x = byteCode->GetArg(5);
    int32_t y = byteCode->GetArg(6);
    int32_t width = byteCode->GetArg(7);
    int32_t height = byteCode->GetArg(8);
    
    mBackend->CopyTexSubImage2D(target,
                                level,
                                format,
                                xOffset, yOffset,
                                x, y,
                                width, height);
    
    PILOGV(PI_GRAPHICS_TAG,
           "CopyTexSubImage2D(target:%s, level:%d, format:%s, xOffset:%d, yOffset:%d, x:%d, y:%d, width:%d, height:%d)",
           piTexTargetName(target).c_str(),
           level,
           piPixelFormatName(format).c_str(),
           xOffset, yOffset,
           x, y,
           width, height);
}

void GraphicsVM::CallTexParam(iGraphicsByteCode* byteCode)
{
    int target = byteCode->GetArg(0);
    int32_t name = byteCode->GetArg(1);
    int32_t value = byteCode->GetArg(2);
    mBackend->TexParam(target, name, value);
    
    PILOGV(PI_GRAPHICS_TAG,
           "TexParam(target:%s, name:%s, value:%s)",
           piTexTargetName(target).c_str(),
           piTexConfigName(name).c_str(),
           piTexValueName(value).c_str());
}

void GraphicsVM::CallPixelStorei(iGraphicsByteCode* byteCode)
{
    int name = byteCode->GetArg(0);
    int value = byteCode->GetArg(1);
    mBackend->PixelStorei(name, value);
    
    PILOGV(PI_GRAPHICS_TAG,
           "SetPixelStorei(name:%s, value:%d)",
           piTexConfigName(name).c_str(),
           value);
}


NSPI_END()
































