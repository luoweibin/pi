/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2015.4.12   0.1     Create
 ******************************************************************************/
#ifndef PI_SRC_GRAPHICS_GRAPHICSVM_GRAPHICSUTILSOPENGLDESKTOP_H
#define PI_SRC_GRAPHICS_GRAPHICSVM_GRAPHICSUTILSOPENGLDESKTOP_H

#include "GraphicsUtils_OpenGL.h"

NSPI_BEGIN()

static inline GLenum ToGLDraw(int mode)
{
    switch (mode)
    {
        case eGraphicsDraw_Lines:
            return GL_LINES;
        case eGraphicsDraw_LineLoop:
            return GL_LINE_LOOP;
        case eGraphicsDraw_Triangles:
            return GL_TRIANGLES;
        case eGraphicsDraw_TriangleStrip:
            return GL_TRIANGLE_STRIP;
        case eGraphicsDraw_Points:
            return GL_POINTS;
        case eGraphicsDraw_Quads:
            return GL_QUADS;
        default:
            return eGraphicsDraw_Unknown;
    }
}

static inline GLenum ToGLPolygonMode(int mode)
{
#if defined(PI_OPENGL)
    switch (mode)
    {
        case ePolygonMode_Point:
            return GL_POINT;
        case ePolygonMode_Line:
            return GL_LINE;
        case ePolygonMode_Fill:
        default:
            return GL_FILL;
    }
#else
    PILOGE(PI_GRAPHICS_TAG, "%s not supported in OpenGL ES.", piPolygonModeName(mode).c_str());
    piAssert(false, -1);
    return -1;
#endif
}

static inline GLenum ToGLFeature(int32_t features)
{
    GLenum value = 0;
    
    if (piFlagIs(features, eGraphicsFeature_DepthTest))
    {
        piFlagOn(value, GL_DEPTH_TEST);
    }
    
    if (piFlagIs(features, eGraphicsFeature_CullFace))
    {
        piFlagOn(value, GL_CULL_FACE);
    }
    
    if (piFlagIs(features, eGraphicsFeature_Blend))
    {
        piFlagOn(value, GL_BLEND);
    }
    
    if (piFlagIs(features, eGraphicsFeature_PolygonOffset_Fill))
    {
        piFlagOn(value, GL_POLYGON_OFFSET_FILL);
    }
    
#if defined(PI_OPENGL)
    if (piFlagIs(features, eGraphicsFeature_PolygonOffset_Line))
    {
        piFlagOn(value, GL_POLYGON_OFFSET_LINE);
    }
    
    if (piFlagIs(features, eGraphicsFeature_PolygonOffset_Point))
    {
        piFlagOn(value, GL_POLYGON_OFFSET_POINT);
    }
    
    if (piFlagIs(features, eGraphicsFeature_MultiSample))
    {
        piFlagOn(value, GL_MULTISAMPLE);
    }
#endif
    
    return value;
}

static inline GLenum ToGLTextureTarget(int target)
{
    switch (target) {
#if defined(PI_OPENGL)
        case eTexTarget_1D:
            return GL_TEXTURE_1D;
        case eTexTarget_3D:
            return GL_TEXTURE_3D;
#endif
        case eTexTarget_2D:
            return GL_TEXTURE_2D;
        case eTexTarget_CubeMap:
            return GL_TEXTURE_CUBE_MAP;
        default:
            return -1;
    }
}

static inline GLenum ToGLTextureParamName(int name)
{
    switch (name)
    {
        case eTexConfig_MinFilter:
            return GL_TEXTURE_MIN_FILTER;
        case eTexConfig_MagFilter:
            return GL_TEXTURE_MAG_FILTER;
        case eTexConfig_PackAligment:
            return GL_PACK_ALIGNMENT;
        case eTexConfig_UnpackAligment:
            return GL_UNPACK_ALIGNMENT;
        case eTexConfig_WrapS:
            return GL_TEXTURE_WRAP_S;
        case eTexConfig_WrapT:
            return GL_TEXTURE_WRAP_T;
        case eTexConfig_WrapR:
            return GL_TEXTURE_WRAP_R;
        default:
            return -1;
    }
}

static inline GLenum ToGLTextureParamValue(int value)
{
    switch (value)
    {
        case eTexValue_Nearest:
            return GL_NEAREST;
        case eTexValue_Linear:
            return GL_LINEAR;
        case eTexValue_Clamp:
            return GL_CLAMP_TO_EDGE;
        case eTexValue_Repeat:
            return GL_REPEAT;
#if defined(PI_OPENGL)
        case eTexValue_Border:
            return GL_CLAMP_TO_BORDER;
        case eTexValue_Mirror:
            return GL_MIRRORED_REPEAT;
#endif
        default:
            return eTexValue_Unknown;
    }
}

static inline GLenum ToGLHintTarget(int value)
{
    switch (value)
    {
#if defined(PI_OPENGL)
        case eHintTarget_TextureCompression:
            return GL_TEXTURE_COMPRESSION_HINT;
        case eHintTarget_PolygonSmooth:
            return GL_POLYGON_SMOOTH_HINT;
        case eHintTarget_LineSmooth:
            return GL_LINE_SMOOTH_HINT;
#endif
        default:
            return 0;
    }
}



static inline GLenum ToGLFormat(int format)
{
    switch (format)
    {
        case ePixelFormat_RGBA:
            return GL_RGBA;
        case ePixelFormat_BGRA:
            return GL_BGRA;
        case ePixelFormat_R:
            return GL_RED;
        case ePixelFormat_RGB:
            return GL_RGB;
        case ePixelFormat_Depth16:
            return GL_DEPTH_COMPONENT16;
        case ePixelFormat_Depth24:
            return GL_DEPTH_COMPONENT24;
        case ePixelFormat_Depth24Stencil8:
            return GL_DEPTH24_STENCIL8;
        case ePixelFormat_Depth32:
            return GL_DEPTH_COMPONENT32;
        case ePixelFormat_Depth32F:
            return GL_DEPTH_COMPONENT32F;
        case ePixelFormat_Stencil:
            return GL_STENCIL_INDEX8;
        default:
            return -1;
    }
}


//TODO
static inline GLenum ToGLAttachment(int attachment)
{
    switch (attachment)
    {
        case eAttachment_Depth:
            return GL_DEPTH_ATTACHMENT;
        case eAttachment_Stencil:
            return GL_STENCIL_ATTACHMENT;
        case eAttachment_DepthStencil:
            return GL_DEPTH_STENCIL_ATTACHMENT;
        case eAttachment_Color0:
            return GL_COLOR_ATTACHMENT0;
        case eAttachment_Color1:
            return GL_COLOR_ATTACHMENT1;
        case eAttachment_Color2:
            return GL_COLOR_ATTACHMENT2;
        case eAttachment_Color3:
            return GL_COLOR_ATTACHMENT3;
        case eAttachment_Color4:
            return GL_COLOR_ATTACHMENT4;
        case eAttachment_Color5:
            return GL_COLOR_ATTACHMENT5;
        case eAttachment_Color6:
            return GL_COLOR_ATTACHMENT6;
        case eAttachment_Color7:
            return GL_COLOR_ATTACHMENT7;
        case eAttachment_Color8:
            return GL_COLOR_ATTACHMENT8;
        case eAttachment_Color9:
            return GL_COLOR_ATTACHMENT9;
        case eAttachment_Color10:
            return GL_COLOR_ATTACHMENT10;
        case eAttachment_Color11:
            return GL_COLOR_ATTACHMENT11;
        case eAttachment_Color12:
            return GL_COLOR_ATTACHMENT12;
        case eAttachment_Color13:
            return GL_COLOR_ATTACHMENT13;
        case eAttachment_Color14:
            return GL_COLOR_ATTACHMENT14;
        case eAttachment_Color15:
            return GL_COLOR_ATTACHMENT15;
        default:
            return -1;
    }
}


static inline GLenum ToGLFramebufferTarget(int target)
{
    switch (target)
    {
#if defined(PI_OPENGL)
        case eFramebuffer_Draw:
            return GL_DRAW_FRAMEBUFFER;
        case eFramebuffer_Read:
            return GL_READ_FRAMEBUFFER;
#endif
        case eFramebuffer_DrawRead:
        default:
            return GL_FRAMEBUFFER;
    }
}

NSPI_END()


#endif



























