/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.10.13   0.1     Create
 ******************************************************************************/
#ifndef PI_SRC_GRAPHICS_GRAPHICSVM_GRAPHICSUTILOPENGLES2_H
#define PI_SRC_GRAPHICS_GRAPHICSVM_GRAPHICSUTILOPENGLES2_H

#include "GraphicsUtils_OpenGL_ES.h"

NSPI_BEGIN()

static inline GLenum ToGLFramebufferTarget(int target)
{
    switch (target)
    {
        case eFramebuffer_DrawRead:
            return GL_FRAMEBUFFER;
#if defined(PI_APPLE)
        case eFramebuffer_Draw:
            return GL_DRAW_FRAMEBUFFER_APPLE;
        case eFramebuffer_Read:
            return GL_READ_FRAMEBUFFER_APPLE;
#endif
        default:
            PILOGE(PI_GRAPHICS_TAG,
                   "%s not supported in OpenGL ES2.",
                   piFramebufferName(target).c_str());
            return eFramebuffer_Unknown;
    }
}

static inline GLenum ToGLTextureParamName(int name)
{
    switch (name)
    {
        case eTexConfig_MinFilter:
            return GL_TEXTURE_MIN_FILTER;
        case eTexConfig_MagFilter:
            return GL_TEXTURE_MAG_FILTER;
        case eTexConfig_PackAligment:
            return GL_PACK_ALIGNMENT;
        case eTexConfig_UnpackAligment:
            return GL_UNPACK_ALIGNMENT;
        case eTexConfig_WrapS:
            return GL_TEXTURE_WRAP_S;
        case eTexConfig_WrapT:
            return GL_TEXTURE_WRAP_T;
        default:
            PILOGE(PI_GRAPHICS_TAG,
                   "%s not supported in OpenGL ES2.",
                   piTexConfigName(name).c_str());
            return -1;
    }
}

static inline GLenum ToGLFormat(int format)
{
    switch (format)
    {
        case ePixelFormat_RGBA:
            return GL_RGBA;
#if !defined(PI_ANDROID)
        case ePixelFormat_BGRA:
            return GL_BGRA;
#endif
        case ePixelFormat_R:
            return GL_LUMINANCE;
        case ePixelFormat_RGB:
            return GL_RGB;
        case ePixelFormat_Depth16:
            return GL_DEPTH_COMPONENT16;
        case ePixelFormat_Depth24:
            return GL_DEPTH_COMPONENT24_OES;
        case ePixelFormat_Depth24Stencil8:
            return GL_DEPTH24_STENCIL8_OES;
        case ePixelFormat_Depth32:
            return GL_DEPTH_COMPONENT32_OES;
        case ePixelFormat_Stencil:
            return GL_STENCIL_INDEX8;
        default:
            return -1;
    }
}



NSPI_END()



#endif



























