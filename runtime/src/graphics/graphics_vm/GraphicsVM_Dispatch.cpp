/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.10.13   0.1     Create
 ******************************************************************************/
#include "GraphicsVM_Impl.h"

NSPI_BEGIN()

void GraphicsVM::Dispatch(iGraphicsByteCode* byteCode)
{
    int op = byteCode->GetOperation();
    switch (op)
    {
        case eGraphicsOp_DumpDebugInfo:
            CallDumpDebugInfo(byteCode);
            break;
        case eGraphicsOp_CreateNativeGraphicsObject:
            CallCreateNativeGraphicsObject(byteCode);
            break;
        case eGraphicsOp_InsertEventMarker:
            CallInsertEventMarker(byteCode);
            break;
        case eGraphicsOp_PopGroupMarker:
            CallPopGroupMarker(byteCode);
            break;
        case eGraphicsOp_PushGroupMarker:
            CallPushGroupMarker(byteCode);
            break;
        case eGraphicsOp_ActiveTexture:
            CallActiveTexture(byteCode);
            break;
        case eGraphicsOp_BindBuffer:
            CallBindBuffer(byteCode);
            break;
        case eGraphicsOp_BindVertexArray:
            CallBindVertexArray(byteCode);
            break;
        case eGraphicsOp_CreateVertexArray:
            CallCreateVertexArray(byteCode);
            break;
        case eGraphicsOp_BindFramebuffer:
            CallBindFramebuffer(byteCode);
            break;
        case eGraphicsOp_BindTexture:
            CallBindTexture(byteCode);
            break;
        case eGraphicsOp_Clear:
            CallClear(byteCode);
            break;
        case eGraphicsOp_ClearDepth:
            CallClearDepth(byteCode);
            break;
        case eGraphicsOp_DepthMask:
            CallDepthMask(byteCode);
            break;
        case eGraphicsOp_ColorMask:
            CallColorMask(byteCode);
            break;
        case eGraphicsOp_CompileProgram:
            CallCompileProgram(byteCode);
            break;
        case eGraphicsOp_LinkProgram:
            CallLinkProgram(byteCode);
            break;
        case eGraphicsOp_TexParam:
            CallTexParam(byteCode);
            break;
        case eGraphicsOp_CreateBuffer:
            CallCreateBuffer(byteCode);
            break;
        case eGraphicsOp_CreateFramebuffer:
            CallCreateFramebuffer(byteCode);
            break;
        case eGraphicsOp_CreateRenderbuffer:
            CallCreateRenderbuffer(byteCode);
            break;
        case eGraphicsOp_BindRenderbuffer:
            CallBindRenderbuffer(byteCode);
            break;
        case eGraphicsOp_RenderbufferStorage:
            CallRenderbufferStorage(byteCode);
            break;
        case eGraphicsOp_CreateProgram:
            CallCreateProgram(byteCode);
            break;
        case eGraphicsOp_CreateTexture:
            CallCreateTexture(byteCode);
            break;
        case eGraphicsOp_Retain:
            CallRetain(byteCode);
            break;
        case eGraphicsOp_Release:
            CallDestroy(byteCode);
            break;
        case eGraphicsOp_Flush:
            CallFlush(byteCode);
            break;
        case eGraphicsOp_PolygonOffset:
            CallPolygonOffset(byteCode);
            break;
        case eGraphicsOp_PolygonMode:
            CallPolygonMode(byteCode);
            break;
        case eGraphicsOp_DepthFunction:
            CallDepthFunction(byteCode);
            break;
        case eGraphicsOp_DisableFeatures:
            CallDisableFeatures(byteCode);
            break;
        case eGraphicsOp_DisableVertexAttr:
            CallDisableVertexAttr(byteCode);
            break;
        case eGraphicsOp_DrawArrays:
            CallDrawArrays(byteCode);
            break;
        case eGraphicsOp_DrawArraysInstanced:
            CallDrawArraysInstanced(byteCode);
            break;
        case eGraphicsOp_DrawElements:
            CallDrawElements(byteCode);
            break;
        case eGraphicsOp_DrawElementsOffset:
            CallDrawElementsOffset(byteCode);
            break;
        case eGraphicsOp_DrawElementsInstanced:
            CallDrawElementsInstanced(byteCode);
            break;
        case eGraphicsOp_EnableFeatures:
            CallEnableFeatures(byteCode);
            break;
        case eGraphicsOp_EnableVertexAttr:
            CallEnableVertexAttr(byteCode);
            break;
        case eGraphicsOp_BindVertexAttr:
            CallBindVertexAttr(byteCode);
            break;
        case eGraphicsOp_BlendColor:
            CallBlendColor(byteCode);
            break;
        case eGraphicsOp_BlendEquation:
            CallBlendEquation(byteCode);
            break;
        case eGraphicsOp_BlendFunc:
            CallBlendFunc(byteCode);
            break;
        case eGraphicsOp_BlendFuncSeparate:
            CallBlendFuncSeparate(byteCode);
            break;
        case eGraphicsOp_ClearColor:
            CallClearColor(byteCode);
            break;
        case eGraphicsOp_CullFace:
            CallCullFace(byteCode);
            break;
        case eGraphicsOp_VerifyFramebufferState:
            CallVerifyFramebufferState(byteCode);
            break;
        case eGraphicsOp_FramebufferTexture2D:
            CallFramebufferTexture2D(byteCode);
            break;
        case eGraphicsOp_FramebufferRenderbuffer:
            CallFramebufferRenderbuffer(byteCode);
            break;
        case eGraphicsOp_PixelStorei:
            CallPixelStorei(byteCode);
            break;
        case eGraphicsOp_Uniform1f:
            CallUniform1f(byteCode);
            break;
        case eGraphicsOp_Uniform1fv:
            CallUniform1fv(byteCode);
            break;
        case eGraphicsOp_Uniform1i:
            CallUniform1i(byteCode);
            break;
        case eGraphicsOp_Uniform1iv:
            CallUniform1iv(byteCode);
            break;
        case eGraphicsOp_Uniform2f:
            CallUniform2f(byteCode);
            break;
        case eGraphicsOp_Uniform2fv:
            CallUniform2fv(byteCode);
            break;
        case eGraphicsOp_Uniform2i:
            CallUniform2i(byteCode);
            break;
        case eGraphicsOp_Uniform2iv:
            CallUniform2iv(byteCode);
            break;
        case eGraphicsOp_Uniform3f:
            CallUniform3f(byteCode);
            break;
        case eGraphicsOp_Uniform3fv:
            CallUniform3fv(byteCode);
            break;
        case eGraphicsOp_Uniform3i:
            CallUniform3i(byteCode);
            break;
        case eGraphicsOp_Uniform3iv:
            CallUniform3iv(byteCode);
            break;
        case eGraphicsOp_UniformVec3:
            CallUniformVec3(byteCode);
            break;
        case eGraphicsOp_UniformVec3v:
            CallUniformVec3v(byteCode);
            break;
        case eGraphicsOp_Uniform4f:
            CallUniform4f(byteCode);
            break;
        case eGraphicsOp_Uniform4fv:
            CallUniform4fv(byteCode);
            break;
        case eGraphicsOp_Uniform4i:
            CallUniform4i(byteCode);
            break;
        case eGraphicsOp_Uniform4iv:
            CallUniform4iv(byteCode);
            break;
        case eGraphicsOp_UniformVec4:
            CallUniformVec4(byteCode);
            break;
        case eGraphicsOp_UniformRect:
            CallUniformRect(byteCode);
            break;
        case eGraphicsOp_UniformQuat:
            CallUniformQuat(byteCode);
            break;
        case eGraphicsOp_UniformMat4f:
            CallUniformMat4f(byteCode);
            break;
        case eGraphicsOp_UniformMat4fv:
            CallUniformMat4fv(byteCode);
            break;
        case eGraphicsOp_VertexAttr:
            CallVertexAttr(byteCode);
            break;
        case eGraphicsOp_Viewport:
            CallViewport(byteCode);
            break;
        case eGraphicsOp_Winding:
            CallWinding(byteCode);
            break;
        case eGraphicsOp_BufferData:
            CallBufferData(byteCode);
            break;
        case eGraphicsOp_BufferSubData:
            CallBufferSubData(byteCode);
            break;
        case eGraphicsOp_TexImage2D:
            CallTexImage2D(byteCode);
            break;
        case eGraphicsOp_CompressedTexImage2D:
            CallCompressedTexImage2D(byteCode);
            break;
        case eGraphicsOp_CopyTexImage2D:
            CallCopyTexImage2D(byteCode);
            break;
        case eGraphicsOp_TexSubImage2D:
            CallTexSubImage2D(byteCode);
            break;
        case eGraphicsOp_CompressedTexSubImage2D:
            CallCompressedTexSubImage2D(byteCode);
            break;
        case eGraphicsOp_CopyTexSubImage2D:
            CallCopyTexSubImage2D(byteCode);
            break;
        case eGraphicsOp_UseProgram:
            CallUseProgram(byteCode);
            break;
        case eGraphicsOp_ClearStencil:
            CallClearStencil(byteCode);
            break;
        case eGraphicsOp_StencilFunc:
            CallStencilFunc(byteCode);
            break;
        case eGraphicsOp_StencilOp:
            CallStencilOp(byteCode);
            break;
        case eGraphicsOp_StencilOpSeparate:
            CallStencilOpSeparate(byteCode);
            break;
        case eGraphicsOp_LineWidth:
            CallLineWidth(byteCode);
            break;
        case eGraphicsOp_StencilMask:
            CallStencilMask(byteCode);
            break;
        case eGraphicsOp_Hint:
            CallHint(byteCode);
            break;
        default:
            PILOGE(PI_GRAPHICS_TAG, "Unknown bytecode:%d", op);
            break;
    }
}

NSPI_END()




















