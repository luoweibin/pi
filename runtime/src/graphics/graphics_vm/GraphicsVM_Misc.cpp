/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2015.4.11   0.1     Create
 ******************************************************************************/
#include <pi/Graphics.h>
#include <mutex>
#include <deque>

#include "GraphicsOp.h"
#include "GraphicsBackend.h"

#include "GraphicsVM_Impl.h"

using namespace std;

NSPI_BEGIN()

void GraphicsVM::DumpByteCodes(string title, const ByteCodeVector& byteCodes)
{
    PILOGV(PI_GRAPHICS_TAG, "=============================================");
    PILOGV(PI_GRAPHICS_TAG, "= [%s] Byte Codes Dump (%d)", title.c_str(), byteCodes.size());
    
    for (int i = 0; i < byteCodes.size(); ++i)
    {
        SmartPtr<iGraphicsByteCode> code = byteCodes[i];
        PILOGV(PI_GRAPHICS_TAG, "= [%d] %d", i, code->GetOperation());
    }
    PILOGV(PI_GRAPHICS_TAG, "=============================================");
}

int32_t GraphicsVM::AllocName()
{
    ++mFreeName;
    
    mRefCounts[mFreeName] = 1;
    
    return mFreeName;
}

void GraphicsVM::PushByteCode(int operation,
                  const Var &arg0,
                  const Var &arg1,
                  const Var &arg2,
                  const Var &arg3,
                  const Var &arg4,
                  const Var &arg5,
                  const Var &arg6,
                  const Var &arg7,
                  const Var &arg8)
{
    mPushBatch->Push(new GraphicsByteCode(operation,
                                          arg0,
                                          arg1,
                                          arg2,
                                          arg3,
                                          arg4,
                                          arg5,
                                          arg6,
                                          arg7,
                                          arg8));
}

GraphicsVM::GraphicsVM(iGraphicsBackend* backend):
mBackend(backend), mFreeName(0),
mFeatures(0),
mFramebuffer(0)
{
    mPushBatch = new ByteCodeBatch();
}

GraphicsVM::~GraphicsVM()
{
}

int32_t GraphicsVM::CreateNativeGraphicsObject(int64_t nativeHandle)
{
    lock_guard<mutex> lock(mMutex);
    int32_t name = AllocName();
    PushByteCode(eGraphicsOp_CreateNativeGraphicsObject, name, nativeHandle);
    return name;
}

void GraphicsVM::DumpDebugInfo()
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_DumpDebugInfo);
}

void GraphicsVM::RetainObject(int32_t name)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Retain, name);
}

bool GraphicsVM::AddAlias(const string &alias, int32_t name)
{
    piAssert(!alias.empty(), false);
    
    lock_guard<mutex> lock(mMutex);
    
    AliasMap::iterator it = mAlias.find(alias);
    piCheck(it == mAlias.end(), false);
    
    mAlias[alias] = name;
    return true;
}

void GraphicsVM::RemoveAlias(const string &alias)
{
    piAssert(!alias.empty(), ;);
    
    lock_guard<mutex> lock(mMutex);
    
    AliasMap::iterator it = mAlias.find(alias);
    if (it != mAlias.end())
    {
        mAlias.erase(it);
    }
}


int32_t GraphicsVM::GetObject(const string &alias) const
{
    piAssert(!alias.empty(), 0);
    
    lock_guard<mutex> lock(mMutex);
    
    AliasMap::const_iterator it = mAlias.find(alias);
    return it != mAlias.end() ? it->second : 0;
}

void GraphicsVM::SetCapability(int cap, bool value)
{
    return mBackend->SetCapability(cap, value);
}

bool GraphicsVM::GetCapability(int cap) const
{
    return mBackend->GetCapability(cap);
}

int GraphicsVM::GetType() const
{
    return mBackend->GetType();
}

void GraphicsVM::FlushCurrentContext()
{
//    InterpretFlush();
}

void GraphicsVM::PushGroupMarker(const std::string& marker)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_PushGroupMarker, marker);
}

void GraphicsVM::PopGroupMarker()
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_PopGroupMarker);
}

void GraphicsVM::InsertEventMarker(const std::string& marker)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_InsertEventMarker, marker);
}



void GraphicsVM::Clear(int32_t fields)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Clear, fields);
}


void GraphicsVM::Submit()
{
    lock_guard<mutex> lock(mMutex);
    
    mPushBatch->SetSubmitTime(piGetSystemTimeMS());
    mBatchQueue.push_back(mPushBatch);
    mPushBatch = new ByteCodeBatch();
}

void GraphicsVM::BeforeRun()
{
    mBackend->OnBeforeRun();
}

void GraphicsVM::AfterRun()
{
    mBackend->OnAfterRun();
}


void GraphicsVM::Run()
{
    mBackend->OnBeforeRun();
    
    int32_t batchCount = 0;
    int32_t batchLeft = 0;
    while (true)
    {
        SmartPtr<ByteCodeBatch> batch;
        {
            lock_guard<mutex> lock(mMutex);
            if (mBatchQueue.empty())
            {
                break;
            }
            
            batch = mBatchQueue.front();
            
            int64_t now = piGetSystemTimeMS();
            if (batchCount > 0 && now - batch->GetSubmitTime() < 1.0/30.0)
            {
                break;
            }
            
            ++batchCount;
            mBatchQueue.pop_front();
            batchLeft = (int32_t)mBatchQueue.size();
        }
        
        const ByteCodeVector& byteCodes = batch->GetByteCodes();
        
        if (!byteCodes.empty())
        {
            int64_t begin = piGetSystemTimeMS();
            int64_t delay = begin - batch->GetSubmitTime();
            PILOGD(PI_GRAPHICS_TAG, "================= Draw Batch Begin (delay:%lldMS)", delay);
            
            for (ByteCodeVector::const_iterator it = byteCodes.begin();
                 it != byteCodes.end();
                 ++it)
            {
                SmartPtr<iGraphicsByteCode> byteCode = *it;
                Dispatch(byteCode);
            }
            
            int64_t timeCost = piGetSystemTimeMS() - begin;
            
            PILOGD(PI_GRAPHICS_TAG,
                   "================= Draw Batch End (timecost:%lldMS)",
                   timeCost);
        }
    }
    
    PILOGD(PI_GRAPHICS_TAG, "Batches executed:%d, batches left:%d", batchCount, batchLeft);
    
    mBackend->OnAfterRun();
}



void GraphicsVM::DrawArrays(int mode, int32_t first, int32_t count)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_DrawArrays, mode, first, count);
}

void GraphicsVM::DrawArraysInstanced(int mode, int32_t first, int32_t count, int32_t instanceCount)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_DrawArraysInstanced, mode, first, count, instanceCount);
}

void GraphicsVM::DrawElements(int mode, int32_t count, int type)
{
    piAssert(type == eType_U8 || type == eType_U16 || type == eType_U32, ;);
    
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_DrawElements, mode, count, type);
}

void GraphicsVM::DrawElementsOffset(int mode, int32_t count, int type, int32_t offset)
{
    piAssert(type == eType_U8 || type == eType_U16 || type == eType_U32, ;);
    
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_DrawElementsOffset, mode, count, type, offset);
}

void GraphicsVM::DrawElementsInstanced(int mode, int32_t count, int type, int32_t instanceCount)
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_DrawElementsInstanced, mode, count, type, instanceCount);
}

void GraphicsVM::ReleaseObejct(int32_t name)
{
    piCheck(name > 0, ;);
    
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Release, name);
}

void GraphicsVM::Flush()
{
    lock_guard<mutex> lock(mMutex);
    PushByteCode(eGraphicsOp_Flush);
}


void GraphicsVM::Viewport(int32_t x, int32_t y, int32_t width, int32_t height)
{
    lock_guard<mutex> lock(mMutex);
    
    mViewport.x = x;
    mViewport.y = y;
    mViewport.width = width;
    mViewport.height = height;
    
    PushByteCode(eGraphicsOp_Viewport, x, y, width, height);
}

rect GraphicsVM::GetViewport() const
{
    lock_guard<mutex> lock(mMutex);
    return mViewport;
}


/**
 * =================================================================================================
 * Dispatch
 */

void GraphicsVM::CallDumpDebugInfo(iGraphicsByteCode *byteCode)
{
    mBackend->DumpDebugInfo();
}

void GraphicsVM::CallCreateNativeGraphicsObject(iGraphicsByteCode *byteCode)
{
    int32_t name = byteCode->GetArg(0);
    int64_t handle = byteCode->GetArg(1);
    mBackend->CreateNativeGraphicsObject(name, handle);
}

void GraphicsVM::CallPushGroupMarker(iGraphicsByteCode* byteCode)
{
    string marker = byteCode->GetArg(0);
    mBackend->PushGroupMarker(marker);

    PILOGV(PI_GRAPHICS_TAG, "PushGroupMarker(%s)", marker.c_str());
}


void GraphicsVM::CallPopGroupMarker(iGraphicsByteCode* byteCode)
{
    mBackend->PopGroupMarker();

    PILOGV(PI_GRAPHICS_TAG, "PopGroupMarker");
}

void GraphicsVM::CallInsertEventMarker(iGraphicsByteCode* byteCode)
{
    string marker = byteCode->GetArg(0);
    mBackend->InsertEventMarker(marker);

    PILOGV(PI_GRAPHICS_TAG, "InsertEventMarker(%s)", marker.c_str());
}

void GraphicsVM::CallClear(iGraphicsByteCode* byteCode)
{
    int32_t fields = byteCode->GetArg(0);
    mBackend->Clear(fields);

    PILOGV(PI_GRAPHICS_TAG, "Clear(fields:%s)", piFramebufferBitName(fields).c_str());
}

void GraphicsVM::CallViewport(iGraphicsByteCode* byteCode)
{
    int32_t x = byteCode->GetArg(0);
    int32_t y = byteCode->GetArg(1);
    int32_t width = byteCode->GetArg(2);
    int32_t height = byteCode->GetArg(3);
    mBackend->Viewport(x, y, width, height);

    PILOGV(PI_GRAPHICS_TAG,
           "Viewport(x:%d, y:%d, width:%d, height:%d)",
           x, y, width, height);
}

void GraphicsVM::CallDrawArrays(iGraphicsByteCode* byteCode)
{
    int mode = byteCode->GetArg(0);
    int32_t first = byteCode->GetArg(1);
    int32_t count = byteCode->GetArg(2);
    mBackend->DrawArrays(mode, first, count);

    PILOGV(PI_GRAPHICS_TAG,
           "DrawArrays(mode:%s, first:%d, count:%d)",
           piGraphicsDrawName(mode).c_str(),
           first,
           count);
}

void GraphicsVM::CallDrawArraysInstanced(iGraphicsByteCode* byteCode)
{
    int mode = byteCode->GetArg(0);
    int32_t first = byteCode->GetArg(1);
    int32_t count = byteCode->GetArg(2);
    int32_t instanceCount = byteCode->GetArg(3);
    mBackend->DrawArraysInstanced(mode, first, count, instanceCount);

    PILOGV(PI_GRAPHICS_TAG,
           "DrawArraysInstanced(mode:%s, first:%d, count:%d, instanceCount:%d)",
           piGraphicsDrawName(mode).c_str(),
           first,
           count,
           instanceCount);
}

void GraphicsVM::CallDrawElements(iGraphicsByteCode* byteCode)
{
    int mode = byteCode->GetArg(0);
    int32_t count = byteCode->GetArg(1);
    int type = byteCode->GetArg(2);
    mBackend->DrawElements(mode, count, type);

    PILOGV(PI_GRAPHICS_TAG,
           "DrawElements(mode:%s, count:%d, type:%s)",
           piGraphicsDrawName(mode).c_str(),
           count,
           piTypeName(type).c_str());
}

void GraphicsVM::CallDrawElementsOffset(iGraphicsByteCode* byteCode)
{
    int mode = byteCode->GetArg(0);
    int32_t count = byteCode->GetArg(1);
    int type = byteCode->GetArg(2);
    int32_t offset = byteCode->GetArg(3);
    mBackend->DrawElementsOffset(mode, count, type, offset);

    PILOGV(PI_GRAPHICS_TAG,
           "DrawElementsOffset(mode:%s, count:%d, type:%s, offset:%d)",
           piGraphicsDrawName(mode).c_str(),
           count,
           piTypeName(type).c_str(),
           offset);
}

void GraphicsVM::CallDrawElementsInstanced(iGraphicsByteCode* byteCode)
{
    int mode = byteCode->GetArg(0);
    int32_t count = byteCode->GetArg(1);
    int type = byteCode->GetArg(2);
    int32_t instanceCount = byteCode->GetArg(3);
    mBackend->DrawElementsInstanced(mode, count, type, instanceCount);

    PILOGV(PI_GRAPHICS_TAG,
           "DrawElementsInstanced(mode:%s, count:%d, type:%s, instanceCount:%d)",
           piGraphicsDrawName(mode).c_str(),
           count,
           piTypeName(type).c_str(),
           instanceCount);
}

void GraphicsVM::CallRetain(iGraphicsByteCode* byteCode)
{
    int32_t name = byteCode->GetArg(0);

    PILOGV(PI_GRAPHICS_TAG, "RetainObject(%d)", name);
    
    RefCountMap::iterator it = mRefCounts.find(name);
    piCheck(it != mRefCounts.end(), ;);
    
    it->second = it->second + 1;
}

void GraphicsVM::CallDestroy(iGraphicsByteCode* byteCode)
{
    int32_t name = byteCode->GetArg(0);
    
    PILOGV(PI_GRAPHICS_TAG, "ReleaseObject(%d)", name);
    
    {
        RefCountMap::iterator it = mRefCounts.find(name);
        piCheck(it != mRefCounts.end(), ;);
        
        it->second = it->second - 1;
        
        if (name == 21)
        {
            PILOGI(PI_GRAPHICS_TAG, "S");
        }
        
        piCheck(it->second <= 0, ;);
    }
    
    {
        AliasMap::iterator it = mAlias.begin();
        while ( it != mAlias.end())
        {
            if (it->second != name)
            {
                ++it;
            }
            else
            {
                it = mAlias.erase(it);
            }
        }
    }
    
    mBackend->ReleaseObejct(name);
}

void GraphicsVM::CallFlush(iGraphicsByteCode* byteCode)
{
    mBackend->Flush();
    
    PILOGV(PI_GRAPHICS_TAG, "Flush()");
}

/**
 * =================================================================================================
 */

#if defined(PI_OPENGL)

iGraphicsBackend* CreateGraphicsBackend_OpenGL2();
iGraphicsBackend* CreateGraphicsBackend_OpenGL3();
iGraphicsBackend* CreateGraphicsBackend_OpenGL4();

#endif

#if defined(PI_OPENGL_ES)

iGraphicsBackend* CreateGraphicsBackend_OpenGL_ES2();
iGraphicsBackend* CreateGraphicsBackend_OpenGL_ES3();

#endif


iGraphicsVM* CreateGraphicsVM(int type)
{
    SmartPtr<iGraphicsBackend> backend;
    switch (type)
    {
#if defined(PI_OPENGL)
        case eGraphicsBackend_OpenGL3:
            backend = CreateGraphicsBackend_OpenGL3();
            break;
        case eGraphicsBackend_OpenGL4:
            backend = CreateGraphicsBackend_OpenGL4();
            break;
#endif

#if defined(PI_OPENGL_ES)
        case eGraphicsBackend_OpenGL_ES2:
            backend = CreateGraphicsBackend_OpenGL_ES2();
            break;
#if !defined(PI_ANDROID)
        case eGraphicsBackend_OpenGL_ES3:
            backend = CreateGraphicsBackend_OpenGL_ES3();
            break;
#endif
#endif
        default:
            break;
    }
    
    piAssert(!backend.IsNull(), nullptr);
    
    return new GraphicsVM(backend);
}



NSPI_END()


















