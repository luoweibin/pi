/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.9.15   0.1     Create
 ******************************************************************************/
#ifndef PI_SRC_GRAPHICS_GRAPHICSBACKEND_H
#define PI_SRC_GRAPHICS_GRAPHICSBACKEND_H

#include <pi/Graphics.h>

namespace nspi
{
    struct iGraphicsBackend : public iGraphics
    {
        virtual ~iGraphicsBackend() {}
        
        virtual int GetType() const = 0;
        
        virtual void CreateNativeGraphicsObject(int32_t name, int64_t nativeHandle) = 0;
        
        virtual bool Support(int op) const = 0;
        
        virtual void CreateProgram(int32_t name) = 0;
        
        virtual void CreateTexture(int32_t name) = 0;
        
        virtual void CreateVertexArray(int32_t name) = 0;
        
        virtual void CreateBuffer(int32_t name) = 0;
        
        virtual void CreateFramebuffer(int32_t name) = 0;
        
        virtual void CreateRenderbuffer(int32_t name) = 0;
        
        virtual void OnBeforeRun() = 0;
        
        virtual void OnAfterRun() = 0;
    };
}

#endif
