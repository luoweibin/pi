/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.10.13   0.1     Create
 ******************************************************************************/
#ifndef PI_SRC_GRAPHICS_GRAPHICSVM_GRAPHICSUTILOPENGLES_H
#define PI_SRC_GRAPHICS_GRAPHICSVM_GRAPHICSUTILOPENGLES_H

#include "GraphicsUtils_OpenGL.h"

NSPI_BEGIN()

static inline GLenum ToGLDraw(int mode)
{
    switch (mode)
    {
        case eGraphicsDraw_Lines:
            return GL_LINES;
        case eGraphicsDraw_LineLoop:
            return GL_LINE_LOOP;
        case eGraphicsDraw_Triangles:
            return GL_TRIANGLES;
        case eGraphicsDraw_TriangleStrip:
            return GL_TRIANGLE_STRIP;
        case eGraphicsDraw_Points:
            return GL_POINTS;
        default:
            PILOGE(PI_GRAPHICS_TAG, "%s not supported in OpenGL ES.", piGraphicsDrawName(mode).c_str());
            return -1;
    }
}


static inline GLenum ToGLPolygonMode(int mode)
{
    PILOGE(PI_GRAPHICS_TAG, "%s not supported in OpenGL ES.", piPolygonModeName(mode).c_str());
    return ePolygonMode_Unknown;
}


static inline GLenum ToGLHintTarget(int value)
{
    PILOGE(PI_GRAPHICS_TAG, "%s not supported in OpenGL ES.", piHintTargetName(value).c_str());
    return eHintTarget_Unknown;
}


static inline GLenum ToGLTextureTarget(int target)
{
    switch (target)
    {
        case eTexTarget_2D:
            return GL_TEXTURE_2D;
        case eTexTarget_CubeMap:
            return GL_TEXTURE_CUBE_MAP;
        case eTexTarget_CubeMapNegativeX:
            return GL_TEXTURE_CUBE_MAP_NEGATIVE_X;
        case eTexTarget_CubeMapNegativeY:
            return GL_TEXTURE_CUBE_MAP_NEGATIVE_Y;
        case eTexTarget_CubeMapNegativeZ:
            return GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;
        case eTexTarget_CubeMapPositiveX:
            return GL_TEXTURE_CUBE_MAP_POSITIVE_X;
        case eTexTarget_CubeMapPositiveY:
            return GL_TEXTURE_CUBE_MAP_POSITIVE_Y;
        case eTexTarget_CubeMapPositiveZ:
            return GL_TEXTURE_CUBE_MAP_POSITIVE_Z;
        default:
            PILOGE(PI_GRAPHICS_TAG,
                   "%s not supported in OpenGL ES.",
                   piTexTargetName(target).c_str());
            return eTexTarget_Unknown;
    }
}

static inline GLenum ToGLTextureParamValue(int value)
{
    switch (value)
    {
        case eTexValue_Nearest:
            return GL_NEAREST;
        case eTexValue_Linear:
            return GL_LINEAR;
        case eTexValue_Clamp:
            return GL_CLAMP_TO_EDGE;
        case eTexValue_Repeat:
            return GL_REPEAT;
        default:
            PILOGE(PI_GRAPHICS_TAG,
                   "%s not supported in OpenGL ES.",
                   piTexValueName(value).c_str());
            return eTexValue_Unknown;
    }
}


static inline GLenum ToGLAttachment(int attachment)
{
    switch (attachment)
    {
        case eAttachment_Depth:
            return GL_DEPTH_ATTACHMENT;
        case eAttachment_Stencil:
            return GL_STENCIL_ATTACHMENT;
        case eAttachment_Color0:
            return GL_COLOR_ATTACHMENT0;
        default:
            PILOGE(PI_GRAPHICS_TAG,
                   "%s not supported in OpenGL ES.",
                   piAttachmentName(attachment).c_str());
            return eAttachment_Unknown;
    }
}

NSPI_END()


#endif



























