/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2015.4.19   0.1     Create
 ******************************************************************************/

#include <pi/Graphics.h>
#include "png.h"

using namespace std;

NSPI_BEGIN()

static void WriteStream(png_structp png, png_bytep buffer, png_size_t size)
{
    SmartPtr<iStream> stream = (iStream*)png_get_io_ptr(png);
    stream->Write(buffer, size);
}

static void FlushStream(png_structp png)
{
    SmartPtr<iStream> stream = (iStream*)png_get_io_ptr(png);
    stream->Flush();
}

bool piEncodePNG(iBitmap *bitmap, iStream *buffer)
{
    piAssert(bitmap != nullptr, false);
    piAssert(buffer != nullptr, false);
    
    SmartPtr<iPixelFormat> format = bitmap->GetPixelFormat();
    
    SmartPtr<iBitmap> bmpRGB;
    
    if (format->GetName() == ePixelFormat_RGB)
    {
        bmpRGB = bitmap;
    }
    else
    {
        bmpRGB = piConvertToBitmapRGB(bitmap);
    }
    piAssert(!bmpRGB.IsNull(), false);
    
    int32_t width = bitmap->GetWidth();
    int32_t height = bitmap->GetHeight();
    
    png_structp pngPtr = png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
    piAssert(pngPtr != nullptr, false);
    
    png_infop infoPtr = png_create_info_struct(pngPtr);
    if (infoPtr == nullptr)
    {
        png_destroy_write_struct(&pngPtr, nullptr);
        return false;
    }
    
    if (setjmp(png_jmpbuf(pngPtr))) {
        png_destroy_write_struct(&pngPtr, &infoPtr);
        return false;
    }

    png_set_write_fn(pngPtr, buffer, WriteStream, FlushStream);
    
    png_set_IHDR(pngPtr,
                 infoPtr,
                 width,
                 height,
                 8,
                 PNG_COLOR_TYPE_RGB,
                 PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_DEFAULT,
                 PNG_FILTER_TYPE_DEFAULT);
    
    png_write_info(pngPtr, infoPtr);
    
    if (setjmp(png_jmpbuf(pngPtr))) {
        png_destroy_write_struct(&pngPtr, &infoPtr);
        return false;
    }
    
    bmpRGB->Map(eMemMap_ReadWrite);
    
    uint8_t* data = (uint8_t*)bmpRGB->GetData(ePlanar_Default)->Ptr();
    int64_t bytesPerRow = bmpRGB->GetBytesPerRow(ePlanar_Default);
    
    uint8_t** rows = new uint8_t*[bmpRGB->GetHeight()];
    for (int32_t i = 0; i < height; ++i)
    {
        rows[i] = data + bytesPerRow * i;
    }
    
    png_write_image(pngPtr, rows);
    
    if (setjmp(png_jmpbuf(pngPtr))) {
        bmpRGB->Unmap();
        png_destroy_write_struct(&pngPtr, &infoPtr);
        return false;
    }
    
    png_write_end(pngPtr, infoPtr);
    
    png_destroy_write_struct(&pngPtr, &infoPtr);
    
    bmpRGB->Unmap();
    delete [] rows;
    return true;
}


//==================================================================================================


void png_user_read(png_structp png_ptr, png_bytep pBuffer, png_size_t luSize)
{
    SmartPtr<iStream> ptrFile = (iStream*)png_get_io_ptr(png_ptr);
    ssize_t ldRead = (ssize_t)ptrFile->Read(pBuffer, luSize);
    
    if (ldRead != (ssize_t)luSize)
    {
        png_error(png_ptr, "Read Error");
    }
}

iBitmap* piDecodePNG(iStream* data)
{
    piAssert(data != nullptr, nullptr);
    
    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
    if (png_ptr == nullptr)
    {
        return nullptr;
    }
    
    /* Allocate/initialize the memory for image information.  REQUIRED. */
    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == nullptr)
    {
        png_destroy_read_struct(&png_ptr, nullptr, nullptr);
        return nullptr;
    }
    
    /* set custom read function */
    png_set_read_fn(png_ptr, data, png_user_read);
    
    /* If we have already read some of the signature */
    png_set_sig_bytes(png_ptr, 0);
    
    png_read_png(png_ptr,
                 info_ptr,
                 PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_PACKING | PNG_TRANSFORM_EXPAND,
                 nullptr);
    
    int dPixelFormat = 0;
    int colorType = png_get_color_type(png_ptr, info_ptr);
    switch (colorType)
    {
        case PNG_COLOR_TYPE_RGBA:
            dPixelFormat = ePixelFormat_RGBA;
            break;
        case PNG_COLOR_TYPE_RGB:
            dPixelFormat = ePixelFormat_RGB;
            break;
        case PNG_COLOR_TYPE_GRAY:
            dPixelFormat = ePixelFormat_Gray;
            break;
        default:
            png_destroy_read_struct(&png_ptr, nullptr, nullptr);
            return nullptr;
    }
    
    int32_t dHeight = png_get_image_height(png_ptr, info_ptr);
    if (dHeight == 0)
    {
        png_destroy_read_struct(&png_ptr, nullptr, nullptr);
        return nullptr;
    }
    
    int32_t dWidth = png_get_image_width(png_ptr, info_ptr);
    if (dWidth == 0)
    {
        png_destroy_read_struct(&png_ptr, nullptr, nullptr);
        return nullptr;
    }
    
    SmartPtr<iBitmap> bmp = CreateBitmap(dPixelFormat,
                                           dWidth,
                                           dHeight);
    piAssert(!bmp.IsNull(), nullptr);
    
    bmp->Map(eMemMap_ReadWrite);
    
    int32_t luRowBytes = (int32_t)png_get_rowbytes(png_ptr, info_ptr);
    char* pBuffer = (char*)bmp->GetData(0)->Ptr();
    
    png_bytepp row_pointers = png_get_rows(png_ptr, info_ptr);
    
    for (int i = 0; i < dHeight; i++)
    {
        // note that png is ordered top to bottom, but OpenGL expect it bottom to
        // top so the order or swapped
        intptr_t offset = luRowBytes * (dHeight-1-i);
        memcpy(pBuffer + offset, row_pointers[i], luRowBytes);
    }
    
    png_destroy_read_struct(&png_ptr, &info_ptr, nullptr);
    
    bmp->Unmap();
    
    return bmp.PtrAndSetNull();
}


NSPI_END()





















