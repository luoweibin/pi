/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2015.5.25   0.1     Create
 ******************************************************************************/
#include <pi/Graphics.h>

using namespace std;

NSPI_BEGIN()

iBitmap* piConvertToBitmapRGB(iBitmap *bitmap)
{
    piAssert(bitmap != nullptr, nullptr);
    
    int32_t width = bitmap->GetWidth();
    int32_t height = bitmap->GetHeight();
    
    iPixelFormat *format = bitmap->GetPixelFormat();
    if (format->GetName() == ePixelFormat_RGB)
    {
        bitmap->Map(eMemMap_ReadOnly);

        SmartPtr<iMemory> src = bitmap->GetData(ePlanar_Default);
        
        SmartPtr<iBitmap> result = CreateBitmapEmpty(format->GetName(), width, height);
        int64_t size = bitmap->GetBytesPerRow(ePlanar_Default) * height;
        SmartPtr<iMemory> dest = CreateMemory(size);
        result->SetData(ePlanar_Default, dest);
        
        memcpy(dest->Ptr(),
               src->Ptr(),
               (size_t)size);
        
        bitmap->Unmap();
        
        return result.PtrAndSetNull();
    }
    else if (format->GetName() == ePixelFormat_YUV420P)
    {
        bitmap->Map(eMemMap_ReadOnly);

        SmartPtr<iMemory> memY = bitmap->GetData(ePlanar_Y);
        SmartPtr<iMemory> memU = bitmap->GetData(ePlanar_U);
        SmartPtr<iMemory> memV = bitmap->GetData(ePlanar_V);
        uint8_t *planarY = (uint8_t*)memY->Ptr();
        uint8_t *planarU = (uint8_t*)memU->Ptr();
        uint8_t *planarV = (uint8_t*)memV->Ptr();
        
        SmartPtr<iBitmap> result = CreateBitmap(ePixelFormat_RGB, width, height);
        SmartPtr<iMemory> mem = result->GetData(ePlanar_Default);
        uint8_t *data = (uint8_t*)mem->Ptr();
        
        int64_t bytesPerRowY = bitmap->GetBytesPerRow(ePlanar_Y);
        int64_t bytesPerRowU = bitmap->GetBytesPerRow(ePlanar_U);
        int64_t bytesPerRowV = bitmap->GetBytesPerRow(ePlanar_V);
        
        for (int32_t i = 0; i < height; ++i)
        {
            for (int32_t j = 0; j < width; ++j)
            {
                int64_t indexY = bytesPerRowY * i + j;
                int64_t indexU = (bytesPerRowU / 2) * (i / 2) + j / 2;
                int64_t indexV = (bytesPerRowV / 2) * (i / 2) + j / 2;
                int64_t index = indexY * 3;
                
                int Y = planarY[indexY];
                int Cb = planarU[indexU];
                int Cr = planarV[indexV];
                
                int r = 1.164 * (Y - 16) + 1.596 * (Cr - 128);
                int g = 1.164 * (Y - 16) - 0.813 * (Cr - 128) - 0.392 * (Cb - 128);
                int b = 1.164 * (Y - 16) + 2.017 * (Cb - 128);
                
                data[index] = piglm::clamp(uint8_t(r), uint8_t(0), uint8_t(255));
                data[index + 1] = piglm::clamp(uint8_t(g), uint8_t(0), uint8_t(255));
                data[index + 2] = piglm::clamp(uint8_t(b), uint8_t(0), uint8_t(255));
            }
        }
        
        bitmap->Unmap();
        
        return result.PtrAndSetNull();
    }
    else
    {
        PILOGE(PI_GRAPHICS_TAG,
               "convert to bitmap RGB from %d not implemented.",
               format->GetName());
        return nullptr;
    }
}



NSPI_END()




































