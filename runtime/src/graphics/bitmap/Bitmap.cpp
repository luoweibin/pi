/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2012.12.22   0.1     Create
 ******************************************************************************/
#include "../../asset/AssetImpl.h"

using namespace std;

NSPI_BEGIN()


class Bitmap : public iBitmap
{
private:
    int32_t mHeight;
    int32_t mWidth;
    SmartPtr<iPixelFormat> mPixelFormat;
    SmartPtr<iMemory> mData[3];
    int64_t mBytesPerRow[3];
    int32_t mWidths[3];
    int32_t mHeights[3];
    vector<SmartPtr<iBitmap>> mMipMaps;
    
public:
    Bitmap(iPixelFormat *format, int32_t dWidth, int32_t dHeight)
    {
        mHeight = dHeight;
        mWidth = dWidth;
        mWidths[0] = mWidth;
        mHeights[0] = mHeight;
        mWidths[1] = mWidths[2] = mWidth / 2;
        mHeights[1] = mHeights[2] = mHeight / 2;
        mPixelFormat = format;
    }
    
    virtual ~Bitmap()
    {
    }
    
    bool Init(bool empty)
    {
        int64_t size = mPixelFormat->CalcSize(mWidth, mHeight);
        
        if (!empty)
        {
            mData[0] = CreateMemory(size);
            piAssert(!mData[0].IsNull(), false);
        }
        
        mBytesPerRow[0] = size / mHeight;
        
        if (mPixelFormat->GetName() == ePixelFormat_YUV420P)
        {
            if (!empty)
            {
                mData[1] = CreateMemory(size / 4);
                piAssert(!mData[1].IsNull(), false);
                
                mData[2] = CreateMemory(size / 4);
                piAssert(!mData[2].IsNull(), false);
            }
            
            mBytesPerRow[1] = mBytesPerRow[0] / 2;
            mBytesPerRow[2] = mBytesPerRow[0] / 2;
        }
        
        return true;
    }
    
    virtual void Map(int access)
    {
    }
    
    virtual void Unmap()
    {
    }
    
    virtual iMemory* GetData(int32_t planar) const
    {
        piAssert(planar >= 0 && planar < piArrayCount(mData), nullptr);
        return mData[planar];
    }
    
    virtual void SetData(int32_t planar, iMemory *data)
    {
        piAssert(planar >= 0 && planar < piArrayCount(mData), ;);
        mData[planar] = data;
    }
    
    virtual int32_t GetWidth() const
    {
        return mWidth;
    }
    
    virtual int32_t GetHeight() const
    {
        return mHeight;
    }
    
    virtual iPixelFormat *GetPixelFormat() const
    {
        return mPixelFormat;
    }
    
    virtual int64_t GetBytesPerRow(int32_t planar) const
    {
        piAssert(planar >= 0 && planar < piArrayCount(mData), 0);
        return mBytesPerRow[planar];
    }
    
    virtual void SetBytesPerRow(int32_t planar, int32_t bytes)
    {
        piAssert(bytes > 0, ;);
        piAssert(planar >= 0 && planar < piArrayCount(mData), ;);
        mBytesPerRow[planar] = bytes;
    }
    
    virtual bool CreateMipMaps(int32_t levels)
    {
        piAssert(levels > 0, false);
        piAssert(piIsPow2(mWidth), false);
        piAssert(piIsPow2(mHeight), false);
        
        int32_t width = mWidth >> 1;
        int32_t height = mHeight >> 1;
        if (width > height)
        {
            for (int32_t level = 0;
                 width >= 1 && level < levels;
                 width = width >> 1, height = height == 1 ? 1 : height >> 1, ++level)
            {
                SmartPtr<iBitmap> mipmap = CreateBitmapEx(mPixelFormat,
                                                          width,
                                                          height);
                piAssert(!mipmap.IsNull(), false);
                mMipMaps.push_back(mipmap);
            }
        }
        else
        {
            for (int32_t level = 0;
                 height >= 1 && level < levels;
                 width = width == 1 ? 1 : width >> 1, height = height >> 1, ++level)
            {
                SmartPtr<iBitmap> mipmap = CreateBitmapEx(mPixelFormat,
                                                          width,
                                                          height);
                piAssert(!mipmap.IsNull(), false);
                mMipMaps.push_back(mipmap);
            }
        }
        
        return true;
    }
    
    virtual int32_t GetMipMapLevels() const
    {
        return (int32_t)mMipMaps.size();
    }
    
    virtual iBitmap *GetMipMap(int32_t level) const
    {
        piCheck(level < mMipMaps.size(), nullptr);
        return mMipMaps[level];
    }
    
    virtual int32_t GetWidthOfPlanar(int32_t planar) const
    {
        piAssert(planar >= 0 && planar < piArrayCount(mWidths), 0);
        return mWidths[planar];
    }
    
    virtual int32_t GetHeightOfPlanar(int32_t planar) const
    {
        piAssert(planar >= 0 && planar < piArrayCount(mHeights), 0);
        return mHeights[planar];
    }
    
    virtual int32_t GetPixelAlignment(int32_t planar) const
    {
        piAssert(planar >= 0 && planar < piArrayCount(mHeights), 0);
        
        int32_t mPixelSize = (int32_t)mBytesPerRow[0] / mWidth;
        
        if (mPixelSize % 4 == 0)
        {
            return 4;
        }
        
        return 1;
    }
    
    virtual string ToString() const
    {
        return piFormat("iBitmap{pixel format:%s, width:%d, height:%d}",
                        mPixelFormat->ToString().c_str(),
                        mWidth,
                        mHeight);
    }
};

iBitmap *CreateBitmap(int format, int32_t dWidth, int32_t dHeight)
{
    return CreateBitmapEx(CreatePixelFormat(format), dWidth, dHeight);
}

iBitmap *CreateBitmapEx(iPixelFormat *format,
                        int32_t dWidth,
                        int32_t dHeight)
{
    piAssert(format != nullptr, nullptr);
    piAssert(dWidth > 0, nullptr);
    piAssert(dHeight > 0, nullptr);
    
    SmartPtr<Bitmap> bmp = new Bitmap(format, dWidth, dHeight);
    if (bmp->Init(false))
    {
        return bmp.PtrAndSetNull();
    }
    else
    {
        return nullptr;
    }
}

iBitmap *CreateBitmapEmpty(int format, int32_t width, int32_t height)
{
    piAssert(width > 0, nullptr);
    piAssert(height > 0, nullptr);
    
    SmartPtr<iPixelFormat> pixelFormat = CreatePixelFormat(format);
    piAssert(!pixelFormat.IsNull(), nullptr);
    
    SmartPtr<Bitmap> bmp = new Bitmap(pixelFormat, width, height);
    if (bmp->Init(true))
    {
        return bmp.PtrAndSetNull();
    }
    else
    {
        return nullptr;
    }
}



NSPI_END()














