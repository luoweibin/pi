/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2013.11.30   0.1     Create
 ******************************************************************************/
#include <pi/Graphics.h>

using namespace std;

NSPI_BEGIN()

class YUV420PPixelFormat : public iPixelFormat
{
public:
    virtual int GetName() const
    {
        return ePixelFormat_YUV420P;
    }
    
    virtual int64_t CalcSize(int32_t width, int32_t heigth) const
    {
        return width * heigth;
    }
    
    virtual int32_t GetBytesPerPixel() const
    {
        return 1;
    }
    
    virtual int GetFormat(int32_t planar) const
    {
        return ePixelFormat_R;
    }
    
    virtual int GetType(int32_t planar) const
    {
        return eType_U8;
    }
    
    virtual int32_t GetPlanarCount() const
    {
        return 3;
    }
    
    virtual string ToString() const
    {
        return piFormat("iPixelFormat{name:%s}", piPixelFormatName(GetName()).c_str());
    }
};

class GrayPixelFormat : public iPixelFormat
{
public:
    virtual int64_t CalcSize(int32_t width, int32_t height) const
    {
        return GetBytesPerPixel() * width * height;
    }
    
    virtual int32_t GetBytesPerPixel() const
    {
        return 1;
    }
    
    virtual int GetName() const
    {
        return ePixelFormat_Gray;
    }
    
    virtual int GetFormat(int32_t planar) const
    {
        return ePixelFormat_R;
    }
    
    virtual int GetType(int32_t planar) const
    {
        return eType_U8;
    }
    
    virtual int32_t GetPlanarCount() const
    {
        return 1;
    }
    
    virtual string ToString() const
    {
        return piFormat("iPixelFormat{name:%s}", piPixelFormatName(GetName()).c_str());
    }
};

class RGBPixelFormat : public iPixelFormat
{
public:
    virtual int64_t CalcSize(int32_t width, int32_t height) const
    {
        return GetBytesPerPixel() * width * height;
    }
    
    virtual int32_t GetBytesPerPixel() const
    {
        return 3;
    }
    
    virtual int GetName() const
    {
        return ePixelFormat_RGB;
    }
    
    virtual int GetFormat(int32_t planar) const
    {
        return ePixelFormat_RGB;
    }
    
    virtual int GetType(int32_t planar) const
    {
        return eType_U8;
    }
    
    virtual int32_t GetPlanarCount() const
    {
        return 1;
    }
    
    virtual string ToString() const
    {
        return piFormat("iPixelFormat{name:%s}", piPixelFormatName(GetName()).c_str());
    }
};

class RGBAPixelFormat : public iPixelFormat
{
public:
    virtual int64_t CalcSize(int32_t width, int32_t height) const
    {
        return GetBytesPerPixel() * width * height;
    }
    
    virtual int32_t GetBytesPerPixel() const
    {
        return 4;
    }
    
    virtual int GetName() const
    {
        return ePixelFormat_RGBA;
    }
    
    virtual int GetFormat(int32_t planar) const
    {
        return ePixelFormat_RGBA;
    }
    
    virtual int GetType(int32_t planar) const
    {
        return eType_U8;
    }
    
    virtual int32_t GetPlanarCount() const
    {
        return 1;
    }
    
    virtual string ToString() const
    {
        return piFormat("iPixelFormat{name:%s}", piPixelFormatName(GetName()).c_str());
    }
};

class BGRAPixelFormat : public iPixelFormat
{
public:
    virtual int64_t CalcSize(int32_t width, int32_t height) const
    {
        return GetBytesPerPixel() * width * height;
    }
    
    virtual int32_t GetBytesPerPixel() const
    {
        return 4;
    }
    
    virtual int GetName() const
    {
        return ePixelFormat_BGRA;
    }
    
    virtual int GetFormat(int32_t planar) const
    {
        return ePixelFormat_BGRA;
    }
    
    virtual int GetType(int32_t planar) const
    {
        return eType_U8;
    }
    
    virtual int32_t GetPlanarCount() const
    {
        return 1;
    }
    
    virtual string ToString() const
    {
        return piFormat("iPixelFormat{name:%s}", piPixelFormatName(GetName()).c_str());
    }
};

class DXTPixelFormat : public iPixelFormat
{
public:
    virtual int32_t GetBytesPerPixel() const
    {
        return 4;
    }
    
    virtual int GetType(int32_t planar) const
    {
        return eType_U8;
    }
    
    virtual string ToString() const
    {
        return piFormat("iPixelFormat{name:%s}", piPixelFormatName(GetName()).c_str());
    }
    
    virtual int32_t GetPlanarCount() const
    {
        return 1;
    }
    
protected:
    int64_t GetSizeEx(int32_t width, int32_t height, int32_t blockSize) const
    {
        return (width + 3) / 4 * (height + 3) / 4 * blockSize;
    }
};

class DXT1PixelFormat : public DXTPixelFormat
{
public:
    virtual int64_t CalcSize(int32_t width, int32_t height) const
    {
        return GetSizeEx(width, height, 8);
    }
    
    virtual int GetName() const
    {
        return ePixelFormat_DXT1;
    }
    
    virtual int GetFormat(int32_t planar) const
    {
        return ePixelFormat_DXT1;
    }
    
    virtual int32_t GetPlanarCount() const
    {
        return 1;
    }
};

class DXT25PixelFormat : public DXTPixelFormat
{
public:
    virtual int64_t CalcSize(int32_t width, int32_t height) const
    {
        return GetSizeEx(width, height, 16);
    }
};

class DXT2PixelFormat : public DXT25PixelFormat
{
public:
    virtual int GetName() const
    {
        return ePixelFormat_DXT2;
    }
    
    virtual int GetFormat(int32_t planar) const
    {
        return ePixelFormat_DXT2;
    }
};

class DXT3PixelFormat : public DXT25PixelFormat
{
public:
    virtual int GetName() const
    {
        return ePixelFormat_DXT3;
    }
    
    virtual int GetFormat(int32_t planar) const
    {
        return ePixelFormat_DXT3;
    }
};

class DXT4PixelFormat : public DXT25PixelFormat
{
public:
    virtual int GetName() const
    {
        return ePixelFormat_DXT4;
    }
    
    virtual int GetFormat(int32_t planar) const
    {
        return ePixelFormat_DXT4;
    }
};

class DXT5PixelFormat : public DXT25PixelFormat
{
public:
    virtual int GetName() const
    {
        return ePixelFormat_DXT5;
    }
    
    virtual int GetFormat(int32_t planar) const
    {
        return ePixelFormat_DXT5;
    }
};

iPixelFormat *CreatePixelFormat(int format)
{
    switch (format)
    {
        case ePixelFormat_Gray:
            return new GrayPixelFormat();
        case ePixelFormat_RGB:
            return new RGBPixelFormat();
        case ePixelFormat_RGBA:
            return new RGBAPixelFormat();
        case ePixelFormat_BGRA:
            return new BGRAPixelFormat();
        case ePixelFormat_DXT1:
            return new DXT1PixelFormat();
        case ePixelFormat_DXT2:
            return new DXT2PixelFormat();
        case ePixelFormat_DXT3:
            return new DXT3PixelFormat();
        case ePixelFormat_DXT4:
            return new DXT4PixelFormat();
        case ePixelFormat_DXT5:
            return new DXT5PixelFormat();
        case ePixelFormat_YUV420P:
            return new YUV420PPixelFormat();
        default:
            return nullptr;
    }
}



string piPixelFormatName(int format)
{
    switch (format)
    {
        case ePixelFormat_YUV420P:
            return "ePixelFormat_YUV420P";
        case ePixelFormat_RGBA:
            return "ePixelFormat_RGBA";
        case ePixelFormat_BGRA:
            return "ePixelFormat_BGRA";
        case ePixelFormat_R:
            return "ePixelFormat_R";
        case ePixelFormat_RGB:
            return "ePixelFormat_RGB";
        case ePixelFormat_Depth16:
            return "ePixelFormat_Depth16";
        case ePixelFormat_Depth24:
            return "ePixelFormat_Depth24";
        case ePixelFormat_Depth24Stencil8:
            return "ePixelFormat_Depth24Stencil8";
        case ePixelFormat_Depth32:
            return "ePixelFormat_Depth32";
        case ePixelFormat_Stencil:
            return "ePixelFormat_Stencil";
        default:
            return "ePixelFormat_Unknown";
    }
}


NSPI_END()





















