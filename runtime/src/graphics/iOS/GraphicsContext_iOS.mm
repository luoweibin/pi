/*******************************************************************************
 FileName: GraphicsContext_iOS.cpp
 Author: wilburluo
 Version: 0.1
 Date: 2016.10.5
 
 History:
 wilburluo     2016.10.5   0.1     Create
 ******************************************************************************/
#include <pi/graphics/iOS/GraphicsContext_iOS.h>


using namespace std;


NSPI_BEGIN()

class GraphicsContext_iOS : public iGraphicsContext
{
private:
    EAGLContext *mCtx;
    
public:
    GraphicsContext_iOS(EAGLContext* ctx):
    mCtx(ctx)
    {
    }
    
    virtual ~GraphicsContext_iOS()
    {
    }
    
    virtual void MakeCurrent()
    {
        [EAGLContext setCurrentContext:mCtx];
    }
};

iGraphicsContext* CreateGraphicsContext(EAGLContext *ctx)
{
    piAssert(ctx != nil, nullptr);
    return new GraphicsContext_iOS(ctx);
}


NSPI_END()












