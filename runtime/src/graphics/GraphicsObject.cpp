/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.11   0.1     Create
 ******************************************************************************/
#include <pi/Graphics.h>

using namespace std;

NSPI_BEGIN()

class GraphicsObject : public iGraphicsObject
{
private:
    int32_t mName;
    
public:
    GraphicsObject():
    mName(0)
    {
    }
    
    virtual ~GraphicsObject()
    {
        piReleaseGraphicsObject(mName);
    }
    
    virtual int32_t GetName() const
    {
        return mName;
    }
    
    virtual void SetName(int32_t name)
    {
        piRetainGraphicsObject(name);
        piReleaseGraphicsObject(mName);
        mName = name;
    }
};


iGraphicsObject* CreateGraphicsObject()
{
    return new GraphicsObject();
}



NSPI_END()















