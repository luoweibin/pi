/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2016.10.5   0.1     Create
 ******************************************************************************/
#include <pi/graphics/macOS/GraphicsContext_macOS.h>

using namespace std;
using namespace nspi;

class GraphicsContext_macOS : public iGraphicsContext
{
private:
    NSOpenGLContext* mNSCtx;
    
public:
    GraphicsContext_macOS(NSOpenGLContext* ctx):
    mNSCtx(ctx)
    {
    }
    
    virtual ~GraphicsContext_macOS()
    {
    }
    
    virtual void MakeCurrent()
    {
        [mNSCtx makeCurrentContext];
    }
    
    virtual void Lock()
    {
        [mNSCtx lock];
    }
    
    virtual void Unlock()
    {
        [mNSCtx unlock];
    }
    
    virtual void Flush()
    {
        [mNSCtx flushBuffer];
    }
};


iGraphicsContext* nspi::CreateGraphicsContext(NSOpenGLContext* ctx)
{
    piAssert(ctx != nil, nullptr);
    return new GraphicsContext_macOS(ctx);
}





























