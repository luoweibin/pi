//
// Created by CYY on 2017/4/7
//
#include <pi/Graphics.h>
#include <EGL/egl.h>
#include "pi/graphics/android/GraphicsContext_android.h"

NSPI_BEGIN()

class GraphicsContext_android : public iGraphicsContext
{
public:
    GraphicsContext_android()
    {
        mCtx = eglGetCurrentContext();
        mDpy = eglGetCurrentDisplay();
        mRead = eglGetCurrentSurface(EGL_READ);
        mDraw = eglGetCurrentSurface(EGL_DRAW);
    }

    virtual ~GraphicsContext_android() {}

    virtual void MakeCurrent()
    {
        eglMakeCurrent(mDpy, mDraw, mRead, mCtx);
    }

private:
    EGLContext mCtx;
    EGLDisplay mDpy;
    EGLSurface mRead;
    EGLSurface mDraw;
};

iGraphicsContext* CreateGraphicsContext()
{
    return new GraphicsContext_android();
}

NSPI_END()