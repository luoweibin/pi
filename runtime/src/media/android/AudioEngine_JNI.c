/*
 * Copyright (C) 2015 Zhang Rui <bbcallen@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * https://github.com/Bilibili/jni4android
 * This file is automatically generated by jni4android, do not modify.
 */

#include "AudioEngine_JNI.h"

typedef struct J4AC_com_lemon_pi_AudioEngine {
    jclass id;

    jmethodID constructor_AudioEngine;
    jmethodID method_start;
    jmethodID method_stop;
    jmethodID method_preloadBackgroundMusic;
    jmethodID method_backgroundMusicSetLoop;
    jmethodID method_playBackgroundMusic;
    jmethodID method_pauseBackgroundMusic;
    jmethodID method_resumeBackgroundMusic;
    jmethodID method_rewindBackgroundMusic;
    jmethodID method_stopBackgroundMusic;
    jmethodID method_releaseBackgroundMusic;
    jmethodID method_preloadSound;
    jmethodID method_createSoundId;
    jmethodID method_soundSetLoop;
    jmethodID method_playSound;
    jmethodID method_pauseSound;
    jmethodID method_resumeSound;
    jmethodID method_stopSound;
    jmethodID method_destroySoundId;
    jmethodID method_unloadSound;
    jmethodID method_stopAllSound;
    jmethodID method_unloadAllSound;
} J4AC_com_lemon_pi_AudioEngine;
static J4AC_com_lemon_pi_AudioEngine class_J4AC_com_lemon_pi_AudioEngine;

jobject J4AC_com_lemon_pi_AudioEngine__AudioEngine(JNIEnv *env)
{
    return (*env)->NewObject(env, class_J4AC_com_lemon_pi_AudioEngine.id, class_J4AC_com_lemon_pi_AudioEngine.constructor_AudioEngine);
}

jobject J4AC_com_lemon_pi_AudioEngine__AudioEngine__catchAll(JNIEnv *env)
{
    jobject ret_object = J4AC_com_lemon_pi_AudioEngine__AudioEngine(env);
    if (J4A_ExceptionCheck__catchAll(env) || !ret_object) {
        return NULL;
    }

    return ret_object;
}

jobject J4AC_com_lemon_pi_AudioEngine__AudioEngine__asGlobalRef__catchAll(JNIEnv *env)
{
    jobject ret_object   = NULL;
    jobject local_object = J4AC_com_lemon_pi_AudioEngine__AudioEngine__catchAll(env);
    if (J4A_ExceptionCheck__catchAll(env) || !local_object) {
        ret_object = NULL;
        goto fail;
    }

    ret_object = J4A_NewGlobalRef__catchAll(env, local_object);
    if (!ret_object) {
        ret_object = NULL;
        goto fail;
    }

fail:
    J4A_DeleteLocalRef__p(env, &local_object);
    return ret_object;
}

jboolean J4AC_com_lemon_pi_AudioEngine__start(JNIEnv *env, jobject thiz)
{
    return (*env)->CallBooleanMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_start);
}

jboolean J4AC_com_lemon_pi_AudioEngine__start__catchAll(JNIEnv *env, jobject thiz)
{
    jboolean ret_value = J4AC_com_lemon_pi_AudioEngine__start(env, thiz);
    if (J4A_ExceptionCheck__catchAll(env)) {
        return false;
    }

    return ret_value;
}

void J4AC_com_lemon_pi_AudioEngine__stop(JNIEnv *env, jobject thiz)
{
    (*env)->CallVoidMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_stop);
}

void J4AC_com_lemon_pi_AudioEngine__stop__catchAll(JNIEnv *env, jobject thiz)
{
    J4AC_com_lemon_pi_AudioEngine__stop(env, thiz);
    J4A_ExceptionCheck__catchAll(env);
}

jboolean J4AC_com_lemon_pi_AudioEngine__preloadBackgroundMusic(JNIEnv *env, jobject thiz, jstring filePath)
{
    return (*env)->CallBooleanMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_preloadBackgroundMusic, filePath);
}

jboolean J4AC_com_lemon_pi_AudioEngine__preloadBackgroundMusic__catchAll(JNIEnv *env, jobject thiz, jstring filePath)
{
    jboolean ret_value = J4AC_com_lemon_pi_AudioEngine__preloadBackgroundMusic(env, thiz, filePath);
    if (J4A_ExceptionCheck__catchAll(env)) {
        return false;
    }

    return ret_value;
}

jboolean J4AC_com_lemon_pi_AudioEngine__preloadBackgroundMusic__withCString(JNIEnv *env, jobject thiz, const char *filePath_cstr__)
{
    jboolean ret_value = false;
    jstring filePath = NULL;

    filePath = (*env)->NewStringUTF(env, filePath_cstr__);
    if (J4A_ExceptionCheck__throwAny(env) || !filePath)
        goto fail;

    ret_value = J4AC_com_lemon_pi_AudioEngine__preloadBackgroundMusic(env, thiz, filePath);
    if (J4A_ExceptionCheck__throwAny(env)) {
        ret_value = false;
        goto fail;
    }

fail:
    J4A_DeleteLocalRef__p(env, &filePath);
    return ret_value;
}

jboolean J4AC_com_lemon_pi_AudioEngine__preloadBackgroundMusic__withCString__catchAll(JNIEnv *env, jobject thiz, const char *filePath_cstr__)
{
    jboolean ret_value = false;
    jstring filePath = NULL;

    filePath = (*env)->NewStringUTF(env, filePath_cstr__);
    if (J4A_ExceptionCheck__catchAll(env) || !filePath)
        goto fail;

    ret_value = J4AC_com_lemon_pi_AudioEngine__preloadBackgroundMusic__catchAll(env, thiz, filePath);
    if (J4A_ExceptionCheck__catchAll(env)) {
        ret_value = false;
        goto fail;
    }

fail:
    J4A_DeleteLocalRef__p(env, &filePath);
    return ret_value;
}

void J4AC_com_lemon_pi_AudioEngine__backgroundMusicSetLoop(JNIEnv *env, jobject thiz, jboolean loop)
{
    (*env)->CallVoidMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_backgroundMusicSetLoop, loop);
}

void J4AC_com_lemon_pi_AudioEngine__backgroundMusicSetLoop__catchAll(JNIEnv *env, jobject thiz, jboolean loop)
{
    J4AC_com_lemon_pi_AudioEngine__backgroundMusicSetLoop(env, thiz, loop);
    J4A_ExceptionCheck__catchAll(env);
}

void J4AC_com_lemon_pi_AudioEngine__playBackgroundMusic(JNIEnv *env, jobject thiz, jstring filePath)
{
    (*env)->CallVoidMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_playBackgroundMusic, filePath);
}

void J4AC_com_lemon_pi_AudioEngine__playBackgroundMusic__catchAll(JNIEnv *env, jobject thiz, jstring filePath)
{
    J4AC_com_lemon_pi_AudioEngine__playBackgroundMusic(env, thiz, filePath);
    J4A_ExceptionCheck__catchAll(env);
}

void J4AC_com_lemon_pi_AudioEngine__playBackgroundMusic__withCString(JNIEnv *env, jobject thiz, const char *filePath_cstr__)
{
    jstring filePath = NULL;

    filePath = (*env)->NewStringUTF(env, filePath_cstr__);
    if (J4A_ExceptionCheck__throwAny(env) || !filePath)
        goto fail;

    J4AC_com_lemon_pi_AudioEngine__playBackgroundMusic(env, thiz, filePath);

fail:
    J4A_DeleteLocalRef__p(env, &filePath);
}

void J4AC_com_lemon_pi_AudioEngine__playBackgroundMusic__withCString__catchAll(JNIEnv *env, jobject thiz, const char *filePath_cstr__)
{
    jstring filePath = NULL;

    filePath = (*env)->NewStringUTF(env, filePath_cstr__);
    if (J4A_ExceptionCheck__catchAll(env) || !filePath)
        goto fail;

    J4AC_com_lemon_pi_AudioEngine__playBackgroundMusic__catchAll(env, thiz, filePath);

fail:
    J4A_DeleteLocalRef__p(env, &filePath);
}

void J4AC_com_lemon_pi_AudioEngine__pauseBackgroundMusic(JNIEnv *env, jobject thiz)
{
    (*env)->CallVoidMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_pauseBackgroundMusic);
}

void J4AC_com_lemon_pi_AudioEngine__pauseBackgroundMusic__catchAll(JNIEnv *env, jobject thiz)
{
    J4AC_com_lemon_pi_AudioEngine__pauseBackgroundMusic(env, thiz);
    J4A_ExceptionCheck__catchAll(env);
}

void J4AC_com_lemon_pi_AudioEngine__resumeBackgroundMusic(JNIEnv *env, jobject thiz)
{
    (*env)->CallVoidMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_resumeBackgroundMusic);
}

void J4AC_com_lemon_pi_AudioEngine__resumeBackgroundMusic__catchAll(JNIEnv *env, jobject thiz)
{
    J4AC_com_lemon_pi_AudioEngine__resumeBackgroundMusic(env, thiz);
    J4A_ExceptionCheck__catchAll(env);
}

void J4AC_com_lemon_pi_AudioEngine__rewindBackgroundMusic(JNIEnv *env, jobject thiz)
{
    (*env)->CallVoidMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_rewindBackgroundMusic);
}

void J4AC_com_lemon_pi_AudioEngine__rewindBackgroundMusic__catchAll(JNIEnv *env, jobject thiz)
{
    J4AC_com_lemon_pi_AudioEngine__rewindBackgroundMusic(env, thiz);
    J4A_ExceptionCheck__catchAll(env);
}

void J4AC_com_lemon_pi_AudioEngine__stopBackgroundMusic(JNIEnv *env, jobject thiz)
{
    (*env)->CallVoidMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_stopBackgroundMusic);
}

void J4AC_com_lemon_pi_AudioEngine__stopBackgroundMusic__catchAll(JNIEnv *env, jobject thiz)
{
    J4AC_com_lemon_pi_AudioEngine__stopBackgroundMusic(env, thiz);
    J4A_ExceptionCheck__catchAll(env);
}

void J4AC_com_lemon_pi_AudioEngine__releaseBackgroundMusic(JNIEnv *env, jobject thiz)
{
    (*env)->CallVoidMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_releaseBackgroundMusic);
}

void J4AC_com_lemon_pi_AudioEngine__releaseBackgroundMusic__catchAll(JNIEnv *env, jobject thiz)
{
    J4AC_com_lemon_pi_AudioEngine__releaseBackgroundMusic(env, thiz);
    J4A_ExceptionCheck__catchAll(env);
}

jboolean J4AC_com_lemon_pi_AudioEngine__preloadSound(JNIEnv *env, jobject thiz, jstring filePath)
{
    return (*env)->CallBooleanMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_preloadSound, filePath);
}

jboolean J4AC_com_lemon_pi_AudioEngine__preloadSound__catchAll(JNIEnv *env, jobject thiz, jstring filePath)
{
    jboolean ret_value = J4AC_com_lemon_pi_AudioEngine__preloadSound(env, thiz, filePath);
    if (J4A_ExceptionCheck__catchAll(env)) {
        return false;
    }

    return ret_value;
}

jboolean J4AC_com_lemon_pi_AudioEngine__preloadSound__withCString(JNIEnv *env, jobject thiz, const char *filePath_cstr__)
{
    jboolean ret_value = false;
    jstring filePath = NULL;

    filePath = (*env)->NewStringUTF(env, filePath_cstr__);
    if (J4A_ExceptionCheck__throwAny(env) || !filePath)
        goto fail;

    ret_value = J4AC_com_lemon_pi_AudioEngine__preloadSound(env, thiz, filePath);
    if (J4A_ExceptionCheck__throwAny(env)) {
        ret_value = false;
        goto fail;
    }

fail:
    J4A_DeleteLocalRef__p(env, &filePath);
    return ret_value;
}

jboolean J4AC_com_lemon_pi_AudioEngine__preloadSound__withCString__catchAll(JNIEnv *env, jobject thiz, const char *filePath_cstr__)
{
    jboolean ret_value = false;
    jstring filePath = NULL;

    filePath = (*env)->NewStringUTF(env, filePath_cstr__);
    if (J4A_ExceptionCheck__catchAll(env) || !filePath)
        goto fail;

    ret_value = J4AC_com_lemon_pi_AudioEngine__preloadSound__catchAll(env, thiz, filePath);
    if (J4A_ExceptionCheck__catchAll(env)) {
        ret_value = false;
        goto fail;
    }

fail:
    J4A_DeleteLocalRef__p(env, &filePath);
    return ret_value;
}

jint J4AC_com_lemon_pi_AudioEngine__createSoundId(JNIEnv *env, jobject thiz, jstring filePath)
{
    return (*env)->CallIntMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_createSoundId, filePath);
}

jint J4AC_com_lemon_pi_AudioEngine__createSoundId__catchAll(JNIEnv *env, jobject thiz, jstring filePath)
{
    jint ret_value = J4AC_com_lemon_pi_AudioEngine__createSoundId(env, thiz, filePath);
    if (J4A_ExceptionCheck__catchAll(env)) {
        return 0;
    }

    return ret_value;
}

jint J4AC_com_lemon_pi_AudioEngine__createSoundId__withCString(JNIEnv *env, jobject thiz, const char *filePath_cstr__)
{
    jint ret_value = 0;
    jstring filePath = NULL;

    filePath = (*env)->NewStringUTF(env, filePath_cstr__);
    if (J4A_ExceptionCheck__throwAny(env) || !filePath)
        goto fail;

    ret_value = J4AC_com_lemon_pi_AudioEngine__createSoundId(env, thiz, filePath);
    if (J4A_ExceptionCheck__throwAny(env)) {
        ret_value = 0;
        goto fail;
    }

fail:
    J4A_DeleteLocalRef__p(env, &filePath);
    return ret_value;
}

jint J4AC_com_lemon_pi_AudioEngine__createSoundId__withCString__catchAll(JNIEnv *env, jobject thiz, const char *filePath_cstr__)
{
    jint ret_value = 0;
    jstring filePath = NULL;

    filePath = (*env)->NewStringUTF(env, filePath_cstr__);
    if (J4A_ExceptionCheck__catchAll(env) || !filePath)
        goto fail;

    ret_value = J4AC_com_lemon_pi_AudioEngine__createSoundId__catchAll(env, thiz, filePath);
    if (J4A_ExceptionCheck__catchAll(env)) {
        ret_value = 0;
        goto fail;
    }

fail:
    J4A_DeleteLocalRef__p(env, &filePath);
    return ret_value;
}

void J4AC_com_lemon_pi_AudioEngine__soundSetLoop(JNIEnv *env, jobject thiz, jint soundId, jboolean loop)
{
    (*env)->CallVoidMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_soundSetLoop, soundId, loop);
}

void J4AC_com_lemon_pi_AudioEngine__soundSetLoop__catchAll(JNIEnv *env, jobject thiz, jint soundId, jboolean loop)
{
    J4AC_com_lemon_pi_AudioEngine__soundSetLoop(env, thiz, soundId, loop);
    J4A_ExceptionCheck__catchAll(env);
}

void J4AC_com_lemon_pi_AudioEngine__playSound(JNIEnv *env, jobject thiz, jint soundId)
{
    (*env)->CallVoidMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_playSound, soundId);
}

void J4AC_com_lemon_pi_AudioEngine__playSound__catchAll(JNIEnv *env, jobject thiz, jint soundId)
{
    J4AC_com_lemon_pi_AudioEngine__playSound(env, thiz, soundId);
    J4A_ExceptionCheck__catchAll(env);
}

void J4AC_com_lemon_pi_AudioEngine__pauseSound(JNIEnv *env, jobject thiz, jint soundId)
{
    (*env)->CallVoidMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_pauseSound, soundId);
}

void J4AC_com_lemon_pi_AudioEngine__pauseSound__catchAll(JNIEnv *env, jobject thiz, jint soundId)
{
    J4AC_com_lemon_pi_AudioEngine__pauseSound(env, thiz, soundId);
    J4A_ExceptionCheck__catchAll(env);
}

void J4AC_com_lemon_pi_AudioEngine__resumeSound(JNIEnv *env, jobject thiz, jint soundId)
{
    (*env)->CallVoidMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_resumeSound, soundId);
}

void J4AC_com_lemon_pi_AudioEngine__resumeSound__catchAll(JNIEnv *env, jobject thiz, jint soundId)
{
    J4AC_com_lemon_pi_AudioEngine__resumeSound(env, thiz, soundId);
    J4A_ExceptionCheck__catchAll(env);
}

void J4AC_com_lemon_pi_AudioEngine__stopSound(JNIEnv *env, jobject thiz, jint soundId)
{
    (*env)->CallVoidMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_stopSound, soundId);
}

void J4AC_com_lemon_pi_AudioEngine__stopSound__catchAll(JNIEnv *env, jobject thiz, jint soundId)
{
    J4AC_com_lemon_pi_AudioEngine__stopSound(env, thiz, soundId);
    J4A_ExceptionCheck__catchAll(env);
}

void J4AC_com_lemon_pi_AudioEngine__destroySoundId(JNIEnv *env, jobject thiz, jint soundId)
{
    (*env)->CallVoidMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_destroySoundId, soundId);
}

void J4AC_com_lemon_pi_AudioEngine__destroySoundId__catchAll(JNIEnv *env, jobject thiz, jint soundId)
{
    J4AC_com_lemon_pi_AudioEngine__destroySoundId(env, thiz, soundId);
    J4A_ExceptionCheck__catchAll(env);
}

void J4AC_com_lemon_pi_AudioEngine__unloadSound(JNIEnv *env, jobject thiz, jstring filePath)
{
    (*env)->CallVoidMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_unloadSound, filePath);
}

void J4AC_com_lemon_pi_AudioEngine__unloadSound__catchAll(JNIEnv *env, jobject thiz, jstring filePath)
{
    J4AC_com_lemon_pi_AudioEngine__unloadSound(env, thiz, filePath);
    J4A_ExceptionCheck__catchAll(env);
}

void J4AC_com_lemon_pi_AudioEngine__unloadSound__withCString(JNIEnv *env, jobject thiz, const char *filePath_cstr__)
{
    jstring filePath = NULL;

    filePath = (*env)->NewStringUTF(env, filePath_cstr__);
    if (J4A_ExceptionCheck__throwAny(env) || !filePath)
        goto fail;

    J4AC_com_lemon_pi_AudioEngine__unloadSound(env, thiz, filePath);

fail:
    J4A_DeleteLocalRef__p(env, &filePath);
}

void J4AC_com_lemon_pi_AudioEngine__unloadSound__withCString__catchAll(JNIEnv *env, jobject thiz, const char *filePath_cstr__)
{
    jstring filePath = NULL;

    filePath = (*env)->NewStringUTF(env, filePath_cstr__);
    if (J4A_ExceptionCheck__catchAll(env) || !filePath)
        goto fail;

    J4AC_com_lemon_pi_AudioEngine__unloadSound__catchAll(env, thiz, filePath);

fail:
    J4A_DeleteLocalRef__p(env, &filePath);
}

void J4AC_com_lemon_pi_AudioEngine__stopAllSound(JNIEnv *env, jobject thiz)
{
    (*env)->CallVoidMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_stopAllSound);
}

void J4AC_com_lemon_pi_AudioEngine__stopAllSound__catchAll(JNIEnv *env, jobject thiz)
{
    J4AC_com_lemon_pi_AudioEngine__stopAllSound(env, thiz);
    J4A_ExceptionCheck__catchAll(env);
}

void J4AC_com_lemon_pi_AudioEngine__unloadAllSound(JNIEnv *env, jobject thiz)
{
    (*env)->CallVoidMethod(env, thiz, class_J4AC_com_lemon_pi_AudioEngine.method_unloadAllSound);
}

void J4AC_com_lemon_pi_AudioEngine__unloadAllSound__catchAll(JNIEnv *env, jobject thiz)
{
    J4AC_com_lemon_pi_AudioEngine__unloadAllSound(env, thiz);
    J4A_ExceptionCheck__catchAll(env);
}

int J4A_loadClass__J4AC_com_lemon_pi_AudioEngine(JNIEnv *env)
{
    int         ret                   = -1;
    const char *J4A_UNUSED(name)      = NULL;
    const char *J4A_UNUSED(sign)      = NULL;
    jclass      J4A_UNUSED(class_id)  = NULL;
    int         J4A_UNUSED(api_level) = 0;

    if (class_J4AC_com_lemon_pi_AudioEngine.id != NULL)
        return 0;

    sign = "com/lemon/pi/AudioEngine";
    class_J4AC_com_lemon_pi_AudioEngine.id = J4A_FindClass__asGlobalRef__catchAll(env, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.id == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "<init>";
    sign     = "()V";
    class_J4AC_com_lemon_pi_AudioEngine.constructor_AudioEngine = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.constructor_AudioEngine == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "start";
    sign     = "()Z";
    class_J4AC_com_lemon_pi_AudioEngine.method_start = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_start == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "stop";
    sign     = "()V";
    class_J4AC_com_lemon_pi_AudioEngine.method_stop = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_stop == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "preloadBackgroundMusic";
    sign     = "(Ljava/lang/String;)Z";
    class_J4AC_com_lemon_pi_AudioEngine.method_preloadBackgroundMusic = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_preloadBackgroundMusic == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "backgroundMusicSetLoop";
    sign     = "(Z)V";
    class_J4AC_com_lemon_pi_AudioEngine.method_backgroundMusicSetLoop = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_backgroundMusicSetLoop == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "playBackgroundMusic";
    sign     = "(Ljava/lang/String;)V";
    class_J4AC_com_lemon_pi_AudioEngine.method_playBackgroundMusic = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_playBackgroundMusic == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "pauseBackgroundMusic";
    sign     = "()V";
    class_J4AC_com_lemon_pi_AudioEngine.method_pauseBackgroundMusic = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_pauseBackgroundMusic == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "resumeBackgroundMusic";
    sign     = "()V";
    class_J4AC_com_lemon_pi_AudioEngine.method_resumeBackgroundMusic = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_resumeBackgroundMusic == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "rewindBackgroundMusic";
    sign     = "()V";
    class_J4AC_com_lemon_pi_AudioEngine.method_rewindBackgroundMusic = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_rewindBackgroundMusic == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "stopBackgroundMusic";
    sign     = "()V";
    class_J4AC_com_lemon_pi_AudioEngine.method_stopBackgroundMusic = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_stopBackgroundMusic == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "releaseBackgroundMusic";
    sign     = "()V";
    class_J4AC_com_lemon_pi_AudioEngine.method_releaseBackgroundMusic = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_releaseBackgroundMusic == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "preloadSound";
    sign     = "(Ljava/lang/String;)Z";
    class_J4AC_com_lemon_pi_AudioEngine.method_preloadSound = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_preloadSound == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "createSoundId";
    sign     = "(Ljava/lang/String;)I";
    class_J4AC_com_lemon_pi_AudioEngine.method_createSoundId = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_createSoundId == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "soundSetLoop";
    sign     = "(IZ)V";
    class_J4AC_com_lemon_pi_AudioEngine.method_soundSetLoop = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_soundSetLoop == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "playSound";
    sign     = "(I)V";
    class_J4AC_com_lemon_pi_AudioEngine.method_playSound = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_playSound == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "pauseSound";
    sign     = "(I)V";
    class_J4AC_com_lemon_pi_AudioEngine.method_pauseSound = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_pauseSound == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "resumeSound";
    sign     = "(I)V";
    class_J4AC_com_lemon_pi_AudioEngine.method_resumeSound = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_resumeSound == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "stopSound";
    sign     = "(I)V";
    class_J4AC_com_lemon_pi_AudioEngine.method_stopSound = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_stopSound == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "destroySoundId";
    sign     = "(I)V";
    class_J4AC_com_lemon_pi_AudioEngine.method_destroySoundId = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_destroySoundId == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "unloadSound";
    sign     = "(Ljava/lang/String;)V";
    class_J4AC_com_lemon_pi_AudioEngine.method_unloadSound = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_unloadSound == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "stopAllSound";
    sign     = "()V";
    class_J4AC_com_lemon_pi_AudioEngine.method_stopAllSound = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_stopAllSound == NULL)
        goto fail;

    class_id = class_J4AC_com_lemon_pi_AudioEngine.id;
    name     = "unloadAllSound";
    sign     = "()V";
    class_J4AC_com_lemon_pi_AudioEngine.method_unloadAllSound = J4A_GetMethodID__catchAll(env, class_id, name, sign);
    if (class_J4AC_com_lemon_pi_AudioEngine.method_unloadAllSound == NULL)
        goto fail;

    J4A_ALOGD("J4ALoader: OK: '%s' loaded\n", "com.lemon.pi.AudioEngine");
    ret = 0;
fail:
    return ret;
}
