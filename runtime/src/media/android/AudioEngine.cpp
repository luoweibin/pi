/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 caiwenqiang     2017.7.10   0.1     Create
 ******************************************************************************/
#include <pi/Media.h>

extern "C" {
#include "AudioEngine_JNI.h"
}

NSPI_BEGIN()

class AndroidAudioEngine : public iAudioEngine
{
private:
    bool mIsStarted;

    jobject jAudioEngine;
    
public:
    AndroidAudioEngine(int backend) :
    mIsStarted(false)
    {
        jAudioEngine = J4AC_AudioEngine__AudioEngine__asGlobalRef__catchAll(piAttachJVM());
    }
    
    virtual ~AndroidAudioEngine()
    {
        Stop();
        J4A_DeleteGlobalRef(piAttachJVM(), jAudioEngine);
    }
    
    virtual bool Start()
    {
        if (!mIsStarted)
        {
            mIsStarted = J4AC_AudioEngine__start__catchAll(piAttachJVM(), jAudioEngine);
        }
        
        return mIsStarted;
    }
    
    // stop and release
    virtual void Stop()
    {
        if (mIsStarted)
        {
            J4AC_AudioEngine__stop__catchAll(piAttachJVM(), jAudioEngine);
        }
    }
    
    // music
    virtual bool PreloadBackgroundMusic(const std::string& filePath)
    {
        piAssert(mIsStarted, false);
        return J4AC_AudioEngine__preloadBackgroundMusic__withCString__catchAll(piAttachJVM(), jAudioEngine, filePath.c_str());
    }
    
    virtual void BackgroundMusicSetLoop(bool loop)
    {
        J4AC_AudioEngine__backgroundMusicSetLoop__catchAll(piAttachJVM(), jAudioEngine, loop);
    }
    
    virtual void PlayBackgroundMusic(const std::string& filePath)
    {
        piAssert(mIsStarted, ;);
        J4AC_AudioEngine__playBackgroundMusic__withCString__catchAll(piAttachJVM(), jAudioEngine, filePath.c_str());
    }
    
    virtual void PauseBackgroundMusic()
    {
        J4AC_AudioEngine__pauseBackgroundMusic__catchAll(piAttachJVM(), jAudioEngine);
    }
    
    virtual void ResumeBackgroundMusic()
    {
        J4AC_AudioEngine__resumeBackgroundMusic__catchAll(piAttachJVM(), jAudioEngine);
    }
    
    virtual void RewindBackgroundMusic()
    {
        J4AC_AudioEngine__rewindBackgroundMusic__catchAll(piAttachJVM(), jAudioEngine);
    }
    
    virtual void StopBackgroundMusic()
    {
        J4AC_AudioEngine__stopBackgroundMusic__catchAll(piAttachJVM(), jAudioEngine);
    }
    
    virtual void ReleaseBackgroundMusic()
    {
        J4AC_AudioEngine__releaseBackgroundMusic__catchAll(piAttachJVM(), jAudioEngine);
    }
    
    // sound
    virtual bool PreloadSound(const std::string& filePath)
    {
        piAssert(mIsStarted, false);
        return J4AC_AudioEngine__preloadSound__withCString__catchAll(piAttachJVM(), jAudioEngine, filePath.c_str());
    }
    
    virtual int CreateSoundId(const std::string& filePath)
    {
        piAssert(mIsStarted, 0);
        return J4AC_AudioEngine__createSoundId__withCString__catchAll(piAttachJVM(), jAudioEngine, filePath.c_str());
    }
    
    virtual void SoundSetLoop(int soundId, bool loop)
    {
        J4AC_AudioEngine__soundSetLoop__catchAll(piAttachJVM(), jAudioEngine, soundId, loop);
    }
    
    virtual void PlaySound(int soundId)
    {
        piAssert(mIsStarted, ;);
        J4AC_AudioEngine__playSound__catchAll(piAttachJVM(), jAudioEngine, soundId);
    }
    
    virtual void PauseSound(int soundId)
    {
        J4AC_AudioEngine__pauseSound__catchAll(piAttachJVM(), jAudioEngine, soundId);
    }
    
    virtual void ResumeSound(int soundId)
    {
        J4AC_AudioEngine__resumeSound__catchAll(piAttachJVM(), jAudioEngine, soundId);
    }
    
    virtual void StopSound(int soundId)
    {
        J4AC_AudioEngine__stopSound__catchAll(piAttachJVM(), jAudioEngine, soundId);
    }
    
    virtual void DestroySoundId(int soundId)
    {
        J4AC_AudioEngine__destroySoundId__catchAll(piAttachJVM(), jAudioEngine, soundId);
    }
    
    virtual void UnloadSound(const std::string& filePath)
    {
        J4AC_AudioEngine__unloadSound__withCString__catchAll(piAttachJVM(), jAudioEngine, filePath.c_str());
    }
    
    virtual void StopAllSound()
    {
        J4AC_AudioEngine__stopAllSound__catchAll(piAttachJVM(), jAudioEngine);
    }
    
    virtual void UnloadAllSound()
    {
        J4AC_AudioEngine__unloadAllSound__catchAll(piAttachJVM(), jAudioEngine);
    }
};

iAudioEngine* CreateAudioEngine(int backend)
{
    return new AndroidAudioEngine(backend);
}

void piInitAudioEngine()
{
    int ret = J4A_loadClass__J4AC_AudioEngine(piAttachJVM());
    if (ret)
    {
        PILOGE(PI_MEDIA_TAG, "Load java class AudioEngine failed!");
    }
}

NSPI_END()
