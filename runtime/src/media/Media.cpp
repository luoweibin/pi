/*******************************************************************************
 See copyright notice in LICENSE.

 History:
 caiwenqiang     2017.7.12   0.1     Create
 ******************************************************************************/
#include <pi/Media.h>

NSPI_BEGIN()

void piInitAudioEngine();

static bool g_MediaInit = false;

void piInitMedia()
{
    if (!g_MediaInit)
    {
        piInitAudioEngine();
        g_MediaInit = true;
    }
}

NSPI_END()
