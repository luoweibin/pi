/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2015.3.30   0.1     Create
 ******************************************************************************/
#ifndef PI_MEDIA_MEDIACODEC_H
#define PI_MEDIA_MEDIACODEC_H

#include <pi/media/MediaFrame.h>

namespace nspi
{
    enum
    {
        eMediaCodec_Idle,
        eMediaCodec_Open,
        eMediaCodec_Finish,
        eMediaCodec_Error,
        eMediaCodec_Closed,
    };
    
    struct iMediaCodec : public iRefObject
    {
        virtual ~iMediaCodec() {}
        
        virtual void SetOutputPixelFormat(int format) = 0;
        
        virtual int GetOutputPixelFormat() const = 0;
        
        virtual bool Open(const std::string& url) = 0;
        
        virtual void Close() = 0;
        
        virtual void Reset() = 0;
        
        /**
         * 在解码缓冲区和输入流中查找指定的位置并将当前进度移动到该位置。
         * \param position 单位毫秒
         */
        virtual void Seek(int64_t position) = 0;
        
        /**
         * 进行demux和decode工作
         * @return true表示继续工作，false表示已完成所有工作
         */
        virtual bool ReadFrame() = 0;
        
        virtual void SetAudioSampleRate(int32_t sampleRate) = 0;
        
        virtual int32_t GetAudioSampleRate() const = 0;
        
        virtual int32_t GetVideoFPS() const = 0;
                
        virtual int64_t GetDuration() const = 0;
        
        /**
         * 弹出音频解码缓冲区中的第一帧音频帧
         */
        virtual iAudioFrame* PopAudioFrame() = 0;
        
        /**
         * 取得音频解码缓冲区中的第一帧音频，不弹出。
         */
        virtual iAudioFrame* PeekAudioFrame() const = 0;
        
        /**
         * 获取音频缓冲进度，单位毫秒。
         * 注意：不要拿buffering position来计算缓冲区数据长度，使用GetAudioBufferingLength();
         */
        virtual int64_t GetAudioBufferingPosition() const = 0;
        
        /**
         * 获取音频解码缓冲区数据长度，单位毫秒。
         */
        virtual int64_t GetAudioBufferingLength() const = 0;
        
        virtual int32_t GetAudioTrack() const = 0;
        
        /**
         * 弹出视频解码缓冲区中的第一帧视频帧
         */
        virtual iVideoFrame* PopVideoFrame() = 0;
        
        /**
         * 取得视频解码缓冲区中的第一帧视频帧，不弹出。
         */
        virtual iVideoFrame* PeekVideoFrame() const = 0;
        
        /**
         * 获取视频缓冲进度，单位毫秒。
         * 注意：不要拿buffering position来计算缓冲区数据长度，使用GetVideoBufferingLength();
         */
        virtual int64_t GetVideoBufferingPosition() const = 0;
        
        /**
         * 获取视频解码缓冲区数据长度，单位毫秒。
         */
        virtual int64_t GetVideoBufferingLength() const = 0;
        
        virtual int32_t GetVideoTrack() const = 0;
        
        virtual int GetState() const = 0;
        
        virtual bool IsVideoFinish() const = 0;
        
        virtual bool IsAudioFinish() const = 0;
    };
    
    
    enum
    {
        eMediaCodec_FFmpeg,
        eMediaCodec_Hardware,
    };
    
    iMediaCodec* CreateMediaCodec(int backend);
    
    
    /**
     * 用于视频截屏
     */
    
    struct iVideoPreview : public iRefObject
    {
        virtual ~iVideoPreview() {}
                
        virtual void Reset() = 0;
        
        virtual bool Open(const std::string& uri) = 0;
        
        virtual void Close() = 0;
        
        /**
         * \param position 单位毫秒
         */
        virtual void Seek(int64_t position) = 0;
        
        virtual iVideoFrame* GetFrame() const = 0;
    };
    
    iVideoPreview* CreateVideoPreview();
}

#endif







































