/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 caiwenqiang     2017.6.29   0.1     Create
 ******************************************************************************/
#include <pi/Media.h>

#import <AVFoundation/AVFoundation.h>

#include <map>

#include "SoundEngine.h"

NSPI_BEGIN()

class BGMController : public iRefObject
{
private:
    std::string mBgmFilePath;
    AVAudioPlayer *mMusicPlayer;
    bool mBgmPaused;
    bool mLoop;
    
public:
    BGMController() :
    mMusicPlayer(nil), mBgmPaused(false), mLoop(false)
    {
        
    }
    
    virtual ~BGMController()
    {
        mMusicPlayer = nil;
    }
    
    virtual bool PreloadBackgroundMusic(const std::string& filePath)
    {
        if (mBgmFilePath.compare(filePath) != 0) {
            NSError *error = nil;
            NSString *path = [NSString stringWithUTF8String:filePath.c_str()];
            AVAudioPlayer *playerTmp = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:&error];
            if (error == nil)
            {
                BOOL ret = [playerTmp prepareToPlay];
                if (ret)
                {
                    mBgmFilePath = filePath;
                    mMusicPlayer = playerTmp;
                    mBgmPaused = false;
                }
                else
                {
                    PILOGE(PI_MEDIA_TAG, "AVAudioPlayer prepareToPlay failed!");
                }
            }
            else
            {
                PILOGE(PI_MEDIA_TAG, "Create AVAudioPlayer error:%s", [[error localizedDescription] UTF8String]);
            }
        }
        
        return mMusicPlayer != nil;
    }
    
    virtual void BackgroundMusicSetLoop(bool loop)
    {
        mLoop = loop;
        if (mMusicPlayer != nil)
        {
            [mMusicPlayer setNumberOfLoops:loop ? -1 : 0];
        }
    }
    
    virtual void PlayBackgroundMusic(const std::string& filePath)
    {
        if (PreloadBackgroundMusic(filePath))
        {
            [mMusicPlayer setNumberOfLoops:mLoop ? -1 : 0];
            [mMusicPlayer setCurrentTime:0];
            // player.delegate
            [mMusicPlayer play];
            mBgmPaused = false;
        }
    }
    
    virtual void PauseBackgroundMusic()
    {
        if (mMusicPlayer != nil && !mBgmPaused)
        {
            [mMusicPlayer pause];
            mBgmPaused = true;
        }
    }
    
    virtual void ResumeBackgroundMusic()
    {
        if (mMusicPlayer != nil && mBgmPaused)
        {
            [mMusicPlayer play];
            mBgmPaused = false;
        }
    }
    
    virtual void RewindBackgroundMusic()
    {
        if (mMusicPlayer != nil)
        {
            [mMusicPlayer setCurrentTime:0];
            [mMusicPlayer play];
            mBgmPaused = false;
        }
    }
    
    virtual void StopBackgroundMusic()
    {
        if (mMusicPlayer != nil)
        {
            [mMusicPlayer stop];
            mBgmPaused = false;
        }
    }
    
    virtual void ReleaseBackgroundMusic()
    {
        mBgmFilePath.clear();
        mMusicPlayer = nil;
    }
};

class AppleAudioEngine : public iAudioEngine
{
private:
    bool mIsStarted;
    
    SmartPtr<BGMController> mBGMController;
    SmartPtr<iSoundEngine> mSoundEngine;
    
public:
    AppleAudioEngine(int backend) :
    mIsStarted(false)
    {
        mBGMController = new BGMController();
        mSoundEngine = CreateSoundEngine(backend);
        piAssert(mSoundEngine.Ptr(), ;);
    }
    
    virtual ~AppleAudioEngine()
    {
        Stop();
    }
    
    virtual bool Start()
    {
        if (!mIsStarted)
        {
            mIsStarted = mSoundEngine->Start();
        }
        
        return mIsStarted;
    }
    
    // stop and release
    virtual void Stop()
    {
        if (mIsStarted)
        {
            StopBackgroundMusic();
            ReleaseBackgroundMusic();
            
            mSoundEngine->Stop();
            
            mIsStarted = false;
        }
    }
    
    // music
    virtual bool PreloadBackgroundMusic(const std::string& filePath)
    {
        piAssert(mIsStarted, false);
        return mBGMController->PreloadBackgroundMusic(filePath);
    }
    
    virtual void BackgroundMusicSetLoop(bool loop)
    {
        mBGMController->BackgroundMusicSetLoop(loop);
    }
    
    virtual void PlayBackgroundMusic(const std::string& filePath)
    {
        piAssert(mIsStarted, ;);
        mBGMController->PlayBackgroundMusic(filePath);
    }
    
    virtual void PauseBackgroundMusic()
    {
        mBGMController->PauseBackgroundMusic();
    }
    
    virtual void ResumeBackgroundMusic()
    {
        mBGMController->ResumeBackgroundMusic();
    }
    
    virtual void RewindBackgroundMusic()
    {
        mBGMController->RewindBackgroundMusic();
    }
    
    virtual void StopBackgroundMusic()
    {
        mBGMController->StopBackgroundMusic();
    }
    
    virtual void ReleaseBackgroundMusic()
    {
        mBGMController->ReleaseBackgroundMusic();
    }
    
    // sound
    virtual bool PreloadSound(const std::string& filePath)
    {
        piAssert(mIsStarted, false);
        return mSoundEngine->PreloadSound(filePath);
    }
    
    virtual int CreateSoundId(const std::string& filePath)
    {
        piAssert(mIsStarted, 0);
        return mSoundEngine->CreateSoundId(filePath);
    }
    
    virtual void SoundSetLoop(int soundId, bool loop)
    {
        mSoundEngine->SoundSetLoop(soundId, loop);
    }
    
    virtual void PlaySound(int soundId)
    {
        piAssert(mIsStarted, ;);
        mSoundEngine->PlaySound(soundId);
    }
    
    virtual void PauseSound(int soundId)
    {
        mSoundEngine->PauseSound(soundId);
    }
    
    virtual void ResumeSound(int soundId)
    {
        mSoundEngine->ResumeSound(soundId);
    }
    
    virtual void StopSound(int soundId)
    {
        mSoundEngine->StopSound(soundId);
    }
    
    virtual void DestroySoundId(int soundId)
    {
        mSoundEngine->DestroySoundId(soundId);
    }
    
    virtual void UnloadSound(const std::string& filePath)
    {
        mSoundEngine->UnloadSound(filePath);
    }
    
    virtual void StopAllSound()
    {
        mSoundEngine->StopAllSound();
    }
    
    virtual void UnloadAllSound()
    {
        mSoundEngine->UnloadAllSound();
    }
};

iAudioEngine* CreateAudioEngine(int backend)
{
    return new AppleAudioEngine(backend);
}

void piInitAudioEngine()
{
    
}

NSPI_END()
