/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 caiwenqiang     2017.6.30   0.1     Create
 ******************************************************************************/
#include "SoundEngineImpl.h"

#include <OpenAL/al.h>
#include <OpenAL/alc.h>

NSPI_BEGIN()

void* GetOpenALAudioData(const std::string& filePath, ALsizei *outDataSize, ALenum *outDataFormat, ALsizei *outSampleRate);

#define AssertAL(ret) piAssert(alGetError() == AL_NO_ERROR, ret)

static void ClearALError()
{
    while (alGetError() != AL_NO_ERROR);
}

class OpenALSoundPlayer : public iSoundPlayer
{
private:
    ALuint mBufferId;   // just use it
    ALuint mSourceId;
    bool mLoop;
    
public:
    OpenALSoundPlayer(const std::string& filePath, ALuint buffer, ALuint source) :
    iSoundPlayer(filePath), mBufferId(buffer), mSourceId(source), mLoop(false)
    {
        
    }
    
    virtual ~OpenALSoundPlayer()
    {
        
    }
    
    virtual void SetLoop(bool loop)
    {
        mLoop = loop;
        alSourcei(mSourceId, AL_LOOPING, mLoop);//Set looping
        AssertAL(;);
    }
    
    virtual void Play()
    {
        ClearALError();
        
        ALint state;
        alGetSourcei(mSourceId, AL_SOURCE_STATE, &state);
        if (state == AL_PLAYING) {
            alSourceStop(mSourceId);
        }
        alSourcei(mSourceId, AL_BUFFER, mBufferId);//Attach to sound
        alSourcef(mSourceId, AL_PITCH, 1.0f);//Set pitch
        alSourcei(mSourceId, AL_LOOPING, mLoop);//Set looping
        alSourcef(mSourceId, AL_GAIN, 1.0f);//Set gain/volume
        float sourcePosAL[] = {0.0f, 0.0f, 0.0f};//Set position - just using left and right panning
        alSourcefv(mSourceId, AL_POSITION, sourcePosAL);
        alSourcePlay(mSourceId);
        
        AssertAL(;);
    }
    
    virtual void Pause()
    {
        alSourcePause(mSourceId);
        AssertAL(;);
    }
    
    virtual void Resume()
    {
        // only resume a sound id that is paused
        ALint state;
        alGetSourcei(mSourceId, AL_SOURCE_STATE, &state);
        if (state == AL_PAUSED)
        {
            alSourcePlay(mSourceId);
        }
        AssertAL(;);
    }
    
    virtual void Stop()
    {
        alSourceStop(mSourceId);
        AssertAL(;);
    }
    
    virtual void Destroy()
    {
        Stop();
        alDeleteSources(1, &mSourceId);
        mSourceId = 0;
        mBufferId = 0;
    }
};

class OpenALSoundEngine : public SoundEngineImpl
{
private:
    ALCdevice *mALCDev;
    ALCcontext *mALCCtx;
    
    // buffer cache
    std::map<std::string, ALuint> mALBufferCache;
    
private:
    bool GetALBuffer(const std::string& filePath, ALuint *buffer)
    {
        bool ret = false;
        
        auto iter = mALBufferCache.find(filePath);
        if (iter == mALBufferCache.end())
        {
            // load sound
            ALvoid* data;
            ALenum  format;
            ALsizei size;
            ALsizei freq;
            data = GetOpenALAudioData(filePath, &size, &format, &freq);
            if (data)
            {
                // to ALData
                alGenBuffers(1, buffer);
                alBufferData(*buffer, format, data, size, freq);
                free(data);
                AssertAL(false);
                
                mALBufferCache.insert(std::make_pair(filePath, *buffer));
                ret = true;
            }
        }
        else
        {
            *buffer = (*iter).second;
            ret = true;
        }
        
        return ret;
    }
    
    virtual iSoundPlayer* CreateSoundPlayer(const std::string& filePath)
    {
        iSoundPlayer *player = nullptr;
        
        ALuint buffer = 0;
        bool ret = GetALBuffer(filePath, &buffer);
        if (ret)
        {
            ALuint source = 0;
            alGenSources(1, &source);
            AssertAL(nullptr);
            
            player = new OpenALSoundPlayer(filePath, buffer, source);
        }
        
        return player;
    }
    
    virtual bool StartInternal()
    {
        ClearALError();
        mALCDev = alcOpenDevice(nullptr);
        if (mALCDev == nullptr)
        {
            PILOGE(PI_MEDIA_TAG, "failed to open OpenAL device, error:%d.", alGetError());
            return false;
        }
        
        mALCCtx = alcCreateContext(mALCDev, nullptr);
        if (mALCCtx == nullptr)
        {
            PILOGE(PI_MEDIA_TAG, "failed to create OpenAL context, error:%d.", alGetError());
            goto quit0;
        }
        
        if (!alcMakeContextCurrent(mALCCtx))
        {
            PILOGE(PI_MEDIA_TAG, "failed to active OpenAL context, error:%d.", alGetError());
            goto quit1;
        }
        
        AssertAL(false);
        return true;
        
    quit1:
        alcDestroyContext(mALCCtx);
        mALCCtx = nullptr;
        
    quit0:
        alcCloseDevice(mALCDev);
        
        return false;
    }
    
    virtual void StopInternal()
    {
        if (mALCCtx != nullptr)
        {
            alcMakeContextCurrent(nullptr);
            alcDestroyContext(mALCCtx);
            mALCCtx = nullptr;
        }
        
        if (mALCDev != nullptr)
        {
            alcCloseDevice(mALCDev);
            mALCDev = nullptr;
        }
    }
    
public:
    OpenALSoundEngine() :
    mALCDev(nullptr), mALCCtx(nullptr)
    {
        
    }
    
    virtual bool PreloadSound(const std::string& filePath)
    {
        piAssert(SoundEngineImpl::PreloadSound(filePath), false);
        ALuint buffer = 0;
        return GetALBuffer(filePath, &buffer);
    }
    
    virtual void UnloadSound(const std::string& filePath)
    {
        SoundEngineImpl::UnloadSound(filePath);
        auto iter = mALBufferCache.find(filePath);
        if (iter != mALBufferCache.end())
        {
            ALuint buffer = (*iter).second;
            alDeleteBuffers(1, &buffer);
            mALBufferCache.erase(iter);
        }
    }
    
    virtual void UnloadAllSound()
    {
        SoundEngineImpl::UnloadAllSound();
        for (auto& it : mALBufferCache)
        {
            ALuint buffer = it.second;
            alDeleteBuffers(1, &buffer);
        }
        mALBufferCache.clear();
    }
};

iSoundEngine* CreateSoundEngineOpenAL()
{
    return new OpenALSoundEngine();
}

NSPI_END()
