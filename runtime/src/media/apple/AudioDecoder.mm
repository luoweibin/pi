/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 caiwenqiang     2017.6.30   0.1     Create
 ******************************************************************************/
#include <pi/Media.h>

#include <OpenAL/al.h>
#include <OpenAL/alc.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AudioToolbox/ExtendedAudioFile.h>

NSPI_BEGIN()

//Taken from oalTouch MyOpenALSupport 1.1
static void* LoadWaveAudioData(CFURLRef inFileURL, ALsizei *outDataSize, ALenum *outDataFormat, ALsizei *outSampleRate)
{
    OSStatus                        err = noErr;
    UInt64                            fileDataSize = 0;
    AudioStreamBasicDescription        theFileFormat;
    UInt32                            thePropertySize = sizeof(theFileFormat);
    AudioFileID                        afid = 0;
    void*                            theData = NULL;
    
    UInt32        dataSize;
    
    // Open a file with ExtAudioFileOpen()
    err = AudioFileOpenURL(inFileURL, kAudioFileReadPermission, 0, &afid);
    if(err) { PILOGE(PI_MEDIA_TAG, "MyGetOpenALAudioData: AudioFileOpenURL FAILED, Error = %ld\n", err); goto Exit; }
    
    // Get the audio data format
    err = AudioFileGetProperty(afid, kAudioFilePropertyDataFormat, &thePropertySize, &theFileFormat);
    if(err) { PILOGE(PI_MEDIA_TAG, "MyGetOpenALAudioData: AudioFileGetProperty(kAudioFileProperty_DataFormat) FAILED, Error = %ld\n", err); goto Exit; }
    
    if (theFileFormat.mChannelsPerFrame > 2)  {
        PILOGE(PI_MEDIA_TAG, "MyGetOpenALAudioData - Unsupported Format, channel count is greater than stereo\n"); goto Exit;
    }
    
    if ((theFileFormat.mFormatID != kAudioFormatLinearPCM) || (!TestAudioFormatNativeEndian(theFileFormat))) {
        PILOGE(PI_MEDIA_TAG, "MyGetOpenALAudioData - Unsupported Format, must be little-endian PCM\n"); goto Exit;
    }
    
    if ((theFileFormat.mBitsPerChannel != 8) && (theFileFormat.mBitsPerChannel != 16)) {
        PILOGE(PI_MEDIA_TAG, "MyGetOpenALAudioData - Unsupported Format, must be 8 or 16 bit PCM\n"); goto Exit;
    }
    
    
    thePropertySize = sizeof(fileDataSize);
    err = AudioFileGetProperty(afid, kAudioFilePropertyAudioDataByteCount, &thePropertySize, &fileDataSize);
    if(err) { PILOGE(PI_MEDIA_TAG, "MyGetOpenALAudioData: AudioFileGetProperty(kAudioFilePropertyAudioDataByteCount) FAILED, Error = %ld\n", err); goto Exit; }
    
    // Read all the data into memory
    dataSize = (UInt32)fileDataSize;
    theData = malloc(dataSize);
    if (theData)
    {
        memset(theData, 0, dataSize);
        AudioFileReadBytes(afid, false, 0, &dataSize, theData);
        if(err == noErr)
        {
            // success
            *outDataSize = (ALsizei)dataSize;
            //This fix was added by me, however, 8 bit sounds have a clipping sound at the end so aren't really usable (SO)
            if (theFileFormat.mBitsPerChannel == 16) {
                *outDataFormat = (theFileFormat.mChannelsPerFrame > 1) ? AL_FORMAT_STEREO16 : AL_FORMAT_MONO16;
            } else {
                *outDataFormat = (theFileFormat.mChannelsPerFrame > 1) ? AL_FORMAT_STEREO8 : AL_FORMAT_MONO8;
            }
            *outSampleRate = (ALsizei)theFileFormat.mSampleRate;
        }
        else
        {
            // failure
            free (theData);
            theData = NULL; // make sure to return NULL
            PILOGE(PI_MEDIA_TAG, "MyGetOpenALAudioData: ExtAudioFileRead FAILED, Error = %ld\n", err); goto Exit;
        }
    }
    
Exit:
    // Dispose the ExtAudioFileRef, it is no longer needed
    if (afid) AudioFileClose(afid);
    return theData;
}

//Taken from oalTouch MyOpenALSupport 1.4
void* LoadCafAudioData(CFURLRef inFileURL, ALsizei *outDataSize, ALenum *outDataFormat, ALsizei *outSampleRate)
{
    OSStatus                        status = noErr;
    BOOL                            abort = NO;
    SInt64                            theFileLengthInFrames = 0;
    AudioStreamBasicDescription        theFileFormat;
    UInt32                            thePropertySize = sizeof(theFileFormat);
    ExtAudioFileRef                    extRef = NULL;
    void*                            theData = NULL;
    AudioStreamBasicDescription        theOutputFormat;
    UInt32                            dataSize = 0;
    
    // Open a file with ExtAudioFileOpen()
    status = ExtAudioFileOpenURL(inFileURL, &extRef);
    if (status != noErr)
    {
        PILOGE(PI_MEDIA_TAG, "MyGetOpenALAudioData: ExtAudioFileOpenURL FAILED, Error = %ld\n", status);
        abort = YES;
    }
    if (abort)
        goto Exit;
    
    // Get the audio data format
    status = ExtAudioFileGetProperty(extRef, kExtAudioFileProperty_FileDataFormat, &thePropertySize, &theFileFormat);
    if (status != noErr)
    {
        PILOGE(PI_MEDIA_TAG, "MyGetOpenALAudioData: ExtAudioFileGetProperty(kExtAudioFileProperty_FileDataFormat) FAILED, Error = %ld\n", status);
        abort = YES;
    }
    if (abort)
        goto Exit;
    
    if (theFileFormat.mChannelsPerFrame > 2)
    {
        PILOGE(PI_MEDIA_TAG, "MyGetOpenALAudioData - Unsupported Format, channel count is greater than stereo\n");
        abort = YES;
    }
    if (abort)
        goto Exit;
    
    // Set the client format to 16 bit signed integer (native-endian) data
    // Maintain the channel count and sample rate of the original source format
    theOutputFormat.mSampleRate = theFileFormat.mSampleRate;
    theOutputFormat.mChannelsPerFrame = theFileFormat.mChannelsPerFrame;
    
    theOutputFormat.mFormatID = kAudioFormatLinearPCM;
    theOutputFormat.mBytesPerPacket = 2 * theOutputFormat.mChannelsPerFrame;
    theOutputFormat.mFramesPerPacket = 1;
    theOutputFormat.mBytesPerFrame = 2 * theOutputFormat.mChannelsPerFrame;
    theOutputFormat.mBitsPerChannel = 16;
    theOutputFormat.mFormatFlags = kAudioFormatFlagsNativeEndian | kAudioFormatFlagIsPacked | kAudioFormatFlagIsSignedInteger;
    
    // Set the desired client (output) data format
    status = ExtAudioFileSetProperty(extRef, kExtAudioFileProperty_ClientDataFormat, sizeof(theOutputFormat), &theOutputFormat);
    if (status != noErr)
    {
        PILOGE(PI_MEDIA_TAG, "MyGetOpenALAudioData: ExtAudioFileSetProperty(kExtAudioFileProperty_ClientDataFormat) FAILED, Error = %ld\n", status);
        abort = YES;
    }
    if (abort)
        goto Exit;
    
    // Get the total frame count
    thePropertySize = sizeof(theFileLengthInFrames);
    status = ExtAudioFileGetProperty(extRef, kExtAudioFileProperty_FileLengthFrames, &thePropertySize, &theFileLengthInFrames);
    if (status != noErr)
    {
        PILOGE(PI_MEDIA_TAG, "MyGetOpenALAudioData: ExtAudioFileGetProperty(kExtAudioFileProperty_FileLengthFrames) FAILED, Error = %ld\n", status);
        abort = YES;
    }
    if (abort)
        goto Exit;
    
    // TODO: try to read more data, 1s?
    theFileLengthInFrames += theOutputFormat.mSampleRate;
    
    // Read all the data into memory
    dataSize = (UInt32) theFileLengthInFrames * theOutputFormat.mBytesPerFrame;
    theData = malloc(dataSize);
    if (theData)
    {
        memset(theData, 0, dataSize);
        AudioBufferList        theDataBuffer;
        theDataBuffer.mNumberBuffers = 1;
        theDataBuffer.mBuffers[0].mDataByteSize = dataSize;
        theDataBuffer.mBuffers[0].mNumberChannels = theOutputFormat.mChannelsPerFrame;
        theDataBuffer.mBuffers[0].mData = theData;
        
        // Read the data into an AudioBufferList
        status = ExtAudioFileRead(extRef, (UInt32*)&theFileLengthInFrames, &theDataBuffer);
        if(status == noErr)
        {
            // success
            *outDataSize = (ALsizei)((UInt32) theFileLengthInFrames * theOutputFormat.mBytesPerFrame);
            *outDataFormat = (theOutputFormat.mChannelsPerFrame > 1) ? AL_FORMAT_STEREO16 : AL_FORMAT_MONO16;
            *outSampleRate = (ALsizei)theOutputFormat.mSampleRate;
        }
        else
        {
            // failure
            free (theData);
            theData = NULL; // make sure to return NULL
            PILOGE(PI_MEDIA_TAG, "MyGetOpenALAudioData: ExtAudioFileRead FAILED, Error = %ld\n", status);
            abort = YES;
        }
    }
    if (abort)
        goto Exit;
    
Exit:
    // Dispose the ExtAudioFileRef, it is no longer needed
    if (extRef) ExtAudioFileDispose(extRef);
    return theData;
}

void* GetOpenALAudioData(const std::string& filePath, ALsizei *outDataSize, ALenum *outDataFormat, ALsizei *outSampleRate) {
    NSString *path = [NSString stringWithUTF8String:filePath.c_str()];
    CFURLRef inFileURL = (__bridge CFURLRef)[NSURL fileURLWithPath:path];
    
    CFStringRef extension = CFURLCopyPathExtension(inFileURL);
    CFComparisonResult isWavFile = kCFCompareEqualTo;
    if (extension != NULL) {
        isWavFile = CFStringCompare (extension,(CFStringRef)@"wav", kCFCompareCaseInsensitive);
        CFRelease(extension);
    }
    
    if (isWavFile == kCFCompareEqualTo) {
        return LoadWaveAudioData(inFileURL, outDataSize, outDataFormat, outSampleRate);
    } else {
        return LoadCafAudioData(inFileURL, outDataSize, outDataFormat, outSampleRate);
    }
}

NSPI_END()
