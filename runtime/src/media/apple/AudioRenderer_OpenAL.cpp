/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2015.3.21   0.1     Create
 ******************************************************************************/
#include <pi/Media.h>

#include <OpenAL/al.h>
#include <OpenAL/alc.h>

#include <mutex>
#include <thread>
#include <atomic>

#include "../AudioRendererImpl.h"

using namespace std;

NSPI_BEGIN()

#define BUFFER_MAX_COUNT 64

class AudioRendererOpenAL : public AudioRendererImpl
{
private:
    ALCdevice *mALCDev;
    ALCcontext *mALCCtx;
    ALuint mALSource;
    ALenum mALFormat;
    list<ALuint> mFreeBuffers;
    vector<ALuint> mALBuffers;
    
public:
    AudioRendererOpenAL()
    {        
        mALCDev = nullptr;
        mALCCtx = nullptr;
        mALSource = -1;
        mALFormat = AL_FORMAT_STEREO16;
    }
    
    virtual ~AudioRendererOpenAL()
    {
    }
    
    virtual bool Start()
    {
        piCheck(GetState() == eState_Idle, false);
        
        while (alGetError() != AL_NO_ERROR);
        mALCDev = alcOpenDevice(nullptr);
        if (mALCDev == nullptr)
        {
            PILOGE(PI_MEDIA_TAG, "failed to open OpenAL device, error:%d.", alGetError());
            return false;
        }
        
        ALCint attrs[] = {
            ALC_FREQUENCY, mSampleRate,
            0,
        };
        while (alGetError() != AL_NO_ERROR);
        mALCCtx = alcCreateContext(mALCDev, attrs);
        if (mALCCtx == nullptr)
        {
            PILOGE(PI_MEDIA_TAG, "failed to create OpenAL context, error:%d.", alGetError());
            goto quit0;
        }
        
        while (alGetError() != AL_NO_ERROR);
        if (!alcMakeContextCurrent(mALCCtx))
        {
            PILOGE(PI_MEDIA_TAG, "failed to active OpenAL context, error:%d.", alGetError());
            goto quit1;
        }
        
        const static float orientation[] = {0, 0, -1, 0, 1, 0};
        alListenerfv(AL_ORIENTATION, orientation);
        alListener3f(AL_POSITION, 0.0, 0.0, 0.0);
        alListener3f(AL_VELOCITY, 0.0, 0.0, 0.0);
        
        InitSource();
        
        PILOGI(PI_MEDIA_TAG, "AudioRenderer start");
        
        return true;
        
    quit1:
        alcDestroyContext(mALCCtx);
        mALCCtx = nullptr;
        
    quit0:
        alcCloseDevice(mALCDev);
        mALCDev = nullptr;
        
        return false;
    }
    
    virtual void Stop()
    {
        piCheck(GetState() != eState_Idle, ;);
        
        if (mALSource != -1)
        {
            DeinitSource();
        }
        
        for (int i = 0; i < mALBuffers.size(); ++i)
        {
            alDeleteBuffers(1, &mALBuffers[i]);
            PILOGI(PI_MEDIA_TAG, "Delete AudioRenderer buffer[%d]", mALBuffers[i]);
        }
        mALBuffers.clear();
        mFreeBuffers.clear();
        
        if (mALCCtx != nullptr)
        {
            alcDestroyContext(mALCCtx);
            mALCCtx = nullptr;
        }
        
        if (mALCDev != nullptr)
        {
            alcCloseDevice(mALCDev);
            mALCDev = nullptr;
        }
        
        SetState(eState_Idle);
        
        PILOGI(PI_MEDIA_TAG, "AudioRenderer stop");
    }
    
    virtual void Clear()
    {
        alSourceStop(mALSource);
        
        ALint state = 0;
        do {
            alGetSourcei(mALSource, AL_SOURCE_STATE, &state);
        } while (state == AL_PLAYING);
        
        CollectBuffers();
        
        DeinitSource();
        InitSource();
    }
    
    virtual void Pause()
    {
        piCheck(GetState() == eState_Playing, ;);
        
        alSourcePause(mALSource);
        
        SetState(eState_Paused);
        
        PILOGI(PI_MEDIA_TAG, "AudioRenderer pause");
    }
    
    virtual void Resume()
    {
        piCheck(GetState() == eState_Paused, ;);
        
        alSourcePlay(mALSource);
        
        SetState(eState_Buffering);
        
        PILOGI(PI_MEDIA_TAG, "AudioRenderer resume");
    }
    
    virtual bool Push(iAudioFrame *frame)
    {
        piAssert(frame != nullptr, false);
        
        {
            lock_guard<mutex> lock(mMutex);
            piCheck(mState == eState_Buffering || mState == eState_Playing, false);
            
            CollectBuffers();
            
            piCheck(mBufferSize <= mBufferCapacity, false);
        }
        
        return UploadFrame(frame);
    }
    
    virtual void Update()
    {        
        int state = GetState();
        if (state == eState_Buffering)
        {
            ALint alState = 0;
            alGetSourcei(mALSource, AL_SOURCE_STATE, &alState);
            
            if (alState != AL_PLAYING && IsBufferEnough())
            {
                PILOGS(PI_MEDIA_TAG, "AudioRenderer restart.");
                alSourcePlay(mALSource);
                SetState(eState_Playing);
            }
        }
        else if (state == eState_Playing)
        {
            ALint alState = 0;
            alGetSourcei(mALSource, AL_SOURCE_STATE, &alState);
            
            if (alState != AL_PLAYING)
            {
                if (!IsFinish())
                {
                    alSourcePlay(mALSource);
                }
                else
                {
                    PILOGS(PI_MEDIA_TAG, "AudioRenderer finish.");
                    SetState(eState_Finish);
                    alSourceStop(mALSource);
                }
            }
        }
    }
    
private:
    
    bool UploadFrame(iAudioFrame *frame)
    {
        ALuint buffer = AllocBuffer();
        piAssert(buffer != -1, false);
        
        alBufferData(buffer,
                     mALFormat,
                     frame->GetData(),
                     (ALsizei)frame->GetSize(),
                     mSampleRate);
        
        alSourceQueueBuffers(mALSource, 1, &buffer);
        PILOGV(PI_MEDIA_TAG,
               "AudioRenderer enqueue buffer[%lld], size:%lld, pts:%s, frame:%p",
               buffer,
               frame->GetSize(),
               frame->GetPTS()->ToString().c_str(),
               frame);
        
        lock_guard<mutex> lock(mMutex);
        mBufferSize += frame->GetSize();
        
        return true;
    }
    
    ALuint AllocBuffer()
    {
        ALuint buffer = -1;
        if (!mFreeBuffers.empty())
        {
            buffer = mFreeBuffers.back();
            mFreeBuffers.pop_back();
        }
        else
        {
            buffer = -1;
            alGenBuffers(1, &buffer);
            PILOGI(PI_MEDIA_TAG, "AudioRenderer allocate buffer[%u]", buffer);
            
            mALBuffers.push_back(buffer);
            
            return buffer;
        }
        
        return buffer;
    }
    
    void CollectBuffers()
    {
        ALint processed = 0;
        alGetSourcei(mALSource, AL_BUFFERS_PROCESSED, &processed);
        
        if (processed > 0)
        {
            piAssert(processed <= BUFFER_MAX_COUNT, ;);
            
            ALuint buffers[BUFFER_MAX_COUNT];
            alSourceUnqueueBuffers(mALSource, processed, buffers);
            for (int i = 0; i < processed; ++i)
            {
                ALuint buffer = buffers[i];
                
                ALsizei size = 0;
                alGetBufferi(buffer, AL_SIZE, &size);
                
                mBufferSize -= size;
                
                PILOGV(PI_MEDIA_TAG, "AudioRenderer unqueue buffer[%lld]", buffers[i]);
                mFreeBuffers.push_back(buffers[i]);
            }
        }
    }
    
    void InitSource()
    {
        alGenSources(1, &mALSource);
        alSource3f(mALSource, AL_POSITION, 0, 0, 0);
        alSourcef(mALSource, AL_PITCH, 1.0);
        alSourcef(mALSource, AL_GAIN, 1.0);
        alSource3f(mALSource, AL_VELOCITY, 0.0, 0.0, 0.0);
        alSourcei(mALSource, AL_LOOPING, AL_FALSE);
        
        ResetFinish();
        
        SetState(eState_Buffering);
    }
    
    void DeinitSource()
    {
        alSourceStop(mALSource);
        alDeleteSources(1, &mALSource);
        mALSource = -1;
    }
};


iAudioRenderer* CreateAudioRendererOpenAL()
{
    return new AudioRendererOpenAL();
}



NSPI_END()


















