/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2015.5.19   0.1     Create
 ******************************************************************************/
#import <pi/Media.h>

#import <AVFoundation/AVFoundation.h>
#import <CoreVideo/CoreVideo.h>

#import <pi/media/apple/MediaFrame_AVFoundation.h>
#import "../MediaFrameImpl.h"
#import "../../asset/AssetImpl.h"

using namespace std;

NSPI_BEGIN()

class MemoryHardware : public iMemory
{
private:
    void *mPtr;
    int64_t mSize;
    
public:
    MemoryHardware(int64_t size):
    mPtr(nullptr), mSize(size)
    {
    }
    
    virtual ~MemoryHardware()
    {
        mPtr = nullptr;
        mSize = 0;
    }
    
    virtual char* Ptr() const
    {
        return (char*)mPtr;
    }
    
    void SetPtr(void* ptr)
    {
        mPtr = ptr;
    }
    
    virtual int64_t Size() const
    {
        return mSize;
    }
    
    virtual bool Resize(int64_t size)
    {
        //未实现，因为不会用到这个接口。
        piAssert(false, false);
        return false;
    }
};

class BitmapHardware : public iBitmap
{
private:
    CVPixelBufferRef mBuffer;
    SmartPtr<iPixelFormat> mPixelFormat;
    vector<SmartPtr<iBitmap>> mMipMaps;
    SmartPtr<MemoryHardware> mData[3];
    
public:
    BitmapHardware(CVPixelBufferRef buffer):
    mBuffer(buffer)
    {
        CFRetain(mBuffer);
        
        OSType format = CVPixelBufferGetPixelFormatType(buffer);
        switch (format)
        {
            case kCVPixelFormatType_420YpCbCr8Planar:
            {
                mPixelFormat = CreatePixelFormat(ePixelFormat_YUV420P);
                int64_t sizeY = mPixelFormat->CalcSize(GetWidth(), GetHeight());
                int64_t sizeUV = sizeY / 4;
                
                mData[ePlanar_Y] = new MemoryHardware(sizeY);
                mData[ePlanar_U] = new MemoryHardware(sizeUV);
                mData[ePlanar_V] = new MemoryHardware(sizeUV);
            }
                break;
            case kCVPixelFormatType_24RGB:
            {
                mPixelFormat = CreatePixelFormat(ePixelFormat_RGB);
                int64_t size = mPixelFormat->CalcSize(GetWidth(), GetHeight());
                mData[ePlanar_Y] = new MemoryHardware(size);
            }
                break;
            case kCVPixelFormatType_32BGRA:
            {
                mPixelFormat = CreatePixelFormat(ePixelFormat_BGRA);
                int64_t size = mPixelFormat->CalcSize(GetWidth(), GetHeight());
                mData[ePlanar_Y] = new MemoryHardware(size);
            }
                break;
            default:
                PILOGE(PI_MEDIA_TAG, "%d pixel format not supported.", format);
                piAssert(false, ;);
                break;
        }
    }
    
    virtual ~BitmapHardware()
    {
        CFRelease(mBuffer);
        mBuffer = nullptr;
    }
    
    virtual iPixelFormat *GetPixelFormat() const
    {
        return mPixelFormat;
    }
    
    virtual int32_t GetWidth() const
    {
        return (int32_t)CVPixelBufferGetWidth(mBuffer);
    }
    
    virtual int32_t GetHeight() const
    {
        return (int32_t)CVPixelBufferGetHeight(mBuffer);
    }
    
    virtual void Map(int access)
    {
        CVPixelBufferLockBaseAddress(mBuffer, kCVPixelBufferLock_ReadOnly);
        
        OSType format = CVPixelBufferGetPixelFormatType(mBuffer);
        switch (format)
        {
            case kCVPixelFormatType_420YpCbCr8Planar:
            {
                void* dataY = CVPixelBufferGetBaseAddressOfPlane(mBuffer, ePlanar_Y);
                mData[ePlanar_Y]->SetPtr(dataY);
                
                void* dataU = CVPixelBufferGetBaseAddressOfPlane(mBuffer, ePlanar_U);
                mData[ePlanar_U]->SetPtr(dataU);
                
                void* dataV = CVPixelBufferGetBaseAddressOfPlane(mBuffer, ePlanar_V);
                mData[ePlanar_V]->SetPtr(dataV);
                
                break;
            }
            case kCVPixelFormatType_24RGB:
            case kCVPixelFormatType_32BGRA:
            {
                void* dataY = CVPixelBufferGetBaseAddress(mBuffer);
                mData[ePlanar_Y]->SetPtr(dataY);
                break;
            }
            default:
                break;
        }
    }
    
    virtual void Unmap()
    {
        CVPixelBufferUnlockBaseAddress(mBuffer, kCVPixelBufferLock_ReadOnly);
        
        if (!mData[ePlanar_Y].IsNull())
        {
            mData[ePlanar_Y]->SetPtr(nullptr);
        }

        if (!mData[ePlanar_U].IsNull())
        {
            mData[ePlanar_U]->SetPtr(nullptr);
        }
        
        if (!mData[ePlanar_V].IsNull())
        {
            mData[ePlanar_V]->SetPtr(nullptr);
        }
    }
    
    virtual iMemory* GetData(int32_t planar) const
    {
        piAssert(planar >= 0 && planar < piArrayCount(mData), nullptr);
        return mData[planar];
    }
    
    virtual void SetData(int32_t planar, iMemory *data)
    {
        piAssert(false, ;);
    }
    
    virtual bool CreateMipMaps(int32_t levels)
    {
        //未实现，因为不会用到这个接口。
        piAssert(false, false);
        return false;
    }
    
    virtual int32_t GetMipMapLevels() const
    {
        //未实现，因为不会用到这个接口。
        return 1;
    }
    
    virtual iBitmap *GetMipMap(int32_t level) const
    {
        //未实现，因为不会用到这个接口。
        return nullptr;
    }
    
    virtual int64_t GetBytesPerRow(int32_t planar) const
    {
        piAssert(planar >= 0 && planar < piArrayCount(mData), 0);
        
        if (CVPixelBufferIsPlanar(mBuffer))
        {
            return CVPixelBufferGetBytesPerRowOfPlane(mBuffer, planar);
        }
        else
        {
            piAssert(planar == 0, 0);
            return CVPixelBufferGetBytesPerRow(mBuffer);
        }
    }
    
    virtual void SetBytesPerRow(int32_t planar, int32_t bytes)
    {
        piAssert(false, ;);
    }
    
    virtual int32_t GetWidthOfPlanar(int32_t planar) const
    {
        piAssert(planar >= 0 && planar < piArrayCount(mData), 0);
        
        if (CVPixelBufferIsPlanar(mBuffer))
        {
            return (int32_t)CVPixelBufferGetWidthOfPlane(mBuffer, planar);
        }
        else
        {
            return (int32_t)CVPixelBufferGetWidth(mBuffer);
        }
    }
    
    virtual int32_t GetHeightOfPlanar(int32_t planar) const
    {
        piAssert(planar >= 0 && planar < piArrayCount(mData), 0);
        
        if (CVPixelBufferIsPlanar(mBuffer))
        {
            return (int32_t)CVPixelBufferGetHeightOfPlane(mBuffer, planar);
        }
        else
        {
            return (int32_t)CVPixelBufferGetHeight(mBuffer);
        }
    }
    
    virtual int32_t GetPixelAlignment(int32_t planar) const
    {
        return 4;
    }
    
    virtual string ToString() const
    {
        return piFormat("iBitmap{pixel format:%s, width:%d, height:%d}",
                        mPixelFormat->ToString().c_str(),
                        GetWidth(),
                        GetHeight());
    }
};

iBitmap* CreateBitmapHardware(CVPixelBufferRef buffer)
{
    piAssert(buffer != nil, nullptr);
    return new BitmapHardware(buffer);
}

iVideoFrame* CreateVideoFrameHardware(CMSampleBufferRef buffer)
{
    CMTime time = CMSampleBufferGetPresentationTimeStamp(buffer);
    SmartPtr<iTime> pts = CreateTime(time.value, time.timescale);
    PILOGV(PI_MEDIA_TAG, "video frame decoded, pts:%s", pts->ToString().c_str());
    
    CVImageBufferRef image = CMSampleBufferGetImageBuffer(buffer);
    
    SmartPtr<iBitmap> bitmap = CreateBitmapHardware(image);
    SmartPtr<iVideoFrame> vFrame = CreateVideoFrame(bitmap);
    vFrame->SetPTS(pts);
        
    return vFrame.PtrAndSetNull();
}

//==============================================================================
// AudioFrame

class AudioFrameHardware : public MediaFrameImpl<iAudioFrame>
{
private:
    CMBlockBufferRef mFrame;
    
public:
    AudioFrameHardware(CMBlockBufferRef frame):
    MediaFrameImpl(),
    mFrame(frame)
    {
        CFRetain(frame);
    }
    
    virtual ~AudioFrameHardware()
    {
        CFRelease(mFrame);
        mFrame = nil;
    }
    
    virtual void* GetData() const
    {
        char* data = nullptr;
        CMBlockBufferGetDataPointer(mFrame, 0, nullptr, nullptr, &data);
        return data;
    }
    
    virtual int64_t GetSize() const
    {
        return CMBlockBufferGetDataLength(mFrame);
    }
};


iAudioFrame* CreateAudioFrameHardware(CMBlockBufferRef frame)
{
    piAssert(frame != nullptr, nullptr);
    return new AudioFrameHardware(frame);
}


NSPI_END()










