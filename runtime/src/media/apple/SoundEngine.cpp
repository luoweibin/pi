/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 caiwenqiang     2017.6.30   0.1     Create
 ******************************************************************************/
#include <pi/Media.h>

NSPI_BEGIN()

struct iSoundEngine;

iSoundEngine* CreateSoundEnginePlatform();
iSoundEngine* CreateSoundEngineOpenAL();

iSoundEngine* CreateSoundEngine(int backend)
{
    switch (backend)
    {
        case eAudioEngine_OpenAL:
            return CreateSoundEngineOpenAL();
        case eAudioEngine_Platform:
            return CreateSoundEnginePlatform();
        default:
            return nullptr;
    }
}


NSPI_END()
