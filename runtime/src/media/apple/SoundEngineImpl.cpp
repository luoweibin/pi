/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 caiwenqiang     2017.6.30   0.1     Create
 ******************************************************************************/
#include "SoundEngineImpl.h"

NSPI_BEGIN()

SoundEngineImpl::SoundEngineImpl() :
mIsStarted(false), mSoundId(0)
{
    
}

SoundEngineImpl::~SoundEngineImpl()
{
    Stop();
}

iSoundPlayer* SoundEngineImpl::GetPlayer(int soundId)
{
    iSoundPlayer *player = nullptr;
    
    auto iter = mSoundPlayers.find(soundId);
    if (iter != mSoundPlayers.end())
    {
        player = (*iter).second.Ptr();
    }
    
    return player;
}

bool SoundEngineImpl::Start()
{
    if (!mIsStarted)
    {
        mIsStarted = StartInternal();
    }
    
    return mIsStarted;
}

void SoundEngineImpl::Stop()
{
    if (mIsStarted)
    {
        StopAllSound();
        UnloadAllSound();
        
        StopInternal();
        
        mIsStarted = false;
    }
}

// sound
bool SoundEngineImpl::PreloadSound(const std::string& filePath)
{
    piAssert(mIsStarted, false);
    return true;
}

int SoundEngineImpl::CreateSoundId(const std::string& filePath)
{
    piAssert(mIsStarted, 0);
    iSoundPlayer *player = CreateSoundPlayer(filePath);
    if (player != nullptr) {
        ++mSoundId;
        mSoundPlayers.insert(std::make_pair(mSoundId, player));
        return mSoundId;
    }

    return 0;
}

void SoundEngineImpl::SoundSetLoop(int soundId, bool loop)
{
    iSoundPlayer *player = GetPlayer(soundId);
    if (player != nullptr)
    {
        player->SetLoop(loop);
    }
}

void SoundEngineImpl::PlaySound(int soundId)
{
    piAssert(mIsStarted, ;);
    iSoundPlayer *player = GetPlayer(soundId);
    if (player != nullptr)
    {
        player->Play();
    }
}

void SoundEngineImpl::PauseSound(int soundId)
{
    iSoundPlayer *player = GetPlayer(soundId);
    if (player != nullptr)
    {
        player->Pause();
    }
}

void SoundEngineImpl::ResumeSound(int soundId)
{
    iSoundPlayer *player = GetPlayer(soundId);
    if (player != nullptr)
    {
        player->Resume();
    }
}

void SoundEngineImpl::StopSound(int soundId)
{
    iSoundPlayer *player = GetPlayer(soundId);
    if (player != nullptr)
    {
        player->Stop();
    }
}

void SoundEngineImpl::DestroySoundId(int soundId)
{
    auto iter = mSoundPlayers.find(soundId);
    if (iter != mSoundPlayers.end())
    {
        iSoundPlayer *player = (*iter).second.Ptr();
        if (player != nullptr)
        {
            player->Stop();
            player->Destroy();
        }
        mSoundPlayers.erase(iter);
    }
}

void SoundEngineImpl::UnloadSound(const std::string& filePath)
{
    for (auto iter = mSoundPlayers.begin(); iter != mSoundPlayers.end();)
    {
        iSoundPlayer *player = (*iter).second.Ptr();
        if ((player != nullptr) && ((player->GetFilePath()).compare(filePath) == 0))
        {
            player->Stop();
            player->Destroy();
            iter = mSoundPlayers.erase(iter);
        }
        else
        {
            ++iter;
        }
    }
}

void SoundEngineImpl::StopAllSound()
{
    for (auto& it : mSoundPlayers)
    {
        (it.second)->Stop();
    }
}

void SoundEngineImpl::UnloadAllSound()
{
    for (auto& it : mSoundPlayers)
    {
        iSoundPlayer *player = it.second.Ptr();
        player->Stop();
        player->Destroy();
    }
    mSoundPlayers.clear();
}

NSPI_END()
