/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 caiwenqiang     2017.6.30   0.1     Create
 ******************************************************************************/
#ifndef PI_MEDIA_SOUNDENGINE_H
#define PI_MEDIA_SOUNDENGINE_H

NSPI_BEGIN()

struct iSoundEngine : public iRefObject
{
    virtual ~iSoundEngine() {}
    
    virtual bool Start() = 0;
    
    virtual void Stop() = 0;

    // sound
    virtual bool PreloadSound(const std::string& filePath) = 0;
    
    virtual int CreateSoundId(const std::string& filePath) = 0;
    
    virtual void SoundSetLoop(int soundId, bool loop) = 0;
    
    virtual void PlaySound(int soundId) = 0;
    
    virtual void PauseSound(int soundId) = 0;
    
    virtual void ResumeSound(int soundId) = 0;
    
    virtual void StopSound(int soundId) = 0;
    
    virtual void DestroySoundId(int soundId) = 0;
    
    virtual void UnloadSound(const std::string& filePath) = 0;
    
    virtual void StopAllSound() = 0;
    
    virtual void UnloadAllSound() = 0;
};

iSoundEngine* CreateSoundEngine(int backend);

NSPI_END()

#endif /* SoundEngine_h */
