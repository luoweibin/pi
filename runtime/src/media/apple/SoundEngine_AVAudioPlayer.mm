/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 caiwenqiang     2017.6.30   0.1     Create
 ******************************************************************************/
#include "SoundEngineImpl.h"
#import <AVFoundation/AVFoundation.h>

NSPI_BEGIN()

class AVSoundPlayer : public iSoundPlayer
{
private:
    AVAudioPlayer *mPlayer;
    bool mPaused;
    
public:
    AVSoundPlayer(const std::string& filePath, AVAudioPlayer *player) :
    iSoundPlayer(filePath), mPlayer(player), mPaused(false)
    {
        
    }
    
    virtual ~AVSoundPlayer()
    {
        mPlayer = nil;
    }
    
    virtual void SetLoop(bool loop)
    {
        [mPlayer setNumberOfLoops:loop ? -1 : 0];
    }
    
    virtual void Play()
    {
        [mPlayer setCurrentTime:0];
        [mPlayer play];
        mPaused = false;
    }
    
    virtual void Pause()
    {
        [mPlayer pause];
        mPaused = true;
    }
    
    virtual void Resume()
    {
        if (mPaused) {
            [mPlayer play];
            mPaused = false;
        }
    }
    
    virtual void Stop()
    {
        [mPlayer stop];
        mPaused = false;
    }
    
    virtual void Destroy()
    {
        mPlayer = nil;
    }
};

class AVPlayerSoundEngine : public SoundEngineImpl
{
private:
    virtual iSoundPlayer* CreateSoundPlayer(const std::string& filePath)
    {
        iSoundPlayer *player = nullptr;
        
        NSError *error = nil;
        NSString *path = [NSString stringWithUTF8String:filePath.c_str()];
        AVAudioPlayer *playerTmp = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:&error];
        if (error == nil)
        {
            BOOL ret = [playerTmp prepareToPlay];
            if (ret)
            {
                player = new AVSoundPlayer(filePath, playerTmp);
            }
            else
            {
                PILOGE(PI_MEDIA_TAG, "AVAudioPlayer prepareToPlay failed!");
            }
        }
        else
        {
            PILOGE(PI_MEDIA_TAG, "Create AVAudioPlayer error:%s", [[error localizedDescription] UTF8String]);
        }
        
        return player;
    }
    
    virtual bool StartInternal()
    {
        return true;
    }
    
    virtual void StopInternal()
    {
        
    }
};

iSoundEngine* CreateSoundEnginePlatform()
{
    return new AVPlayerSoundEngine();
}

NSPI_END()
