/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 caiwenqiang     2017.6.30   0.1     Create
 ******************************************************************************/
#ifndef PI_MEDIA_SOUNDENGINEIMPL_H
#define PI_MEDIA_SOUNDENGINEIMPL_H
#include <pi/Media.h>
#include "SoundEngine.h"

#include <map>

NSPI_BEGIN()

struct iSoundPlayer : public iRefObject
{
private:
    std::string mFilePath;
    
protected:
    iSoundPlayer(const std::string& filePath) :
    mFilePath(filePath)
    {
        
    }
    
public:
    virtual ~iSoundPlayer() {}
    
    virtual void SetLoop(bool loop) = 0;
    
    virtual void Play() = 0;
    
    virtual void Pause() = 0;
    
    virtual void Resume() = 0;
    
    virtual void Stop() = 0;
    
    virtual void Destroy() = 0;
    
    const std::string& GetFilePath() const
    {
        return mFilePath;
    }
};

class SoundEngineImpl : public iSoundEngine
{
private:
    bool mIsStarted;
    
    int mSoundId;
    std::map<int, SmartPtr<iSoundPlayer>> mSoundPlayers;
    
private:
    virtual iSoundPlayer* CreateSoundPlayer(const std::string& filePath) = 0;
    
    virtual bool StartInternal() = 0;
    
    virtual void StopInternal() = 0;
    
    iSoundPlayer* GetPlayer(int soundId);
    
public:
    SoundEngineImpl();
    
    virtual ~SoundEngineImpl();
    
    virtual bool Start();
    
    // stop and release
    virtual void Stop();
    
    // sound
    virtual bool PreloadSound(const std::string& filePath);
    
    virtual int CreateSoundId(const std::string& filePath);
    
    virtual void SoundSetLoop(int soundId, bool loop);
    
    virtual void PlaySound(int soundId);
    
    virtual void PauseSound(int soundId);
    
    virtual void ResumeSound(int soundId);
    
    virtual void StopSound(int soundId);
    
    virtual void DestroySoundId(int soundId);
    
    virtual void UnloadSound(const std::string& filePath);
    
    virtual void StopAllSound();
    
    virtual void UnloadAllSound();
};

NSPI_END()

#endif /* SoundEngineImpl_hpp */
