/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2015.5.21   0.1     Create
 ******************************************************************************/
#include <pi/Media.h>
#include <AudioToolbox/AudioToolbox.h>

#include "../AudioRendererImpl.h"
#include <list>

using namespace std;

NSPI_BEGIN()

static void AQOutputCallback(void *inUserData, AudioQueueRef inAQ,
                             AudioQueueBufferRef inBuffer);


class AudioRendererAudioQueue : public AudioRendererImpl
{
private:
    AudioQueueRef mAQ;
    vector<AudioQueueBufferRef> mBuffers;
    list<AudioQueueBufferRef> mFreeBuffers;
    
    list<SmartPtr<iAudioFrame>> mFrames;
    
public:
    AudioRendererAudioQueue()
    {
    }
    
    virtual ~AudioRendererAudioQueue()
    {
    }
    
    virtual bool Start()
    {
        piAssert(GetState() == eState_Idle, false);
        
        AudioStreamBasicDescription desc;
        desc.mSampleRate = mSampleRate;
        desc.mFormatID = kAudioFormatLinearPCM;
        desc.mFormatFlags = kLinearPCMFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
        desc.mFramesPerPacket = 1;
        desc.mChannelsPerFrame = 2;
        desc.mBytesPerFrame = sizeof(short) * desc.mChannelsPerFrame;
        desc.mBytesPerPacket = desc.mFramesPerPacket * desc.mBytesPerFrame;
        desc.mBitsPerChannel = 8 * sizeof(short);
        desc.mReserved = 0;
        
        {
            OSStatus status = AudioQueueNewOutput(&desc,
                                                  AQOutputCallback,
                                                  this,
                                                  nullptr,
                                                  nullptr,
                                                  0,
                                                  &mAQ);
            piAssert(status == 0, false);
        }
                
        SetState(eState_Buffering);
        
        PILOGI(PI_MEDIA_TAG, "AudioRenderer start");
        
        return true;
    }
    
    virtual void Stop()
    {
        piAssert(GetState() != eState_Idle, ;);
        
        AudioQueueStop(mAQ, false);
        
        for (int i = 0; i < piArrayCount(mBuffers); ++i)
        {
            if (mBuffers[i] != nullptr)
            {
                AudioQueueFreeBuffer(mAQ, mBuffers[i]);
                mBuffers[i] = nullptr;
            }
        }
        
        if (mAQ != nullptr)
        {
            AudioQueueDispose(mAQ, false);
            mAQ = nullptr;
        }
        
        {
            lock_guard<mutex> lock(mMutex);
            mFrames.clear();
            mBufferSize = 0;
        }
        
        SetState(eState_Idle);
        
        PILOGI(PI_MEDIA_TAG, "AudioRenderer stop");
    }
    
    virtual void Pause()
    {
        piCheck(GetState() == eState_Playing, ;);
        
        AudioQueuePause(mAQ);
        
        SetState(eState_Paused);
        
        PILOGI(PI_MEDIA_TAG, "AudioRenderer pause");
    }
    
    virtual void Resume()
    {
        PILOGI(PI_MEDIA_TAG, "AudioRenderer resume");
        
        SetState(eState_Buffering);
    }
    
    /**
     * 清除缓冲区。
     */
    virtual void Clear()
    {
        AudioQueueStop(mAQ, true);
        
        while (true)
        {
            lock_guard<mutex> lock(mMutex);
            if (mBufferSize <= 0)
            {
                break;
            }
        }
    
        lock_guard<mutex> lock(mMutex);
        mFrames.clear();
        mBufferSize = 0;
    }
    
    /**
     * 添加音频帧到缓冲区
     * \return 缓冲区足够，返回true，不足返回false。
     */
    virtual bool Push(iAudioFrame *frame)
    {
        int state = GetState();
        piCheck(state == eState_Buffering || state == eState_Playing, false);
     
        lock_guard<mutex> lock(mMutex);
        mFrames.push_back(frame);
    
        return true;
    }
    
    virtual void Update()
    {
        if (GetState() == eState_Buffering)
        {
            while (!IsBufferEnough())
            {
                SmartPtr<iAudioFrame> frame = PopFrame();
                if (frame.IsNull())
                {
                    break;
                }
                
                //由于每帧大小会有细微差别，为了防止重用时出现缓冲区不足的情况，将缓冲区大小
                //适当增加20%。
                int64_t capacity = frame->GetSize() * 1.2;
                
                AudioQueueBufferRef buffer = AllocBuffer(capacity);
                if (buffer == nullptr)
                {
                    break;
                }
                
                if (!FillAudioBuffer(mAQ, buffer, frame))
                {
                    FreeBuffer(buffer);
                    break;
                }
                
                AudioQueueEnqueueBuffer(mAQ, buffer, 0, nullptr);
                
                mBufferSize += frame->GetSize();
            }
            
            if (IsBufferEnough())
            {
                AudioQueueStart(mAQ, nullptr);
                SetState(eState_Playing);
            }
        }
    }
    
public:
    
    AudioQueueBufferRef AllocBuffer(int64_t size)
    {
        if (!mFreeBuffers.empty())
        {
            for (list<AudioQueueBufferRef>::iterator it = mFreeBuffers.begin();
                 it != mFreeBuffers.end();
                 ++it)
            {
                AudioQueueBufferRef buffer = *it;
                if (buffer->mAudioDataBytesCapacity >= size)
                {
                    mFreeBuffers.erase(it);
                    return buffer;
                }
            }
            
            return nullptr;
        }
        else
        {
            if (mBufferSize < mBufferCapacity)
            {
                AudioQueueBufferRef buffer = nullptr;
                OSStatus status = AudioQueueAllocateBuffer(mAQ, (UInt32)size, &buffer);
                piAssert(status == 0, nullptr);
                PILOGS(PI_MEDIA_TAG, "Allocate AudioQueue buffer, size:%lld, ptr:%p", size, buffer);
                return buffer;
            }
            else
            {
                return nullptr;
            }
        }
    }
    
    void FreeBuffer(AudioQueueBufferRef buffer)
    {
        mBufferSize -= buffer->mAudioDataByteSize;
        buffer->mAudioDataByteSize = 0;
        
        mFreeBuffers.push_back(buffer);
        
        PILOGS(PI_MEDIA_TAG,
               "Free AudioQueue buffer, size:%lld, ptr:%p",
               buffer->mAudioDataBytesCapacity,
               buffer);
    }
    
    void OnReadFrame(AudioQueueBufferRef buffer)
    {
        SmartPtr<iAudioFrame> frame = PopFrame();
        if (frame.IsNull())
        {
            buffer->mAudioDataByteSize = 0;
            FreeBuffer(buffer);
            PILOGE(PI_MEDIA_TAG, "Frame not enough.");
            return;
        }
        
        if (!FillAudioBuffer(mAQ, buffer, frame))
        {
            PILOGE(PI_MEDIA_TAG, "Buffer not enough.");
            FreeBuffer(buffer);
            return;
        }
        
        AudioQueueEnqueueBuffer(mAQ, buffer, 0, nullptr);
    }
    
    iAudioFrame* PopFrame()
    {
        lock_guard<mutex> lock(mMutex);
        if (!mFrames.empty())
        {
            SmartPtr<iAudioFrame> frame = mFrames.front();
            mFrames.pop_front();
            return frame.PtrAndSetNull();
        }
        else
        {
            return nullptr;
        }
    }
    
private:
    
    static bool FillAudioBuffer(AudioQueueRef aq, AudioQueueBufferRef buffer, iAudioFrame *frame)
    {
        int32_t size = (int32_t)frame->GetSize();
        piAssert(buffer->mAudioDataBytesCapacity >= size, false);
        
        buffer->mAudioDataByteSize = size;
        memcpy(buffer->mAudioData, frame->GetData(), size);
        AudioQueueEnqueueBuffer(aq, buffer, 0, nullptr);
        
        PILOGD(PI_MEDIA_TAG,
               "Fill AudioQueue buffer(%p) with frame(%p) of size:%lld",
               buffer,
               frame,
               frame->GetSize());
        
        return true;
    }
};


iAudioRenderer* CreateAudioRendererPlatform()
{
    return new AudioRendererAudioQueue();
}




void AQOutputCallback(void *inUserData, AudioQueueRef inAQ, AudioQueueBufferRef inBuffer)
{
    AudioRendererAudioQueue *renderer = (AudioRendererAudioQueue*)inUserData;
    
    if (inBuffer->mAudioDataByteSize > 0)
    {
        renderer->OnReadFrame(inBuffer);
    }
}



NSPI_END()






















