/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2015.5.19   0.1     Create
 ******************************************************************************/
#import <pi/Media.h>

#import <AVFoundation/AVFoundation.h>
#import <CoreVideo/CoreVideo.h>
#import <pi/media/apple/MediaFrame_AVFoundation.h>

#import "../MediaCodecImpl.h"

using namespace std;

NSPI_BEGIN()

iAudioFrame* CreateAudioFrameHardware(CMBlockBufferRef frame);

static int64_t CMTimeToTimestamp(CMTime time)
{
    int64_t num = time.value / time.timescale;
    double den = double(time.value % time.timescale) / time.timescale;
    return piSec2Ms(num) + piSec2Ms(den);
}

class MediaCodecHardware : public MediaCodecImpl
{
private:
    AVURLAsset *mAsset;
    
    AVAssetReader *mVideoReader;
    AVAssetReaderOutput *mVideoOutput;
    
    AVAssetReader *mAudioReader;
    AVAssetReaderOutput *mAudioOutput;
    
public:
    MediaCodecHardware()
    {
    }
    
    virtual ~MediaCodecHardware()
    {
    }
    
    virtual bool Open(const std::string& url)
    {
        piAssert(GetState() == eMediaCodec_Idle, false);
        
        NSString *aURL = [NSString stringWithUTF8String:url.c_str()];
        NSURL *pathURL = [NSURL fileURLWithPath:aURL];
        mAsset = [[AVURLAsset alloc] initWithURL:pathURL
                                         options:nil];
        piCheck(mAsset != nil, false);
        piCheck(mAsset.playable, false);
        piCheck(mAsset.readable, false);
        
        SmartPtr<iEvent> event = CreateEvent(1);
        LoadDuration(event);
        
        NSArray *tracks = mAsset.tracks;
        for (int i = 0; i < [tracks count]; ++i)
        {
            AVAssetTrack *track = tracks[i];
            NSString* type = track.mediaType;
            if ([type isEqualToString:AVMediaTypeAudio])
            {
                OnAudioStreamFound(i);
            }
            else if ([type isEqualToString:AVMediaTypeVideo])
            {
                OnVideoStreamFound(i);
            }
        }
        
        event->Lock();
        event->Wait();
        event->Unlock();
        
        if (!mVideoStreams.empty())
        {
            mVideoTrack = mVideoStreams.front();
        }
        
        if (!mAudioStreams.empty())
        {
            mAudioTrack = mAudioStreams.front();
        }
        
        if (mVideoTrack >= 0 && mAudioTrack < 0)
        {
            //假设一般的视频都有音轨，如果没有音轨则可能是解码不支持，返回失败。
            //注意：这存在副作用，如果真的是没有音轨那么也播放不了。这个概率应该比较低。
            ResetInternalStates();
            return false;
        }
        
        if (mVideoTrack >= 0)
        {
            AVAssetTrack *track = mAsset.tracks[mVideoTrack];
            mVideoFPS = track.nominalFrameRate;
            
            InitVideoCodec(track);
        }
        
        if (mAudioTrack >= 0)
        {
            AVAssetTrack *track = mAsset.tracks[mAudioTrack];
            InitAudioCodec(track);
        }
        
        mState = eMediaCodec_Open;
        
        PILOGI(PI_MEDIA_TAG, "MediaCodec opened.");
        
        return true;
    }
    
    virtual void Close()
    {
        piAssert(mState != eMediaCodec_Idle && mState != eMediaCodec_Closed, ;);
        
        DeinitAudioCodec();
        DeinitVideoCodec();
        
        mAsset = nil;
        
        ClearFrames();
        
        mState = eMediaCodec_Closed;
    }
    
    virtual bool ReadFrame()
    {
        piCheck(mState = eMediaCodec_Open, false);
        
        bool hasVideoFrame = false;
        bool hasAudioFrame = false;
        
        {
            lock_guard<mutex> lock(mMutex);
            hasVideoFrame = (mVideoTrack >= 0);
            hasAudioFrame = (mAudioTrack >= 0);
        }
        
        if (hasVideoFrame)
        {
            hasVideoFrame = ReadVideoFrame();
        }
        
        if (hasAudioFrame)
        {
            hasAudioFrame = ReadAudioFrame();
        }
        
        if (!hasVideoFrame)
        {
            lock_guard<mutex> lock(mVideoFramesMutex);
            mVideoFinish = true;
        }
        
        if (!hasAudioFrame)
        {
            lock_guard<mutex> lock(mAudioFramesMutex);
            mAudioFinish = true;
        }
        
        if (hasVideoFrame || hasAudioFrame)
        {
            return true;
        }
        else
        {
            mState = eMediaCodec_Finish;
            return false;
        }
    }
    
protected:
    
    virtual void DoSeekStream(int64_t position)
    {
        if (mVideoTrack != -1)
        {
            [mVideoReader cancelReading];
            while (mVideoReader.status == AVAssetReaderStatusReading);
            
            AVAssetTrack *track = mAsset.tracks[mVideoTrack];
            mVideoOutput = CreateVideoOutput(track);
            piAssert(mVideoOutput != nil, ;);
            
            CMTimeRange timeRange = MakeTimeRange(position, track.naturalTimeScale);
            mVideoReader = CreateReader(mVideoOutput, timeRange);
            piAssert(mVideoReader != nil, ;);
        }
        
        if (mAudioTrack != -1)
        {
            [mAudioReader cancelReading];
            while (mAudioReader.status == AVAssetReaderStatusReading);
            
            AVAssetTrack *track = mAsset.tracks[mAudioTrack];
            
            mAudioOutput = CreateAudioOutput(track);
            piAssert(mAudioOutput != nil, ;);
            
            CMTimeRange timeRange = MakeTimeRange(position, track.naturalTimeScale);
            mAudioReader = CreateReader(mAudioOutput, timeRange);
            piAssert(mAudioReader != nil, ;);
        }
    }
    
private:
    void LoadDuration(iEvent *event)
    {
        NSArray *keys = @[@"duration"];
        
        [mAsset loadValuesAsynchronouslyForKeys:keys completionHandler:^() {
         NSError *error = nil;
         AVKeyValueStatus tracksStatus = [mAsset statusOfValueForKey:@"duration" error:&error];
         if (tracksStatus == AVKeyValueStatusLoaded)
         {
         CMTime duration = mAsset.duration;
         mDuration = CMTimeToTimestamp(duration);
         }
         
         event->Fire();
         }];
    }
    
    void InitVideoCodec(AVAssetTrack *track)
    {
        mVideoOutput = CreateVideoOutput(track);
        piAssert(mVideoOutput != nil, ;);
    
        CMTimeRange timeRange = MakeTimeRange(0, track.naturalTimeScale);
        mVideoReader = CreateReader(mVideoOutput, timeRange);
        piAssert(mVideoReader != nil, ;);
        PILOGS(PI_MEDIA_TAG, "video codec init.");
    }
    
    void DeinitVideoCodec()
    {
        [mVideoReader cancelReading];
        mVideoReader = nil;
        mVideoOutput = nil;
        PILOGS(PI_MEDIA_TAG, "video codec deinit.");
    }
    
    //返回false表示完成解码，否则返回true。
    bool ReadVideoFrame()
    {
        piCheck(mVideoReader.status == AVAssetReaderStatusReading, false);
        
        CMSampleBufferRef frame = [mVideoOutput copyNextSampleBuffer];
        piCheck(frame != nil, false);
        
        SmartPtr<iVideoFrame> vFrame = CreateVideoFrameHardware(frame);
        
        CMTime time = CMSampleBufferGetPresentationTimeStamp(frame);
        SmartPtr<iTime> pts = CreateTime(time.value, time.timescale);
        int64_t ptsInMS = piTimeRescale(pts->GetValue(), pts->GetTimeScale(), 1000);
        PILOGV(PI_MEDIA_TAG, "video frame decoded, pts:%s", pts->ToString().c_str());        
        
        lock_guard<mutex> lock(mVideoFramesMutex);
        mVideoBufferPos = ptsInMS;
        mVideoFrames.push_back(vFrame);
        
        CFRelease(frame);
        frame = nil;
        
        return mVideoReader.status == AVAssetReaderStatusReading;
    }
    
    void InitAudioCodec(AVAssetTrack *track)
    {
        mAudioOutput = CreateAudioOutput(track);
        piAssert(mAudioOutput != nil, ;);
        
        CMTimeRange timeRange = MakeTimeRange(0, track.naturalTimeScale);
        mAudioReader = CreateReader(mAudioOutput, timeRange);
        
        PILOGS(PI_MEDIA_TAG, "audio codec init.");
    }
    
    void DeinitAudioCodec()
    {
        [mAudioReader cancelReading];
        
        mAudioOutput = nil;
        mAudioReader = nil;
        PILOGS(PI_MEDIA_TAG, "audio codec deinit.");
    }
    
    //返回false表示完成解码，否则返回true。
    bool ReadAudioFrame()
    {
        piCheck(mAudioReader.status == AVAssetReaderStatusReading, false);
        
        CMSampleBufferRef frame = [mAudioOutput copyNextSampleBuffer];
        piCheck(frame != nil, false);
        
        CMTime time = CMSampleBufferGetPresentationTimeStamp(frame);
        SmartPtr<iTime> pts = CreateTime(time.value, time.timescale);
        PILOGV(PI_MEDIA_TAG,
               "audio frame decoded, samples:%d, pts:%s",
               mAudioSampleRate,
               pts->ToString().c_str());
        
        CMBlockBufferRef oFrame = CMSampleBufferGetDataBuffer(frame);

        SmartPtr<iAudioFrame> aFrame = CreateAudioFrameHardware(oFrame);
        aFrame->SetPTS(pts);
        
        lock_guard<mutex> lock(mAudioFramesMutex);
        mAudioBufferPos = piTimeRescale(pts->GetValue(),
                                        pts->GetTimeScale(),
                                        1000);
        mAudioFrames.push_back(aFrame);
        
        CFRelease(frame);
        frame = nil;

        return mAudioReader.status == AVAssetReaderStatusReading;
    }
    
    AVAssetReaderTrackOutput* CreateVideoOutput(AVAssetTrack *track)
    {
        int format = kCVPixelFormatType_420YpCbCr8Planar;
        if (mOutputPixelFormat == ePixelFormat_RGB)
        {
            format = kCVPixelFormatType_24RGB;
        }
        else if (mOutputPixelFormat != ePixelFormat_Unknown)
        {
            PILOGE(PI_MEDIA_TAG, "Only ePixelFormat_RGB and ePixelFormat_YUV420P supported, will set to default ePixelFormat_YUV420P.");
        }
        NSDictionary *settings =
        @{
          (NSString*)kCVPixelBufferPixelFormatTypeKey: [NSNumber numberWithUnsignedInt:format]
          };
        return [AVAssetReaderTrackOutput assetReaderTrackOutputWithTrack:track
                                                          outputSettings:settings];
    }
    
    AVAssetReaderOutput* CreateAudioOutput(AVAssetTrack *track)
    {
        NSDictionary *settings =
        @{
          AVFormatIDKey: [NSNumber numberWithInteger:kAudioFormatLinearPCM],
          AVSampleRateKey: [NSNumber numberWithInteger:mAudioSampleRate],
          AVLinearPCMBitDepthKey: [NSNumber numberWithInteger:16],
          AVNumberOfChannelsKey: [NSNumber numberWithInteger:2],
          AVLinearPCMIsNonInterleaved: @NO,
          AVLinearPCMIsFloatKey: @NO,
          AVLinearPCMIsBigEndianKey: @NO,
          };
        
        NSArray* tracks = [mAsset tracksWithMediaType:AVMediaTypeAudio];
        return [AVAssetReaderAudioMixOutput assetReaderAudioMixOutputWithAudioTracks:tracks
                                                                       audioSettings:settings];
    }
    
    AVAssetReader* CreateReader(AVAssetReaderOutput *output, const CMTimeRange &timeRange)
    {
        NSError *error = nil;
        AVAssetReader *reader = [AVAssetReader assetReaderWithAsset:mAsset
                                                              error:&error];
        piAssert(error == nil, nil);
        
        reader.timeRange = timeRange;
        [reader addOutput:output];
        [reader startReading];
        
        return reader;
    }
    
    static CMTimeRange MakeTimeRange(int64_t position, int32_t timescale)
    {
        int64_t seconds = piMs2Sec(position);
        double milliseconds = position % 1000;
        int64_t value = seconds * timescale + int64_t(piMs2Sec(milliseconds) * timescale);
        
        CMTime start = CMTimeMake(value, timescale);
        return CMTimeRangeMake(start, kCMTimePositiveInfinity);
    }
};


iMediaCodec* CreateMediaCodecHardware()
{
    return new MediaCodecHardware();
}


NSPI_END()





























