/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2015.5.19   0.1     Create
 ******************************************************************************/
#ifndef pi_MediaCodecImpl_h
#define pi_MediaCodecImpl_h

#include <pi/Media.h>
#include <mutex>
#include <deque>
#include "MediaCodec.h"

NSPI_BEGIN()

class MediaCodecImpl : public iMediaCodec
{
protected:
    
    mutable std::mutex mMutex;
    mutable std::mutex mAudioFramesMutex;
    mutable std::mutex mVideoFramesMutex;
    
    //输出像素格式
    int mOutputPixelFormat;
    
    //音频采样率
    int32_t mAudioSampleRate;
    
    //==========================================================================
    //以下是临时状态
    int mState;
    
    //时长
    int64_t mDuration;
    
    //音频解码
    bool mAudioFinish;
    int mAudioTrack;
    std::vector<int> mAudioStreams;
    std::deque<SmartPtr<iAudioFrame>> mAudioFrames;
    //音频缓冲位置
    int64_t mAudioBufferPos;
    
    //视频解码
    bool mVideoFinish;
    int mVideoTrack;
    std::vector<int> mVideoStreams;
    std::deque<SmartPtr<iVideoFrame>> mVideoFrames;
    //视频缓冲位置
    int64_t mVideoBufferPos;
    int32_t mVideoFPS;
    
public:
    MediaCodecImpl():
    mOutputPixelFormat(ePixelFormat_Unknown),
    mState(eMediaCodec_Idle),
    mVideoFinish(false), mAudioFinish(false),
    mDuration(0), mVideoFPS(0),
    mAudioBufferPos(0), mVideoBufferPos(0),
    mAudioSampleRate(PI_AUDIO_SAMPLE_RATE),
    mVideoTrack(-1), mAudioTrack(-1)
    {
    }
    
    virtual ~MediaCodecImpl()
    {
    }
    
    virtual void Reset()
    {
        piAssert(mState == eMediaCodec_Closed, ;);
        
        ResetInternalStates();
        
        SetState(eMediaCodec_Idle);
        
        PILOGI(PI_MEDIA_TAG, "MediaCodec reset.");
    }
    
    virtual int GetState() const
    {
        std::lock_guard<std::mutex> lock(mMutex);
        return mState;
    }
    
    virtual void SetOutputPixelFormat(int format)
    {
        std::lock_guard<std::mutex> lock(mMutex);
        mOutputPixelFormat = format;
    }
    
    virtual int GetOutputPixelFormat() const
    {
        std::lock_guard<std::mutex> lock(mMutex);
        return mOutputPixelFormat;
    }
    
    virtual void Seek(int64_t position)
    {
        piAssert(GetState() == eMediaCodec_Open, ;);
        piAssert(position >= 0, ;);
        
        int64_t timeMS = piMin(position, mDuration);
        
        mVideoBufferPos = -1;
        mAudioBufferPos = -1;
        
        //如果没有视频轨，则将mVideoBufferPos设成0，这样下面ReadFrame判断时即可跳过视频轨。
        if (GetVideoTrack() == -1)
        {
            mVideoBufferPos = 0;
        }
        
        //如果没有音频轨，则将mAudioBufferPos设成0，这样下面ReadFrame判断时即可跳过音频轨。
        if (GetAudioTrack() == -1)
        {
            mAudioBufferPos = 0;
        }
        
        {
            std::lock_guard<std::mutex> lock(mVideoFramesMutex);
            mVideoFrames.clear();
        }
        
        {
            std::lock_guard<std::mutex> lock(mAudioFramesMutex);
            mAudioFrames.clear();
        }
        
        int64_t begin0 = piGetUpTimeUS();
        
        DoSeekStream(timeMS);
        
        int64_t begin1 = piGetUpTimeUS();
        PILOGI(PI_MEDIA_TAG, "Time cost of seeking file:%lldMS", piUs2Ms(begin1 - begin0));
        
        while (mVideoBufferPos < 0 || mAudioBufferPos < 0)
        {
            if (!ReadFrame())
            {
                break;
            }
        }
        
        int64_t begin2 = piGetUpTimeUS();
        PILOGI(PI_MEDIA_TAG, "Time cost of reading frame:%lldMS", piUs2Ms(begin2 - begin1));
    }
    
    virtual void SetAudioSampleRate(int32_t sampleRate)
    {
        piAssert(sampleRate > 0, ;);
        piAssert(GetState() == eMediaCodec_Idle, ;);
        
        std::lock_guard<std::mutex> lock(mMutex);
        mAudioSampleRate = sampleRate;
    }
    
    virtual int32_t GetAudioSampleRate() const
    {
        std::lock_guard<std::mutex> lock(mMutex);
        return mAudioSampleRate;
    }
    
    virtual int64_t GetDuration() const
    {
        std::lock_guard<std::mutex> lock(mMutex);
        return mDuration;
    }
    
    virtual iAudioFrame* PopAudioFrame()
    {
        piCheck(mAudioTrack >= 0, nullptr);
        
        SmartPtr<iAudioFrame> frame;
        
        std::lock_guard<std::mutex> lock(mAudioFramesMutex);
        if (!mAudioFrames.empty())
        {
            frame = mAudioFrames.front();
            mAudioFrames.pop_front();
        }
        
        return frame.PtrAndSetNull();
    }
    
    virtual iAudioFrame* PeekAudioFrame() const
    {
        piCheck(mAudioTrack >= 0, nullptr);
        
        SmartPtr<iAudioFrame> frame;
        
        std::lock_guard<std::mutex> lock(mAudioFramesMutex);
        if (!mAudioFrames.empty())
        {
            frame = mAudioFrames.front();
        }
        
        return frame.PtrAndSetNull();
    }
    
    virtual int64_t GetAudioBufferingPosition() const
    {
        return mAudioBufferPos;
    }
    
    virtual int64_t GetAudioBufferingLength() const
    {
        std::lock_guard<std::mutex> lock(mAudioFramesMutex);
        if (!mAudioFrames.empty())
        {
            SmartPtr<iTime> begin = mAudioFrames.front()->GetPTS();
            SmartPtr<iTime> end = mAudioFrames.back()->GetPTS();
            return piTimeRescale(end->GetValue() - begin->GetValue(), begin->GetTimeScale(), 1000);
        }
        else
        {
            return 0;
        }
    }
    
    virtual int32_t GetAudioTrack() const
    {
        std::lock_guard<std::mutex> lock(mMutex);
        return mAudioTrack;
    }
    
    virtual iVideoFrame* PopVideoFrame()
    {
        piCheck(mVideoTrack >= 0, nullptr);
        
        SmartPtr<iVideoFrame> currFrame;
        
        std::lock_guard<std::mutex> lock(mVideoFramesMutex);
        if (!mVideoFrames.empty())
        {
            currFrame = mVideoFrames.front();
            mVideoFrames.pop_front();
        }
        
        return currFrame.PtrAndSetNull();
    }
    
    virtual iVideoFrame* PeekVideoFrame() const
    {
        piCheck(mVideoTrack >= 0, nullptr);
        
        SmartPtr<iVideoFrame> currFrame;
        
        std::lock_guard<std::mutex> lock(mVideoFramesMutex);
        if (!mVideoFrames.empty())
        {
            currFrame = mVideoFrames.front();
        }
        
        return currFrame.PtrAndSetNull();
    }
    
    virtual int64_t GetVideoBufferingPosition() const
    {
        return mVideoBufferPos;
    }
    
    virtual int64_t GetVideoBufferingLength() const
    {
        std::lock_guard<std::mutex> lock(mVideoFramesMutex);
        if (!mVideoFrames.empty())
        {
            SmartPtr<iTime> begin = mVideoFrames.front()->GetPTS();
            SmartPtr<iTime> end = mVideoFrames.back()->GetPTS();
            return piTimeRescale(end->GetValue() - begin->GetValue(), begin->GetTimeScale(), 1000);
        }
        else
        {
            return 0;
        }
    }
    
    virtual int32_t GetVideoFPS() const
    {
        piAssert(GetState() == eMediaCodec_Open, -1);
        piAssert(mVideoTrack >= 0, -1);
        
        std::lock_guard<std::mutex> lock(mMutex);
        return mVideoFPS;
    }
    
    virtual int32_t GetVideoTrack() const
    {
        std::lock_guard<std::mutex> lock(mMutex);
        return mVideoTrack;
    }
    
    virtual bool IsVideoFinish() const
    {
        std::lock_guard<std::mutex> lock(mVideoFramesMutex);
        return mVideoFinish && mVideoFrames.empty();
    }
    
    virtual bool IsAudioFinish() const
    {
        std::lock_guard<std::mutex> lock(mAudioFramesMutex);
        return mAudioFinish && mAudioFrames.empty();
    }
    
protected:
    
    virtual void DoSeekStream(int64_t position) = 0;
    
    void ResetInternalStates()
    {
        mDuration = 0;
        mAudioBufferPos = 0;
        mVideoBufferPos = 0;
        mAudioTrack = -1;
        mVideoTrack = -1;
        mAudioFinish = false;
        mVideoFinish = false;
        mAudioStreams.clear();
        mVideoStreams.clear();
    }
    
    void OnVideoStreamFound(int index)
    {
        PILOGI(PI_MEDIA_TAG, "video stream found at %d", index);
        mVideoStreams.push_back(index);
    }
    
    void OnAudioStreamFound(int index)
    {
        mAudioStreams.push_back(index);
        PILOGI(PI_MEDIA_TAG, "audio stream found at %d", index);
    }
    
    void ClearFrames()
    {
        {
            std::lock_guard<std::mutex> lock(mVideoFramesMutex);
            mVideoFrames.clear();
        }
        
        {
            std::lock_guard<std::mutex> lock(mAudioFramesMutex);
            mAudioFrames.clear();
        }
    }
    
    void SetState(int state)
    {
        std::lock_guard<std::mutex> lock(mMutex);
        if (mState != state)
        {
            mState = state;
        }
    }
};


NSPI_END()



#endif
































