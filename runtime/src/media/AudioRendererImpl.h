/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2015.5.21   0.1     Create
 ******************************************************************************/
#ifndef pi_AudioRendererImpl_h
#define pi_AudioRendererImpl_h

#include <pi/Media.h>
#include <mutex>

NSPI_BEGIN()

class AudioRendererImpl : public iAudioRenderer
{
protected:
    
    enum
    {
        eState_Idle,
        eState_Buffering,
        eState_Playing,
        eState_Paused,
        eState_Finish,
    };
    
    mutable std::mutex mMutex;
    
    int mState;
    int32_t mSampleRate;
    
    int64_t mBufferEnough;
    int64_t mBufferCapacity;
    
    int64_t mBufferSize;
    bool mFinish;
    
public:
    AudioRendererImpl():
    mFinish(false),
    mState(eState_Idle),
    mSampleRate(PI_AUDIO_SAMPLE_RATE),
    mBufferSize(0),
    mBufferEnough(PI_AUDIO_BUFFER_ENOUGH),
    mBufferCapacity(PI_AUDIO_BUFFER_CAPACITY)
    {
    }
    
    virtual ~AudioRendererImpl()
    {
    }
    
    virtual void SetSampleRate(int32_t sampleRate)
    {
        piAssert(sampleRate > 0, ;);
        
        std::lock_guard<std::mutex> lock(mMutex);
        piAssert(mState == eState_Idle, ;);
        
        mSampleRate = sampleRate;
    }
    
    virtual int32_t GetSampleRate() const
    {
        std::lock_guard<std::mutex> lock(mMutex);
        return mSampleRate;
    }
    
    virtual void SetBufferEnough(int64_t size)
    {
        piAssert(size > 0, ;);
        std::lock_guard<std::mutex> lock(mMutex);
        mBufferEnough = size;
    }
    
    virtual int64_t GetBufferEnough() const
    {
        std::lock_guard<std::mutex> lock(mMutex);
        return mBufferEnough;
    }
    
    virtual void SetBufferCapacity(int64_t size)
    {
        piAssert(size > 0, ;);
        std::lock_guard<std::mutex> lock(mMutex);
        mBufferCapacity = size;
    }
    
    virtual int64_t GetBufferCapacity() const
    {
        std::lock_guard<std::mutex> lock(mMutex);
        return mBufferCapacity;
    }
    
    virtual int GetState() const
    {
        std::lock_guard<std::mutex> lock(mMutex);
        return mState;
    }
    
    virtual void Finish()
    {
        std::lock_guard<std::mutex> lock(mMutex);
        mFinish = true;
    }
    
public:
    
    bool IsFinish() const
    {
        std::lock_guard<std::mutex> lock(mMutex);
        return mFinish;
    }
    
    void ResetFinish()
    {
        std::lock_guard<std::mutex> lock(mMutex);
        mFinish = false;
    }
    
protected:
    
    void SetState(int state)
    {
        std::lock_guard<std::mutex> lock(mMutex);
        if (state != mState)
        {
            mState = state;
        }
    }
    
    bool IsBufferEnough() const
    {
        std::lock_guard<std::mutex> lock(mMutex);
        return mFinish || mBufferSize >= mBufferEnough;
    }
};

NSPI_END()


#endif
