//////////////////////////////////////////////////////////////////////////////////
// OpenGL Mathematics Copyright (c) 2005 - 2013 G-Truc Creation (www.g-truc.net)
//////////////////////////////////////////////////////////////////////////////////
// Created : 2007-09-28
// Updated : 2008-10-07
// Licence : This source is under MIT License
// File    : glm/gtx/normalize_dot_inl.h
//////////////////////////////////////////////////////////////////////////////////

namespace piglm
{
	template <typename genType> 
	GLM_FUNC_QUALIFIER genType normalizeDot
	(
		genType const & x, 
		genType const & y
	)
	{
		return 
			piglm::dot(x, y) * 
			piglm::inversesqrt(piglm::dot(x, x) * 
			piglm::dot(y, y));
	}

	template <typename T, precision P>
	GLM_FUNC_QUALIFIER T normalizeDot
	(
		detail::tvec2<T, P> const & x, 
		detail::tvec2<T, P> const & y
	)
	{
		return 
			piglm::dot(x, y) * 
			piglm::inversesqrt(piglm::dot(x, x) * 
			piglm::dot(y, y));
	}

	template <typename T, precision P>
	GLM_FUNC_QUALIFIER T normalizeDot
	(
		detail::tvec3<T, P> const & x, 
		detail::tvec3<T, P> const & y
	)
	{
		return 
			piglm::dot(x, y) * 
			piglm::inversesqrt(piglm::dot(x, x) * 
			piglm::dot(y, y));
	}

	template <typename T, precision P>
	GLM_FUNC_QUALIFIER T normalizeDot
	(
		detail::tvec4<T, P> const & x, 
		detail::tvec4<T, P> const & y
	)
	{
		return 
			piglm::dot(x, y) * 
			piglm::inversesqrt(piglm::dot(x, x) * 
			piglm::dot(y, y));
	}

	template <typename genType> 
	GLM_FUNC_QUALIFIER genType fastNormalizeDot
	(
		genType const & x, 
		genType const & y
	)
	{
		return 
			piglm::dot(x, y) * 
			fastInverseSqrt(piglm::dot(x, x) * 
			piglm::dot(y, y));
	}

	template <typename T, precision P>
	GLM_FUNC_QUALIFIER T fastNormalizeDot
	(
		detail::tvec2<T, P> const & x, 
		detail::tvec2<T, P> const & y
	)
	{
		return 
			piglm::dot(x, y) * 
			fastInverseSqrt(piglm::dot(x, x) * 
			piglm::dot(y, y));
	}

	template <typename T, precision P>
	GLM_FUNC_QUALIFIER T fastNormalizeDot
	(
		detail::tvec3<T, P> const & x, 
		detail::tvec3<T, P> const & y
	)
	{
		return 
			piglm::dot(x, y) * 
			fastInverseSqrt(piglm::dot(x, x) * 
			piglm::dot(y, y));
	}

	template <typename T, precision P>
	GLM_FUNC_QUALIFIER T fastNormalizeDot
	(
		detail::tvec4<T, P> const & x, 
		detail::tvec4<T, P> const & y
	)
	{
		return 
			piglm::dot(x, y) * 
			fastInverseSqrt(piglm::dot(x, x) * 
			piglm::dot(y, y));
	}
}//namespace piglm
