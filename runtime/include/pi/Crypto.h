/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2012.10.19   0.1     Create
 ******************************************************************************/
#ifndef PI_CRYPTO_H
#define PI_CRYPTO_H

#include <pi/Core.h>

namespace nspi
{
    struct iHash : public iRefObject
    {
        virtual ~iHash() {}
        
        virtual void Update(const void* data, int64_t size) = 0;
        
        virtual int64_t Final(void* buffer, int64_t size) = 0;
        
        virtual std::string FinalHex() = 0;
        
        virtual void Reset() = 0;
        
        virtual int GetName() const = 0;
        
        virtual int64_t GetBytes() const = 0;
    };
    
    
    enum
    {
        eHash_CRC32,
        eHash_MD5,
    };
    
    iHash* CreateHash(int name);
    
    
    //==========================================================================
    
    enum
    {
        eEncoder_Base64,
        eDecoder_Base64,
        eEncoder_Blowfish,
        eDecoder_Blowfish,
    };
    
    iStream* CreateCipherStream(iStream* source, int name, iMemory* key, iMemory* iv);
}

#endif

























