/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.16   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_CV_CALIBRATE_H
#define PI_GAME_CV_CALIBRATE_H

namespace nspi
{
    PIInterface()
    struct iCalibrate : public iRefObject
    {
        PI_DEFINE(iCalibrate)
                
        virtual void Update(iFaceTrackerResult* result) = 0;
        
        virtual mat4 GetViewMatrix(int32_t index) const = 0;
        
        virtual void SetViewportSize(const vec2& size) = 0;
    };
    
    iCalibrate* CreateCalibrate();
    
    PIFunction()
    mat4 piProject(double left, double right, double bottom, double top, double near, double far);
}

#endif






























