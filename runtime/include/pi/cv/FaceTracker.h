/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.16   0.1     Create
 ******************************************************************************/
#ifndef PI_CV_FACETRACKER_H
#define PI_CV_FACETRACKER_H

namespace nspi
{
    struct iTexture2D;
    
    PIEnum()
    enum eHumanAction
    {
        eHumanAction_EyeBlink    = piBit(0),
        eHumanAction_MouthOpen   = piBit(1),
        eHumanAction_HeadYaw     = piBit(2),
        eHumanAction_HeadPitch   = piBit(3),
        eHumanAction_BrowJump    = piBit(4),
        eHumanAction_HandGood    = piBit(5),
        eHumanAction_HandPalm    = piBit(6),
        eHumanAction_HandLove    = piBit(7),
        eHumanAction_HandHoldup  = piBit(8),
        eHumanAction_HandCong    = piBit(9), // congratulate
        eHumanAction_FingerHeart = piBit(10),
        eHumanAction_Kiss        = piBit(11),
    };
    
    PIInterface()
    struct iFaceInfo : public iRefObjectReadonly
    {
        PI_DEFINE(iFaceInfo)
        
        /**
         * 人脸106关键点的数组
         * @export
         */
        PIMethod()
        virtual iVec3Array* GetPoints() const = 0;
        
        virtual void SetPoints(iVec3Array* points) = 0;
        
        /**
         * 代表面部的矩形区域
         * @export
         */
        PIMethod()
        virtual rect GetRect() const = 0;
        
        virtual void SetRect(const rect& rect) = 0;
        
        /**
         * 水平转角，真实度量的左负右正
         * @export
         */
        PIMethod()
        virtual float GetYaw() const = 0;
        
        virtual void SetYaw(float degrees) = 0;
        
        /**
         * 俯仰角，真实度量的上负下正
         * @export
         */
        PIMethod()
        virtual float GetPitch() const = 0;
        
        virtual void SetPitch(float value) = 0;
        
        /**
         * 旋转角，真实度量的左负右正
         * @export
         */
        PIMethod()
        virtual float GetRoll() const = 0;
        
        virtual void SetRoll(float value) = 0;
        
        // 两眼间距
        PIMethod()
        virtual float GetEyeDistance() const = 0;
        
        virtual void SetEyeDistance(float value) = 0;
        
        PIMethod()
        virtual int32_t GetTrackID() const = 0;
        
        virtual void SetTrackID(int32_t trackID) = 0;
        
        virtual void SetActionFlags(int32_t value) = 0;
        
        PIMethod()
        virtual int32_t GetActionFlags() const = 0;
        
        PIMethod()
        virtual bool ActionFlagsIs(int32_t actions) const = 0;
    };
    
    iFaceInfo* CreateFaceInfo();
    
    
    PIInterface()
    struct iHandInfo : public iRefObject
    {
        PI_DEFINE(iHandInfo)
        
        PIMethod()
        virtual rect GetRect() const = 0;
        
        virtual void SetRect(const rect& rect) = 0;
        
        PIMethod()
        virtual vec3 GetPoint() const = 0;

        virtual void SetPoint(const vec3& point) = 0;

        PIMethod()
        virtual int32_t GetActionFlags() const = 0;
        
        PIMethod()
        virtual bool ActionFlagsIs(int32_t actions) const = 0;
        
        virtual void SetActionFlags(int32_t value) = 0;
        
        PIMethod()
        virtual float GetScore() const = 0;
        
        virtual void SetScore(float value) = 0;
        
        PIMethod()
        virtual float GetActionScore() const = 0;
        
        virtual void SetActionScore(float value) = 0;
    };
    
    iHandInfo* CreateHandInfo();
    
    
    PIInterface()
    struct iFaceTrackerResult : public iRefObjectReadonly
    {
        PI_DEFINE(iFaceTrackerResult)
        
        PIMethod()
        virtual int32_t GetFaceCount() const = 0;
        
        PIMethod()
        virtual iFaceInfo* GetFace(int32_t index) const = 0;
        
        virtual void AddFace(iFaceInfo* face) = 0;
        
        PIMethod()
        virtual iFaceInfo* FindFaceByTrackID(int32_t trackID) const = 0;
        
        
        
        PIMethod()
        virtual int32_t GetHandCount() const = 0;
        
        PIMethod()
        virtual iHandInfo* GetHand(int32_t index) const = 0;
        
        virtual void AddHand(iHandInfo* hand) = 0;
        
        
        PIMethod()
        virtual iGraphicsObject* GetBgMask() const = 0;
        
        virtual iTexture2D* GetBgMaskTexture() const = 0;
        
        virtual void SetBgMask(iGraphicsObject* object) = 0;
        
        PIMethod()
        virtual bool HasBgMask() const = 0;
        
        virtual void SetBgMaskAvailable(bool value) = 0;
        
        
        PIMethod()
        virtual mat4 GetBgMaskMatrix() const = 0;
        
        virtual void SetBgMaskMatrix(const mat4& matrix) = 0;
        
        PIMethod()
        virtual int32_t GetImageWidth() const = 0;
        
        virtual void SetImageWidth(int32_t width) = 0;
        
        PIMethod()
        virtual int32_t GetImageHeight() const = 0;
        
        virtual void SetImageHeight(int32_t height) = 0;
    };
    
    iFaceTrackerResult* CreateFaceTrackerResult();
    
    
    /**
     * Degree of home button
     *
     * +------+
     * |      |
     * |      |
     * | +--+ |
     * | +--+ |
     * +------+
     *    90
     */
    
    PIEnum()
    enum eOrient
    {
        eOrient_0   = 0,
        eOrient_90  = 90,
        eOrient_180 = 180,
        eOrient_270 = 270,
    };
    
    PIEnum()
    enum eFaceTracker
    {
        eFaceTracker_Face       = piBit(0),
        eFaceTracker_Bg         = piBit(1),
        eFaceTracker_FaceAction = piBit(2),
        eFaceTracker_HandAction = piBit(3),
    };
    
    struct iFaceTracker : public iRefObject
    {
        virtual ~iFaceTracker() {}
                        
        virtual void Reset() = 0;
        
        PISetter(Class = eFaceTracker)
        virtual void SetFeatures(int flags) = 0;
        
        PIGetter()
        virtual int GetFeatures() const = 0;
                
        virtual iFaceTrackerResult* Track(iBitmap *bitmap, int32_t orientation) = 0;
    };
    
}

#endif















