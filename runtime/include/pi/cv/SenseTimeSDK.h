/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.2   0.1     Create
 ******************************************************************************/
#ifndef PI_CV_SENSETIMESDK_H
#define PI_CV_SENSETIMESDK_H

namespace nspi
{
    
    bool InitSenseTimeSDK(const std::string& license);
    
    iFaceTracker* CreateFaceTrackerSenseTime(const std::string& modelPath);
}

#endif



































