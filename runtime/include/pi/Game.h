/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2013.3.31   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_H
#define PI_GAME_H

#include <pi/game/GameConfig.h>

#include <pi/Input.h>
#include <pi/DOM.h>
#include <pi/Graphics.h>

#include <pi/game/Component.h>
#include <pi/game/Entity.h>
#include <pi/game/System.h>
#include <pi/game/Scene.h>

#include <pi/game/Animation.h>
#include <pi/game/2D.h>
#include <pi/game/3D.h>
#include <pi/game/Scripting.h>
#include <pi/game/Camera.h>
#include <pi/game/Transform.h>
#include <pi/game/FaceCapture.h>
#include <pi/game/SceneParent.h>
#include <pi/game/Table.h>
#include <pi/game/FrameCapture.h>
#include <pi/game/Filter.h>
#include <pi/game/Particle.h>
#include <pi/game/ScriptComp.h>

#include <pi/game/base_system.h>

#include <pi/game/Serialization.h>

#define PI_GAME_TAG "PI-GAME"

namespace nspi
{
    PIEnum()
    enum eGameMsgPri
    {
        eGameMsgPri_Low,
        eGameMsgPri_Normal,
        eGameMsgPri_High,
    };
    
    PIEnum()
    enum eGameMsg
    {
        eGameMsg_HIDEvent = 1,
        eGameMsg_Resize,
        eGameMsg_Update,
        eGameMsg_Load,
        eGameMsg_Unload,
        eGameMsg_Pause,
        eGameMsg_Resume,
        eGameMsg_LoadScene,
        eGameMsg_UnloadScene,
        eGameMsg_CaptureFrame,
        eGameMsg_UpdateFrame,
        eGameMsg_SetGraphicsVM,
        eGameMsg_SetResContext,
        
        eGameMsg_User = 100000,
    };
    
    struct iGame : public iRefObject
    {
        virtual ~iGame() {}
        
        virtual void SetClassLoader(iClassLoader* loader) = 0;
        
        virtual iClassLoader* GetClassLoader() const = 0;
        
        virtual void PostMessage(iRefObject* sender,
                                 int priority,
                                 int32_t message,
                                 const Var& arg1 = Var(),
                                 const Var& arg2 = Var()) = 0;
        
        virtual void SetGraphicsVM(iGraphicsVM* vm) = 0;
                
        virtual rect GetBounds() const = 0;
        
        virtual void SetResourceContext(iGraphicsContext* context) = 0;
        
        virtual iGraphicsContext* GetResourceContext() const = 0;
        
        virtual void LoadSceneObject(iAssetManager* assetMgr, iScene* scene) = 0;
        
        virtual void LoadScene(iAssetManager* assetMgr, const std::string& uri) = 0;
        
        virtual void UnloadScene(const std::string& uri) = 0;
        
        virtual iAssetManager* GetAssetManager() const = 0;
        
        virtual void SetAssetManager(iAssetManager* manager) = 0;
                
        virtual iHID* GetHID() const = 0;
        
        virtual iMotionManager* GetMotionManager() const = 0;
        
        virtual void SwapBuffer() = 0;
                
        virtual int32_t GetFrontBufferTex() const = 0;
        
        virtual int32_t GetBackBufferTex() const = 0;
        
        virtual void SetBufferMatrix(const mat4& matrix) = 0;
        
#if PI_TIME_SYNC
        virtual void UpdateTime(float timeMS) = 0;
#endif
        
    };
}

#endif





















