/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.12   0.1     Create
 ******************************************************************************/
#ifndef PI_SCRIPTING_H
#define PI_SCRIPTING_H

#include <pi/Core.h>

namespace nspi
{
    PIInterface(HasCreator = false)
    struct iScriptRef : public iRefObject
    {
        PI_DEFINE(iScriptRef)
        
        virtual Var Call(const std::string& name,
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var()) = 0;
    };
    
    PIInterface(HasCreator = false)
    struct iScriptLoader : public iRefObject
    {
        PI_DEFINE(iScriptLoader)
        
        virtual iMemory* Load(const std::string& path) = 0;
    };
    
    PIInterface(HasCreator = false)
    struct iScript : public iRefObject
    {
        PI_DEFINE(iScript)
        
        virtual bool SetGlobal(const std::string& name, const Var& value) = 0;
        
        virtual Var GetGlobal(const std::string& name) const = 0;
        
        virtual void GarbageCollect() = 0;
        
        virtual void RegisterLoader(iScriptLoader* loader) = 0;

        virtual Var Call(const std::string& name,
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var()) = 0;
        
        virtual iScriptRef* EvaluateMemory(iMemory* content) = 0;
        
        virtual iScriptRef* EvaluateFile(const std::string& uri) = 0;
        
        virtual void OnUnload() = 0;
    };
}

#endif





























