/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2015.4.4   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_H
#define PI_CORE_H

#include <pi/PIConfig.h>

#include <pi/core/CoreTypes.h>
#include <pi/core/ToString.h>
#include <pi/core/System.h>
#include <pi/core/MessageSystem.h>
#include <pi/core/CoreMemory.h>
#include <pi/core/CoreTime.h>
#include <pi/core/Async.h>
#include <pi/core/Log.h>
#include <pi/core/impl/StreamImpl.h>
#include <pi/core/impl/ArrayImpl.h>
#include <pi/core/impl/ReflectionImpl.h>
#include <pi/core/Algorithm.h>

#define NSPI_BEGIN() namespace nspi {
#define NSPI_END()   }

namespace nspi
{
    
    PIFunction()
    int32_t piGetVersionCode();
    
    PIFunction()
    std::string piGetVersionString();
    
}


#endif



















