/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2015.3.30   0.1     Create
 ******************************************************************************/
#ifndef PI_MEDIA_H
#define PI_MEDIA_H

#include <pi/Graphics.h>

#include <pi/media/MediaConfig.h>
#include <pi/media/MediaFrame.h>
#include <pi/media/AudioRenderer.h>
#include <pi/media/AudioEngine.h>

namespace nspi
{
    void piInitMedia();
}

#endif
