/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2012.2.27   0.1     Create
 ******************************************************************************/
#ifndef PI_DOM_H
#define PI_DOM_H

#include <pi/Core.h>

namespace nspi
{
    struct iDOMDocument;

    enum
    {
        eDOMNode_Element    = 1,
        eDOMNode_Attr       = 2,
        eDOMNode_CDATA      = 3,
        eDOMNode_Comment    = 4,
        eDOMNode_Document   = 5,
        eDOMNode_Text       = 6,
    };
    
    struct iDOMNode;
    struct iDOMElement;
    struct iDOMAttr;
    
    PI_DEFINE_OBJECT_ARRAY(iDOMNode);
    iDOMNodeArray* CreateDOMNodeArray();

    PI_DEFINE_OBJECT_ARRAY(iDOMAttr);
    iDOMAttrArray* CreateDOMAttrArray();

    PI_DEFINE_OBJECT_ARRAY(iDOMElement);
    iDOMElementArray* CreateDOMElementArray();

    /**
     * =============================================================================================
     * DOM Node
     */
    
    struct iDOMNode : public iRefObject
    {
        virtual ~iDOMNode() {}
       
        /**
         * @return 节点值
         */
        virtual std::string GetNodeValue() const = 0;

        virtual void SetNodeValue(const std::string &value) = 0;

        virtual std::string GetNodeName() const = 0;

        virtual void SetNodeName(const std::string &name) = 0;
        
        virtual std::string GetText() const = 0;

        virtual iDOMNode* GetFirstChild() const = 0;

        virtual iDOMNode* GetLastChild() const = 0;

        virtual iDOMNodeArray* GetChildNodes() const = 0;

        virtual iDOMNode* GetParent() const = 0;
        
        virtual void SetParent(iDOMNode* parent) = 0;

        virtual iDOMNode* GetNextSibling() const = 0;
        
        virtual void SetNextSibling(iDOMNode *node) = 0;

        virtual iDOMNode* GetPreviousSibling() const = 0;
        
        virtual void SetPreviousSibling(iDOMNode* node) = 0;

        virtual iDOMNode* InsertBefore(iDOMNode *newChild, iDOMNode *refChild) = 0;

        virtual iDOMNode* ReplaceChild(iDOMNode *newChild, iDOMNode *oldChild) = 0;

        virtual iDOMNode* RemoveChild(iDOMNode *oldChild) = 0;

        virtual iDOMNode* AppendChild(iDOMNode *newChild) = 0;

        virtual bool HasChildNodes() const = 0;

        virtual iDOMDocument* GetDocument() const = 0;

        virtual iDOMNode *NullNode() const = 0;

        virtual int GetNodeType() const = 0;
        
        virtual void FindElementsByTagName(const std::string &name,
                                           iDOMElementArray* elements,
                                           bool recursively) const= 0;
    };
    
    
    /**
     * =============================================================================================
     * DOM Attr
     */
    
    struct iDOMAttr : virtual public iDOMNode
    {
        virtual ~iDOMAttr() {}
        
        virtual std::string GetName() const = 0;

        virtual std::string GetValue() const = 0;

        virtual void SetValue(const std::string &value) = 0;
    };
    
    /**
     * =============================================================================================
     * DOM Text
     */

    struct iDOMText : virtual public iDOMNode
    {
        virtual ~iDOMText() {}

        virtual std::string GetData() const = 0;

        virtual void SetData(const std::string &data) = 0;
    };
    
    /**
     * =============================================================================================
     * DOM CDATA
     */

    struct iDOMCDATA : public iDOMText
    {
        virtual ~iDOMCDATA() {}
    };

    /**
     * =============================================================================================
     * DOM Element
     */
    
    struct iDOMElement : virtual public iDOMNode
    {
        virtual ~iDOMElement() {}

        virtual std::string GetTagName() const = 0;

        virtual std::string GetAttr(const std::string &name) const = 0;

        virtual void SetAttr(const std::string &name, const std::string &value) = 0;

        virtual void RemoveAttr(const std::string &name) = 0;

        virtual iDOMAttr* GetAttrNode(const std::string &name) const = 0;
    
        virtual iDOMAttr* SetAttrNode(iDOMAttr *attr) = 0;
        

        virtual iDOMAttr* RemoveAttrNode(iDOMAttr *attr) = 0;

        virtual iDOMAttrArray* GetAttrs() const = 0;

        virtual iDOMElementArray* GetElementsByTagName(const std::string &name, bool recursively) const = 0;

        virtual iDOMElement* GetElementByTagName(const std::string &name) const = 0;
    };
    
    

    struct iDOMDocument : virtual public iDOMNode
    {
        virtual ~iDOMDocument() {}

        virtual iDOMElement* GetDocumentElement() const = 0;

        virtual void SetDocumentElement(iDOMElement *element) = 0;

        virtual iDOMText* CreateText(const std::string &data) = 0;

        virtual iDOMCDATA* CreateCDATA(const std::string &data) = 0;

        virtual iDOMAttr* CreateAttr(const std::string &name, const std::string &value) = 0;

        virtual iDOMElement* CreateElement(const std::string &tagName) = 0;

        virtual iDOMElementArray* GetElementsByTagName(const std::string &name, bool recursively) const = 0;
        
        virtual iDOMElement* GetElementByTagName(const std::string& name, bool recursively) const = 0;
    };

    iDOMDocument* CreateDOMDocument();

    iDOMDocument* piParseXML(iStream *input);

    void piSerializeXML(iDOMDocument *doc, iStream* output);
}

#endif

























