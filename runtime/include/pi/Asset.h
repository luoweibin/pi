/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.1   0.1     Create
 ******************************************************************************/
#ifndef PI_ASSET_H
#define PI_ASSET_H

#include <pi/Graphics.h>
#include <pi/asset/AssetManager.h>
#include <pi/asset/MemoryAsset.h>
#include <pi/asset/BitmapAsset.h>
#include <pi/asset/GraphicsAsset.h>
#include <pi/asset/Texture.h>
#include <pi/asset/AnimSeq.h>

#endif























