/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.18   0.1     Create
 ******************************************************************************/
#ifndef PI_ASSET_ASSETMANAGER_H
#define PI_ASSET_ASSETMANAGER_H

namespace nspi
{
    struct iAssetManager;
    
    PIInterface(HasCreator = false)
    struct iAsset : public iRefObjectReadonly
    {
        PI_DEFINE(iAsset)
        
        PIGetter()
        virtual int32_t GetVersion() const = 0;
        
        PISetter()
        virtual void SetVersion(int32_t version) = 0;
        
        PISetter()
        virtual void SetUri(const std::string& uri) = 0;
        
        PIGetter()
        virtual std::string GetUri() const = 0;
        
        PIGetter()
        virtual iAssetManager* GetManager() const = 0;
        
        PISetter()
        virtual void SetManager(iAssetManager* manager) = 0;
    };
    
    
    /**
     * --------------------------------------------------------------------------
     * Asset Loader
     */
    
    struct iAssetLoader : public iRefObject
    {
        virtual ~iAssetLoader() {}
        
        virtual iAsset* Load(iAssetManager* manager, iClassLoader* classLoader, iStream* stream) = 0;
    };
    
    struct iAssetLoaderFactory : public iRefObject
    {
        virtual ~iAssetLoaderFactory() {}
        
        virtual iAssetLoader* CreateLoader(const std::string& fileExt, iStream* stream) = 0;
    };
    
    iAssetLoaderFactory* piGetDefaultAssetLoaderFactory();
    
    
    /**
     * --------------------------------------------------------------------------
     * Uri Resolver
     */
    
    struct iUriResolver : public iRefObject
    {
        virtual ~iUriResolver() {}
        
        virtual iStream* Open(const std::string& uri) = 0;
        
        virtual std::string Resolve(const std::string& uri) = 0;
    };
    
    iUriResolver* CreateDirectoryResolver(const std::string& dir);
    
    iUriResolver* CreateShareResolver(const std::string& dir);
    
    iUriResolver* CreateZipResolver(iStream* source);
    
    /**
     * --------------------------------------------------------------------------
     * Asset Manager
     */
    
    PIInterface()
    struct iAssetManager : public iRefObject
    {
        PI_DEFINE(iAssetManager)
        
        virtual void SetParent(iAssetManager* parent) = 0;
        
        virtual iAssetManager* GetParent() const = 0;
        
        PIMethod()
        virtual void OnLoad() = 0;
        
        PIMethod()
        virtual void OnUnload() = 0;
        
        /**
         * Load asset synchronouslly.
         * Return nullptr if asset not exists.
         */
        PIMethod()
        virtual iAsset* SyncLoad(iClassLoader* classLoader,
                                 const std::string& uri,
                                 bool cache = true) = 0;
        
        /**
         * Load asset asynchronouslly.
         * Return false if asset not exists.
         */
        virtual iFuture* AsyncLoad(iClassLoader* classLoader,
                                   const std::string& uri,
                                   bool cache = true) = 0;
        
        /**
         * Resovle path
         */
        PIMethod()
        virtual std::string Resolve(const std::string& uri) = 0;
        
        /**
         * Register asset object.
         * Return false if asset of uri already exists.
         */
        virtual bool SetAsset(const std::string& uri, const Var& asset) = 0;
        
        /**
         * Send a unload request. This is an asynchronous operation.
         */
        virtual void Unload(const std::string& uri) = 0;
        
        /**
         * Register URI resolver.
         */
        virtual void AddUriResolver(iUriResolver* resolver) = 0;
        
        /**
         * Register Loader Factory
         */
        virtual void AddLoaderFactory(iAssetLoaderFactory* factory) = 0;
    };
    
    iAssetManager* CreateAssetManager();
    
    template <class T>
    static inline T* piLoadAsset(iAssetManager* manager,
                                 iClassLoader* classLoader,
                                 const std::string& uri,
                                 bool cache = true)
    {
        return dynamic_cast<T*>(manager->SyncLoad(classLoader, uri, cache));
    }
}


#endif



























