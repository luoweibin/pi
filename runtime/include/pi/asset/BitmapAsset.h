/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.19   0.1     Create
 ******************************************************************************/
#ifndef PI_ASSET_BITMAPASSET_H
#define PI_ASSET_BITMAPASSET_H

namespace nspi
{
    
    PIInterface()
    struct iBitmapAsset : public iAsset
    {
        PI_DEFINE(iBitmapAsset)
        
        PIGetter()
        virtual iBitmap* GetBitmap() const = 0;
        
        PISetter()
        virtual void SetBitmap(iBitmap* bitmap) = 0;
    };
    
    iBitmapAsset* CreateBitmapAsset();
}

#endif
