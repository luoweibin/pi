/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.30   0.1     Create
 ******************************************************************************/
#ifndef PI_ASSET_ANIMSEQ_H
#define PI_ASSET_ANIMSEQ_H

namespace nspi
{
    
    PIInterface()
    struct iAnimSeq : public iAsset
    {
        PI_DEFINE(iAnimSeq)
        
        PIGetter()
        virtual int32_t GetFPS() const = 0;
        
        PISetter()
        virtual void SetFPS(int32_t value) = 0;
        
        PIGetter()
        virtual iTexture2DArray* GetFrames() const = 0;
        
        PISetter()
        virtual void SetFrames(iTexture2DArray* frames) = 0;
        
        PIMethod()
        virtual iTexture2D* GetFrame(int32_t index) const = 0;
    };
    
    iAnimSeq* CreateAnimSeq();
}

#endif
































