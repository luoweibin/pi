/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.10   0.1     Create
 ******************************************************************************/
#ifndef PI_ASSET_TEXTURE_H
#define PI_ASSET_TEXTURE_H

namespace nspi
{
    
    PIFunction()
    int32_t CreateColorTexture(const vec4& color);
    
    PIFunction()
    int32_t CreateColorTextureEx(const vec4& color, int width, int height, int texture = 0);
    
    PIInterface(Alias = Texture, HasCreator = false)
    struct iTexture : public iGraphicsAsset
    {
        PI_DEFINE(iTexture)
        
        PIGetter(Class = eTexValue)
        virtual int GetWrapS() const = 0;
        
        PISetter()
        virtual void SetWrapS(int value) = 0;
        
        PIGetter(Class = eTexValue)
        virtual int GetWrapT() const = 0;
        
        PISetter()
        virtual void SetWrapT(int value) = 0;
        
        PIGetter(Class = eTexValue)
        virtual int GetWrapR() const = 0;
        
        PISetter()
        virtual void SetWrapR(int value) = 0;
        
        PIGetter(Class = eTexValue)
        virtual int GetMinFilter() const = 0;
        
        PISetter()
        virtual void SetMinFilter(int value) = 0;
        
        PIGetter(Class = eTexValue)
        virtual int GetMagFilter() const = 0;
        
        PISetter()
        virtual void SetMagFilter(int value) = 0;
        
        PIGetter(Class = ePixelFormat)
        virtual int GetFormat() const = 0;
        
        PISetter()
        virtual void SetFormat(int format) = 0;
    };
    
    
    PIInterface(Alias = Texture2D)
    struct iTexture2D : public iTexture
    {
        PI_DEFINE(iTexture2D)
      
        PIGetter()
        virtual std::string GetSource() const = 0;
        
        PISetter()
        virtual void SetSource(const std::string& uri) = 0;
        
        PIGetter()
        virtual int32_t GetWidth() const = 0;
        
        PISetter()
        virtual void SetWidth(int32_t width) = 0;
        
        PIGetter()
        virtual int32_t GetHeight() const = 0;
        
        PISetter()
        virtual void SetHeight(int32_t height) = 0;
    };
    
    iTexture2D* CreateTexture2D();
    
    
    PI_DEFINE_OBJECT_ARRAY(iTexture2D);
    iTexture2DArray* CreateTexture2DArray();
    
    
    PIInterface(Alias = CubeMap)
    struct iCubeMap : public iTexture
    {
        PI_DEFINE(iCubeMap)
        
        PIGetter()
        virtual std::string GetPositiveX() const = 0;
        
        PISetter()
        virtual void SetPositiveX(const std::string& name) = 0;
        
        PIGetter()
        virtual std::string GetNegativeX() const = 0;
        
        PISetter()
        virtual void SetNegativeX(const std::string& name) = 0;
        
        
        PIGetter()
        virtual std::string GetPositiveY() const = 0;
        
        PISetter()
        virtual void SetPositiveY(const std::string& name) = 0;
        
        PIGetter()
        virtual std::string GetNegativeY() const = 0;
        
        PISetter()
        virtual void SetNegativeY(const std::string& name) = 0;
        
        
        PIGetter()
        virtual std::string GetPositiveZ() const = 0;
        
        PISetter()
        virtual void SetPositiveZ(const std::string& name) = 0;
        
        PIGetter()
        virtual std::string GetNegativeZ() const = 0;
        
        PISetter()
        virtual void SetNegativeZ(const std::string& name) = 0;
    };
    
    iCubeMap* CreateCubeMap();
    
    PIEnum()
    enum eRenderTextureDimession
    {
        eRenderTextureDimession_2D,
        eRenderTextureDimession_Cube,
        eRenderTextureDimession_3D,
    };
    
    PIEnum()
    enum eRenderTextureDepthFormat
    {
        eRenderTextureDepthFormat_None,
        eRenderTextureDepthFormat_D16,
        eRenderTextureDepthFormat_D24S8,
    };
    
    PIInterface(Alias = RenderTexture)
    struct iRenderTexture : public iTexture
    {
        PI_DEFINE(iRenderTexture)
        
        PIGetter()
        virtual bool IsFixScreenBounds() const  = 0;
        
        PISetter()
        virtual void SetFixScreenBounds(bool flag) = 0;
        
        PIGetter()
        virtual int32_t GetWidth() const = 0;
        
        PISetter()
        virtual void SetWidth(int32_t width) = 0;
        
        PIGetter()
        virtual int32_t GetHeight() const = 0;
        
        PISetter()
        virtual void SetHeight(int32_t height) = 0;
        
        // For 3D Texture's depth, reserved.
        PIGetter()
        virtual int32_t GetDepth() const = 0;
        
        PISetter()
        virtual void SetDepth(int32_t depth) = 0;
        
        PIGetter(Class = eRenderTextureDepthFormat)
        virtual int32_t GetDepthFormat() const = 0;
        
        PISetter()
        virtual void SetDepthFormat(int32_t format) = 0;
        
        PIGetter(Class = eRenderTextureDimession)
        virtual int32_t GetDimession() const  = 0;
        
        PISetter()
        virtual void SetDimession(int32_t dimession) = 0;
        
        PIMethod()
        virtual bool DoCreate() = 0;
        
        PIMethod()
        virtual bool Resize(int32_t width, int32_t height) = 0;
        
        PIMethod()
        virtual int32_t GetFBO() = 0;
        
        
    };
    
    iRenderTexture* CreateRenderTexture();
    
    void AddToSourceRenderTexture(const std::string& uri, iRenderTexture* texture);
    
    iRenderTexture* GetRenderTexture(const std::string& uri);
    
    void ClearRenderTextureTable();

}

#endif
















