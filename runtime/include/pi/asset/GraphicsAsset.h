/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.18   0.1     Create
 ******************************************************************************/
#ifndef PI_GRAPHICS_GRAPHICSASSET_H
#define PI_GRAPHICS_GRAPHICSASSET_H

namespace nspi
{

    PIInterface(HasCreator = false)
    struct iGraphicsAsset : public iAsset
    {
        PI_DEFINE(iGraphicsAsset)
        
        PIGetter()
        virtual int32_t GetGraphicsName() const = 0;
        
        PISetter()
        virtual void SetGraphicsName(int32_t name) = 0;
    };
    
    
    PIInterface()
    struct iShaderProgram : public iGraphicsAsset
    {
        PI_DEFINE(iShaderProgram)
        
        PIGetter()
        virtual std::string GetVertexShader() const = 0;
        
        PISetter()
        virtual void SetVertexShader(const std::string& uri) = 0;
        
        PIGetter()
        virtual std::string GetFragShader() const = 0;
        
        PISetter()
        virtual void SetFragShader(const std::string& uri) = 0;
        
        PIGetter()
        virtual iTable* GetAttrs() const = 0;
        
        PISetter()
        virtual void SetAttrs(iTable* attrs) = 0;
    };
    
    iShaderProgram* CreateShaderProgram();
    
    iShaderProgram* CreateDefaultShaderProgram();
}

#endif

























