/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.19   0.1     Create
 ******************************************************************************/
#ifndef PI_ASSET_MEMORYASSET_H
#define PI_ASSET_MEMORYASSET_H

namespace nspi
{
    
    PIInterface()
    struct iMemoryAsset : public iAsset
    {
        PI_DEFINE(iMemoryAsset)
        
        PIGetter()
        virtual iMemory* GetMemory() const = 0;
        
        PISetter()
        virtual void SetMemory(iMemory* memory) = 0;
        
        virtual char* Ptr() const = 0;
        
        virtual int64_t Size() const = 0;
    };
    
    iMemoryAsset* CreateMemoryAsset();
}

#endif

























