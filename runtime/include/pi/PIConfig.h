/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin      2016.10.10   0.1     Create
 ******************************************************************************/
#ifndef PI_CONFIG_H
#define PI_CONFIG_H

#define PI_TAG "PI"

// Enable c++ reflection support.
#ifndef PI_REFLECTION_ENABLED
#   define PI_REFLECTION_ENABLED    1
#endif

// Enable lua scripting
#ifndef PI_LUA_ENABLED
#   define PI_LUA_ENABLED   1
#endif

// piFormat buffer size.
#ifndef PI_FORMAT_BUFFER
#   define PI_FORMAT_BUFFER        (1024*4)
#endif

// Maxium size of single log message.
#ifndef PI_LOG_MESSAGE_MAXSIZE
#   define PI_LOG_MESSAGE_MAXSIZE  4096
#endif

// Schedule interval of logging
#ifndef PI_LOG_SCHEDULE_INTERVAL
#   define PI_LOG_SCHEDULE_INTERVAL    100
#endif

// enable graphics marker for debugging.
#ifndef PI_GRAPHICS_MARKER_ENABLED
#   define PI_GRAPHICS_MARKER_ENABLED  1
#endif

// Float compare pricision
#ifndef PI_EPSILON
#   define PI_EPSILON  0.000001
#endif

#ifndef PI_THREAD_LOCAL 
#   define PI_THREAD_LOCAL 0
#endif

#ifndef PI_MAX_JOINTS
#   define PI_MAX_JOINTS 4
#endif

// Enable face tracker
#ifndef PI_FACETRACKER_ENABLED
#   define PI_FACETRACKER_ENABLED 0
#endif

#ifndef PI_TIME_SYNC
#   define PI_TIME_SYNC 0
#endif

#endif





















