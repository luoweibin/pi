/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.12   0.1     Create
 ******************************************************************************/
#ifndef PI_LUA_LUASCRIPT_H
#define PI_LUA_LUASCRIPT_H

#include <pi/Scripting.h>

#define PI_LUA_TAG "PI-LUA"

typedef struct lua_State lua_State;

namespace nspi
{
    
    PIInterface()
    struct iLuaScript : public iScript
    {
        PI_DEFINE(iLuaScript)
        
        virtual lua_State* GetState() const = 0;
    };
    
    iLuaScript* CreateLuaScript();
    
}


#endif













