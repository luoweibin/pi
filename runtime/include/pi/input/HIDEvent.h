/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.22   0.1     Create
 ******************************************************************************/
#ifndef PI_INPUT_HIDEVENT_H
#define PI_INPUT_HIDEVENT_H

namespace nspi
{

    PIEnum()
    enum eEventFlag
    {
        eEventFlag_Control         = piBit(0),
        eEventFlag_Shift           = piBit(1),
        eEventFlag_AltOption       = piBit(2),
        eEventFlag_NumLock         = piBit(3),
        eEventFlag_CapsLock        = piBit(4),
        eEventFlag_ScrollLock      = piBit(5),
        eEventFlag_Function        = piBit(6),
        eEventFlag_WindowCommand   = piBit(7),
    };
    
    int piMapEventFlags(int systemFlags);

    
    PIEnum()
    enum eHIDEvent
    {
        eHIDEvent_LeftMouseDown         = 1,
        eHIDEvent_LeftMouseUp           = 2,
        eHIDEvent_RightMouseDown        = 3,
        eHIDEvent_RightMouseUp          = 4,
        eHIDEvent_MouseMoved            = 5,
        eHIDEvent_LeftMouseDragged      = 6,
        eHIDEvent_RightMouseDragged     = 7,
        eHIDEvent_MouseEntered          = 8,
        eHIDEvent_MouseExited           = 9,
        eHIDEvent_KeyDown               = 10,
        eHIDEvent_KeyUp                 = 11,
        eHIDEvent_ScrollWheel           = 12,
        eHIDEvent_Magnify               = 13,
        eHIDEvent_Swipe                 = 14,
        eHIDEvent_Rotate                = 15,
        eHIDEvent_BeginGesture          = 16,
        eHIDEvent_EndGesture            = 17,
        eHIDEvent_SmartMagnify          = 18,
        eHIDEvent_Tap                   = 19,
        eHIDEvent_KeyModifierChanged    = 20,
        eHIDEvent_FaceTrackerChanged    = 21,
        eHIDevent_AudioLevelChanged     = 22,
        eHIDEvent_VideoFrameChanged     = 23,
        eHIDEvent_FilteredFrameChanged  = 24,
    };
    
    PIInterface(HasCreator = false)
    struct iHIDEvent : public iRefObjectReadonly
    {
        PI_DEFINE(iHIDEvent)
        
        PIMethod()
        virtual int GetID() const = 0;
        
        PIMethod()
        virtual void SetID(int eventID) = 0;
        
        PIMethod()
        virtual int64_t GetTime() const = 0;
        
        PIMethod()
        virtual void SetTime(int64_t timeMS) = 0;
        
        PIMethod()
        virtual void SetFlags(int32_t flags) = 0;
        
        PIMethod()
        virtual int32_t GetFlags() const = 0;
        
        PIMethod()
        virtual bool IsFlagsOn(int32_t flags) const = 0;
    };
}


#endif



































