/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.22   0.1     Create
 ******************************************************************************/
#ifndef PI_INPUT_IMPL_HIDEVENTIMPL_H
#define PI_INPUT_IMPL_HIDEVENTIMPL_H

#include <pi/Input.h>

namespace nspi
{
    template <class I>
    class HIDEventImpl : public I
    {
    private:
        int mID;
        int64_t mTimeMS;
        int32_t mFlags;
        
    protected:
        using I::IsReadonly;
        
    public:
        HIDEventImpl():
        mTimeMS(0), mID(-1), mFlags(0)
        {
        }
        
        virtual ~HIDEventImpl()
        {
        }
        
        virtual int64_t GetTime() const
        {
            return mTimeMS;
        }
        
        virtual void SetTime(int64_t timeMS)
        {
            piAssert(!IsReadonly(), ;);
            mTimeMS = timeMS;
        }
        
        virtual int GetID() const
        {
            return mID;
        }
        
        virtual void SetID(int eventID)
        {
            piAssert(!IsReadonly(), ;);
            mID = eventID;
        }
        
        virtual int32_t GetFlags() const
        {
            return mFlags;
        }
        
        virtual void SetFlags(int32_t flags)
        {
            mFlags = flags;
        }
        
        virtual bool IsFlagsOn(int32_t flags) const
        {
            return piFlagIs(mFlags, flags);
        }
    };
}



#endif













