/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.22   0.1     Create
 ******************************************************************************/
#ifndef PI_INPUT_AVCAPTURE_H
#define PI_INPUT_AVCAPTURE_H

namespace nspi
{
    
    PIInterface()
    struct iNativeTexture : public iRefObjectReadonly
    {
        PI_DEFINE(iNativeTexture)
        
        PIMethod()
        virtual int32_t GetName() const = 0;
        
        PIMethod()
        virtual void SetName(int32_t name) = 0;
        
        PIMethod()
        virtual int32_t GetWidth() const = 0;
        
        PIMethod()
        virtual void SetWidth(int32_t width) = 0;
        
        PIMethod()
        virtual int32_t GetHeight() const = 0;
        
        PIMethod()
        virtual void SetHeight(int32_t height) = 0;
        
        PIMethod()
        virtual int GetFormat() const = 0;
        
        PIMethod()
        virtual void SetFormat(int format) = 0;
        
        PIMethod()
        virtual int GetOrient() const = 0;
        
        PIMethod()
        virtual void SetOrient(int orient) = 0;
    };
    
    iNativeTexture* CreateNativeTexture();
    
    PIInterface()
    struct iFilteredFrameEvent : public iHIDEvent
    {
        PI_DEFINE(iFilteredFrameEvent)
        
        PIMethod()
        virtual void SetTexture(iNativeTexture* texture) = 0;
        
        PIMethod()
        virtual iNativeTexture* GetTexture() const = 0;
    };
    
    iFilteredFrameEvent* CreateFilteredFrameEvent();
    
    
    PIInterface()
    struct iAVCaptureEvent : public iHIDEvent
    {
        PI_DEFINE(iAVCaptureEvent)
        
        PIMethod()
        virtual void SetAudioLevel(double level) = 0;
        
        PIMethod()
        virtual double GetAudioLevel() const = 0;
        
        PIMethod()
        virtual iVideoFrame* GetVideoFrame() const = 0;
        
        PIMethod()
        virtual void SetVideoFrame(iVideoFrame* frame) = 0;
        
        PIMethod()
        virtual void SetVideoOrient(int orient) = 0;
        
        PIMethod()
        virtual int GetVideoOrient() const = 0;
    };
    
    iAVCaptureEvent* CreateAVCaptureEvent();
}


#endif
































