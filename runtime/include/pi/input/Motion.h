/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.22   0.1     Create
 ******************************************************************************/
#ifndef PI_INPUT_MONTH_H
#define PI_INPUT_MONTH_H

namespace nspi
{
    PIEnum()
    enum eAccuracy
    {
        eAccuracy_None,
        eAccuracy_Low,
        eAccuracy_Medium,
        eAccuracy_High,
    };
    
    PIInterface()
    struct iDeviceMotion : public iRefObjectReadonly
    {
        PI_DEFINE(iDeviceMotion)
        
        PIMethod()
        virtual vec3 GetAttitude() const = 0;
        
        virtual void SetAttitude(const vec3& value) = 0;
        
        PIMethod()
        virtual quat GetAttitudeQuat() const = 0;
        
        virtual void SetAttitudeQuat(const quat& value) = 0;
        
        PIMethod()
        virtual vec3 GetRotationRate() const = 0;
        
        virtual void SetRotationRate(const vec3& rate) = 0;
        
        // user acceleration, total acceleration = gravity + user acceleration
        virtual void SetGravity(const vec3& value) = 0;
        
        PIMethod()
        virtual vec3 GetGravity() const = 0;
        
        virtual void SetUserAccel(const vec3& value) = 0;
        
        PIMethod()
        virtual vec3 GetUserAccel() const = 0;
        
        // maganetic field
        virtual void SetMagField(const vec3& value) = 0;
        
        PIMethod()
        virtual vec3 GetMagField() const = 0;
        
        virtual void SetMagAccuracy(int value) = 0;
        
        PIGetter()
        virtual int GetMagAccuracy() const = 0;
    };
    
    iDeviceMotion* CreateDeviceMotion();

    
    PIInterface()
    struct iMotionManager : public iRefObject
    {
        PI_DEFINE(iMotionManager)
        
        PIMethod()
        virtual void Start() = 0;
        
        PIMethod()
        virtual void Stop() = 0;
        
        PIMethod()
        virtual bool IsActive() const = 0;
        
        PIMethod()
        virtual bool IsAvailable() const = 0;
        
        PIMethod()
        virtual iDeviceMotion* GetMotion() const = 0;
        
        virtual void OnUpdate() = 0;
    };
    
    iMotionManager* CreateMotionManager();
}

#endif



























