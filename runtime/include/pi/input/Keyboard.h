/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.22   0.1     Create
 ******************************************************************************/
#ifndef PI_INPUT_KEYBOARD_H
#define PI_INPUT_KEYBOARD_H

namespace nspi
{
    
    PIEnum()
    enum eKey
    {
        eKey_Unknown              = 0xFFFFFFFF,
        eKey_A                    = 0x00,
        eKey_S                    = 0x01,
        eKey_D                    = 0x02,
        eKey_F                    = 0x03,
        eKey_H                    = 0x04,
        eKey_G                    = 0x05,
        eKey_Z                    = 0x06,
        eKey_X                    = 0x07,
        eKey_C                    = 0x08,
        eKey_V                    = 0x09,
        eKey_B                    = 0x0B,
        eKey_Q                    = 0x0C,
        eKey_W                    = 0x0D,
        eKey_E                    = 0x0E,
        eKey_R                    = 0x0F,
        eKey_Y                    = 0x10,
        eKey_T                    = 0x11,
        eKey_1                    = 0x12,
        eKey_2                    = 0x13,
        eKey_3                    = 0x14,
        eKey_4                    = 0x15,
        eKey_6                    = 0x16,
        eKey_5                    = 0x17,
        eKey_Equals               = 0x18,
        eKey_9                    = 0x19,
        eKey_7                    = 0x1A,
        eKey_Minus                = 0x1B,
        eKey_8                    = 0x1C,
        eKey_0                    = 0x1D,
        eKey_RightBracket         = 0x1E,
        eKey_O                    = 0x1F,
        eKey_U                    = 0x20,
        eKey_LeftBracket          = 0x21,
        eKey_I                    = 0x22,
        eKey_P                    = 0x23,
        eKey_L                    = 0x25,
        eKey_J                    = 0x26,
        eKey_Quote                = 0x27,
        eKey_K                    = 0x28,
        eKey_Semicolon            = 0x29,
        eKey_Backslash            = 0x2A,
        eKey_Comma                = 0x2B,
        eKey_Slash                = 0x2C,
        eKey_N                    = 0x2D,
        eKey_M                    = 0x2E,
        eKey_Period               = 0x2F,
        eKey_Grave                = 0x32,
        eKey_KeypadDecimal        = 0x41,
        eKey_KeypadMultiply       = 0x43,
        eKey_KeypadPlus           = 0x45,
        eKey_KeypadClear          = 0x47,
        eKey_KeypadDivide         = 0x4B,
        eKey_KeypadEnter          = 0x4C,
        eKey_KeypadMinus          = 0x4E,
        eKey_KeypadEquals         = 0x51,
        eKey_Keypad0              = 0x52,
        eKey_Keypad1              = 0x53,
        eKey_Keypad2              = 0x54,
        eKey_Keypad3              = 0x55,
        eKey_Keypad4              = 0x56,
        eKey_Keypad5              = 0x57,
        eKey_Keypad6              = 0x58,
        eKey_Keypad7              = 0x59,
        eKey_Keypad8              = 0x5B,
        eKey_Keypad9              = 0x5C,
    
        /* keycodes for keys that are independent of keyboard layout*/
        eKey_Return                    = 0x24,
        eKey_Tab                       = 0x30,
        eKey_Space                     = 0x31,
        eKey_Delete                    = 0x33,
        eKey_Escape                    = 0x35,
        eKey_Command                   = 0x37,
        eKey_Shift                     = 0x38,
        eKey_CapsLock                  = 0x39,
        eKey_Option                    = 0x3A,
        eKey_Control                   = 0x3B,
        eKey_RightShift                = 0x3C,
        eKey_RightOption               = 0x3D,
        eKey_RightControl              = 0x3E,
        eKey_Function                  = 0x3F,
        eKey_F17                       = 0x40,
        eKey_VolumeUp                  = 0x48,
        eKey_VolumeDown                = 0x49,
        eKey_Mute                      = 0x4A,
        eKey_F18                       = 0x4F,
        eKey_F19                       = 0x50,
        eKey_F20                       = 0x5A,
        eKey_F5                        = 0x60,
        eKey_F6                        = 0x61,
        eKey_F7                        = 0x62,
        eKey_F3                        = 0x63,
        eKey_F8                        = 0x64,
        eKey_F9                        = 0x65,
        eKey_F11                       = 0x67,
        eKey_F13                       = 0x69,
        eKey_F16                       = 0x6A,
        eKey_F14                       = 0x6B,
        eKey_F10                       = 0x6D,
        eKey_F12                       = 0x6F,
        eKey_F15                       = 0x71,
        eKey_Help                      = 0x72,
        eKey_Home                      = 0x73,
        eKey_PageUp                    = 0x74,
        eKey_ForwardDelete             = 0x75,
        eKey_F4                        = 0x76,
        eKey_End                       = 0x77,
        eKey_F2                        = 0x78,
        eKey_PageDown                  = 0x79,
        eKey_F1                        = 0x7A,
        eKey_LeftArrow                 = 0x7B,
        eKey_RightArrow                = 0x7C,
        eKey_DownArrow                 = 0x7D,
        eKey_UpArrow                   = 0x7E,
    
        /* ISO keyboards only*/
        eKey_ISO_Section               = 0x0A,
    
        /* JIS keyboards only*/
        eKey_JIS_Yen                   = 0x5D,
        eKey_JIS_Underscore            = 0x5E,
        eKey_JIS_KeypadComma           = 0x5F,
        eKey_JIS_Eisu                  = 0x66,
        eKey_JIS_Kana                  = 0x68
    };
    
    int piMapKeyCode(int systemCode);
    
    
    PIInterface()
    struct iKeyEvent : public iHIDEvent
    {
        PI_DEFINE(iKeyEvent)
        
        PIMethod()
        virtual void SetCode(int code) = 0;
        
        PIMethod()
        virtual int GetCode() const = 0;
        
        PIMethod()
        virtual std::string GetText() const = 0;
    };
    
    iKeyEvent* CreateKeyEvent();
    
    PI_DEFINE_OBJECT_ARRAY(iKeyEvent);
    iKeyEventArray* CreateKeyEventArray();
}

#endif

























