/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.6   0.1     Create
 ******************************************************************************/
#ifndef PI_INPUT_HID_H
#define PI_INPUT_HID_H

namespace nspi
{
    PIInterface()
    struct iHID : public iRefObject
    {
        PI_DEFINE(iHID)
        
        /**
         * -----------------------------------------------------------------------------------------
         * Setup
         */
        
        PIMethod()
        virtual void SetFaceTracker(iFaceTracker* tracker) = 0;
        
        /**
         * -----------------------------------------------------------------------------------------
         * Input States
         */
        
        virtual void Reset() = 0;
        
        virtual iTouchEvent* GetTapEvent() const = 0;
        
        /**
         * -----------------------------------------------------------------------------------------
         * Keyboard
         */
        
        PIMethod()
        virtual iKeyEventArray* GetKeyEvents() const = 0;
        
        PIMethod()
        virtual bool IsKeyDown(int keyCode) const = 0;
        
        PIMethod()
        virtual bool IsKeyUp(int keyCode) const = 0;
        
        PIMethod()
        virtual bool IsFlagsOn(int32_t flags) const = 0;
        
        /**
         * -----------------------------------------------------------------------------------------
         * Camera Calibration
         */
        
        PIMethod()
        virtual mat4 GetFaceViewMatrix(int32_t faceIndex) const = 0;
        
        PIMethod()
        virtual iFaceTrackerResult* GetFaceTrackerResult() const = 0;
        
        /**
         * -----------------------------------------------------------------------------------------
         * AVCapture
         */
        
        PIMethod()
        virtual double GetAudioLevel() const = 0;
        
        PIMethod()
        virtual iBitmap* GetOriginBitmap() const = 0;
        
        PIMethod()
        virtual iNativeTexture* GetOriginFrame() const = 0;
        
        PIMethod()
        virtual iNativeTexture* GetFilteredFrame() const = 0;
        
        /**
         * -----------------------------------------------------------------------------------------
         * Callbacks
         */
        
        virtual void OnLoad() = 0;
        
        virtual void OnUnload() = 0;
        
        virtual void OnUpdate() = 0;
        
        virtual void OnEvent(iHIDEvent* event) = 0;
        
        virtual void OnResize(const rect& bounds) = 0;
    };
    
    iHID* CreateHID();
}


#endif













