/*******************************************************************************
See copyright notice in LICENSE.

History:
luo weibin     2017.3.22   0.1     Create
******************************************************************************/
#ifndef PI_INPUT_FACETRACKER_H
#define PI_INPUT_FACETRACKER_H

namespace nspi
{
    PIInterface()
    struct iFaceTrackerEvent : public iHIDEvent
    {
        PI_DEFINE(iFaceTrackerEvent)
        
        PIMethod()
        virtual void SetResult(iFaceTrackerResult* result) = 0;
        
        PIMethod()
        virtual iFaceTrackerResult* GetResult() const = 0;
    };
    
    iFaceTrackerEvent* CreateFaceTrackerEvent();
}

#endif
















