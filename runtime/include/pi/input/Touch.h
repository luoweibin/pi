/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.22   0.1     Create
 ******************************************************************************/
#ifndef PI_INPUT_TOUCH_H
#define PI_INPUT_TOUCH_H

namespace nspi
{
    PIInterface()
    struct iTouchEvent : public iHIDEvent
    {
        PI_DEFINE(iTouchEvent)
        
        PIMethod()
        virtual int32_t GetCount() const = 0;
        
        /**
         * Get location of the first touch.
         */
        PIMethod()
        virtual vec3 GetLocation() const = 0;
        
        /**
         * Get location of the specified touch.
         */
        PIMethod()
        virtual vec3 GetLocationOfTouch(int32_t index) const = 0;
        
        PIMethod()
        virtual void AddLocation(const vec3& location) = 0;
    };
    
    iTouchEvent* CreateTouchEvent();
}


#endif

























