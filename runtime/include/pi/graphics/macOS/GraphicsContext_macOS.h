/*******************************************************************************
 FileName: GraphicsContext_macOS.h
 Author: wilburluo
 Version: 0.1
 Date: 2016.10.5
 
 History:
 wilburluo     2016.10.5   0.1     Create
 ******************************************************************************/
#ifndef PI_GRAPHICS_MACOS_GRAPHICSCONTEXTMACOS_H
#define PI_GRAPHICS_MACOS_GRAPHICSCONTEXTMACOS_H

#include <AppKit/NSOpenGL.h>
#include <pi/Graphics.h>

namespace nspi
{
    
    iGraphicsContext* CreateGraphicsContext(NSOpenGLContext* ctx);
    
}

#endif
