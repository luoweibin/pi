/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2012.12.22   0.1     Create
 ******************************************************************************/
#ifndef PI_GRAPHICS_BITMAP_H
#define PI_GRAPHICS_BITMAP_H

namespace nspi
{
    
    enum
    {
        ePlanar_Default = 0,
        ePlanar_Y = 0,
        ePlanar_U = 1,
        ePlanar_V = 2,
    };
    
    PIInterface(HasCreator = false)
    struct iPixelFormat : public iRefObject
    {
        PI_DEFINE(iPixelFormat)
        
        PIMethod()
        virtual int GetName() const = 0;
        
        PIMethod()
        virtual int64_t CalcSize(int32_t width, int32_t heigth) const = 0;
        
        PIMethod()
        virtual int32_t GetBytesPerPixel() const = 0;
        
        PIMethod()
        virtual int GetFormat(int32_t planar) const = 0;
        
        PIMethod()
        virtual int GetType(int32_t planar) const = 0;
        
        PIMethod()
        virtual int32_t GetPlanarCount() const = 0;
    };
    
    iPixelFormat* CreatePixelFormat(int format);
    
    PIEnum()
    enum eMemMap
    {
        eMemMap_ReadOnly    = 1,
        eMemMap_WriteOnly   = 2,
        eMemMap_ReadWrite   = 3,
    };
    
    PIInterface(HasCreator = false)
    struct iBitmap : public iRefObject
    {
        PI_DEFINE(iBitmap)
        
        PIMethod()
        virtual iPixelFormat *GetPixelFormat() const = 0;
        
        PIMethod()
        virtual int32_t GetWidth() const = 0;
        
        PIMethod()
        virtual int32_t GetHeight() const = 0;
        
        PIMethod()
        virtual int32_t GetWidthOfPlanar(int32_t planar) const = 0;
        
        PIMethod()
        virtual int32_t GetHeightOfPlanar(int32_t planar) const = 0;
        
        PIMethod()
        virtual int64_t GetBytesPerRow(int32_t planar) const = 0;
        
        PIMethod()
        virtual void SetBytesPerRow(int32_t planar, int32_t bytes) = 0;
        
        PIMethod()
        virtual void Map(int access) = 0;
        
        PIMethod()
        virtual void Unmap() = 0;
        
        PIMethod()
        virtual iMemory* GetData(int32_t planar) const = 0;
        
        PIMethod()
        virtual void SetData(int32_t planar, iMemory *data) = 0;
        
        PIMethod()
        virtual bool CreateMipMaps(int32_t levels) = 0;
        
        PIMethod()
        virtual int32_t GetMipMapLevels() const = 0;
        
        PIMethod()
        virtual iBitmap* GetMipMap(int32_t level) const = 0;
        
        PIMethod()
        virtual int32_t GetPixelAlignment(int32_t planar) const = 0;
    };
    
    iBitmap *CreateBitmap(int format,
                          int32_t width,
                          int32_t height);
    
    iBitmap *CreateBitmapEmpty(int format,
                               int32_t width,
                               int32_t height);
    
    iBitmap *CreateBitmapEx(iPixelFormat *format,
                            int32_t width,
                            int32_t height);
    
    iBitmap *piConvertToBitmapRGB(iBitmap *bitmap);
    
    bool piEncodePNG(iBitmap* bitmap, iStream* buffer);
    
    iBitmap* piDecodePNG(iStream *buffer);
}

#endif























