/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2015.4.9   0.1     Create
 ******************************************************************************/
#ifndef PI_GRAPHICS_GRAPHICSVM_H
#define PI_GRAPHICS_GRAPHICSVM_H

namespace nspi
{
    
    struct iGraphics : public iRefObject
    {
        virtual ~iGraphics() {}
        
        /**
         * =========================================================================================
         * Misc
         */
        
        virtual void DumpDebugInfo() = 0;
        
        virtual void SetCapability(int cap, bool value) = 0;
        
        virtual bool GetCapability(int cap) const = 0;
        
        virtual void PushGroupMarker(const std::string& marker) = 0;
        
        virtual void PopGroupMarker() = 0;
        
        virtual void InsertEventMarker(const std::string& marker) = 0;
        
        virtual void Clear(int32_t fields) = 0;
        
        virtual void Viewport(int32_t x, int32_t y, int32_t width, int32_t height) = 0;
        
        virtual void DrawElements(int mode, int32_t count, int type) = 0;
        
        virtual void DrawElementsOffset(int mode, int32_t count, int type, int32_t offset) = 0;
        
        virtual void DrawElementsInstanced(int mode, int32_t count, int type, int32_t instanceCount) = 0;
        
        virtual void DrawArrays(int mode, int32_t first, int32_t count) = 0;
        
        virtual void DrawArraysInstanced(int mode, int32_t first, int32_t count, int instanceCount) = 0;
        
        virtual void ReleaseObejct(int32_t name) = 0;
        
        virtual void Flush() = 0;
        
        /**
         * =========================================================================================
         * State
         */
        
        virtual void Winding(int32_t winding) = 0;
        
        virtual void EnableFeatures(int features) = 0;
        
        virtual void DisableFeatures(int features) = 0;
        
        virtual void BlendColor(const vec4& color) = 0;
        
        virtual void BlendEquation(int mode) = 0;
        
        virtual void BlendFunc(int sfactor, int dfactor) = 0;
        
        virtual void BlendFuncSeparate(int32_t index, int sfactor, int dfactor) = 0;
        
        virtual void CullFace(int face) = 0;
        
        virtual void PolygonOffset(float factor, float units) = 0;
        
        virtual void PolygonMode(int face, int mode) = 0;
        
        virtual void DepthFunction(int func) = 0;
        
        virtual void ClearDepth(float value) = 0;
        
        virtual void DepthMask(bool flag) = 0;
        
        virtual void ClearColor(const vec4 &color) = 0;
        
        virtual void ColorMask(bool red, bool green, bool blue, bool alpha) = 0;
        
        virtual void ClearStencil(int value) = 0;
        
        virtual void StencilFunc(int func, int32_t ref, int64_t mask) = 0;
        
        virtual void StencilOp(int sfail, int dpfail, int dppass) = 0;
        
        virtual void StencilOpSeparate(int face, int sfail, int dpfail, int dppass) = 0;
        
        virtual void StencilMask(int64_t mask) = 0;
        
        virtual void LineWidth(float width) = 0;
        
        virtual void Hint(int target, int value) = 0;
        
        
        /**
         * =========================================================================================
         * Program
         */
        
        virtual void CompileProgram(int32_t program,
                                    const std::string& vertex,
                                    const std::string& fragment) = 0;
        
        virtual void LinkProgram(int32_t program) = 0;
        
        virtual void UseProgram(int32_t program) = 0;
        
        virtual void Uniform1i(int32_t program,
                               const std::string& name,
                               int32_t v) = 0;
        
        virtual void Uniform1iv(int32_t program,
                                const std::string& name,
                                iI32Array* values) = 0;
        
        virtual void Uniform2i(int32_t program,
                               const std::string& name, int32_t v1, int32_t v2) = 0;
        
        virtual void Uniform2iv(int32_t program,
                                const std::string& name,
                                iI32Array* values) = 0;
        
        virtual void Uniform3i(int32_t program,
                               const std::string& name,
                               int32_t v1,
                               int32_t v2,
                               int32_t v3) = 0;
        
        virtual void Uniform3iv(int32_t program,
                                const std::string& name,
                                iI32Array* values) = 0;
        
        virtual void Uniform4i(int32_t program,
                               const std::string& name,
                               int32_t v1,
                               int32_t v2,
                               int32_t v3,
                               int32_t v4) = 0;
        
        virtual void Uniform4iv(int32_t program,
                                const std::string& name,
                                iI32Array* values) = 0;
        
        virtual void Uniform1f(int32_t program,
                               const std::string& name, float v) = 0;
        
        virtual void Uniform1fv(int32_t program,
                                const std::string& name,
                                iF32Array* values) = 0;
        
        virtual void Uniform2f(int32_t program,
                               const std::string& name, float v1, float v2) = 0;
        
        virtual void Uniform2fv(int32_t program,
                                const std::string& name,
                                iF32Array* values) = 0;
        
        virtual void Uniform3f(int32_t program,
                               const std::string& name,
                               float v1,
                               float v2,
                               float v3) = 0;
        
        virtual void Uniform3fv(int32_t program,
                                const std::string& name,
                                iF32Array* values) = 0;
        
        virtual void Uniform4f(int32_t program,
                               const std::string& name,
                               float v1,
                               float v2,
                               float v3,
                               float v4) = 0;
        
        virtual void Uniform4fv(int32_t program,
                                const std::string& name,
                                iF32Array* values) = 0;
        
        virtual void UniformVec3v(int32_t program, const std::string&name, iVec3Array* values) = 0;
        
        virtual void UniformVec3(int32_t program, const std::string& name, const vec3& value) = 0;
        
        virtual void UniformVec4(int32_t program, const std::string& name, const vec4& value) = 0;
        
        virtual void UniformRect(int32_t program, const std::string& name, const rect& value) = 0;
        
        virtual void UniformQuat(int32_t program, const std::string& name, const quat& value) = 0;
        
        virtual void UniformMat4f(int32_t program,
                                  const std::string& name, const mat4& value) = 0;
        
        virtual void UniformMat4fv(int32_t program, const std::string& name, iMat4Array* values) = 0;
        
        /**
         * =========================================================================================
         * Object
         */
        
        virtual void BindVertexAttr(int32_t program, int32_t index, const std::string& name) = 0;
        
        virtual void EnableVertexAttr(int32_t name) = 0;
        
        virtual void DisableVertexAttr(int32_t name) = 0;
        
        virtual void VertexAttr(int32_t name,
                                int32_t size,
                                int32_t type,
                                int32_t stride,
                                int32_t offset) = 0;
        
        virtual void BindVertexArray(int32_t name) = 0;
        
        virtual void BindBuffer(int target, int32_t name) = 0;
        
        virtual void BufferData(int32_t target, int32_t name, int64_t size, iMemory* data) = 0;
        
        virtual void BufferSubData(int32_t target, int64_t offset, int64_t size, iMemory* data) = 0;
        
        
        /**
         * =========================================================================================
         * Texture
         */
        
        virtual void GenerateMipmap(int target) = 0;
        
        virtual void ActiveTexture(int32_t unit) = 0;
        
        virtual void BindTexture(int32_t target, int32_t name) = 0;
        
        virtual void TexParam(int32_t target, int32_t name, int32_t value) = 0;
        
        virtual void TexImage2D(int target,
                                int32_t level,
                                int32_t format,
                                iBitmap *bitmap,
                                int32_t planar) = 0;
        
        virtual void CompressedTexImage2D(int target,
                                          int32_t level,
                                          int32_t format,
                                          iBitmap *bitmap,
                                          int32_t planar) = 0;
        
        virtual void CopyTexImage2D(int target,
                                    int32_t level,
                                    int32_t format,
                                    int32_t x,
                                    int32_t y,
                                    int32_t width,
                                    int32_t height) = 0;
        
        virtual void TexSubImage2D(int target,
                                   int32_t level,
                                   int32_t xOffset, int32_t yOffset,
                                   int32_t width, int32_t height,
                                   iBitmap *bitmap,
                                   int32_t planar) = 0;
        
        virtual void CompressedTexSubImage2D(int target,
                                             int32_t level,
                                             int32_t xOffset, int32_t yOffset,
                                             int32_t width, int32_t height,
                                             iBitmap *bitmap,
                                             int32_t planar) = 0;
        
        virtual void CopyTexSubImage2D(int target,
                                       int32_t level,
                                       int32_t format,
                                       int32_t xOffset, int32_t yOffset,
                                       int32_t x, int32_t y,
                                       int32_t width, int32_t height) = 0;
        
        virtual void PixelStorei(int name, int32_t value) = 0;
        
        /**
         * =========================================================================================
         * Framebuffer
         */
        
        virtual void BindFramebuffer(int target, int32_t name) = 0;
        
        virtual void FramebufferTexture2D(int target,
                                          int attachment,
                                          int textureTarget,
                                          int texture,
                                          int32_t level) = 0;
        
        virtual void FramebufferRenderbuffer(int target,
                                             int attachment,
                                             int32_t renderbuffer) = 0;
        
        virtual void VerifyFramebufferState(int target) = 0;
        
        virtual void BindRenderbuffer(int32_t name) = 0;
        
        virtual void RenderbufferStorage(int format, int32_t width, int32_t height) = 0;
    };
    
    struct iGraphicsVM : public iGraphics
    {
        virtual ~iGraphicsVM() {}
        
        virtual int32_t CreateNativeGraphicsObject(int64_t nativeHandle) = 0;
        
        virtual void RetainObject(int32_t name) = 0;
        
        virtual bool AddAlias(const std::string& alias, int32_t name) = 0;
        
        virtual void RemoveAlias(const std::string& alias) = 0;
        
        virtual int32_t GetObject(const std::string& alias) const = 0;
        
        virtual void FlushCurrentContext() = 0;
        
        virtual rect GetViewport() const = 0;
        
        virtual int32_t CreateProgram() = 0;
        
        virtual int32_t CreateTexture() = 0;
        
        virtual int32_t CreateVertexArray() = 0;
        
        virtual int32_t CreateBuffer() = 0;
        
        virtual int32_t CreateFramebuffer() = 0;
        
        virtual int32_t CreateRenderbuffer() = 0;
        
        virtual bool IsFeatureEnabled(int32_t feature) const = 0;
        
        virtual int32_t GetFramebuffer() const = 0;
        
        virtual int GetType() const = 0;
        
        virtual void Submit() = 0;
        
        virtual void BeforeRun() = 0;
        
        virtual void Run() = 0;
        
        virtual void AfterRun() = 0;
    };
    
    iGraphicsVM* CreateGraphicsVM(int type);
}

#endif


























