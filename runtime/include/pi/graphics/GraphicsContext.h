/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.10.5   0.1     Create
 ******************************************************************************/
#ifndef PI_GRAPHICS_GRAPHICSCONTEXT_H
#define PI_GRAPHICS_GRAPHICSCONTEXT_H

namespace nspi
{
    
    struct iGraphicsContext : public iRefObject
    {
        virtual ~iGraphicsContext() {}
        
        virtual void MakeCurrent() = 0;
    };
    
}

#endif
























