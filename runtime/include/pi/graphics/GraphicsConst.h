/*******************************************************************************
 See copyright notice in LICENSE.

 History:
 luo weibin     2015.4.9   0.1     Create
 ******************************************************************************/
#ifndef PI_GRAPHICS_GRAPHICSCONST_H
#define PI_GRAPHICS_GRAPHICSCONST_H

namespace nspi
{
    PIEnum()
    enum eGraphicsBackend
    {
        eGraphicsBackend_Unknown        = -1,
        eGraphicsBackend_OpenGL3        = 1,
        eGraphicsBackend_OpenGL4        = 2,
        eGraphicsBackend_OpenGL_ES2     = 3,
        eGraphicsBackend_OpenGL_ES3     = 4,
    };
    
    std::string piGraphicsBackendName(int backend);
    
    PIEnum()
    enum eGraphicsCap
    {
        eGraphicsCap_VAO = piBit(0),
    };
    
    PIEnum()
    enum eGraphicsFeature
    {
        eGraphicsFeature_DepthTest              = piBit(0),
        eGraphicsFeature_CullFace               = piBit(1),
        eGraphicsFeature_Blend                  = piBit(2),
        eGraphicsFeature_PolygonOffset_Line     = piBit(3),
        eGraphicsFeature_PolygonOffset_Point    = piBit(4),
        eGraphicsFeature_PolygonOffset_Fill     = piBit(5),
        eGraphicsFeature_MultiSample            = piBit(6),
        eGraphicsFeature_StencilTest            = piBit(7),
        eGraphicsFeature_LineSmooth             = piBit(8),
        eGraphicsFeature_Texture1D              = piBit(9),
        eGraphicsFeature_Texture2D              = piBit(10),
        eGraphicsFeature_Texture3D              = piBit(11),
    };
    
    std::string piGraphicsFeatureNames(int features);
    
    PIEnum()
    enum eHintTarget
    {
        eHintTarget_Unknown = -1,
        eHintTarget_LineSmooth,
        eHintTarget_PolygonSmooth,
        eHintTarget_TextureCompression,
    };
    
    std::string piHintTargetName(int value);
    
    PIEnum()
    enum eHint
    {
        eHint_Unknown = -1,
        eHint_Nicest,
        eHint_Fastest,
        eHint_DontCare,
    };
    
    std::string piHintName(int value);
    
    PIEnum()
    enum eBlendFunc
    {
        eBlendFunc_One                      = 1,
        eBlendFunc_Zero                     = 2,
        eBlendFunc_SrcColor                 = 3,
        eBlendFunc_OneMinusSrcColor         = 4,
        eBlendFunc_DstColor                 = 5,
        eBlendFunc_OneMinusDstColor         = 6,
        eBlendFunc_SrcAlpha                 = 7,
        eBlendFunc_OneMinusSrcAlpha         = 8,
        eBlendFunc_DstAlpha                 = 9,
        eBlendFunc_OneMinusDstAlpha         = 10,
        eBlendFunc_ConstColor               = 11,
        eBlendFunc_OneMinusConstColor       = 12,
        eBlendFunc_ConstAlpha               = 13,
        eBlendFunc_OneMinusConstAlpha       = 14,
    };
    
    std::string piBlendFuncName(int value);
    
    PIEnum()
    enum eWinding
    {
        eWinding_CW,
        eWinding_CCW,
    };
    
    std::string piWindingName(int mode);
    
    PIEnum()
    enum eFace
    {
        eFace_Front,
        eFace_Back,
        eFace_Both,
    };
    
    std::string piFaceName(int mode);
    
    PIEnum()
    enum eBufferBit
    {
        eBufferBit_Color    = piBit(0),
        eBufferBit_Depth    = piBit(1),
        eBufferBit_Stencil  = piBit(2),
    };
    
    std::string piFramebufferBitName(int bits);
    
    PIEnum()
    enum eFramebuffer
    {
        eFramebuffer_Unknown,
        eFramebuffer_Draw,
        eFramebuffer_Read,
        eFramebuffer_DrawRead,
    };
    
    std::string piFramebufferName(int target);
        
    PIEnum()
    enum eTexConfig
    {
        eTexConfig_Unknown = -1,
        eTexConfig_MinFilter,
        eTexConfig_MagFilter,
        eTexConfig_WrapS,
        eTexConfig_WrapT,
        eTexConfig_WrapR,
        eTexConfig_PackAligment,
        eTexConfig_UnpackAligment,
    };
    
    std::string piTexConfigName(int name);
    
    PIEnum()
    enum eTexValue
    {
        eTexValue_Unknown,
        eTexValue_Nearest,
        eTexValue_Linear,
        eTexValue_Repeat,
        eTexValue_Clamp,
        eTexValue_Border,
        eTexValue_Mirror,
    };
    
    std::string piTexValueName(int name);
    
    PIEnum()
    enum eTexTarget
    {
        eTexTarget_Unknown = -1,
        eTexTarget_1D,
        eTexTarget_2D,
        eTexTarget_3D,
        eTexTarget_CubeMap,
        eTexTarget_CubeMapPositiveX,
        eTexTarget_CubeMapPositiveY,
        eTexTarget_CubeMapPositiveZ,
        eTexTarget_CubeMapNegativeX,
        eTexTarget_CubeMapNegativeY,
        eTexTarget_CubeMapNegativeZ,
    };
    
    std::string piTexTargetName(int target);
    
    PIEnum()
    enum eAttachment
    {
        eAttachment_Unknown = -1,
        eAttachment_Depth,
        eAttachment_Stencil,
        eAttachment_DepthStencil,
        eAttachment_Color0,
        eAttachment_Color1,
        eAttachment_Color2,
        eAttachment_Color3,
        eAttachment_Color4,
        eAttachment_Color5,
        eAttachment_Color6,
        eAttachment_Color7,
        eAttachment_Color8,
        eAttachment_Color9,
        eAttachment_Color10,
        eAttachment_Color11,
        eAttachment_Color12,
        eAttachment_Color13,
        eAttachment_Color14,
        eAttachment_Color15,
    };
    
    std::string piAttachmentName(int target);
    
    PIEnum()
    enum eGraphicsBuffer
    {
        eGraphicsBuffer_Vertex,
        eGraphicsBuffer_Index,
    };
    
    std::string piGraphicsBufferName(int name);
    
    PIEnum()
    enum eGraphicsDraw
    {
        eGraphicsDraw_Unknown = -1,
        eGraphicsDraw_Lines,
        eGraphicsDraw_LineLoop,
        eGraphicsDraw_Triangles,
        eGraphicsDraw_TriangleStrip,
        eGraphicsDraw_Quads,
        eGraphicsDraw_QuadStrip,
        eGraphicsDraw_Points,
    };
    
    std::string piGraphicsDrawName(int name);
    
    PIEnum()
    enum ePolygonMode
    {
        ePolygonMode_Unknown = -1,
        ePolygonMode_Line,
        ePolygonMode_Fill,
        ePolygonMode_Point,
    };
    std::string piPolygonModeName(int name);
    
    PIEnum()
    enum eFunc
    {
        eFunc_Greater = 1,
        eFunc_GreaterAndEqual = 2,
        eFunc_Equal = 3,
        eFunc_NotEqual= 4,
        eFunc_LessAndEqual = 5,
        eFunc_Less = 6,
        eFunc_Never = 7,
        eFunc_Always = 8,
    };
    std::string piFuncName(int func);
    
    PIEnum()
    enum eStencilOp
    {
        eStencilOp_Keep,
        eStencilOp_Zero,
        eStencilOp_Replace,
        eStencilOp_Increase,
        eStencilOp_IncreaseWrap,
        eStencilOp_Decrease,
        eStencilOp_DecreaseWrap,
        eStencilOp_Invert,
    };
    std::string piStencilOpName(int op);
 
    PIEnum()
    enum eEqFunc
    {
        eEqFunc_Add,
        eEqFunc_Sub,
        eEqFunc_RevSub, // reverse
    };
    std::string piEqFuncName(int value);
}

#endif



































