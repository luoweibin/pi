/*******************************************************************************
 FileName: GraphicsContext_iOS.h
 Author: wilburluo
 Version: 0.1
 Date: 2016.10.5
 
 History:
 wilburluo     2016.10.5   0.1     Create
 ******************************************************************************/
#ifndef PI_GRAPHICS_IOS_GRAPHICSCONTEXTIOS_H
#define PI_GRAPHICS_IOS_GRAPHICSCONTEXTIOS_H

#include <OpenGLES/EAGL.h>
#include <pi/Graphics.h>

namespace nspi
{
    
    iGraphicsContext* CreateGraphicsContext(EAGLContext* ctx);
    
}

#endif
