//
// Created by CYY on 2017/4/7.
//

#ifndef FUPI_GRAPHICSCONTEXT_ANDROID_H
#define FUPI_GRAPHICSCONTEXT_ANDROID_H

namespace nspi
{
    iGraphicsContext* CreateGraphicsContext();
}

#endif //FUPI_GRAPHICSCONTEXT_ANDROID_H
