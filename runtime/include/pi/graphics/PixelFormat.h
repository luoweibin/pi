/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.19   0.1     Create
 ******************************************************************************/
#ifndef PI_GRAPHICS_PIXELFORMAT_H
#define PI_GRAPHICS_PIXELFORMAT_H

namespace nspi
{
    
    PIEnum()
    enum ePixelFormat
    {
        ePixelFormat_Unknown = -1,
        
        ePixelFormat_RGBA = 0,
        
        ePixelFormat_BGRA = 1,
        
        ePixelFormat_R = 2,
        
        ePixelFormat_RGB = 3,
        
        ePixelFormat_Gray = 4,
        
        ePixelFormat_DXT1 = 5,
        
        ePixelFormat_DXT2 = 6,
        
        ePixelFormat_DXT3 = 7,
        
        ePixelFormat_DXT4 = 8,
        
        ePixelFormat_DXT5 = 9,
        
        ePixelFormat_R16G16B16A16 = 10,
        
        ePixelFormat_FR16 = 11,
        
        ePixelFormat_FR16G16 = 12,
        
        ePixelFormat_FR16G16B16A16 = 13,
        
        ePixelFormat_FR32 = 14,
        
        ePixelFormat_FR32G32 = 15,
        
        ePixelFormat_FR32G32B32A32 = 16,
        
        ePixelFormat_B10G10R10A2 = 17,
        
        ePixelFormat_R10G10B10A2 = 18,
        
        ePixelFormat_B8G8R8A8 = 19,
        
        ePixelFormat_R8G8B8A8 = 20,
        
        ePixelFormat_B8G8R8X8 = 21,
        
        ePixelFormat_R8G8B8X8 = 22,
        
        ePixelFormat_B8G8R8 = 23,
        
        ePixelFormat_B2G3R3A8 = 24,
        
        ePixelFormat_B5G5R5A1 = 25,
        
        ePixelFormat_B5G5R5X1 = 26,
        
        ePixelFormat_B4G4R4X4 = 27,
        
        ePixelFormat_B4G4R4A4 = 28,
        
        ePixelFormat_B5G6R5 = 29,
        
        ePixelFormat_B2G3R3 = 30,
        
        ePixelFormat_A8 = 31,
        
        ePixelFormat_L8 = 32,
        
        ePixelFormat_A4L4 = 33,
        
        ePixelFormat_A8L8 = 34,
        
        ePixelFormat_P8 = 35,
        
        ePixelFormat_YUV420P = 36,
        
        ePixelFormat_Alpha = 37,
        
        ePixelFormat_Depth16 = 38,
        
        ePixelFormat_Depth24 = 39,
        
        ePixelFormat_Depth32 = 40,
        
        ePixelFormat_Stencil = 41,
        
        ePixelFormat_Depth32F = 42,
        
        ePixelFormat_Depth24Stencil8 = 43,
    };
    
    std::string piPixelFormatName(int format);
    
}

#endif
