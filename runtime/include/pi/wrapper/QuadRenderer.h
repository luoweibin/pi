/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.16   1     Create
 ******************************************************************************/
#ifndef PI_WRAPPER_QUADRENDERER_H
#define PI_WRAPPER_QUADRENDERER_H

#include <pi/Game.h>

namespace nspi
{
    
    class QuadRenderer : public iRefObject
    {
    private:
        int32_t mProgram;
        int32_t mVAO;
        int32_t mVBO;
        
    public:
        QuadRenderer();
        
        virtual ~QuadRenderer();
        
        void OnLoad();
        
        void OnUnload();
        
        void Present(int32_t texture, const mat4& matrix);
        
    private:
        void InitProgram();
        void InitVBO();
    };
    
}


#endif






















