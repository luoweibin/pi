/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.9.15   1     Create
 ******************************************************************************/
#ifndef PI_WRAPPER_GAMEIMPL_H
#define PI_WRAPPER_GAMEIMPL_H

#include <pi/Game.h>
#include <pi/Input.h>
#include <pi/wrapper/GameBuffer.h>
#include <pi/wrapper/QuadRenderer.h>

#include <mutex>
#include <thread>

namespace nspi
{
    template<class I>
    class GameImpl : public I
    {
    protected:
        bool mPaused;
        
#if PI_TIME_SYNC
        float mLastTimeMS;
        float mCurrTimeMS;
#else
        int64_t mStartTimeMS;
#endif
        
        mutable std::mutex mMutex;
        SmartPtr<iEvent> mEventMQ;
        
        SmartPtr<iGraphicsVM> mGraphicsVM;
        SmartPtr<iGraphicsContext> mResCtx;
        
        SmartPtr<iMessageQueue> mCmdMQ;
        SmartPtr<iMessageQueue> mLowMQ;
        SmartPtr<iMessageQueue> mNormalMQ;
        SmartPtr<iMessageQueue> mHighMQ;
        
        SmartPtr<iClassLoader> mClassLoader;
        
        rect mBounds;
        
        typedef std::map<std::string, SmartPtr<iScene>> SceneMap;
        SceneMap mScenes;
        
        SmartPtr<iAssetManager> mAssetMgr;
        
        SmartPtr<iHID> mHID;
        SmartPtr<iMotionManager> mMotionMgr;
        
        bool mLoaded;
        
        SmartPtr<GameBuffer> mBuffer;
        SmartPtr<QuadRenderer> mBufferRenderer;
        
        mat4 mBufMatrix;
        
    public:
        GameImpl():
        mPaused(false), mLoaded(false)
#if PI_TIME_SYNC
        , mCurrTimeMS(0), mLastTimeMS(0)
#else
        , mStartTimeMS(0)
#endif
        {
            mCmdMQ = CreateMessageQueue();
            mLowMQ = CreateMessageQueue();
            mNormalMQ = CreateMessageQueue();
            mHighMQ = CreateMessageQueue();
            
            mEventMQ = CreateEvent(1);
            mAssetMgr = CreateAssetManager();
            
            mHID = CreateHID();
            
            mMotionMgr = CreateMotionManager();
            
            mBuffer = new GameBuffer();
            
            mBufferRenderer = new QuadRenderer();
        }
        
        virtual ~GameImpl()
        {
        }
        
#if PI_TIME_SYNC
        virtual void UpdateTime(float currTimeMS)
        {
            mCurrTimeMS = currTimeMS;
        }
#endif
        
        virtual void SetBufferMatrix(const mat4& matrix)
        {
            mBufMatrix = matrix;
        }
        
        virtual void SetAssetManager(iAssetManager* manager)
        {
            piAssert(manager != nullptr, ;);
            mAssetMgr = manager;
        }
        
        virtual void SetClassLoader(iClassLoader* loader)
        {
            mClassLoader = loader;
        }
        
        virtual iClassLoader* GetClassLoader() const
        {
            return mClassLoader;
        }
        
        virtual rect GetBounds() const
        {
            return mBounds;
        }
        
        virtual void SetResourceContext(iGraphicsContext* context)
        {
            mCmdMQ->PostMessage(nullptr, eGameMsg_SetResContext, context);
        }
        
        virtual iGraphicsContext* GetResourceContext() const
        {
            std::lock_guard<std::mutex> lock(mMutex);
            return mResCtx;
        }
        
        virtual void SetGraphicsVM(iGraphicsVM* vm)
        {
            mCmdMQ->PostMessage(nullptr, eGameMsg_SetGraphicsVM, vm);
        }
        
        virtual iGraphicsVM* GetGraphicsVM() const
        {
            std::lock_guard<std::mutex> lock(mMutex);
            return mGraphicsVM;
        }
        
        virtual void PostMessage(iRefObject* sender,
                                 int priority,
                                 int32_t message,
                                 const Var &arg1 = Var(),
                                 const Var &arg2 = Var())
        {
            switch (priority)
            {
                case eGameMsgPri_High:
                    mHighMQ->PostMessage(sender, message, arg1, arg2);
                    break;
                case eGameMsgPri_Low:
                    mLowMQ->PostMessage(sender, message, arg1, arg2);
                    break;
                case eGameMsgPri_Normal:
                default:
                    mNormalMQ->PostMessage(sender, message, arg1, arg2);
                    break;
            }
            
            mEventMQ->Fire();
        }
        
        virtual void LoadScene(iAssetManager* assetMgr, const std::string& uri)
        {
            piAssert(assetMgr != nullptr, ;);
            piAssert(!uri.empty(), ;);
            
            PostMessage(nullptr, eGameMsgPri_Normal, eGameMsg_LoadScene, assetMgr, uri);
        }
        
        virtual void LoadSceneObject(iAssetManager* assetMgr, iScene* scene)
        {
            piAssert(assetMgr != nullptr, ;);
            piAssert(scene != nullptr, ;);
            
            PostMessage(nullptr, eGameMsgPri_Normal, eGameMsg_LoadScene, assetMgr, scene);
        }
        
        virtual void UnloadScene(const std::string &uri)
        {
            piAssert(!uri.empty(), ;);
            
            PostMessage(nullptr, eGameMsgPri_Normal, eGameMsg_UnloadScene, uri);
        }
        
        virtual iAssetManager* GetAssetManager() const
        {
            return mAssetMgr;
        }
        
        virtual iHID* GetHID() const
        {
            return mHID;
        }
        
        virtual iMotionManager* GetMotionManager() const
        {
            return mMotionMgr;
        }
        
        virtual void SwapBuffer()
        {
            mBuffer->Swap();
        }
        
        virtual int32_t GetFrontBufferTex() const
        {
            return mBuffer->GetFrontTexture();
        }
        
        virtual int32_t GetBackBufferTex() const
        {
            return mBuffer->GetBackTexture();
        }
        
    protected:
        
        virtual bool OnUserMessage(iMessage* message)
        {
            return false;
        }
        
        virtual bool OnSystemMessage(iMessage* message)
        {
            switch (message->GetID())
            {
                case eGameMsg_SetGraphicsVM:
                    OnSetGraphicsVM(message);
                    break;
                case eGameMsg_SetResContext:
                    OnSetResContext(message);
                    break;
                case eGameMsg_Load:
                    OnLoad(message);
                    break;
                case eGameMsg_Unload:
                    OnUnload(message);
                    break;
                case eGameMsg_LoadScene:
                    OnLoadScene(message);
                    break;
                case eGameMsg_UnloadScene:
                    OnUnloadScene(message);
                    break;
                case eGameMsg_Update:
                    if (!mPaused)
                    {
                        float delta = 0;
                        
#if PI_TIME_SYNC
                        if (mCurrTimeMS > 0)
                        {
                            delta = mCurrTimeMS - mLastTimeMS;
                        }
                        mLastTimeMS = mCurrTimeMS;
#else
                        int64_t now = piGetSystemTimeMS();
                        if (mStartTimeMS > 0)
                        {
                            delta = (now - mStartTimeMS) / 1000.0;
                        }
                        mStartTimeMS = now;
#endif
                        
                        OnUpdate(delta);
                        
                        PresentBuffer();
                        
                        mGraphicsVM->Submit();
                        
                        mHID->Reset();
                    }
                    break;
                case eGameMsg_Resize:
                    OnResize(message->GetArg1());
                    break;
                case eGameMsg_Pause:
                    mPaused = true;
                    break;
                case eGameMsg_Resume:
                    mPaused = false;
                    break;
                case eGameMsg_HIDEvent:
                    OnHIDEvent(message);
                    break;
                default:
                    SmartPtr<iRefObject> sender = message->GetSender();
                    for (auto pair : mScenes)
                    {
                        SmartPtr<iScene> scene = pair.second;
                        if (scene != sender.Ptr())
                        {
                            scene->OnMessage(message);
                        }
                    }
                    break;
            }
            
            return false;
        }
        
        
        void PresentBuffer()
        {
            mBufferRenderer->Present(GetBackBufferTex(), mBufMatrix);
        }
        
        virtual void OnUpdate(float delta)
        {
            mMotionMgr->OnUpdate();
            mHID->OnUpdate();
            
            int32_t fb = mBuffer->GetFramebuffer();
            piBindFramebuffer(eFramebuffer_DrawRead, fb);
            
            for (auto s : mScenes)
            {
                s.second->OnUpdate(delta);
            }
            
            piBindFramebuffer(eFramebuffer_DrawRead, 0);
            
            mGraphicsVM->DumpDebugInfo();
        }
        
        virtual void OnResize(const rect& bounds)
        {
            if (!piEqual(bounds.width, mBounds.width)
                || !piEqual(bounds.height, mBounds.height))
            {
                mBounds = bounds;
                piViewport(bounds.x, bounds.y, bounds.width, bounds.height);
                mHID->OnResize(bounds);
                
                mBuffer->OnResize(bounds.width, bounds.height);
                
                for (auto s : mScenes)
                {
                    s.second->OnResize(bounds);
                }
            }
        }
        
        void OnSetGraphicsVM(iMessage* message)
        {
            SmartPtr<iGraphicsVM> vm = piGetMessageArg1<iGraphicsVM>(message);
            piSetGraphicsVM(vm);
            mGraphicsVM = vm;
        }
        
        virtual void OnLoad(iMessage* message)
        {
            piCheck(!mLoaded, ;);
            
            mHID->OnLoad();
            mHID->OnResize(mBounds);
            
            mBuffer->OnLoad();
            
            mBufferRenderer->OnLoad();
            
            mLoaded = true;
        }
        
        virtual void OnUnload(iMessage* message)
        {
            piCheck(mLoaded, ;);
            
            mHID->OnUnload();
            mHighMQ->Clear();
            mLowMQ->Clear();
            mNormalMQ->Clear();
            mCmdMQ->Clear();
            
            mBuffer->OnUnload();
            mBufferRenderer->OnUnload();
            
            mLoaded = false;
        }
        
        virtual void OnLoadScene(iMessage* msg)
        {
            int64_t begin = piGetSystemTimeMS();
            
            SmartPtr<iAssetManager> assetMgr = piGetMessageArg1<iAssetManager>(msg);
            
            SmartPtr<iScene> scene;
            
            Var arg2 = msg->GetArg2();
            if (arg2.GetType() == eType_String)
            {
                std::string uri = arg2;
                
                SceneMap::iterator it = mScenes.find(uri);
                piCheck(it == mScenes.end(), ;);
                
                scene = piLoadAsset<iScene>(assetMgr, GetClassLoader(), "meta.sce", false);
                piCheck(!scene.IsNull(), ;);
                
                scene->SetName(uri);
            }
            else if (arg2.GetType() == eType_Object)
            {
                scene = piQueryVarObject<iScene>(arg2);
                piCheck(!scene.IsNull(), ;);
            }
            else
            {
                return;
            }
            
            scene->SetGame(this);
            scene->SetAssetManager(assetMgr);
            scene->OnLoad();
            scene->OnResize(mBounds);
            
            auto name = scene->GetName();
            piAssert(!name.empty(), ;);
            mScenes[name] = scene;
            
            int64_t timecost = piGetSystemTimeMS() - begin;
            PILOGI(PI_GAME_TAG, "Timecost for Scene OnLoadScene:%lldMS, uri:%s", timecost, name.c_str());
        }
        
        virtual void OnUnloadScene(iMessage* msg)
        {
            int64_t begin = piGetSystemTimeMS();
            
            std::string name = msg->GetArg1();
            SceneMap::iterator it = mScenes.find(name);
            piCheck(it != mScenes.end(), ;);
            
            SmartPtr<iScene> scene = it->second;
            scene->OnUnload();
            
            mScenes.erase(it);
            
            // This is very tricky.
            // Asset Manager of scene must be released after scene.
            // Hold an asset manager reference to delay the release.
            SmartPtr<iAssetManager> mgr = scene->GetManager();
            scene.PtrAndSetNull();
            
            int64_t timecost = piGetSystemTimeMS() - begin;
            PILOGI(PI_GAME_TAG, "Timecost for Scene OnUnloadScene:%lldMS, uri:%s", timecost, name.c_str());
        }
        
        virtual void OnHIDEvent(iMessage* message)
        {
            SmartPtr<iHIDEvent> event = piGetMessageArg1<iHIDEvent>(message);
            if (!event.IsNull())
            {
                mHID->OnEvent(event);
            }
            
            for (auto s : mScenes)
            {
                if (s.second->OnHIDEvent(event))
                {
                    break;
                }
            }
        }
        
        void OnSetResContext(iMessage* message)
        {
            SmartPtr<iGraphicsContext> ctx = piGetMessageArg1<iGraphicsContext>(message);
            mResCtx = ctx;
        }
        
        iMessage* WaitForMessage()
        {
            while (true)
            {
                if (!mCmdMQ->Empty())
                {
                    return mCmdMQ->PopMessage();
                }
                
                if (!mHighMQ->Empty())
                {
                    return mHighMQ->PopMessage();
                }
                
                if (!mNormalMQ->Empty())
                {
                    return mNormalMQ->PopMessage();
                }
                
                if (!mLowMQ->Empty())
                {
                    return mLowMQ->PopMessage();
                }
                
                mEventMQ->Lock();
                mEventMQ->Wait();
                mEventMQ->Reset();
                mEventMQ->Unlock();
            }
        }
        
        iMessage *PopMessage()
        {
            if (!mCmdMQ->Empty())
            {
                return mCmdMQ->PopMessage();
            }
            
            if (!mHighMQ->Empty())
            {
                return mHighMQ->PopMessage();
            }
            
            if (!mNormalMQ->Empty())
            {
                return mNormalMQ->PopMessage();
            }
            
            if (!mLowMQ->Empty())
            {
                return mLowMQ->PopMessage();
            }
            
            return nullptr;
        }
    };
    
    
    template<class T>
    class ThreadedGameImpl : public GameImpl<T>
    {
    protected:
        std::thread mThread;
        
        using GameImpl<T>::mHighMQ;
        using GameImpl<T>::mNormalMQ;
        using GameImpl<T>::mLowMQ;
        using GameImpl<T>::mGraphicsVM;
        using GameImpl<T>::mMutex;
        using GameImpl<T>::mResCtx;
        using GameImpl<T>::WaitForMessage;
        using GameImpl<T>::OnUserMessage;
        using GameImpl<T>::OnSystemMessage;
        using GameImpl<T>::mHID;
        
    public:
        ThreadedGameImpl()
        {
        }
        
        virtual ~ThreadedGameImpl()
        {
        }
        
        virtual void Start()
        {
            mThread = std::thread([this](){RunLoop();});
        }
        
        virtual void Pause() {
            mHighMQ->PostMessage(nullptr, eGameMsg_Pause);
        }
        
        virtual void Resume() {
            mHighMQ->PostMessage(nullptr, eGameMsg_Resume);
        }
        
    private:
        
        void RunLoop()
        {
            while (true)
            {
                SmartPtr<iMessage> msg = WaitForMessage();
                
                int32_t id = msg->GetID();
                if (id < eGameMsg_User)
                {
                    OnSystemMessage(msg);
                }
                else
                {
                    OnUserMessage(msg);
                }
            }
        }
        
    };
}

#endif



























