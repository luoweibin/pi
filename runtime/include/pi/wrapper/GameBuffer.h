/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.16   1     Create
 ******************************************************************************/
#ifndef PI_WRAPPER_GAMEBUFFER_H
#define PI_WRAPPER_GAMEBUFFER_H

#include <pi/Game.h>

namespace nspi
{
    
    class GameBuffer : public iRefObject
    {
    private:
        int32_t mFB;
        int32_t mDepthBuffer;
        
        int32_t mWidth;
        int32_t mHeight;
        
        int32_t mTexs[2];
        
        bool mSizeChanged;
        
    public:
        GameBuffer();
        
        virtual ~GameBuffer();
        
        void Swap();
        
        int32_t GetFramebuffer();
        
        int32_t GetFrontTexture() const;
        
        int32_t GetBackTexture() const;
        
        void OnLoad();
        
        void OnUnload();
        
        void OnResize(int32_t width, int32_t height);
    };
}

#endif

































