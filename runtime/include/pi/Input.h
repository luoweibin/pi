/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.21   0.1     Create
 ******************************************************************************/
#ifndef PI_INPUT_H
#define PI_INPUT_H

#include <pi/CV.h>
#include <pi/Media.h>

#include <pi/input/HIDEvent.h>
#include <pi/input/Touch.h>
#include <pi/input/Keyboard.h>
#include <pi/input/FaceTracker.h>
#include <pi/input/AVCapture.h>
#include <pi/input/HID.h>

#include <pi/input/Motion.h>

#endif













