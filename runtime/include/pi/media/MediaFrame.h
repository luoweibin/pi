/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2015.3.30   0.1     Create
 ******************************************************************************/
#ifndef PI_MEDIA_MEDIAFRAME_H
#define PI_MEDIA_MEDIAFRAME_H

namespace nspi
{
    PIInterface(HasCreator = false)
    struct iMediaFrame : public iRefObject
    {
        PI_DEFINE(iMediaFrame)
        
        //单位毫秒
        PIMethod()
        virtual iTime* GetPTS() const = 0;
        
        //单位毫秒
        virtual void SetPTS(iTime *time) = 0;
    };
    
    PIInterface(HasCreator = false)
    struct iVideoFrame : public iMediaFrame
    {
        PI_DEFINE(iVideoFrame)
        
        PIMethod()
        virtual iBitmap* GetBitmap() const = 0;
    };
    
    PIInterface(HasCreator = false)
    struct iAudioFrame : public iMediaFrame
    {
        PI_DEFINE(iAudioFrame)
        
        virtual void* GetData() const = 0;
        
        virtual int64_t GetSize() const = 0;
    };
    
    iVideoFrame* CreateVideoFrame(iBitmap* bitmap);
}

#endif







































