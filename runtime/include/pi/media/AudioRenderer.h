/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2015.4.2   0.1     Create
 ******************************************************************************/
#ifndef PI_MEDIA_AUDIORENDERER_H
#define PI_MEDIA_AUDIORENDERER_H

namespace nspi
{
    struct iAudioRenderer : public iRefObject
    {
        virtual ~iAudioRenderer() {}
        
        virtual bool Start() = 0;
        
        virtual void Stop() = 0;
        
        virtual void Pause() = 0;
        
        virtual void Resume() = 0;
        
        /**
         * 清除缓冲区。
         */
        virtual void Clear() = 0;
        
        /**
         * \param size 单位Byte。
         */
        virtual void SetBufferEnough(int64_t size) = 0;
        
        virtual int64_t GetBufferEnough() const = 0;
        
        virtual void SetBufferCapacity(int64_t size) = 0;
        
        virtual int64_t GetBufferCapacity() const = 0;
        
        virtual int32_t GetSampleRate() const = 0;
        
        virtual void SetSampleRate(int32_t sampleRate) = 0;
        
        virtual int GetState() const = 0;
        
        /**
         * 添加音频帧到缓冲区
         * \return 缓冲区足够，返回true，不足返回false。
         */
        virtual bool Push(iAudioFrame *frame) = 0;
        
        virtual void Finish() = 0;
        
        virtual void Update() = 0;
    };
    
    enum
    {
        /**
         * 采用跨平台接口OpenAL
         * 注意：在iOS/OSX上的实现非常多BUG，不建议使用。
         */
        eAudioRenderer_OpenAL,
        
        /**
         * 采用系统的播放接口
         */
        eAudioRenderer_Platform,
    };
    
    iAudioRenderer* CreateAudioRenderer(int backend);
}

#endif
































