/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2015.4.1   0.1     Create
 ******************************************************************************/
#ifndef PI_MEDIA_MEDIACONFIG_H
#define PI_MEDIA_MEDIACONFIG_H

#define PI_MEDIA_TAG "PI-MEDIA"

//音频输出采样率
#define PI_AUDIO_SAMPLE_RATE 44100

//设置AudioRenderer缓冲区大小，单位Byte。
#define PI_AUDIO_BUFFER_CAPACITY (1024*128)

//设置AudioRenderer开始播放的缓冲区大小，单位Byte。值越大，延迟越大。足够大即可，过大也没意义。
#define PI_AUDIO_BUFFER_ENOUGH (1024*8)

//设置默认的AudioRenderer
#define PI_AUDIO_RENDERER eAudioRenderer_Platform

//设置默认的AudioEngine
#define PI_AUDIO_ENGINE eAudioEngine_Platform

#endif



























