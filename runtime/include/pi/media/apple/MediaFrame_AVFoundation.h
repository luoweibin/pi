/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2015.4.2   0.1     Create
 ******************************************************************************/
#ifndef PI_MEDIA_IOS_MEDIAFRAME_AVFOUNDATION_H
#define PI_MEDIA_IOS_MEDIAFRAME_AVFOUNDATION_H

#import <AVFoundation/AVFoundation.h>
#import <pi/Media.h>

namespace nspi
{
    iBitmap* CreateBitmapHardware(CVPixelBufferRef buffer);
    iVideoFrame *CreateVideoFrameHardware(CMSampleBufferRef buffer);
}

#endif
