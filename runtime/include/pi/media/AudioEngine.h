/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 caiwenqiang     2017.6.29   0.1     Create
 ******************************************************************************/
#ifndef PI_MEDIA_AUDIOENGINE_H
#define PI_MEDIA_AUDIOENGINE_H

namespace nspi
{
    struct iAudioEngine : public iRefObject
    {
        virtual ~iAudioEngine() {}
        
        virtual bool Start() = 0;
        
        virtual void Stop() = 0;
        
        // music
        virtual bool PreloadBackgroundMusic(const std::string& filePath) = 0;
        
        virtual void BackgroundMusicSetLoop(bool loop) = 0;
        
        virtual void PlayBackgroundMusic(const std::string& filePath) = 0;
        
        virtual void PauseBackgroundMusic() = 0;
        
        virtual void ResumeBackgroundMusic() = 0;
        
        virtual void RewindBackgroundMusic() = 0;
        
        virtual void StopBackgroundMusic() = 0;
        
        virtual void ReleaseBackgroundMusic() = 0;
        
        // sound
        virtual bool PreloadSound(const std::string& filePath) = 0;
        
        // positive soundId if successful, zero if failed
        virtual int CreateSoundId(const std::string& filePath) = 0;
        
        virtual void SoundSetLoop(int soundId, bool loop) = 0;
        
        virtual void PlaySound(int soundId) = 0;
        
        virtual void PauseSound(int soundId) = 0;
        
        virtual void ResumeSound(int soundId) = 0;
        
        virtual void StopSound(int soundId) = 0;
        
        virtual void DestroySoundId(int soundId) = 0;
        
        virtual void UnloadSound(const std::string& filePath) = 0;
        
        virtual void StopAllSound() = 0;
        
        virtual void UnloadAllSound() = 0;
    };
    
    enum
    {
        /**
         * iOS/OSX
         */
        eAudioEngine_OpenAL,
        
        /**
         * 采用系统的播放接口
         * iOS/OSX: AVAudioPlayer
         * Android: MediaPlayer, SoundPool
         */
        eAudioEngine_Platform,
    };
    
    iAudioEngine* CreateAudioEngine(int backend);
}

#endif
































