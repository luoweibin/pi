/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2015.3.26   0.1     Create
 ******************************************************************************/
#ifndef PI_GRAPHICS_H
#define PI_GRAPHICS_H

#define PI_GRAPHICS_TAG "PI-GRAPHICS"

#include <pi/Core.h>

#include <pi/graphics/PixelFormat.h>
#include <pi/graphics/Bitmap.h>

#include <pi/graphics/GraphicsConst.h>
#include <pi/graphics/GraphicsContext.h>
#include <pi/graphics/GraphicsVM.h>



namespace nspi
{
    void piSetGraphicsVM(iGraphicsVM* vm);
    
    PIFunction()
    int piGetGraphicsType();
    
    // =========================================================================
    // 图形指令
    
    PIFunction()
    bool piAddGraphicsAlias(const std::string& alias, int32_t name);
    
    PIFunction()
    void piRemoveGraphicsAlias(const std::string& alias);
    
    PIFunction()
    int32_t piGetGraphicsObject(const std::string& alias);
    
    PIFunction()
    void piPushGroupMarker(const std::string& marker);
    
    PIFunction()
    void piPopGroupMarker();
    
    PIFunction()
    void piInsertEventMarker(const std::string& marker);
    
    PIFunction()
    void piClear(int32_t fields);
    
    PIFunction()
    void piBlendColor(const vec4& color);
    
    PIFunction()
    void piBlendEquation(int mode);
    
    PIFunction()
    void piBlendFunc(int sfactor, int dfactor);
    
    PIFunction()
    void piBlendFuncSeparate(int32_t index, int sfactor, int dfactor);
    
    PIFunction()
    void piCullFace(int face);
    
    PIFunction()
    void piViewport(int32_t x, int32_t y, int32_t width, int32_t height);
    
    PIFunction()
    rect piGetViewport();
    
    PIFunction()
    void piWinding(int32_t winding);
    
    PIFunction()
    void piEnableFeatures(int features);
    
    PIFunction()
    void piDisableFeatures(int features);
    
    PIFunction()
    void piDrawElements(int mode, int32_t count, int type);
    
    PIFunction()
    void piDrawElementsOffset(int mode, int32_t count, int type, int32_t offset);
    
    PIFunction()
    void piDrawElementsInstanced(int mode, int32_t count, int type, int32_t instanceCount);
    
    PIFunction()
    void piDrawArrays(int mode, int32_t first, int32_t count);
    
    PIFunction()
    void piDrawArraysInstanced(int mode, int32_t first, int32_t count, int instanceCount);
    
    PIFunction()
    void piRetainGraphicsObject(int32_t name);
    
    PIFunction()
    void piReleaseGraphicsObject(int32_t name);
    
    PIFunction()
    void piFlush();
    
    int32_t piCreateNativeGraphicsObject(int64_t nativeHandle);
    
    //----------------------------------------------------------------------
    
    PIFunction()
    void piPolygonOffset(float factor, float units);
    
    PIFunction()
    void piPolygonMode(int face, int mode);
    
    PIFunction()
    void piDepthFunction(int func);
    
    PIFunction()
    void piClearDepth(float value);
    
    PIFunction()
    void piDepthMask(bool flag);
    
    PIFunction()
    void piClearColor(const vec4 &color);
    
    PIFunction()
    void piColorMask(bool red, bool green, bool blue, bool alpha);
    
    PIFunction()
    void piClearStencil(int value);
    
    PIFunction()
    void piStencilFunc(int func, int32_t ref, int64_t mask);
    
    PIFunction()
    void piStencilOp(int sfail, int dpfail, int dppass);
    
    PIFunction()
    void piStencilOpSeparate(int face, int sfail, int dpfail, int dppass);
    
    PIFunction()
    void piStencilMask(int64_t mask);
    
    PIFunction()
    void piLineWidth(float width);
    
    PIFunction()
    void piHint(int target, int value);
    
    //----------------------------------------------------------------------
    
    PIFunction()
    int32_t piCreateProgram();
    
    PIFunction()
    void piCompileProgram(int32_t program,
                          const std::string& vertex,
                          const std::string& fragment);
    
    PIFunction()
    void piLinkProgram(int32_t program);
    
    PIFunction()
    void piUseProgram(int32_t program);
    
    PIFunction()
    void piBindVertexAttr(int32_t program, int32_t index, const std::string& name);
    
    PIFunction()
    void piEnableVertexAttr(int32_t name);
    
    PIFunction()
    void piDisableVertexAttr(int32_t name);
    
    PIFunction()
    void piVertexAttr(int32_t name,
                      int32_t size,
                      int32_t type,
                      int32_t stride,
                      int32_t offset);
    
    PIFunction()
    void piUniform1i(int32_t program,
                     const std::string& name,
                     int32_t v);
    
    PIFunction()
    void piUniform1iv(int32_t program,
                      const std::string& name,
                      iI32Array* values);
    
    PIFunction()
    void piUniform2i(int32_t program,
                     const std::string& name, int32_t v1, int32_t v2);
    
    PIFunction()
    void piUniform2iv(int32_t program,
                      const std::string& name,
                      iI32Array* values);
    
    PIFunction()
    void piUniform3i(int32_t program,
                     const std::string& name,
                     int32_t v1,
                     int32_t v2,
                     int32_t v3);
    
    PIFunction()
    void piUniform3iv(int32_t program,
                      const std::string& name,
                      iI32Array* values);
    
    PIFunction()
    void piUniform4i(int32_t program,
                     const std::string& name,
                     int32_t v1,
                     int32_t v2,
                     int32_t v3,
                     int32_t v4);
    
    PIFunction()
    void piUniform4iv(int32_t program,
                      const std::string& name,
                      iI32Array* values);
    
    PIFunction()
    void piUniform1f(int32_t program,
                     const std::string& name, float v);
    
    PIFunction()
    void piUniform1fv(int32_t program,
                      const std::string& name,
                      iF32Array* values);
    
    PIFunction()
    void piUniform2f(int32_t program,
                     const std::string& name, float v1, float v2);
    
    PIFunction()
    void piUniform2fv(int32_t program,
                      const std::string& name,
                      iF32Array* values);
    
    PIFunction()
    void piUniform3f(int32_t program,
                     const std::string& name,
                     float v1,
                     float v2,
                     float v3);
    
    PIFunction()
    void piUniform3fv(int32_t program,
                      const std::string& name,
                      iF32Array* values);
    
    PIFunction()
    void piUniform4f(int32_t program,
                     const std::string& name,
                     float v1,
                     float v2,
                     float v3,
                     float v4);
    
    PIFunction()
    void piUniform4fv(int32_t program,
                      const std::string& name,
                      iF32Array* values);
    
    PIFunction()
    void piUniformVec3v(int32_t program, const std::string&name, iVec3Array* values);
    
    PIFunction()
    void piUniformVec3(int32_t program, const std::string& name, const vec3& value);
    
    PIFunction()
    void piUniformVec4(int32_t program, const std::string& name, const vec4& value);
    
    PIFunction()
    void piUniformRect(int32_t program, const std::string& name, const rect& value);
    
    PIFunction()
    void piUniformMat4f(int32_t program,
                        const std::string& name, const mat4& value);
    
    PIFunction()
    void piUniformMat4fv(int32_t program, const std::string& name, iMat4Array* values);
    
    //----------------------------------------------------------------------
    
    PIFunction()
    int32_t piCreateVertexArray();
    
    PIFunction()
    void piBindVertexArray(int32_t name);
    
    PIFunction()
    int32_t piCreateBuffer();
    
    PIFunction()
    void piBindBuffer(int target, int32_t name);
    
    PIFunction()
    void piBufferData(int target, int32_t name, int64_t size, iMemory* data);
    
    PIFunction()
    void piBufferSubData(int target, int64_t offset, int64_t size, iMemory* data);
    
    
    //----------------------------------------------------------------------
    
    PIFunction()
    void piGenerateMipmap(int target);
    
    PIFunction()
    int32_t piCreateTexture();
    
    PIFunction()
    void piActiveTexture(int32_t unit);
    
    PIFunction()
    void piBindTexture(int32_t target, int32_t name);
    
    PIFunction()
    void piTexParam(int32_t target, int32_t name, int32_t value);
    
    PIFunction()
    void piTexImage2D(int target,
                      int32_t level,
                      int32_t format,
                      iBitmap *bitmap,
                      int32_t planar);
    
    PIFunction()
    void piCompressedTexImage2D(int target,
                                int32_t level,
                                int32_t format,
                                iBitmap *bitmap,
                                int32_t planar);
    
    PIFunction()
    void piCopyTexImage2D(int target,
                          int32_t level,
                          int32_t format,
                          int32_t x,
                          int32_t y,
                          int32_t width,
                          int32_t height);
    
    PIFunction()
    void piTexSubImage2D(int target,
                         int32_t level,
                         int32_t xOffset, int32_t yOffset,
                         int32_t width, int32_t height,
                         iBitmap *bitmap,
                         int32_t planar);
    
    PIFunction()
    void piCompressedTexSubImage2D(int target,
                                   int32_t level,
                                   int32_t xOffset, int32_t yOffset,
                                   int32_t width, int32_t height,
                                   iBitmap *bitmap,
                                   int32_t planar);
    
    PIFunction()
    void piCopyTexSubImage2D(int target,
                             int32_t level,
                             int32_t format,
                             int32_t xOffset, int32_t yOffset,
                             int32_t x, int32_t y,
                             int32_t width, int32_t height);
    
    PIFunction()
    void piPixelStorei(int name, int32_t value);
    
    
    //----------------------------------------------------------------------
    
    PIFunction()
    int32_t piGetFramebuffer();
    
    PIFunction()
    int32_t piCreateFramebuffer();
    
    PIFunction()
    void piBindFramebuffer(int target, int32_t name);
    
    PIFunction()
    void piFramebufferTexture2D(int target,
                                int attachment,
                                int textureTarget,
                                int texture,
                                int32_t level);
    
    PIFunction()
    void piFramebufferRenderbuffer(int target, int attachment, int32_t renderbuffer);
    
    PIFunction()
    void piVerifyFramebufferState(int target);
    
    PIFunction()
    int32_t piCreateRenderbuffer();
    
    PIFunction()
    void piBindRenderbuffer(int32_t name);
    
    PIFunction()
    void piRenderbufferStorage(int format, int32_t width, int32_t height);
    
    
    
    PIInterface()
    struct iGraphicsObject : public iRefObject
    {
        PI_DEFINE(iGraphicsObject)
        
        PIGetter()
        virtual int32_t GetName() const = 0;
        
        PISetter()
        virtual void SetName(int32_t name) = 0;
    };
    
    PIFunction()
    iGraphicsObject* CreateGraphicsObject();
}

#endif
































