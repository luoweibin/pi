/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.5   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_REFLECTION_H
#define PI_CORE_REFLECTION_H

namespace nspi
{
    
    struct iClass;
    struct iClassLoader;
    
    PIInterface()
    struct iEnum : public iRefObjectReadonly
    {
        PI_DEFINE(iEnum)
        
        virtual std::string GetName() const = 0;
        
        virtual void SetName(const std::string& name) = 0;
        
        virtual std::string GetFullName() const = 0;
        
        virtual void SetFullName(const std::string& name) = 0;
        
        virtual int GetValue(const std::string& name, int defValue = -1) const = 0;
        
        virtual bool HasValue(const std::string& name) const = 0;
        
        virtual std::string GetString(int value) const = 0;
        
        virtual void RegisterValue(const std::string& name, int value) = 0;
    };
    
    iEnum* CreateEnum();
    
    PIInterface(HasCreator = false)
    struct iProperty : public iRefObjectReadonly
    {
        PI_DEFINE(iProperty)
        
        PIMethod()
        virtual iClass* GetTypeClass() const = 0;
        
        PIMethod()
        virtual std::string GetName() const = 0;
        
        PIMethod()
        virtual Var GetValue(iRefObject* object) const = 0;
        
        PIMethod()
        virtual void SetValue(iRefObject* object, const Var& value) = 0;
        
        PIMethod()
        virtual Var GetConfig(const std::string& name) const = 0;
        
        PIMethod()
        virtual void SetConfig(const std::string& name, const Var& value) = 0;
    };
    
    PI_DEFINE_OBJECT_ARRAY(iProperty);
    iPropertyArray* CreatePropertyArray();
    
    PIInterface(HasCreator = false)
    struct iMethod : public iRefObjectReadonly
    {
        PI_DEFINE(iMethod)
        
        PIMethod()
        virtual std::string GetName() const = 0;
        
        PIMethod()
        virtual Var GetConfig(const std::string& name) const = 0;
        
        PIMethod()
        virtual void SetConfig(const std::string& name, const Var& value) = 0;
        
        virtual Var Call(iRefObject* object,
                         const Var& arg0 = Var(),
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var(),
                         const Var& arg6 = Var(),
                         const Var& arg7 = Var()) = 0;
    };
    
    PI_DEFINE_OBJECT_ARRAY(iMethod);
    iMethodArray* CreateMethodArray();
    
    PIInterface(HasCreator = false)
    struct iClass : public iRefObjectReadonly
    {
        PI_DEFINE(iClass)
        
        PIMethod()
        virtual int GetType() const = 0;
        
        PIMethod()
        virtual bool IsArray() const = 0;
        
        PIMethod()
        virtual iClass* GetParent() const = 0;
        
        PIMethod()
        virtual iClass* GetElementClass() const = 0;
        
        PIMethod()
        virtual Var CreateInstance() = 0;
        
        PIMethod()
        virtual std::string GetName() const = 0;
        
        PIMethod()
        virtual std::string GetFullName() const = 0;
        
        PIMethod()
        virtual iMethodArray* GetMethods() const = 0;
        
        PIMethod()
        virtual iMethod* GetMethod(const std::string& name) const = 0;
        
        PIMethod()
        virtual iPropertyArray* GetProperties() const = 0;
        
        PIMethod()
        virtual iProperty* GetProperty(const std::string& name) const = 0;
        
        PIMethod()
        virtual bool InstanceOf(const Var& value) const = 0;
        
        PIMethod()
        virtual bool SubclassOf(iClass* klass) const = 0;
        
        PIMethod()
        virtual iClassLoader* GetClassLoader() const = 0;
    };
    
    PI_DEFINE_OBJECT_ARRAY(iClass);
    iClassArray* CreateClassArray();
    
    PIInterface(HasCreator = false)
    struct iClassLoader : public iRefObject
    {
        PI_DEFINE(iClassLoader)
        
        PIMethod()
        virtual void SetParent(iClassLoader* loader) = 0;
        
        PIMethod()
        virtual iClassLoader* GetParent() const = 0;
        
        PIMethod()
        virtual iClass* FindClass(const std::string& name) const = 0;
        
        PIMethod()
        virtual void RegisterClass(const std::string& name, iClass* klass) = 0;
        
        PIMethod()
        virtual void UnregisterClass(iClass* klass) = 0;
        
        PIMethod()
        virtual void RegisterEnum(const std::string& name, iEnum* enumClass) = 0;
        
        PIMethod()
        virtual void UnregisterEnum(const std::string& name) = 0;
        
        PIMethod()
        virtual iEnum* FindEnum(const std::string& name) const = 0;
    };
    
    iClassLoader* piGetRootClassLoader();
  
    
    
    struct PrimitiveClass
    {
        static iClass* Boolean();
        
        static iClass* I8();
        
        static iClass* U8();
        
        static iClass* I16();
        
        static iClass* U16();
        
        static iClass* I32();
        
        static iClass* U32();
        
        static iClass* I64();
        
        static iClass* U64();
        
        static iClass* F32();
        
        static iClass* F64();
        
        static iClass* String();
        
        static iClass* Vec2();
        
        static iClass* Vec3();
        
        static iClass* Vec4();
        
        static iClass* Mat4();
        
        static iClass* Quat();
        
        static iClass* Rect();
            
        static iClass* Var();
        
        static iClass* Get(int type);
    };
    
    
}

#endif





















