/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2012.8.16   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_CORETIME_H
#define PI_CORE_CORETIME_H

namespace nspi
{
    struct iTime : public iRefObject
    {
        virtual ~iTime() {}
        
        virtual int64_t GetValue() const = 0;
        
        virtual void SetValue(int64_t value) = 0;
        
        virtual int64_t GetTimeScale() const = 0;
        
        virtual void SetTimeScale(int64_t value) = 0;
        
        virtual void Rescale(int64_t timeScale) = 0;
        
        virtual iTime* Clone() const = 0;
        
        virtual int64_t Timestamp() const = 0;
    };
    
    iTime* CreateTime(int64_t value, int64_t timeScale);
    
    iTime* piGetCurrentTime();
    
    static inline int64_t piTimeRescale(int64_t value, int64_t src, int64_t dest)
    {
        return value * (double(dest) / double(src));
    }
    
    
    struct iTimeStamp : public iRefObject
    {
        virtual ~iTimeStamp() {}
        
        virtual iTime* GetHostTime() const = 0;
        
        virtual void SetHostTime(iTime *time) = 0;
        
        virtual iTime* GetFrameTime() const = 0;
        
        virtual void SetFrameTime(iTime *time) = 0;
    };
    
    iTimeStamp* CreateTimeStamp(iTime *frameTime, iTime *hostTime);
    
    
    
    /**
     * @export
     */
    int64_t piGetSystemTimeMS();
    
    int64_t piGetUpTimeUS();
    
    /**
     * @export
     */
    void piSleepMS(int64_t timeout);
}

#endif


















