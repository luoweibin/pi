/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2012.9.12   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_MESSAGESYSTEM_H
#define PI_CORE_MESSAGESYSTEM_H

namespace nspi
{
    PIInterface(HasCreator = false)
    struct iMessage : public iRefObject
    {
        PI_DEFINE(iMessage)
        
        PIMethod()
        virtual int32_t GetID() const = 0;
        
        PIMethod()
        virtual Var GetArg1() const = 0;
        
        PIMethod()
        virtual Var GetArg2() const = 0;
        
        PIMethod()
        virtual void SetSender(iRefObject* sender) = 0;
        
        PIMethod()
        virtual iRefObject* GetSender() const = 0;
        
        PIMethod()
        virtual iMessage* Clone() const = 0;
    };
    
    PIMethod()
    iMessage* CreateMessage(uint32_t messageId,
                            const Var& arg1=Var(),
                            const Var& arg2=Var());
    
    template <class T>
    static inline T* piGetMessageArg1(iMessage* msg)
    {
        SmartPtr<T> o = piQueryVarObject<T>(msg->GetArg1());
        return o.PtrAndSetNull();
    }
    
    template <class T>
    static inline T* piGetMessageArg2(iMessage* msg)
    {
        SmartPtr<T> o = piQueryVarObject<T>(msg->GetArg2());
        return o.PtrAndSetNull();
    }
    
    struct iMessageQueue : public iRefObject
    {
        virtual ~iMessageQueue() {}
        
        virtual bool Empty() const = 0;
        
        virtual void SendMessage(iRefObject* sender,
                                 int32_t message,
                                 const Var& arg1 = Var(),
                                 const Var& arg2 = Var()) = 0;
        
        virtual void PostMessage(iRefObject* sender,
                                 int32_t message,
                                 const Var& arg1 = Var(),
                                 const Var& arg2 = Var()) = 0;
        
        virtual iMessage *PopMessage() = 0;
        
        virtual iMessage *PeekMessage() = 0;
        
        virtual iMessage *WaitForMessage() = 0;
        
        virtual void Clear() = 0;

        virtual int32_t GetMessageCount() const = 0;
    };
    
    iMessageQueue* CreateMessageQueue();
}

#endif


















