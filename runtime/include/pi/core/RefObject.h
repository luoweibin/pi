/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 luo weibin     2013.6.11   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_REFOBJECT_H
#define PI_CORE_REFOBJECT_H

namespace nspi
{
    struct iClass;
    
    PIInterface(HasCreator = false)
    struct iRefObject
    {
    private:
        std::atomic_int mdRefCount;
        
    public:
        PI_DEFINE(iRefObject)
        
        iRefObject()
        :mdRefCount(0)
        {
        }
        
        virtual void Retain()
        {
            std::atomic_fetch_add(&mdRefCount, 1);
        }
        
        virtual void Release()
        {
            if (std::atomic_fetch_sub(&mdRefCount, 1) == 1)
            {
                delete this;
                return;
            }
        }
        
        virtual void Reduce()
        {
            std::atomic_fetch_sub(&mdRefCount, 1);
        }
        
        virtual int GetRefCount() const
        {
            return mdRefCount;
        }
        
        virtual void SetReadonly()
        {
        }
        
        virtual bool IsReadonly() const
        {
            return false;
        }
    };
    
    PIInterface(HasCreator = false)
    struct iRefObjectReadonly : public iRefObject
    {
    private:
        std::atomic_int mReadonly;
        
    public:

        PI_DEFINE(iRefObjectReadonly)
        
        iRefObjectReadonly():
        mReadonly(0)
        {
        }
        
        virtual void SetReadonly()
        {
            mReadonly.store(1);
        }
        
        virtual bool IsReadonly() const
        {
            return mReadonly.load();
        }
    };
    
    
    struct iRefObjectArray : public iRefObject
    {
        virtual ~iRefObjectArray() {}
        
        virtual bool IsEmpty() const = 0;
        
        virtual iRefObject* GetFront() const = 0;
        
        virtual iRefObject* GetBack() const = 0;
        
        virtual void Insert( int32_t pos, iRefObject* node) = 0;
        
        virtual void PushBack(iRefObject* node) = 0;
        
        virtual void PopBack() = 0;
        
        virtual int32_t GetCount() const = 0;
        
        virtual iRefObject* GetItem(int32_t index) const = 0;
        
        virtual void SetItem(int32_t index, iRefObject* value) = 0;
        
        virtual void Resize(int32_t count) = 0;
        
        virtual void Clear() = 0;
        
        virtual void Remove(int32_t i) = 0;
    };
    
    iRefObjectArray* CreateRefObjectArray();
    
    ////////////////////////////////////////////////////////////////////////////
    
    template<class T>
    class SmartPtr
    {
    private:
        T* mPtr;
        
    public:
        SmartPtr() : mPtr(nullptr) {}
        
        SmartPtr(const T* _p)
        {
            mPtr = const_cast<T*>(_p);
            if (mPtr != nullptr)
                mPtr->Retain();
        }
        
        SmartPtr(const SmartPtr<T>& _p)
        : mPtr(_p.mPtr)
        {
            if (mPtr != nullptr)
                mPtr->Retain();
        }
        
        ~SmartPtr()
        {
            if (mPtr != nullptr)
                mPtr->Release();
        }
        
        //! Null the smart pointer and return it's contained pointer.
        //! \remark This method makes sure that the pointer returned is not released.
        //!			It can return zero reference objects.
        T* PtrAndSetNull()
        {
            if (!mPtr) return nullptr;
            T* rawPtr = mPtr;
            mPtr = nullptr;
            rawPtr->Reduce();
            return rawPtr;
        }
        
        // Get the pointer
        T* Ptr() const
        {
            return mPtr;
        }
        
        // Get the pointer's address
        T** PtrPtr() const
        {
            return const_cast<T**>(&mPtr);
        }
        
        // Casting to normal pointer.
        // \remark Yes we return a non-const pointer from the const cast operator, this allows
        //         'if (ptr)' to work seamlessly without having implicit cast to 'int' which
        //         would happen if we'd override to cast to bool.
        operator const T* () const
        {
            return mPtr;
        }
        
        operator T* () const
        {
            return mPtr;
        }
        
        // Dereference operator, allow dereferencing smart pointer just like regular pointer.
        T& operator * () const
        {
            return *mPtr;
        }
        
        // Arrow operator, allow to use regular C syntax to access members of class.
        T* operator -> (void) const
        {
            return mPtr;
        }
        
        // Replace pointer.
        SmartPtr& operator = (T* newp)
        {
            if (newp != mPtr)
            {
                if (newp != nullptr)
                {
                    newp->Retain();
                }
                if (mPtr != nullptr)
                {
                    mPtr->Release();
                }
            }
            mPtr = newp;
            return *this;
        }
        
        // Replace pointer.
        SmartPtr& operator = (const SmartPtr<T> &newp)
        {
            if (newp.mPtr != mPtr)
            {
                if (newp.mPtr != nullptr)
                {
                    newp.mPtr->Retain();
                }
                if (mPtr != nullptr)
                {
                    mPtr->Release();
                }
            }
            mPtr = newp.mPtr;
            return *this;
        }
        
        // Cast to boolean, simplify if statements with smart pointers.
        bool IsNull() const
        {
            return mPtr == nullptr;
        };
        bool operator !() const
        {
            return mPtr == nullptr;
        };
        
        bool operator == (T* null) const
        {
            return mPtr == null;
        }
        
        bool operator != (T* null) const
        {
            return mPtr != null;
        }
    };
    
    template<class T, class U> inline bool operator==(SmartPtr<T> const& a, SmartPtr<U> const& b)
    {
        return a.Ptr() == b.Ptr();
    }
    template<class T, class U> inline bool operator!=(SmartPtr<T> const& a, SmartPtr<U> const& b)
    {
        return a.Ptr() != b.Ptr();
    }
    template<class T, class U> inline bool operator<(SmartPtr<T> const& a, SmartPtr<U> const& b)
    {
        return a.Ptr() < b.Ptr();
    }
    template<class T, class U> inline bool operator>(SmartPtr<T> const& a, SmartPtr<U> const& b)
    {
        return a.Ptr() > b.Ptr();
    }
    template<class T, class U> inline bool operator<=(SmartPtr<T> const& a, SmartPtr<U> const& b)
    {
        return a.Ptr() <= b.Ptr();
    }
    template<class T, class U> inline bool operator>=(SmartPtr<T> const& a, SmartPtr<U> const& b)
    {
        return a.Ptr() >= b.Ptr();
    }
}

#endif
































