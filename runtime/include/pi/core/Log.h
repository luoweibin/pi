/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2012.8.30   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_LOG_H
#define PI_CORE_LOG_H

namespace nspi
{
    enum
    {
        LOG_DISABLED    = 0,
        LOG_ERROR       = 10,
        LOG_WARNING     = 20,
        LOG_SYSTEM      = 30,
        LOG_INFO        = 40,
        LOG_DEBUG       = 50,
        LOG_VERBOSE     = 60,
    };
    
    /**
     * @export
     */
    enum
    {
        eLog_Console,
        eLog_File,
        eLog_Global,
    };
    
    /**
     * @export
     */
    void piSetLogLevel(int backend, int level);
    
    /**
     * @export
     */
    void piSetLogFilter(const std::string tag);
        
    void _piLog(const char_t* pszFile, int dLine, int dLevel, const char_t* pszFormat, ...);
    void _piLogT(const char_t* pszFile, int dLine, int dLevel, const char_t* pszTag, const char_t* pszFormat, ...);
    void _piLogv(const char_t* pszFile, int dLine, int dLevel, const char_t* pszFormat, va_list args);
    void _piLogvT(const char_t* pszFile, int dLine, int dLevel, const char_t* pszTag, const char_t* pszFormat, va_list args);
}

#define piLog(level, ...) _piLog(TT(__FILE__), __LINE__, (level), __VA_ARGS__)
#define piLogT(level, tag, ...) _piLogT(TT(__FILE__), __LINE__, (level), (tag), __VA_ARGS__)
#define piLogv(level, args) _piLogv(TT(__FILE__), __LINE__, (level), (args))
#define piLogvT(level, tag, args) _piLogvT(TT(__FILE__), __LINE__, (level), (tag), (args))


#if 1
#define PILOGE(tag, ...) _piLogT(TT(__FILE__), __LINE__, LOG_ERROR, (tag), __VA_ARGS__)
#define PILOGW(tag, ...) _piLogT(TT(__FILE__), __LINE__, LOG_WARNING, (tag), __VA_ARGS__)
#define PILOGS(tag, ...) _piLogT(TT(__FILE__), __LINE__, LOG_SYSTEM, (tag), __VA_ARGS__)
#define PILOGI(tag, ...) _piLogT(TT(__FILE__), __LINE__, LOG_INFO, (tag), __VA_ARGS__)

#   if PI_DEBUG
#       define PILOGD(tag, ...) _piLogT(TT(__FILE__), __LINE__, LOG_DEBUG, (tag), __VA_ARGS__)
#       define PILOGV(tag, ...) _piLogT(TT(__FILE__), __LINE__, LOG_VERBOSE, (tag), __VA_ARGS__)
#   else
#       define PILOGD(tag, ...)
#       define PILOGV(tag, ...)
#   endif

#else
#define PILOGE(tag, ...)
#define PILOGW(tag, ...)
#define PILOGS(tag, ...)
#define PILOGI(tag, ...)
#define PILOGD(tag, ...)
#define PILOGV(tag, ...)
#endif

#endif
























































