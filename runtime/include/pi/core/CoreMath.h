/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin      2014.4.14   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_COREMATH_H
#define PI_CORE_COREMATH_H

#include <cmath>
#include <piglm/glm.h>
#include <piglm/gtx/transform.h>
#include <piglm/gtc/quaternion.h>
#include <piglm/gtx/quaternion.h>
#include <piglm/gtc/type_ptr.h>
#include <piglm/gtx/rotate_vector.h>
#include <piglm/gtc/matrix_transform.h>
#include <piglm/gtx/matrix_interpolation.h>
#include <piglm/gtc/epsilon.h>
#include <piglm/gtx/euler_angles.h>

namespace nspi
{
    typedef piglm::vec2 vec2;
    typedef piglm::vec3 vec3;
    typedef piglm::vec4 vec4;
    typedef piglm::mat2 mat2;
    typedef piglm::mat3 mat3;
    typedef piglm::mat4 mat4;
    typedef piglm::quat quat;
    
    static inline bool piEqualEpsilon(double left, double right, double epsilon)
    {
        return piglm::epsilonEqual(left, right, epsilon);
    }
    
    static inline bool piEqual(double left, double right)
    {
        return piEqualEpsilon(left, right, PI_EPSILON);
    }
    
    static inline bool piNotEqualEpsilon(double left, double right, double epsilon)
    {
        return piglm::epsilonNotEqual(left, right, epsilon);
    }
    
    static inline bool piNotEqual(double left, double right)
    {
        return piEqualEpsilon(left, right, PI_EPSILON);
    }
    
    struct rect
    {
        float x;
        float y;
        float width;
        float height;
        
        rect():
        x(0), y(0), width(0), height(0)
        {
        }
        
        rect(float x, float y, float width, float height)
        {
            this->x = x;
            this->y = y;
            this->width = width;
            this->height = height;
        }
        
        rect(const rect& r)
        {
            x = r.x;
            y = r.y;
            width = r.width;
            height = r.height;
        }
        
        bool operator==(const rect& a) const
        {
            return piEqual(x, a.x)
            && piEqual(y, a.y)
            && piEqual(width, a.width)
            && piEqual(height, a.height);
        }
    };
    
    static inline vec4 piNormalize(const vec4& v)
    {
        return vec4(v.x/v.w, v.y/v.w, v.z/v.w, 1.0);
    }
    
    static inline vec4 piViewportToNDC(const vec4& pos,
                                       float viewportWidth,
                                       float viewportHeight,
                                       float near)
    {
        return vec4(pos.x * 2.0f / viewportWidth - 1.0f, pos.y * 2.0f / viewportHeight - 1.0f, near, 1.0f);
    }
}

#endif

























