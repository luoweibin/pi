/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin      2015.1.12   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_ASYNC_H
#define PI_CORE_ASYNC_H

namespace nspi
{
    /**
     * 事件
     * 当事件有信号时，Wait和TimedWait会堵塞。信号减为0时，等待退出。
     */
    struct iEvent : public iRefObject
    {
        virtual ~iEvent() {}
        
        /**
         * 重置信号，每次信号减为0后必须reset采用继续使用。
         */
        virtual void Reset() = 0;
        
        virtual void Lock() = 0;
        
        virtual void Unlock() = 0;
        
        virtual void Wait() = 0;
        
        virtual bool TimedWait(int64_t ms) = 0;
        
        /**
         * 调用一次信号减1，只通知一个线程
         */
        virtual void Fire() = 0;
        
        /**
         * 调用一次信号减1，通知所有线程
         */
        virtual void FireAll() = 0;
        
        /**
         * 调用一次信号减为0，只通知一个线程
         */
        virtual void Trigger() = 0;
        
        /**
         * 调用一次信号减为0，通知所有线程
         */
        virtual void TriggerAll() = 0;
        
        virtual int32_t GetSignals() const = 0;
    };
    
    iEvent* CreateEvent(int32_t signals);
    
    
    
    struct iFuture : public iRefObject
    {
        virtual ~iFuture() {}
        
        virtual void Reset() = 0;

        virtual void Wait() = 0;
        
        virtual bool TimedWait(int64_t ms) = 0;
        
        virtual void SetValue(const Var& value) = 0;
        
        virtual Var GetValue() const = 0;
        
        virtual bool IsValid() const = 0;
    };
    
    iFuture* CreateFuture();
}


#endif































