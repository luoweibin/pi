/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2015.4.11   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_TOSTRING_H
#define PI_CORE_TOSTRING_H

#include <string>

namespace nspi
{
    typedef std::string String;
    
    static inline std::string piToString(int32_t v)
    {
        return piFormat("%d", v);
    }
    
    static inline std::string piToString(uint32_t v)
    {
        return piFormat("%u", v);
    }
    
    static inline std::string piToString(int64_t v)
    {
        return piFormat("%lld", v);
    }
    
    static inline std::string piToString(uint64_t v)
    {
        return piFormat("%llu", v);
    }
    
    static inline std::string piToString(double v)
    {
        return piFormat("%f", v);
    }
 
    static inline std::string piToString(const nspi::iRefObject* v)
    {
        return v ? v->ToString() : "iRefObject(nullptr)";
    }
    
    static inline std::string piToString(const std::string& v)
    {
        return piFormat("string(%d)", v.size());
    }
    
    static inline std::string piToString(const vec2& v)
    {
        return piFormat("vec2{%f, %f}", v.x, v.y);
    }
    
    static inline std::string piToString(const vec3& v)
    {
        return piFormat("vec3{%f, %f, %f}", v.x, v.y, v.z);
    }
    
    static inline std::string piToString(const vec4& v)
    {
        return piFormat("vec4{%f, %f, %f, %f}", v.x, v.y, v.z, v.w);
    }
    
    static inline std::string piToString(const quat& v)
    {
        return piFormat("quat{%f, %f, %f, %f}", v.x, v.y, v.z, v.w);
    }
    
    static inline std::string piToString(const rect& v)
    {
        return piFormat("rect{%f, %f, %f, %f}", v.x, v.y, v.width, v.height);
    }
    
    static inline std::string piToString(const mat4& v)
    {
        return piFormat("mat4{%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f}",
                        v[0][0], v[0][1], v[0][2], v[0][3],
                        v[1][0], v[1][1], v[1][2], v[1][3],
                        v[2][0], v[2][1], v[2][2], v[2][3],
                        v[3][0], v[3][1], v[3][2], v[3][3]);
    }
}


#endif







































