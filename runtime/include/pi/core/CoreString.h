/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2013.6.11   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_CORESTRING_H
#define PI_CORE_CORESTRING_H


#define kuUTF8_SB_7F ((char)0x0)
#define kuUTF8_SB_7FF ((char)0xC0)
#define kuUTF8_SB_FFFF ((char)0xE0)
#define kuUTF8_SB_10FFFF ((char)0xF0)
#define kuUTF8_SB_Part ((char)0x80)

#define kuUTF8_SBM_7F ((char)0x80)
#define kuUTF8_SBM_7FF ((char)0xE0)
#define kuUTF8_SBM_FFFF ((char)0xF0)
#define kuUTF8_SBM_10FFFF ((char)0xF8)
#define kuUTF8_SBM_Part ((char)0xC0)

#define kuUTF8_VM_Part ((char)0x3F)
#define kuUTF8_VM_7FF ((char)0x1F)
#define kuUTF8_VM_FFFF ((char)0x0F)
#define kuUTF8_VM_10FFFF ((char)0x07)

#define kuMaxSingleCodePointUTF16 ((char32_t)0x10000)
#define kuSurrogateHighMin ((char16_t)0xD800)
#define kuSurrogateHighMax ((char16_t)0xDBFF)
#define kuSurrogateLowMin ((char16_t)0xDC00)
#define kuSurrogateLowMax ((char16_t)0xDFFF)
#define kuUTF8_VM_PartUTF16 ((char16_t)0x0C00)

#define kuUTF8_VM_HighBitsUTF16 ((char32_t)0xFFC00)
#define kuUTF8_VM_LowBitsUTF16 ((char32_t)0x3FF)

#define kuMaxUnicodeCodePoint 0x10FFFF
#define kuInvalidUnicodeCodePoint 0xFFFFFFFFU

#include <string>
#include <vector>

namespace nspi
{
    enum eCharset
    {
        eCharset_UTF8 = 1,
        eCharset_UTF16 = 2,
    };
    
    static inline bool piIsValidUnicodeChar(char32_t c)
    {
        return c <= kuMaxUnicodeCodePoint;
    }
    
    template <typename CharType>
    struct StringInterface
    {};
    
    template<>
    struct StringInterface<char>
    {
        static int64_t Length(const char *string)
        {
            return strlen(string);
        }
        
        static int64_t Compare(const char *a, const char *b)
        {
            return strcmp(a, b);
        }
        
        static int64_t CaseCompare(const char *a, const char *b)
        {
            return strcasecmp(a, b);
        }
        
        static int64_t Print(char *buffer, int64_t size, const char *format, ...)
        {
            va_list args;
            va_start(args, format);
            int64_t ret = vsnprintf(buffer, (size_t)size, format, args);
            va_end(args);
            return ret;
        }
        
        static int64_t PrintV(char *buffer, int64_t size, const char *format, va_list args)
        {
            return vsnprintf(buffer, (size_t)size, format, args);
        }
        
        /**
         * \param c UNICODE字符
         * \return 返回UTF8字符数
         */
        static int64_t CharSize(char32_t c)
        {
            if (c <= 0x7F)
            {
                return 1;
            }
            else if (c <= 0x7FF)
            {
                return 2;
            }
            else if (c <= 0xFFFF)
            {
                return 3;
            }
            else if (c <= 0x10FFFF)
            {
                return 4;
            }
            else
            {
                return 0;
            }
        }
        
        /**
         * 判断当前UTF8字符数
         */
        static int64_t CodePointSize(const char* start)
        {
            char c = *start;
            if ((kuUTF8_SBM_10FFFF & c) == kuUTF8_SB_10FFFF)
            {
                return 4;
            }
            else if ((kuUTF8_SBM_FFFF & c) == kuUTF8_SB_FFFF)
            {
                return 3;
            }
            else if ((kuUTF8_SBM_7FF & c) == kuUTF8_SB_7FF)
            {
                return 2;
            }
            else if ((c & kuUTF8_SBM_7F) == kuUTF8_SB_7F)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        
        static char* Encode(char* start, char32_t c)
        {
            if (c <= 0x7F)
            {
                start[0] = (char)c;
                return start + 1;
            }
            else if (c <= 0x7FF)
            {
                start[0] = (char)((0x7C0 & c) >> 6) | kuUTF8_SB_7FF;
                start[1] = (char)(0x3F & c) | kuUTF8_SB_Part;
                return start + 2;
            }
            else if (c <= 0xFFFF)
            {
                start[0] = (char)(((0xF000) & c) >> 12) | kuUTF8_SB_FFFF;
                start[1] = (char)((0xFC0 & c) >> 6) | kuUTF8_SB_Part;
                start[2] = (char)(0x3F & c) | kuUTF8_SB_Part;
                return start + 3;
            }
            else if (c <= 0x10FFFF)
            {
                start[0] = (char)((0x1C0000 & c) >> 18) | kuUTF8_SB_10FFFF;
                start[1] = (char)((0x3F000 & c) >> 12) | kuUTF8_SB_Part;
                start[2] = (char)((0xFC0 & c) >> 6) | kuUTF8_SB_Part;
                start[3] = (char)(0x3F & c) | kuUTF8_SB_Part;
                return start + 4;
            }
            else
            {
                return start;
            }
        }
        
        static char32_t Decode(const char *start)
        {
            char c = *start;
            if ((kuUTF8_SBM_10FFFF & c) == kuUTF8_SB_10FFFF)
            {
                char32_t ch = 0;
                ch |= ((char32_t)(c & kuUTF8_VM_10FFFF)) << 18;
                ch |= ((char32_t)(start[1] & kuUTF8_VM_Part)) << 12;
                ch |= ((char32_t)(start[2] & kuUTF8_VM_Part)) << 6;
                ch |= (char32_t)(start[3] & kuUTF8_VM_Part);
                return ch;
            }
            else if ((kuUTF8_SBM_FFFF & c) == kuUTF8_SB_FFFF)
            {
                char32_t ch = 0;
                ch |= ((char32_t)(c & kuUTF8_VM_FFFF)) << 12;
                ch |= ((char32_t)(start[1] & kuUTF8_VM_Part)) << 6;
                ch |= (char32_t)(start[2] & kuUTF8_VM_Part);
                return ch;
            }
            else if ((kuUTF8_SBM_7FF & c) == kuUTF8_SB_7FF)
            {
                char32_t ch = 0;
                ch |= ((char32_t)(c & kuUTF8_VM_7FF)) << 6;
                ch |= (char32_t)(start[1] & kuUTF8_VM_Part);
                return ch;
            }
            else if ((c & kuUTF8_SBM_7F) == kuUTF8_SB_7F)
            {
                return c;
            }
            else
            {
                return kuInvalidUnicodeCodePoint;
            }
        }
        
        static const char *Move(const char *start, const char *end, int64_t count)
        {
            if (count > 0)
            {
                const char* current = start;
                int64_t i = count;
                while (current != end)
                {
                    int64_t codeSize = CodePointSize(current);
                    if (codeSize > 0)
                    {
                        current += codeSize;
                        --i;
                        if (i == 0)
                        {
                            return current;
                        }
                    }
                    else
                    {
                        ++current;
                    }
                }
                return end;
            }
            else if (count < 0)
            {
                const char* current = start;
                int64_t i = -count;
                while (current != end)
                {
                    int64_t codeSize = CodePointSize(current);
                    --current;
                    if (codeSize > 0)
                    {
                        --i;
                        if (i == 0)
                        {
                            return current;
                        }
                    }
                }
                return end;
            }
            else
            {
                return start;
            }
        }
    };
    
    //--------------------------------------------------------------------------
    
    template <>
    struct StringInterface<char16_t>
    {
        static int64_t Length(const char16_t *string)
        {
            return wcslen((wchar_t*)string);
        }
        
        static int64_t Print(char16_t *buffer, int64_t size, const char16_t *format, ...)
        {
            va_list args;
            va_start(args, format);
            int64_t ret = vswprintf((wchar_t*)buffer, (size_t)size, (const wchar_t*)format, args);
            va_end(args);
            return ret;
        }
        
        static int64_t PrintV(char16_t *buffer, int64_t size, const char16_t *format, va_list args)
        {
            return vswprintf((wchar_t*)buffer, (size_t)size, (const wchar_t*)format, args);
        }
        
        static int64_t CharSize(char32_t c)
        {
            if (c < kuMaxSingleCodePointUTF16)
            {
                return 1;
            }
            else
            {
                return 2;
            }
        }
        
        static int64_t CodePointSize(const char16_t *start)
        {
            char16_t w1 = *start;
            if (w1 < kuSurrogateHighMin || w1 > kuSurrogateLowMax)
            {
                return 1;
            }
            else if (w1 <= kuSurrogateHighMax)
            {
                return 2;
            }
            else
            {
                return 0;
            }
        }
        
        static char16_t *Encode(char16_t *start, char32_t c)
        {
            if (c < kuMaxSingleCodePointUTF16)
            {
                *start = (char16_t)c;
                return start + 1;
            }
            else
            {
                char32_t u = c - kuMaxSingleCodePointUTF16;
                start[0] = kuSurrogateHighMin | ((char16_t)((u & kuUTF8_VM_HighBitsUTF16) >> 10));
                start[1] = kuSurrogateLowMin | ((char16_t)(u & kuUTF8_VM_LowBitsUTF16));
                return start + 2;
            }
        }
        
        static char32_t Decode(const char16_t *start)
        {
            char16_t w1 = *start;
            if (w1 < kuSurrogateHighMin || w1 > kuSurrogateLowMax)
            {
                return w1;
            }
            else if (w1 <= kuSurrogateHighMax
                     && start[1] >= kuSurrogateLowMin && start[1] <= kuSurrogateLowMax)
            {
                char32_t c32 = 0;
                c32 |= (w1 & kuUTF8_VM_PartUTF16) << 10;
                c32 |= start[1] & kuUTF8_VM_PartUTF16;
                c32 += kuMaxSingleCodePointUTF16;
                return c32;
            }
            else
            {
                return kuInvalidUnicodeCodePoint;
            }
        }
        
        static const char16_t *Move(const char16_t *start, const char16_t *end, int64_t count)
        {
            if (count > 0)
            {
                const char16_t* current = start;
                int64_t i = count;
                while (current != end)
                {
                    int64_t codeSize = CodePointSize(current);
                    if (codeSize > 0)
                    {
                        current += codeSize;
                        --i;
                        if (i == 0)
                        {
                            return current;
                        }
                    }
                    else
                    {
                        ++current;
                    }
                }
                return end;
            }
            else if (count < 0)
            {
                const char16_t* current = start;
                int64_t i = count;
                while (current != end)
                {
                    int64_t codeSize = CodePointSize(current);
                    --current;
                    if (codeSize > 0)
                    {
                        --i;
                        if (i == 0)
                        {
                            return current;
                        }
                    }
                }
                return end;
            }
            else
            {
                return start;
            }
        }
    };
    
    //--------------------------------------------------------------------------

    std::string piTrimLeft(const std::string& str, const std::string& chars);
    std::string piTrimRight(const std::string& str, const std::string& chars);
    std::string piTrim(const std::string& str, const std::string& chars);
    
    //--------------------------------------------------------------------------
        
    template <typename CharType>
    int64_t piStrSize(const CharType *string, int64_t size)
    {
        piAssert(string != NULL, 0);
        piCheck(size > 0, 0);
        
        int64_t count = 0;
        const CharType *current = string;
        const CharType *end = string + size;
        do {
            ++count;
            current = StringInterface<CharType>::Move(current, end, 1);
        } while (current != end);
        
        return count;
    }
    
    template <typename CharType>
    std::vector<std::basic_string<CharType>> piStrSplit(const CharType *v, int64_t size, char32_t c)
    {
        std::vector<std::basic_string<CharType>> result;
        
        const CharType *current = v;
        const CharType *end = current + size;
        
        bool bPushed = false;
        std::basic_string<CharType> seg;
        while (current != end)
        {
            char32_t c32 = StringInterface<CharType>::Decode(current);
            if (c32 == c)
            {
                result.push_back(seg);
                seg.clear();
                bPushed = true;
            }
            else
            {
                bPushed = false;
                seg.append(1, c32);
            }
            current = StringInterface<CharType>::Move(current, end, 1);
        }
        
        if (!bPushed)
        {
            result.push_back(seg);
        }
        
        return result;
    }
    
    template <typename CharType>
    bool piIsEmptyStringTP(const CharType *string)
    {
        return string == NULL || StringInterface<CharType>::Length(string) == 0;
    }
    
    //--------------------------------------------------------------------------
    
    template <typename CharType>
    std::basic_string<CharType> piFormatTP(const CharType *format, ...)
    {
        CharType buffer[PI_FORMAT_BUFFER];
        va_list args;
        va_start(args, format);
        size_t size = (size_t)StringInterface<CharType>::PrintV(buffer, PI_FORMAT_BUFFER, format, args);
        va_end(args);
        return std::basic_string<CharType>(buffer, size);
    }
    
    template <typename CharType>
    std::basic_string<CharType> piFormatVTP(const CharType *format, va_list args)
    {
        CharType buffer[PI_FORMAT_BUFFER];
        size_t size = (size_t)StringInterface<CharType>::PrintV(buffer, PI_FORMAT_BUFFER, format, args);
        return std::basic_string<CharType>(buffer, size);
    }
    
    std::string piBinToHexStringUTF8(const void* pData, int64_t luSize, bool bUpper);
    
    std::u16string piU8ToU16(const std::string& text);
    std::string piU16ToU8(const std::u16string& text);
}

#define piStrSize(string, size) \
nspi::piStrSize<char_t>((string), (size))
#define piStrSizeUTF8(string, size) \
nspi::piStrSize<char>((string), (size))
#define piStrSizeUTF16(string, size) \
nspi::piStrSize<char16_t>((string), (size))
    
#define piIsEmptyString(string) \
nspi::piIsEmptyStringTP<char_t>(string)
#define piIsEmptyStringUTF8(string) \
nspi::piIsEmptyStringTP<char>(string)
#define piIsEmptyStringUTF16(string) \
nspi::piIsEmptyStringTP<char16_t>(string)
    
#define piStrCmp(a, b) \
nspi::StringInterface<char_t>::Compare((a), (b))
#define piStrCmpUTF8(a, b) \
nspi::StringInterface<char>::Compare((a), (b))
#define piStrCmpUTF16(a, b) \
nspi::StringInterface<char16_t>::Compare((a), (b))

#define piStrCaseCmp(a, b) \
nspi::StringInterface<char_t>::CaseCompare((a), (b))
#define piStrCaseCmpUTF8(a, b) \
nspi::StringInterface<char>::CaseCompare((a), (b))
#define piStrCaseCmpUTF16(a, b) \
nspi::StringInterface<char16_t>::CaseCompare((a), (b))
    
#define piStrLen(s) \
nspi::StringInterface<char_t>::Length(s)
#define piStrLenUTF8(s) \
nspi::StringInterface<char>::Length(s)
#define piStrLenUTF16(s) \
nspi::StringInterface<char16_t>::Length(s)
    
#define piPrint(buffer, size, format, ...) \
nspi::StringInterface<char_t>::Print((buffer), (size), (format), __VA_ARGS__)
#define piPrintUTF8(buffer, size, format, ...) \
nspi::StringInterface<char>::Print((buffer), (size), (format), __VA_ARGS__)
#define piPrintUTF16(buffer, size, format, ...) \
nspi::StringInterface<char16_t>::Print((buffer), (size), (format), __VA_ARGS__)
    
#define piPrintV(buffer, size, format, args) \
nspi::StringInterface<char_t>::PrintV((buffer), (size), (format), (args))
#define piPrintVUTF8(buffer, size, format, ...) \
nspi::StringInterface<char>::PrintV((buffer), (size), (format), (args))
#define piPrintVUTF16(buffer, size, format, ...) \
nspi::StringInterface<char16_t>::PrintV((buffer), (size), (format), (args))
    
#define piFormat(format, ...) \
nspi::piFormatTP<char_t>((format), __VA_ARGS__)
#define piFormatUTF8(format, ...) \
nspi::piFormatTP<char>((format), __VA_ARGS__)
#define piFormatUTF16(format, ...) \
nspi::piFormatTP<char16_t>((format), __VA_ARGS__)
    
#define piFormatV(format, args) \
nspi::piFormatVTP<char_t>((format), (args))
#define piFormatVUTF8(format, args) \
nspi::piFormatVTP<char>((format), (args))
#define piFormatVUTF16(format, args) \
nspi::piFormatVTP<char16_t>((format), (args))

#endif
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
