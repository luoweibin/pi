/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin      2016.10.11   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_COREARRAY_H
#define PI_CORE_COREARRAY_H

namespace nspi
{
    struct iMemory;
    
    template <class T, class Array>
    struct iPrimityArrayDecl : public iRefObject
    {
        virtual bool IsEmpty() const = 0;
        
        virtual T GetBack() const = 0;
        
        virtual T GetFront() const = 0;
        
        virtual std::vector<T>& GetValues() = 0;
        
        virtual void Insert( int32_t pos, T o) = 0;
        
        virtual void PushBack(T value) = 0;
        
        virtual void PopBack() = 0;
        
        virtual int32_t GetCount() const = 0;
        
        virtual T GetItem(int32_t index) const = 0;
        
        virtual void SetItem(int32_t index, T value) = 0;
        
        virtual void Resize(int32_t count) = 0;
        
        virtual void Clear() = 0;
        
        virtual void Remove(int32_t i) = 0;
        
        virtual T* GetData() = 0;
        
        virtual iMemory* ToMemory() const = 0;
    };
    
    
    template <class T, class Array>
    struct iStructArrayDecl : public iRefObject
    {
        virtual bool IsEmpty() const = 0;
        
        virtual T GetBack() const = 0;
        
        virtual T GetFront() const = 0;
        
        virtual std::vector<T>& GetValues() = 0;
        
        virtual void Insert( int32_t pos, const T& o) = 0;
        
        virtual void PushBack(const T& value) = 0;
        
        virtual void PopBack() = 0;
        
        virtual int32_t GetCount() const = 0;
        
        virtual T GetItem(int32_t index) const = 0;
        
        virtual void SetItem(int32_t index, const T& value) = 0;
        
        virtual void Resize(int32_t count) = 0;
        
        virtual void Clear() = 0;
        
        virtual void Remove(int32_t i) = 0;
        
        virtual T* GetData() = 0;
    };
    
    
    template <class T, class Array>
    struct iObjectArrayDecl : public iRefObject
    {
        virtual bool IsEmpty() const = 0;
        
        virtual T* GetBack() const = 0;
        
        virtual T* GetFront() const = 0;
        
        virtual std::vector<SmartPtr<T>>& GetValues() = 0;
        
        virtual void Insert( int32_t pos, T* o) = 0;
        
        virtual void PushBack(T* value) = 0;
        
        virtual void PopBack() = 0;
        
        virtual int32_t GetCount() const = 0;
        
        virtual T* GetItem(int32_t index) const = 0;
        
        virtual void SetItem(int32_t index, T* value) = 0;
        
        virtual void Resize(int32_t count) = 0;
        
        virtual void Clear() = 0;
        
        virtual void Remove(int32_t i) = 0;
    };
    
#define PI_DEFINE_PRIMITIVE_ARRAY(type, name) \
    class name : public iPrimityArrayDecl<type, name> \
    { \
    public: \
        typedef type ItemType; \
        typedef std::vector<type> Type; \
        name() \
        { \
        } \
        PI_DEFINE(name) \
    };

#define PI_DEFINE_STRUCT_ARRAY(type, name) \
    class name : public iStructArrayDecl<type, name> \
    { \
    public: \
        typedef type ItemType; \
        typedef std::vector<type> Type; \
        name() \
        { \
        } \
        PI_DEFINE(name) \
    };
    
#define PI_DEFINE_OBJECT_ARRAY(type) \
    class type##Array : public iObjectArrayDecl<type, type##Array> \
    { \
    public: \
        typedef type ItemType; \
        typedef std::vector<SmartPtr<type>> Type; \
        type##Array() \
        { \
        } \
        PI_DEFINE(type##Array) \
        virtual int GetType() const \
        { \
            return eType_ObjectArray; \
        } \
    }

    /**
     * =============================================================================================
     * F32
     */
    
    PI_DEFINE_PRIMITIVE_ARRAY(float, iF32Array);
    
    PIFunction()
    iF32Array* CreateF32Array();
    
    iF32Array* CreateF32ArrayRaw(const float* values, int32_t count);
    
#define CreateF32ArrayEx(arrs)  CreateF32ArrayRaw(arrs, piArrayCount(arrs))
    
    /**
     * =============================================================================================
     * F64
     */
    
    PI_DEFINE_PRIMITIVE_ARRAY(double, iF64Array);
    
    PIFunction()
    iF64Array* CreateF64Array();
    
    iF64Array* CreateF64ArrayRaw(const double* values, int32_t count);
    
    #define CreateF64ArrayEx(arrs)  CreateF64ArrayRaw(arrs, piArrayCount(arrs))
    
    /**
     * =============================================================================================
     * I8
     */
    
    PI_DEFINE_PRIMITIVE_ARRAY(int8_t, iI8Array);
    
    PIFunction()
    iI8Array* CreateI8Array();
    
    iI8Array* CreateI8ArrayRaw(const int8_t* values, int32_t count);
    
    #define CreateI8ArrayEx(arrs)  CreateI8ArrayRaw(arrs, piArrayCount(arrs))
    
    /**
     * =============================================================================================
     * U8
     */
    
    PI_DEFINE_PRIMITIVE_ARRAY(uint8_t, iU8Array);
    
    PIFunction()
    iU8Array* CreateU8Array();
    
    iU8Array* CreateU8ArrayRaw(const uint8_t* values, int32_t count);
    
#define CreateU8ArrayEx(arrs)  CreateU8ArrayRaw(arrs, piArrayCount(arrs))
    
    /**
     * =============================================================================================
     * I16
     */
    
    PI_DEFINE_PRIMITIVE_ARRAY(int16_t, iI16Array);
    
    PIFunction()
    iI16Array* CreateI16Array();
    
    iI16Array* CreateI16ArrayRaw(const int16_t* values, int32_t count);
    
    #define CreateI16ArrayEx(arrs)  CreateI16ArrayRaw(arrs, piArrayCount(arrs))
    
    /**
     * =============================================================================================
     * U16
     */
    
    PI_DEFINE_PRIMITIVE_ARRAY(uint16_t, iU16Array);
    
    PIFunction()
    iU16Array* CreateU16Array();
    
    iU16Array* CreateU16ArrayRaw(const uint16_t* values, int32_t count);
    
#define CreateU16ArrayEx(arrs)  CreateU16ArrayRaw(arrs, piArrayCount(arrs))
    
    /**
     * =============================================================================================
     * I32
     */
    
    PI_DEFINE_PRIMITIVE_ARRAY(int32_t, iI32Array);
    
    PIFunction()
    iI32Array* CreateI32Array();
    
    iI32Array* CreateI32ArrayRaw(const int32_t* values, int32_t count);
    
#define CreateI32ArrayEx(arrs)  CreateI32ArrayRaw(arrs, piArrayCount(arrs))
    
    
    /**
     * =============================================================================================
     * U32
     */
    
    PI_DEFINE_PRIMITIVE_ARRAY(uint32_t, iU32Array);
    
    PIFunction()
    iU32Array* CreateU32Array();
    
    iU32Array* CreateU32ArrayRaw(const uint32_t* values, int32_t count);
    
#define CreateU32ArrayEx(arrs)  CreateU32ArrayRaw(arrs, piArrayCount(arrs))
    
    /**
     * =============================================================================================
     * I64
     */
    
    PI_DEFINE_PRIMITIVE_ARRAY(int64_t, iI64Array);
    
    PIFunction()
    iI64Array* CreateI64Array();
    
    iI64Array* CreateI64ArrayRaw(const int64_t* values, int32_t count);
    
    #define CreateI64ArrayEx(arrs)  CreateI64ArrayRaw(arrs, piArrayCount(arrs))
    
    /**
     * =============================================================================================
     * U64
     */
    
    PI_DEFINE_PRIMITIVE_ARRAY(uint64_t, iU64Array);
    
    PIFunction()
    iU64Array* CreateU64Array();
    
    iU64Array* CreateU64ArrayRaw(const uint64_t* values, int32_t count);
    
#define CreateU64ArrayEx(arrs)  CreateU64ArrayRaw(arrs, piArrayCount(arrs))
    
    /**
     * =============================================================================================
     * string
     */
    
    PI_DEFINE_STRUCT_ARRAY(std::string, iStringArray);
    
    PIFunction()
    iStringArray* CreateStringArray();
    
    /**
     * =============================================================================================
     * Vec2
     */
    
    PI_DEFINE_STRUCT_ARRAY(vec2, iVec2Array);
    
    PIFunction()
    iVec2Array* CreateVec2Array();
    
    iVec2Array* CreateVec2ArrayRaw(const vec2* values, int32_t count);
    
#define CreateVec2ArrayEx(arrs)  CreateVec2ArrayRaw(arrs, piArrayCount(arrs))
    
    
    /**
     * =============================================================================================
     * Vec3
     */
    
    PI_DEFINE_STRUCT_ARRAY(vec3, iVec3Array);
    
    PIFunction()
    iVec3Array* CreateVec3Array();
    
    iVec3Array* CreateVec3ArrayRaw(const vec3* values, int32_t count);
    
#define CreateVec3ArrayEx(arrs)  CreateVec3ArrayRaw(arrs, piArrayCount(arrs))
    
    
    /**
     * =============================================================================================
     * Vec4
     */
    
    PI_DEFINE_STRUCT_ARRAY(vec4, iVec4Array);
    
    PIFunction()
    iVec4Array* CreateVec4Array();
    
    iVec4Array* CreateVec4ArrayRaw(const vec4* values, int32_t count);
    
#define CreateVec4ArrayEx(arrs)  CreateVec4ArrayRaw(arrs, piArrayCount(arrs))
    
    
    /**
     * =============================================================================================
     * Mat4
     */
    
    PI_DEFINE_STRUCT_ARRAY(mat4, iMat4Array);
    
    PIFunction()
    iMat4Array* CreateMat4Array();
    
    iMat4Array* CreateMat4ArrayRaw(const mat4* values, int32_t count);
    
#define CreateMat4ArrayEx(arrs)  CreateMat4ArrayRaw(arrs, piArrayCount(arrs))
    
    
    /**
     * =============================================================================================
     * Quat
     */
    
    PI_DEFINE_STRUCT_ARRAY(quat, iQuatArray);
    
    PIFunction()
    iQuatArray* CreateQuatArray();
    
    iQuatArray* CreateQuatArrayRaw(const quat* values, int32_t count);
    
#define CreateQuatArrayEx(arrs)  CreateQuatArrayRaw(arrs, piArrayCount(arrs))


    /**
     * =============================================================================================
     * Rect
     */
    
    PI_DEFINE_STRUCT_ARRAY(rect, iRectArray);

    PIFunction()
    iRectArray* CreateRectArray();
    
    iRectArray* CreateRectArrayRaw(const rect* values, int32_t count);
    
#define CreateRectArrayEx(arrs)  CreateRectArrayRaw(arrs, piArrayCount(arrs))
    
}


#endif























