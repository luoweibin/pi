/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2013.1.25   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_CORESTREAM_H
#define PI_CORE_CORESTREAM_H

namespace nspi
{
    PIEnum()
    enum eSeek
    {
        eSeek_Set       = 0,
        eSeek_Current   = 1,
        eSeek_End       = 2,
    };
    
    
    PIInterface(HasCreator = false)
    struct iStream : public iRefObject
    {
        PI_DEFINE(iStream)
        
        PIMethod()
        virtual int64_t GetSize() const = 0;
        
        PIMethod()
        virtual int64_t Seek(int64_t offset, int seek) = 0;
        
        PIMethod()
        virtual int64_t GetOffset() const = 0;
        
        PIMethod()
        virtual bool End() const = 0;
        
        virtual int64_t Read(void* pBuffer, int64_t size) = 0;
        
        virtual int64_t Write(const void* pData, int64_t size) = 0;
        
        PIMethod()
        virtual int64_t WriteString(const std::string &v) = 0;
        
        PIMethod()
        virtual int8_t ReadI8(int8_t def = 0) = 0;
        
        PIMethod()
        virtual int64_t WriteI8(int8_t v) = 0;
        
        PIMethod()
        virtual uint8_t ReadU8(uint8_t def = 0U) = 0;
        
        PIMethod()
        virtual int64_t WriteU8(uint8_t v) = 0;
        
        PIMethod()
        virtual int16_t ReadI16(int16_t def = 0) = 0;
        
        PIMethod()
        virtual int64_t WriteI16(int16_t v) = 0;
        
        PIMethod()
        virtual int16_t ReadI16LE(int16_t def = 0) = 0;
        
        PIMethod()
        virtual int64_t WriteI16LE(int16_t v) = 0;
        
        PIMethod()
        virtual int16_t ReadI16BE(int16_t def = 0) = 0;
        
        PIMethod()
        virtual int64_t WriteI16BE(int16_t v) = 0;
        
        PIMethod()
        virtual uint16_t ReadU16(uint16_t def = 0) = 0;
        
        PIMethod()
        virtual int64_t WriteU16(uint16_t v) = 0;
        
        PIMethod()
        virtual uint16_t ReadU16LE(uint16_t def = 0) = 0;
        
        PIMethod()
        virtual int64_t WriteU16LE(uint16_t v) = 0;
        
        PIMethod()
        virtual uint16_t ReadU16BE(uint16_t def = 0) = 0;
        
        PIMethod()
        virtual int64_t WriteU16BE(uint16_t v) = 0;
        
        PIMethod()
        virtual int32_t ReadI32(int32_t def = 0) = 0;
        
        PIMethod()
        virtual int64_t WriteI32(int32_t v) = 0;
        
        PIMethod()
        virtual int32_t ReadI32LE(int32_t def = 0) = 0;
        
        PIMethod()
        virtual int64_t WriteI32LE(int32_t v) = 0;
        
        PIMethod()
        virtual int32_t ReadI32BE(int32_t def = 0) = 0;
        
        PIMethod()
        virtual int64_t WriteI32BE(int32_t v) = 0;
        
        PIMethod()
        virtual uint32_t ReadU32(uint32_t def = 0) = 0;
        
        PIMethod()
        virtual int64_t WriteU32(uint32_t v) = 0;
        
        PIMethod()
        virtual uint32_t ReadU32LE(uint32_t def = 0) = 0;
        
        PIMethod()
        virtual int64_t WriteU32LE(uint32_t v) = 0;
        
        PIMethod()
        virtual uint32_t ReadU32BE(uint32_t def = 0) = 0;
        
        PIMethod()
        virtual int64_t WriteU32BE(uint32_t v) = 0;
        
        PIMethod()
        virtual int64_t ReadI64(int64_t def = 0) = 0;
        
        PIMethod()
        virtual int64_t WriteI64(int64_t v) = 0;
        
        PIMethod()
        virtual int64_t ReadI64LE(int64_t def = 0) = 0;
        
        PIMethod()
        virtual int64_t WriteI64LE(int64_t v) = 0;
        
        PIMethod()
        virtual int64_t ReadI64BE(int64_t def = 0) = 0;
        
        PIMethod()
        virtual int64_t WriteI64BE(int64_t v) = 0;
        
        PIMethod()
        virtual uint64_t ReadU64(uint64_t def = 0) = 0;
        
        PIMethod()
        virtual int64_t WriteU64(uint64_t v) = 0;
        
        PIMethod()
        virtual uint64_t ReadU64LE(uint64_t def = 0) = 0;
        
        PIMethod()
        virtual int64_t WriteU64LE(uint64_t v) = 0;
        
        PIMethod()
        virtual uint64_t ReadU64BE(uint64_t def = 0) = 0;
        
        PIMethod()
        virtual int64_t WriteU64BE(uint64_t v) = 0;
        
        PIMethod()
        virtual int64_t WriteMat4(const mat4& v) = 0;
        
        PIMethod()
        virtual mat4 ReadMat4(const mat4& def = mat4()) = 0;
        
        PIMethod()
        virtual int64_t WriteVec2(const vec2& v) = 0;
        
        PIMethod()
        virtual vec2 ReadVec2(const vec2& def = vec2()) = 0;
        
        PIMethod()
        virtual int64_t WriteVec3(const vec3& v) = 0;
        
        PIMethod()
        virtual vec3 ReadVec3(const vec3& def = vec3()) = 0;
        
        PIMethod()
        virtual int64_t WriteVec4(const vec4& v) = 0;
        
        PIMethod()
        virtual vec4 ReadVec4(const vec4& def = vec4()) = 0;
        
        PIMethod()
        virtual int64_t WriteQuat(const quat& v) = 0;
        
        PIMethod()
        virtual quat ReadQuat(const quat& def = quat()) = 0;
        
        PIMethod()
        virtual float ReadF32(float def = 0.0) = 0;
        
        PIMethod()
        virtual int64_t WriteF32(float v) = 0;
        
        PIMethod()
        virtual double ReadF64(double def = 0.0) = 0;
        
        PIMethod()
        virtual int64_t WriteF64(double v) = 0;
        
        PIMethod()
        virtual bool Flush() = 0;
        
        PIMethod()
        virtual std::string GetUri() const = 0;
        
        PIMethod()
        virtual void SetUri(const std::string& uri) = 0;
    };
    
    PIInterface()
    struct iStreamGroup : public iStream
    {
        PI_DEFINE(iStreamGroup)
        
        PIMethod()
        virtual void Push(iStream* pStream) = 0;
        
        PIMethod()
        virtual void Unshift(iStream* pStream) = 0;
        
        PIMethod()
        virtual void Remove(iStream* pStream) = 0;
        
        PIMethod()
        virtual void Clear() = 0;
    };
    
    PIFunction()
    iStreamGroup* CreateStreamGroup();
    
    PIFunction()
    iStream* CreateStreamRegion(iStream* stream, int64_t start, int64_t size);
    
    //==========================================================================
    
    struct iReaderUTF8 : public iRefObject
    {
        virtual ~iReaderUTF8() {}
                
        virtual const char *GetLine() const = 0;
        
        virtual int64_t GetSize() const = 0;
        
        virtual bool ReadLine() = 0;
    };
    
    iReaderUTF8 *CreateReaderUTF8(iStream *data, int64_t bufferSize);
}

#endif
































