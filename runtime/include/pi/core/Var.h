/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2013.6.11   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_VAR_H
#define PI_CORE_VAR_H

namespace nspi
{
    struct iTable;
    struct iArray;
    
    PIEnum()
    enum eType
    {
        eType_Unknown       = -1,
        eType_Void          = 0,
        eType_Null          = 1,
        eType_Boolean       = 2,
        eType_I8            = 3,
        eType_U8            = 4,
        eType_I16           = 5,
        eType_U16           = 6,
        eType_I32           = 7,
        eType_U32           = 8,
        eType_I64           = 9,
        eType_U64           = 10,
        eType_F32           = 11,
        eType_F64           = 12,
        eType_Object        = 13,
        eType_String        = 14,
        eType_Vec2          = 15,
        eType_Vec3          = 16,
        eType_Vec4          = 17,
        eType_Mat4          = 18,
        eType_Enum          = 19,
        eType_Quat          = 20,
        eType_Var           = 21,
        eType_Rect          = 22,
        eType_I16Array      = 24,
        eType_U16Array      = 25,
        eType_I32Array      = 26,
        eType_U32Array      = 27,
        eType_I64Array      = 28,
        eType_U64Array      = 29,
        eType_F32Array      = 30,
        eType_F64Array      = 31,
        eType_I8Array       = 32,
        eType_U8Array       = 33,
        eType_StringArray   = 34,
        eType_ObjectArray   = 35,
        eType_Vec2Array     = 36,
        eType_Vec3Array     = 37,
        eType_Vec4Array     = 38,
        eType_QuatArray     = 39,
        eType_RectArray     = 40,
        eType_VarArray      = 42,
        eType_Mat4Array     = 43,
    };
    
    class Var
    {
    private:
        int mdType;
        union
        {
            bool b;
            int8_t i8;
            int16_t i16;
            int32_t i32;
            int64_t i64;
            uint8_t u8;
            uint16_t u16;
            uint32_t u32;
            uint64_t u64;
            float f32;
            double f64;
            std::string* pStr;
            vec2* pVec2;
            vec3* pVec3;
            vec4* pVec4;
            mat4* pMat4;
            quat* pQuat;
            rect* pRect;
            iRefObject* pObj;
            iI8Array* pI8Array;
            iU8Array* pU8Array;
            iI16Array* pI16Array;
            iU16Array* pU16Array;
            iI32Array* pI32Array;
            iU32Array* pU32Array;
            iI64Array* pI64Array;
            iU64Array* pU64Array;
            iF32Array* pF32Array;
            iF64Array* pF64Array;
            iVec2Array* pVec2Array;
            iVec3Array* pVec3Array;
            iVec4Array* pVec4Array;
            iQuatArray* pQuatArray;
            iRectArray* pRectArray;
            iMat4Array* pMat4Array;
        } mValue;
        
    public:
        Var();
        Var(const Var& a);
        virtual ~Var();
        
        Var(float v);
        Var(int64_t v);
        Var(int32_t v);
        Var(int16_t v);
        Var(int8_t v);
        Var(uint64_t v);
        Var(uint32_t v);
        Var(uint16_t v);
        Var(uint8_t v);
        Var(double v);
        Var(bool v);
        Var(const char* v);
        Var(const std::string &v);
        Var(const vec2 &v);
        Var(const vec3 &v);
        Var(const vec4 &v);
        Var(const mat4 &v);
        Var(const quat &v);
        Var(const rect &v);
        Var(iRefObject *v);
        Var(iI8Array* v);
        Var(iU8Array* v);
        Var(iI16Array* v);
        Var(iU16Array* v);
        Var(iI32Array* v);
        Var(iU32Array* v);
        Var(iI64Array* v);
        Var(iU64Array* v);
        Var(iF32Array* v);
        Var(iF64Array* v);
        Var(iVec2Array* v);
        Var(iVec3Array* v);
        Var(iVec4Array* v);
        Var(iRectArray* v);
        Var(iQuatArray* v);
        Var(iMat4Array* v);
        
        Var& operator=(const Var& a);
        int GetType() const;
        
        bool IsNull() const;
        void SetNull();
        
        bool GetBoolean() const;
        void SetBoolean(bool v);
        
        int8_t GetI8(int8_t dDefault = 0) const;
        void SetI8(int8_t v);
        
        int16_t GetI16(int16_t lldDefault = 0) const;
        void SetI16(int16_t v);
        
        int32_t GetI32(int32_t dDefault = 0) const;
        void SetI32(int32_t v);
        
        int64_t GetI64(int64_t lldDefault = 0) const;
        void SetI64(int64_t v);
        
        uint8_t GetU8(uint8_t dDefault = 0) const;
        void SetU8(uint8_t v);
        
        uint16_t GetU16(uint16_t lldDefault = 0) const;
        void SetU16(uint16_t v);
        
        uint32_t GetU32(uint32_t dDefault = 0) const;
        void SetU32(uint32_t v);
        
        uint64_t GetU64(uint64_t lldDefault = 0) const;
        void SetU64(uint64_t v);
        
        float GetF32(float fDefault = 0.0f) const;
        void SetF32(float v);
        
        double GetF64(double dbDefault = 0.0) const;
        void SetF64(double v);
        
        std::string GetString(const std::string &def = std::string()) const;
        void SetString(const std::string &v);
        
        vec2 GetVec2(const vec2 &def = vec2()) const;
        void SetVec2(const vec2 &v);
        
        vec3 GetVec3(const vec3 &def = vec3()) const;
        void SetVec3(const vec3 &v);
        
        vec4 GetVec4(const vec4 &def = vec4()) const;
        void SetVec4(const vec4 &v);
        
        mat4 GetMat4(const mat4 &def = mat4()) const;
        void SetMat4(const mat4 &v);
        
        quat GetQuat(const quat &def = quat()) const;
        void SetQuat(const quat &v);
        
        rect GetRect(const rect &def = rect()) const;
        void SetRect(const rect &v);
        
        iRefObject* GetObject() const;
        void SetObject(iRefObject* v);
        
        iI8Array* GetI8Array() const;
        void SetI8Array(iI8Array* v);
        
        iU8Array* GetU8Array() const;
        void SetU8Array(iU8Array* v);
        
        iI16Array* GetI16Array() const;
        void SetI16Array(iI16Array* v);
        
        iU16Array* GetU16Array() const;
        void SetU16Array(iU16Array* v);
        
        iI32Array* GetI32Array() const;
        void SetI32Array(iI32Array* v);
        
        iU32Array* GetU32Array() const;
        void SetU32Array(iU32Array* v);
        
        iI64Array* GetI64Array() const;
        void SetI64Array(iI64Array* v);
        
        iU64Array* GetU64Array() const;
        void SetU64Array(iU64Array* v);
        
        iF32Array* GetF32Array() const;
        void SetF32Array(iF32Array* v);
        
        iF64Array* GetF64Array() const;
        void SetF64Array(iF64Array* v);
        
        iRectArray* GetRectArray() const;
        void SetRectArray(iRectArray* v);
        
        iQuatArray* GetQuatArray() const;
        void SetQuatArray(iQuatArray* v);
        
        iMat4Array* GetMat4Array() const;
        void SetMat4Array();
        
        iVec2Array* GetVec2Array() const;
        void SetVec2Array(iVec2Array* v);
        
        iVec3Array* GetVec3Array() const;
        void SetVec3Array(iVec3Array* v);
        
        iVec4Array* GetVec4Array() const;
        void SetVec4Array(iVec4Array* v);
        
        operator bool() const;
        operator int8_t() const;
        operator int16_t() const;
        operator int32_t() const;
        operator int64_t() const;
        operator uint8_t() const;
        operator uint16_t() const;
        operator uint32_t() const;
        operator uint64_t() const;
        operator float() const;
        operator double() const;
        operator std::string() const;
        operator vec2() const;
        operator vec3() const;
        operator vec4() const;
        operator mat4() const;
        operator quat() const;
        operator rect() const;
        operator iRefObject*() const;
        operator iI8Array*() const;
        operator iI16Array*() const;
        operator iI32Array*() const;
        operator iI64Array*() const;
        operator iU8Array*() const;
        operator iU16Array*() const;
        operator iU32Array*() const;
        operator iU64Array*() const;
        operator iF32Array*() const;
        operator iF64Array*() const;
        operator iRectArray*() const;
        operator iMat4Array*() const;
        operator iQuatArray*() const;
        operator iVec2Array*() const;
        operator iVec3Array*() const;
        operator iVec4Array*() const;
        
    private:
        void Copy(const Var& a);
        void Init();
        void ReleaseMemory();
    };
 
    
    PI_DEFINE_STRUCT_ARRAY(Var, iVarArray);
    iVarArray* CreateVarArray();
    
    PIFunction()
    iI8Array* VarArrayToI8Array(iVarArray* array);
    
    PIFunction()
    iU8Array* VarArrayToU8Array(iVarArray* array);
    
    PIFunction()
    iI16Array* VarArrayToI16Array(iVarArray* array);
    
    PIFunction()
    iU16Array* VarArrayToU16Array(iVarArray* array);
    
    PIFunction()
    iI32Array* VarArrayToI32Array(iVarArray* array);
    
    PIFunction()
    iU32Array* VarArrayToU32Array(iVarArray* array);
    
    PIFunction()
    iI64Array* VarArrayToI64Array(iVarArray* array);
    
    PIFunction()
    iU64Array* VarArrayToU64Array(iVarArray* array);
    
    PIFunction()
    iF32Array* VarArrayToF32Array(iVarArray* array);
    
    PIFunction()
    iF64Array* VarArrayToF64Array(iVarArray* array);
    
    
    template<class T>
    static inline T* piQueryVarObject(const Var& var)
    {
        return dynamic_cast<T*>(var.GetObject());
    }
    
    std::string piTypeName(int type);
    
}

#endif























