/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin      2012.9.2    1.0     Added errno
 luo weibin      2012.7.13   0.1     Create
 ******************************************************************************/

#ifndef PI_CORE_CORETYPES_H
#define PI_CORE_CORETYPES_H

//////////////////////////////////////////////////////////////////
// Common headers

#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <stddef.h>
#include <limits.h>
#include <wchar.h>
#include <stdint.h>
#include <new>
#include <vector>
#include <map>
#include <list>
#include <algorithm>
#include <atomic>
#include <functional>

#if defined _UNICODE || defined UNICODE
#   define PI_UNICODE
#else
#	define PI_ASCII
#endif

#if defined(_DEBUG) || (defined(DEBUG) && DEBUG)
#   define PI_DEBUG    1
#   define PI_RELEASE  0
#else
#   define PI_DEBUG    0
#   define PI_RELEASE  1
#endif

///////////////////////////////////////////////////////////////////
// macros

#define piUnused(x) \
do { (void)(x); } while(0)

#define piArrayCount(array)                 (sizeof(array) / sizeof(array[0]))
#define piArrayIndex(array_ptr, elem_ptr)   (((elem_ptr) - (array_ptr)) / sizeof(*elem_ptr))

// bits operations
#define piFlagOn(var, x)  do { *((uint32_t*)&(var)) |= (uint32_t)(x); } while(0)
#define piFlagOff(var, x) do { *((uint32_t*)&(var)) &= ~((uint32_t)(x)); } while(0)
#define piFlagGet(var, x) (((uint32_t)(var))&((uint32_t)(x)))
#define piFlagIs(var, x) (((uint32_t)(x))&&(piFlagGet(var, x)==((uint32_t)(x))))
#define piFlagIsNot(var, x) (!piFlagIs((var),(x)))

#define piFlag64On(var, x) do { *((uint64_t*)&(var)) |= ((uint64_t)(x)); } while(0)
#define piFlag64Off(var, x) do { *((uint64_t)(var)) &= ~((uint64_t)(x)); } while(0)
#define piFlag64Is(var, x) (((uint64_t)(x))&&((((uint64_t)(var))&((uint64_t)(x)))==((uint64_t)(x))))
#define piFlag64IsNot(var, x) (!piFlag64Is((var),(x)))

#define piBit(x)   ((uint32_t)(1<<(x)))
#define piBit64(x)   ((uint64_t)(((uint64_t)1)<<((uint64_t)(x))))

#define piMax(a, b) (((a)>(b))?(a):(b))
#define piMin(a, b) (((a)<(b))?(a):(b))

//==============================================================================
// 内存对齐

//以N对齐
#define piAlignPow2(v, n) \
    (((v) + ((1 << (n)) - 1)) & (~((1 << (n)) - 1)))

static inline bool piIsPow2(uint64_t x) {
    return ((x != 0) && !(x & (x - 1)));
}

//==============================================================================
// time
#define piSec2Ms(sec)  ((sec)*1000)
#define piSec2Us(sec)  ((sec)*1000000)
#define piSec2Ns(sec)  ((sec)*1000000000)

#define piUs2Sec(us)   ((us)/1000000)
#define piUs2Ms(us)    ((us)/1000)
#define piUs2Ns(us)    ((us)*1000)

#define piMs2Sec(ms)   ((ms)/1000)
#define piMs2Us(ms)    ((ms)*1000)
#define piMs2Ns(ms)    ((ms)*1000000)

#define piNs2Us(ns)    ((ns)/1000)
#define piNs2Ms(ns)    ((ns)/1000000)
#define piNs2Sec(ns)   ((ns)/1000000000)

/////////////////////////////////////////////////////////////////////////

#define piFourCC(a, b, c, d) \
((((uint32_t)(d) << 24) & 0xFF000000U) | (((uint32_t)(c) << 16) & 0x00FF0000U) | (((uint32_t)(b) << 8) & 0x0000FF00U) | ((uint32_t)(a) & 0x000000FFU))
#define piFourA(cc) \
((((uint32_t)0x000000FF << 24) & (cc)) >> 24)
#define piFourB(cc) \
((((uint32_t)0x000000FF << 16) & (cc)) >> 16)
#define piFourC(cc) \
((((uint32_t)0x000000FF << 8) & (cc)) >> 8)
#define piFourD(cc) \
(((uint32_t)0x000000FF) & (cc))

//=============================================================================

#if PI_DEBUG
#   define piAssert(cond,ret) assert(cond)
#elif defined(PI_ANDROID)
#   define piAssert(cond,ret) \
    do { \
        if (!(cond)) { \
            __android_log_print(ANDROID_LOG_WARN, "piAssert", "piAssert failed:%s, %s(%d)\n", #cond, __FILE__, __LINE__); \
            return ret; \
        } \
    } while (0)
#else
#   define piAssert(cond, ret) \
    do { \
        if (!(cond)) { \
            fprintf(stderr, "piAssert failed:%s, %s(%d)\n", #cond, __FILE__, __LINE__); \
            return ret; \
        } \
    } while (0)
#endif

#if defined(PI_ANDROID)
#   define piAssertDontStop(cond) \
    do { \
        if (!(cond)) { \
            __android_log_print(ANDROID_LOG_ERROR, "piAssert", "piAssert failed:%s, %s(%d)\n", #cond, __FILE__, __LINE__); \
        } \
    } while (0)
#else
#   define piAssertDontStop(cond) \
    do { \
        if (!(cond)) { \
            fprintf(stderr, "piAssert failed:%s, %s(%d)\n", #cond, __FILE__, __LINE__); \
        } \
    } while (0)
#endif

#define piCheck(cond, ret) \
do { \
    if (!(cond)) { \
        return ret; \
    } \
} while (0)

//====================================================================================

#if PI_REFLECTION_ENABLED

#   define PI_DEFINE(name) \
        virtual ~ name() {} \
        static nspi::iClass* StaticClass(); \
        virtual nspi::iClass* GetClass() const; \
        virtual std::string ToString() const;

#else

#   define PI_DEFINE(name) \
        virtual ~ name() {} \
        static nspi::iClass* StaticClass() {return nullptr;} \
        virtual nspi::iClass* GetClass() const {return nullptr;} ; \
        virtual std::string ToString() const { return ""; }

#endif

/**
 *
 */
#define PIInterface(...)
#define PIMethod(...)
#define PISetter(...)
#define PIGetter(...)
#define PIFunction(...)
#define PIEnum(...)

#include <pi/core/Platform.h>
#include <pi/core/CoreString.h>
#include <pi/core/RefObject.h>
#include <pi/core/CoreMath.h>
#include <pi/core/CoreArray.h>
#include <pi/core/Var.h>
#include <pi/core/Reflection.h>
#include <pi/core/CoreStream.h>

#if defined(PI_GCC) || defined(PI_LLVM)
/*
 ** The Count of all Bit = 1
 */
#   define piBitsCount(v)       __builtin_popcount(v)
#   define piBits64Count(v)    __builtin_popcountl(v)
#   define piBitsMax(n)         (1U << (32 - __builtin_clz(n)))
#   define piBits64Max(n)       (1ULL << (64 - __builtin_clzll(n)))
#   define piBitsMin(v)        (__builtin_ffs(v) > 0 ? (1U << (__builtin_ffs(v) - 1)) : 0)
#   define piBits64Min(v)        (__builtin_ffsl(v) > 0 ? (1ULL << (__builtin_ffsl(v) - 1)) : 0)
#endif

static inline uint16_t piSwap16(uint16_t v) {
    union {
        char b[2];
        uint16_t I16;
    } t1;
    t1.I16 = v;

    union {
        char b[2];
        uint16_t I16;
    } t2;
    t2.b[0] = t1.b[1];
    t2.b[1] = t1.b[0];
    return t2.I16;
}

#if defined(PI_GCC) || defined(PI_LLVM)

static inline uint32_t piSwap32(uint32_t v) {
    return __builtin_bswap32(v);
}

static inline uint64_t piSwap64(uint64_t v) {
    return __builtin_bswap64(v);
}

#else

#error "endian swapping not defined!"

#endif

//==============================================================================
// errno

/*  A number random enough not to collide with different errno ranges on      */
/*  different OSes. The assumption is that error_t is at least 32-bit type.   */
#define ELIBWIN        200000000U

#if defined (PI_WINDOWS)

#   if _MSC_VER < 1600
#       define ENODATA		(ELIBWIN + 1)
#       define ENOTCONN     (ELIBWIN + 2)
#		define ECONNRESET	(ELIBWIN + 3)
#		define ENOBUFS		(ELIBWIN + 4)
#		define EADDRINUSE	(ELIBWIN + 5)
#		define EALREADY		(ELIBWIN + 6)
#		define ECONNREFUSED	(ELIBWIN + 7)
#		define EINPROGRESS	(ELIBWIN + 8)
#		define ENETUNREACH	(ELIBWIN + 9)
#		define ETIMEDOUT	(ELIBWIN + 10)
#		define ENOPROTO		(ELIBWIN + 11)
#		define EWOULDBLOCK	EAGAIN
#   endif
#   define EDEVERR          (ELIBWIN + 12)
#endif

#define ELIB        (ELIBWIN + 1000) //internal library error, bug need to be reported.
#define EUNKNOWN    (ELIB + 1)
#define ENOIMPL     (ELIB + 2) // no implementation
#define ENOSUPPORT  (ELIB + 3) // not supported
#define EORDER      (ELIB + 4) // operation called in wrong order
#define EBADDATA    (ELIB + 5) // bad data

#define EUSER       300000000U

////////////////////////////////////////////////////////////////////////////////

#if defined (PI_APPLE)
#   include <sys/errno.h>
#   include <unistd.h>
#endif

#if defined (PI_LINUX) || defined(PI_WINDOWS)

#   include <errno.h>

#endif

namespace nspi {
    void piInit();

    void piDeinit();

#if defined(PI_ANDROID)

    std::string piJavaStringToString(JNIEnv *pEnv, jstring javaString);

#endif

    //==========================================================================
    bool piIsStringUTF8Empty(const char *pszValue);

#define piIsStringEmpty piIsStringUTF8Empty
}


#endif




















