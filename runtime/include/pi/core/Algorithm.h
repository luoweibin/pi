/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin      2015.1.12   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_ALGORITHM_H
#define PI_CORE_ALGORITHM_H

#include <tuple>

namespace nspi
{
    
    /**
     * -----------------------------------------------------------------------------------------
     * Search
     */
    
    template <class ArrayType, class ItemType>
    static int32_t piIndexOf(ArrayType* array, ItemType v)
    {
        for (auto i = 0; i < array->GetCount(); ++i)
        {
            ItemType item = array->GetItem(i);
            if (item == v)
            {
                return i;
            }
        }
        
        return -1;
    }
    
    
    template <class ArrayType, class ItemType>
    static int32_t piIndexOf(ArrayType* array, std::function<bool(ItemType)> func)
    {
        for (auto i = 0; i < array->GetCount(); ++i)
        {
            ItemType item = array->GetItem(i);
            if (func(item))
            {
                return i;
            }
        }
        
        return -1;
    }
    
    
    template <class ArrayType, class ItemType>
    static int32_t piLastIndexOf(ArrayType* array, ItemType v)
    {
        auto count = array->GetCount();
        for (auto i = count - 1; i >= 0; --i)
        {
            ItemType item = array->GetItem(i);
            if (item == v)
            {
                return i;
            }
        }
        
        return -1;
    }
    
    
    template <class ArrayType, class ItemType>
    static int32_t piLastIndexOf(ArrayType* array, std::function<bool(ItemType)> func)
    {
        for (auto i = 0; i < array->GetCount(); ++i)
        {
            ItemType item = array->GetItem(i);
            if (func(item))
            {
                return i;
            }
        }
        
        return -1;
    }
    
    
    /**
     * -----------------------------------------------------------------------------------------
     * RemoveOne
     */
    
    template <class ArrayType, class ItemType>
    static int32_t piRemoveOne(ArrayType* array, ItemType v)
    {
        for (auto i = 0; i < array->GetCount(); ++i)
        {
            ItemType item = array->GetItem(i);
            if (item == v)
            {
                array->Remove(i);
                return i;
            }
        }
        
        return -1;
    }
    
    template <class ArrayType, class ItemType>
    static int32_t piRemoveOne(ArrayType* array, std::function<bool(ItemType)> func)
    {
        for (auto i = 0; i < array->GetCount(); ++i)
        {
            ItemType item = array->GetItem(i);
            if (func(item))
            {
                array->Remove(i);
                return i;
            }
        }
        
        return -1;
    }
    
    
    /**
     * -----------------------------------------------------------------------------------------
     * RemoveAll
     */
    
    template <class ArrayType, class ItemType>
    static bool piRemoveAll(ArrayType* array, ItemType v)
    {
        bool ret = false;
        
        for (auto i = 0; i < array->GetCount();)
        {
            auto index = array->GetCount() - 1 - i;
            ItemType item = array->GetItem(index);
            if (item == v)
            {
                array->Remove(index);
                ret = true;
            }
            else
            {
                ++i;
            }
        }
        
        return ret;
    }
    
    template <class ArrayType, class ItemType>
    static bool piRemoveAll(ArrayType* array, std::function<bool(ItemType)> func)
    {
        bool ret = false;
        
        for (auto i = 0; i < array->GetCount();)
        {
            auto index = array->GetCount() - 1 - i;
            ItemType item = array->GetItem(index);
            if (func(item))
            {
                array->Remove(index);
                ret = true;
            }
            else
            {
                ++i;
            }
        }
        
        return ret;
    }
 
    
    /**
     * -----------------------------------------------------------------------------------------
     * Each
     */
    
    template <class ArrayType, class ItemType>
    static void piEach(ArrayType* array, std::function<void(ItemType)> func)
    {
        for (auto i = 0; i < array->GetCount(); ++i)
        {
            ItemType item = array->GetItem(i);
            func(item);
        }
    }

}

#endif

























