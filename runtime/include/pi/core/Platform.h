/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2012.7.13   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_PLATFORM_H
#define PI_CORE_PLATFORM_H

//==============================================================================
// APPLE

#if defined(__APPLE__)
#include <TargetConditionals.h>

#define PI_APPLE
#define PI_POSIX
#define PI_UNIX
#define PI_PATHDEL          ((char_t)'/')
#define PI_PATHMAX          PATH_MAX
#define PI_CHARSET          eCharset_UTF8
#define PI_LITTLE_ENDIAN    1
#define PI_BIG_ENDIAN       0

#if defined (__llvm__)
#	define PI_LLVM
#elif defined (__GNUC__)
#	define PI_GCC
#endif

#define TT(text)    (text)
#define PI_MBS

#if defined(__OBJC__)
#   define PI_OBJC
#endif

#define char_t char // for file path string, etc.

//------------------------------------------------------------------------------
// iOS
#   if defined(__IPHONE_OS_VERSION_MIN_REQUIRED)
#       define PI_IOS
#       define PI_OPENGL_ES

#   if defined(TARGET_IPHONE_SIMULATOR)
#       define PI_SIMULATOR  
#   endif

#   endif

//------------------------------------------------------------------------------
// Mac OSX
#   if defined(__MAC_OS_X_VERSION_MIN_REQUIRED)
#       define PI_MACOS
#       define PI_OPENGL
#   endif

#endif

//=============================================================================
// MSVC
#if defined(_MSC_VER)

#define PI_MSVC
#define PI_WINDOWS
#define PI_PATHDEL   '\\'
#define PI_PATHMAX   MAX_PATH
#define PI_CHARSET   eCharset_UTF16
#define PI_LITTLE_ENDIAN    1
#define PI_BIG_ENDIAN       0

#define TT _T
#if defined(PI_UNICODE)
#   define PI_WS
#else
#   define PI_MBS
#endif

#include <Winsock2.h>
#include <windows.h>
#include <TCHAR.h>

#pragma comment(lib, "Ws2_32.lib")

#define snprintf(buffer, size, format, ...) _snprintf_s((buffer), (size), _TRUNCATE, format, __VA_ARGS__)
#define strcasecmp _stricmp
#define strtoll _strtoi64

typedef int sint32_t;

#endif // _MSC_VER

//=============================================================================
// Android

#if defined (ANDROID)

#include <jni.h>
#include <android/log.h>
#include <features.h>

#define PI_ANDROID
#define PI_LINUX
#define PI_UNIX
#define PI_POSIX
#define PI_GCC
#define PI_PATHDEL '/'
#define PI_PATHMAX PATH_MAX
#define PI_CHARSET eCharset_UTF8
#define PI_LITTLE_ENDIAN    1
#define PI_BIG_ENDIAN       0
#define PI_OPENGL_ES

#define TT(text) (text)
#define PI_MBS

namespace nspi
{
    struct iRefObject;
    
#define char_t char

    bool piInitJNI(JavaVM* pJVM);
    void piDeinitJNI(JavaVM* pJVM);
    
    JNIEnv* piAttachJVM();
    void piDetachJVM();
    
    iRefObject* piGetNativePtr(JNIEnv* pEnv, jobject object);
    jobject CreateRefObject(JNIEnv* pEnv, jclass klass, iRefObject* pObject);
    jobject CreateRefObjectEx(JNIEnv* pEnv, const char* pszClassName, iRefObject* pObject);
    bool piIsRefObject(JNIEnv* pEnv, jobject object);
    jclass piFindClass(JNIEnv* pEnv, const char* pszClassName);
    
    template <class T>
    static inline T* piGetNativeObject(JNIEnv* pEnv, jobject object)
    {
        return dynamic_cast<T*>(piGetNativePtr(pEnv, object));
    }
}

#endif // ANDROID

#endif






