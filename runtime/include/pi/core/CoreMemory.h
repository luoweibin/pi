/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2013.6.11   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_COREMEMORY_H
#define PI_CORE_COREMEMORY_H

namespace nspi
{

    PIInterface(HasCreator = false)
    struct iMemory : public iRefObject
    {
        PI_DEFINE(iMemory)
        
        virtual char* Ptr() const = 0;
       
        PIMethod()
        virtual int64_t Size() const = 0;
       
        virtual bool Resize(int64_t size) = 0;
    };
    
    iMemory* CreateMemory(int64_t size);
    iMemory* CreateMemoryStatic(const void* mem, int64_t size);
    iMemory* CreateMemoryCopy(const void* mem, int64_t size);
    
    
    PIInterface(HasCreator = false)
    struct iMemoryStream : public iStream
    {
        PI_DEFINE(iMemoryStream)
        
        PIMethod()
        virtual iMemory* GetMemory() const = 0;
    };
    
    PIFunction()
    iMemoryStream* CreateMemoryStream(int64_t size);
    
    PIFunction()
    iMemoryStream* CreateMemoryStreamEx(iMemory* mem, int64_t start, int64_t size);
    
    iStream* CreateMemoryStreamCopy(const void* data, int64_t size);
    
    iStream* CreateMemoryStreamStatic(const void* data, int64_t size);
}

#endif









