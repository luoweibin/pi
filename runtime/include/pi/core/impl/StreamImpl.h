/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2013.6.10   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_IMPL_STREAMIMPL_H
#define PI_CORE_IMPL_STREAMIMPL_H

namespace nspi {
    
    template<class I>
    class cStreamImpl : public I
    {
    protected:
        int64_t mlldOffset;
        std::string mUri;
        
    public:
        cStreamImpl()
        {
            mlldOffset = 0;
        }
        
        virtual ~cStreamImpl()
        {
            Flush();
        }
        
        virtual void SetUri(const std::string& uri)
        {
            mUri = uri;
        }
        
        virtual int64_t Seek(int64_t lldOffset, int dSeek)
        {
            switch (dSeek)
            {
                case eSeek_Set:
                    mlldOffset = piMin(lldOffset, GetSize());
                    return mlldOffset;
                case eSeek_Current:
                    mlldOffset = piMin(lldOffset + mlldOffset, GetSize());
                    return mlldOffset;
                case eSeek_End:
                    mlldOffset = piMin(GetSize() + lldOffset, GetSize());
                    return mlldOffset;
                default:
                    return -1;
            }
        }
        
        virtual int64_t GetOffset() const
        {
            return mlldOffset;
        }
        
        virtual bool End() const
        {
            return mlldOffset == GetSize();
        }
        
        virtual int8_t ReadI8(int8_t def)
        {
            int8_t v = 0;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteI8(int8_t v)
        {
            return Write(&v, sizeof(v));
        }
        
        virtual uint8_t ReadU8(uint8_t def)
        {
            uint8_t v = 0;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteU8(uint8_t v)
        {
            return Write(&v, sizeof(v));
        }
        
        virtual int16_t ReadI16(int16_t def)
        {
            int16_t v = 0;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteI16(int16_t v)
        {
            return Write(&v, sizeof(v));
        }
        
        virtual int16_t ReadI16LE(int16_t def)
        {
            int16_t v = 0;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
#if PI_BIG_ENDIAN
                v = piSwap16(v);
#endif
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteI16LE(int16_t v)
        {
#if PI_BIG_ENDIAN
            v = piSwap16(v);
#endif
            return Write(&v, sizeof(v));
        }
        
        virtual int16_t ReadI16BE(int16_t def)
        {
            int16_t v = 0;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
#if PI_LITTLE_ENDIAN
                v = piSwap16(v);
#endif
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteI16BE(int16_t v)
        {
#if PI_LITTLE_ENDIAN
            v = piSwap16(v);
#endif
            return Write(&v, sizeof(v));
        }
        
        virtual uint16_t ReadU16(uint16_t def)
        {
            uint16_t v = 0;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteU16(uint16_t v)
        {
            return Write(&v, sizeof(v));
        }
        
        virtual uint16_t ReadU16LE(uint16_t def)
        {
            uint16_t v = 0;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
#if PI_BIG_ENDIAN
                v = piSwap16(v);
#endif
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteU16LE(uint16_t v)
        {
#if PI_BIG_ENDIAN
            v = piSwap16(v);
#endif
            return Write(&v, sizeof(v));
        }
        
        virtual uint16_t ReadU16BE(uint16_t def)
        {
            uint16_t v = 0;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
#if PI_LITTLE_ENDIAN
                v = piSwap16(v);
#endif
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteU16BE(uint16_t v)
        {
#if PI_LITTLE_ENDIAN
            v = piSwap16(v);
#endif
            return Write(&v, sizeof(v));
        }
        
        virtual int32_t ReadI32(int32_t def)
        {
            int32_t v = 0;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteI32(int32_t v)
        {
            return Write(&v, sizeof(v));
        }
        
        virtual int32_t ReadI32LE(int32_t def)
        {
            int32_t v = 0;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
#if PI_BIG_ENDIAN
                v = piSwap32(v);
#endif
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteI32LE(int32_t v)
        {
#if PI_BIG_ENDIAN
            v = piSwap32(v);
#endif
            return Write(&v, sizeof(v));
        }
        
        virtual int32_t ReadI32BE(int32_t def)
        {
            int32_t v = 0;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
#if PI_LITTLE_ENDIAN
                v = piSwap32(v);
#endif
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteI32BE(int32_t v)
        {
#if PI_LITTLE_ENDIAN
            v = piSwap32(v);
#endif
            return Write(&v, sizeof(v));
        }
        
        virtual uint32_t ReadU32(uint32_t def)
        {
            uint32_t v = 0;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteU32(uint32_t v)
        {
            return Write(&v, sizeof(v));
        }
        
        
        virtual uint32_t ReadU32LE(uint32_t def)
        {
            uint32_t v = 0;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
#if PI_BIG_ENDIAN
                v = piSwap32(v);
#endif
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteU32LE(uint32_t v)
        {
#if PI_BIG_ENDIAN
            v = piSwap32(v);
#endif
            return Write(&v, sizeof(v));
        }
        
        virtual uint32_t ReadU32BE(uint32_t def)
        {
            uint32_t v = 0;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
#if PI_LITTLE_ENDIAN
                v = piSwap32(v);
#endif
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteU32BE(uint32_t v)
        {
#if PI_LITTLE_ENDIAN
            v = piSwap32(v);
#endif
            return Write(&v, sizeof(v));
        }
        
        virtual int64_t ReadI64(int64_t def)
        {
            int64_t v = 0;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteI64(int64_t v)
        {
            return Write(&v, sizeof(v));
        }

        
        virtual int64_t ReadI64LE(int64_t def)
        {
            int64_t v = 0;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
#if PI_BIG_ENDIAN
                v = piSwap64(v);
#endif
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteI64LE(int64_t v)
        {
#if PI_BIG_ENDIAN
            v = piSwap64(v);
#endif
            return Write(&v, sizeof(v));
        }
        
        virtual int64_t ReadI64BE(int64_t def)
        {
            int64_t v = 0;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
#if PI_LITTEL_ENDIAN
                v = piSwap64(v);
#endif
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteI64BE(int64_t v)
        {
#if PI_LITTLE_ENDIAN
            v = piSwap64(v);
#endif
            return Write(&v, sizeof(v));
        }
        
        virtual uint64_t ReadU64(uint64_t def)
        {
            uint64_t v = 0;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteU64(uint64_t v)
        {
            return Write(&v, sizeof(v));
        }
        
        virtual uint64_t ReadU64LE(uint64_t def)
        {
            uint64_t v = 0;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
#if PI_BIG_ENDIAN
                v = piSwap64(v);
#endif
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteU64LE(uint64_t v)
        {
#if PI_BIG_ENDIAN
            v = piSwap64(v);
#endif
            return Write(&v, sizeof(v));
        }
        
        virtual uint64_t ReadU64BE(uint64_t def)
        {
            uint64_t v = 0;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
#if PI_LITTLE_ENDIAN
                v = piSwap64(v);
#endif
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteU64BE(uint64_t v)
        {
#if PI_LITTLE_ENDIAN
            v = piSwap64(v);
#endif
            return Write(&v, sizeof(v));
        }
        
        virtual float ReadF32(float def)
        {
            float f32;
            
            if (Read(&f32, sizeof(float)) == sizeof(float))
            {
                return f32;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteF32(float v)
        {
            return Write(&v, sizeof(v));
        }
        
        virtual double ReadF64(double def)
        {
            double f64;
            
            if (Read(&f64, sizeof(double)) == sizeof(double))
            {
                return f64;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteF64(double v)
        {
            return Write(&v, sizeof(v));
        }
        
        virtual int64_t WriteMat4(const mat4& v)
        {
            return Write(&v, sizeof(v));
        }
        
        virtual mat4 ReadMat4(const mat4& def = mat4())
        {
            mat4 v;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteVec2(const vec2& v)
        {
            return Write(&v, sizeof(v));
        }
        
        virtual vec2 ReadVec2(const vec2& def = vec2())
        {
            vec2 v;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteVec3(const vec3& v)
        {
            return Write(&v, sizeof(v));
        }
        
        virtual vec3 ReadVec3(const vec3& def = vec3())
        {
            vec3 v;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteVec4(const vec4& v)
        {
            return Write(&v, sizeof(v));
        }
        
        virtual vec4 ReadVec4(const vec4& def = vec4())
        {
            vec4 v;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual int64_t WriteQuat(const quat& v)
        {
            return Write(&v, sizeof(v));
        }
        
        virtual quat ReadQuat(const quat& def = quat())
        {
            quat v;
            if (Read(&v, sizeof(v)) == sizeof(v))
            {
                return v;
            }
            else
            {
                return def;
            }
        }
        
        virtual bool Flush()
        {
            return true;
        }
        
        virtual int64_t WriteString(const std::string &v)
        {
            if (!v.empty())
            {
                return Write(v.c_str(), v.size());
            }
            else
            {
                return 0;
            }
        }
        
        virtual int64_t GetSize() const = 0;
        virtual int64_t Read(void* pBuffer, int64_t size) = 0;
        virtual int64_t Write(const void* pData, int64_t size) = 0;
    };
    
}

#endif































