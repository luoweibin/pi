/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.10.11   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_IMPL_ARRAYIMPL_H
#define PI_CORE_IMPL_ARRAYIMPL_H

namespace nspi
{
    template <typename T, class I>
    class PrimitiveArrayImpl : public I
    {
    protected:
        std::vector<T> mValues;
        
    public:
        PrimitiveArrayImpl()
        {
        }
        
        PrimitiveArrayImpl(const T* values, int32_t count)
        :mValues(values, values + count)
        {
        }
        
        virtual ~PrimitiveArrayImpl()
        {
            
        }
        
        virtual bool IsEmpty() const
        {
            return mValues.empty();
        }
        
        virtual T GetBack() const
        {
            return mValues.back();
        }
        
        virtual T GetFront() const
        {
            return mValues.front();
        }
        
        virtual std::vector<T>& GetValues()
        {
            return mValues;
        }
        
        virtual void Insert( int32_t pos, T o)
        {
            mValues.insert( mValues.begin() + pos, o);
        }
        
        virtual void PushBack(T o)
        {
            mValues.push_back(o);
        }
        
        virtual void PopBack()
        {
            mValues.pop_back();
        }
        
        virtual int32_t GetCount() const
        {
            return (int32_t)mValues.size();
        }
        
        virtual T GetItem(int32_t index) const
        {
            piAssert(index >= 0 && index < mValues.size(), T());
            return mValues[index];
        }
        
        virtual void SetItem(int32_t index, T value)
        {
            piAssert(index >= 0 && index < mValues.size(), ;);
            mValues[index] = value;
        }
        
        virtual void Resize(int32_t count)
        {
            piAssert(count > 0, ;);
            mValues.resize(count);
        }
        
        virtual void Clear()
        {
            mValues.clear();
        }
        
        virtual T* GetData()
        {
            return mValues.data();
        }
        
        virtual void Remove(int32_t i)
        {
            piAssert(i >= 0 && i < mValues.size(), ;);
            mValues.erase(mValues.begin() + i);
        }
        
        virtual iMemory* ToMemory() const
        {
            return CreateMemoryCopy(mValues.data(), mValues.size() * sizeof(T));
        }
    };
    
    
    template <typename T, class I, class C>
    I* CreatePrimitiveArray(const T* values, int32_t count)
    {
        piAssert(values != nullptr, nullptr);
        piAssert(count > 0, nullptr);
        return new C(values, count);
    }
    
    
    template <typename T, class I>
    class StructArrayImpl : public I
    {
    protected:
        std::vector<T> mValues;
        
    public:
        StructArrayImpl()
        {
        }
        
        StructArrayImpl(const T* values, int32_t count)
        :mValues(values, values + count)
        {
        }
        
        virtual ~StructArrayImpl()
        {
        }
        
        virtual bool IsEmpty() const
        {
            return mValues.empty();
        }
        
        virtual T GetBack() const
        {
            return mValues.back();
        }
        
        virtual T GetFront() const
        {
            return mValues.front();
        }
        
        virtual std::vector<T>& GetValues()
        {
            return mValues;
        }
        
        virtual void Insert( int32_t pos,const T& o)
        {
            mValues.insert( mValues.begin() + pos, o);
        }
        
        virtual void PushBack(const T& o)
        {
            mValues.push_back(o);
        }
        
        virtual void PopBack()
        {
            mValues.pop_back();
        }
        
        
        virtual int32_t GetCount() const
        {
            return (int32_t)mValues.size();
        }
        
        virtual T GetItem(int32_t index) const
        {
            piAssert(index >= 0 && index < mValues.size(), T());
            return mValues[index];
        }
        
        virtual void SetItem(int32_t index, const T& value)
        {
            piAssert(index >= 0 && index < mValues.size(), ;);
            mValues[index] = value;
        }
        
        virtual void Resize(int32_t count)
        {
            piAssert(count > 0, ;);
            mValues.resize(count);
        }
        
        virtual void Clear()
        {
            mValues.clear();
        }
        
        virtual T* GetData()
        {
            return mValues.data();
        }
        
        virtual void Remove(int32_t i)
        {
            piAssert(i >= 0 && i < mValues.size(), ;);
            mValues.erase(mValues.begin() + i);
        }
    };
    
    template <typename T, class I, class C>
    I* CreateStructArray(const T* values, int32_t count)
    {
        piAssert(values != nullptr, nullptr);
        piAssert(count > 0, nullptr);
        return new C(values, count);
    }
    
    
    template <typename T, class I>
    class ObjectArrayImpl : public I
    {
    protected:
        std::vector<SmartPtr<T>> mValues;
        
    public:
        ObjectArrayImpl()
        {
        }
        
        virtual ~ObjectArrayImpl()
        {
        }
        
        virtual bool IsEmpty() const
        {
            return mValues.empty();
        }
        
        virtual T* GetBack() const
        {
            return mValues.back();
        }
        
        virtual T* GetFront() const
        {
            return mValues.front();
        }
        
        virtual std::vector<SmartPtr<T>>& GetValues()
        {
            return mValues;
        }
        
        virtual void Insert(int32_t pos, T* o)
        {
            mValues.insert(mValues.begin() + pos, o);
        }
        
        virtual void PushBack(T* o)
        {
            mValues.push_back(o);
        }
        
        virtual void PopBack()
        {
            mValues.pop_back();
        }
        
        virtual int32_t GetCount() const
        {
            return (int32_t)mValues.size();
        }
        
        virtual T* GetItem(int32_t index) const
        {
            piAssert(index >= 0 && index < mValues.size(), nullptr);
            return mValues[index];
        }
        
        virtual void SetItem(int32_t index, T* value)
        {
            piAssert(index >= 0 && index < mValues.size(), ;);
            mValues[index] = value;
        }
        
        virtual void Resize(int32_t count)
        {
            piAssert(count > 0, ;);
            mValues.resize(count);
        }
        
        virtual void Clear()
        {
            mValues.clear();
        }
        
        virtual void Remove(int32_t index)
        {
            piAssert(index >= 0 && index < mValues.size(), ;);
            mValues.erase(mValues.begin() + index);
        }
    };
    
}

#endif




















