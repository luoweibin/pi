/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.11.21   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_IMPL_THREADLOCALIMPL_H
#define PI_CORE_IMPL_THREADLOCALIMPL_H

#include <thread>
#include <mutex>

namespace nspi
{
    template <typename T>
    class ThreadLocal
    {
    private:
        typedef std::map<std::thread::id, T> TypeMap;
        
        TypeMap mValues;
        mutable std::mutex mMutex;
        
    public:
        ThreadLocal()
        {
        }
        
        bool Has() const
        {
            std::lock_guard<std::mutex> lock(mMutex);
            return mValues.find(std::this_thread::get_id()) != mValues.end();
        }
        
        void Set(const T& value)
        {
            auto threadId = std::this_thread::get_id();
            
            std::lock_guard<std::mutex> lock(mMutex);
            mValues[threadId] = value;
        }
        
        T Get() const
        {
            auto threadId = std::this_thread::get_id();
            
            std::lock_guard<std::mutex> lock(mMutex);
            auto it = mValues.find(threadId);
            return it != mValues.end() ? it->second : T();
        }
    };
}

#endif


















