/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.5   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_IMPL_REFLECTIONIMPL_H
#define PI_CORE_IMPL_REFLECTIONIMPL_H

#include <pi/Core.h>

namespace nspi
{
    
    template <typename T>
    struct VarExtractor
    {
        static T Extract(const Var& value)
        {
            return value;
        }
    };
    
    template <typename T>
    struct VarExtractor<T&>
    {
        static T Extract(const Var& value)
        {
            return value;
        }
    };
    
    template <typename T>
    struct VarExtractor<const T&>
    {
        static T Extract(const Var& value)
        {
            return value;
        }
    };
    
    template <typename T>
    struct VarExtractor<T*>
    {
        static T* Extract(const Var& value)
        {
            return piQueryVarObject<T>(value);
        }
    };
    
    class RefObjectClassImpl : public iClass
    {
    protected:
        SmartPtr<iMethodArray> mMethods;
        typedef std::map<std::string, iMethod*> MethodMap;
        MethodMap mMethodMap;
        
        SmartPtr<iPropertyArray> mProps;
        typedef std::map<std::string, iProperty*> PropMap;
        PropMap mPropMap;
        
        std::string mSimpleName;
        std::string mFullName;
        
        iClassLoader* mClassLoader;
        
    public:
        RefObjectClassImpl(const std::string& simpleName,
                           const std::string& fullName,
                           iClassLoader* classLoader):
        mSimpleName(simpleName), mFullName(fullName), mClassLoader(classLoader)
        {
            mMethods = CreateMethodArray();
            mProps = CreatePropertyArray();
        }
        
        virtual ~RefObjectClassImpl()
        {
        }
        
        virtual int GetType() const
        {
            return eType_Object;
        }
        
        virtual iMethodArray* GetMethods() const
        {
            return mMethods;
        }
        
        virtual iClassLoader* GetClassLoader() const
        {
            return mClassLoader;
        }
        
        virtual iMethod* GetMethod(const std::string& name) const
        {
            auto it = mMethodMap.find(name);
            if (it != mMethodMap.end())
            {
                return it->second;
            }
            
            iClass* parent = GetParent();
            return parent != nullptr ? parent->GetMethod(name) : nullptr;
        }
        
        virtual iPropertyArray* GetProperties() const
        {
            return mProps;
        }
        
        virtual iProperty* GetProperty(const std::string& name) const
        {
            auto it = mPropMap.find(name);
            if (it != mPropMap.end())
            {
                return it->second;
            }
            
            iClass* parent = GetParent();
            return parent != nullptr ? parent->GetProperty(name) : nullptr;
        }
        
        virtual std::string GetName() const
        {
            return mSimpleName;
        }
        
        virtual std::string GetFullName() const
        {
            return mFullName;
        }
        
        virtual bool InstanceOf(const Var& value) const
        {
            piCheck(value.GetType() == eType_Object || value.GetType() == eType_ObjectArray, false);
            
            SmartPtr<iRefObject> object = value.GetObject();
            SmartPtr<iClass> klass = object->GetClass();
            
            while (klass != this)
            {
                klass = klass->GetParent();
            }
            
            return !klass.IsNull();
        }
        
        virtual bool SubclassOf(iClass* klass) const
        {
            piAssert(klass != nullptr, false);
            
            SmartPtr<iClass> current = klass;
            while (!current.IsNull())
            {
                if (current == this)
                {
                    return true;
                }
                
                current = current->GetParent();
            }
            
            return false;
        }
    };
    
    typedef iClass* (*RefObjectPropertyClassGetter)();
    
    template <class ClassType, class ParamValueType, class ReturnValueType>
    class RefObjectProperty : public iProperty
    {
    private:
        typedef void(ClassType::*SetterPtr)(ParamValueType);
        SetterPtr mSetter;
        
        typedef ReturnValueType(ClassType::*GetterPtr)() const;
        GetterPtr mGetter;
        
        std::string mName;
        
        typedef std::map<std::string, Var> ConfigMap;
        ConfigMap mConfigMap;
        
        RefObjectPropertyClassGetter mClassGetter;
        
    public:
        RefObjectProperty(const std::string& name, RefObjectPropertyClassGetter classGetter):
        mGetter(nullptr), mSetter(nullptr), mName(name), mClassGetter(classGetter)
        {
        }
        
        virtual std::string GetName() const
        {
            return mName;
        }
        
        virtual iClass* GetTypeClass() const
        {
            return mClassGetter();
        }
        
        virtual void SetConfig(const std::string& name, const Var& value)
        {
            piAssert(!name.empty(), ;);
            piAssert(!IsReadonly(), ;);
            mConfigMap[name] = value;
        }
        
        virtual Var GetConfig(const std::string& name) const
        {
            piAssert(!name.empty(), Var());
            auto it = mConfigMap.find(name);
            return it != mConfigMap.end() ? it->second : Var();
        }
        
        virtual void SetValue(iRefObject* object, const Var& value)
        {
            if (mGetter != nullptr)
            {
                ClassType* self = dynamic_cast<ClassType*>(object);
                ReturnValueType v = VarExtractor<ParamValueType>::Extract(value);
                (self->*mSetter)(v);
            }
        }
        
        virtual Var GetValue(iRefObject* object) const
        {
            if (mGetter != nullptr)
            {
                ClassType* self = dynamic_cast<ClassType*>(object);
                return (self->*mGetter)();
            }
            else
            {
                return Var();
            }
        }
        
        void SetGetter(GetterPtr func)
        {
            piAssert(!IsReadonly(), ;);
            mGetter = func;
        }
        
        void SetSetter(SetterPtr func)
        {
            piAssert(!IsReadonly(), ;);
            mSetter = func;
        }
        
    };
    
    template <typename FuncPtr,
    class ClassType,
    typename A0 = int,
    typename A1 = int,
    typename A2 = int,
    typename A3 = int,
    typename A4 = int,
    typename A5 = int,
    typename A6 = int,
    typename A7 = int>
    class RefObjectMethod : public iMethod
    {
    protected:
        FuncPtr mFunc;
        std::string mName;
        
        typedef std::map<std::string, Var> ConfigMap;
        ConfigMap mConfigMap;
        
    public:
        RefObjectMethod(FuncPtr func, const std::string& name):
        mFunc(func), mName(name)
        {
        }
        
        virtual std::string GetName() const
        {
            return mName;
        }
        
        virtual void SetConfig(const std::string& name, const Var& value)
        {
            piAssert(!name.empty(), ;);
            piAssert(!IsReadonly(), ;);
            mConfigMap[name] = value;
        }
        
        virtual Var GetConfig(const std::string& name) const
        {
            piAssert(!name.empty(), Var());
            auto it = mConfigMap.find(name);
            return it != mConfigMap.end() ? it->second : Var();
        }
    };
    
    
    template <typename FuncPtr, class ClassType>
    class RefObjectMethodVoid : public RefObjectMethod<FuncPtr, ClassType>
    {
    private:
        using RefObjectMethod<FuncPtr, ClassType>::mFunc;
        
    public:
        RefObjectMethodVoid(FuncPtr func, const std::string& name):
        RefObjectMethod<FuncPtr, ClassType>(func, name)
        {
        }
        
        virtual Var Call(iRefObject* object,
                         const Var& arg0 = Var(),
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var(),
                         const Var& arg6 = Var(),
                         const Var& arg7 = Var())
        {
            ClassType* self = dynamic_cast<ClassType*>(object);
            return (self->*mFunc)();
        }
    };
    
    
    template <typename FuncPtr, class ClassType, typename A0>
    class RefObjectMethod1 : public RefObjectMethod<FuncPtr, ClassType, A0>
    {
    private:
        using RefObjectMethod<FuncPtr, ClassType, A0>::mFunc;
        
    public:
        RefObjectMethod1(FuncPtr func, const std::string& name):
        RefObjectMethod<FuncPtr, ClassType, A0>(func, name)
        {
        }
        
        virtual Var Call(iRefObject* object,
                         const Var& arg0 = Var(),
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var(),
                         const Var& arg6 = Var(),
                         const Var& arg7 = Var())
        {
            ClassType* self = dynamic_cast<ClassType*>(object);
            return (self->*mFunc)(VarExtractor<A0>::Extract(arg0));
        }
    };
    
    
    template <typename FuncPtr, class ClassType,
    typename A0, typename A1>
    class RefObjectMethod2 : public RefObjectMethod<FuncPtr, ClassType, A0, A1>
    {
    private:
        using RefObjectMethod<FuncPtr, ClassType, A0, A1>::mFunc;
        
    public:
        RefObjectMethod2(FuncPtr func, const std::string& name):
        RefObjectMethod<FuncPtr, ClassType, A0, A1>(func, name)
        {
        }
        
        virtual Var Call(iRefObject* object,
                         const Var& arg0 = Var(),
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var(),
                         const Var& arg6 = Var(),
                         const Var& arg7 = Var())
        {
            ClassType* self = dynamic_cast<ClassType*>(object);
            return (self->*mFunc)(VarExtractor<A0>::Extract(arg0),
                                  VarExtractor<A1>::Extract(arg1));
        }
    };
    
    
    template <typename FuncPtr, class ClassType,
    typename A0, typename A1, typename A2>
    class RefObjectMethod3 : public RefObjectMethod<FuncPtr, ClassType, A0, A1, A2>
    {
    private:
        using RefObjectMethod<FuncPtr, ClassType, A0, A1, A2>::mFunc;
        
    public:
        RefObjectMethod3(FuncPtr func, const std::string& name):
        RefObjectMethod<FuncPtr, ClassType, A0, A1, A2>(func, name)
        {
        }
        
        virtual Var Call(iRefObject* object,
                         const Var& arg0 = Var(),
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var(),
                         const Var& arg6 = Var(),
                         const Var& arg7 = Var())
        {
            ClassType* self = dynamic_cast<ClassType*>(object);
            return (self->*mFunc)(VarExtractor<A0>::Extract(arg0),
                                  VarExtractor<A1>::Extract(arg1),
                                  VarExtractor<A2>::Extract(arg2));
        }
    };
    
    
    template <typename FuncPtr, class ClassType,
    typename A0, typename A1, typename A2, typename A3>
    class RefObjectMethod4 : public RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3>
    {
    private:
        using RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3>::mFunc;
        
    public:
        RefObjectMethod4(FuncPtr func, const std::string& name):
        RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3>(func, name)
        {
        }
        
        virtual Var Call(iRefObject* object,
                         const Var& arg0 = Var(),
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var(),
                         const Var& arg6 = Var(),
                         const Var& arg7 = Var())
        {
            ClassType* self = dynamic_cast<ClassType*>(object);
            return (self->*mFunc)(VarExtractor<A0>::Extract(arg0),
                                  VarExtractor<A1>::Extract(arg1),
                                  VarExtractor<A2>::Extract(arg2),
                                  VarExtractor<A3>::Extract(arg3));
        }
    };
    
    
    template <typename FuncPtr, class ClassType,
    typename A0, typename A1, typename A2, typename A3, typename A4>
    class RefObjectMethod5 : public RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4>
    {
    private:
        using RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4>::mFunc;
        
    public:
        RefObjectMethod5(FuncPtr func, const std::string& name):
        RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4>(func, name)
        {
        }
        
        virtual Var Call(iRefObject* object,
                         const Var& arg0 = Var(),
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var(),
                         const Var& arg6 = Var(),
                         const Var& arg7 = Var())
        {
            ClassType* self = dynamic_cast<ClassType*>(object);
            return (self->*mFunc)(VarExtractor<A0>::Extract(arg0),
                                  VarExtractor<A1>::Extract(arg1),
                                  VarExtractor<A2>::Extract(arg2),
                                  VarExtractor<A3>::Extract(arg3),
                                  VarExtractor<A4>::Extract(arg4));
        }
    };
    
    
    template <typename FuncPtr, class ClassType,
    typename A0, typename A1, typename A2, typename A3, typename A4, typename A5>
    class RefObjectMethod6 : public RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4, A5>
    {
    private:
        using RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4, A5>::mFunc;
        
    public:
        RefObjectMethod6(FuncPtr func, const std::string& name):
        RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4, A5>(func, name)
        {
        }
        
        virtual Var Call(iRefObject* object,
                         const Var& arg0 = Var(),
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var(),
                         const Var& arg6 = Var(),
                         const Var& arg7 = Var())
        {
            ClassType* self = dynamic_cast<ClassType*>(object);
            return (self->*mFunc)(VarExtractor<A0>::Extract(arg0),
                                  VarExtractor<A1>::Extract(arg1),
                                  VarExtractor<A2>::Extract(arg2),
                                  VarExtractor<A3>::Extract(arg3),
                                  VarExtractor<A4>::Extract(arg4),
                                  VarExtractor<A5>::Extract(arg5));
        }
    };
    
    
    template <typename FuncPtr, class ClassType,
    typename A0, typename A1, typename A2, typename A3, typename A4, typename A5, typename A6>
    class RefObjectMethod7 : public RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4, A5, A6>
    {
    private:
        using RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4, A5, A6>::mFunc;
        
    public:
        RefObjectMethod7(FuncPtr func, const std::string& name):
        RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4, A5, A6>(func, name)
        {
        }
        
        virtual Var Call(iRefObject* object,
                         const Var& arg0 = Var(),
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var(),
                         const Var& arg6 = Var(),
                         const Var& arg7 = Var())
        {
            ClassType* self = dynamic_cast<ClassType*>(object);
            return (self->*mFunc)(VarExtractor<A0>::Extract(arg0),
                                  VarExtractor<A1>::Extract(arg1),
                                  VarExtractor<A2>::Extract(arg2),
                                  VarExtractor<A3>::Extract(arg3),
                                  VarExtractor<A4>::Extract(arg4),
                                  VarExtractor<A5>::Extract(arg5),
                                  VarExtractor<A6>::Extract(arg6));
        }
    };
    
    
    template <typename FuncPtr, class ClassType,
    typename A0, typename A1, typename A2, typename A3, typename A4, typename A5, typename A6, typename A7>
    class RefObjectMethod8 : public RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4, A5, A6, A7>
    {
    private:
        using RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4, A5, A6, A7>::mFunc;
        
    public:
        RefObjectMethod8(FuncPtr func, const std::string& name):
        RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4, A5, A6, A7>(func, name)
        {
        }
        
        virtual Var Call(iRefObject* object,
                         const Var& arg0 = Var(),
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var(),
                         const Var& arg6 = Var(),
                         const Var& arg7 = Var())
        {
            ClassType* self = dynamic_cast<ClassType*>(object);
            return (self->*mFunc)(VarExtractor<A0>::Extract(arg0),
                                  VarExtractor<A1>::Extract(arg1),
                                  VarExtractor<A2>::Extract(arg2),
                                  VarExtractor<A3>::Extract(arg3),
                                  VarExtractor<A4>::Extract(arg4),
                                  VarExtractor<A5>::Extract(arg5),
                                  VarExtractor<A6>::Extract(arg6),
                                  VarExtractor<A7>::Extract(arg7));
        }
    };
    
    
    template <typename FuncPtr, class ClassType>
    class RefObjectMethodVoidVoid : public RefObjectMethod<FuncPtr, ClassType>
    {
    private:
        using RefObjectMethod<FuncPtr, ClassType>::mFunc;
        
    public:
        RefObjectMethodVoidVoid(FuncPtr func, const std::string& name):
        RefObjectMethod<FuncPtr, ClassType>(func, name)
        {
        }
        
        virtual Var Call(iRefObject* object,
                         const Var& arg0 = Var(),
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var(),
                         const Var& arg6 = Var(),
                         const Var& arg7 = Var())
        {
            ClassType* self = dynamic_cast<ClassType*>(object);
            (self->*mFunc)();
            return Var();
        }
    };
    
    
    template <typename FuncPtr, class ClassType, typename A0>
    class RefObjectMethod1Void : public RefObjectMethod<FuncPtr, ClassType, A0>
    {
    private:
        using RefObjectMethod<FuncPtr, ClassType, A0>::mFunc;
        
    public:
        RefObjectMethod1Void(FuncPtr func, const std::string& name):
        RefObjectMethod<FuncPtr, ClassType, A0>(func, name)
        {
        }
        
        virtual Var Call(iRefObject* object,
                         const Var& arg0 = Var(),
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var(),
                         const Var& arg6 = Var(),
                         const Var& arg7 = Var())
        {
            ClassType* self = dynamic_cast<ClassType*>(object);
            (self->*mFunc)(VarExtractor<A0>::Extract(arg0));
            return Var();
        }
    };
    
    
    template <typename FuncPtr, class ClassType,
    typename A0, typename A1>
    class RefObjectMethod2Void : public RefObjectMethod<FuncPtr, ClassType, A0, A1>
    {
    private:
        using RefObjectMethod<FuncPtr, ClassType, A0, A1>::mFunc;
        
    public:
        RefObjectMethod2Void(FuncPtr func, const std::string& name):
        RefObjectMethod<FuncPtr, ClassType, A0, A1>(func, name)
        {
        }
        
        virtual Var Call(iRefObject* object,
                         const Var& arg0 = Var(),
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var(),
                         const Var& arg6 = Var(),
                         const Var& arg7 = Var())
        {
            ClassType* self = dynamic_cast<ClassType*>(object);
            (self->*mFunc)(VarExtractor<A0>::Extract(arg0),
                           VarExtractor<A1>::Extract(arg1));
            return Var();
        }
    };
    
    
    template <typename FuncPtr, class ClassType,
    typename A0, typename A1, typename A2>
    class RefObjectMethod3Void : public RefObjectMethod<FuncPtr, ClassType, A0, A1, A2>
    {
    private:
        using RefObjectMethod<FuncPtr, ClassType, A0, A1, A2>::mFunc;
        
    public:
        RefObjectMethod3Void(FuncPtr func, const std::string& name):
        RefObjectMethod<FuncPtr, ClassType, A0, A1, A2>(func, name)
        {
        }
        
        virtual Var Call(iRefObject* object,
                         const Var& arg0 = Var(),
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var(),
                         const Var& arg6 = Var(),
                         const Var& arg7 = Var())
        {
            ClassType* self = dynamic_cast<ClassType*>(object);
            (self->*mFunc)(VarExtractor<A0>::Extract(arg0),
                           VarExtractor<A1>::Extract(arg1),
                           VarExtractor<A2>::Extract(arg2));
            return Var();
        }
    };
    
    
    template <typename FuncPtr, class ClassType,
    typename A0, typename A1, typename A2, typename A3>
    class RefObjectMethod4Void : public RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3>
    {
    private:
        using RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3>::mFunc;
        
    public:
        RefObjectMethod4Void(FuncPtr func, const std::string& name):
        RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3>(func, name)
        {
        }
        
        virtual Var Call(iRefObject* object,
                         const Var& arg0 = Var(),
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var(),
                         const Var& arg6 = Var(),
                         const Var& arg7 = Var())
        {
            ClassType* self = dynamic_cast<ClassType*>(object);
            (self->*mFunc)(VarExtractor<A0>::Extract(arg0),
                           VarExtractor<A1>::Extract(arg1),
                           VarExtractor<A2>::Extract(arg2),
                           VarExtractor<A3>::Extract(arg3));
            return Var();
        }
    };
    
    
    template <typename FuncPtr, class ClassType,
    typename A0, typename A1, typename A2, typename A3, typename A4>
    class RefObjectMethod5Void : public RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4>
    {
    private:
        using RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4>::mFunc;
        
    public:
        RefObjectMethod5Void(FuncPtr func, const std::string& name):
        RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4>(func, name)
        {
        }
        
        virtual Var Call(iRefObject* object,
                         const Var& arg0 = Var(),
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var(),
                         const Var& arg6 = Var(),
                         const Var& arg7 = Var())
        {
            ClassType* self = dynamic_cast<ClassType*>(object);
            (self->*mFunc)(VarExtractor<A0>::Extract(arg0),
                           VarExtractor<A1>::Extract(arg1),
                           VarExtractor<A2>::Extract(arg2),
                           VarExtractor<A3>::Extract(arg3),
                           VarExtractor<A4>::Extract(arg4));
            return Var();
        }
    };
    
    
    template <typename FuncPtr, class ClassType,
    typename A0, typename A1, typename A2, typename A3, typename A4, typename A5>
    class RefObjectMethod6Void : public RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4, A5>
    {
    private:
        using RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4, A5>::mFunc;
        
    public:
        RefObjectMethod6Void(FuncPtr func, const std::string& name):
        RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4, A5>(func, name)
        {
        }
        
        virtual Var Call(iRefObject* object,
                         const Var& arg0 = Var(),
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var(),
                         const Var& arg6 = Var(),
                         const Var& arg7 = Var())
        {
            ClassType* self = dynamic_cast<ClassType*>(object);
            (self->*mFunc)(VarExtractor<A0>::Extract(arg0),
                           VarExtractor<A1>::Extract(arg1),
                           VarExtractor<A2>::Extract(arg2),
                           VarExtractor<A3>::Extract(arg3),
                           VarExtractor<A4>::Extract(arg4),
                           VarExtractor<A5>::Extract(arg5));
            return Var();
        }
    };
    
    
    template <typename FuncPtr, class ClassType,
    typename A0, typename A1, typename A2, typename A3, typename A4, typename A5, typename A6>
    class RefObjectMethod7Void : public RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4, A5, A6>
    {
    private:
        using RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4, A5, A6>::mFunc;
        
    public:
        RefObjectMethod7Void(FuncPtr func, const std::string& name):
        RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4, A5, A6>(func, name)
        {
        }
        
        virtual Var Call(iRefObject* object,
                         const Var& arg0 = Var(),
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var(),
                         const Var& arg6 = Var(),
                         const Var& arg7 = Var())
        {
            ClassType* self = dynamic_cast<ClassType*>(object);
            (self->*mFunc)(VarExtractor<A0>::Extract(arg0),
                           VarExtractor<A1>::Extract(arg1),
                           VarExtractor<A2>::Extract(arg2),
                           VarExtractor<A3>::Extract(arg3),
                           VarExtractor<A4>::Extract(arg4),
                           VarExtractor<A5>::Extract(arg5),
                           VarExtractor<A6>::Extract(arg6));
            return Var();
        }
    };
    
    
    template <typename FuncPtr, class ClassType,
    typename A0, typename A1, typename A2, typename A3, typename A4, typename A5, typename A6, typename A7>
    class RefObjectMethod8Void : public RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4, A5, A6, A7>
    {
    private:
        using RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4, A5, A6, A7>::mFunc;
        
    public:
        RefObjectMethod8Void(FuncPtr func, const std::string& name):
        RefObjectMethod<FuncPtr, ClassType, A0, A1, A2, A3, A4, A5, A6, A7>(func, name)
        {
        }
        
        virtual Var Call(iRefObject* object,
                         const Var& arg0 = Var(),
                         const Var& arg1 = Var(),
                         const Var& arg2 = Var(),
                         const Var& arg3 = Var(),
                         const Var& arg4 = Var(),
                         const Var& arg5 = Var(),
                         const Var& arg6 = Var(),
                         const Var& arg7 = Var())
        {
            ClassType* self = dynamic_cast<ClassType*>(object);
            (self->*mFunc)(VarExtractor<A0>::Extract(arg0),
                           VarExtractor<A1>::Extract(arg1),
                           VarExtractor<A2>::Extract(arg2),
                           VarExtractor<A3>::Extract(arg3),
                           VarExtractor<A4>::Extract(arg4),
                           VarExtractor<A5>::Extract(arg5),
                           VarExtractor<A6>::Extract(arg6),
                           VarExtractor<A7>::Extract(arg7));
            return Var();
        }
    };
}

#endif



















