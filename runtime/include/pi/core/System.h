/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2012.8.21   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_SYSTEM_H
#define PI_CORE_SYSTEM_H

namespace nspi
{
    //==========================================================================
    // file system
    
    enum eFileType
    {
        eFileType_File      = 1,
        eFileType_Directory = 2,
        eFileType_Unknown   = 3,
    };

    iStream* CreateFileStream(const std::string &path, const std::string &mode);
    
    //==========================================================================
    int64_t piGetFileSize(const std::string &path);
    bool piMoveFile(const std::string &pszOrigin, const std::string &pszNew);
    bool piDeleteFile(const std::string &pszPath);
	bool piFileExist(const std::string &pszPath);
    bool CreateDirectory(const std::string &pszPath, bool recursively);
    bool piDeleteDirectory(const std::string &pszPath, bool recursively);
    std::string piGetWorkingDirectory();
    std::string piGetDirectory(const std::string &pszPath);
    std::string piGetBaseName(const std::string &pszPath);
    std::string piGetExtendName(const std::string &path);
    bool piDirectoryExist(const std::string &pszPath);
    std::string piAbsPath(const std::string &pszPath);
    
    enum eFileFinderFlag
    {
        eFileFinderFlag_Recursive = piBit(0),
        eFileFinderFlag_Directory = piBit(1),
        eFileFinderFlag_Ascending = piBit(2),
        eFileFinderFlag_Descending = piBit(3),
    };
    
    struct iFileFinder : public iRefObject
    {
        virtual ~iFileFinder() {}
        
        virtual int32_t Search(const std::string &pszPath, uint32_t uFlags) = 0;
        virtual void Reset() = 0;
        /**
         * \deprecated 使用Name()。
         */
        virtual std::string Path() const = 0;
        virtual std::string Name() const = 0;
        virtual iStream* OpenStream(const char *mode) const = 0;
        virtual bool IsDirectory() const = 0;
        virtual bool End() const = 0;
        virtual bool MoveNext() = 0;
        virtual bool MovePrevious() = 0;
        virtual int32_t Size() const = 0;
    };
    iFileFinder* CreateFileFinder();
    
    std::string piFindResource(const std::string& filename, const std::string& extension);
    
    
    /**
     * 监听文件或文件夹的改变
     */
    
    enum
    {
        eFileEvent_Modify = piBit(0),
        eFileEvent_Delete = piBit(1),
        eFileEvent_Move = piBit(2),
    };
    
    struct iFileListener : public iRefObject
    {
        virtual ~iFileListener() {}
        
        virtual void OnFileEvent(const std::string &path, int32_t events) = 0;
    };
    
    struct iFileObserver : public iRefObject
    {
        virtual ~iFileObserver() {}
        
        virtual int32_t Watch(const std::string &path, int32_t events) = 0;
        
        virtual void Remove(int32_t eventId) = 0;
        
        virtual void RegisterListener(iFileListener *listener) = 0;
        
        virtual void UnregisterListener(iFileListener *listener) = 0;
    };
    
    iFileObserver* CreateFileObserver();
    
    std::string piGetAppDirectory();
    
    
    /**
     * ----------------------------------------------------------------------------------
     */
    
    PIEnum()
    enum eOS
    {
        eOS_Unknown         = -1,
        eOS_iOS             = 1,
        eOS_macOS           = 2,
        eOS_tvOS            = 3,
        eOS_watchOS         = 4,
        eOS_Android         = 5,
        eOS_Windows         = 6,
    };
    
    PIFunction()
    int piGetSystemCode();
    
    PIFunction()
    int32_t piGetSystemVersionCode();
    
    PIFunction()
    int32_t piGetSystemMajorCode();
    
    PIFunction()
    int32_t piGetSystemMinorCode();
    
    PIFunction()
    int32_t piGetSystemPatchCode();
    
    PIFunction()
    std::string piGetSystemVersionString();
    
}

#endif






























