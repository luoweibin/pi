/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2015.4.12   0.1     Create
 ******************************************************************************/
#ifndef PI_TEST_H
#define PI_TEST_H

#include <pi/Core.h>

namespace nspi
{
    struct iTestCase
    {
        virtual ~iTestCase() {}
        
        virtual void Run() = 0;
        
        virtual std::string GetGroup() const = 0;
        
        virtual std::string GetFeature() const = 0;
    };
    
    void piSetTestParam(const std::string& name, const Var& value);
    Var piGetTestParam(const std::string& name, const Var& defValue = Var());

    void piRegisterTestCase(iTestCase* testCase);
    
    void piRunTests();
    
}

#define PI_TEST(group, feature) \
static void TestCase_##group##_##feature##_Run(); \
struct TestCase_##group##_##feature : public iTestCase \
{ \
    TestCase_##group##_##feature() \
    { \
        nspi::piRegisterTestCase(this); \
    } \
    virtual void Run() \
    { \
        TestCase_##group##_##feature##_Run(); \
    } \
    virtual std::string GetGroup() const \
    { \
        return #group; \
    } \
    virtual std::string GetFeature() const \
    { \
        return #feature; \
    } \
} g_TestCase_##group##_##feature; \
void TestCase_##group##_##feature##_Run()

#define PI_ASSERT(v) assert(v)

#endif

































