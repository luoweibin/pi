/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.16   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_FACECAPTURE_H
#define PI_GAME_FACECAPTURE_H

namespace nspi
{
    
    PIInterface()
    struct iFaceCapture : public iComponent
    {
        PI_DEFINE(iFaceCapture)
        
        PIGetter()
        virtual int32_t GetFaceID() const = 0;
        
        PISetter()
        virtual void SetFaceID(int32_t id) = 0;
        
        PIGetter()
        virtual bool IsOrth() const = 0;
        
        PISetter()
        virtual void SetOrth(bool flag) = 0;
    };
    
    iFaceCapture* CreateFaceCapture();
    
    
    PIInterface()
    struct iFaceClone : public iComponent
    {
        PI_DEFINE(iFaceClone)
        
        PIGetter()
        virtual int32_t GetFrom() const = 0;
        
        PISetter()
        virtual void SetFrom(int32_t id) = 0;
        
        PIGetter()
        virtual int32_t GetTo() const = 0;
        
        PISetter()
        virtual void SetTo(int32_t id) = 0;
        
        PIGetter()
        virtual std::string GetMaskUri() const = 0;
        
        PISetter()
        virtual void SetMaskUri(const std::string& uri) = 0;
    };
    
    iFaceClone* CreateFaceClone();
}

#endif























