/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.27   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_ANIMATION_H
#define PI_GAME_ANIMATION_H

#include <pi/game/animation/AnimCurve.h>
#include <pi/game/animation/ModelAnim.h>
#include <pi/game/animation/Animation.h>

namespace nspi
{
    
    PIInterface(HasCreator = false)
    struct iAnimProp : public iRefObject
    {
        PI_DEFINE(iAnimProp)
        
        PIMethod()
        virtual void SetValue(const Var& value) = 0;
        
        PIMethod()
        virtual int GetType() const = 0;
    };
    
    iAnimProp* CreateAnimProp(iRefObject* target, iProperty* prop);    
    
    
    PIInterface(HasCreator = false)
    struct iAnimComp : public iComponent
    {
        PI_DEFINE(iAnimComp)
        
        PIMethod()
        virtual void OnUpdate() = 0;
        
        PIMethod()
        virtual iModelAnim* GetAnimation() const = 0;
        
        PIMethod()
        virtual iAnimProp* GetProperty(const std::string& uri) const = 0;
    };
    
}


#endif



























