/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.10   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_3D_MODELMATERIAL_H
#define PI_GAME_3D_MODELMATERIAL_H

namespace nspi
{
    enum
    {
        eRenderMode_Opaque              = 0,
        eRenderMode_Transparent         = 1,
        eRenderMode_Semitransparent     = 2,
    };
    
    
    PIInterface()
    struct iModelMaterial : public iRefObject
    {
        PI_DEFINE(iModelMaterial)
        
        PIGetter()
        virtual std::string GetName() const = 0;
        
        PISetter()
        virtual void SetName(const std::string& name) = 0;
        
        PIGetter()
        virtual int GetRenderMode() const = 0;
        
        PISetter()
        virtual void SetRenderMode(int mode) = 0;
        
        
        PIGetter()
        virtual std::string GetAlbedoUri() const = 0;
        
        PISetter()
        virtual void SetAlbedoUri(const std::string& uri) = 0;
        
        PIGetter()
        virtual std::string GetMaskUri() const = 0;
        
        PISetter()
        virtual void SetMaskUri(const std::string& uri) = 0;
        
        PIGetter()
        virtual std::string GetEnvMapUri() const = 0;
        
        PISetter()
        virtual void SetEnvMapUri(const std::string& uri) = 0;
        
        
        
        PIGetter()
        virtual iTexture2D* GetAlbedoTex() const = 0;
        
        PISetter(AssetUri = "AlbedoUri")
        virtual void SetAlbedoTex(iTexture2D* tex) = 0;
        
        PIGetter()
        virtual iTexture2D* GetMaskTex() const = 0;
        
        PISetter(AssetUri = "MaskUri")
        virtual void SetMaskTex(iTexture2D* tex) = 0;
        
        PIGetter()
        virtual iCubeMap* GetEnvMap() const = 0;
        
        PISetter(AssetUri = "EnvMapUri")
        virtual void SetEnvMap(iCubeMap* cubeMap) = 0;
    };
    
    iModelMaterial* CreateModelMaterial();
    
    PI_DEFINE_OBJECT_ARRAY(iModelMaterial);
    iModelMaterialArray* CreateModelMaterialArray();
}

#endif














