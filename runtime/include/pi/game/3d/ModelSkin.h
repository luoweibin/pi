/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.12.2   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_3D_MODELSKIN_H
#define PI_GAME_3D_MODELSKIN_H

namespace nspi
{
    struct iModelNodeArray;
    struct iModelMesh;
    struct iModelMaterial;
    
    
    PIInterface()
    struct iModelSkin : public iAsset
    {
        PI_DEFINE(iModelSkin)
        
        PISetter()
        virtual void SetName(const std::string& name) = 0;
        
        PIGetter()
        virtual std::string GetName() const = 0;
        
        PIGetter()
        virtual iModelNodeArray* GetJoints() const = 0;
        
        PISetter()
        virtual void SetJoints(iModelNodeArray* joints) = 0;
        
        /**
         * Inverse Bind Matrics
         */
        PIGetter()
        virtual iMat4Array* GetIBMs() const = 0;
        
        PIGetter()
        virtual iModelMesh* GetMesh() const = 0;
        
        PISetter()
        virtual void SetMesh(iModelMesh* mesh) = 0;
    };
    

    iModelSkin* CreateModelSkin();
    
    PI_DEFINE_OBJECT_ARRAY(iModelSkin);
    iModelSkinArray* CreateModelSkinArray();
    
    
}

#endif































