/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.1   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_3D_MESH_H
#define PI_GAME_3D_MESH_H


namespace nspi
{
    PIEnum()
    enum eMesh
    {
        eMesh_Position      = 1,
        eMesh_UV            = 2,
        eMesh_Normal        = 3,
        eMesh_JointWeight   = 4,
    };
    
    struct VertexWeight
    {
        int32_t jointIndex;
        int32_t vertexIndex;
        float bias;
    };
    
    PIInterface()
    struct iMeshComp : public iRefObject
    {
        PI_DEFINE(iMeshComp)
        
        PIMethod()
        virtual int GetName() const = 0;
        
        PIMethod()
        virtual void SetName(int name) = 0;
        
        PIMethod()
        virtual void SetData(iMemory* data) = 0;
        
        PIMethod()
        virtual iMemory* GetData() const = 0;
    };
    
    iMeshComp* CreateMeshComp();
    
    PI_DEFINE_OBJECT_ARRAY(iMeshComp);
    iMeshCompArray* CreateMeshCompArray();
    
    
    PIInterface()
    struct iModelMesh : public iAsset
    {
        PI_DEFINE(iModelMesh)
        
        PIMethod()
        virtual void SetName(const std::string& name) = 0;
        
        PIMethod()
        virtual std::string GetName() const = 0;
        
        PIMethod()
        virtual void SetVertexCount(int32_t count) = 0;
        
        PIMethod()
        virtual int32_t GetVertexCount() const = 0;
        
        PIMethod()
        virtual iMeshCompArray* GetComps() const = 0;
        
        PIMethod()
        virtual void SetComps(iMeshCompArray* components) = 0;
        
        PIMethod()
        virtual iMeshComp* GetComp(int32_t name) const = 0;
        
        PIMethod()
        virtual void SetMaterial(iMaterial* material) = 0;
        
        PIMethod()
        virtual iMaterial* GetMaterial() const = 0;
    };
    
    iModelMesh* CreateModelMesh();
    
    PI_DEFINE_OBJECT_ARRAY(iModelMesh);
    iModelMeshArray* CreateModelMeshArray();
    
    struct MeshVertex
    {
        float pos[3];
        float uv[2];
        float jindex[4];
        float jweight[3];
        float normal[3];
    };
    
    PIInterface()
    struct iMesh : public iRefObject
    {
        PI_DEFINE(iMesh)
        
        PIMethod()
        virtual void SetVBO(int32_t vbo) = 0;
        
        PIMethod()
        virtual int32_t GetVBO() const = 0;
        
        PIMethod()
        virtual void SetIBO(int32_t ibo) = 0;
        
        PIMethod()
        virtual int32_t GetIBO() const = 0;
        
        PIMethod()
        virtual int32_t GetCount() const = 0;
        
        PIMethod()
        virtual void SetCount(int32_t count) = 0;
        
        PIMethod()
        virtual int GetType() const = 0;
        
        PIMethod()
        virtual int GetIndexSize() const = 0;
    };
    
    iMesh* CreateMesh();
    
    iMesh* CreateMeshFromLib(iModelMesh* lib);
}


#endif
























