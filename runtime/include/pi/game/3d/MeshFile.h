/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.1   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_3D_MESHFILE_H
#define PI_GAME_3D_MESHFILE_H

namespace nspi
{
    enum
    {
        eMeshFace_Triangle = 1,
    };
    
    // model
    struct MeshFile
    {
        // file magic number
        int32_t magic;
        
        // version
        uint32_t version;
        
        // name
        char name[PI_MODEL_NAMESIZE+1];
        
        // number of vertices
        uint32_t vertexCount;
        
        // start offset of attributes
        uint32_t attrStart;
        
        // number of attributes
        uint32_t attrCount;
        
        MeshFile():
        version(1),
        vertexCount(0),
        attrStart(0), attrCount(0)
        {
            char* hdr = (char*)&magic;
            hdr[0] = 'P';
            hdr[1] = 'I';
            hdr[2] = 'M';
            hdr[3] = 'L';
            
            memset(name, 0, sizeof(name));
        }
    };
    
    struct JointItem
    {
        // name
        char name[PI_MODEL_NAMESIZE + 1];
        
        // index of parent, -1 for root joint.
        int32_t parent;
        
        // position, in xyz order.
        vec3 pos;
        
        // orientation, in xyz order.
        vec3 orient;
        
        // scale, in xyz order.
        vec3 scale;
        
        JointItem():
        parent(-1)
        {
            memset(name, 0, sizeof(name));
        }
    };
    
    // Skeleton header
    struct SkeletonHeader
    {
        // name
        char name[PI_MODEL_NAMESIZE + 1];
        
        // start offset of joints
        uint32_t start;
        
        // number of joints.
        uint32_t count;
    };
    
    
    // attribute of mesh
    struct MeshAttr
    {
        // name
        char name[PI_MODEL_NAMESIZE + 1];
        
        // start offset
        uint32_t start;
        
        // size of attribute
        uint32_t size;
        
        // element type
        int32_t type;
        
        MeshAttr():
        start(0), size(0), type(eType_U16)
        {
            memset(name, 0, sizeof(name));
        }
    };
    
    struct MeshWeight
    {
        // index of vertex
        uint32_t vertexIndex;
        
        // index of joint
        uint32_t jointIndex;
        
        // bias of joint
        float jointBias;
        
        MeshWeight():
        vertexIndex(0), jointIndex(0), jointBias(0.0)
        {
        }
    };
}

#endif























