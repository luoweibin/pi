/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.6   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_3D_MATERIAL_H
#define PI_GAME_3D_MATERIAL_H

namespace nspi
{
    PIEnum()
    enum eRenderType
    {
        eRenderType_Opaque,         // Sort from near to far, always use without blend.
        eRenderType_Transparent,    // Sort from far to near, always use with blend.
        eRenderType_ByArray,        // Do not sort, always use in 2D Sprite.
        eRenderType_Count,
    };
    
    PIEnum()
    enum eRenderQueue
    {
        eRenderQueue_BackGround     = 1000,
        eRenderQueue_Opaque         = 2000,
        eRenderQueue_Transparent    = 3000,
    };
    
    PIInterface()
    struct iMaterialProp : public iRefObject
    {
        PI_DEFINE(iMaterialProp)
        
        PIGetter()
        virtual std::string GetName() const = 0;
        
        PISetter()
        virtual void SetName(const std::string& name) = 0;
        
        PIGetter()
        virtual Var GetValue() const = 0;
        
        PISetter()
        virtual void SetValue(const Var& value) = 0;
        
        PIGetter()
        virtual std::string GetUri() const = 0;
        
        PISetter()
        virtual void SetUri(const std::string& uri) = 0;
    };
    
    iMaterialProp* CreateMaterialProp();
    
    PI_DEFINE_OBJECT_ARRAY(iMaterialProp);
    
    iMaterialPropArray* CreateMaterialPropArray();
    
    
    PIInterface()
    struct iMaterial : public iAsset
    {
        PI_DEFINE(iMaterial)
        
        PIGetter()
        virtual std::string GetName() const = 0;
        
        PISetter()
        virtual void SetName(const std::string& name) = 0;
        
        PIGetter()
        virtual iMaterialPropArray* GetProps() const = 0;
        
        PISetter()
        virtual void SetProps(iMaterialPropArray* props) = 0;
        
        PIGetter()
        virtual std::string GetShaderUri() const = 0;
        
        PISetter()
        virtual void SetShaderUri(const std::string& uri) = 0;
        
        PIGetter()
        virtual iShaderProgram* GetShader() const = 0;
        
        PISetter(AssetUri = "ShaderUri")
        virtual void SetShader(iShaderProgram* shader) = 0;
        
        PIGetter()
        virtual std::string GetRenderStateUri() const = 0;
        
        PISetter()
        virtual void SetRenderStateUri(const std::string& uri) = 0;
        
        PIGetter()
        virtual iRenderState* GetRenderState() const = 0;
        
        PISetter(AssetUri = "RenderStateUri")
        virtual void SetRenderState(iRenderState* state) = 0;
        
        PIGetter(Class = eRenderQueue)
        virtual int GetRenderQueue() const = 0;

        PISetter()
        virtual void SetRenderQueue(int Queue) = 0;
        
        PIGetter()
        virtual int GetRenderType() const = 0;
        
        PISetter()
        virtual void SetRenderType(int type) = 0;
        
        PIMethod()
        virtual void SetProperty(const std::string& name, const Var& value) = 0;
        
        PIMethod()
        virtual Var GetProperty(const std::string& name, const Var& defValue) const = 0;
        
        PIMethod()
        virtual iMaterialProp* FindProperty(const std::string& name) const = 0;
    };
    
    iMaterial* CreateMaterial();
    
    iMaterial* CreateDefaultMaterial();
    
    PI_DEFINE_OBJECT_ARRAY(iMaterial);
    
    iMaterialArray* CreateMaterialArray();
}

#endif



























