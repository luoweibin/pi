/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 Sword     2017.7.6   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_3D_BASEMESH_FACE2DMESH_H
#define PI_GAME_3D_BASEMESH_FACE2DMESH_H

namespace nspi
{
    PIInterface()
    struct iFace2DMesh : public iDynamicMesh
    {
        PI_DEFINE(iFace2DMesh)
       
        PIGetter()
        virtual bool IsLockFace() const = 0;
        
        PISetter()
        virtual void SetLockFace(bool flag) = 0;

        PISetter()
        virtual void SetAnchor(const vec2& anchor) = 0;
        
        PIGetter()
        virtual vec2 GetAnchor() const = 0;
        
        PIGetter()
        virtual rect GetPosRect() const = 0;
        
        PISetter()
        virtual void SetPosRect(const rect& posRect) = 0;
        
        PIGetter()
        virtual vec2 GetPos() const = 0;
        
        PISetter()
        virtual void SetPos(const vec2& pos) = 0;
        
        PIGetter()
        virtual float GetFaceHeadMul1() const = 0;
        
        PISetter()
        virtual void SetFaceHeadMul1(float mul1) = 0;
        
        PIGetter()
        virtual float GetFaceHeadMul2() const = 0;
        
        PISetter()
        virtual void SetFaceHeadMul2(float mul2) = 0;
        
        PIGetter()
        virtual bool IsBaseXScaling() const = 0;
        
        PISetter()
        virtual void SetBaseXScaling(bool flag) = 0;
        
        PIGetter()
        virtual int32_t GetFaceID() const = 0;
        
        PISetter()
        virtual void SetFaceID(int32_t faceID) = 0;
        
        PIMethod()
        virtual rect GetFaceScreenRect() = 0;
    };
    
    iFace2DMesh* CreateFace2DMesh();
}

#endif
