/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 Sword     2017.6.22   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_3D_BASEMESH_BASEMESHCOMMON_H
#define PI_GAME_3D_BASEMESH_BASEMESHCOMMON_H

namespace nspi
{

    PIInterface(HasCreator = false)
    struct iDynamicMesh : public iComponent
    {
        PI_DEFINE(iDynamicMesh)
        
        PISetter()
        virtual void SetMaterialUri(const std::string& uri) = 0;
        
        PIGetter()
        virtual std::string GetMaterialUri() const = 0;
        
        PISetter(AssetUri = "MaterialUri")
        virtual void SetMaterial(iMaterial* mat) = 0;

        PIGetter()
        virtual iMaterial* GetMaterial() const = 0;
        
        PISetter()
        virtual void SetDirty(bool dirty) = 0;
        
        PIGetter()
        virtual bool IsDirty() const = 0;
        
        PIMethod()
        virtual iMemory* GetVertexBuffer() const = 0;
     
        PIMethod()
        virtual iMemory* GetIndexBuffer() const = 0;
        
        PIMethod()
        virtual void SetVertexCount(int32_t count) = 0;
        
        PIMethod()
        virtual int32_t GetVAO() = 0;
        
        virtual mat4& GetAnchorMatrix() = 0;
        
        PIMethod()
        virtual iModelNode* GetModelNode() const = 0;
        
        PIMethod()
        virtual iMesh* GetMesh() const = 0;
        
        PIMethod()
        virtual void Apply() = 0;
        
        PIMethod()
        virtual void ApplyVBO() = 0;
        
        PIMethod()
        virtual void ApplyIBO() = 0;
        
        PIMethod()
        virtual void ApplyVAO() = 0;
    };
    
    // UIMesh is a quad mesh, but this mesh will change size when resize has been called.
    PIInterface()
    struct iUIMesh : public iDynamicMesh
    {
        PI_DEFINE(iUIMesh)
        
        PISetter()
        virtual void SetMaterialUri(const std::string& uri) = 0;
        
        PIGetter()
        virtual std::string GetMaterialUri() const = 0;
        
        PISetter(AssetUri = "MaterialUri")
        virtual void SetMaterial(iMaterial* mat) = 0;
        
        PIGetter()
        virtual iMaterial* GetMaterial() const = 0;
    
        PIGetter()
        virtual rect GetUVRect() const = 0;
        
        PISetter()
        virtual void SetUVRect(const rect& rect) = 0;
        
        PIGetter()
        virtual rect GetPosRect() const = 0;
        
        PISetter()
        virtual void SetPosRect(const rect& rect) = 0;
        
        PIGetter()
        virtual vec2 GetPos() const = 0;
        
        PISetter()
        virtual void SetPos(const vec2& left_bottom_pos) = 0;
        
        PIGetter()
        virtual vec2 GetPosSize() const = 0;
        
        PISetter()
        virtual void SetPosSize(const vec2& size) = 0;
        
        PIGetter()
        virtual vec2 GetUVBegin() const = 0;
        
        PISetter()
        virtual void SetUVBegin(const vec2& uvBegin) = 0;
        
        PIGetter()
        virtual vec2 GetUVSize() const = 0;
        
        PISetter()
        virtual void SetUVSize(const vec2& uvSize) = 0;
        
        PIGetter()
        virtual vec2 GetAnchor() const = 0;
        
        PISetter()
        virtual void SetAnchor(const vec2& anchor) = 0;
    };
    
    iUIMesh* CreateUIMesh();
    
    PIInterface()
    struct iQuadMesh : public iDynamicMesh
    {
        PI_DEFINE(iQuadMesh)
        
        PIGetter()
        virtual vec4 GetUVRect() const = 0;
        
        PISetter()
        virtual void SetUVRect(const vec4& rect) = 0;
        
        PIGetter()
        virtual vec4 GetPosRect() const = 0;
        
        PISetter()
        virtual void SetPosRect(const vec4& rect) = 0;

    };
    
    iQuadMesh* CreateQuadMesh();
}

#endif
