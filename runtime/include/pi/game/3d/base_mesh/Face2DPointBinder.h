/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 Sword     2017.6.22   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_3D_BASEMESH_FACE2DPOINTBINDER_H
#define PI_GAME_3D_BASEMESH_FACE2DPOINTBINDER_H

namespace nspi
{
    PIInterface()
    struct iFace2DPointBinder  : public iComponent
    {
        PI_DEFINE(iFace2DPointBinder)
        
        PISetter()
        virtual void SetBindTranslate(bool flag) = 0;
        
        PIGetter()
        virtual bool IsBindTranslate() const = 0;
        
        PISetter()
        virtual void SetBindRotation(bool flag) = 0;
        
        PIGetter()
        virtual bool IsBindRotation() const = 0;
        
        
    };
    
    iFace2DPointBinder* CreateFace2DPointBinder();
}

#endif
