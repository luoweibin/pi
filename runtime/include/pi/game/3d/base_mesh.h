/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
Sword     2017.7.4   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_3D_BASEMESH_H
#define PI_GAME_3D_BASEMESH_H

#include <pi/game/3d/base_mesh/BaseMeshCommon.h>
#include <pi/game/3d/base_mesh/Face2DPointBinder.h>
#include <pi/game/3d/base_mesh/Face2DMesh.h>

#endif
