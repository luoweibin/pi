/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 Sword     2017.6.29   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_3D_RENDERSTATE_H
#define PI_GAME_3D_RENDERSTATE_H

namespace nspi
{
    PIEnum()
    enum eColorMask
    {
        eColorMask_R = piBit(0),
        eColorMask_G = piBit(8),
        eColorMask_B = piBit(16),
        eColorMask_A = piBit(24),
        eColorMask_None = 0,
    };
    
    PIInterface()
    struct iRenderState : public iAsset
    {
        PI_DEFINE(iRenderState)
        
        PIGetter(Class = eGraphicsFeature)
        virtual int32_t GetFeatures() const = 0;
        
        PISetter()
        virtual void SetFeatures(int32_t features) = 0;
        
        PIGetter(Class = eBlendFunc)
        virtual int32_t GetSrcBlendFunc() const = 0;
        
        PISetter()
        virtual void SetSrcBlendFunc(int32_t func) = 0;
        
        PIGetter(Class = eBlendFunc)
        virtual int32_t GetDstBlendFunc() const = 0;
        
        PISetter()
        virtual void SetDstBlendFunc(int32_t func) = 0;
        
        PIGetter()
        virtual bool IsZWrite() const = 0;
        
        PISetter()
        virtual void SetZWrite(bool ZWrite) = 0;
        
        PIGetter(Class = eColorMask)
        virtual int32_t GetColorMask() const = 0;
        
        PISetter()
        virtual void SetColorMask(int32_t mask) = 0;
        
        PIMethod()
        virtual void EnableState() = 0;
        
        PIMethod()
        virtual void DisableState() = 0;
    };
    
    iRenderState* CreateRenderState();
}

#endif
