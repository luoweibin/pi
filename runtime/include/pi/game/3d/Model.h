/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.7   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_3D_MODEL_H
#define PI_GAME_3D_MODEL_H

namespace nspi
{
    
    /**
     * -------------------------------------------------------------------------
     * Component
     */
    struct iModelScene;
    
    PIInterface()
    struct iModelInstance : public iComponent
    {
        PI_DEFINE(iModelInstance)
        
        PIGetter()
        virtual std::string GetUri() const = 0;
        
        PISetter()
        virtual void SetUri(const std::string& uri) = 0;
        
        PIGetter()
        virtual std::string GetScript() const = 0;
        
        PISetter()
        virtual void SetScript(const std::string& uri) = 0;
        
        /**
         * Methods
         */
        
        PISetter(AssetUri = "Uri")
        virtual void SetModelScene(iModelScene* scene) = 0;
        
        PIGetter()
        virtual iModelScene* GetModelScene() const = 0;
    };
    
    iModelInstance* CreateModelInstance();
    
    
    /**
     * -------------------------------------------------------------------------
     * ModelScene
     */
    
    PIInterface()
    struct iModelScene : public iAsset
    {
        PI_DEFINE(iModelScene)
        
        PIGetter()
        virtual std::string GetName() const = 0;
        
        PISetter()
        virtual void SetName(const std::string& name) = 0;
        
        PISetter()
        virtual void SetAnimation(iModelAnim* animation) = 0;
        
        PIGetter()
        virtual iModelAnim* GetAnimation() const = 0;
        
        
        PIGetter()
        virtual iModelMeshArray* GetMeshes() const = 0;
 
        PISetter()
        virtual void SetMeshes(iModelMeshArray* meshes) = 0;
        
        PIMethod()
        virtual iModelMesh* FindMesh(const std::string& name) const = 0;
        
        
        PIGetter()
        virtual iModelSkinArray* GetSkins() const = 0;
        
        PISetter()
        virtual void SetSkins(iModelSkinArray* skins) = 0;
        
        PIMethod()
        virtual iModelSkin* FindSkin(const std::string& name) const = 0;
        
        
        PIGetter()
        virtual iMaterialArray* GetMaterials() const = 0;

        PISetter()
        virtual void SetMaterials(iMaterialArray* materials) = 0;
        
        PIMethod()
        virtual iMaterial* FindMaterial(const std::string& name) const = 0;
        
        
        PIMethod()
        virtual iModelNode* GetRootNode() const = 0;
        
        PIMethod()
        virtual void SetRootNode(iModelNode* node) = 0;
        
        
        /**
         * Callbacks
         */
        
        virtual void OnUpdate(float delta) = 0;
    };
    
    iModelScene* CreateModelScene();
}

#endif























