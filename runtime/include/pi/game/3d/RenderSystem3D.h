/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.5   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_3D_RENDERSYSTEM3D_H
#define PI_GAME_3D_RENDERSYSTEM3D_H

namespace nspi
{
    
    PIInterface()
    struct iRenderSystem3D : public iSystem
    {
        PI_DEFINE(iRenderSystem3D)
    };
    
    iRenderSystem3D* CreateRenderSystem3D();
    
    
    
    PIInterface()
    struct iModelSkinDebug : public iSystem
    {
        PI_DEFINE(iModelSkinDebug)
    };
    
    iModelSkinDebug* CreateModelSkinDebug();
    
    
}

#endif
