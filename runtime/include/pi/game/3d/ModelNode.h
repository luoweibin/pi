/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.5   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_3D_MODELNODE_H
#define PI_GAME_3D_MODELNODE_H

namespace nspi
{
    
    struct iModelNodeArray;
    struct iTransform;
    
    
    PIEnum()
    enum eModelNode
    {
        eModelNode_Null     = 0,
        eModelNode_Joint    = 1,
        eModelNode_Skin     = 2,
        eModelNode_Mesh     = 3,
    };
    
    PIInterface()
    struct iModelNode : public iRefObject
    {
        PI_DEFINE(iModelNode)
        
        PIGetter(Class = eModelNode)
        virtual int GetType() const = 0;
        
        PISetter()
        virtual void SetType(int type) = 0;
        
        PIGetter()
        virtual std::string GetName() const = 0;
        
        PISetter()
        virtual void SetName(const std::string& name) = 0;
        
        PIGetter()
        virtual iModelNodeArray* GetChildren() const = 0;
        
        PISetter()
        virtual void SetChildren(iModelNodeArray* children) = 0;
        
        PIGetter()
        virtual iTransform* GetTransform() const = 0;
        
        PISetter()
        virtual void SetTransform(iTransform* transform) = 0;
        
        PIGetter()
        virtual iModelNode* GetParent() const = 0;
        
        PISetter()
        virtual void SetParent(iModelNode* node) = 0;
        
        PIGetter()
        virtual iModelMeshArray* GetMeshes() const = 0;
        
        PISetter()
        virtual void SetMeshes(iModelMeshArray* meshes) = 0;
        
        PIGetter()
        virtual iModelSkinArray* GetSkins() const = 0;
        
        PISetter()
        virtual void SetSkins(iModelSkinArray* skins) = 0;
        
        PIMethod()
        virtual mat4 EvaluateGlobalMatrix() const = 0;
    };
    
    iModelNode* CreateModelNode();
    
    PI_DEFINE_OBJECT_ARRAY(iModelNode);
    iModelNodeArray* CreateModelNodeArray();
    
}



#endif






