/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.23   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_SYSTEM_H
#define PI_GAME_SYSTEM_H

namespace nspi
{
    struct iScene;
    
    PIInterface(HasCreator = false)
    struct iSystem : public iRefObject
    {
        PI_DEFINE(iSystem)
        
        virtual void SetScene(iScene* scene) = 0;
        
        PIMethod()
        virtual iEntityArray* GetEntities() const = 0;
        
        PIMethod()
        virtual iScene* GetScene() const = 0;
        
        PIMethod()
        virtual void AddEntity(iEntity* entity) = 0;
        
        PIMethod()
        virtual void RemoveEntity(iEntity* entity) = 0;
        
        virtual bool Accept(iEntity* entity) = 0;
        
        /**
         * ---------------------------------------------------------------------------
         * Callback
         */
        
        virtual void OnResize(const rect& bounds) = 0;
        
        virtual bool OnMessage(iMessage* message) = 0;
        
        virtual bool OnHIDEvent(iHIDEvent* event) = 0;
        
        virtual void OnBeforeUpdate(float delta) = 0;
        
        virtual void OnUpdate(float delta) = 0;

        virtual void OnAfterUpdate(float delta) = 0;
        
        virtual void OnLoad() = 0;
        
        virtual void OnUnload() = 0;
    };
    
    PI_DEFINE_OBJECT_ARRAY(iSystem);
    iSystemArray* CreateSystemArray();
    
}

#endif






















