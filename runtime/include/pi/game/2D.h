/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.27   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_2D_H
#define PI_GAME_2D_H

#include <pi/game/2d/Quad.h>
#include <pi/game/2d/FrameAnim2D.h>
#include <pi/game/2d/Material2D.h>

#endif
