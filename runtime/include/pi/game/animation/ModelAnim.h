/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.3   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_ANIMATION_MODELANIM_H
#define PI_GAME_ANIMATION_MODELANIM_H

namespace nspi
{
    
    /**
     * ---------------------------------------------------------------------------------------------
     * Animantion Channel
     */

    PIInterface()
    struct iAnimChannel : public iRefObject
    {
        PI_DEFINE(iAnimChannel)
        
        PIGetter()
        virtual iAnimCurveKeyArray* GetKeys() const = 0;
        
        PISetter()
        virtual void SetKeys(iAnimCurveKeyArray* keys) = 0;
        
        PISetter()
        virtual void SetTarget(iRefObject* target) = 0;
        
        PIGetter()
        virtual iRefObject* GetTarget() const = 0;
        
        PISetter()
        virtual void SetProperty(iProperty* prop) = 0;
        
        PIGetter()
        virtual iProperty* GetProperty() const = 0;
    };
    
    iAnimChannel* CreateAnimChannel();
    
    PI_DEFINE_OBJECT_ARRAY(iAnimChannel);
    
    iAnimChannelArray* CreateAnimChannelArray();
    
    
    /**
     * ---------------------------------------------------------------------------------------------
     * Animantion Layer
     */
    
    PIInterface()
    struct iAnimLayer : public iRefObject
    {
        PI_DEFINE(iAnimLayer)
        
        PIGetter()
        virtual std::string GetName() const = 0;
        
        PISetter()
        virtual void SetName(const std::string& name) = 0;
        
        PIGetter()
        virtual iAnimChannelArray* GetChannels() const = 0;
        
        PISetter()
        virtual void SetChannels(iAnimChannelArray* channels) = 0;
    };
    
    iAnimLayer* CreateAnimLayer();
    
    PI_DEFINE_OBJECT_ARRAY(iAnimLayer);
    
    iAnimLayerArray* CreateAnimLayerArray();
    
    
    /**
     * ---------------------------------------------------------------------------------------------
     * Animantion
     */
    
    PIInterface()
    struct iModelAnim : public iRefObject
    {
        PI_DEFINE(iModelAnim)
        
        PISetter()
        virtual void SetName(const std::string& name) = 0;
        
        PIGetter()
        virtual std::string GetName() const = 0;
        
        PISetter()
        virtual void SetLayers(iAnimLayerArray* layers) = 0;
        
        PIGetter()
        virtual iAnimLayerArray* GetLayers() const = 0;
        
        /**
         * Callbacks
         */
        
        virtual void OnUpdate(float delta) = 0;
    };
    
    iModelAnim* CreateModelAnim();
    
    PI_DEFINE_OBJECT_ARRAY(iModelAnim);
    
    iModelAnimArray* CreateModelAnimArray();
    
}

#endif




















