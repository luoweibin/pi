/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.3   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_ANIMATION_ANIMCURVE_H
#define PI_GAME_ANIMATION_ANIMCURVE_H

namespace nspi
{
    PIEnum()
    enum eInterpMode
    {
        eInterpMode_Constant    = 0,
        eInterpMode_Linear      = 1,
        eInterpMode_Cubic       = 2,
    };
    
    PIEnum()
    enum eTangentMode
    {
        // Auto (Spline cardinal)
        eTangentMode_Auto   = 0,
        
        // Spline TCB (Tension, Continuity, Bias)
        eTangentMode_TCB    = 1,
    };
    
    PIEnum()
    enum eTangentOption
    {
        eTangentOption_Break = 0,
    };
    
    PIInterface()
    struct iAnimCurveKey : public iRefObject
    {
        PI_DEFINE(iAnimCurveKey)
        
        PIGetter()
        virtual Var GetValue() const = 0;
        
        PISetter()
        virtual void SetValue(const Var& value) = 0;
        
        // interpolation
        PIGetter()
        virtual int GetInterpMode() const = 0;
        
        PISetter()
        virtual void SetInterpMode(int mode) = 0;
        
        PIGetter()
        virtual int GetTangentMode() const = 0;
        
        PISetter()
        virtual void SetTangentMode(int mode) = 0;
        
        PIGetter()
        virtual int GetTangentOption() const = 0;
        
        PISetter()
        virtual void SetTangentOption(int option) = 0;
        
        PIGetter()
        virtual float GetLeftTangent() const = 0;
        
        PISetter()
        virtual void SetLeftTangent(float value) = 0;
        
        PIGetter()
        virtual float GetRightTangent() const = 0;
      
        PISetter()
        virtual void SetRightTangent(float value) = 0;
        
        PIGetter()
        virtual float GetTension() const = 0;
        
        PISetter()
        virtual void SetTension(float value) = 0;
        
        PISetter()
        virtual void SetTime(int64_t ms) = 0;
        
        PIGetter()
        virtual int64_t GetTime() const = 0;
    };
    
    iAnimCurveKey* CreateAnimCurveKey();
    
    PI_DEFINE_OBJECT_ARRAY(iAnimCurveKey);
    
    iAnimCurveKeyArray* CreateAnimCurveKeyArray();
    
    
    PIInterface()
    struct iAnimCurve : public iRefObject
    {
        PI_DEFINE(iAnimCurve)
        
        PIGetter()
        virtual iAnimCurveKeyArray* GetKeys() const = 0;
        
        PISetter()
        virtual void SetKeys(iAnimCurveKeyArray* keys) = 0;
    };
    
    iAnimCurve* CreateAnimCurve();

    PI_DEFINE_OBJECT_ARRAY(iAnimCurve);
    
    iAnimCurveArray* CreateAnimCurveArray();
    
    
    
    PIEnum()
    enum eInterp
    {
        eInterp_Const,
        eInterp_Step,
        eInterp_Linear,
        eInterp_Cosine,
        eInterp_SmoothStep,
        eInterp_Accel,
        eInterp_Decel,
    };
    
    
    PIInterface(HasCreator = false)
    struct iInterp : public iRefObject
    {
        PI_DEFINE(iInterp)
        
        /**
         * time range in [0, 1]
         */
        PIMethod()
        virtual Var Evaluate(float time) = 0;
    };

    
    
    PIInterface()
    struct iLerpInterp : public iInterp
    {
        PI_DEFINE(iLerpInterp)
        
        PIGetter()
        virtual Var GetLeft() const = 0;
        
        PISetter()
        virtual void SetLeft(const Var& value) = 0;
        
        PIGetter()
        virtual Var GetRight() const = 0;
        
        PISetter()
        virtual void SetRight(const Var& value) = 0;
        
        PIGetter(Class = eInterp)
        virtual int GetMode() const = 0;
        
        PISetter()
        virtual void SetMode(int value) = 0;
    };
    
    iLerpInterp* CreateLerpInterp();
    
    
    PIInterface()
    struct iSeqInterp : public iInterp
    {
        PI_DEFINE(iSeqInterp)
        
        PIGetter()
        virtual std::string GetUri() const = 0;
        
        PISetter()
        virtual void SetUri(const std::string& uri) = 0;
    
        PIGetter()
        virtual iAnimSeq* GetSeq() const = 0;
        
        PISetter(AssetUri = "Uri")
        virtual void SetSeq(iAnimSeq* seq) = 0;
    };
    
    iSeqInterp* CreateSeqInterp();
}

#endif




























