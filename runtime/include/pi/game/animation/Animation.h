/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.27   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_ANIMATION_ANIMATION_H
#define PI_GAME_ANIMATION_ANIMATION_H


namespace nspi
{

    PIInterface()
    struct iAnimKey : public iRefObject
    {
        PI_DEFINE(iAnimKey)
        
        PIGetter()
        virtual float GetTime() const = 0;
        
        PISetter()
        virtual void SetTime(float time) = 0;
        
        PIGetter()
        virtual Var GetLeft() const = 0;
        
        PISetter()
        virtual void SetLeft(const Var& value) = 0;
        
        PIGetter()
        virtual Var GetRight() const = 0;
        
        PISetter()
        virtual void SetRight(const Var& value) = 0;
        
        PIGetter(Class = eInterp)
        virtual int GetMode() const = 0;
        
        PISetter()
        virtual void SetMode(int value) = 0;
    };
    
    iAnimKey* CreateAnimKey();

    PI_DEFINE_OBJECT_ARRAY(iAnimKey);
    iAnimKeyArray* CreateAnimKeyArray();
    
    
    PIInterface(HasCreator = false)
    struct iAnimator : public iRefObject
    {
        PI_DEFINE(iAnimator)
        
        PIGetter()
        virtual std::string GetComp() const = 0;
        
        PISetter()
        virtual void SetComp(const std::string& name) = 0;
        
        PIGetter()
        virtual std::string GetProperty() const = 0;
        
        PISetter()
        virtual void SetProperty(const std::string& name) = 0;
        
        PIMethod()
        virtual void OnUpdate(iEntity* entity, iAnimKey* leftKey, iAnimKey* rightKey, float delta) = 0;
    };
    
    PIInterface()
    struct iCompAnimator : public iAnimator
    {
        PI_DEFINE(iCompAnimator)
    };
    
    iCompAnimator* CreateCompAnimator();
    
    
    PIInterface()
    struct iMaterialAnimator : public iAnimator
    {
        PI_DEFINE(iMaterialAnimator)
    };
    
    iMaterialAnimator* CreateMaterialAnimator();
    
    PIInterface()
    struct iAnimTrack : public iRefObject
    {
        PI_DEFINE(iAnimTrack)
        
        PIGetter()
        virtual iAnimKeyArray* GetKeys() const = 0;
        
        PISetter()
        virtual void SetKeys(iAnimKeyArray* keys) = 0;
        
        PIGetter()
        virtual float GetBegin() const = 0;
        
        PISetter()
        virtual void SetBegin(float value) = 0;
        
        PIMethod()
        virtual float GetEnd() const = 0;
        
        PIGetter()
        virtual bool IsRepeat() const = 0;
        
        PISetter()
        virtual void SetRepeat(bool value) = 0;
        
        PIGetter()
        virtual iAnimator* GetAnimator() const = 0;
        
        PISetter()
        virtual void SetAnimator(iAnimator* animator) = 0;
    };
    
    iAnimTrack* CreateAnimTrack();
    
    PI_DEFINE_OBJECT_ARRAY(iAnimTrack);
    iAnimTrackArray* CreateAnimTrackArray();
    
    PIInterface()
    struct iAnimation : public iAsset
    {
        PI_DEFINE(iAnimation)
        
        PIGetter()
        virtual bool IsRepeat() const = 0;
        
        PISetter()
        virtual void SetRepeat(bool value) = 0;
        
        PISetter()
        virtual void SetTracks(iAnimTrackArray* tracks) = 0;
        
        PIGetter()
        virtual iAnimTrackArray* GetTracks() const = 0;
        
        PIMethod()
        virtual void OnUpdate(iEntity* entity, float delta) = 0;
    };
    
    iAnimation* CreateAnimation();
    
    
    PIInterface()
    struct iAnimationComp : public iComponent
    {
        PI_DEFINE(iAnimationComp)
        
        PIGetter()
        virtual std::string GetUri() const = 0;
        
        PISetter()
        virtual void SetUri(const std::string& uri) = 0;
        
        PIGetter()
        virtual iAnimation* GetAnimation() const = 0;
        
        PISetter(AssetUri = "Uri")
        virtual void SetAnimation(iAnimation* animation) = 0;
    };
    
    iAnimationComp* CreateAnimationComp();
    
    
    
    
    PIInterface()
    struct iAnimSeqComp : public iComponent
    {
        PI_DEFINE(iAnimSeqComp)
        
        PIGetter()
        virtual float GetDuration() const = 0;
        
        PISetter()
        virtual void SetDuration(float duration) = 0;
        
        PIGetter()
        virtual bool IsRepeat() const = 0;
        
        PISetter()
        virtual void SetRepeat(bool value) = 0;
        
        PIGetter()
        virtual std::string GetUri() const = 0;
        
        PISetter()
        virtual void SetUri(const std::string& uri) = 0;
        
        PIGetter()
        virtual iAnimSeq* GetSeq() const = 0;
        
        PISetter(AssetUri = "Uri")
        virtual void SetSeq(iAnimSeq* seq) = 0;
        
        PIGetter()
        virtual std::string GetProperty() const = 0;
        
        PISetter()
        virtual void SetProperty(const std::string& name) = 0;
    };
    
    iAnimSeqComp* CreateAnimSeqComp();
}


#endif



































