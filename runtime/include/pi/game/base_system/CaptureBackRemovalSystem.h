/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 Sword     2017.7.5   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_BASESYSTEM_CAPTUREBACKREMOVALSYSTEM_H
#define PI_GAME_BASESYSTEM_CAPTUREBACKREMOVALSYSTEM_H

namespace nspi
{
    PIInterface()
    struct iCaptureBackRemovalSystem : public iSystem
    {
        PI_DEFINE(iCaptureBackRemovalSystem)
    };
    
    iCaptureBackRemovalSystem* CreateCaptureBackRemovalSystem();
}

#endif
