/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.11.21   0.1     Create
 ******************************************************************************/

#ifndef PARTICLE_SHADER_H
#define PARTICLE_SHADER_H
namespace nspi
{
    static const char g_Particle_VS_GLES2[] =
    "precision highp float;\n"
    "attribute vec4 position;\n"
    "attribute vec2 uv;\n"
    "attribute vec4 jointIndices;\n"
    "attribute vec4 jointWeights;\n"
    "attribute vec3 normal;\n"
    "varying vec2 uv0;\n"
    "uniform mat4 MVPMatrix;\n"
    "void main(void)\n"
    "{\n"
        "gl_Position = MVPMatrix * position;\n"
        "uv0 = uv.st;\n"
    "}\n";
    
    static const char g_Particle_FS_GLES2[] =
    "//Particle_FS_GLES2\n"
    "precision highp float;\n"
    "varying vec2 uv0;\n"
    "uniform sampler2D frame;\n"
    "void main(void)\n"
    "{\n"
    "    vec4 color = texture2D(frame, uv0);\n"
    "    gl_FragColor = color;\n"
    "}\n";
}
#endif
