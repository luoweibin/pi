/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 Sword 2017.5.15   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_PARTICLE_PARTICLELIB_H
#define PI_GAME_PARTICLE_PARTICLELIB_H

namespace nspi
{
    PIEnum()
    enum eParticleType
    {
        eParticleType_Quad  = 0,
        eParticleType_Mesh  = 1,
        eParticleType_Count,
    };
    
    PIEnum()
    enum eParticlePlayType
    {
        eParticlePlayType_ByFrame  = 0,
        eParticlePlayType_ByTime  = 1,
        eParticlePlayType_Count,
    };
    
    PIEnum()
    enum eParticleBlend
    {
        eParticleBlend_Add,
        eParticleBlend_Blend,
        eParticleBlend_Count,
    };
    
    PIEnum()
    enum eBillboardType
    {
        eBillboardType_Y,
        eBillboardType_All,
        eBillboardType_None,
        eBillboardType_Count,
    };
    
    PIInterface(Alias = ParticleEmitterLib)
    struct iParticleEmitterLib : public iAsset
    {
        PI_DEFINE(iParticleEmitterLib);
        
        PISetter()
        virtual void SetParticleType(int type) = 0;
        
        PIGetter(Class = eParticleType)
        virtual int GetParticleType() const = 0;
        
        PISetter()
        virtual void SetParticlePlayType(int type) = 0;
        
        PIGetter(Class = eParticlePlayType)
        virtual int GetParticlePlayType() const = 0;
        
        PISetter()
        virtual void SetParticleBlend(int type) = 0;
        
        PIGetter(Class = eParticleBlend)
        virtual int GetParticleBlend() const = 0;
        
        PISetter()
        virtual void SetEmissionRate(float rate) = 0;
        
        PIGetter()
        virtual float GetEmissionRate() const = 0;
        
        PISetter()
        virtual void SetMaxNum(int MaxNum) = 0;
        
        PIGetter()
        virtual int GetMaxNum() const = 0;
        
        PISetter()
        virtual void SetWidth(float width) = 0;
        
        PIGetter()
        virtual float GetWidth() const = 0;
        
        PISetter()
        virtual void SetHeight(float height) = 0;
        
        PIGetter()
        virtual float GetHeight() const = 0;
        
        PISetter()
        virtual void SetTextureUri(const std::string& uri) = 0;
        
        PIGetter()
        virtual std::string GetTextureUri() const = 0;
        
        PISetter(AssetUri = "TextureUri")
        virtual void SetTexture2D(iTexture2D* tex) = 0;
        
        PIGetter()
        virtual iTexture2D* GetTexture2D() const = 0;
        
        PISetter()
        virtual void SetFrameSizeX(int pitch) = 0;
        
        PIGetter()
        virtual int GetFrameSizeX() const = 0;

        PISetter()
        virtual void SetFrameSizeY(int pitch) = 0;
        
        PIGetter()
        virtual int GetFrameSizeY() const = 0;
        
        PISetter()
        virtual void SetFrameCount(int count) = 0;
        
        PIGetter()
        virtual int GetFrameCount() const = 0;
        
        PISetter()
        virtual void SetFrameDuration(float duration) = 0;
        
        PIGetter()
        virtual float GetFrameDuration() const = 0;
        
        PISetter()
        virtual void SetBillboardType(int type) = 0;
        
        PIGetter(Class = eBillboardType)
        virtual int  GetBillboardType() const = 0;

        PISetter()
        virtual void SetBornBasePosition(const vec3& pos) = 0;
        
        PIGetter()
        virtual vec3 GetBornBasePosition() const = 0;
        
        PISetter()
        virtual void SetPositionOffsetAbs(bool flag) = 0;
        
        PIGetter()
        virtual bool IsPositionOffsetAbs() const = 0;

        PISetter()
        virtual void SetBornPositionMinOffset(const vec3& pos) = 0;
        
        PIGetter()
        virtual vec3 GetBornPositionMinOffset() const = 0;
        
        PISetter()
        virtual void SetBornPositionMaxOffset(const vec3& pos) = 0;
        
        PIGetter()
        virtual vec3 GetBornPositionMaxOffset() const = 0;
        
        PISetter()
        virtual void SetBornBaseVelocity(const vec3& v) = 0;
        
        PIGetter()
        virtual vec3 GetBornBaseVelocity() const = 0;
        
        PISetter()
        virtual void SetVelocityOffsetAbs(bool flag) = 0;
        
        PIGetter()
        virtual bool IsVelocityOffsetAbs() const = 0;
        
        PISetter()
        virtual void SetBornVelocityMinOffset(const vec3& v) = 0;
        
        PIGetter()
        virtual vec3 GetBornVelocityMinOffset() const = 0;
        
        PISetter()
        virtual void SetBornVelocityMaxOffset(const vec3& v) = 0;
        
        PIGetter()
        virtual vec3 GetBornVelocityMaxOffset() const = 0;
        
        PISetter()
        virtual void SetBaseAcceleration(const vec3& acc) = 0;
        
        PIGetter()
        virtual vec3 GetBaseAcceleration() const = 0;
        
        PISetter()
        virtual void SetAccelerationOffsetAbs(bool flag) = 0;
        
        PIGetter()
        virtual bool IsAccelerationOffsetAbs() const = 0;
        
        PISetter()
        virtual void SetAccelerationMinOffset(const vec3& acc) = 0;
        
        PIGetter()
        virtual vec3 GetAccelerationMinOffset() const = 0;
        
        PISetter()
        virtual void SetAccelerationMaxOffset(const vec3& acc) = 0;
        
        PIGetter()
        virtual vec3 GetAccelerationMaxOffset() const = 0;
        
        PISetter()
        virtual void SetBornBaseRotation(const vec3& rotation) = 0;
        
        PIGetter()
        virtual vec3 GetBornBaseRotation() const = 0;
        
        PISetter()
        virtual void SetRotationOffsetAbs(bool flag) = 0;
        
        PIGetter()
        virtual bool IsRotationOffsetAbs() const = 0;
        
        PISetter()
        virtual void SetBornRotationMinOffset(const vec3& rotation) = 0;
        
        PIGetter()
        virtual vec3 GetBornRotationMinOffset() const = 0;
        
        PISetter()
        virtual void SetBornRotationMaxOffset(const vec3& rotation) = 0;
        
        PIGetter()
        virtual vec3 GetBornRotationMaxOffset() const = 0;
        
        PISetter()
        virtual void SetBornBaseRotationRate(const vec3& rotationRate) = 0;
        
        PIGetter()
        virtual vec3 GetBornBaseRotationRate() const = 0;
        
        PISetter()
        virtual void SetRotationRateOffsetAbs(bool flag) = 0;
        
        PIGetter()
        virtual bool IsRotationRateOffsetAbs() const = 0;
        
        PISetter()
        virtual void SetBornRotationRateMinOffset(const vec3& rotationRate) = 0;
        
        PIGetter()
        virtual vec3 GetBornRotationRateMinOffset() const = 0;
        
        PISetter()
        virtual void SetBornRotationRateMaxOffset(const vec3& rotationRate) = 0;
        
        PIGetter()
        virtual vec3 GetBornRotationRateMaxOffset() const = 0;
        
        PISetter()
        virtual void SetBaseRotationalAcceleration(const vec3& rotationalAcceleration) = 0;
        
        PIGetter()
        virtual vec3 GetBaseRotationalAcceleration() const = 0;
        
        PISetter()
        virtual void SetRotationalAccelerationOffsetAbs(bool flag) = 0;
        
        PIGetter()
        virtual bool IsRotationalAccelerationOffsetAbs() const = 0;
        
        PISetter()
        virtual void SetRotationalAccelerationMinOffset(const vec3& rotationalAcceleration) = 0;
        
        PIGetter()
        virtual vec3 GetRotationalAccelerationMinOffset() const = 0;
        
        PISetter()
        virtual void SetRotationalAccelerationMaxOffset(const vec3& rotationalAcceleration) = 0;
        
        PIGetter()
        virtual vec3 GetRotationalAccelerationMaxOffset() const = 0;
        
        PISetter()
        virtual void SetBaseLife(float sec) = 0;
        
        PIGetter()
        virtual float GetBaseLife() const = 0;
        
        PISetter()
        virtual void SetLifeMinOffset(float sec) = 0;
        
        PIGetter()
        virtual float GetLifeMinOffset() const = 0;
        
        PISetter()
        virtual void SetLifeMaxOffset(float sec) = 0;
        
        PIGetter()
        virtual float GetLifeMaxOffset() const = 0;
        
        PIMethod()
        virtual bool Init() = 0;
    };
    
    iParticleEmitterLib* CreateParticleEmitterLib();
    
    PIInterface(HasCreator = false)
    struct iParticle : public iRefObject
    {
        PI_DEFINE(iParticle);
        
        PIGetter()
        virtual float GetLifeTime() const = 0;
        
        PISetter()
        virtual void SetLifeTime(float time) = 0;
        
        PIGetter()
        virtual float GetPassTime() const = 0;
        
        PISetter()
        virtual void SetPassTime(float time) = 0;
        
        PIGetter()
        virtual bool IsAlive() const = 0;
        
        PISetter()
        virtual void SetAlive(bool flag) = 0;
        
        PIGetter()
        virtual vec3 GetPosition() const = 0;
        
        PISetter()
        virtual void SetPosition(const vec3& pos) = 0;
        
        PIGetter()
        virtual vec3 GetRotation() const = 0;
        
        PISetter()
        virtual void SetRotation(const vec3& rotation) = 0;
        
        PIGetter()
        virtual vec3 GetVelocity() const = 0;
        
        PISetter()
        virtual void SetVelocity(const vec3& velocity) = 0;
        
        PIGetter()
        virtual vec3 GetAcceleration() const = 0;
        
        PISetter()
        virtual void SetAcceleration(const vec3& acceleration) = 0;
        
        PIGetter()
        virtual vec3 GetRotationRate() const = 0;
        
        PISetter()
        virtual void SetRotationRate(const vec3& rotationRate) = 0;
        
        PIGetter()
        virtual vec3 GetRotationalAcceleration() const = 0;
        
        PISetter()
        virtual void SetRotationalAcceleration(const vec3& rotationalAcceleration) = 0;
    };
    
    PIInterface()
    struct iQuadParticle : public iParticle
    {
        PI_DEFINE(iQuadParticle);
        
        PIGetter()
        virtual int GetFrameNum() const = 0;
        
        PISetter()
        virtual void SetFrameNum(int frameNum) = 0;
        
        PIGetter()
        virtual float GetDuration() const = 0;
        
        PISetter()
        virtual void SetDuration(float duration) = 0;
    };
    
    iQuadParticle* CreateQuadParticle();
}

#endif
