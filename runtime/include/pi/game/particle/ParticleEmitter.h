/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 Sword 2017.5.16   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_PARTICLE_PARTICLEEMITTER_H
#define PI_GAME_PARTICLE_PARTICLEEMITTER_H

namespace nspi
{
    PIInterface(Alias = ParticleEmitter)
    struct iParticleEmitter : public iComponent
    {
        PI_DEFINE(iParticleEmitter)
        
        PISetter()
        virtual void SetUri(const std::string& uri) = 0;
        
        PIGetter()
        virtual std::string GetUri() const = 0;
        
        PISetter(AssetUri = "Uri")
        virtual void SetLibrary(iParticleEmitterLib* lib) = 0;
        
        PIGetter()
        virtual iParticleEmitterLib* GetLibrary() const = 0;
        
        PIMethod()
        virtual void Init() = 0;
        
        PIMethod()
        virtual void OnUpdate(float delta) = 0;
        
        virtual void DoRender( iCamera* camera, iTransform* camTransform) = 0;
    };
    
    iParticleEmitter* CreateParticleEmitter();
}

#endif
