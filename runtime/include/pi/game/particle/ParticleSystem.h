/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 Sword 2017.5.15   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_PARTICLE_PARTICLESYSTEM_H
#define PI_GAME_PARTICLE_PARTICLESYSTEM_H

namespace nspi
{
    PIInterface()
    struct iParticleSystem : public iSystem
    {
        PI_DEFINE(iParticleSystem)
    };
    
    iParticleSystem* CreateParticleSystem();
}

#endif
