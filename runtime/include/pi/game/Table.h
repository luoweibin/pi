/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.24   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_TABLE_H
#define PI_GAME_TABLE_H


namespace nspi
{

    PIInterface()
    struct iTable : public iComponent
    {
        PI_DEFINE(iTable)
        
        PIMethod()
        virtual void Set(const std::string& key, const Var& value) = 0;
        
        PIMethod()
        virtual Var Get(const std::string& key) const = 0;
        
        PIMethod()
        virtual iStringArray* GetKeys() const = 0;
    };
    
    iTable* CreateTable();
}




#endif


















