/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.27   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_SCRIPTING_H
#define PI_GAME_SCRIPTING_H

namespace nspi
{
    
    PIInterface()
    struct iScriptSystem : public iSystem
    {
        PI_DEFINE(iScriptSystem)
        
        PIGetter()
        virtual std::string GetUri() const = 0;
        
        PISetter()
        virtual void SetUri(const std::string& uri) = 0;
    };
    
    iScriptSystem* CreateScriptSystem();

}

#endif






















