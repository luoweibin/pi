/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 archie zhou    2017.3.16   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_2D_QUAD_H
#define PI_GAME_2D_QUAD_H

namespace nspi
{
    PIInterface()
    struct iQuad : public iComponent
    {
        PI_DEFINE(iQuad)
        
        PIGetter()
        virtual std::string GetUri() const = 0;
        
        PISetter()
        virtual void SetUri(const std::string& uri) = 0;
    };
    
    iQuad* CreateQuad();
}

#endif
