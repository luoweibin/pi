/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.27   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_2D_FRAMEANIM2D_H
#define PI_GAME_2D_FRAMEANIM2D_H

namespace nspi
{
    
    /**
     * -------------------------------------------------------------------------
     * Library
     */
    struct iMaterial2DArray;
    
    PIInterface()
    struct iFrameAnim2DLib : public iAsset
    {
        PI_DEFINE(iFrameAnim2DLib)
        
        PIGetter()
        virtual int32_t GetFPS() const = 0;
        
        PISetter()
        virtual void SetFPS(int32_t fps) = 0;
        
        PIMethod()
        virtual bool IsCache() const = 0;
        
        PISetter()
        virtual void SetCache(bool value) = 0;
        
        PIGetter()
        virtual iMaterial2DArray* GetFrames() const = 0;
        
        PISetter()
        virtual void SetFrames(iMaterial2DArray* frames) = 0;
    };
    
    iFrameAnim2DLib* CreateFrameAnim2DLib();


    
    PIInterface()
    struct iFrameAnim2D : public iAnimComp
    {
        PI_DEFINE(iFrameAnim2D)
        
        
        /**
         * Properties
         */
        
        PIGetter()
        virtual int32_t GetFrame() const = 0;
        
        PISetter()
        virtual void SetFrame(int32_t index) = 0;
        
        PIGetter()
        virtual std::string GetUri() const = 0;
        
        PISetter()
        virtual void SetUri(const std::string& uri) = 0;
        
        PIGetter()
        virtual int32_t GetFPS() const = 0;
        
        PISetter()
        virtual void SetFPS(int32_t value) = 0;
        
        /**
         * Methods
         */
        
        PISetter(AssetUri = "Uri")
        virtual void SetLibrary(iFrameAnim2DLib* lib) = 0;
        
        PIGetter()
        virtual iFrameAnim2DLib* GetLibrary() const = 0;
    };
    
    iFrameAnim2D* CreateFrameAnim2D();
}

#endif





















