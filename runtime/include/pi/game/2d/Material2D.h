/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.27   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_2D_MATERIAL2D_H
#define PI_GAME_2D_MATERIAL2D_H

namespace nspi
{
    
    PIInterface()
    struct iMaterial2D : public iComponent
    {
        PI_DEFINE(iMaterial2D)
        
        PIGetter()
        virtual vec4 GetColor() const = 0;
        
        PISetter()
        virtual void SetColor(const vec4& color) = 0;
        
        PIGetter()
        virtual std::string GetColorTexUri() const = 0;
        
        PISetter()
        virtual void SetColorTexUri(const std::string& uri) = 0;
        
        PIGetter()
        virtual std::string GetMaskTexUri() const = 0;
        
        PISetter()
        virtual void SetMaskTexUri(const std::string& uri) = 0;
        
        PISetter()
        virtual void SetTexTransform(const mat4& matrix) = 0;
        
        PIGetter()
        virtual mat4 GetTexTransform() const = 0;
        
        PIGetter()
        virtual std::string GetScript() const = 0;
        
        PISetter()
        virtual void SetScript(const std::string& uri) = 0;
        
        PIGetter()
        virtual iGraphicsObject* GetColorTex() const = 0;
        
        PISetter(AssetUri = "ColorTexUri")
        virtual void SetColorTex(iGraphicsObject* texture) = 0;
        
        PIGetter()
        virtual iGraphicsObject* GetMaskTex() const = 0;
        
        PISetter(AssetUri = "MaskTexUri")
        virtual void SetMaskTex(iGraphicsObject* texture) = 0;
    };
    
    iMaterial2D* CreateMaterial2D();
    
    PI_DEFINE_OBJECT_ARRAY(iMaterial2D);
    iMaterial2DArray* CreateMaterial2DArray();
    
}

#endif

























