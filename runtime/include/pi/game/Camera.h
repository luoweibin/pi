/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2013.3.31   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_CAMERA_H
#define PI_GAME_CAMERA_H

namespace nspi
{
    PIEnum()
    enum eClearFlag
    {
        eClearFlag_Color,
        eClearFlag_DepthOnly,
        eClearFlag_None,
    };
    
    PIEnum()
    enum eCameraMode
    {
        eCameraMode_Ortho = 0,
        eCameraMode_Persp = 1,
    };
    
    PIEnum()
    enum eCameraFollowType
    {
        eCameraFollowType_Free          = 0,
        eCameraFollowType_Motion        = 1,
    };
    
    PIInterface()
    struct iCamera : public iComponent
    {
        PI_DEFINE(iCamera)
        
        PISetter()
        virtual void SetActive(bool value) = 0;
        
        PIGetter()
        virtual bool IsActive() const = 0;
        
        PISetter()
        virtual void SetUp(const vec3& value) = 0;
        
        PIGetter()
        virtual vec3 GetUp() const = 0;
        
        PISetter()
        virtual void SetMode(int mode) = 0;
        
        PIGetter(Class = eCameraMode)
        virtual int GetMode() const = 0;
        
        PISetter(Class = eTag)
        virtual void SetDrawTag(int tag) = 0;

        PIGetter()
        virtual int GetDrawTag() const = 0;
        
        PISetter()
        virtual void SetFov(float fovAngle) = 0;
        
        PIGetter()
        virtual float GetFov() const = 0;
        
        PISetter()
        virtual void SetNear(float value) = 0;
        
        PIGetter()
        virtual float GetNear() const = 0;
        
        PISetter()
        virtual void SetFar(float value) = 0;
        
        PIGetter()
        virtual float GetFar() const = 0;
        
        PIGetter()
        virtual std::string GetRenderTargetUri() const = 0;
        
        PISetter()
        virtual void SetRenderTargetUri(const std::string& uri) = 0;
        
        PISetter(AssetUri = "RenderTargetUri")
        virtual void SetRenderTarget(iRenderTexture* renderTarget) = 0;
        
        PIGetter()
        virtual iRenderTexture* GetRenderTarget() const = 0;
        
        PISetter()
        virtual void SetCameraFollowType(int type) = 0;
        
        PIGetter(Class = eCameraFollowType)
        virtual int GetCameraFollowType() const = 0;
        
        PISetter()
        virtual void SetClearFlag(int flag) = 0;
        
        PIGetter(Class = eClearFlag)
        virtual int GetClearFlag() const = 0;
        
        PISetter()
        virtual void SetClearColor(const vec4& color) = 0;
        
        PIGetter()
        virtual vec4 GetClearColor() const = 0;
        
        PIMethod()
        virtual void OnPrepareRender() = 0;
        
        PIMethod()
        virtual void OnFinishRender() = 0;
    };
    
    iCamera* CreateCamera();
}

#endif





























