/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.27   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_FRAMECAPTURE_H
#define PI_GAME_FRAMECAPTURE_H

namespace nspi
{
    PIInterface()
    struct iFrameCaptureSystem : public iSystem
    {
        PI_DEFINE(iFrameCaptureSystem)
    };
    
    iFrameCaptureSystem* CreateFrameCaptureSystem();
}

#endif





























