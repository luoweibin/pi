/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.12.2   1.0     Create
 ******************************************************************************/
#ifndef PI_GAME_IMPL_SYSTEMIMPL_H
#define PI_GAME_IMPL_SYSTEMIMPL_H

#include <pi/Game.h>

namespace nspi
{
    
    template <class T>
    class SystemImpl : public T
    {
    protected:
        SmartPtr<iEntityArray> mEntities;
        
        iScene* mScene;
        
    public:
        SystemImpl():
        mScene(nullptr)
        {
            mEntities = CreateEntityArray();
        }
        
        virtual ~SystemImpl()
        {
        }
        
        virtual iEntityArray* GetEntities() const
        {
            return mEntities;
        }
        
        virtual void SetScene(iScene* scene)
        {
            piAssert(scene != nullptr, ;);
            mScene = scene;
        }
        
        virtual iScene* GetScene() const
        {
            return mScene;
        }
        
        virtual void AddEntity(iEntity* entity)
        {
            piAssert(entity != nullptr, ;);
            
            auto index = piIndexOf<iEntityArray, iEntity*>(mEntities, entity);
            piAssert(index < 0, ;);
            
            OnAddEntity(entity);
            
            mEntities->PushBack(entity);
        }
        
        virtual void RemoveEntity(iEntity* entity)
        {
            piAssert(entity != nullptr, ;);
            
            auto index = piIndexOf<iEntityArray, iEntity*>(mEntities, entity);
            piCheck(index >= 0, ;);
            
            OnRemoveEntity(entity);
            
            mEntities->Remove(index);
        }
        
        virtual void OnLoad()
        {
        }
        
        virtual void OnUnload()
        {
        }
        
        virtual void OnBeforeUpdate(float delta)
        {
        }
        
        virtual void OnUpdate(float delta)
        {
        }
        
        virtual void OnAfterUpdate(float delta)
        {
        }
        
        virtual bool OnMessage(iMessage* message)
        {
            return false;
        }
        
        virtual void OnResize(const rect& bounds)
        {
        }
        
        virtual bool OnHIDEvent(iHIDEvent* event)
        {
            return false;
        }
        
    protected:
        
        rect GetBounds() const
        {
            return mScene->GetBounds();
        }
        
        iEntity* GetFirstActiveCameraEntity() const
        {
            return mScene->GetFirstActiveCameraEntity();
        }
        
        iScript* GetScript() const
        {
            return mScene->GetScript();
        }
        
        iClassLoader* GetClassLoader() const
        {
            return mScene->GetClassLoader();
        }
        
        iAssetManager* GetAssetManager() const
        {
            return mScene->GetAssetManager();
        }
        
        iHID* GetHID() const
        {
            return mScene->GetHID();
        }
        
        iMotionManager* GetMotionManager() const
        {
            return mScene->GetMotionManager();
        }
        
        void SwapBuffer()
        {
            return mScene->SwapBuffer();
        }
        
        int32_t GetBackBufferTex() const
        {
            return mScene->GetBackBufferTex();
        }
        
        int32_t GetFrontBufferTex() const
        {
            return mScene->GetFrontBufferTex();
        }
        
        static bool HasAll(iClassArray* sysClassArray, iClassArray* entityClassArray)
        {
            for (int32_t i = 0; i < sysClassArray->GetCount(); ++i)
            {
                SmartPtr<iClass> sysClass = sysClassArray->GetItem(i);
                
                if (!HasClass(entityClassArray, sysClass))
                {
                    return false;
                }
            }
            
            return true;
        }
        
        static bool HasAny(iClassArray* sysClassArray, iClassArray* entityClassArray)
        {
            for (int32_t i = 0; i < sysClassArray->GetCount(); ++i)
            {
                SmartPtr<iClass> sysClass = sysClassArray->GetItem(i);
                if (HasClass(entityClassArray, sysClass))
                {
                    return true;
                }
            }
            
            return false;
        }
        
        static bool HasClass(iClassArray* classArray, iClass* klass)
        {
            for (int32_t i = 0; i < classArray->GetCount(); ++i)
            {
                if (classArray->GetItem(i) == klass)
                {
                    return true;
                }
            }
            
            return false;
        }
        
        virtual void OnAddEntity(iEntity* entity)
        {
        }
        
        virtual void OnRemoveEntity(iEntity* entity)
        {
        }
    };
    
}

#endif




























