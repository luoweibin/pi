/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.12.4   1.0     Create
 ******************************************************************************/
#ifndef PI_GAME_IMPL_RENDERERIMPL_H
#define PI_GAME_IMPL_RENDERERIMPL_H

#include <pi/game/impl/SystemImpl.h>

using namespace std;
using namespace nspi;

struct RenderData
{
    SmartPtr<iGeometry> geo;
    vector<int32_t> vaos;
};

template <class T>
class RendererImpl : public SystemImpl<T>
{
protected:
    typedef vector<RenderData> RenderDataVector;
    RenderDataVector mData;
    
    typedef map<int32_t, vector<RenderData>> DataMap;
    DataMap mDataMap;
    
    using SystemImpl<T>::mEntities;
    using SystemImpl<T>::mScene;
    
public:
    RendererImpl()
    {
    }
    
    virtual ~RendererImpl()
    {
        for (auto e : mEntities)
        {
        }
    }
    
    virtual void OnUpdate(float delta)
    {
        for (auto e : mEntities)
        {
            DataMap::iterator it = mDataMap.find(e);
            if (it != mDataMap.end())
            {
                
            }
            SmartPtr<iGeometry> geo = mScene->GetCompByClass(e, iGeometry::StaticClass());
            
        }
        
        if (!geo.IsNull() && geo != mGeo)
        {
            RemoveGeometry();
            
            RenderData data;
            data.geo = geo;
            LoadGeometry(geo, data.vaos);
            mData.push_back(data);
            
            mGeo = geo;
        }
    }
    
protected:
    
    iGeometry* GetGeometry
    
    void RemoveGeometry()
    {
        for (RenderDataVector::iterator it = mData.begin(); it != mData.end(); ++it)
        {
            if (it->geo == mGeo)
            {
                for (auto vao : it->vaos)
                {
                    piReleaseGraphicsObject(vao);
                }
                
                mData.erase(it);
                break;
            }
        }
    }
        
    mat4 MakeMVPMatrix(iGameContext* context) const
    {
        mat4 MVPMatrix;
        SmartPtr<iCamera> camera = mCamera;
        if (camera.IsNull())
        {
            camera = context->GetCamera();
        }
        
        if (!camera.IsNull())
        {
            MVPMatrix = camera->GetMatrix();
        }
        
        mat4 worldMatrix = mEntity->GetWorldMatrix();
        PILOGI(PI_GAME_TAG, "[%s]world matrix:%s", mEntity->GetID().c_str(), piToString(worldMatrix).c_str());

        MVPMatrix *= mEntity->GetWorldMatrix();
        
        return MVPMatrix;
    }
    
    mat4 GetUVMatrix() const
    {
        return !mMaterial.IsNull() ? mMaterial->GetUVMatrix() : mat4();
    }
    
private:
    
    void LoadGeometry(iGeometry* geo, vector<int32_t>& vaos)
    {
        for (int32_t i = 0; i < geo->GetMeshCount(); ++i)
        {
            int32_t vao = CreateVertexArray();
            piBindVertexArray(vao);
            
            SmartPtr<iMesh> mesh = geo->GetMesh(i);
            piBindBuffer(eGraphicsBuffer_Vertex, mesh->GetVertexBuffer());
            piBindBuffer(eGraphicsBuffer_Index, mesh->GetIndexBuffer());
            
            for (int32_t i = 0; i < mesh->GetAttrCount(); ++i)
            {
                SmartPtr<iVertexAttr> attr = mesh->GetAttr(i);
                piEnableVertexAttr(attr->GetName());
                piSetVertexAttr(attr->GetName(),
                                attr->GetSize(),
                                attr->GetType(),
                                attr->GetStride(),
                                attr->GetOffset());
            }
            
            piBindVertexArray(0);
            piBindBuffer(eGraphicsBuffer_Vertex, 0);
            piBindBuffer(eGraphicsBuffer_Index, 0);
            
            vaos.push_back(vao);
        }
    }
};

#endif 















