/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.23   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_IMPL_COMPONENTIMPL_H
#define PI_GAME_IMPL_COMPONENTIMPL_H

#include <pi/Game.h>

namespace nspi
{
    template <class I>
    class ComponentImpl : public I
    {
    private:
        bool mEnabled;
        iEntity* mEntity;
        
    public:
        ComponentImpl():
        mEnabled(true), mEntity(nullptr)
        {
        }
        
        virtual ~ComponentImpl()
        {
        }
        
        virtual bool IsEnabled() const
        {
            return mEnabled;
        }
        
        virtual void SetEnabled(bool value)
        {
            piCheck(mEnabled != value, ;);
            mEnabled = value;
            OnEnabledChanged(value);
        }
        
        virtual uint64_t GetTag() const
        {
            piAssert(mEntity, 0;);
            return mEntity->GetTag();
        }
        
        virtual void SetEntity(iEntity* entity)
        {
            mEntity = entity;
        }
        
        virtual iEntity* GetEntity() const
        {
            return mEntity;
        }
        
        virtual void OnLoad()
        {
        }
        
        virtual void OnUnload()
        {
        }
        
        virtual void OnResize(const rect& bounds)
        {
        }
        
        virtual bool OnMessage(iMessage* message)
        {
            return false;
        }
        
        virtual bool OnHIDEvent(iHIDEvent* event)
        {
            return false;
        }
        
        virtual void OnUpdate(float delta)
        {
        }
        
    protected:
        virtual void OnEnabledChanged(bool value)
        {
        }
    };
    

}



#endif














