/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.12.2   1.0     Create
 ******************************************************************************/
#ifndef PI_GAME_IMPL_ASSETANIMIMPL_H
#define PI_GAME_IMPL_ASSETANIMIMPL_H

#include <pi/Game.h>

namespace nspi
{
    template<class I>
    class AnimImpl : public I
    {
    protected:
        float mDuration;
        float mLocalTime;
        int32_t mFlags;
        
    public:
        AnimImpl():
        mDuration(0.0f), mLocalTime(0), mFlags(0)
        {
        }
        
        virtual void EnableFlags(int32_t flags)
        {
            piAssert(flags > 0, ;);
            piFlagOn(mFlags, flags);
        }
        
        virtual void DisableFlags(int32_t flags)
        {
            piAssert(flags > 0, ;);
            piFlagOff(mFlags, flags);
        }
        
        virtual void SetFlags(int32_t flags)
        {
            piAssert(flags > 0, ;);
            mFlags = flags;
        }
        
        virtual int32_t GetFlags() const
        {
            return mFlags;
        }
        
        virtual float GetDuration() const
        {
            return mDuration;
        }
        
    protected:
        
        void UpdateTime(float delta)
        {
            mLocalTime += delta;
            
            if (piFlagIs(mFlags, eAnim_Looping))
            {
                mLocalTime = fmod(mLocalTime, mDuration);
            }
            else
            {
                mLocalTime = piMin(mLocalTime, mDuration);
            }
        }
    };
    
    template<class I>
    class AnimClipImpl : public AnimImpl<I>
    {
    protected:
        float mBeginTime;
        SmartPtr<iRefObject> mTarget;

    public:
        AnimClipImpl():
        mBeginTime(0.0f)
        {
        }
        
        virtual ~AnimClipImpl()
        {
        }
        
        virtual float GetBeginTime() const
        {
            return mBeginTime;
        }
        
        virtual void SetBeginTiem(float time)
        {
            piAssert(time >= 0, ;);
            mBeginTime = time;
        }
        
        virtual void SetTarget(iRefObject* target)
        {
            mTarget = target;
        }
        
        virtual iRefObject* GetTarget() const
        {
            return mTarget;
        }
    };
    
    
    
    template<class I>
    class FrameAnimClipImpl : public AnimClipImpl<I>
    {
    protected:
        typedef std::vector<SmartPtr<iAnimFrame>> FrameVector;
        FrameVector mFrames;
        
    public:
        FrameAnimClipImpl()
        {
        }
        
        virtual ~FrameAnimClipImpl()
        {
        }
        
        virtual void AddFrame(iAnimFrame* frame)
        {
            piAssert(frame != nullptr, ;);
            
            float time= frame->GetTime();
            
            for (FrameVector::iterator it = mFrames.begin(); it != mFrames.end(); ++it)
            {
                if ((*it)->GetTime() > time)
                {
                    mFrames.insert(it, frame);
                    return;
                }
            }
            
            mFrames.push_back(frame);
        }
        
        virtual int32_t GetFrameCount() const
        {
            return (int32_t)mFrames.size();
        }
        
        virtual iAnimFrame* GetFrame(int32_t index) const
        {
            piAssert(index >= 0 && index < mFrames.size(), nullptr);
            return mFrames[index];
        }
        
    protected:
        
        int32_t FindBeginFrame(float time) const
        {
            int32_t index = 0;
            for (; index < mFrames.size(); ++index)
            {
                SmartPtr<iAnimFrame> frame = mFrames[index];
                if (frame->GetTime() > time)
                {
                    return index - 1;
                }
            }
            
            return GetFrameCount() - 1;
        }
    };
}

#endif































