/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.12.23   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_IMPL_CAMERAIMPL_H
#define PI_GAME_IMPL_CAMERAIMPL_H

#include <pi/game/impl/ComponentImpl.h>

namespace nspi
{
    
    template<class I>
    class CameraImpl : public ComponentImpl<I>
    {
    protected:
        mutable mat4 mProjMatrix;
        mutable mat4 mViewMatrix;
        
        vec3 mPosition;
        vec3 mTarget;
        float mNear;
        float mFar;
        vec3 mUp;
        float mLeft;
        float mRight;
        float mTop;
        float mBottom;
        
        int32_t mDirtyFlags;
        
        enum
        {
            eDirty_ProjectMatrix = piBit(0),
            eDirty_ViewMatrix    = piBit(1),
        };
        
    public:
        CameraImpl():
        mPosition(0.0f, 0.0f, 1.0f),
        mTarget(0.0f, 0.0f, 0.0f),
        mUp(0.0f, 1.0f, 0.0f),
        mDirtyFlags(0),
        mNear(1), mFar(1000000),
        mLeft(0), mRight(0), mTop(0), mBottom(0)
        {
        }
        
        virtual ~CameraImpl()
        {
        }
        
        virtual void SetNear(float value)
        {
            mNear = value;
            piFlagOn(mDirtyFlags, eDirty_ProjectMatrix);
        }
        
        virtual float GetNear() const
        {
            return mNear;
        }
        
        virtual void SetFar(float value)
        {
            mFar = value;
            piFlagOn(mDirtyFlags, eDirty_ProjectMatrix);
        }
        
        virtual float GetFar() const
        {
            return mFar;
        }
        
        virtual mat4 GetViewMatrix() const
        {
            if (piFlagIs(mDirtyFlags, eDirty_ViewMatrix))
            {
                mViewMatrix = piglm::lookAt(mPosition, mTarget, mUp);
                piFlagOff(mDirtyFlags, eDirty_ViewMatrix);
            }
            return mViewMatrix;
        }
        
        virtual void SetViewMatrix(const mat4& matrix)
        {
            mViewMatrix = matrix;
            piFlagOff(mDirtyFlags, eDirty_ViewMatrix);
        }
        
        virtual void SetProjectionMatrix(const mat4& matrix)
        {
            mProjMatrix = matrix;
            piFlagOff(mDirtyFlags, eDirty_ProjectMatrix);
        }
        
        virtual void SetPosition(const vec3& pos)
        {
            mPosition = pos;
            piFlagOn(mDirtyFlags, eDirty_ViewMatrix);
        }
        
        virtual vec3 GetPosition() const
        {
            return mPosition;
        }
        
        virtual void SetTarget(const vec3& target)
        {
            mTarget = target;
            piFlagOn(mDirtyFlags, eDirty_ViewMatrix);
        }
        
        virtual vec3 GetTarget() const
        {
            return mTarget;
        }
        
        virtual void SetUp(const vec3& up)
        {
            mUp = up;
            piFlagOn(mDirtyFlags, eDirty_ViewMatrix);
        }
        
        virtual vec3 GetUp() const
        {
            return mUp;
        }
        
        virtual void SetLeft(float value)
        {
            mLeft = value;
            piFlagOn(mDirtyFlags, eDirty_ProjectMatrix);
        }
        
        virtual float GetLeft() const
        {
            return mLeft;
        }
        
        virtual void SetRight(float value)
        {
            mRight = value;
            piFlagOn(mDirtyFlags, eDirty_ProjectMatrix);
        }
        
        virtual float GetRight() const
        {
            return mRight;
        }
        
        virtual void SetBottom(float value)
        {
            mBottom = value;
            piFlagOn(mDirtyFlags, eDirty_ProjectMatrix);
        }
        
        virtual float GetBottom() const
        {
            return mBottom;
        }
        
        virtual void SetTop(float value)
        {
            mTop = value;
            piFlagOn(mDirtyFlags, eDirty_ProjectMatrix);
        }
        
        virtual float GetTop() const
        {
            return mTop;
        }
        
        virtual void SetBounds(float left, float right, float bottom, float top)
        {
            mLeft = left;
            mRight = right;
            mBottom = bottom;
            mTop = top;
            piFlagOn(mDirtyFlags, eDirty_ProjectMatrix);
        }
    };
    
}


#endif










