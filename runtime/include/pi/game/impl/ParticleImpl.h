/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 Sword     2017.5.18   1.0     Create
 ******************************************************************************/
#ifndef PI_GAME_IMPL_PARTICLEIMPL_H
#define PI_GAME_IMPL_PARTICLEIMPL_H

namespace nspi
{
    template <class I>
    class ParticleImpl : public I
    {
    protected:
        bool mAlive;
        float mPassTime;
        float mLifeTime;
        
        vec3 mPosition;
        vec3 mRotation;
        
        vec3 mVelocity;
        
        vec3 mRotationRate;
        vec3 mAcceleration;
        
        vec3 mRotationalAcceleration;
    public:
        ParticleImpl()
        : mAlive(true)
        , mPassTime(0.0f)
        , mLifeTime(0.0f)
        {
        }
        
        virtual ~ParticleImpl()
        {
        }
        
        virtual float GetLifeTime() const
        {
            return mLifeTime;
        }
        
        virtual void SetLifeTime(float time)
        {
            mLifeTime = time;
        }
        
        virtual float GetPassTime() const
        {
            return mPassTime;
        }
        
        virtual void SetPassTime(float time)
        {
            mPassTime = time;
        }
        
        virtual bool IsAlive() const
        {
            return mAlive;
        }
        
        virtual void SetAlive(bool flag)
        {
            mAlive = flag;
        }
        
        virtual vec3 GetPosition() const
        {
            return mPosition;
        }
        
        virtual void SetPosition(const vec3& pos)
        {
            mPosition = pos;
        }
        
        virtual vec3 GetRotation() const
        {
            return mRotation;
        }
        
        virtual void SetRotation(const vec3& rotation)
        {
            mRotation = rotation;
        }
        
        virtual vec3 GetVelocity() const
        {
            return mVelocity;
        }
        
        virtual void SetVelocity(const vec3& velocity)
        {
            mVelocity = velocity;
        }
        
        virtual vec3 GetAcceleration() const
        {
            return mAcceleration;
        }
        
        virtual void SetAcceleration(const vec3& acceleration)
        {
            mAcceleration = acceleration;
        }
        
        virtual vec3 GetRotationRate() const
        {
            return mRotationRate;
        }
        
        virtual void SetRotationRate(const vec3& rotationRate)
        {
            mRotationRate = rotationRate;
        }
        
        virtual vec3 GetRotationalAcceleration() const
        {
            return mRotationalAcceleration;
        }
        
        virtual void SetRotationalAcceleration(const vec3& rotationalAcceleration)
        {
            mRotationalAcceleration = rotationalAcceleration;
        }
    };
}


#endif



















