/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.23   1.0     Create
 ******************************************************************************/
#ifndef PI_GAME_IMPL_TRANSFORMIMPL_H
#define PI_GAME_IMPL_TRANSFORMIMPL_H

#include <pi/game/impl/ComponentImpl.h>

namespace nspi
{
    template <class I>
    class TransformImpl : public ComponentImpl<I>
    {
    protected:
        vec3 mTranslation;
        vec3 mScale;
        vec3 mOrientation;
        
        mutable bool mDirty;
        mutable mat4 mMatrix;
        
    public:
        TransformImpl():
        mScale(1,1,1),
        mMatrix (1.0f)
        {
        }
        
        virtual ~TransformImpl()
        {
        }
        
        
        virtual vec3 GetTranslation() const
        {
            return mTranslation;
        }
        
        virtual void SetTranslation(const vec3& value)
        {
            mTranslation = value;
            mDirty = true;
        }
        
        virtual void Translate(const vec3& value)
        {
            mTranslation += value;
            mDirty = true;
        }
        
        virtual vec3 GetOrientation() const
        {
            return mOrientation;
        }
        
        virtual void SetOrientation(const vec3& value)
        {
            mOrientation = value;
            mDirty = true;
        }
        
        virtual void Rotate(const vec3& angles)
        {
            mOrientation += angles;
        }
        
        virtual vec3 GetScale() const
        {
            return mScale;
        }
        
        virtual void SetScale(const vec3& value)
        {
            mScale = value;
            mDirty = true;
        }
        
        virtual void Scale(const vec3& value)
        {
            mScale.x *= value.x;
            mScale.y *= value.y;
            mScale.z *= value.z;
            mDirty = true;
        }
        
        virtual void SetMatrix(const mat4& value)
        {
            mMatrix = value;
            mDirty = false;
        }
        
        virtual mat4 GetMatrix() const
        {
            if (mDirty)
            {
                mat4 translation = piglm::translate(mTranslation);
                
                float x = piglm::radians(mOrientation.x);
                float y = piglm::radians(mOrientation.y);
                float z = piglm::radians(mOrientation.z);
                mat4 orient = piglm::yawPitchRoll(y, x, z);
                
                mat4 scale = piglm::scale(mScale);
                
                mMatrix = translation * orient * scale;
                mDirty = false;
            }
            
            return mMatrix;
        }
        
        virtual mat4 EvaluateMatrix() const
        {
            return GetMatrix();
        }
    };
}

#endif


















