/*******************************************************************************
See copyright notice in LICENSE.

History:
Sword     2017.6.23   0.1     Create
******************************************************************************/
#ifndef _DYNAMIC_MESH_IMPL_H_
#define _DYNAMIC_MESH_IMPL_H_

#include "ComponentImpl.h"

namespace nspi
{
    template <class T>
    class DynamicMeshImpl : public ComponentImpl<T>
    {
    protected:
        using ComponentImpl<T>::GetEntity;
        
        bool                    mIsDirty;
        
        SmartPtr<iMemory>       mVertexBuffer;
        SmartPtr<iMemory>       mIndexBuffer;
        
        SmartPtr<iModelNode>    mModelNode;
        SmartPtr<iMesh>         mMesh;
        
        std::string             mMaterialUri;
        SmartPtr<iMaterial>     mMaterial;
        
        // Anchor Matrix, this will be mul before any other matrix.
        mat4                    mAnchorMatrix;
        
        std::vector< SmartPtr<iMemory> > mBufferList;
        
        int32_t mVBO;
        int32_t mIBO;
        int32_t mVAO;
        
        bool mIsMeshBufferInited;
    public:
        DynamicMeshImpl()
        : mVBO(0)
        , mIBO(0)
        , mVAO(0)
        , mIsDirty(false)
        , mIsMeshBufferInited(false)
        {
            mMesh = CreateMesh();
            mModelNode = CreateModelNode();
        }
        
        virtual ~DynamicMeshImpl()
        {
            piReleaseGraphicsObject(mVBO);
            piReleaseGraphicsObject(mIBO);
            piReleaseGraphicsObject(mVAO);
        }
        
        virtual void SetMaterialUri(const std::string& uri)
        {
            mMaterialUri = uri;
        }
        
        virtual std::string GetMaterialUri() const
        {
            return mMaterialUri;
        }
        
        virtual void SetMaterial(iMaterial* mat)
        {
            mMaterial = mat;
        }
        
        virtual iMaterial* GetMaterial() const
        {
            return mMaterial;
        }

        virtual void SetDirty(bool dirty)
        {
            mIsDirty = dirty;
        }
        
        virtual bool IsDirty() const
        {
            return mIsDirty;
        }
        
        virtual bool InitMeshBuffer() = 0;
        
        virtual iMemory* GetVertexBuffer() const
        {
            piAssert(!mVertexBuffer.IsNull(), nullptr);
            return mVertexBuffer;
        }

        virtual iMemory* GetIndexBuffer() const
        {
            piAssert(!mIndexBuffer.IsNull(), nullptr);
            return mIndexBuffer;
        }

        virtual mat4& GetAnchorMatrix()
        {
            return mAnchorMatrix;
        }
        
        virtual int32_t GetVAO()
        {
            return mVAO;
        }
        
        virtual void SetVertexCount(int32_t count)
        {
            mMesh->SetCount(count);
        }
        
        virtual iModelNode* GetModelNode() const
        {
            return mModelNode;
        }
        
        virtual iMesh* GetMesh() const
        {
            return mMesh;
        }
        
        virtual void OnLoad()
        {
            std::string name = GetEntity()->GetName();
            
            mModelNode->SetName(name + "_mesh");
            
            InitMeshBuffer();
            Apply();
        }
        
        virtual void OnUpdate(float delta)
        {
            if (IsDirty())
            {
                Apply();
            }
        }
        
        virtual void Apply()
        {
            InitMeshBuffer();
            
            ApplyVBO();
            ApplyIBO();
            ApplyVAO();
            SetDirty(false);
        }
        
        virtual void ApplyVBO()
        {
            piCheck(!mVertexBuffer.IsNull(), ;);
            
            if (mVBO == 0)
            {
                mVBO = piCreateBuffer();
                mMesh->SetVBO(mVBO);
            }
            
            SmartPtr<iMemory> vboDrawBuffer = GetUnusedBuffer(mVertexBuffer->Size());
            piAssert(!vboDrawBuffer.IsNull(), ;);
            
            memcpy(vboDrawBuffer->Ptr(), mVertexBuffer->Ptr(), mVertexBuffer->Size());
            
            piBindBuffer(eGraphicsBuffer_Vertex, mVBO);
            piBufferData(eGraphicsBuffer_Vertex, mVBO, vboDrawBuffer->Size(), vboDrawBuffer);
            piBindBuffer(eGraphicsBuffer_Vertex, 0);
        }
        
        virtual void ApplyIBO()
        {
            piCheck(!mIndexBuffer.IsNull(), ;);
            
            if (mIBO == 0)
            {
                mIBO = piCreateBuffer();
                mMesh->SetVBO(mIBO);
            }
            
            SmartPtr<iMemory> iboDrawBuffer = GetUnusedBuffer(mIndexBuffer->Size());
            piAssert(!iboDrawBuffer.IsNull(), ;);
            
            memcpy(iboDrawBuffer->Ptr(), mIndexBuffer->Ptr(), mIndexBuffer->Size());
            
            piBindBuffer(eGraphicsBuffer_Index, mIBO);
            piBufferData(eGraphicsBuffer_Index, mIBO, iboDrawBuffer->Size(), iboDrawBuffer);
            piBindBuffer(eGraphicsBuffer_Index, 0);
        }
        
        virtual void ApplyVAO()
        {
            // TO DO if the vertex format is dynamic, this logic should be change.
            if (mVAO == 0)
            {
                mVAO = piCreateVertexArray();
                
                PILOGI(PI_GAME_TAG, "DynamicMesh VAO:[%d]", mVAO);
                
                piBindVertexArray(mVAO);
                
                piBindBuffer(eGraphicsBuffer_Vertex, mVBO);
                piBindBuffer(eGraphicsBuffer_Index, mIBO);
                
                piEnableVertexAttr(0);
                piEnableVertexAttr(1);
                piEnableVertexAttr(2);
                piEnableVertexAttr(3);
                piEnableVertexAttr(4);
                
                piVertexAttr(0, 3, eType_F32, sizeof(MeshVertex), offsetof(MeshVertex, pos));
                piVertexAttr(1, 2, eType_F32, sizeof(MeshVertex), offsetof(MeshVertex, uv));
                piVertexAttr(2, 4, eType_F32, sizeof(MeshVertex), offsetof(MeshVertex, jindex));
                piVertexAttr(3, 3, eType_F32, sizeof(MeshVertex), offsetof(MeshVertex, jweight));
                piVertexAttr(4, 3, eType_F32, sizeof(MeshVertex), offsetof(MeshVertex, normal));
                
                piBindVertexArray(0);
                piBindBuffer(eGraphicsBuffer_Vertex, 0);
                piBindBuffer(eGraphicsBuffer_Index, 0);
            }
        }
        
        iMemory* GetUnusedBuffer(size_t bufferSize)
        {
            SmartPtr<iMemory> unusedBuffer;
            for (size_t i = 0; i < mBufferList.size(); ++i)
            {
                if (mBufferList[i]->GetRefCount() < 2)
                {
                    unusedBuffer = mBufferList[i];
                    if (unusedBuffer->Size() < bufferSize)
                    {
                        unusedBuffer->Resize(bufferSize);
                    }
                    break;
                }
            }
            
            if (unusedBuffer.IsNull())
            {
                unusedBuffer = CreateMemory(bufferSize);
                mBufferList.push_back(unusedBuffer);
                
                piAssert(bufferSize <= unusedBuffer->Size(), nullptr);
            }
            
            return unusedBuffer.PtrAndSetNull();
        }
    };
}

#endif
