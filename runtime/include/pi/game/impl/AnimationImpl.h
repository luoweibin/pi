/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.23   1.0     Create
 ******************************************************************************/
#ifndef PI_GAME_IMPL_ANIMATIONIMPL_H
#define PI_GAME_IMPL_ANIMATIONIMPL_H

#include <pi/game/impl/ComponentImpl.h>

namespace nspi
{
    template <class I>
    class AnimationImpl : public ComponentImpl<I>
    {
    protected:
        SmartPtr<iAssetAnim> mAssetAnim;
        
    public:
        AnimationImpl()
        {
        }
        
        virtual ~AnimationImpl()
        {
        }
        
        virtual iAssetAnim* GetAnimation() const
        {
            return mAssetAnim;
        }
        
        virtual void SetAnimation(iAssetAnim* anim)
        {
            piAssert(anim != nullptr, ;);
            mAssetAnim = anim;
        }
    };
}


#endif




















