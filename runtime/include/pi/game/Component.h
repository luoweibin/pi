/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.23   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_COMPONENT_H
#define PI_GAME_COMPONENT_H

namespace nspi
{
    struct iEntity;
    
    PIInterface(HasCreator = false)
    struct iComponent : public iRefObject
    {
        PI_DEFINE(iComponent)
        
        PIGetter()
        virtual bool IsEnabled() const = 0;
        
        PISetter()
        virtual void SetEnabled(bool value) = 0;
        
        PIMethod()
        virtual uint64_t GetTag() const = 0;
        
        PIMethod()
        virtual iEntity* GetEntity() const = 0;
        
        PIMethod()
        virtual void SetEntity(iEntity* entity) = 0;
        
        /**
         * =========================================================================
         * Callbacks
         */
        
        virtual void OnLoad() = 0;
        
        virtual void OnUnload() = 0;
        
        virtual void OnResize(const rect& bounds) = 0;
        
        virtual bool OnMessage(iMessage* message) = 0;
        
        virtual bool OnHIDEvent(iHIDEvent* event) = 0;
        
        virtual void OnUpdate(float delta) = 0;
    };
    
    PI_DEFINE_OBJECT_ARRAY(iComponent);
    iComponentArray* CreateComponentArray();
}

#endif













