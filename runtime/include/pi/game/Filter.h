/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.5.4   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_FILTER_H
#define PI_GAME_FILTER_H

namespace nspi
{
    
    PIInterface()
    struct iFilter : public iComponent
    {
        PI_DEFINE(iFilter)
    };
    
    iFilter* CreateFilter();
    
    PIInterface()
    struct iFilterSystem : public iSystem
    {
        PI_DEFINE(iFilterSystem)
    };
    
    iFilterSystem* CreateFilterSystem();
}

#endif























