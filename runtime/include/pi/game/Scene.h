/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.10   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_SCENE_H
#define PI_GAME_SCENE_H

#include <pi/Asset.h>
#include <pi/CV.h>
#include <pi/Scripting.h>

typedef struct lua_State lua_State;

namespace nspi
{
    struct iGame;
    struct iAssetManager;
    
    PIEnum()
    enum eSceneCap
    {
        eSceneCap_FacialLandmarks   = 1,
        eSceneCap_Calibrate         = 2,
    };
    
    PIInterface(HasCreator = false)
    struct iScene : public iAsset
    {
        PI_DEFINE(iScene)
        
        virtual void SetName(const std::string& name) = 0;
        
        virtual std::string GetName() const = 0;
        
        PIGetter()
        virtual iEntity* GetRootEntity() const = 0;
        
        PISetter()
        virtual void SetRootEntity(iEntity* entity) = 0;
        
        PIMethod()
        virtual void AddEntity(iEntity* entity) = 0;
        
        PIMethod()
        virtual void AddEntityRecursively(iEntity* entity) = 0;
        
        PIMethod()
        virtual void RemoveEntity(iEntity* entity) = 0;
        
        PIMethod()
        virtual void RemoveEntityRecursively(iEntity* entity) = 0;
        
        PIMethod()
        virtual iEntity* FindEntity(const std::string& name) const = 0;
        
        PIMethod()
        virtual void AddSystem(iSystem* system) = 0;
        
        PIMethod()
        virtual void RemoveSystem(iSystem* system) = 0;
        
        /**
         * In Scene messages, broadcast to current scene.
         */
        PIMethod()
        virtual void PostLocalMessage(iSystem* sender,
                                      int priority,
                                      int32_t message,
                                      const Var& arg1 = Var(),
                                      const Var& arg2 = Var()) = 0;
        
        /**
         * In Game messages, broadcast to other scenes. Current scene will not receive the message.
         */
        PIMethod()
        virtual void PostGlobalMessage(int priority,
                                       int32_t message,
                                       const Var& arg1 = Var(),
                                       const Var& arg2 = Var()) = 0;
        
        PIMethod()
        virtual rect GetBounds() const = 0;
        
        PIMethod()
        virtual iEntityArray* GetCameraEntities() const = 0;
        
        PIMethod()
        virtual iEntity* GetFirstActiveCameraEntity() const = 0;
        
        /**
         * -----------------------------------------------------------------------------------------
         */
        
        virtual iScript* GetScript() const = 0;
                
        PIMethod()
        virtual iHID* GetHID() const = 0;
        
        PIMethod()
        virtual iMotionManager* GetMotionManager() const = 0;
        
        PIMethod()
        virtual iClassLoader* GetClassLoader() const = 0;
        
        PIMethod()
        virtual iI32Array* GetCaps() const = 0;
        
        virtual void SetCaps(iI32Array* caps) = 0;
        
        virtual void SetAssetManager(iAssetManager* manager) = 0;
        
        PIMethod()
        virtual iAssetManager* GetAssetManager() const = 0;
        
        virtual void SetGame(iGame* game) = 0;
        
        
        /**
         * -----------------------------------------------------------------------------------------
         */
        PIGetter()
        virtual vec2 GetDesignSize() const = 0;
        
        PISetter()
        virtual void SetDesignSize(const vec2& value) = 0;
        
        PIGetter()
        virtual float GetXScaling() const = 0;
        
        PISetter()
        virtual void SetXScaling(float percent) = 0;
        
        PIMethod()
        virtual mat4 GetFaceProjMatrix() = 0;
        
        PIMethod()
        virtual float GetPixelSize() const = 0;
        
        PIMethod()
        virtual float GetRenderScale() const = 0;
        
        PIMethod()
        virtual vec2 VideoUVToScreenPos(const vec2& uv) = 0;
        
        PIMethod()
        virtual vec3 VideoUVToScreenPosV3(const vec3& uv) = 0;
        
        PIMethod()
        virtual vec2 ScreenSizeToGLOrthSize(const vec2& screenSize) = 0;
        
        PIMethod()
        virtual vec3 ScreenSizeToGLOrthSizeV3(const vec3& screenSize) = 0;
        
        PIMethod()
        virtual vec2 ScreenPosToGLOrthPos(const vec2& pos) = 0;
        
        PIMethod()
        virtual vec3 ScreenPosToGLOrthPosV3(const vec3& pos) = 0;

        /**
         * -----------------------------------------------------------------------------------------
         */
        
        PIMethod()
        virtual void SwapBuffer() = 0;
        
        PIMethod()
        virtual int32_t GetFrontBufferTex() const = 0;
        
        PIMethod()
        virtual int32_t GetBackBufferTex() const = 0;
        
        /**
         * -----------------------------------------------------------------------------------------
         */
        
        virtual void OnLoad() = 0;
        
        virtual void OnUnload() = 0;
        
        virtual void OnResize(const rect& bounds) = 0;
        
        virtual bool OnMessage(iMessage* message) = 0;
        
        virtual bool OnHIDEvent(iHIDEvent* event) = 0;
        
        virtual void OnUpdate(float delta) = 0;
    };
    
    iScene* CreateScene();
}


#endif













