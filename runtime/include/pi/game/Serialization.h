/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.6.13   0.1     Create
 ******************************************************************************/
#ifndef PI_CORE_SERIALIZATION_H
#define PI_CORE_SERIALIZATION_H

namespace nspi
{
    
    PIInterface(HasCreator = false)
    struct iSerializer : public iRefObject
    {
        PI_DEFINE(iSerializer)
        
        PIMethod()
        virtual bool Write(iStream* stream, const Var& value) = 0;
    };
    
    iSerializer* CreateJsonSerializer();
    
    
    PIInterface(HasCreator = false)
    struct iUnserializer : public iRefObject
    {
        PI_DEFINE(iUnserializer)
        
        PIMethod()
        virtual Var Read(iStream* stream) = 0;
        
        PIMethod()
        virtual Var ReadClass(iStream* stream, iClass* klass) = 0;
    };
    
    iUnserializer* CreateJsonUnserializer(iAssetManager* assetManager, iClassLoader* classLoader);
}

#endif























