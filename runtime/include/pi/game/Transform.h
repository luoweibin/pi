/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.12.2   0.1     Create
 ******************************************************************************/
#ifndef PI_SCENE_TRANSFORM_H
#define PI_SCENE_TRANSFORM_H

namespace nspi
{
    
    PIEnum()
    enum eAlignment
    {
        eAlignment_Left         = 0,
        eAlignment_Center       = 1,
        eAlignment_Right        = 2,
        
        eAlignment_Top          = 4,
        eAlignment_Bottom       = 5,
        
        eAlignment_Stretch      = 6
    };
    
    PIInterface()
    struct iTransform : public iComponent
    {
        PI_DEFINE(iTransform)
        
        PIGetter()
        virtual vec3 GetTranslation() const = 0;
        
        PISetter()
        virtual void SetTranslation(const vec3& value) = 0;
        
        PIMethod()
        virtual void Translate(const vec3& value) = 0;
        
        PIGetter()
        virtual vec3 GetOrientation() const = 0;
        
        PISetter()
        virtual void SetOrientation(const vec3& value) = 0;
        
        PIMethod()
        virtual void Rotate(const vec3& angles) = 0;
        
        PIGetter()
        virtual vec3 GetScale() const = 0;
        
        PISetter()
        virtual void SetScale(const vec3& value) = 0;
        
        PIMethod()
        virtual void Scale(const vec3& value) = 0;
        
        PISetter()
        virtual void SetMatrix(const mat4& value) = 0;
        
        PIGetter()
        virtual mat4 GetMatrix() const = 0;
        
        PIMethod()
        virtual mat4 EvaluateMatrix() const = 0;
    };
    
    iTransform* CreateTransform();
    
    PIInterface()
    struct iTransform2D : public iTransform
    {
        PI_DEFINE(iTransform2D)
        
        PIGetter()
        virtual vec2 GetSize() const = 0;
        
        PISetter()
        virtual void SetSize(const vec2& size) = 0;
        
        PIGetter()
        virtual vec2 GetAnchor() const = 0;
        
        PISetter()
        virtual void SetAnchor(const vec2& value) = 0;
        
        PIGetter(Class = eAlignment)
        virtual int GetAlignmentH() const = 0;
        
        PISetter()
        virtual void SetAlignmentH(int value) = 0;
        
        PIGetter(Class = eAlignment)
        virtual int GetAlignmentV() const = 0;
        
        PISetter()
        virtual void SetAlignmentV(int value) = 0;
        
        PIMethod()
        virtual void SetRenderScale(float scale) = 0;
        
        PIGetter()
        virtual vec2 GetAlignment() const = 0;
        
        PISetter()
        virtual void SetAlignment(const vec2& value) = 0;
    };
    
    iTransform2D* CreateTransform2D();
    
}

#endif













