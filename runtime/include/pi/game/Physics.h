/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.9.7   0.1     Create
 ******************************************************************************/
#ifndef PI_PHYSICS_H
#define PI_PHYSICS_H

#include <pi/Game.h>

namespace nspi
{
    
    enum
    {
        ePhysicsBody_Dynamic,
        ePhysicsBody_Kinematic,
        ePhysicsBody_Static,
    };

    struct iPhysicsBody2D : public iRefObject
    {
        virtual ~iPhysicsBody2D() {}
        
        virtual void SetPosition(const vec3& position) = 0;
        
        virtual vec3 GetPosition() const = 0;
        
        // degrees，not radius
        virtual float GetAngle() const = 0;
        
        // degrees，not radius
        virtual void SetAngle(float degrees) = 0;
        
        virtual float GetMass() const = 0;
        
        virtual void SetMass(float mass) = 0;
        
        virtual vec2 GetForce() const = 0;
        
        virtual void SetForce(const vec2& force) = 0;
        
        virtual float GetTorque() const = 0;
        
        virtual void SetTorque(float torque) = 0;
        
        virtual void SetType(int type) = 0;
        
        virtual int GetType() const = 0;
        
        virtual void SetVelocity(const vec3& velocity) = 0;
        
        virtual vec3 GetVelocity() const = 0;
        
        virtual vec3 WorldToLocal(const vec3& point) = 0;
    };
    
    
    struct iPhysicsShape2D : public iRefObject
    {
        virtual ~iPhysicsShape2D() {}
        
        virtual void SetElasticity(float value) = 0;
        
        virtual float GetElasticity() const = 0;
    };
    
    struct iPhysicsJoint2D : public iRefObject
    {
        virtual ~iPhysicsJoint2D() {}
        
        virtual float GetMaxForce() const = 0;
        
        virtual void SetMaxForce(float value) = 0;
        
        virtual float GetMaxBias() const = 0;
        
        virtual void SetMaxBias(float value) = 0;
        
        virtual float GetErrorBias() const = 0;
        
        virtual void SetErrorBias(float value) = 0;
    };
    
    
    enum
    {
        ePhysics_Chipmunk,
        ePhysics_Box2D,
    };
    
    struct iPhysicsWorld2D : public iRefObject
    {
        virtual ~iPhysicsWorld2D() {}
        
        virtual void SetGravity(const vec2& gravity) = 0;
        
        virtual vec2 GetGravity() const = 0;
        
        virtual void SetIterations(int32_t steps) = 0;
        
        virtual int32_t GetIterations() const = 0;
        
        virtual void Step(float delta) = 0;
        
        //==========================================================================================
        // Body
        
        virtual iPhysicsBody2D* GetStaticBody() const = 0;
        
        virtual iPhysicsBody2D* CreatePolygonBody(float mass,
                                                  iVec3Array* vertices,
                                                  const vec2& offset,
                                                  float radius) = 0;
        
        virtual iPhysicsBody2D* CreateCircleBody(float mass,
                                                 float innerDiameter,
                                                 float outerDiameter,
                                                 const vec2& offset) = 0;
        
        virtual iPhysicsBody2D* CreateSegmentBody(float mass,
                                                  const vec2& begin,
                                                  const vec2& end,
                                                  float radius) = 0;
        
        virtual iPhysicsBody2D* CreateKinematicBody() = 0;
        
        virtual iPhysicsBody2D* CreateStaticBody() = 0;
        
        virtual iPhysicsBody2D* CreateBoxBody(float mass, float width, float height) = 0;
        
        virtual void AddBody(iPhysicsBody2D* body) = 0;
        
        virtual void RemoveBody(iPhysicsBody2D *body) = 0;
        
        virtual bool ContainsBody(iPhysicsBody2D *body) const = 0;
        
        //==========================================================================================
        // Shape
        
        virtual iPhysicsShape2D* CreatePolygonShape(iPhysicsBody2D* body,
                                                    iVec3Array* vertices,
                                                    const vec2& offset,
                                                    float radius) = 0;
        
        virtual iPhysicsShape2D* CreateCircleShape(iPhysicsBody2D* body, float radius, const vec2& offset) = 0;
        
        virtual iPhysicsShape2D* CreateSegmentShape(iPhysicsBody2D* body,
                                                    const vec2& begin,
                                                    const vec2& end,
                                                    float radius) = 0;
        
        virtual iPhysicsShape2D* CreateBoxShape(iPhysicsBody2D* body,
                                                float width,
                                                float height,
                                                float radius) = 0;
        
        virtual void AddShape(iPhysicsShape2D *shape) = 0;
        
        virtual void RemoveShape(iPhysicsShape2D *shape) = 0;
        
        virtual bool ContainsShape(iPhysicsShape2D *shape) const = 0;
        
        //==========================================================================================
        // Joint
        
        virtual iPhysicsJoint2D* CreatePivotJointPivot(iPhysicsBody2D* bodyA,
                                                       iPhysicsBody2D* bodyB,
                                                       const vec3& pivot) = 0;
        
        virtual iPhysicsJoint2D* CreatePivotJointAnchors(iPhysicsBody2D* bodyA,
                                                         iPhysicsBody2D* bodyB,
                                                         const vec3& anchorA,
                                                         const vec3& anchorB) = 0;
    
        virtual void AddJoint(iPhysicsJoint2D* joint) = 0;
        
        virtual void RemoveJoint(iPhysicsJoint2D* joint) = 0;
        
        virtual bool ContainsJoint(iPhysicsJoint2D* joint) const = 0;
    };
    
    iPhysicsWorld2D* CreatePhysicsWorld2D(int backend, const vec2& gravity, int32_t iterations);
    
        
}

#endif














