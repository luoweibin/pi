/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.9   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_SCENEPARENT_H
#define PI_GAME_SCENEPARENT_H

namespace nspi
{
    PIInterface()
    struct iSceneParent : public iComponent
    {
        PI_DEFINE(iSceneParent)
        
        PIGetter()
        virtual std::string GetEntityName() const = 0;
        
        PISetter()
        virtual void SetEntityName(const std::string& name) = 0;
    };
    
    iSceneParent* CreateSceneParent();
}

#endif





















