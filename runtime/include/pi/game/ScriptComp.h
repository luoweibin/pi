/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 Sword    2017.7.13   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_SCRIPTCOMP_H
#define PI_GAME_SCRIPTCOMP_H

namespace nspi
{
    PIInterface()
    struct iScriptComp : public iComponent
    {
        PI_DEFINE(iScriptComp)
        
        PIGetter()
        virtual std::string GetUri() const = 0;
        
        PISetter()
        virtual void SetUri(const std::string& uri) = 0;
    };
    
    iScriptComp* CreateScriptComp();
}

#endif
