/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.27   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_3D_H
#define PI_GAME_3D_H

#include <pi/game/3d/MeshFile.h>
#include <pi/game/3d/RenderState.h>
#include <pi/game/3d/ModelMaterial.h>
#include <pi/game/3d/Material.h>
#include <pi/game/3d/ModelMesh.h>
#include <pi/game/3d/ModelSkin.h>
#include <pi/game/3d/ModelNode.h>
#include <pi/game/3d/Model.h>
#include <pi/game/3d/RenderSystem3D.h>

#include <pi/game/3d/base_mesh.h>

#endif
