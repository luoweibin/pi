/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.23   0.1     Create
 ******************************************************************************/
#ifndef PI_GAME_ENTITY_H
#define PI_GAME_ENTITY_H

namespace nspi
{
    struct iScene;
    
    PIEnum()
    enum eTag
    {
        eTag_0   = piBit(0),
        eTag_1   = piBit(1),
        eTag_2   = piBit(2),
        eTag_3   = piBit(3),
        eTag_4   = piBit(4),
        eTag_5   = piBit(5),
        eTag_6   = piBit(6),
        eTag_7   = piBit(7),
        eTag_8   = piBit(8),
        eTag_9   = piBit(9),
        eTag_10  = piBit(10),
        eTag_11  = piBit(11),
        eTag_12  = piBit(12),
        eTag_13  = piBit(13),
        eTag_14  = piBit(14),
        eTag_15  = piBit(15),
        eTag_16  = piBit(16),
        eTag_17  = piBit(17),
        eTag_18  = piBit(18),
        eTag_19  = piBit(19),
        eTag_20  = piBit(20),
        eTag_21  = piBit(21),
        eTag_22  = piBit(22),
        eTag_23  = piBit(23),
        eTag_24  = piBit(24),
        eTag_25  = piBit(25),
        eTag_26  = piBit(26),
        eTag_27  = piBit(27),
        eTag_28  = piBit(28),
        eTag_29  = piBit(29),
        eTag_30  = piBit(30),
        eTag_31  = piBit(31),
        
        eTag_Default = piBit(0),
    };
    
    class iEntityArray;
    
    PIInterface()
    struct iEntity : public iRefObject
    {
        PI_DEFINE(iEntity)
        
        PIGetter()
        virtual std::string GetName() const = 0;
        
        PISetter()
        virtual void SetName(const std::string& name) = 0;
        
        PIGetter(Class = eTag)
        virtual int GetTag() const = 0;
        
        PISetter()
        virtual void SetTag(int tag) = 0;
        
        PIGetter()
        virtual bool IsActive() const = 0;
        
        PISetter()
        virtual void SetActive(bool flag) = 0;
        
        PIGetter()
        virtual iEntity* GetParent() const = 0;
        
        PISetter()
        virtual void SetParent(iEntity* parent) = 0;
        
        // This will set parent point only, do not add to children array.
        virtual void SetParent_Only(iEntity* parent) = 0;
        
        PIGetter()
        virtual iEntityArray* GetChildren() const = 0;
        
        PISetter()
        virtual void SetChildren(iEntityArray* array) = 0;
        
        PIMethod()
        virtual void AddChild( iEntity* entity ) = 0;
        
        PIMethod()
        virtual void RemoveChild( iEntity* entity ) = 0;
        
        PIGetter()
        virtual iComponentArray* GetComps() const = 0;
        
        PIMethod()
        virtual void AddComp(iComponent* component) = 0;
        
        PIMethod()
        virtual void RemoveComp(iComponent* component) = 0;
        
        PIMethod()
        virtual iComponent* GetCompByClass(iClass* klass) const = 0;
        
        PIMethod()
        virtual iComponentArray* GetCompsByClass(iClass* klass) const = 0;
        
        PIMethod()
        virtual iComponent* GetCompByClassName(const std::string& name) const = 0;
        
        PIMethod()
        virtual iComponentArray* GetCompsByClassName(const std::string& name) const = 0;
        
        PIMethod()
        virtual iTable* FindDynamicComp(const std::string& name) const = 0;
        
        PIMethod()
        virtual iClassArray* GetCompClassArray() const = 0;
        
        PIMethod()
        virtual mat4 EvaluateGlobalTransform() = 0;
        
        virtual void SetScene(iScene* scene) = 0;
        
        PIMethod()
        virtual iScene* GetScene() const = 0;
        
        PIMethod()
        virtual bool IsActiveRecursively() const = 0;
        
        /**
         * Callbacks
         */
        
        virtual void OnLoad() = 0;
        
        virtual void OnUnload() = 0;
        
        virtual void OnResize(const rect& bounds) = 0;
        
        virtual bool OnMessage(iMessage* message) = 0;
        
        virtual bool OnHIDEvent(iHIDEvent* event) = 0;
        
        virtual void OnUpdate(float delta) = 0;
    };
    
    iEntity* CreateEntity();
    
    template <class T>
    static inline T* piGetEntityComponent(iEntity* entity)
    {
        return dynamic_cast<T*>(entity->GetCompByClass(T::StaticClass()));
    }
        
    PI_DEFINE_OBJECT_ARRAY(iEntity);    
    iEntityArray* CreateEntityArray();
    
}


#endif



















