/*******************************************************************************
 See copyright notice in LICENSE.

 **DO NOT** edit manually.

 This is generated by pic, which is a c++ code generator for reflection support.
 ******************************************************************************/
#include <pi/core/impl/ReflectionImpl.h>
#include <functional>

using namespace nspi;
using namespace std;

/**
 * ==========================================================================
 * eGraphicsBackend
 */

SmartPtr<iEnum> g_nspi_eGraphicsBackend_Enum;

static struct nspi_eGraphicsBackend_Enum
{
    nspi_eGraphicsBackend_Enum()
    {
        g_nspi_eGraphicsBackend_Enum = CreateEnum();
        g_nspi_eGraphicsBackend_Enum->SetName("eGraphicsBackend");
        g_nspi_eGraphicsBackend_Enum->SetFullName("nspi::eGraphicsBackend");
        g_nspi_eGraphicsBackend_Enum->RegisterValue("OpenGL3", 1);
        g_nspi_eGraphicsBackend_Enum->RegisterValue("OpenGL4", 2);
        g_nspi_eGraphicsBackend_Enum->RegisterValue("OpenGL_ES2", 3);
        g_nspi_eGraphicsBackend_Enum->RegisterValue("OpenGL_ES3", 4);
        g_nspi_eGraphicsBackend_Enum->RegisterValue("Unknown", -1);
        piGetRootClassLoader()->RegisterEnum("nspi::eGraphicsBackend", g_nspi_eGraphicsBackend_Enum);
        piGetRootClassLoader()->RegisterEnum("eGraphicsBackend", g_nspi_eGraphicsBackend_Enum);
    }
} g_nspi_eGraphicsBackend_Enum_Instance;
/**
 * ==========================================================================
 * eGraphicsCap
 */

SmartPtr<iEnum> g_nspi_eGraphicsCap_Enum;

static struct nspi_eGraphicsCap_Enum
{
    nspi_eGraphicsCap_Enum()
    {
        g_nspi_eGraphicsCap_Enum = CreateEnum();
        g_nspi_eGraphicsCap_Enum->SetName("eGraphicsCap");
        g_nspi_eGraphicsCap_Enum->SetFullName("nspi::eGraphicsCap");
        g_nspi_eGraphicsCap_Enum->RegisterValue("VAO", 1);
        piGetRootClassLoader()->RegisterEnum("nspi::eGraphicsCap", g_nspi_eGraphicsCap_Enum);
        piGetRootClassLoader()->RegisterEnum("eGraphicsCap", g_nspi_eGraphicsCap_Enum);
    }
} g_nspi_eGraphicsCap_Enum_Instance;
/**
 * ==========================================================================
 * eGraphicsFeature
 */

SmartPtr<iEnum> g_nspi_eGraphicsFeature_Enum;

static struct nspi_eGraphicsFeature_Enum
{
    nspi_eGraphicsFeature_Enum()
    {
        g_nspi_eGraphicsFeature_Enum = CreateEnum();
        g_nspi_eGraphicsFeature_Enum->SetName("eGraphicsFeature");
        g_nspi_eGraphicsFeature_Enum->SetFullName("nspi::eGraphicsFeature");
        g_nspi_eGraphicsFeature_Enum->RegisterValue("Blend", 4);
        g_nspi_eGraphicsFeature_Enum->RegisterValue("CullFace", 2);
        g_nspi_eGraphicsFeature_Enum->RegisterValue("DepthTest", 1);
        g_nspi_eGraphicsFeature_Enum->RegisterValue("LineSmooth", 256);
        g_nspi_eGraphicsFeature_Enum->RegisterValue("MultiSample", 64);
        g_nspi_eGraphicsFeature_Enum->RegisterValue("PolygonOffset_Fill", 32);
        g_nspi_eGraphicsFeature_Enum->RegisterValue("PolygonOffset_Line", 8);
        g_nspi_eGraphicsFeature_Enum->RegisterValue("PolygonOffset_Point", 16);
        g_nspi_eGraphicsFeature_Enum->RegisterValue("StencilTest", 128);
        g_nspi_eGraphicsFeature_Enum->RegisterValue("Texture1D", 512);
        g_nspi_eGraphicsFeature_Enum->RegisterValue("Texture2D", 1024);
        g_nspi_eGraphicsFeature_Enum->RegisterValue("Texture3D", 2048);
        piGetRootClassLoader()->RegisterEnum("nspi::eGraphicsFeature", g_nspi_eGraphicsFeature_Enum);
        piGetRootClassLoader()->RegisterEnum("eGraphicsFeature", g_nspi_eGraphicsFeature_Enum);
    }
} g_nspi_eGraphicsFeature_Enum_Instance;
/**
 * ==========================================================================
 * eHintTarget
 */

SmartPtr<iEnum> g_nspi_eHintTarget_Enum;

static struct nspi_eHintTarget_Enum
{
    nspi_eHintTarget_Enum()
    {
        g_nspi_eHintTarget_Enum = CreateEnum();
        g_nspi_eHintTarget_Enum->SetName("eHintTarget");
        g_nspi_eHintTarget_Enum->SetFullName("nspi::eHintTarget");
        g_nspi_eHintTarget_Enum->RegisterValue("LineSmooth", 0);
        g_nspi_eHintTarget_Enum->RegisterValue("PolygonSmooth", 1);
        g_nspi_eHintTarget_Enum->RegisterValue("TextureCompression", 2);
        g_nspi_eHintTarget_Enum->RegisterValue("Unknown", -1);
        piGetRootClassLoader()->RegisterEnum("nspi::eHintTarget", g_nspi_eHintTarget_Enum);
        piGetRootClassLoader()->RegisterEnum("eHintTarget", g_nspi_eHintTarget_Enum);
    }
} g_nspi_eHintTarget_Enum_Instance;
/**
 * ==========================================================================
 * eHint
 */

SmartPtr<iEnum> g_nspi_eHint_Enum;

static struct nspi_eHint_Enum
{
    nspi_eHint_Enum()
    {
        g_nspi_eHint_Enum = CreateEnum();
        g_nspi_eHint_Enum->SetName("eHint");
        g_nspi_eHint_Enum->SetFullName("nspi::eHint");
        g_nspi_eHint_Enum->RegisterValue("DontCare", 2);
        g_nspi_eHint_Enum->RegisterValue("Fastest", 1);
        g_nspi_eHint_Enum->RegisterValue("Nicest", 0);
        g_nspi_eHint_Enum->RegisterValue("Unknown", -1);
        piGetRootClassLoader()->RegisterEnum("nspi::eHint", g_nspi_eHint_Enum);
        piGetRootClassLoader()->RegisterEnum("eHint", g_nspi_eHint_Enum);
    }
} g_nspi_eHint_Enum_Instance;
/**
 * ==========================================================================
 * eBlendFunc
 */

SmartPtr<iEnum> g_nspi_eBlendFunc_Enum;

static struct nspi_eBlendFunc_Enum
{
    nspi_eBlendFunc_Enum()
    {
        g_nspi_eBlendFunc_Enum = CreateEnum();
        g_nspi_eBlendFunc_Enum->SetName("eBlendFunc");
        g_nspi_eBlendFunc_Enum->SetFullName("nspi::eBlendFunc");
        g_nspi_eBlendFunc_Enum->RegisterValue("ConstAlpha", 13);
        g_nspi_eBlendFunc_Enum->RegisterValue("ConstColor", 11);
        g_nspi_eBlendFunc_Enum->RegisterValue("DstAlpha", 9);
        g_nspi_eBlendFunc_Enum->RegisterValue("DstColor", 5);
        g_nspi_eBlendFunc_Enum->RegisterValue("One", 1);
        g_nspi_eBlendFunc_Enum->RegisterValue("OneMinusConstAlpha", 14);
        g_nspi_eBlendFunc_Enum->RegisterValue("OneMinusConstColor", 12);
        g_nspi_eBlendFunc_Enum->RegisterValue("OneMinusDstAlpha", 10);
        g_nspi_eBlendFunc_Enum->RegisterValue("OneMinusDstColor", 6);
        g_nspi_eBlendFunc_Enum->RegisterValue("OneMinusSrcAlpha", 8);
        g_nspi_eBlendFunc_Enum->RegisterValue("OneMinusSrcColor", 4);
        g_nspi_eBlendFunc_Enum->RegisterValue("SrcAlpha", 7);
        g_nspi_eBlendFunc_Enum->RegisterValue("SrcColor", 3);
        g_nspi_eBlendFunc_Enum->RegisterValue("Zero", 2);
        piGetRootClassLoader()->RegisterEnum("nspi::eBlendFunc", g_nspi_eBlendFunc_Enum);
        piGetRootClassLoader()->RegisterEnum("eBlendFunc", g_nspi_eBlendFunc_Enum);
    }
} g_nspi_eBlendFunc_Enum_Instance;
/**
 * ==========================================================================
 * eWinding
 */

SmartPtr<iEnum> g_nspi_eWinding_Enum;

static struct nspi_eWinding_Enum
{
    nspi_eWinding_Enum()
    {
        g_nspi_eWinding_Enum = CreateEnum();
        g_nspi_eWinding_Enum->SetName("eWinding");
        g_nspi_eWinding_Enum->SetFullName("nspi::eWinding");
        g_nspi_eWinding_Enum->RegisterValue("CCW", 1);
        g_nspi_eWinding_Enum->RegisterValue("CW", 0);
        piGetRootClassLoader()->RegisterEnum("nspi::eWinding", g_nspi_eWinding_Enum);
        piGetRootClassLoader()->RegisterEnum("eWinding", g_nspi_eWinding_Enum);
    }
} g_nspi_eWinding_Enum_Instance;
/**
 * ==========================================================================
 * eFace
 */

SmartPtr<iEnum> g_nspi_eFace_Enum;

static struct nspi_eFace_Enum
{
    nspi_eFace_Enum()
    {
        g_nspi_eFace_Enum = CreateEnum();
        g_nspi_eFace_Enum->SetName("eFace");
        g_nspi_eFace_Enum->SetFullName("nspi::eFace");
        g_nspi_eFace_Enum->RegisterValue("Back", 1);
        g_nspi_eFace_Enum->RegisterValue("Both", 2);
        g_nspi_eFace_Enum->RegisterValue("Front", 0);
        piGetRootClassLoader()->RegisterEnum("nspi::eFace", g_nspi_eFace_Enum);
        piGetRootClassLoader()->RegisterEnum("eFace", g_nspi_eFace_Enum);
    }
} g_nspi_eFace_Enum_Instance;
/**
 * ==========================================================================
 * eBufferBit
 */

SmartPtr<iEnum> g_nspi_eBufferBit_Enum;

static struct nspi_eBufferBit_Enum
{
    nspi_eBufferBit_Enum()
    {
        g_nspi_eBufferBit_Enum = CreateEnum();
        g_nspi_eBufferBit_Enum->SetName("eBufferBit");
        g_nspi_eBufferBit_Enum->SetFullName("nspi::eBufferBit");
        g_nspi_eBufferBit_Enum->RegisterValue("Color", 1);
        g_nspi_eBufferBit_Enum->RegisterValue("Depth", 2);
        g_nspi_eBufferBit_Enum->RegisterValue("Stencil", 4);
        piGetRootClassLoader()->RegisterEnum("nspi::eBufferBit", g_nspi_eBufferBit_Enum);
        piGetRootClassLoader()->RegisterEnum("eBufferBit", g_nspi_eBufferBit_Enum);
    }
} g_nspi_eBufferBit_Enum_Instance;
/**
 * ==========================================================================
 * eFramebuffer
 */

SmartPtr<iEnum> g_nspi_eFramebuffer_Enum;

static struct nspi_eFramebuffer_Enum
{
    nspi_eFramebuffer_Enum()
    {
        g_nspi_eFramebuffer_Enum = CreateEnum();
        g_nspi_eFramebuffer_Enum->SetName("eFramebuffer");
        g_nspi_eFramebuffer_Enum->SetFullName("nspi::eFramebuffer");
        g_nspi_eFramebuffer_Enum->RegisterValue("Draw", 1);
        g_nspi_eFramebuffer_Enum->RegisterValue("DrawRead", 3);
        g_nspi_eFramebuffer_Enum->RegisterValue("Read", 2);
        g_nspi_eFramebuffer_Enum->RegisterValue("Unknown", 0);
        piGetRootClassLoader()->RegisterEnum("nspi::eFramebuffer", g_nspi_eFramebuffer_Enum);
        piGetRootClassLoader()->RegisterEnum("eFramebuffer", g_nspi_eFramebuffer_Enum);
    }
} g_nspi_eFramebuffer_Enum_Instance;
/**
 * ==========================================================================
 * eTexConfig
 */

SmartPtr<iEnum> g_nspi_eTexConfig_Enum;

static struct nspi_eTexConfig_Enum
{
    nspi_eTexConfig_Enum()
    {
        g_nspi_eTexConfig_Enum = CreateEnum();
        g_nspi_eTexConfig_Enum->SetName("eTexConfig");
        g_nspi_eTexConfig_Enum->SetFullName("nspi::eTexConfig");
        g_nspi_eTexConfig_Enum->RegisterValue("MagFilter", 1);
        g_nspi_eTexConfig_Enum->RegisterValue("MinFilter", 0);
        g_nspi_eTexConfig_Enum->RegisterValue("PackAligment", 5);
        g_nspi_eTexConfig_Enum->RegisterValue("Unknown", -1);
        g_nspi_eTexConfig_Enum->RegisterValue("UnpackAligment", 6);
        g_nspi_eTexConfig_Enum->RegisterValue("WrapR", 4);
        g_nspi_eTexConfig_Enum->RegisterValue("WrapS", 2);
        g_nspi_eTexConfig_Enum->RegisterValue("WrapT", 3);
        piGetRootClassLoader()->RegisterEnum("nspi::eTexConfig", g_nspi_eTexConfig_Enum);
        piGetRootClassLoader()->RegisterEnum("eTexConfig", g_nspi_eTexConfig_Enum);
    }
} g_nspi_eTexConfig_Enum_Instance;
/**
 * ==========================================================================
 * eTexValue
 */

SmartPtr<iEnum> g_nspi_eTexValue_Enum;

static struct nspi_eTexValue_Enum
{
    nspi_eTexValue_Enum()
    {
        g_nspi_eTexValue_Enum = CreateEnum();
        g_nspi_eTexValue_Enum->SetName("eTexValue");
        g_nspi_eTexValue_Enum->SetFullName("nspi::eTexValue");
        g_nspi_eTexValue_Enum->RegisterValue("Border", 5);
        g_nspi_eTexValue_Enum->RegisterValue("Clamp", 4);
        g_nspi_eTexValue_Enum->RegisterValue("Linear", 2);
        g_nspi_eTexValue_Enum->RegisterValue("Mirror", 6);
        g_nspi_eTexValue_Enum->RegisterValue("Nearest", 1);
        g_nspi_eTexValue_Enum->RegisterValue("Repeat", 3);
        g_nspi_eTexValue_Enum->RegisterValue("Unknown", 0);
        piGetRootClassLoader()->RegisterEnum("nspi::eTexValue", g_nspi_eTexValue_Enum);
        piGetRootClassLoader()->RegisterEnum("eTexValue", g_nspi_eTexValue_Enum);
    }
} g_nspi_eTexValue_Enum_Instance;
/**
 * ==========================================================================
 * eTexTarget
 */

SmartPtr<iEnum> g_nspi_eTexTarget_Enum;

static struct nspi_eTexTarget_Enum
{
    nspi_eTexTarget_Enum()
    {
        g_nspi_eTexTarget_Enum = CreateEnum();
        g_nspi_eTexTarget_Enum->SetName("eTexTarget");
        g_nspi_eTexTarget_Enum->SetFullName("nspi::eTexTarget");
        g_nspi_eTexTarget_Enum->RegisterValue("1D", 0);
        g_nspi_eTexTarget_Enum->RegisterValue("2D", 1);
        g_nspi_eTexTarget_Enum->RegisterValue("3D", 2);
        g_nspi_eTexTarget_Enum->RegisterValue("CubeMap", 3);
        g_nspi_eTexTarget_Enum->RegisterValue("CubeMapNegativeX", 7);
        g_nspi_eTexTarget_Enum->RegisterValue("CubeMapNegativeY", 8);
        g_nspi_eTexTarget_Enum->RegisterValue("CubeMapNegativeZ", 9);
        g_nspi_eTexTarget_Enum->RegisterValue("CubeMapPositiveX", 4);
        g_nspi_eTexTarget_Enum->RegisterValue("CubeMapPositiveY", 5);
        g_nspi_eTexTarget_Enum->RegisterValue("CubeMapPositiveZ", 6);
        g_nspi_eTexTarget_Enum->RegisterValue("Unknown", -1);
        piGetRootClassLoader()->RegisterEnum("nspi::eTexTarget", g_nspi_eTexTarget_Enum);
        piGetRootClassLoader()->RegisterEnum("eTexTarget", g_nspi_eTexTarget_Enum);
    }
} g_nspi_eTexTarget_Enum_Instance;
/**
 * ==========================================================================
 * eAttachment
 */

SmartPtr<iEnum> g_nspi_eAttachment_Enum;

static struct nspi_eAttachment_Enum
{
    nspi_eAttachment_Enum()
    {
        g_nspi_eAttachment_Enum = CreateEnum();
        g_nspi_eAttachment_Enum->SetName("eAttachment");
        g_nspi_eAttachment_Enum->SetFullName("nspi::eAttachment");
        g_nspi_eAttachment_Enum->RegisterValue("Color0", 3);
        g_nspi_eAttachment_Enum->RegisterValue("Color1", 4);
        g_nspi_eAttachment_Enum->RegisterValue("Color10", 13);
        g_nspi_eAttachment_Enum->RegisterValue("Color11", 14);
        g_nspi_eAttachment_Enum->RegisterValue("Color12", 15);
        g_nspi_eAttachment_Enum->RegisterValue("Color13", 16);
        g_nspi_eAttachment_Enum->RegisterValue("Color14", 17);
        g_nspi_eAttachment_Enum->RegisterValue("Color15", 18);
        g_nspi_eAttachment_Enum->RegisterValue("Color2", 5);
        g_nspi_eAttachment_Enum->RegisterValue("Color3", 6);
        g_nspi_eAttachment_Enum->RegisterValue("Color4", 7);
        g_nspi_eAttachment_Enum->RegisterValue("Color5", 8);
        g_nspi_eAttachment_Enum->RegisterValue("Color6", 9);
        g_nspi_eAttachment_Enum->RegisterValue("Color7", 10);
        g_nspi_eAttachment_Enum->RegisterValue("Color8", 11);
        g_nspi_eAttachment_Enum->RegisterValue("Color9", 12);
        g_nspi_eAttachment_Enum->RegisterValue("Depth", 0);
        g_nspi_eAttachment_Enum->RegisterValue("DepthStencil", 2);
        g_nspi_eAttachment_Enum->RegisterValue("Stencil", 1);
        g_nspi_eAttachment_Enum->RegisterValue("Unknown", -1);
        piGetRootClassLoader()->RegisterEnum("nspi::eAttachment", g_nspi_eAttachment_Enum);
        piGetRootClassLoader()->RegisterEnum("eAttachment", g_nspi_eAttachment_Enum);
    }
} g_nspi_eAttachment_Enum_Instance;
/**
 * ==========================================================================
 * eGraphicsBuffer
 */

SmartPtr<iEnum> g_nspi_eGraphicsBuffer_Enum;

static struct nspi_eGraphicsBuffer_Enum
{
    nspi_eGraphicsBuffer_Enum()
    {
        g_nspi_eGraphicsBuffer_Enum = CreateEnum();
        g_nspi_eGraphicsBuffer_Enum->SetName("eGraphicsBuffer");
        g_nspi_eGraphicsBuffer_Enum->SetFullName("nspi::eGraphicsBuffer");
        g_nspi_eGraphicsBuffer_Enum->RegisterValue("Index", 1);
        g_nspi_eGraphicsBuffer_Enum->RegisterValue("Vertex", 0);
        piGetRootClassLoader()->RegisterEnum("nspi::eGraphicsBuffer", g_nspi_eGraphicsBuffer_Enum);
        piGetRootClassLoader()->RegisterEnum("eGraphicsBuffer", g_nspi_eGraphicsBuffer_Enum);
    }
} g_nspi_eGraphicsBuffer_Enum_Instance;
/**
 * ==========================================================================
 * eGraphicsDraw
 */

SmartPtr<iEnum> g_nspi_eGraphicsDraw_Enum;

static struct nspi_eGraphicsDraw_Enum
{
    nspi_eGraphicsDraw_Enum()
    {
        g_nspi_eGraphicsDraw_Enum = CreateEnum();
        g_nspi_eGraphicsDraw_Enum->SetName("eGraphicsDraw");
        g_nspi_eGraphicsDraw_Enum->SetFullName("nspi::eGraphicsDraw");
        g_nspi_eGraphicsDraw_Enum->RegisterValue("LineLoop", 1);
        g_nspi_eGraphicsDraw_Enum->RegisterValue("Lines", 0);
        g_nspi_eGraphicsDraw_Enum->RegisterValue("Points", 6);
        g_nspi_eGraphicsDraw_Enum->RegisterValue("QuadStrip", 5);
        g_nspi_eGraphicsDraw_Enum->RegisterValue("Quads", 4);
        g_nspi_eGraphicsDraw_Enum->RegisterValue("TriangleStrip", 3);
        g_nspi_eGraphicsDraw_Enum->RegisterValue("Triangles", 2);
        g_nspi_eGraphicsDraw_Enum->RegisterValue("Unknown", -1);
        piGetRootClassLoader()->RegisterEnum("nspi::eGraphicsDraw", g_nspi_eGraphicsDraw_Enum);
        piGetRootClassLoader()->RegisterEnum("eGraphicsDraw", g_nspi_eGraphicsDraw_Enum);
    }
} g_nspi_eGraphicsDraw_Enum_Instance;
/**
 * ==========================================================================
 * ePolygonMode
 */

SmartPtr<iEnum> g_nspi_ePolygonMode_Enum;

static struct nspi_ePolygonMode_Enum
{
    nspi_ePolygonMode_Enum()
    {
        g_nspi_ePolygonMode_Enum = CreateEnum();
        g_nspi_ePolygonMode_Enum->SetName("ePolygonMode");
        g_nspi_ePolygonMode_Enum->SetFullName("nspi::ePolygonMode");
        g_nspi_ePolygonMode_Enum->RegisterValue("Fill", 1);
        g_nspi_ePolygonMode_Enum->RegisterValue("Line", 0);
        g_nspi_ePolygonMode_Enum->RegisterValue("Point", 2);
        g_nspi_ePolygonMode_Enum->RegisterValue("Unknown", -1);
        piGetRootClassLoader()->RegisterEnum("nspi::ePolygonMode", g_nspi_ePolygonMode_Enum);
        piGetRootClassLoader()->RegisterEnum("ePolygonMode", g_nspi_ePolygonMode_Enum);
    }
} g_nspi_ePolygonMode_Enum_Instance;
/**
 * ==========================================================================
 * eFunc
 */

SmartPtr<iEnum> g_nspi_eFunc_Enum;

static struct nspi_eFunc_Enum
{
    nspi_eFunc_Enum()
    {
        g_nspi_eFunc_Enum = CreateEnum();
        g_nspi_eFunc_Enum->SetName("eFunc");
        g_nspi_eFunc_Enum->SetFullName("nspi::eFunc");
        g_nspi_eFunc_Enum->RegisterValue("Always", 8);
        g_nspi_eFunc_Enum->RegisterValue("Equal", 3);
        g_nspi_eFunc_Enum->RegisterValue("Greater", 1);
        g_nspi_eFunc_Enum->RegisterValue("GreaterAndEqual", 2);
        g_nspi_eFunc_Enum->RegisterValue("Less", 6);
        g_nspi_eFunc_Enum->RegisterValue("LessAndEqual", 5);
        g_nspi_eFunc_Enum->RegisterValue("Never", 7);
        g_nspi_eFunc_Enum->RegisterValue("NotEqual", 4);
        piGetRootClassLoader()->RegisterEnum("nspi::eFunc", g_nspi_eFunc_Enum);
        piGetRootClassLoader()->RegisterEnum("eFunc", g_nspi_eFunc_Enum);
    }
} g_nspi_eFunc_Enum_Instance;
/**
 * ==========================================================================
 * eStencilOp
 */

SmartPtr<iEnum> g_nspi_eStencilOp_Enum;

static struct nspi_eStencilOp_Enum
{
    nspi_eStencilOp_Enum()
    {
        g_nspi_eStencilOp_Enum = CreateEnum();
        g_nspi_eStencilOp_Enum->SetName("eStencilOp");
        g_nspi_eStencilOp_Enum->SetFullName("nspi::eStencilOp");
        g_nspi_eStencilOp_Enum->RegisterValue("Decrease", 5);
        g_nspi_eStencilOp_Enum->RegisterValue("DecreaseWrap", 6);
        g_nspi_eStencilOp_Enum->RegisterValue("Increase", 3);
        g_nspi_eStencilOp_Enum->RegisterValue("IncreaseWrap", 4);
        g_nspi_eStencilOp_Enum->RegisterValue("Invert", 7);
        g_nspi_eStencilOp_Enum->RegisterValue("Keep", 0);
        g_nspi_eStencilOp_Enum->RegisterValue("Replace", 2);
        g_nspi_eStencilOp_Enum->RegisterValue("Zero", 1);
        piGetRootClassLoader()->RegisterEnum("nspi::eStencilOp", g_nspi_eStencilOp_Enum);
        piGetRootClassLoader()->RegisterEnum("eStencilOp", g_nspi_eStencilOp_Enum);
    }
} g_nspi_eStencilOp_Enum_Instance;
/**
 * ==========================================================================
 * eEqFunc
 */

SmartPtr<iEnum> g_nspi_eEqFunc_Enum;

static struct nspi_eEqFunc_Enum
{
    nspi_eEqFunc_Enum()
    {
        g_nspi_eEqFunc_Enum = CreateEnum();
        g_nspi_eEqFunc_Enum->SetName("eEqFunc");
        g_nspi_eEqFunc_Enum->SetFullName("nspi::eEqFunc");
        g_nspi_eEqFunc_Enum->RegisterValue("Add", 0);
        g_nspi_eEqFunc_Enum->RegisterValue("RevSub", 2);
        g_nspi_eEqFunc_Enum->RegisterValue("Sub", 1);
        piGetRootClassLoader()->RegisterEnum("nspi::eEqFunc", g_nspi_eEqFunc_Enum);
        piGetRootClassLoader()->RegisterEnum("eEqFunc", g_nspi_eEqFunc_Enum);
    }
} g_nspi_eEqFunc_Enum_Instance;
