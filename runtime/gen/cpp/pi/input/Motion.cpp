/*******************************************************************************
 See copyright notice in LICENSE.

 **DO NOT** edit manually.

 This is generated by pic, which is a c++ code generator for reflection support.
 ******************************************************************************/
#include <pi/core/impl/ReflectionImpl.h>
#include <functional>

using namespace nspi;
using namespace std;

/**
 * ==========================================================================
 * iDeviceMotion
 */

class nspi_iDeviceMotion_Class : public RefObjectClassImpl
{
public:
    nspi_iDeviceMotion_Class(const std::string& simpleName, const std::string& fullName, iClassLoader* classLoader):
    RefObjectClassImpl(simpleName, fullName, classLoader)
    {
        {
            SmartPtr<RefObjectProperty<nspi::iDeviceMotion, int, int>> p = new RefObjectProperty<nspi::iDeviceMotion, int, int>("MagAccuracy", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iDeviceMotion::GetMagAccuracy);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            typedef vec3(nspi::iDeviceMotion::*nspi_iDeviceMotion_GetAttitude_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iDeviceMotion_GetAttitude_FuncPtr, nspi::iDeviceMotion> nspi_iDeviceMotion_GetAttitude_MethodClass;
            SmartPtr<nspi_iDeviceMotion_GetAttitude_MethodClass> m = new nspi_iDeviceMotion_GetAttitude_MethodClass(&nspi::iDeviceMotion::GetAttitude, "GetAttitude");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef quat(nspi::iDeviceMotion::*nspi_iDeviceMotion_GetAttitudeQuat_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iDeviceMotion_GetAttitudeQuat_FuncPtr, nspi::iDeviceMotion> nspi_iDeviceMotion_GetAttitudeQuat_MethodClass;
            SmartPtr<nspi_iDeviceMotion_GetAttitudeQuat_MethodClass> m = new nspi_iDeviceMotion_GetAttitudeQuat_MethodClass(&nspi::iDeviceMotion::GetAttitudeQuat, "GetAttitudeQuat");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef vec3(nspi::iDeviceMotion::*nspi_iDeviceMotion_GetRotationRate_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iDeviceMotion_GetRotationRate_FuncPtr, nspi::iDeviceMotion> nspi_iDeviceMotion_GetRotationRate_MethodClass;
            SmartPtr<nspi_iDeviceMotion_GetRotationRate_MethodClass> m = new nspi_iDeviceMotion_GetRotationRate_MethodClass(&nspi::iDeviceMotion::GetRotationRate, "GetRotationRate");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef vec3(nspi::iDeviceMotion::*nspi_iDeviceMotion_GetGravity_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iDeviceMotion_GetGravity_FuncPtr, nspi::iDeviceMotion> nspi_iDeviceMotion_GetGravity_MethodClass;
            SmartPtr<nspi_iDeviceMotion_GetGravity_MethodClass> m = new nspi_iDeviceMotion_GetGravity_MethodClass(&nspi::iDeviceMotion::GetGravity, "GetGravity");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef vec3(nspi::iDeviceMotion::*nspi_iDeviceMotion_GetUserAccel_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iDeviceMotion_GetUserAccel_FuncPtr, nspi::iDeviceMotion> nspi_iDeviceMotion_GetUserAccel_MethodClass;
            SmartPtr<nspi_iDeviceMotion_GetUserAccel_MethodClass> m = new nspi_iDeviceMotion_GetUserAccel_MethodClass(&nspi::iDeviceMotion::GetUserAccel, "GetUserAccel");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef vec3(nspi::iDeviceMotion::*nspi_iDeviceMotion_GetMagField_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iDeviceMotion_GetMagField_FuncPtr, nspi::iDeviceMotion> nspi_iDeviceMotion_GetMagField_MethodClass;
            SmartPtr<nspi_iDeviceMotion_GetMagField_MethodClass> m = new nspi_iDeviceMotion_GetMagField_MethodClass(&nspi::iDeviceMotion::GetMagField, "GetMagField");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

    }

    virtual ~nspi_iDeviceMotion_Class()
    {
    }

    virtual Var CreateInstance()
    {
        return nspi::CreateDeviceMotion();
    }
    virtual bool IsArray() const
    {
        return false;
    }
    virtual iClass* GetElementClass() const
    {
        return nullptr;
    }
    virtual iClass* GetParent() const
    {
        return nullptr;
    }
};

static SmartPtr<iClass> g_nspi_iDeviceMotion_Class;

iClass* iDeviceMotion::StaticClass()
{
    return g_nspi_iDeviceMotion_Class;
}

iClass* iDeviceMotion::GetClass() const
{
    return g_nspi_iDeviceMotion_Class;
}

std::string iDeviceMotion::ToString() const
{
    return piFormat("%s(%p)", GetClass()->GetFullName().c_str(), this);
}

static struct nspi_iDeviceMotion_Class_Reg
{
    nspi_iDeviceMotion_Class_Reg()
    {
        g_nspi_iDeviceMotion_Class = new nspi_iDeviceMotion_Class("iDeviceMotion", "nspi::iDeviceMotion", piGetRootClassLoader());
        g_nspi_iDeviceMotion_Class->SetReadonly();
        piGetRootClassLoader()->RegisterClass("nspi::iDeviceMotion", g_nspi_iDeviceMotion_Class);
        piGetRootClassLoader()->RegisterClass("DeviceMotion", g_nspi_iDeviceMotion_Class);
    }
} g_nspi_iDeviceMotion_Class_RegInstance;



/**
 * ==========================================================================
 * iMotionManager
 */

class nspi_iMotionManager_Class : public RefObjectClassImpl
{
public:
    nspi_iMotionManager_Class(const std::string& simpleName, const std::string& fullName, iClassLoader* classLoader):
    RefObjectClassImpl(simpleName, fullName, classLoader)
    {
        {
            typedef void(nspi::iMotionManager::*nspi_iMotionManager_Start_FuncPtr)();
            typedef RefObjectMethodVoidVoid<nspi_iMotionManager_Start_FuncPtr, nspi::iMotionManager> nspi_iMotionManager_Start_MethodClass;
            SmartPtr<nspi_iMotionManager_Start_MethodClass> m = new nspi_iMotionManager_Start_MethodClass(&nspi::iMotionManager::Start, "Start");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef void(nspi::iMotionManager::*nspi_iMotionManager_Stop_FuncPtr)();
            typedef RefObjectMethodVoidVoid<nspi_iMotionManager_Stop_FuncPtr, nspi::iMotionManager> nspi_iMotionManager_Stop_MethodClass;
            SmartPtr<nspi_iMotionManager_Stop_MethodClass> m = new nspi_iMotionManager_Stop_MethodClass(&nspi::iMotionManager::Stop, "Stop");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef bool(nspi::iMotionManager::*nspi_iMotionManager_IsActive_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iMotionManager_IsActive_FuncPtr, nspi::iMotionManager> nspi_iMotionManager_IsActive_MethodClass;
            SmartPtr<nspi_iMotionManager_IsActive_MethodClass> m = new nspi_iMotionManager_IsActive_MethodClass(&nspi::iMotionManager::IsActive, "IsActive");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef bool(nspi::iMotionManager::*nspi_iMotionManager_IsAvailable_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iMotionManager_IsAvailable_FuncPtr, nspi::iMotionManager> nspi_iMotionManager_IsAvailable_MethodClass;
            SmartPtr<nspi_iMotionManager_IsAvailable_MethodClass> m = new nspi_iMotionManager_IsAvailable_MethodClass(&nspi::iMotionManager::IsAvailable, "IsAvailable");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef iDeviceMotion*(nspi::iMotionManager::*nspi_iMotionManager_GetMotion_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iMotionManager_GetMotion_FuncPtr, nspi::iMotionManager> nspi_iMotionManager_GetMotion_MethodClass;
            SmartPtr<nspi_iMotionManager_GetMotion_MethodClass> m = new nspi_iMotionManager_GetMotion_MethodClass(&nspi::iMotionManager::GetMotion, "GetMotion");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

    }

    virtual ~nspi_iMotionManager_Class()
    {
    }

    virtual Var CreateInstance()
    {
        return nspi::CreateMotionManager();
    }
    virtual bool IsArray() const
    {
        return false;
    }
    virtual iClass* GetElementClass() const
    {
        return nullptr;
    }
    virtual iClass* GetParent() const
    {
        return nullptr;
    }
};

static SmartPtr<iClass> g_nspi_iMotionManager_Class;

iClass* iMotionManager::StaticClass()
{
    return g_nspi_iMotionManager_Class;
}

iClass* iMotionManager::GetClass() const
{
    return g_nspi_iMotionManager_Class;
}

std::string iMotionManager::ToString() const
{
    return piFormat("%s(%p)", GetClass()->GetFullName().c_str(), this);
}

static struct nspi_iMotionManager_Class_Reg
{
    nspi_iMotionManager_Class_Reg()
    {
        g_nspi_iMotionManager_Class = new nspi_iMotionManager_Class("iMotionManager", "nspi::iMotionManager", piGetRootClassLoader());
        g_nspi_iMotionManager_Class->SetReadonly();
        piGetRootClassLoader()->RegisterClass("nspi::iMotionManager", g_nspi_iMotionManager_Class);
        piGetRootClassLoader()->RegisterClass("MotionManager", g_nspi_iMotionManager_Class);
    }
} g_nspi_iMotionManager_Class_RegInstance;



/**
 * ==========================================================================
 * eAccuracy
 */

SmartPtr<iEnum> g_nspi_eAccuracy_Enum;

static struct nspi_eAccuracy_Enum
{
    nspi_eAccuracy_Enum()
    {
        g_nspi_eAccuracy_Enum = CreateEnum();
        g_nspi_eAccuracy_Enum->SetName("eAccuracy");
        g_nspi_eAccuracy_Enum->SetFullName("nspi::eAccuracy");
        g_nspi_eAccuracy_Enum->RegisterValue("High", 3);
        g_nspi_eAccuracy_Enum->RegisterValue("Low", 1);
        g_nspi_eAccuracy_Enum->RegisterValue("Medium", 2);
        g_nspi_eAccuracy_Enum->RegisterValue("None", 0);
        piGetRootClassLoader()->RegisterEnum("nspi::eAccuracy", g_nspi_eAccuracy_Enum);
        piGetRootClassLoader()->RegisterEnum("eAccuracy", g_nspi_eAccuracy_Enum);
    }
} g_nspi_eAccuracy_Enum_Instance;
