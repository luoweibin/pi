/*******************************************************************************
 See copyright notice in LICENSE.

 **DO NOT** edit manually.

 This is generated by pic, which is a c++ code generator for reflection support.
 ******************************************************************************/
#include <pi/core/impl/ReflectionImpl.h>
#include <functional>

using namespace nspi;
using namespace std;

/**
 * ==========================================================================
 * iFaceInfo
 */

class nspi_iFaceInfo_Class : public RefObjectClassImpl
{
public:
    nspi_iFaceInfo_Class(const std::string& simpleName, const std::string& fullName, iClassLoader* classLoader):
    RefObjectClassImpl(simpleName, fullName, classLoader)
    {
        {
            typedef iVec3Array*(nspi::iFaceInfo::*nspi_iFaceInfo_GetPoints_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iFaceInfo_GetPoints_FuncPtr, nspi::iFaceInfo> nspi_iFaceInfo_GetPoints_MethodClass;
            SmartPtr<nspi_iFaceInfo_GetPoints_MethodClass> m = new nspi_iFaceInfo_GetPoints_MethodClass(&nspi::iFaceInfo::GetPoints, "GetPoints");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef rect(nspi::iFaceInfo::*nspi_iFaceInfo_GetRect_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iFaceInfo_GetRect_FuncPtr, nspi::iFaceInfo> nspi_iFaceInfo_GetRect_MethodClass;
            SmartPtr<nspi_iFaceInfo_GetRect_MethodClass> m = new nspi_iFaceInfo_GetRect_MethodClass(&nspi::iFaceInfo::GetRect, "GetRect");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef float(nspi::iFaceInfo::*nspi_iFaceInfo_GetYaw_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iFaceInfo_GetYaw_FuncPtr, nspi::iFaceInfo> nspi_iFaceInfo_GetYaw_MethodClass;
            SmartPtr<nspi_iFaceInfo_GetYaw_MethodClass> m = new nspi_iFaceInfo_GetYaw_MethodClass(&nspi::iFaceInfo::GetYaw, "GetYaw");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef float(nspi::iFaceInfo::*nspi_iFaceInfo_GetPitch_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iFaceInfo_GetPitch_FuncPtr, nspi::iFaceInfo> nspi_iFaceInfo_GetPitch_MethodClass;
            SmartPtr<nspi_iFaceInfo_GetPitch_MethodClass> m = new nspi_iFaceInfo_GetPitch_MethodClass(&nspi::iFaceInfo::GetPitch, "GetPitch");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef float(nspi::iFaceInfo::*nspi_iFaceInfo_GetRoll_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iFaceInfo_GetRoll_FuncPtr, nspi::iFaceInfo> nspi_iFaceInfo_GetRoll_MethodClass;
            SmartPtr<nspi_iFaceInfo_GetRoll_MethodClass> m = new nspi_iFaceInfo_GetRoll_MethodClass(&nspi::iFaceInfo::GetRoll, "GetRoll");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef float(nspi::iFaceInfo::*nspi_iFaceInfo_GetEyeDistance_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iFaceInfo_GetEyeDistance_FuncPtr, nspi::iFaceInfo> nspi_iFaceInfo_GetEyeDistance_MethodClass;
            SmartPtr<nspi_iFaceInfo_GetEyeDistance_MethodClass> m = new nspi_iFaceInfo_GetEyeDistance_MethodClass(&nspi::iFaceInfo::GetEyeDistance, "GetEyeDistance");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef int32_t(nspi::iFaceInfo::*nspi_iFaceInfo_GetTrackID_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iFaceInfo_GetTrackID_FuncPtr, nspi::iFaceInfo> nspi_iFaceInfo_GetTrackID_MethodClass;
            SmartPtr<nspi_iFaceInfo_GetTrackID_MethodClass> m = new nspi_iFaceInfo_GetTrackID_MethodClass(&nspi::iFaceInfo::GetTrackID, "GetTrackID");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef int32_t(nspi::iFaceInfo::*nspi_iFaceInfo_GetActionFlags_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iFaceInfo_GetActionFlags_FuncPtr, nspi::iFaceInfo> nspi_iFaceInfo_GetActionFlags_MethodClass;
            SmartPtr<nspi_iFaceInfo_GetActionFlags_MethodClass> m = new nspi_iFaceInfo_GetActionFlags_MethodClass(&nspi::iFaceInfo::GetActionFlags, "GetActionFlags");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef bool(nspi::iFaceInfo::*nspi_iFaceInfo_ActionFlagsIs_FuncPtr)(int32_t) const;
            typedef RefObjectMethod1<nspi_iFaceInfo_ActionFlagsIs_FuncPtr, nspi::iFaceInfo, int32_t> nspi_iFaceInfo_ActionFlagsIs_MethodClass;
            SmartPtr<nspi_iFaceInfo_ActionFlagsIs_MethodClass> m = new nspi_iFaceInfo_ActionFlagsIs_MethodClass(&nspi::iFaceInfo::ActionFlagsIs, "ActionFlagsIs");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

    }

    virtual ~nspi_iFaceInfo_Class()
    {
    }

    virtual Var CreateInstance()
    {
        return nspi::CreateFaceInfo();
    }
    virtual bool IsArray() const
    {
        return false;
    }
    virtual iClass* GetElementClass() const
    {
        return nullptr;
    }
    virtual iClass* GetParent() const
    {
        return nullptr;
    }
};

static SmartPtr<iClass> g_nspi_iFaceInfo_Class;

iClass* iFaceInfo::StaticClass()
{
    return g_nspi_iFaceInfo_Class;
}

iClass* iFaceInfo::GetClass() const
{
    return g_nspi_iFaceInfo_Class;
}

std::string iFaceInfo::ToString() const
{
    return piFormat("%s(%p)", GetClass()->GetFullName().c_str(), this);
}

static struct nspi_iFaceInfo_Class_Reg
{
    nspi_iFaceInfo_Class_Reg()
    {
        g_nspi_iFaceInfo_Class = new nspi_iFaceInfo_Class("iFaceInfo", "nspi::iFaceInfo", piGetRootClassLoader());
        g_nspi_iFaceInfo_Class->SetReadonly();
        piGetRootClassLoader()->RegisterClass("nspi::iFaceInfo", g_nspi_iFaceInfo_Class);
        piGetRootClassLoader()->RegisterClass("FaceInfo", g_nspi_iFaceInfo_Class);
    }
} g_nspi_iFaceInfo_Class_RegInstance;



/**
 * ==========================================================================
 * iHandInfo
 */

class nspi_iHandInfo_Class : public RefObjectClassImpl
{
public:
    nspi_iHandInfo_Class(const std::string& simpleName, const std::string& fullName, iClassLoader* classLoader):
    RefObjectClassImpl(simpleName, fullName, classLoader)
    {
        {
            typedef rect(nspi::iHandInfo::*nspi_iHandInfo_GetRect_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iHandInfo_GetRect_FuncPtr, nspi::iHandInfo> nspi_iHandInfo_GetRect_MethodClass;
            SmartPtr<nspi_iHandInfo_GetRect_MethodClass> m = new nspi_iHandInfo_GetRect_MethodClass(&nspi::iHandInfo::GetRect, "GetRect");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef vec3(nspi::iHandInfo::*nspi_iHandInfo_GetPoint_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iHandInfo_GetPoint_FuncPtr, nspi::iHandInfo> nspi_iHandInfo_GetPoint_MethodClass;
            SmartPtr<nspi_iHandInfo_GetPoint_MethodClass> m = new nspi_iHandInfo_GetPoint_MethodClass(&nspi::iHandInfo::GetPoint, "GetPoint");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef int32_t(nspi::iHandInfo::*nspi_iHandInfo_GetActionFlags_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iHandInfo_GetActionFlags_FuncPtr, nspi::iHandInfo> nspi_iHandInfo_GetActionFlags_MethodClass;
            SmartPtr<nspi_iHandInfo_GetActionFlags_MethodClass> m = new nspi_iHandInfo_GetActionFlags_MethodClass(&nspi::iHandInfo::GetActionFlags, "GetActionFlags");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef bool(nspi::iHandInfo::*nspi_iHandInfo_ActionFlagsIs_FuncPtr)(int32_t) const;
            typedef RefObjectMethod1<nspi_iHandInfo_ActionFlagsIs_FuncPtr, nspi::iHandInfo, int32_t> nspi_iHandInfo_ActionFlagsIs_MethodClass;
            SmartPtr<nspi_iHandInfo_ActionFlagsIs_MethodClass> m = new nspi_iHandInfo_ActionFlagsIs_MethodClass(&nspi::iHandInfo::ActionFlagsIs, "ActionFlagsIs");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef float(nspi::iHandInfo::*nspi_iHandInfo_GetScore_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iHandInfo_GetScore_FuncPtr, nspi::iHandInfo> nspi_iHandInfo_GetScore_MethodClass;
            SmartPtr<nspi_iHandInfo_GetScore_MethodClass> m = new nspi_iHandInfo_GetScore_MethodClass(&nspi::iHandInfo::GetScore, "GetScore");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef float(nspi::iHandInfo::*nspi_iHandInfo_GetActionScore_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iHandInfo_GetActionScore_FuncPtr, nspi::iHandInfo> nspi_iHandInfo_GetActionScore_MethodClass;
            SmartPtr<nspi_iHandInfo_GetActionScore_MethodClass> m = new nspi_iHandInfo_GetActionScore_MethodClass(&nspi::iHandInfo::GetActionScore, "GetActionScore");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

    }

    virtual ~nspi_iHandInfo_Class()
    {
    }

    virtual Var CreateInstance()
    {
        return nspi::CreateHandInfo();
    }
    virtual bool IsArray() const
    {
        return false;
    }
    virtual iClass* GetElementClass() const
    {
        return nullptr;
    }
    virtual iClass* GetParent() const
    {
        return nullptr;
    }
};

static SmartPtr<iClass> g_nspi_iHandInfo_Class;

iClass* iHandInfo::StaticClass()
{
    return g_nspi_iHandInfo_Class;
}

iClass* iHandInfo::GetClass() const
{
    return g_nspi_iHandInfo_Class;
}

std::string iHandInfo::ToString() const
{
    return piFormat("%s(%p)", GetClass()->GetFullName().c_str(), this);
}

static struct nspi_iHandInfo_Class_Reg
{
    nspi_iHandInfo_Class_Reg()
    {
        g_nspi_iHandInfo_Class = new nspi_iHandInfo_Class("iHandInfo", "nspi::iHandInfo", piGetRootClassLoader());
        g_nspi_iHandInfo_Class->SetReadonly();
        piGetRootClassLoader()->RegisterClass("nspi::iHandInfo", g_nspi_iHandInfo_Class);
        piGetRootClassLoader()->RegisterClass("HandInfo", g_nspi_iHandInfo_Class);
    }
} g_nspi_iHandInfo_Class_RegInstance;



/**
 * ==========================================================================
 * iFaceTrackerResult
 */

class nspi_iFaceTrackerResult_Class : public RefObjectClassImpl
{
public:
    nspi_iFaceTrackerResult_Class(const std::string& simpleName, const std::string& fullName, iClassLoader* classLoader):
    RefObjectClassImpl(simpleName, fullName, classLoader)
    {
        {
            typedef int32_t(nspi::iFaceTrackerResult::*nspi_iFaceTrackerResult_GetFaceCount_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iFaceTrackerResult_GetFaceCount_FuncPtr, nspi::iFaceTrackerResult> nspi_iFaceTrackerResult_GetFaceCount_MethodClass;
            SmartPtr<nspi_iFaceTrackerResult_GetFaceCount_MethodClass> m = new nspi_iFaceTrackerResult_GetFaceCount_MethodClass(&nspi::iFaceTrackerResult::GetFaceCount, "GetFaceCount");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef iFaceInfo*(nspi::iFaceTrackerResult::*nspi_iFaceTrackerResult_GetFace_FuncPtr)(int32_t) const;
            typedef RefObjectMethod1<nspi_iFaceTrackerResult_GetFace_FuncPtr, nspi::iFaceTrackerResult, int32_t> nspi_iFaceTrackerResult_GetFace_MethodClass;
            SmartPtr<nspi_iFaceTrackerResult_GetFace_MethodClass> m = new nspi_iFaceTrackerResult_GetFace_MethodClass(&nspi::iFaceTrackerResult::GetFace, "GetFace");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef iFaceInfo*(nspi::iFaceTrackerResult::*nspi_iFaceTrackerResult_FindFaceByTrackID_FuncPtr)(int32_t) const;
            typedef RefObjectMethod1<nspi_iFaceTrackerResult_FindFaceByTrackID_FuncPtr, nspi::iFaceTrackerResult, int32_t> nspi_iFaceTrackerResult_FindFaceByTrackID_MethodClass;
            SmartPtr<nspi_iFaceTrackerResult_FindFaceByTrackID_MethodClass> m = new nspi_iFaceTrackerResult_FindFaceByTrackID_MethodClass(&nspi::iFaceTrackerResult::FindFaceByTrackID, "FindFaceByTrackID");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef int32_t(nspi::iFaceTrackerResult::*nspi_iFaceTrackerResult_GetHandCount_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iFaceTrackerResult_GetHandCount_FuncPtr, nspi::iFaceTrackerResult> nspi_iFaceTrackerResult_GetHandCount_MethodClass;
            SmartPtr<nspi_iFaceTrackerResult_GetHandCount_MethodClass> m = new nspi_iFaceTrackerResult_GetHandCount_MethodClass(&nspi::iFaceTrackerResult::GetHandCount, "GetHandCount");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef iHandInfo*(nspi::iFaceTrackerResult::*nspi_iFaceTrackerResult_GetHand_FuncPtr)(int32_t) const;
            typedef RefObjectMethod1<nspi_iFaceTrackerResult_GetHand_FuncPtr, nspi::iFaceTrackerResult, int32_t> nspi_iFaceTrackerResult_GetHand_MethodClass;
            SmartPtr<nspi_iFaceTrackerResult_GetHand_MethodClass> m = new nspi_iFaceTrackerResult_GetHand_MethodClass(&nspi::iFaceTrackerResult::GetHand, "GetHand");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef iGraphicsObject*(nspi::iFaceTrackerResult::*nspi_iFaceTrackerResult_GetBgMask_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iFaceTrackerResult_GetBgMask_FuncPtr, nspi::iFaceTrackerResult> nspi_iFaceTrackerResult_GetBgMask_MethodClass;
            SmartPtr<nspi_iFaceTrackerResult_GetBgMask_MethodClass> m = new nspi_iFaceTrackerResult_GetBgMask_MethodClass(&nspi::iFaceTrackerResult::GetBgMask, "GetBgMask");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef bool(nspi::iFaceTrackerResult::*nspi_iFaceTrackerResult_HasBgMask_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iFaceTrackerResult_HasBgMask_FuncPtr, nspi::iFaceTrackerResult> nspi_iFaceTrackerResult_HasBgMask_MethodClass;
            SmartPtr<nspi_iFaceTrackerResult_HasBgMask_MethodClass> m = new nspi_iFaceTrackerResult_HasBgMask_MethodClass(&nspi::iFaceTrackerResult::HasBgMask, "HasBgMask");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef mat4(nspi::iFaceTrackerResult::*nspi_iFaceTrackerResult_GetBgMaskMatrix_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iFaceTrackerResult_GetBgMaskMatrix_FuncPtr, nspi::iFaceTrackerResult> nspi_iFaceTrackerResult_GetBgMaskMatrix_MethodClass;
            SmartPtr<nspi_iFaceTrackerResult_GetBgMaskMatrix_MethodClass> m = new nspi_iFaceTrackerResult_GetBgMaskMatrix_MethodClass(&nspi::iFaceTrackerResult::GetBgMaskMatrix, "GetBgMaskMatrix");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef int32_t(nspi::iFaceTrackerResult::*nspi_iFaceTrackerResult_GetImageWidth_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iFaceTrackerResult_GetImageWidth_FuncPtr, nspi::iFaceTrackerResult> nspi_iFaceTrackerResult_GetImageWidth_MethodClass;
            SmartPtr<nspi_iFaceTrackerResult_GetImageWidth_MethodClass> m = new nspi_iFaceTrackerResult_GetImageWidth_MethodClass(&nspi::iFaceTrackerResult::GetImageWidth, "GetImageWidth");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef int32_t(nspi::iFaceTrackerResult::*nspi_iFaceTrackerResult_GetImageHeight_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iFaceTrackerResult_GetImageHeight_FuncPtr, nspi::iFaceTrackerResult> nspi_iFaceTrackerResult_GetImageHeight_MethodClass;
            SmartPtr<nspi_iFaceTrackerResult_GetImageHeight_MethodClass> m = new nspi_iFaceTrackerResult_GetImageHeight_MethodClass(&nspi::iFaceTrackerResult::GetImageHeight, "GetImageHeight");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

    }

    virtual ~nspi_iFaceTrackerResult_Class()
    {
    }

    virtual Var CreateInstance()
    {
        return nspi::CreateFaceTrackerResult();
    }
    virtual bool IsArray() const
    {
        return false;
    }
    virtual iClass* GetElementClass() const
    {
        return nullptr;
    }
    virtual iClass* GetParent() const
    {
        return nullptr;
    }
};

static SmartPtr<iClass> g_nspi_iFaceTrackerResult_Class;

iClass* iFaceTrackerResult::StaticClass()
{
    return g_nspi_iFaceTrackerResult_Class;
}

iClass* iFaceTrackerResult::GetClass() const
{
    return g_nspi_iFaceTrackerResult_Class;
}

std::string iFaceTrackerResult::ToString() const
{
    return piFormat("%s(%p)", GetClass()->GetFullName().c_str(), this);
}

static struct nspi_iFaceTrackerResult_Class_Reg
{
    nspi_iFaceTrackerResult_Class_Reg()
    {
        g_nspi_iFaceTrackerResult_Class = new nspi_iFaceTrackerResult_Class("iFaceTrackerResult", "nspi::iFaceTrackerResult", piGetRootClassLoader());
        g_nspi_iFaceTrackerResult_Class->SetReadonly();
        piGetRootClassLoader()->RegisterClass("nspi::iFaceTrackerResult", g_nspi_iFaceTrackerResult_Class);
        piGetRootClassLoader()->RegisterClass("FaceTrackerResult", g_nspi_iFaceTrackerResult_Class);
    }
} g_nspi_iFaceTrackerResult_Class_RegInstance;



/**
 * ==========================================================================
 * eHumanAction
 */

SmartPtr<iEnum> g_nspi_eHumanAction_Enum;

static struct nspi_eHumanAction_Enum
{
    nspi_eHumanAction_Enum()
    {
        g_nspi_eHumanAction_Enum = CreateEnum();
        g_nspi_eHumanAction_Enum->SetName("eHumanAction");
        g_nspi_eHumanAction_Enum->SetFullName("nspi::eHumanAction");
        g_nspi_eHumanAction_Enum->RegisterValue("BrowJump", 16);
        g_nspi_eHumanAction_Enum->RegisterValue("EyeBlink", 1);
        g_nspi_eHumanAction_Enum->RegisterValue("FingerHeart", 1024);
        g_nspi_eHumanAction_Enum->RegisterValue("HandCong", 512);
        g_nspi_eHumanAction_Enum->RegisterValue("HandGood", 32);
        g_nspi_eHumanAction_Enum->RegisterValue("HandHoldup", 256);
        g_nspi_eHumanAction_Enum->RegisterValue("HandLove", 128);
        g_nspi_eHumanAction_Enum->RegisterValue("HandPalm", 64);
        g_nspi_eHumanAction_Enum->RegisterValue("HeadPitch", 8);
        g_nspi_eHumanAction_Enum->RegisterValue("HeadYaw", 4);
        g_nspi_eHumanAction_Enum->RegisterValue("Kiss", 2048);
        g_nspi_eHumanAction_Enum->RegisterValue("MouthOpen", 2);
        piGetRootClassLoader()->RegisterEnum("nspi::eHumanAction", g_nspi_eHumanAction_Enum);
        piGetRootClassLoader()->RegisterEnum("eHumanAction", g_nspi_eHumanAction_Enum);
    }
} g_nspi_eHumanAction_Enum_Instance;
/**
 * ==========================================================================
 * eOrient
 */

SmartPtr<iEnum> g_nspi_eOrient_Enum;

static struct nspi_eOrient_Enum
{
    nspi_eOrient_Enum()
    {
        g_nspi_eOrient_Enum = CreateEnum();
        g_nspi_eOrient_Enum->SetName("eOrient");
        g_nspi_eOrient_Enum->SetFullName("nspi::eOrient");
        g_nspi_eOrient_Enum->RegisterValue("0", 0);
        g_nspi_eOrient_Enum->RegisterValue("180", 180);
        g_nspi_eOrient_Enum->RegisterValue("270", 270);
        g_nspi_eOrient_Enum->RegisterValue("90", 90);
        piGetRootClassLoader()->RegisterEnum("nspi::eOrient", g_nspi_eOrient_Enum);
        piGetRootClassLoader()->RegisterEnum("eOrient", g_nspi_eOrient_Enum);
    }
} g_nspi_eOrient_Enum_Instance;
/**
 * ==========================================================================
 * eFaceTracker
 */

SmartPtr<iEnum> g_nspi_eFaceTracker_Enum;

static struct nspi_eFaceTracker_Enum
{
    nspi_eFaceTracker_Enum()
    {
        g_nspi_eFaceTracker_Enum = CreateEnum();
        g_nspi_eFaceTracker_Enum->SetName("eFaceTracker");
        g_nspi_eFaceTracker_Enum->SetFullName("nspi::eFaceTracker");
        g_nspi_eFaceTracker_Enum->RegisterValue("Bg", 2);
        g_nspi_eFaceTracker_Enum->RegisterValue("Face", 1);
        g_nspi_eFaceTracker_Enum->RegisterValue("FaceAction", 4);
        g_nspi_eFaceTracker_Enum->RegisterValue("HandAction", 8);
        piGetRootClassLoader()->RegisterEnum("nspi::eFaceTracker", g_nspi_eFaceTracker_Enum);
        piGetRootClassLoader()->RegisterEnum("eFaceTracker", g_nspi_eFaceTracker_Enum);
    }
} g_nspi_eFaceTracker_Enum_Instance;
