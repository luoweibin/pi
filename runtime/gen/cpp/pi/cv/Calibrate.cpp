/*******************************************************************************
 See copyright notice in LICENSE.

 **DO NOT** edit manually.

 This is generated by pic, which is a c++ code generator for reflection support.
 ******************************************************************************/
#include <pi/core/impl/ReflectionImpl.h>
#include <functional>

using namespace nspi;
using namespace std;

/**
 * ==========================================================================
 * iCalibrate
 */

class nspi_iCalibrate_Class : public RefObjectClassImpl
{
public:
    nspi_iCalibrate_Class(const std::string& simpleName, const std::string& fullName, iClassLoader* classLoader):
    RefObjectClassImpl(simpleName, fullName, classLoader)
    {
    }

    virtual ~nspi_iCalibrate_Class()
    {
    }

    virtual Var CreateInstance()
    {
        return nspi::CreateCalibrate();
    }
    virtual bool IsArray() const
    {
        return false;
    }
    virtual iClass* GetElementClass() const
    {
        return nullptr;
    }
    virtual iClass* GetParent() const
    {
        return nullptr;
    }
};

static SmartPtr<iClass> g_nspi_iCalibrate_Class;

iClass* iCalibrate::StaticClass()
{
    return g_nspi_iCalibrate_Class;
}

iClass* iCalibrate::GetClass() const
{
    return g_nspi_iCalibrate_Class;
}

std::string iCalibrate::ToString() const
{
    return piFormat("%s(%p)", GetClass()->GetFullName().c_str(), this);
}

static struct nspi_iCalibrate_Class_Reg
{
    nspi_iCalibrate_Class_Reg()
    {
        g_nspi_iCalibrate_Class = new nspi_iCalibrate_Class("iCalibrate", "nspi::iCalibrate", piGetRootClassLoader());
        g_nspi_iCalibrate_Class->SetReadonly();
        piGetRootClassLoader()->RegisterClass("nspi::iCalibrate", g_nspi_iCalibrate_Class);
        piGetRootClassLoader()->RegisterClass("Calibrate", g_nspi_iCalibrate_Class);
    }
} g_nspi_iCalibrate_Class_RegInstance;



