/*******************************************************************************
 See copyright notice in LICENSE.

 **DO NOT** edit manually.

 This is generated by pic, which is a c++ code generator for reflection support.
 ******************************************************************************/
#include <pi/core/impl/ReflectionImpl.h>
#include <functional>

using namespace nspi;
using namespace std;

/**
 * ==========================================================================
 * iModelNode
 */

class nspi_iModelNode_Class : public RefObjectClassImpl
{
public:
    nspi_iModelNode_Class(const std::string& simpleName, const std::string& fullName, iClassLoader* classLoader):
    RefObjectClassImpl(simpleName, fullName, classLoader)
    {
        {
            SmartPtr<RefObjectProperty<nspi::iModelNode, int, int>> p = new RefObjectProperty<nspi::iModelNode, int, int>("Type", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iModelNode::GetType);
            p->SetSetter(&nspi::iModelNode::SetType);
            p->SetConfig("Class", "eModelNode");
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iModelNode, const std::string&, std::string>> p = new RefObjectProperty<nspi::iModelNode, const std::string&, std::string>("Name", []() {return PrimitiveClass::String();});
            p->SetGetter(&nspi::iModelNode::GetName);
            p->SetSetter(&nspi::iModelNode::SetName);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iModelNode, iModelNodeArray*, iModelNodeArray*>> p = new RefObjectProperty<nspi::iModelNode, iModelNodeArray*, iModelNodeArray*>("Children", []() {return nspi::iModelNodeArray::StaticClass();});
            p->SetGetter(&nspi::iModelNode::GetChildren);
            p->SetSetter(&nspi::iModelNode::SetChildren);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iModelNode, iTransform*, iTransform*>> p = new RefObjectProperty<nspi::iModelNode, iTransform*, iTransform*>("Transform", []() {return nspi::iTransform::StaticClass();});
            p->SetGetter(&nspi::iModelNode::GetTransform);
            p->SetSetter(&nspi::iModelNode::SetTransform);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iModelNode, iModelNode*, iModelNode*>> p = new RefObjectProperty<nspi::iModelNode, iModelNode*, iModelNode*>("Parent", []() {return nspi::iModelNode::StaticClass();});
            p->SetGetter(&nspi::iModelNode::GetParent);
            p->SetSetter(&nspi::iModelNode::SetParent);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iModelNode, iModelMeshArray*, iModelMeshArray*>> p = new RefObjectProperty<nspi::iModelNode, iModelMeshArray*, iModelMeshArray*>("Meshes", []() {return nspi::iModelMeshArray::StaticClass();});
            p->SetGetter(&nspi::iModelNode::GetMeshes);
            p->SetSetter(&nspi::iModelNode::SetMeshes);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iModelNode, iModelSkinArray*, iModelSkinArray*>> p = new RefObjectProperty<nspi::iModelNode, iModelSkinArray*, iModelSkinArray*>("Skins", []() {return nspi::iModelSkinArray::StaticClass();});
            p->SetGetter(&nspi::iModelNode::GetSkins);
            p->SetSetter(&nspi::iModelNode::SetSkins);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            typedef mat4(nspi::iModelNode::*nspi_iModelNode_EvaluateGlobalMatrix_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iModelNode_EvaluateGlobalMatrix_FuncPtr, nspi::iModelNode> nspi_iModelNode_EvaluateGlobalMatrix_MethodClass;
            SmartPtr<nspi_iModelNode_EvaluateGlobalMatrix_MethodClass> m = new nspi_iModelNode_EvaluateGlobalMatrix_MethodClass(&nspi::iModelNode::EvaluateGlobalMatrix, "EvaluateGlobalMatrix");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

    }

    virtual ~nspi_iModelNode_Class()
    {
    }

    virtual Var CreateInstance()
    {
        return nspi::CreateModelNode();
    }
    virtual bool IsArray() const
    {
        return false;
    }
    virtual iClass* GetElementClass() const
    {
        return nullptr;
    }
    virtual iClass* GetParent() const
    {
        return nullptr;
    }
};

static SmartPtr<iClass> g_nspi_iModelNode_Class;

iClass* iModelNode::StaticClass()
{
    return g_nspi_iModelNode_Class;
}

iClass* iModelNode::GetClass() const
{
    return g_nspi_iModelNode_Class;
}

std::string iModelNode::ToString() const
{
    return piFormat("%s(%p)", GetClass()->GetFullName().c_str(), this);
}

static struct nspi_iModelNode_Class_Reg
{
    nspi_iModelNode_Class_Reg()
    {
        g_nspi_iModelNode_Class = new nspi_iModelNode_Class("iModelNode", "nspi::iModelNode", piGetRootClassLoader());
        g_nspi_iModelNode_Class->SetReadonly();
        piGetRootClassLoader()->RegisterClass("nspi::iModelNode", g_nspi_iModelNode_Class);
        piGetRootClassLoader()->RegisterClass("ModelNode", g_nspi_iModelNode_Class);
    }
} g_nspi_iModelNode_Class_RegInstance;



/**
 * ==========================================================================
 * iModelNodeArray
 */

class nspi_iModelNodeArray_Class : public RefObjectClassImpl
{
public:
    nspi_iModelNodeArray_Class(const std::string& simpleName, const std::string& fullName, iClassLoader* classLoader):
    RefObjectClassImpl(simpleName, fullName, classLoader)
    {
        {
            typedef bool(nspi::iModelNodeArray::*nspi_iModelNodeArray_IsEmpty_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iModelNodeArray_IsEmpty_FuncPtr, nspi::iModelNodeArray> nspi_iModelNodeArray_IsEmpty_MethodClass;
            SmartPtr<nspi_iModelNodeArray_IsEmpty_MethodClass> m = new nspi_iModelNodeArray_IsEmpty_MethodClass(&nspi::iModelNodeArray::IsEmpty, "IsEmpty");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef iModelNode*(nspi::iModelNodeArray::*nspi_iModelNodeArray_GetBack_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iModelNodeArray_GetBack_FuncPtr, nspi::iModelNodeArray> nspi_iModelNodeArray_GetBack_MethodClass;
            SmartPtr<nspi_iModelNodeArray_GetBack_MethodClass> m = new nspi_iModelNodeArray_GetBack_MethodClass(&nspi::iModelNodeArray::GetBack, "GetBack");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef iModelNode*(nspi::iModelNodeArray::*nspi_iModelNodeArray_GetFront_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iModelNodeArray_GetFront_FuncPtr, nspi::iModelNodeArray> nspi_iModelNodeArray_GetFront_MethodClass;
            SmartPtr<nspi_iModelNodeArray_GetFront_MethodClass> m = new nspi_iModelNodeArray_GetFront_MethodClass(&nspi::iModelNodeArray::GetFront, "GetFront");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef void(nspi::iModelNodeArray::*nspi_iModelNodeArray_PushBack_FuncPtr)(iModelNode*);
            typedef RefObjectMethod1Void<nspi_iModelNodeArray_PushBack_FuncPtr, nspi::iModelNodeArray, iModelNode*> nspi_iModelNodeArray_PushBack_MethodClass;
            SmartPtr<nspi_iModelNodeArray_PushBack_MethodClass> m = new nspi_iModelNodeArray_PushBack_MethodClass(&nspi::iModelNodeArray::PushBack, "PushBack");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef void(nspi::iModelNodeArray::*nspi_iModelNodeArray_PopBack_FuncPtr)();
            typedef RefObjectMethodVoidVoid<nspi_iModelNodeArray_PopBack_FuncPtr, nspi::iModelNodeArray> nspi_iModelNodeArray_PopBack_MethodClass;
            SmartPtr<nspi_iModelNodeArray_PopBack_MethodClass> m = new nspi_iModelNodeArray_PopBack_MethodClass(&nspi::iModelNodeArray::PopBack, "PopBack");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef int32_t(nspi::iModelNodeArray::*nspi_iModelNodeArray_GetCount_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iModelNodeArray_GetCount_FuncPtr, nspi::iModelNodeArray> nspi_iModelNodeArray_GetCount_MethodClass;
            SmartPtr<nspi_iModelNodeArray_GetCount_MethodClass> m = new nspi_iModelNodeArray_GetCount_MethodClass(&nspi::iModelNodeArray::GetCount, "GetCount");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef iModelNode*(nspi::iModelNodeArray::*nspi_iModelNodeArray_GetItem_FuncPtr)(int32_t) const;
            typedef RefObjectMethod1<nspi_iModelNodeArray_GetItem_FuncPtr, nspi::iModelNodeArray, int32_t> nspi_iModelNodeArray_GetItem_MethodClass;
            SmartPtr<nspi_iModelNodeArray_GetItem_MethodClass> m = new nspi_iModelNodeArray_GetItem_MethodClass(&nspi::iModelNodeArray::GetItem, "GetItem");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef void(nspi::iModelNodeArray::*nspi_iModelNodeArray_SetItem_FuncPtr)(int32_t,iModelNode*);
            typedef RefObjectMethod2Void<nspi_iModelNodeArray_SetItem_FuncPtr, nspi::iModelNodeArray, int32_t, iModelNode*> nspi_iModelNodeArray_SetItem_MethodClass;
            SmartPtr<nspi_iModelNodeArray_SetItem_MethodClass> m = new nspi_iModelNodeArray_SetItem_MethodClass(&nspi::iModelNodeArray::SetItem, "SetItem");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef void(nspi::iModelNodeArray::*nspi_iModelNodeArray_Resize_FuncPtr)(int32_t);
            typedef RefObjectMethod1Void<nspi_iModelNodeArray_Resize_FuncPtr, nspi::iModelNodeArray, int32_t> nspi_iModelNodeArray_Resize_MethodClass;
            SmartPtr<nspi_iModelNodeArray_Resize_MethodClass> m = new nspi_iModelNodeArray_Resize_MethodClass(&nspi::iModelNodeArray::Resize, "Resize");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef void(nspi::iModelNodeArray::*nspi_iModelNodeArray_Clear_FuncPtr)();
            typedef RefObjectMethodVoidVoid<nspi_iModelNodeArray_Clear_FuncPtr, nspi::iModelNodeArray> nspi_iModelNodeArray_Clear_MethodClass;
            SmartPtr<nspi_iModelNodeArray_Clear_MethodClass> m = new nspi_iModelNodeArray_Clear_MethodClass(&nspi::iModelNodeArray::Clear, "Clear");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef void(nspi::iModelNodeArray::*nspi_iModelNodeArray_Remove_FuncPtr)(int32_t);
            typedef RefObjectMethod1Void<nspi_iModelNodeArray_Remove_FuncPtr, nspi::iModelNodeArray, int32_t> nspi_iModelNodeArray_Remove_MethodClass;
            SmartPtr<nspi_iModelNodeArray_Remove_MethodClass> m = new nspi_iModelNodeArray_Remove_MethodClass(&nspi::iModelNodeArray::Remove, "Remove");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

    }

    virtual ~nspi_iModelNodeArray_Class()
    {
    }

    virtual Var CreateInstance()
    {
        return nspi::CreateModelNodeArray();
    }
    virtual bool IsArray() const
    {
        return true;
    }
    virtual iClass* GetElementClass() const
    {
        return nspi::iModelNode::StaticClass();
    }
    virtual iClass* GetParent() const
    {
        return nullptr;
    }
};

static SmartPtr<iClass> g_nspi_iModelNodeArray_Class;

iClass* iModelNodeArray::StaticClass()
{
    return g_nspi_iModelNodeArray_Class;
}

iClass* iModelNodeArray::GetClass() const
{
    return g_nspi_iModelNodeArray_Class;
}

std::string iModelNodeArray::ToString() const
{
    return piFormat("%s(%p)", GetClass()->GetFullName().c_str(), this);
}

static struct nspi_iModelNodeArray_Class_Reg
{
    nspi_iModelNodeArray_Class_Reg()
    {
        g_nspi_iModelNodeArray_Class = new nspi_iModelNodeArray_Class("iModelNodeArray", "nspi::iModelNodeArray", piGetRootClassLoader());
        g_nspi_iModelNodeArray_Class->SetReadonly();
        piGetRootClassLoader()->RegisterClass("nspi::iModelNodeArray", g_nspi_iModelNodeArray_Class);
        piGetRootClassLoader()->RegisterClass("ModelNodeArray", g_nspi_iModelNodeArray_Class);
    }
} g_nspi_iModelNodeArray_Class_RegInstance;

class nspi_iModelNodeArray_Impl : public ObjectArrayImpl<nspi::iModelNode, nspi::iModelNodeArray>
{
public:
    nspi_iModelNodeArray_Impl()
    {
    }

    virtual ~nspi_iModelNodeArray_Impl()
    {
    }

    virtual string ToString()
    {
        return piFormat("nspi::iModelNodeArray[%d]", GetCount());
    }
};

iModelNodeArray* nspi::CreateModelNodeArray()
{
    return new nspi_iModelNodeArray_Impl();
}


/**
 * ==========================================================================
 * eModelNode
 */

SmartPtr<iEnum> g_nspi_eModelNode_Enum;

static struct nspi_eModelNode_Enum
{
    nspi_eModelNode_Enum()
    {
        g_nspi_eModelNode_Enum = CreateEnum();
        g_nspi_eModelNode_Enum->SetName("eModelNode");
        g_nspi_eModelNode_Enum->SetFullName("nspi::eModelNode");
        g_nspi_eModelNode_Enum->RegisterValue("Joint", 1);
        g_nspi_eModelNode_Enum->RegisterValue("Mesh", 3);
        g_nspi_eModelNode_Enum->RegisterValue("Null", 0);
        g_nspi_eModelNode_Enum->RegisterValue("Skin", 2);
        piGetRootClassLoader()->RegisterEnum("nspi::eModelNode", g_nspi_eModelNode_Enum);
        piGetRootClassLoader()->RegisterEnum("eModelNode", g_nspi_eModelNode_Enum);
    }
} g_nspi_eModelNode_Enum_Instance;
