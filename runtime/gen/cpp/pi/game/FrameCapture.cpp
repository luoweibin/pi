/*******************************************************************************
 See copyright notice in LICENSE.

 **DO NOT** edit manually.

 This is generated by pic, which is a c++ code generator for reflection support.
 ******************************************************************************/
#include <pi/core/impl/ReflectionImpl.h>
#include <functional>

using namespace nspi;
using namespace std;

/**
 * ==========================================================================
 * iFrameCaptureSystem
 */

class nspi_iFrameCaptureSystem_Class : public RefObjectClassImpl
{
public:
    nspi_iFrameCaptureSystem_Class(const std::string& simpleName, const std::string& fullName, iClassLoader* classLoader):
    RefObjectClassImpl(simpleName, fullName, classLoader)
    {
    }

    virtual ~nspi_iFrameCaptureSystem_Class()
    {
    }

    virtual Var CreateInstance()
    {
        return nspi::CreateFrameCaptureSystem();
    }
    virtual bool IsArray() const
    {
        return false;
    }
    virtual iClass* GetElementClass() const
    {
        return nullptr;
    }
    virtual iClass* GetParent() const
    {
        return nspi::iSystem::StaticClass();
    }
};

static SmartPtr<iClass> g_nspi_iFrameCaptureSystem_Class;

iClass* iFrameCaptureSystem::StaticClass()
{
    return g_nspi_iFrameCaptureSystem_Class;
}

iClass* iFrameCaptureSystem::GetClass() const
{
    return g_nspi_iFrameCaptureSystem_Class;
}

std::string iFrameCaptureSystem::ToString() const
{
    return piFormat("%s(%p)", GetClass()->GetFullName().c_str(), this);
}

static struct nspi_iFrameCaptureSystem_Class_Reg
{
    nspi_iFrameCaptureSystem_Class_Reg()
    {
        g_nspi_iFrameCaptureSystem_Class = new nspi_iFrameCaptureSystem_Class("iFrameCaptureSystem", "nspi::iFrameCaptureSystem", piGetRootClassLoader());
        g_nspi_iFrameCaptureSystem_Class->SetReadonly();
        piGetRootClassLoader()->RegisterClass("nspi::iFrameCaptureSystem", g_nspi_iFrameCaptureSystem_Class);
        piGetRootClassLoader()->RegisterClass("FrameCaptureSystem", g_nspi_iFrameCaptureSystem_Class);
    }
} g_nspi_iFrameCaptureSystem_Class_RegInstance;



