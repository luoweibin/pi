/*******************************************************************************
 See copyright notice in LICENSE.

 **DO NOT** edit manually.

 This is generated by pic, which is a c++ code generator for reflection support.
 ******************************************************************************/
#include <pi/core/impl/ReflectionImpl.h>
#include <functional>

using namespace nspi;
using namespace std;

/**
 * ==========================================================================
 * iParticleEmitterLib
 */

class nspi_iParticleEmitterLib_Class : public RefObjectClassImpl
{
public:
    nspi_iParticleEmitterLib_Class(const std::string& simpleName, const std::string& fullName, iClassLoader* classLoader):
    RefObjectClassImpl(simpleName, fullName, classLoader)
    {
        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, int, int>> p = new RefObjectProperty<nspi::iParticleEmitterLib, int, int>("ParticleType", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetParticleType);
            p->SetSetter(&nspi::iParticleEmitterLib::SetParticleType);
            p->SetConfig("Class", "eParticleType");
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, int, int>> p = new RefObjectProperty<nspi::iParticleEmitterLib, int, int>("ParticlePlayType", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetParticlePlayType);
            p->SetSetter(&nspi::iParticleEmitterLib::SetParticlePlayType);
            p->SetConfig("Class", "eParticlePlayType");
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, int, int>> p = new RefObjectProperty<nspi::iParticleEmitterLib, int, int>("ParticleBlend", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetParticleBlend);
            p->SetSetter(&nspi::iParticleEmitterLib::SetParticleBlend);
            p->SetConfig("Class", "eParticleBlend");
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, float, float>> p = new RefObjectProperty<nspi::iParticleEmitterLib, float, float>("EmissionRate", []() {return PrimitiveClass::F32();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetEmissionRate);
            p->SetSetter(&nspi::iParticleEmitterLib::SetEmissionRate);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, int, int>> p = new RefObjectProperty<nspi::iParticleEmitterLib, int, int>("MaxNum", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetMaxNum);
            p->SetSetter(&nspi::iParticleEmitterLib::SetMaxNum);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, float, float>> p = new RefObjectProperty<nspi::iParticleEmitterLib, float, float>("Width", []() {return PrimitiveClass::F32();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetWidth);
            p->SetSetter(&nspi::iParticleEmitterLib::SetWidth);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, float, float>> p = new RefObjectProperty<nspi::iParticleEmitterLib, float, float>("Height", []() {return PrimitiveClass::F32();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetHeight);
            p->SetSetter(&nspi::iParticleEmitterLib::SetHeight);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, const std::string&, std::string>> p = new RefObjectProperty<nspi::iParticleEmitterLib, const std::string&, std::string>("TextureUri", []() {return PrimitiveClass::String();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetTextureUri);
            p->SetSetter(&nspi::iParticleEmitterLib::SetTextureUri);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, iTexture2D*, iTexture2D*>> p = new RefObjectProperty<nspi::iParticleEmitterLib, iTexture2D*, iTexture2D*>("Texture2D", []() {return nspi::iTexture2D::StaticClass();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetTexture2D);
            p->SetSetter(&nspi::iParticleEmitterLib::SetTexture2D);
            p->SetConfig("AssetUri", "TextureUri");
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, int, int>> p = new RefObjectProperty<nspi::iParticleEmitterLib, int, int>("FrameSizeX", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetFrameSizeX);
            p->SetSetter(&nspi::iParticleEmitterLib::SetFrameSizeX);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, int, int>> p = new RefObjectProperty<nspi::iParticleEmitterLib, int, int>("FrameSizeY", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetFrameSizeY);
            p->SetSetter(&nspi::iParticleEmitterLib::SetFrameSizeY);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, int, int>> p = new RefObjectProperty<nspi::iParticleEmitterLib, int, int>("FrameCount", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetFrameCount);
            p->SetSetter(&nspi::iParticleEmitterLib::SetFrameCount);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, float, float>> p = new RefObjectProperty<nspi::iParticleEmitterLib, float, float>("FrameDuration", []() {return PrimitiveClass::F32();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetFrameDuration);
            p->SetSetter(&nspi::iParticleEmitterLib::SetFrameDuration);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, int, int>> p = new RefObjectProperty<nspi::iParticleEmitterLib, int, int>("BillboardType", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetBillboardType);
            p->SetSetter(&nspi::iParticleEmitterLib::SetBillboardType);
            p->SetConfig("Class", "eBillboardType");
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>("BornBasePosition", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetBornBasePosition);
            p->SetSetter(&nspi::iParticleEmitterLib::SetBornBasePosition);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, bool, bool>> p = new RefObjectProperty<nspi::iParticleEmitterLib, bool, bool>("PositionOffsetAbs", []() {return PrimitiveClass::Boolean();});
            p->SetGetter(&nspi::iParticleEmitterLib::IsPositionOffsetAbs);
            p->SetSetter(&nspi::iParticleEmitterLib::SetPositionOffsetAbs);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>("BornPositionMinOffset", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetBornPositionMinOffset);
            p->SetSetter(&nspi::iParticleEmitterLib::SetBornPositionMinOffset);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>("BornPositionMaxOffset", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetBornPositionMaxOffset);
            p->SetSetter(&nspi::iParticleEmitterLib::SetBornPositionMaxOffset);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>("BornBaseVelocity", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetBornBaseVelocity);
            p->SetSetter(&nspi::iParticleEmitterLib::SetBornBaseVelocity);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, bool, bool>> p = new RefObjectProperty<nspi::iParticleEmitterLib, bool, bool>("VelocityOffsetAbs", []() {return PrimitiveClass::Boolean();});
            p->SetGetter(&nspi::iParticleEmitterLib::IsVelocityOffsetAbs);
            p->SetSetter(&nspi::iParticleEmitterLib::SetVelocityOffsetAbs);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>("BornVelocityMinOffset", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetBornVelocityMinOffset);
            p->SetSetter(&nspi::iParticleEmitterLib::SetBornVelocityMinOffset);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>("BornVelocityMaxOffset", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetBornVelocityMaxOffset);
            p->SetSetter(&nspi::iParticleEmitterLib::SetBornVelocityMaxOffset);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>("BaseAcceleration", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetBaseAcceleration);
            p->SetSetter(&nspi::iParticleEmitterLib::SetBaseAcceleration);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, bool, bool>> p = new RefObjectProperty<nspi::iParticleEmitterLib, bool, bool>("AccelerationOffsetAbs", []() {return PrimitiveClass::Boolean();});
            p->SetGetter(&nspi::iParticleEmitterLib::IsAccelerationOffsetAbs);
            p->SetSetter(&nspi::iParticleEmitterLib::SetAccelerationOffsetAbs);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>("AccelerationMinOffset", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetAccelerationMinOffset);
            p->SetSetter(&nspi::iParticleEmitterLib::SetAccelerationMinOffset);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>("AccelerationMaxOffset", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetAccelerationMaxOffset);
            p->SetSetter(&nspi::iParticleEmitterLib::SetAccelerationMaxOffset);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>("BornBaseRotation", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetBornBaseRotation);
            p->SetSetter(&nspi::iParticleEmitterLib::SetBornBaseRotation);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, bool, bool>> p = new RefObjectProperty<nspi::iParticleEmitterLib, bool, bool>("RotationOffsetAbs", []() {return PrimitiveClass::Boolean();});
            p->SetGetter(&nspi::iParticleEmitterLib::IsRotationOffsetAbs);
            p->SetSetter(&nspi::iParticleEmitterLib::SetRotationOffsetAbs);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>("BornRotationMinOffset", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetBornRotationMinOffset);
            p->SetSetter(&nspi::iParticleEmitterLib::SetBornRotationMinOffset);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>("BornRotationMaxOffset", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetBornRotationMaxOffset);
            p->SetSetter(&nspi::iParticleEmitterLib::SetBornRotationMaxOffset);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>("BornBaseRotationRate", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetBornBaseRotationRate);
            p->SetSetter(&nspi::iParticleEmitterLib::SetBornBaseRotationRate);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, bool, bool>> p = new RefObjectProperty<nspi::iParticleEmitterLib, bool, bool>("RotationRateOffsetAbs", []() {return PrimitiveClass::Boolean();});
            p->SetGetter(&nspi::iParticleEmitterLib::IsRotationRateOffsetAbs);
            p->SetSetter(&nspi::iParticleEmitterLib::SetRotationRateOffsetAbs);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>("BornRotationRateMinOffset", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetBornRotationRateMinOffset);
            p->SetSetter(&nspi::iParticleEmitterLib::SetBornRotationRateMinOffset);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>("BornRotationRateMaxOffset", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetBornRotationRateMaxOffset);
            p->SetSetter(&nspi::iParticleEmitterLib::SetBornRotationRateMaxOffset);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>("BaseRotationalAcceleration", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetBaseRotationalAcceleration);
            p->SetSetter(&nspi::iParticleEmitterLib::SetBaseRotationalAcceleration);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, bool, bool>> p = new RefObjectProperty<nspi::iParticleEmitterLib, bool, bool>("RotationalAccelerationOffsetAbs", []() {return PrimitiveClass::Boolean();});
            p->SetGetter(&nspi::iParticleEmitterLib::IsRotationalAccelerationOffsetAbs);
            p->SetSetter(&nspi::iParticleEmitterLib::SetRotationalAccelerationOffsetAbs);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>("RotationalAccelerationMinOffset", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetRotationalAccelerationMinOffset);
            p->SetSetter(&nspi::iParticleEmitterLib::SetRotationalAccelerationMinOffset);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticleEmitterLib, const vec3&, vec3>("RotationalAccelerationMaxOffset", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetRotationalAccelerationMaxOffset);
            p->SetSetter(&nspi::iParticleEmitterLib::SetRotationalAccelerationMaxOffset);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, float, float>> p = new RefObjectProperty<nspi::iParticleEmitterLib, float, float>("BaseLife", []() {return PrimitiveClass::F32();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetBaseLife);
            p->SetSetter(&nspi::iParticleEmitterLib::SetBaseLife);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, float, float>> p = new RefObjectProperty<nspi::iParticleEmitterLib, float, float>("LifeMinOffset", []() {return PrimitiveClass::F32();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetLifeMinOffset);
            p->SetSetter(&nspi::iParticleEmitterLib::SetLifeMinOffset);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticleEmitterLib, float, float>> p = new RefObjectProperty<nspi::iParticleEmitterLib, float, float>("LifeMaxOffset", []() {return PrimitiveClass::F32();});
            p->SetGetter(&nspi::iParticleEmitterLib::GetLifeMaxOffset);
            p->SetSetter(&nspi::iParticleEmitterLib::SetLifeMaxOffset);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            typedef bool(nspi::iParticleEmitterLib::*nspi_iParticleEmitterLib_Init_FuncPtr)();
            typedef RefObjectMethodVoid<nspi_iParticleEmitterLib_Init_FuncPtr, nspi::iParticleEmitterLib> nspi_iParticleEmitterLib_Init_MethodClass;
            SmartPtr<nspi_iParticleEmitterLib_Init_MethodClass> m = new nspi_iParticleEmitterLib_Init_MethodClass(&nspi::iParticleEmitterLib::Init, "Init");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

    }

    virtual ~nspi_iParticleEmitterLib_Class()
    {
    }

    virtual Var CreateInstance()
    {
        return nspi::CreateParticleEmitterLib();
    }
    virtual bool IsArray() const
    {
        return false;
    }
    virtual iClass* GetElementClass() const
    {
        return nullptr;
    }
    virtual iClass* GetParent() const
    {
        return nspi::iAsset::StaticClass();
    }
};

static SmartPtr<iClass> g_nspi_iParticleEmitterLib_Class;

iClass* iParticleEmitterLib::StaticClass()
{
    return g_nspi_iParticleEmitterLib_Class;
}

iClass* iParticleEmitterLib::GetClass() const
{
    return g_nspi_iParticleEmitterLib_Class;
}

std::string iParticleEmitterLib::ToString() const
{
    return piFormat("%s(%p)", GetClass()->GetFullName().c_str(), this);
}

static struct nspi_iParticleEmitterLib_Class_Reg
{
    nspi_iParticleEmitterLib_Class_Reg()
    {
        g_nspi_iParticleEmitterLib_Class = new nspi_iParticleEmitterLib_Class("iParticleEmitterLib", "nspi::iParticleEmitterLib", piGetRootClassLoader());
        g_nspi_iParticleEmitterLib_Class->SetReadonly();
        piGetRootClassLoader()->RegisterClass("nspi::iParticleEmitterLib", g_nspi_iParticleEmitterLib_Class);
        piGetRootClassLoader()->RegisterClass("ParticleEmitterLib", g_nspi_iParticleEmitterLib_Class);
        piGetRootClassLoader()->RegisterClass("ParticleEmitterLib", g_nspi_iParticleEmitterLib_Class);
    }
} g_nspi_iParticleEmitterLib_Class_RegInstance;



/**
 * ==========================================================================
 * iParticle
 */

class nspi_iParticle_Class : public RefObjectClassImpl
{
public:
    nspi_iParticle_Class(const std::string& simpleName, const std::string& fullName, iClassLoader* classLoader):
    RefObjectClassImpl(simpleName, fullName, classLoader)
    {
        {
            SmartPtr<RefObjectProperty<nspi::iParticle, float, float>> p = new RefObjectProperty<nspi::iParticle, float, float>("LifeTime", []() {return PrimitiveClass::F32();});
            p->SetGetter(&nspi::iParticle::GetLifeTime);
            p->SetSetter(&nspi::iParticle::SetLifeTime);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticle, float, float>> p = new RefObjectProperty<nspi::iParticle, float, float>("PassTime", []() {return PrimitiveClass::F32();});
            p->SetGetter(&nspi::iParticle::GetPassTime);
            p->SetSetter(&nspi::iParticle::SetPassTime);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticle, bool, bool>> p = new RefObjectProperty<nspi::iParticle, bool, bool>("Alive", []() {return PrimitiveClass::Boolean();});
            p->SetGetter(&nspi::iParticle::IsAlive);
            p->SetSetter(&nspi::iParticle::SetAlive);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticle, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticle, const vec3&, vec3>("Position", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticle::GetPosition);
            p->SetSetter(&nspi::iParticle::SetPosition);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticle, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticle, const vec3&, vec3>("Rotation", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticle::GetRotation);
            p->SetSetter(&nspi::iParticle::SetRotation);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticle, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticle, const vec3&, vec3>("Velocity", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticle::GetVelocity);
            p->SetSetter(&nspi::iParticle::SetVelocity);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticle, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticle, const vec3&, vec3>("Acceleration", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticle::GetAcceleration);
            p->SetSetter(&nspi::iParticle::SetAcceleration);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticle, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticle, const vec3&, vec3>("RotationRate", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticle::GetRotationRate);
            p->SetSetter(&nspi::iParticle::SetRotationRate);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iParticle, const vec3&, vec3>> p = new RefObjectProperty<nspi::iParticle, const vec3&, vec3>("RotationalAcceleration", []() {return PrimitiveClass::Vec3();});
            p->SetGetter(&nspi::iParticle::GetRotationalAcceleration);
            p->SetSetter(&nspi::iParticle::SetRotationalAcceleration);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

    }

    virtual ~nspi_iParticle_Class()
    {
    }

    virtual Var CreateInstance()
    {
        return Var();
    }
    virtual bool IsArray() const
    {
        return false;
    }
    virtual iClass* GetElementClass() const
    {
        return nullptr;
    }
    virtual iClass* GetParent() const
    {
        return nullptr;
    }
};

static SmartPtr<iClass> g_nspi_iParticle_Class;

iClass* iParticle::StaticClass()
{
    return g_nspi_iParticle_Class;
}

iClass* iParticle::GetClass() const
{
    return g_nspi_iParticle_Class;
}

std::string iParticle::ToString() const
{
    return piFormat("%s(%p)", GetClass()->GetFullName().c_str(), this);
}

static struct nspi_iParticle_Class_Reg
{
    nspi_iParticle_Class_Reg()
    {
        g_nspi_iParticle_Class = new nspi_iParticle_Class("iParticle", "nspi::iParticle", piGetRootClassLoader());
        g_nspi_iParticle_Class->SetReadonly();
        piGetRootClassLoader()->RegisterClass("nspi::iParticle", g_nspi_iParticle_Class);
        piGetRootClassLoader()->RegisterClass("Particle", g_nspi_iParticle_Class);
    }
} g_nspi_iParticle_Class_RegInstance;



/**
 * ==========================================================================
 * iQuadParticle
 */

class nspi_iQuadParticle_Class : public RefObjectClassImpl
{
public:
    nspi_iQuadParticle_Class(const std::string& simpleName, const std::string& fullName, iClassLoader* classLoader):
    RefObjectClassImpl(simpleName, fullName, classLoader)
    {
        {
            SmartPtr<RefObjectProperty<nspi::iQuadParticle, int, int>> p = new RefObjectProperty<nspi::iQuadParticle, int, int>("FrameNum", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iQuadParticle::GetFrameNum);
            p->SetSetter(&nspi::iQuadParticle::SetFrameNum);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iQuadParticle, float, float>> p = new RefObjectProperty<nspi::iQuadParticle, float, float>("Duration", []() {return PrimitiveClass::F32();});
            p->SetGetter(&nspi::iQuadParticle::GetDuration);
            p->SetSetter(&nspi::iQuadParticle::SetDuration);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

    }

    virtual ~nspi_iQuadParticle_Class()
    {
    }

    virtual Var CreateInstance()
    {
        return nspi::CreateQuadParticle();
    }
    virtual bool IsArray() const
    {
        return false;
    }
    virtual iClass* GetElementClass() const
    {
        return nullptr;
    }
    virtual iClass* GetParent() const
    {
        return nspi::iParticle::StaticClass();
    }
};

static SmartPtr<iClass> g_nspi_iQuadParticle_Class;

iClass* iQuadParticle::StaticClass()
{
    return g_nspi_iQuadParticle_Class;
}

iClass* iQuadParticle::GetClass() const
{
    return g_nspi_iQuadParticle_Class;
}

std::string iQuadParticle::ToString() const
{
    return piFormat("%s(%p)", GetClass()->GetFullName().c_str(), this);
}

static struct nspi_iQuadParticle_Class_Reg
{
    nspi_iQuadParticle_Class_Reg()
    {
        g_nspi_iQuadParticle_Class = new nspi_iQuadParticle_Class("iQuadParticle", "nspi::iQuadParticle", piGetRootClassLoader());
        g_nspi_iQuadParticle_Class->SetReadonly();
        piGetRootClassLoader()->RegisterClass("nspi::iQuadParticle", g_nspi_iQuadParticle_Class);
        piGetRootClassLoader()->RegisterClass("QuadParticle", g_nspi_iQuadParticle_Class);
    }
} g_nspi_iQuadParticle_Class_RegInstance;



/**
 * ==========================================================================
 * eParticleType
 */

SmartPtr<iEnum> g_nspi_eParticleType_Enum;

static struct nspi_eParticleType_Enum
{
    nspi_eParticleType_Enum()
    {
        g_nspi_eParticleType_Enum = CreateEnum();
        g_nspi_eParticleType_Enum->SetName("eParticleType");
        g_nspi_eParticleType_Enum->SetFullName("nspi::eParticleType");
        g_nspi_eParticleType_Enum->RegisterValue("Count", 2);
        g_nspi_eParticleType_Enum->RegisterValue("Mesh", 1);
        g_nspi_eParticleType_Enum->RegisterValue("Quad", 0);
        piGetRootClassLoader()->RegisterEnum("nspi::eParticleType", g_nspi_eParticleType_Enum);
        piGetRootClassLoader()->RegisterEnum("eParticleType", g_nspi_eParticleType_Enum);
    }
} g_nspi_eParticleType_Enum_Instance;
/**
 * ==========================================================================
 * eParticlePlayType
 */

SmartPtr<iEnum> g_nspi_eParticlePlayType_Enum;

static struct nspi_eParticlePlayType_Enum
{
    nspi_eParticlePlayType_Enum()
    {
        g_nspi_eParticlePlayType_Enum = CreateEnum();
        g_nspi_eParticlePlayType_Enum->SetName("eParticlePlayType");
        g_nspi_eParticlePlayType_Enum->SetFullName("nspi::eParticlePlayType");
        g_nspi_eParticlePlayType_Enum->RegisterValue("ByFrame", 0);
        g_nspi_eParticlePlayType_Enum->RegisterValue("ByTime", 1);
        g_nspi_eParticlePlayType_Enum->RegisterValue("Count", 2);
        piGetRootClassLoader()->RegisterEnum("nspi::eParticlePlayType", g_nspi_eParticlePlayType_Enum);
        piGetRootClassLoader()->RegisterEnum("eParticlePlayType", g_nspi_eParticlePlayType_Enum);
    }
} g_nspi_eParticlePlayType_Enum_Instance;
/**
 * ==========================================================================
 * eParticleBlend
 */

SmartPtr<iEnum> g_nspi_eParticleBlend_Enum;

static struct nspi_eParticleBlend_Enum
{
    nspi_eParticleBlend_Enum()
    {
        g_nspi_eParticleBlend_Enum = CreateEnum();
        g_nspi_eParticleBlend_Enum->SetName("eParticleBlend");
        g_nspi_eParticleBlend_Enum->SetFullName("nspi::eParticleBlend");
        g_nspi_eParticleBlend_Enum->RegisterValue("Add", 0);
        g_nspi_eParticleBlend_Enum->RegisterValue("Blend", 1);
        g_nspi_eParticleBlend_Enum->RegisterValue("Count", 2);
        piGetRootClassLoader()->RegisterEnum("nspi::eParticleBlend", g_nspi_eParticleBlend_Enum);
        piGetRootClassLoader()->RegisterEnum("eParticleBlend", g_nspi_eParticleBlend_Enum);
    }
} g_nspi_eParticleBlend_Enum_Instance;
/**
 * ==========================================================================
 * eBillboardType
 */

SmartPtr<iEnum> g_nspi_eBillboardType_Enum;

static struct nspi_eBillboardType_Enum
{
    nspi_eBillboardType_Enum()
    {
        g_nspi_eBillboardType_Enum = CreateEnum();
        g_nspi_eBillboardType_Enum->SetName("eBillboardType");
        g_nspi_eBillboardType_Enum->SetFullName("nspi::eBillboardType");
        g_nspi_eBillboardType_Enum->RegisterValue("All", 1);
        g_nspi_eBillboardType_Enum->RegisterValue("Count", 3);
        g_nspi_eBillboardType_Enum->RegisterValue("None", 2);
        g_nspi_eBillboardType_Enum->RegisterValue("Y", 0);
        piGetRootClassLoader()->RegisterEnum("nspi::eBillboardType", g_nspi_eBillboardType_Enum);
        piGetRootClassLoader()->RegisterEnum("eBillboardType", g_nspi_eBillboardType_Enum);
    }
} g_nspi_eBillboardType_Enum_Instance;
