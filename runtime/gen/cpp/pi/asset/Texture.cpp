/*******************************************************************************
 See copyright notice in LICENSE.

 **DO NOT** edit manually.

 This is generated by pic, which is a c++ code generator for reflection support.
 ******************************************************************************/
#include <pi/core/impl/ReflectionImpl.h>
#include <functional>

using namespace nspi;
using namespace std;

/**
 * ==========================================================================
 * iTexture
 */

class nspi_iTexture_Class : public RefObjectClassImpl
{
public:
    nspi_iTexture_Class(const std::string& simpleName, const std::string& fullName, iClassLoader* classLoader):
    RefObjectClassImpl(simpleName, fullName, classLoader)
    {
        {
            SmartPtr<RefObjectProperty<nspi::iTexture, int, int>> p = new RefObjectProperty<nspi::iTexture, int, int>("WrapS", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iTexture::GetWrapS);
            p->SetSetter(&nspi::iTexture::SetWrapS);
            p->SetConfig("Class", "eTexValue");
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iTexture, int, int>> p = new RefObjectProperty<nspi::iTexture, int, int>("WrapT", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iTexture::GetWrapT);
            p->SetSetter(&nspi::iTexture::SetWrapT);
            p->SetConfig("Class", "eTexValue");
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iTexture, int, int>> p = new RefObjectProperty<nspi::iTexture, int, int>("WrapR", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iTexture::GetWrapR);
            p->SetSetter(&nspi::iTexture::SetWrapR);
            p->SetConfig("Class", "eTexValue");
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iTexture, int, int>> p = new RefObjectProperty<nspi::iTexture, int, int>("MinFilter", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iTexture::GetMinFilter);
            p->SetSetter(&nspi::iTexture::SetMinFilter);
            p->SetConfig("Class", "eTexValue");
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iTexture, int, int>> p = new RefObjectProperty<nspi::iTexture, int, int>("MagFilter", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iTexture::GetMagFilter);
            p->SetSetter(&nspi::iTexture::SetMagFilter);
            p->SetConfig("Class", "eTexValue");
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iTexture, int, int>> p = new RefObjectProperty<nspi::iTexture, int, int>("Format", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iTexture::GetFormat);
            p->SetSetter(&nspi::iTexture::SetFormat);
            p->SetConfig("Class", "ePixelFormat");
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

    }

    virtual ~nspi_iTexture_Class()
    {
    }

    virtual Var CreateInstance()
    {
        return Var();
    }
    virtual bool IsArray() const
    {
        return false;
    }
    virtual iClass* GetElementClass() const
    {
        return nullptr;
    }
    virtual iClass* GetParent() const
    {
        return nspi::iGraphicsAsset::StaticClass();
    }
};

static SmartPtr<iClass> g_nspi_iTexture_Class;

iClass* iTexture::StaticClass()
{
    return g_nspi_iTexture_Class;
}

iClass* iTexture::GetClass() const
{
    return g_nspi_iTexture_Class;
}

std::string iTexture::ToString() const
{
    return piFormat("%s(%p)", GetClass()->GetFullName().c_str(), this);
}

static struct nspi_iTexture_Class_Reg
{
    nspi_iTexture_Class_Reg()
    {
        g_nspi_iTexture_Class = new nspi_iTexture_Class("iTexture", "nspi::iTexture", piGetRootClassLoader());
        g_nspi_iTexture_Class->SetReadonly();
        piGetRootClassLoader()->RegisterClass("nspi::iTexture", g_nspi_iTexture_Class);
        piGetRootClassLoader()->RegisterClass("Texture", g_nspi_iTexture_Class);
        piGetRootClassLoader()->RegisterClass("Texture", g_nspi_iTexture_Class);
    }
} g_nspi_iTexture_Class_RegInstance;



/**
 * ==========================================================================
 * iTexture2D
 */

class nspi_iTexture2D_Class : public RefObjectClassImpl
{
public:
    nspi_iTexture2D_Class(const std::string& simpleName, const std::string& fullName, iClassLoader* classLoader):
    RefObjectClassImpl(simpleName, fullName, classLoader)
    {
        {
            SmartPtr<RefObjectProperty<nspi::iTexture2D, const std::string&, std::string>> p = new RefObjectProperty<nspi::iTexture2D, const std::string&, std::string>("Source", []() {return PrimitiveClass::String();});
            p->SetGetter(&nspi::iTexture2D::GetSource);
            p->SetSetter(&nspi::iTexture2D::SetSource);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iTexture2D, int32_t, int32_t>> p = new RefObjectProperty<nspi::iTexture2D, int32_t, int32_t>("Width", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iTexture2D::GetWidth);
            p->SetSetter(&nspi::iTexture2D::SetWidth);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iTexture2D, int32_t, int32_t>> p = new RefObjectProperty<nspi::iTexture2D, int32_t, int32_t>("Height", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iTexture2D::GetHeight);
            p->SetSetter(&nspi::iTexture2D::SetHeight);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

    }

    virtual ~nspi_iTexture2D_Class()
    {
    }

    virtual Var CreateInstance()
    {
        return nspi::CreateTexture2D();
    }
    virtual bool IsArray() const
    {
        return false;
    }
    virtual iClass* GetElementClass() const
    {
        return nullptr;
    }
    virtual iClass* GetParent() const
    {
        return nspi::iTexture::StaticClass();
    }
};

static SmartPtr<iClass> g_nspi_iTexture2D_Class;

iClass* iTexture2D::StaticClass()
{
    return g_nspi_iTexture2D_Class;
}

iClass* iTexture2D::GetClass() const
{
    return g_nspi_iTexture2D_Class;
}

std::string iTexture2D::ToString() const
{
    return piFormat("%s(%p)", GetClass()->GetFullName().c_str(), this);
}

static struct nspi_iTexture2D_Class_Reg
{
    nspi_iTexture2D_Class_Reg()
    {
        g_nspi_iTexture2D_Class = new nspi_iTexture2D_Class("iTexture2D", "nspi::iTexture2D", piGetRootClassLoader());
        g_nspi_iTexture2D_Class->SetReadonly();
        piGetRootClassLoader()->RegisterClass("nspi::iTexture2D", g_nspi_iTexture2D_Class);
        piGetRootClassLoader()->RegisterClass("Texture2D", g_nspi_iTexture2D_Class);
        piGetRootClassLoader()->RegisterClass("Texture2D", g_nspi_iTexture2D_Class);
    }
} g_nspi_iTexture2D_Class_RegInstance;



/**
 * ==========================================================================
 * iTexture2DArray
 */

class nspi_iTexture2DArray_Class : public RefObjectClassImpl
{
public:
    nspi_iTexture2DArray_Class(const std::string& simpleName, const std::string& fullName, iClassLoader* classLoader):
    RefObjectClassImpl(simpleName, fullName, classLoader)
    {
        {
            typedef bool(nspi::iTexture2DArray::*nspi_iTexture2DArray_IsEmpty_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iTexture2DArray_IsEmpty_FuncPtr, nspi::iTexture2DArray> nspi_iTexture2DArray_IsEmpty_MethodClass;
            SmartPtr<nspi_iTexture2DArray_IsEmpty_MethodClass> m = new nspi_iTexture2DArray_IsEmpty_MethodClass(&nspi::iTexture2DArray::IsEmpty, "IsEmpty");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef iTexture2D*(nspi::iTexture2DArray::*nspi_iTexture2DArray_GetBack_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iTexture2DArray_GetBack_FuncPtr, nspi::iTexture2DArray> nspi_iTexture2DArray_GetBack_MethodClass;
            SmartPtr<nspi_iTexture2DArray_GetBack_MethodClass> m = new nspi_iTexture2DArray_GetBack_MethodClass(&nspi::iTexture2DArray::GetBack, "GetBack");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef iTexture2D*(nspi::iTexture2DArray::*nspi_iTexture2DArray_GetFront_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iTexture2DArray_GetFront_FuncPtr, nspi::iTexture2DArray> nspi_iTexture2DArray_GetFront_MethodClass;
            SmartPtr<nspi_iTexture2DArray_GetFront_MethodClass> m = new nspi_iTexture2DArray_GetFront_MethodClass(&nspi::iTexture2DArray::GetFront, "GetFront");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef void(nspi::iTexture2DArray::*nspi_iTexture2DArray_PushBack_FuncPtr)(iTexture2D*);
            typedef RefObjectMethod1Void<nspi_iTexture2DArray_PushBack_FuncPtr, nspi::iTexture2DArray, iTexture2D*> nspi_iTexture2DArray_PushBack_MethodClass;
            SmartPtr<nspi_iTexture2DArray_PushBack_MethodClass> m = new nspi_iTexture2DArray_PushBack_MethodClass(&nspi::iTexture2DArray::PushBack, "PushBack");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef void(nspi::iTexture2DArray::*nspi_iTexture2DArray_PopBack_FuncPtr)();
            typedef RefObjectMethodVoidVoid<nspi_iTexture2DArray_PopBack_FuncPtr, nspi::iTexture2DArray> nspi_iTexture2DArray_PopBack_MethodClass;
            SmartPtr<nspi_iTexture2DArray_PopBack_MethodClass> m = new nspi_iTexture2DArray_PopBack_MethodClass(&nspi::iTexture2DArray::PopBack, "PopBack");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef int32_t(nspi::iTexture2DArray::*nspi_iTexture2DArray_GetCount_FuncPtr)() const;
            typedef RefObjectMethodVoid<nspi_iTexture2DArray_GetCount_FuncPtr, nspi::iTexture2DArray> nspi_iTexture2DArray_GetCount_MethodClass;
            SmartPtr<nspi_iTexture2DArray_GetCount_MethodClass> m = new nspi_iTexture2DArray_GetCount_MethodClass(&nspi::iTexture2DArray::GetCount, "GetCount");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef iTexture2D*(nspi::iTexture2DArray::*nspi_iTexture2DArray_GetItem_FuncPtr)(int32_t) const;
            typedef RefObjectMethod1<nspi_iTexture2DArray_GetItem_FuncPtr, nspi::iTexture2DArray, int32_t> nspi_iTexture2DArray_GetItem_MethodClass;
            SmartPtr<nspi_iTexture2DArray_GetItem_MethodClass> m = new nspi_iTexture2DArray_GetItem_MethodClass(&nspi::iTexture2DArray::GetItem, "GetItem");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef void(nspi::iTexture2DArray::*nspi_iTexture2DArray_SetItem_FuncPtr)(int32_t,iTexture2D*);
            typedef RefObjectMethod2Void<nspi_iTexture2DArray_SetItem_FuncPtr, nspi::iTexture2DArray, int32_t, iTexture2D*> nspi_iTexture2DArray_SetItem_MethodClass;
            SmartPtr<nspi_iTexture2DArray_SetItem_MethodClass> m = new nspi_iTexture2DArray_SetItem_MethodClass(&nspi::iTexture2DArray::SetItem, "SetItem");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef void(nspi::iTexture2DArray::*nspi_iTexture2DArray_Resize_FuncPtr)(int32_t);
            typedef RefObjectMethod1Void<nspi_iTexture2DArray_Resize_FuncPtr, nspi::iTexture2DArray, int32_t> nspi_iTexture2DArray_Resize_MethodClass;
            SmartPtr<nspi_iTexture2DArray_Resize_MethodClass> m = new nspi_iTexture2DArray_Resize_MethodClass(&nspi::iTexture2DArray::Resize, "Resize");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef void(nspi::iTexture2DArray::*nspi_iTexture2DArray_Clear_FuncPtr)();
            typedef RefObjectMethodVoidVoid<nspi_iTexture2DArray_Clear_FuncPtr, nspi::iTexture2DArray> nspi_iTexture2DArray_Clear_MethodClass;
            SmartPtr<nspi_iTexture2DArray_Clear_MethodClass> m = new nspi_iTexture2DArray_Clear_MethodClass(&nspi::iTexture2DArray::Clear, "Clear");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef void(nspi::iTexture2DArray::*nspi_iTexture2DArray_Remove_FuncPtr)(int32_t);
            typedef RefObjectMethod1Void<nspi_iTexture2DArray_Remove_FuncPtr, nspi::iTexture2DArray, int32_t> nspi_iTexture2DArray_Remove_MethodClass;
            SmartPtr<nspi_iTexture2DArray_Remove_MethodClass> m = new nspi_iTexture2DArray_Remove_MethodClass(&nspi::iTexture2DArray::Remove, "Remove");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

    }

    virtual ~nspi_iTexture2DArray_Class()
    {
    }

    virtual Var CreateInstance()
    {
        return nspi::CreateTexture2DArray();
    }
    virtual bool IsArray() const
    {
        return true;
    }
    virtual iClass* GetElementClass() const
    {
        return nspi::iTexture2D::StaticClass();
    }
    virtual iClass* GetParent() const
    {
        return nullptr;
    }
};

static SmartPtr<iClass> g_nspi_iTexture2DArray_Class;

iClass* iTexture2DArray::StaticClass()
{
    return g_nspi_iTexture2DArray_Class;
}

iClass* iTexture2DArray::GetClass() const
{
    return g_nspi_iTexture2DArray_Class;
}

std::string iTexture2DArray::ToString() const
{
    return piFormat("%s(%p)", GetClass()->GetFullName().c_str(), this);
}

static struct nspi_iTexture2DArray_Class_Reg
{
    nspi_iTexture2DArray_Class_Reg()
    {
        g_nspi_iTexture2DArray_Class = new nspi_iTexture2DArray_Class("iTexture2DArray", "nspi::iTexture2DArray", piGetRootClassLoader());
        g_nspi_iTexture2DArray_Class->SetReadonly();
        piGetRootClassLoader()->RegisterClass("nspi::iTexture2DArray", g_nspi_iTexture2DArray_Class);
        piGetRootClassLoader()->RegisterClass("Texture2DArray", g_nspi_iTexture2DArray_Class);
    }
} g_nspi_iTexture2DArray_Class_RegInstance;

class nspi_iTexture2DArray_Impl : public ObjectArrayImpl<nspi::iTexture2D, nspi::iTexture2DArray>
{
public:
    nspi_iTexture2DArray_Impl()
    {
    }

    virtual ~nspi_iTexture2DArray_Impl()
    {
    }

    virtual string ToString()
    {
        return piFormat("nspi::iTexture2DArray[%d]", GetCount());
    }
};

iTexture2DArray* nspi::CreateTexture2DArray()
{
    return new nspi_iTexture2DArray_Impl();
}


/**
 * ==========================================================================
 * iCubeMap
 */

class nspi_iCubeMap_Class : public RefObjectClassImpl
{
public:
    nspi_iCubeMap_Class(const std::string& simpleName, const std::string& fullName, iClassLoader* classLoader):
    RefObjectClassImpl(simpleName, fullName, classLoader)
    {
        {
            SmartPtr<RefObjectProperty<nspi::iCubeMap, const std::string&, std::string>> p = new RefObjectProperty<nspi::iCubeMap, const std::string&, std::string>("PositiveX", []() {return PrimitiveClass::String();});
            p->SetGetter(&nspi::iCubeMap::GetPositiveX);
            p->SetSetter(&nspi::iCubeMap::SetPositiveX);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iCubeMap, const std::string&, std::string>> p = new RefObjectProperty<nspi::iCubeMap, const std::string&, std::string>("NegativeX", []() {return PrimitiveClass::String();});
            p->SetGetter(&nspi::iCubeMap::GetNegativeX);
            p->SetSetter(&nspi::iCubeMap::SetNegativeX);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iCubeMap, const std::string&, std::string>> p = new RefObjectProperty<nspi::iCubeMap, const std::string&, std::string>("PositiveY", []() {return PrimitiveClass::String();});
            p->SetGetter(&nspi::iCubeMap::GetPositiveY);
            p->SetSetter(&nspi::iCubeMap::SetPositiveY);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iCubeMap, const std::string&, std::string>> p = new RefObjectProperty<nspi::iCubeMap, const std::string&, std::string>("NegativeY", []() {return PrimitiveClass::String();});
            p->SetGetter(&nspi::iCubeMap::GetNegativeY);
            p->SetSetter(&nspi::iCubeMap::SetNegativeY);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iCubeMap, const std::string&, std::string>> p = new RefObjectProperty<nspi::iCubeMap, const std::string&, std::string>("PositiveZ", []() {return PrimitiveClass::String();});
            p->SetGetter(&nspi::iCubeMap::GetPositiveZ);
            p->SetSetter(&nspi::iCubeMap::SetPositiveZ);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iCubeMap, const std::string&, std::string>> p = new RefObjectProperty<nspi::iCubeMap, const std::string&, std::string>("NegativeZ", []() {return PrimitiveClass::String();});
            p->SetGetter(&nspi::iCubeMap::GetNegativeZ);
            p->SetSetter(&nspi::iCubeMap::SetNegativeZ);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

    }

    virtual ~nspi_iCubeMap_Class()
    {
    }

    virtual Var CreateInstance()
    {
        return nspi::CreateCubeMap();
    }
    virtual bool IsArray() const
    {
        return false;
    }
    virtual iClass* GetElementClass() const
    {
        return nullptr;
    }
    virtual iClass* GetParent() const
    {
        return nspi::iTexture::StaticClass();
    }
};

static SmartPtr<iClass> g_nspi_iCubeMap_Class;

iClass* iCubeMap::StaticClass()
{
    return g_nspi_iCubeMap_Class;
}

iClass* iCubeMap::GetClass() const
{
    return g_nspi_iCubeMap_Class;
}

std::string iCubeMap::ToString() const
{
    return piFormat("%s(%p)", GetClass()->GetFullName().c_str(), this);
}

static struct nspi_iCubeMap_Class_Reg
{
    nspi_iCubeMap_Class_Reg()
    {
        g_nspi_iCubeMap_Class = new nspi_iCubeMap_Class("iCubeMap", "nspi::iCubeMap", piGetRootClassLoader());
        g_nspi_iCubeMap_Class->SetReadonly();
        piGetRootClassLoader()->RegisterClass("nspi::iCubeMap", g_nspi_iCubeMap_Class);
        piGetRootClassLoader()->RegisterClass("CubeMap", g_nspi_iCubeMap_Class);
        piGetRootClassLoader()->RegisterClass("CubeMap", g_nspi_iCubeMap_Class);
    }
} g_nspi_iCubeMap_Class_RegInstance;



/**
 * ==========================================================================
 * iRenderTexture
 */

class nspi_iRenderTexture_Class : public RefObjectClassImpl
{
public:
    nspi_iRenderTexture_Class(const std::string& simpleName, const std::string& fullName, iClassLoader* classLoader):
    RefObjectClassImpl(simpleName, fullName, classLoader)
    {
        {
            SmartPtr<RefObjectProperty<nspi::iRenderTexture, bool, bool>> p = new RefObjectProperty<nspi::iRenderTexture, bool, bool>("FixScreenBounds", []() {return PrimitiveClass::Boolean();});
            p->SetGetter(&nspi::iRenderTexture::IsFixScreenBounds);
            p->SetSetter(&nspi::iRenderTexture::SetFixScreenBounds);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iRenderTexture, int32_t, int32_t>> p = new RefObjectProperty<nspi::iRenderTexture, int32_t, int32_t>("Width", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iRenderTexture::GetWidth);
            p->SetSetter(&nspi::iRenderTexture::SetWidth);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iRenderTexture, int32_t, int32_t>> p = new RefObjectProperty<nspi::iRenderTexture, int32_t, int32_t>("Height", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iRenderTexture::GetHeight);
            p->SetSetter(&nspi::iRenderTexture::SetHeight);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iRenderTexture, int32_t, int32_t>> p = new RefObjectProperty<nspi::iRenderTexture, int32_t, int32_t>("Depth", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iRenderTexture::GetDepth);
            p->SetSetter(&nspi::iRenderTexture::SetDepth);
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iRenderTexture, int32_t, int32_t>> p = new RefObjectProperty<nspi::iRenderTexture, int32_t, int32_t>("DepthFormat", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iRenderTexture::GetDepthFormat);
            p->SetSetter(&nspi::iRenderTexture::SetDepthFormat);
            p->SetConfig("Class", "eRenderTextureDepthFormat");
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            SmartPtr<RefObjectProperty<nspi::iRenderTexture, int32_t, int32_t>> p = new RefObjectProperty<nspi::iRenderTexture, int32_t, int32_t>("Dimession", []() {return PrimitiveClass::I32();});
            p->SetGetter(&nspi::iRenderTexture::GetDimession);
            p->SetSetter(&nspi::iRenderTexture::SetDimession);
            p->SetConfig("Class", "eRenderTextureDimession");
            p->SetReadonly();
            mProps->PushBack(p);
            mPropMap[p->GetName()] = p;
        }

        {
            typedef bool(nspi::iRenderTexture::*nspi_iRenderTexture_DoCreate_FuncPtr)();
            typedef RefObjectMethodVoid<nspi_iRenderTexture_DoCreate_FuncPtr, nspi::iRenderTexture> nspi_iRenderTexture_DoCreate_MethodClass;
            SmartPtr<nspi_iRenderTexture_DoCreate_MethodClass> m = new nspi_iRenderTexture_DoCreate_MethodClass(&nspi::iRenderTexture::DoCreate, "DoCreate");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef bool(nspi::iRenderTexture::*nspi_iRenderTexture_Resize_FuncPtr)(int32_t,int32_t);
            typedef RefObjectMethod2<nspi_iRenderTexture_Resize_FuncPtr, nspi::iRenderTexture, int32_t, int32_t> nspi_iRenderTexture_Resize_MethodClass;
            SmartPtr<nspi_iRenderTexture_Resize_MethodClass> m = new nspi_iRenderTexture_Resize_MethodClass(&nspi::iRenderTexture::Resize, "Resize");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

        {
            typedef int32_t(nspi::iRenderTexture::*nspi_iRenderTexture_GetFBO_FuncPtr)();
            typedef RefObjectMethodVoid<nspi_iRenderTexture_GetFBO_FuncPtr, nspi::iRenderTexture> nspi_iRenderTexture_GetFBO_MethodClass;
            SmartPtr<nspi_iRenderTexture_GetFBO_MethodClass> m = new nspi_iRenderTexture_GetFBO_MethodClass(&nspi::iRenderTexture::GetFBO, "GetFBO");
            m->SetReadonly();
            mMethods->PushBack(m);
            mMethodMap[m->GetName()] = m;
        }

    }

    virtual ~nspi_iRenderTexture_Class()
    {
    }

    virtual Var CreateInstance()
    {
        return nspi::CreateRenderTexture();
    }
    virtual bool IsArray() const
    {
        return false;
    }
    virtual iClass* GetElementClass() const
    {
        return nullptr;
    }
    virtual iClass* GetParent() const
    {
        return nspi::iTexture::StaticClass();
    }
};

static SmartPtr<iClass> g_nspi_iRenderTexture_Class;

iClass* iRenderTexture::StaticClass()
{
    return g_nspi_iRenderTexture_Class;
}

iClass* iRenderTexture::GetClass() const
{
    return g_nspi_iRenderTexture_Class;
}

std::string iRenderTexture::ToString() const
{
    return piFormat("%s(%p)", GetClass()->GetFullName().c_str(), this);
}

static struct nspi_iRenderTexture_Class_Reg
{
    nspi_iRenderTexture_Class_Reg()
    {
        g_nspi_iRenderTexture_Class = new nspi_iRenderTexture_Class("iRenderTexture", "nspi::iRenderTexture", piGetRootClassLoader());
        g_nspi_iRenderTexture_Class->SetReadonly();
        piGetRootClassLoader()->RegisterClass("nspi::iRenderTexture", g_nspi_iRenderTexture_Class);
        piGetRootClassLoader()->RegisterClass("RenderTexture", g_nspi_iRenderTexture_Class);
        piGetRootClassLoader()->RegisterClass("RenderTexture", g_nspi_iRenderTexture_Class);
    }
} g_nspi_iRenderTexture_Class_RegInstance;



/**
 * ==========================================================================
 * eRenderTextureDimession
 */

SmartPtr<iEnum> g_nspi_eRenderTextureDimession_Enum;

static struct nspi_eRenderTextureDimession_Enum
{
    nspi_eRenderTextureDimession_Enum()
    {
        g_nspi_eRenderTextureDimession_Enum = CreateEnum();
        g_nspi_eRenderTextureDimession_Enum->SetName("eRenderTextureDimession");
        g_nspi_eRenderTextureDimession_Enum->SetFullName("nspi::eRenderTextureDimession");
        g_nspi_eRenderTextureDimession_Enum->RegisterValue("2D", 0);
        g_nspi_eRenderTextureDimession_Enum->RegisterValue("3D", 2);
        g_nspi_eRenderTextureDimession_Enum->RegisterValue("Cube", 1);
        piGetRootClassLoader()->RegisterEnum("nspi::eRenderTextureDimession", g_nspi_eRenderTextureDimession_Enum);
        piGetRootClassLoader()->RegisterEnum("eRenderTextureDimession", g_nspi_eRenderTextureDimession_Enum);
    }
} g_nspi_eRenderTextureDimession_Enum_Instance;
/**
 * ==========================================================================
 * eRenderTextureDepthFormat
 */

SmartPtr<iEnum> g_nspi_eRenderTextureDepthFormat_Enum;

static struct nspi_eRenderTextureDepthFormat_Enum
{
    nspi_eRenderTextureDepthFormat_Enum()
    {
        g_nspi_eRenderTextureDepthFormat_Enum = CreateEnum();
        g_nspi_eRenderTextureDepthFormat_Enum->SetName("eRenderTextureDepthFormat");
        g_nspi_eRenderTextureDepthFormat_Enum->SetFullName("nspi::eRenderTextureDepthFormat");
        g_nspi_eRenderTextureDepthFormat_Enum->RegisterValue("D16", 1);
        g_nspi_eRenderTextureDepthFormat_Enum->RegisterValue("D24S8", 2);
        g_nspi_eRenderTextureDepthFormat_Enum->RegisterValue("None", 0);
        piGetRootClassLoader()->RegisterEnum("nspi::eRenderTextureDepthFormat", g_nspi_eRenderTextureDepthFormat_Enum);
        piGetRootClassLoader()->RegisterEnum("eRenderTextureDepthFormat", g_nspi_eRenderTextureDepthFormat_Enum);
    }
} g_nspi_eRenderTextureDepthFormat_Enum_Instance;
