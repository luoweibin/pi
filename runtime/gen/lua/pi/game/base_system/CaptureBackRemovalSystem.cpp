/*******************************************************************************
 See copyright notice in LICENSE.

 **DO NOT** edit manually.

 This is generated by pilua, which is a lua binding code generator.
 ******************************************************************************/

using namespace nspi;
using namespace std;

/**
 * ==========================================================================
 * nspi_iCaptureBackRemovalSystem
 */

static int nspi_iCaptureBackRemovalSystem_GC(lua_State* L)
{
    auto refObject = LuaArg<nspi::iCaptureBackRemovalSystem*>::Value(L, 1);
    refObject->Release();
    return 0;
}

static int nspi_iCaptureBackRemovalSystem_ToString(lua_State* L)
{
    auto refObject = LuaArg<nspi::iCaptureBackRemovalSystem*>::Value(L, 1);
    auto str = refObject->ToString();
    LuaValue<std::string>::Push(L, str);
    return 1;
}

static int nspi_iCaptureBackRemovalSystem_GetClass(lua_State* L)
{
    auto refObject = LuaArg<nspi::iCaptureBackRemovalSystem*>::Value(L, 1);
    auto klass = refObject->GetClass();
    LuaValue<iClass*>::Push(L, klass);
    return 1;
}

void nspi_iSystem_Init(lua_State* L, bool derived);
void nspi_iCaptureBackRemovalSystem_Init(lua_State* L, bool derived)
{
    if (!derived)
    {
        luaL_newmetatable(L, "nspi::iCaptureBackRemovalSystem");
        lua_pushlightuserdata(L, nspi::iCaptureBackRemovalSystem::StaticClass());
        lua_setfield(L, -2, "_class");
        lua_pushcfunction(L, nspi_iCaptureBackRemovalSystem_GC);
        lua_setfield(L, -2, "__gc");
        lua_pushcfunction(L, nspi_iCaptureBackRemovalSystem_ToString);
        lua_setfield(L, -2, "__tostring");
        lua_newtable(L);
    }

    lua_pushcfunction(L, nspi_iCaptureBackRemovalSystem_GetClass);
    lua_setfield(L, -2, "GetClass");

    nspi_iSystem_Init(L, true);
    if (!derived)
    {
        lua_setfield(L, -2, "__index");
        lua_pop(L, 1);
    }

}

void lua_pi_game_base_system_CaptureBackRemovalSystem_Init(lua_State* L)
{
    // Register interfaces
    nspi_iCaptureBackRemovalSystem_Init(L, false);

    // Register enums

    // Register global functions
}

