/*******************************************************************************
 See copyright notice in LICENSE.

 **DO NOT** edit manually.

 This is generated by pilua, which is a lua binding code generator.
 ******************************************************************************/

using namespace nspi;
using namespace std;

/**
 * ==========================================================================
 * nspi_iMediaFrame
 */

static int nspi_iMediaFrame_GC(lua_State* L)
{
    auto refObject = LuaArg<nspi::iMediaFrame*>::Value(L, 1);
    refObject->Release();
    return 0;
}

static int nspi_iMediaFrame_ToString(lua_State* L)
{
    auto refObject = LuaArg<nspi::iMediaFrame*>::Value(L, 1);
    auto str = refObject->ToString();
    LuaValue<std::string>::Push(L, str);
    return 1;
}

static int nspi_iMediaFrame_GetClass(lua_State* L)
{
    auto refObject = LuaArg<nspi::iMediaFrame*>::Value(L, 1);
    auto klass = refObject->GetClass();
    LuaValue<iClass*>::Push(L, klass);
    return 1;
}

static int nspi_iMediaFrame_GetPTS(lua_State* L)
{
    auto refObject = LuaArg<nspi::iMediaFrame*>::Value(L, 1);
    auto ret = refObject->GetPTS();
    LuaValue<iTime*>::Push(L, ret);
    return 1;
}

void nspi_iRefObject_Init(lua_State* L, bool derived);
void nspi_iMediaFrame_Init(lua_State* L, bool derived)
{
    if (!derived)
    {
        luaL_newmetatable(L, "nspi::iMediaFrame");
        lua_pushlightuserdata(L, nspi::iMediaFrame::StaticClass());
        lua_setfield(L, -2, "_class");
        lua_pushcfunction(L, nspi_iMediaFrame_GC);
        lua_setfield(L, -2, "__gc");
        lua_pushcfunction(L, nspi_iMediaFrame_ToString);
        lua_setfield(L, -2, "__tostring");
        lua_newtable(L);
    }

    lua_pushcfunction(L, nspi_iMediaFrame_GetPTS);
    lua_setfield(L, -2, "GetPTS");
    lua_pushcfunction(L, nspi_iMediaFrame_GetClass);
    lua_setfield(L, -2, "GetClass");

    nspi_iRefObject_Init(L, true);
    if (!derived)
    {
        lua_setfield(L, -2, "__index");
        lua_pop(L, 1);
    }

}

/**
 * ==========================================================================
 * nspi_iVideoFrame
 */

static int nspi_iVideoFrame_GC(lua_State* L)
{
    auto refObject = LuaArg<nspi::iVideoFrame*>::Value(L, 1);
    refObject->Release();
    return 0;
}

static int nspi_iVideoFrame_ToString(lua_State* L)
{
    auto refObject = LuaArg<nspi::iVideoFrame*>::Value(L, 1);
    auto str = refObject->ToString();
    LuaValue<std::string>::Push(L, str);
    return 1;
}

static int nspi_iVideoFrame_GetClass(lua_State* L)
{
    auto refObject = LuaArg<nspi::iVideoFrame*>::Value(L, 1);
    auto klass = refObject->GetClass();
    LuaValue<iClass*>::Push(L, klass);
    return 1;
}

static int nspi_iVideoFrame_GetBitmap(lua_State* L)
{
    auto refObject = LuaArg<nspi::iVideoFrame*>::Value(L, 1);
    auto ret = refObject->GetBitmap();
    LuaValue<iBitmap*>::Push(L, ret);
    return 1;
}

void nspi_iMediaFrame_Init(lua_State* L, bool derived);
void nspi_iVideoFrame_Init(lua_State* L, bool derived)
{
    if (!derived)
    {
        luaL_newmetatable(L, "nspi::iVideoFrame");
        lua_pushlightuserdata(L, nspi::iVideoFrame::StaticClass());
        lua_setfield(L, -2, "_class");
        lua_pushcfunction(L, nspi_iVideoFrame_GC);
        lua_setfield(L, -2, "__gc");
        lua_pushcfunction(L, nspi_iVideoFrame_ToString);
        lua_setfield(L, -2, "__tostring");
        lua_newtable(L);
    }

    lua_pushcfunction(L, nspi_iVideoFrame_GetBitmap);
    lua_setfield(L, -2, "GetBitmap");
    lua_pushcfunction(L, nspi_iVideoFrame_GetClass);
    lua_setfield(L, -2, "GetClass");

    nspi_iMediaFrame_Init(L, true);
    if (!derived)
    {
        lua_setfield(L, -2, "__index");
        lua_pop(L, 1);
    }

}

/**
 * ==========================================================================
 * nspi_iAudioFrame
 */

static int nspi_iAudioFrame_GC(lua_State* L)
{
    auto refObject = LuaArg<nspi::iAudioFrame*>::Value(L, 1);
    refObject->Release();
    return 0;
}

static int nspi_iAudioFrame_ToString(lua_State* L)
{
    auto refObject = LuaArg<nspi::iAudioFrame*>::Value(L, 1);
    auto str = refObject->ToString();
    LuaValue<std::string>::Push(L, str);
    return 1;
}

static int nspi_iAudioFrame_GetClass(lua_State* L)
{
    auto refObject = LuaArg<nspi::iAudioFrame*>::Value(L, 1);
    auto klass = refObject->GetClass();
    LuaValue<iClass*>::Push(L, klass);
    return 1;
}

void nspi_iMediaFrame_Init(lua_State* L, bool derived);
void nspi_iAudioFrame_Init(lua_State* L, bool derived)
{
    if (!derived)
    {
        luaL_newmetatable(L, "nspi::iAudioFrame");
        lua_pushlightuserdata(L, nspi::iAudioFrame::StaticClass());
        lua_setfield(L, -2, "_class");
        lua_pushcfunction(L, nspi_iAudioFrame_GC);
        lua_setfield(L, -2, "__gc");
        lua_pushcfunction(L, nspi_iAudioFrame_ToString);
        lua_setfield(L, -2, "__tostring");
        lua_newtable(L);
    }

    lua_pushcfunction(L, nspi_iAudioFrame_GetClass);
    lua_setfield(L, -2, "GetClass");

    nspi_iMediaFrame_Init(L, true);
    if (!derived)
    {
        lua_setfield(L, -2, "__index");
        lua_pop(L, 1);
    }

}

void lua_pi_media_MediaFrame_Init(lua_State* L)
{
    // Register interfaces
    nspi_iMediaFrame_Init(L, false);
    nspi_iVideoFrame_Init(L, false);
    nspi_iAudioFrame_Init(L, false);

    // Register enums

    // Register global functions
}

