/*******************************************************************************
 See copyright notice in LICENSE.

 **DO NOT** edit manually.

 This is generated by pilua, which is a lua binding code generator.
 ******************************************************************************/

using namespace nspi;
using namespace std;

/**
 * ==========================================================================
 * nspi_iGraphicsObject
 */

static int nspi_iGraphicsObject_GC(lua_State* L)
{
    auto refObject = LuaArg<nspi::iGraphicsObject*>::Value(L, 1);
    refObject->Release();
    return 0;
}

static int nspi_iGraphicsObject_ToString(lua_State* L)
{
    auto refObject = LuaArg<nspi::iGraphicsObject*>::Value(L, 1);
    auto str = refObject->ToString();
    LuaValue<std::string>::Push(L, str);
    return 1;
}

static int nspi_iGraphicsObject_GetClass(lua_State* L)
{
    auto refObject = LuaArg<nspi::iGraphicsObject*>::Value(L, 1);
    auto klass = refObject->GetClass();
    LuaValue<iClass*>::Push(L, klass);
    return 1;
}

static int nspi_iGraphicsObject_GetName(lua_State* L)
{
    auto refObject = LuaArg<nspi::iGraphicsObject*>::Value(L, 1);
    auto ret = refObject->GetName();
    LuaValue<int32_t>::Push(L, ret);
    return 1;
}

static int nspi_iGraphicsObject_SetName(lua_State* L)
{
    auto refObject = LuaArg<nspi::iGraphicsObject*>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    refObject->SetName(a1);
    return 0;
}

void nspi_iRefObject_Init(lua_State* L, bool derived);
void nspi_iGraphicsObject_Init(lua_State* L, bool derived)
{
    if (!derived)
    {
        luaL_newmetatable(L, "nspi::iGraphicsObject");
        lua_pushlightuserdata(L, nspi::iGraphicsObject::StaticClass());
        lua_setfield(L, -2, "_class");
        lua_pushcfunction(L, nspi_iGraphicsObject_GC);
        lua_setfield(L, -2, "__gc");
        lua_pushcfunction(L, nspi_iGraphicsObject_ToString);
        lua_setfield(L, -2, "__tostring");
        lua_newtable(L);
    }

    lua_pushcfunction(L, nspi_iGraphicsObject_GetName);
    lua_setfield(L, -2, "GetName");
    lua_pushcfunction(L, nspi_iGraphicsObject_SetName);
    lua_setfield(L, -2, "SetName");
    lua_pushcfunction(L, nspi_iGraphicsObject_GetClass);
    lua_setfield(L, -2, "GetClass");

    nspi_iRefObject_Init(L, true);
    if (!derived)
    {
        lua_setfield(L, -2, "__index");
        lua_pop(L, 1);
    }

}

static int lua_piGetGraphicsType(lua_State* L)
{
    auto ret = piGetGraphicsType();
    LuaValue<int>::Push(L, ret);
    return 1;
}

static int lua_piAddGraphicsAlias(lua_State* L)
{
    auto a0 = LuaArg<std::string>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    auto ret = piAddGraphicsAlias(a0, a1);
    LuaValue<bool>::Push(L, ret);
    return 1;
}

static int lua_piRemoveGraphicsAlias(lua_State* L)
{
    auto a0 = LuaArg<std::string>::Value(L, 1);
    piRemoveGraphicsAlias(a0);
    return 0;
}

static int lua_piGetGraphicsObject(lua_State* L)
{
    auto a0 = LuaArg<std::string>::Value(L, 1);
    auto ret = piGetGraphicsObject(a0);
    LuaValue<int32_t>::Push(L, ret);
    return 1;
}

static int lua_piPushGroupMarker(lua_State* L)
{
    auto a0 = LuaArg<std::string>::Value(L, 1);
    piPushGroupMarker(a0);
    return 0;
}

static int lua_piPopGroupMarker(lua_State* L)
{
    piPopGroupMarker();
    return 0;
}

static int lua_piInsertEventMarker(lua_State* L)
{
    auto a0 = LuaArg<std::string>::Value(L, 1);
    piInsertEventMarker(a0);
    return 0;
}

static int lua_piClear(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    piClear(a0);
    return 0;
}

static int lua_piBlendColor(lua_State* L)
{
    auto a0 = LuaArg<vec4>::Value(L, 1);
    piBlendColor(a0);
    return 0;
}

static int lua_piBlendEquation(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    piBlendEquation(a0);
    return 0;
}

static int lua_piBlendFunc(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int>::Value(L, 2);
    piBlendFunc(a0, a1);
    return 0;
}

static int lua_piBlendFuncSeparate(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<int>::Value(L, 2);
    auto a2 = LuaArg<int>::Value(L, 3);
    piBlendFuncSeparate(a0, a1, a2);
    return 0;
}

static int lua_piCullFace(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    piCullFace(a0);
    return 0;
}

static int lua_piViewport(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    auto a2 = LuaArg<int32_t>::Value(L, 3);
    auto a3 = LuaArg<int32_t>::Value(L, 4);
    piViewport(a0, a1, a2, a3);
    return 0;
}

static int lua_piGetViewport(lua_State* L)
{
    auto ret = piGetViewport();
    LuaValue<rect>::Push(L, ret);
    return 1;
}

static int lua_piWinding(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    piWinding(a0);
    return 0;
}

static int lua_piEnableFeatures(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    piEnableFeatures(a0);
    return 0;
}

static int lua_piDisableFeatures(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    piDisableFeatures(a0);
    return 0;
}

static int lua_piDrawElements(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    auto a2 = LuaArg<int>::Value(L, 3);
    piDrawElements(a0, a1, a2);
    return 0;
}

static int lua_piDrawElementsOffset(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    auto a2 = LuaArg<int>::Value(L, 3);
    auto a3 = LuaArg<int32_t>::Value(L, 4);
    piDrawElementsOffset(a0, a1, a2, a3);
    return 0;
}

static int lua_piDrawElementsInstanced(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    auto a2 = LuaArg<int>::Value(L, 3);
    auto a3 = LuaArg<int32_t>::Value(L, 4);
    piDrawElementsInstanced(a0, a1, a2, a3);
    return 0;
}

static int lua_piDrawArrays(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    auto a2 = LuaArg<int32_t>::Value(L, 3);
    piDrawArrays(a0, a1, a2);
    return 0;
}

static int lua_piDrawArraysInstanced(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    auto a2 = LuaArg<int32_t>::Value(L, 3);
    auto a3 = LuaArg<int>::Value(L, 4);
    piDrawArraysInstanced(a0, a1, a2, a3);
    return 0;
}

static int lua_piRetainGraphicsObject(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    piRetainGraphicsObject(a0);
    return 0;
}

static int lua_piReleaseGraphicsObject(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    piReleaseGraphicsObject(a0);
    return 0;
}

static int lua_piFlush(lua_State* L)
{
    piFlush();
    return 0;
}

static int lua_piPolygonOffset(lua_State* L)
{
    auto a0 = LuaArg<float>::Value(L, 1);
    auto a1 = LuaArg<float>::Value(L, 2);
    piPolygonOffset(a0, a1);
    return 0;
}

static int lua_piPolygonMode(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int>::Value(L, 2);
    piPolygonMode(a0, a1);
    return 0;
}

static int lua_piDepthFunction(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    piDepthFunction(a0);
    return 0;
}

static int lua_piClearDepth(lua_State* L)
{
    auto a0 = LuaArg<float>::Value(L, 1);
    piClearDepth(a0);
    return 0;
}

static int lua_piDepthMask(lua_State* L)
{
    auto a0 = LuaArg<bool>::Value(L, 1);
    piDepthMask(a0);
    return 0;
}

static int lua_piClearColor(lua_State* L)
{
    auto a0 = LuaArg<vec4>::Value(L, 1);
    piClearColor(a0);
    return 0;
}

static int lua_piColorMask(lua_State* L)
{
    auto a0 = LuaArg<bool>::Value(L, 1);
    auto a1 = LuaArg<bool>::Value(L, 2);
    auto a2 = LuaArg<bool>::Value(L, 3);
    auto a3 = LuaArg<bool>::Value(L, 4);
    piColorMask(a0, a1, a2, a3);
    return 0;
}

static int lua_piClearStencil(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    piClearStencil(a0);
    return 0;
}

static int lua_piStencilFunc(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    auto a2 = LuaArg<int64_t>::Value(L, 3);
    piStencilFunc(a0, a1, a2);
    return 0;
}

static int lua_piStencilOp(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int>::Value(L, 2);
    auto a2 = LuaArg<int>::Value(L, 3);
    piStencilOp(a0, a1, a2);
    return 0;
}

static int lua_piStencilOpSeparate(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int>::Value(L, 2);
    auto a2 = LuaArg<int>::Value(L, 3);
    auto a3 = LuaArg<int>::Value(L, 4);
    piStencilOpSeparate(a0, a1, a2, a3);
    return 0;
}

static int lua_piStencilMask(lua_State* L)
{
    auto a0 = LuaArg<int64_t>::Value(L, 1);
    piStencilMask(a0);
    return 0;
}

static int lua_piLineWidth(lua_State* L)
{
    auto a0 = LuaArg<float>::Value(L, 1);
    piLineWidth(a0);
    return 0;
}

static int lua_piHint(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int>::Value(L, 2);
    piHint(a0, a1);
    return 0;
}

static int lua_piCreateProgram(lua_State* L)
{
    auto ret = piCreateProgram();
    LuaValue<int32_t>::Push(L, ret);
    return 1;
}

static int lua_piCompileProgram(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<std::string>::Value(L, 3);
    piCompileProgram(a0, a1, a2);
    return 0;
}

static int lua_piLinkProgram(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    piLinkProgram(a0);
    return 0;
}

static int lua_piUseProgram(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    piUseProgram(a0);
    return 0;
}

static int lua_piBindVertexAttr(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    auto a2 = LuaArg<std::string>::Value(L, 3);
    piBindVertexAttr(a0, a1, a2);
    return 0;
}

static int lua_piEnableVertexAttr(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    piEnableVertexAttr(a0);
    return 0;
}

static int lua_piDisableVertexAttr(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    piDisableVertexAttr(a0);
    return 0;
}

static int lua_piVertexAttr(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    auto a2 = LuaArg<int32_t>::Value(L, 3);
    auto a3 = LuaArg<int32_t>::Value(L, 4);
    auto a4 = LuaArg<int32_t>::Value(L, 5);
    piVertexAttr(a0, a1, a2, a3, a4);
    return 0;
}

static int lua_piUniform1i(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<int32_t>::Value(L, 3);
    piUniform1i(a0, a1, a2);
    return 0;
}

static int lua_piUniform1iv(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<iI32Array*>::Value(L, 3);
    piUniform1iv(a0, a1, a2);
    return 0;
}

static int lua_piUniform2i(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<int32_t>::Value(L, 3);
    auto a3 = LuaArg<int32_t>::Value(L, 4);
    piUniform2i(a0, a1, a2, a3);
    return 0;
}

static int lua_piUniform2iv(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<iI32Array*>::Value(L, 3);
    piUniform2iv(a0, a1, a2);
    return 0;
}

static int lua_piUniform3i(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<int32_t>::Value(L, 3);
    auto a3 = LuaArg<int32_t>::Value(L, 4);
    auto a4 = LuaArg<int32_t>::Value(L, 5);
    piUniform3i(a0, a1, a2, a3, a4);
    return 0;
}

static int lua_piUniform3iv(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<iI32Array*>::Value(L, 3);
    piUniform3iv(a0, a1, a2);
    return 0;
}

static int lua_piUniform4i(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<int32_t>::Value(L, 3);
    auto a3 = LuaArg<int32_t>::Value(L, 4);
    auto a4 = LuaArg<int32_t>::Value(L, 5);
    auto a5 = LuaArg<int32_t>::Value(L, 6);
    piUniform4i(a0, a1, a2, a3, a4, a5);
    return 0;
}

static int lua_piUniform4iv(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<iI32Array*>::Value(L, 3);
    piUniform4iv(a0, a1, a2);
    return 0;
}

static int lua_piUniform1f(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<float>::Value(L, 3);
    piUniform1f(a0, a1, a2);
    return 0;
}

static int lua_piUniform1fv(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<iF32Array*>::Value(L, 3);
    piUniform1fv(a0, a1, a2);
    return 0;
}

static int lua_piUniform2f(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<float>::Value(L, 3);
    auto a3 = LuaArg<float>::Value(L, 4);
    piUniform2f(a0, a1, a2, a3);
    return 0;
}

static int lua_piUniform2fv(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<iF32Array*>::Value(L, 3);
    piUniform2fv(a0, a1, a2);
    return 0;
}

static int lua_piUniform3f(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<float>::Value(L, 3);
    auto a3 = LuaArg<float>::Value(L, 4);
    auto a4 = LuaArg<float>::Value(L, 5);
    piUniform3f(a0, a1, a2, a3, a4);
    return 0;
}

static int lua_piUniform3fv(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<iF32Array*>::Value(L, 3);
    piUniform3fv(a0, a1, a2);
    return 0;
}

static int lua_piUniform4f(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<float>::Value(L, 3);
    auto a3 = LuaArg<float>::Value(L, 4);
    auto a4 = LuaArg<float>::Value(L, 5);
    auto a5 = LuaArg<float>::Value(L, 6);
    piUniform4f(a0, a1, a2, a3, a4, a5);
    return 0;
}

static int lua_piUniform4fv(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<iF32Array*>::Value(L, 3);
    piUniform4fv(a0, a1, a2);
    return 0;
}

static int lua_piUniformVec3v(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<iVec3Array*>::Value(L, 3);
    piUniformVec3v(a0, a1, a2);
    return 0;
}

static int lua_piUniformVec3(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<vec3>::Value(L, 3);
    piUniformVec3(a0, a1, a2);
    return 0;
}

static int lua_piUniformVec4(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<vec4>::Value(L, 3);
    piUniformVec4(a0, a1, a2);
    return 0;
}

static int lua_piUniformRect(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<rect>::Value(L, 3);
    piUniformRect(a0, a1, a2);
    return 0;
}

static int lua_piUniformMat4f(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<mat4>::Value(L, 3);
    piUniformMat4f(a0, a1, a2);
    return 0;
}

static int lua_piUniformMat4fv(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<std::string>::Value(L, 2);
    auto a2 = LuaArg<iMat4Array*>::Value(L, 3);
    piUniformMat4fv(a0, a1, a2);
    return 0;
}

static int lua_piCreateVertexArray(lua_State* L)
{
    auto ret = piCreateVertexArray();
    LuaValue<int32_t>::Push(L, ret);
    return 1;
}

static int lua_piBindVertexArray(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    piBindVertexArray(a0);
    return 0;
}

static int lua_piCreateBuffer(lua_State* L)
{
    auto ret = piCreateBuffer();
    LuaValue<int32_t>::Push(L, ret);
    return 1;
}

static int lua_piBindBuffer(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    piBindBuffer(a0, a1);
    return 0;
}

static int lua_piBufferData(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    auto a2 = LuaArg<int64_t>::Value(L, 3);
    auto a3 = LuaArg<iMemory*>::Value(L, 4);
    piBufferData(a0, a1, a2, a3);
    return 0;
}

static int lua_piBufferSubData(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int64_t>::Value(L, 2);
    auto a2 = LuaArg<int64_t>::Value(L, 3);
    auto a3 = LuaArg<iMemory*>::Value(L, 4);
    piBufferSubData(a0, a1, a2, a3);
    return 0;
}

static int lua_piGenerateMipmap(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    piGenerateMipmap(a0);
    return 0;
}

static int lua_piCreateTexture(lua_State* L)
{
    auto ret = piCreateTexture();
    LuaValue<int32_t>::Push(L, ret);
    return 1;
}

static int lua_piActiveTexture(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    piActiveTexture(a0);
    return 0;
}

static int lua_piBindTexture(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    piBindTexture(a0, a1);
    return 0;
}

static int lua_piTexParam(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    auto a2 = LuaArg<int32_t>::Value(L, 3);
    piTexParam(a0, a1, a2);
    return 0;
}

static int lua_piTexImage2D(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    auto a2 = LuaArg<int32_t>::Value(L, 3);
    auto a3 = LuaArg<iBitmap*>::Value(L, 4);
    auto a4 = LuaArg<int32_t>::Value(L, 5);
    piTexImage2D(a0, a1, a2, a3, a4);
    return 0;
}

static int lua_piCompressedTexImage2D(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    auto a2 = LuaArg<int32_t>::Value(L, 3);
    auto a3 = LuaArg<iBitmap*>::Value(L, 4);
    auto a4 = LuaArg<int32_t>::Value(L, 5);
    piCompressedTexImage2D(a0, a1, a2, a3, a4);
    return 0;
}

static int lua_piCopyTexImage2D(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    auto a2 = LuaArg<int32_t>::Value(L, 3);
    auto a3 = LuaArg<int32_t>::Value(L, 4);
    auto a4 = LuaArg<int32_t>::Value(L, 5);
    auto a5 = LuaArg<int32_t>::Value(L, 6);
    auto a6 = LuaArg<int32_t>::Value(L, 7);
    piCopyTexImage2D(a0, a1, a2, a3, a4, a5, a6);
    return 0;
}

static int lua_piTexSubImage2D(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    auto a2 = LuaArg<int32_t>::Value(L, 3);
    auto a3 = LuaArg<int32_t>::Value(L, 4);
    auto a4 = LuaArg<int32_t>::Value(L, 5);
    auto a5 = LuaArg<int32_t>::Value(L, 6);
    auto a6 = LuaArg<iBitmap*>::Value(L, 7);
    auto a7 = LuaArg<int32_t>::Value(L, 8);
    piTexSubImage2D(a0, a1, a2, a3, a4, a5, a6, a7);
    return 0;
}

static int lua_piCompressedTexSubImage2D(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    auto a2 = LuaArg<int32_t>::Value(L, 3);
    auto a3 = LuaArg<int32_t>::Value(L, 4);
    auto a4 = LuaArg<int32_t>::Value(L, 5);
    auto a5 = LuaArg<int32_t>::Value(L, 6);
    auto a6 = LuaArg<iBitmap*>::Value(L, 7);
    auto a7 = LuaArg<int32_t>::Value(L, 8);
    piCompressedTexSubImage2D(a0, a1, a2, a3, a4, a5, a6, a7);
    return 0;
}

static int lua_piCopyTexSubImage2D(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    auto a2 = LuaArg<int32_t>::Value(L, 3);
    auto a3 = LuaArg<int32_t>::Value(L, 4);
    auto a4 = LuaArg<int32_t>::Value(L, 5);
    auto a5 = LuaArg<int32_t>::Value(L, 6);
    auto a6 = LuaArg<int32_t>::Value(L, 7);
    auto a7 = LuaArg<int32_t>::Value(L, 8);
    auto a8 = LuaArg<int32_t>::Value(L, 9);
    piCopyTexSubImage2D(a0, a1, a2, a3, a4, a5, a6, a7, a8);
    return 0;
}

static int lua_piPixelStorei(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    piPixelStorei(a0, a1);
    return 0;
}

static int lua_piGetFramebuffer(lua_State* L)
{
    auto ret = piGetFramebuffer();
    LuaValue<int32_t>::Push(L, ret);
    return 1;
}

static int lua_piCreateFramebuffer(lua_State* L)
{
    auto ret = piCreateFramebuffer();
    LuaValue<int32_t>::Push(L, ret);
    return 1;
}

static int lua_piBindFramebuffer(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    piBindFramebuffer(a0, a1);
    return 0;
}

static int lua_piFramebufferTexture2D(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int>::Value(L, 2);
    auto a2 = LuaArg<int>::Value(L, 3);
    auto a3 = LuaArg<int>::Value(L, 4);
    auto a4 = LuaArg<int32_t>::Value(L, 5);
    piFramebufferTexture2D(a0, a1, a2, a3, a4);
    return 0;
}

static int lua_piFramebufferRenderbuffer(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int>::Value(L, 2);
    auto a2 = LuaArg<int32_t>::Value(L, 3);
    piFramebufferRenderbuffer(a0, a1, a2);
    return 0;
}

static int lua_piVerifyFramebufferState(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    piVerifyFramebufferState(a0);
    return 0;
}

static int lua_piCreateRenderbuffer(lua_State* L)
{
    auto ret = piCreateRenderbuffer();
    LuaValue<int32_t>::Push(L, ret);
    return 1;
}

static int lua_piBindRenderbuffer(lua_State* L)
{
    auto a0 = LuaArg<int32_t>::Value(L, 1);
    piBindRenderbuffer(a0);
    return 0;
}

static int lua_piRenderbufferStorage(lua_State* L)
{
    auto a0 = LuaArg<int>::Value(L, 1);
    auto a1 = LuaArg<int32_t>::Value(L, 2);
    auto a2 = LuaArg<int32_t>::Value(L, 3);
    piRenderbufferStorage(a0, a1, a2);
    return 0;
}

static int lua_CreateGraphicsObject(lua_State* L)
{
    auto ret = CreateGraphicsObject();
    LuaValue<iGraphicsObject*>::Push(L, ret);
    return 1;
}

void lua_pi_Graphics_Init(lua_State* L)
{
    // Register interfaces
    nspi_iGraphicsObject_Init(L, false);

    // Register enums

    // Register global functions
    lua_pushcfunction(L, lua_piGetGraphicsType);
    lua_setfield(L, -2, "GetGraphicsType");
    lua_pushcfunction(L, lua_piAddGraphicsAlias);
    lua_setfield(L, -2, "AddGraphicsAlias");
    lua_pushcfunction(L, lua_piRemoveGraphicsAlias);
    lua_setfield(L, -2, "RemoveGraphicsAlias");
    lua_pushcfunction(L, lua_piGetGraphicsObject);
    lua_setfield(L, -2, "GetGraphicsObject");
    lua_pushcfunction(L, lua_piPushGroupMarker);
    lua_setfield(L, -2, "PushGroupMarker");
    lua_pushcfunction(L, lua_piPopGroupMarker);
    lua_setfield(L, -2, "PopGroupMarker");
    lua_pushcfunction(L, lua_piInsertEventMarker);
    lua_setfield(L, -2, "InsertEventMarker");
    lua_pushcfunction(L, lua_piClear);
    lua_setfield(L, -2, "Clear");
    lua_pushcfunction(L, lua_piBlendColor);
    lua_setfield(L, -2, "BlendColor");
    lua_pushcfunction(L, lua_piBlendEquation);
    lua_setfield(L, -2, "BlendEquation");
    lua_pushcfunction(L, lua_piBlendFunc);
    lua_setfield(L, -2, "BlendFunc");
    lua_pushcfunction(L, lua_piBlendFuncSeparate);
    lua_setfield(L, -2, "BlendFuncSeparate");
    lua_pushcfunction(L, lua_piCullFace);
    lua_setfield(L, -2, "CullFace");
    lua_pushcfunction(L, lua_piViewport);
    lua_setfield(L, -2, "Viewport");
    lua_pushcfunction(L, lua_piGetViewport);
    lua_setfield(L, -2, "GetViewport");
    lua_pushcfunction(L, lua_piWinding);
    lua_setfield(L, -2, "Winding");
    lua_pushcfunction(L, lua_piEnableFeatures);
    lua_setfield(L, -2, "EnableFeatures");
    lua_pushcfunction(L, lua_piDisableFeatures);
    lua_setfield(L, -2, "DisableFeatures");
    lua_pushcfunction(L, lua_piDrawElements);
    lua_setfield(L, -2, "DrawElements");
    lua_pushcfunction(L, lua_piDrawElementsOffset);
    lua_setfield(L, -2, "DrawElementsOffset");
    lua_pushcfunction(L, lua_piDrawElementsInstanced);
    lua_setfield(L, -2, "DrawElementsInstanced");
    lua_pushcfunction(L, lua_piDrawArrays);
    lua_setfield(L, -2, "DrawArrays");
    lua_pushcfunction(L, lua_piDrawArraysInstanced);
    lua_setfield(L, -2, "DrawArraysInstanced");
    lua_pushcfunction(L, lua_piRetainGraphicsObject);
    lua_setfield(L, -2, "RetainGraphicsObject");
    lua_pushcfunction(L, lua_piReleaseGraphicsObject);
    lua_setfield(L, -2, "ReleaseGraphicsObject");
    lua_pushcfunction(L, lua_piFlush);
    lua_setfield(L, -2, "Flush");
    lua_pushcfunction(L, lua_piPolygonOffset);
    lua_setfield(L, -2, "PolygonOffset");
    lua_pushcfunction(L, lua_piPolygonMode);
    lua_setfield(L, -2, "PolygonMode");
    lua_pushcfunction(L, lua_piDepthFunction);
    lua_setfield(L, -2, "DepthFunction");
    lua_pushcfunction(L, lua_piClearDepth);
    lua_setfield(L, -2, "ClearDepth");
    lua_pushcfunction(L, lua_piDepthMask);
    lua_setfield(L, -2, "DepthMask");
    lua_pushcfunction(L, lua_piClearColor);
    lua_setfield(L, -2, "ClearColor");
    lua_pushcfunction(L, lua_piColorMask);
    lua_setfield(L, -2, "ColorMask");
    lua_pushcfunction(L, lua_piClearStencil);
    lua_setfield(L, -2, "ClearStencil");
    lua_pushcfunction(L, lua_piStencilFunc);
    lua_setfield(L, -2, "StencilFunc");
    lua_pushcfunction(L, lua_piStencilOp);
    lua_setfield(L, -2, "StencilOp");
    lua_pushcfunction(L, lua_piStencilOpSeparate);
    lua_setfield(L, -2, "StencilOpSeparate");
    lua_pushcfunction(L, lua_piStencilMask);
    lua_setfield(L, -2, "StencilMask");
    lua_pushcfunction(L, lua_piLineWidth);
    lua_setfield(L, -2, "LineWidth");
    lua_pushcfunction(L, lua_piHint);
    lua_setfield(L, -2, "Hint");
    lua_pushcfunction(L, lua_piCreateProgram);
    lua_setfield(L, -2, "CreateProgram");
    lua_pushcfunction(L, lua_piCompileProgram);
    lua_setfield(L, -2, "CompileProgram");
    lua_pushcfunction(L, lua_piLinkProgram);
    lua_setfield(L, -2, "LinkProgram");
    lua_pushcfunction(L, lua_piUseProgram);
    lua_setfield(L, -2, "UseProgram");
    lua_pushcfunction(L, lua_piBindVertexAttr);
    lua_setfield(L, -2, "BindVertexAttr");
    lua_pushcfunction(L, lua_piEnableVertexAttr);
    lua_setfield(L, -2, "EnableVertexAttr");
    lua_pushcfunction(L, lua_piDisableVertexAttr);
    lua_setfield(L, -2, "DisableVertexAttr");
    lua_pushcfunction(L, lua_piVertexAttr);
    lua_setfield(L, -2, "VertexAttr");
    lua_pushcfunction(L, lua_piUniform1i);
    lua_setfield(L, -2, "Uniform1i");
    lua_pushcfunction(L, lua_piUniform1iv);
    lua_setfield(L, -2, "Uniform1iv");
    lua_pushcfunction(L, lua_piUniform2i);
    lua_setfield(L, -2, "Uniform2i");
    lua_pushcfunction(L, lua_piUniform2iv);
    lua_setfield(L, -2, "Uniform2iv");
    lua_pushcfunction(L, lua_piUniform3i);
    lua_setfield(L, -2, "Uniform3i");
    lua_pushcfunction(L, lua_piUniform3iv);
    lua_setfield(L, -2, "Uniform3iv");
    lua_pushcfunction(L, lua_piUniform4i);
    lua_setfield(L, -2, "Uniform4i");
    lua_pushcfunction(L, lua_piUniform4iv);
    lua_setfield(L, -2, "Uniform4iv");
    lua_pushcfunction(L, lua_piUniform1f);
    lua_setfield(L, -2, "Uniform1f");
    lua_pushcfunction(L, lua_piUniform1fv);
    lua_setfield(L, -2, "Uniform1fv");
    lua_pushcfunction(L, lua_piUniform2f);
    lua_setfield(L, -2, "Uniform2f");
    lua_pushcfunction(L, lua_piUniform2fv);
    lua_setfield(L, -2, "Uniform2fv");
    lua_pushcfunction(L, lua_piUniform3f);
    lua_setfield(L, -2, "Uniform3f");
    lua_pushcfunction(L, lua_piUniform3fv);
    lua_setfield(L, -2, "Uniform3fv");
    lua_pushcfunction(L, lua_piUniform4f);
    lua_setfield(L, -2, "Uniform4f");
    lua_pushcfunction(L, lua_piUniform4fv);
    lua_setfield(L, -2, "Uniform4fv");
    lua_pushcfunction(L, lua_piUniformVec3v);
    lua_setfield(L, -2, "UniformVec3v");
    lua_pushcfunction(L, lua_piUniformVec3);
    lua_setfield(L, -2, "UniformVec3");
    lua_pushcfunction(L, lua_piUniformVec4);
    lua_setfield(L, -2, "UniformVec4");
    lua_pushcfunction(L, lua_piUniformRect);
    lua_setfield(L, -2, "UniformRect");
    lua_pushcfunction(L, lua_piUniformMat4f);
    lua_setfield(L, -2, "UniformMat4f");
    lua_pushcfunction(L, lua_piUniformMat4fv);
    lua_setfield(L, -2, "UniformMat4fv");
    lua_pushcfunction(L, lua_piCreateVertexArray);
    lua_setfield(L, -2, "CreateVertexArray");
    lua_pushcfunction(L, lua_piBindVertexArray);
    lua_setfield(L, -2, "BindVertexArray");
    lua_pushcfunction(L, lua_piCreateBuffer);
    lua_setfield(L, -2, "CreateBuffer");
    lua_pushcfunction(L, lua_piBindBuffer);
    lua_setfield(L, -2, "BindBuffer");
    lua_pushcfunction(L, lua_piBufferData);
    lua_setfield(L, -2, "BufferData");
    lua_pushcfunction(L, lua_piBufferSubData);
    lua_setfield(L, -2, "BufferSubData");
    lua_pushcfunction(L, lua_piGenerateMipmap);
    lua_setfield(L, -2, "GenerateMipmap");
    lua_pushcfunction(L, lua_piCreateTexture);
    lua_setfield(L, -2, "CreateTexture");
    lua_pushcfunction(L, lua_piActiveTexture);
    lua_setfield(L, -2, "ActiveTexture");
    lua_pushcfunction(L, lua_piBindTexture);
    lua_setfield(L, -2, "BindTexture");
    lua_pushcfunction(L, lua_piTexParam);
    lua_setfield(L, -2, "TexParam");
    lua_pushcfunction(L, lua_piTexImage2D);
    lua_setfield(L, -2, "TexImage2D");
    lua_pushcfunction(L, lua_piCompressedTexImage2D);
    lua_setfield(L, -2, "CompressedTexImage2D");
    lua_pushcfunction(L, lua_piCopyTexImage2D);
    lua_setfield(L, -2, "CopyTexImage2D");
    lua_pushcfunction(L, lua_piTexSubImage2D);
    lua_setfield(L, -2, "TexSubImage2D");
    lua_pushcfunction(L, lua_piCompressedTexSubImage2D);
    lua_setfield(L, -2, "CompressedTexSubImage2D");
    lua_pushcfunction(L, lua_piCopyTexSubImage2D);
    lua_setfield(L, -2, "CopyTexSubImage2D");
    lua_pushcfunction(L, lua_piPixelStorei);
    lua_setfield(L, -2, "PixelStorei");
    lua_pushcfunction(L, lua_piGetFramebuffer);
    lua_setfield(L, -2, "GetFramebuffer");
    lua_pushcfunction(L, lua_piCreateFramebuffer);
    lua_setfield(L, -2, "CreateFramebuffer");
    lua_pushcfunction(L, lua_piBindFramebuffer);
    lua_setfield(L, -2, "BindFramebuffer");
    lua_pushcfunction(L, lua_piFramebufferTexture2D);
    lua_setfield(L, -2, "FramebufferTexture2D");
    lua_pushcfunction(L, lua_piFramebufferRenderbuffer);
    lua_setfield(L, -2, "FramebufferRenderbuffer");
    lua_pushcfunction(L, lua_piVerifyFramebufferState);
    lua_setfield(L, -2, "VerifyFramebufferState");
    lua_pushcfunction(L, lua_piCreateRenderbuffer);
    lua_setfield(L, -2, "CreateRenderbuffer");
    lua_pushcfunction(L, lua_piBindRenderbuffer);
    lua_setfield(L, -2, "BindRenderbuffer");
    lua_pushcfunction(L, lua_piRenderbufferStorage);
    lua_setfield(L, -2, "RenderbufferStorage");
    lua_pushcfunction(L, lua_CreateGraphicsObject);
    lua_setfield(L, -2, "CreateGraphicsObject");
}

