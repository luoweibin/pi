/*******************************************************************************
 See copyright notice in LICENSE.

 **DO NOT** edit manually.

 This is generated by pilua, which is a lua binding code generator.
 ******************************************************************************/

using namespace nspi;
using namespace std;

/**
 * ==========================================================================
 * nspi_iBitmapAsset
 */

static int nspi_iBitmapAsset_GC(lua_State* L)
{
    auto refObject = LuaArg<nspi::iBitmapAsset*>::Value(L, 1);
    refObject->Release();
    return 0;
}

static int nspi_iBitmapAsset_ToString(lua_State* L)
{
    auto refObject = LuaArg<nspi::iBitmapAsset*>::Value(L, 1);
    auto str = refObject->ToString();
    LuaValue<std::string>::Push(L, str);
    return 1;
}

static int nspi_iBitmapAsset_GetClass(lua_State* L)
{
    auto refObject = LuaArg<nspi::iBitmapAsset*>::Value(L, 1);
    auto klass = refObject->GetClass();
    LuaValue<iClass*>::Push(L, klass);
    return 1;
}

static int nspi_iBitmapAsset_GetBitmap(lua_State* L)
{
    auto refObject = LuaArg<nspi::iBitmapAsset*>::Value(L, 1);
    auto ret = refObject->GetBitmap();
    LuaValue<iBitmap*>::Push(L, ret);
    return 1;
}

static int nspi_iBitmapAsset_SetBitmap(lua_State* L)
{
    auto refObject = LuaArg<nspi::iBitmapAsset*>::Value(L, 1);
    auto a1 = LuaArg<iBitmap*>::Value(L, 2);
    refObject->SetBitmap(a1);
    return 0;
}

void nspi_iAsset_Init(lua_State* L, bool derived);
void nspi_iBitmapAsset_Init(lua_State* L, bool derived)
{
    if (!derived)
    {
        luaL_newmetatable(L, "nspi::iBitmapAsset");
        lua_pushlightuserdata(L, nspi::iBitmapAsset::StaticClass());
        lua_setfield(L, -2, "_class");
        lua_pushcfunction(L, nspi_iBitmapAsset_GC);
        lua_setfield(L, -2, "__gc");
        lua_pushcfunction(L, nspi_iBitmapAsset_ToString);
        lua_setfield(L, -2, "__tostring");
        lua_newtable(L);
    }

    lua_pushcfunction(L, nspi_iBitmapAsset_GetBitmap);
    lua_setfield(L, -2, "GetBitmap");
    lua_pushcfunction(L, nspi_iBitmapAsset_SetBitmap);
    lua_setfield(L, -2, "SetBitmap");
    lua_pushcfunction(L, nspi_iBitmapAsset_GetClass);
    lua_setfield(L, -2, "GetClass");

    nspi_iAsset_Init(L, true);
    if (!derived)
    {
        lua_setfield(L, -2, "__index");
        lua_pop(L, 1);
    }

}

void lua_pi_asset_BitmapAsset_Init(lua_State* L)
{
    // Register interfaces
    nspi_iBitmapAsset_Init(L, false);

    // Register enums

    // Register global functions
}

