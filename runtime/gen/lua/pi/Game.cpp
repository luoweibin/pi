/*******************************************************************************
 See copyright notice in LICENSE.

 **DO NOT** edit manually.

 This is generated by pilua, which is a lua binding code generator.
 ******************************************************************************/

using namespace nspi;
using namespace std;

void lua_pi_Game_Init(lua_State* L)
{
    // Register interfaces

    // Register enums
lua_pushinteger(L, 2);
lua_setfield(L, -2, "eGameMsgPri_High");
lua_pushinteger(L, 0);
lua_setfield(L, -2, "eGameMsgPri_Low");
lua_pushinteger(L, 1);
lua_setfield(L, -2, "eGameMsgPri_Normal");
lua_pushinteger(L, 10);
lua_setfield(L, -2, "eGameMsg_CaptureFrame");
lua_pushinteger(L, 1);
lua_setfield(L, -2, "eGameMsg_HIDEvent");
lua_pushinteger(L, 4);
lua_setfield(L, -2, "eGameMsg_Load");
lua_pushinteger(L, 8);
lua_setfield(L, -2, "eGameMsg_LoadScene");
lua_pushinteger(L, 6);
lua_setfield(L, -2, "eGameMsg_Pause");
lua_pushinteger(L, 2);
lua_setfield(L, -2, "eGameMsg_Resize");
lua_pushinteger(L, 7);
lua_setfield(L, -2, "eGameMsg_Resume");
lua_pushinteger(L, 12);
lua_setfield(L, -2, "eGameMsg_SetGraphicsVM");
lua_pushinteger(L, 13);
lua_setfield(L, -2, "eGameMsg_SetResContext");
lua_pushinteger(L, 5);
lua_setfield(L, -2, "eGameMsg_Unload");
lua_pushinteger(L, 9);
lua_setfield(L, -2, "eGameMsg_UnloadScene");
lua_pushinteger(L, 3);
lua_setfield(L, -2, "eGameMsg_Update");
lua_pushinteger(L, 11);
lua_setfield(L, -2, "eGameMsg_UpdateFrame");
lua_pushinteger(L, 100000);
lua_setfield(L, -2, "eGameMsg_User");

    // Register global functions
}

