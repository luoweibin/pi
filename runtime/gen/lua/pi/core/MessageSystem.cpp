/*******************************************************************************
 See copyright notice in LICENSE.

 **DO NOT** edit manually.

 This is generated by pilua, which is a lua binding code generator.
 ******************************************************************************/

using namespace nspi;
using namespace std;

/**
 * ==========================================================================
 * nspi_iMessage
 */

static int nspi_iMessage_GC(lua_State* L)
{
    auto refObject = LuaArg<nspi::iMessage*>::Value(L, 1);
    refObject->Release();
    return 0;
}

static int nspi_iMessage_ToString(lua_State* L)
{
    auto refObject = LuaArg<nspi::iMessage*>::Value(L, 1);
    auto str = refObject->ToString();
    LuaValue<std::string>::Push(L, str);
    return 1;
}

static int nspi_iMessage_GetClass(lua_State* L)
{
    auto refObject = LuaArg<nspi::iMessage*>::Value(L, 1);
    auto klass = refObject->GetClass();
    LuaValue<iClass*>::Push(L, klass);
    return 1;
}

static int nspi_iMessage_GetID(lua_State* L)
{
    auto refObject = LuaArg<nspi::iMessage*>::Value(L, 1);
    auto ret = refObject->GetID();
    LuaValue<int32_t>::Push(L, ret);
    return 1;
}

static int nspi_iMessage_GetArg1(lua_State* L)
{
    auto refObject = LuaArg<nspi::iMessage*>::Value(L, 1);
    auto ret = refObject->GetArg1();
    LuaValue<Var>::Push(L, ret);
    return 1;
}

static int nspi_iMessage_GetArg2(lua_State* L)
{
    auto refObject = LuaArg<nspi::iMessage*>::Value(L, 1);
    auto ret = refObject->GetArg2();
    LuaValue<Var>::Push(L, ret);
    return 1;
}

static int nspi_iMessage_SetSender(lua_State* L)
{
    auto refObject = LuaArg<nspi::iMessage*>::Value(L, 1);
    auto a0 = LuaArg<iRefObject*>::Value(L, 2);
    refObject->SetSender(a0);
    return 0;
}

static int nspi_iMessage_GetSender(lua_State* L)
{
    auto refObject = LuaArg<nspi::iMessage*>::Value(L, 1);
    auto ret = refObject->GetSender();
    LuaValue<iRefObject*>::Push(L, ret);
    return 1;
}

static int nspi_iMessage_Clone(lua_State* L)
{
    auto refObject = LuaArg<nspi::iMessage*>::Value(L, 1);
    auto ret = refObject->Clone();
    LuaValue<iMessage*>::Push(L, ret);
    return 1;
}

void nspi_iRefObject_Init(lua_State* L, bool derived);
void nspi_iMessage_Init(lua_State* L, bool derived)
{
    if (!derived)
    {
        luaL_newmetatable(L, "nspi::iMessage");
        lua_pushlightuserdata(L, nspi::iMessage::StaticClass());
        lua_setfield(L, -2, "_class");
        lua_pushcfunction(L, nspi_iMessage_GC);
        lua_setfield(L, -2, "__gc");
        lua_pushcfunction(L, nspi_iMessage_ToString);
        lua_setfield(L, -2, "__tostring");
        lua_newtable(L);
    }

    lua_pushcfunction(L, nspi_iMessage_GetID);
    lua_setfield(L, -2, "GetID");
    lua_pushcfunction(L, nspi_iMessage_GetArg1);
    lua_setfield(L, -2, "GetArg1");
    lua_pushcfunction(L, nspi_iMessage_GetArg2);
    lua_setfield(L, -2, "GetArg2");
    lua_pushcfunction(L, nspi_iMessage_SetSender);
    lua_setfield(L, -2, "SetSender");
    lua_pushcfunction(L, nspi_iMessage_GetSender);
    lua_setfield(L, -2, "GetSender");
    lua_pushcfunction(L, nspi_iMessage_Clone);
    lua_setfield(L, -2, "Clone");
    lua_pushcfunction(L, nspi_iMessage_GetClass);
    lua_setfield(L, -2, "GetClass");

    nspi_iRefObject_Init(L, true);
    if (!derived)
    {
        lua_setfield(L, -2, "__index");
        lua_pop(L, 1);
    }

}

void lua_pi_core_MessageSystem_Init(lua_State* L)
{
    // Register interfaces
    nspi_iMessage_Init(L, false);

    // Register enums

    // Register global functions
}

