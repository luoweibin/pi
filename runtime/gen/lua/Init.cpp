#include "pi/Asset.cpp"
#include "pi/CV.cpp"
#include "pi/Core.cpp"
#include "pi/Crypto.cpp"
#include "pi/DOM.cpp"
#include "pi/Game.cpp"
#include "pi/Graphics.cpp"
#include "pi/Input.cpp"
#include "pi/Media.cpp"
#include "pi/PIConfig.cpp"
#include "pi/Scripting.cpp"
#include "pi/Test.cpp"
#include "pi/asset/AnimSeq.cpp"
#include "pi/asset/AssetManager.cpp"
#include "pi/asset/BitmapAsset.cpp"
#include "pi/asset/GraphicsAsset.cpp"
#include "pi/asset/MemoryAsset.cpp"
#include "pi/asset/Texture.cpp"
#include "pi/core/Algorithm.cpp"
#include "pi/core/Async.cpp"
#include "pi/core/CoreArray.cpp"
#include "pi/core/CoreMath.cpp"
#include "pi/core/CoreMemory.cpp"
#include "pi/core/CoreStream.cpp"
#include "pi/core/CoreString.cpp"
#include "pi/core/CoreTime.cpp"
#include "pi/core/CoreTypes.cpp"
#include "pi/core/Log.cpp"
#include "pi/core/MessageSystem.cpp"
#include "pi/core/Platform.cpp"
#include "pi/core/RefObject.cpp"
#include "pi/core/Reflection.cpp"
#include "pi/core/System.cpp"
#include "pi/core/ToString.cpp"
#include "pi/core/Var.cpp"
#include "pi/core/impl/ArrayImpl.cpp"
#include "pi/core/impl/ReflectionImpl.cpp"
#include "pi/core/impl/StreamImpl.cpp"
#include "pi/core/impl/ThreadLocalImpl.cpp"
#include "pi/cv/Calibrate.cpp"
#include "pi/cv/FaceTracker.cpp"
#include "pi/cv/SenseTimeSDK.cpp"
#include "pi/game/2D.cpp"
#include "pi/game/2d/FrameAnim2D.cpp"
#include "pi/game/2d/Material2D.cpp"
#include "pi/game/2d/Quad.cpp"
#include "pi/game/3D.cpp"
#include "pi/game/3d/Material.cpp"
#include "pi/game/3d/MeshFile.cpp"
#include "pi/game/3d/Model.cpp"
#include "pi/game/3d/ModelMaterial.cpp"
#include "pi/game/3d/ModelMesh.cpp"
#include "pi/game/3d/ModelNode.cpp"
#include "pi/game/3d/ModelSkin.cpp"
#include "pi/game/3d/RenderState.cpp"
#include "pi/game/3d/RenderSystem3D.cpp"
#include "pi/game/3d/base_mesh.cpp"
#include "pi/game/3d/base_mesh/BaseMeshCommon.cpp"
#include "pi/game/3d/base_mesh/Face2DMesh.cpp"
#include "pi/game/3d/base_mesh/Face2DPointBinder.cpp"
#include "pi/game/Animation.cpp"
#include "pi/game/Camera.cpp"
#include "pi/game/Component.cpp"
#include "pi/game/Entity.cpp"
#include "pi/game/FaceCapture.cpp"
#include "pi/game/Filter.cpp"
#include "pi/game/FrameCapture.cpp"
#include "pi/game/GameConfig.cpp"
#include "pi/game/Particle.cpp"
#include "pi/game/Physics.cpp"
#include "pi/game/Scene.cpp"
#include "pi/game/SceneParent.cpp"
#include "pi/game/ScriptComp.cpp"
#include "pi/game/Scripting.cpp"
#include "pi/game/Serialization.cpp"
#include "pi/game/System.cpp"
#include "pi/game/Table.cpp"
#include "pi/game/Transform.cpp"
#include "pi/game/animation/AnimCurve.cpp"
#include "pi/game/animation/Animation.cpp"
#include "pi/game/animation/ModelAnim.cpp"
#include "pi/game/base_system.cpp"
#include "pi/game/base_system/CaptureBackRemovalSystem.cpp"
#include "pi/game/impl/AnimationImpl.cpp"
#include "pi/game/impl/AssetAnimImpl.cpp"
#include "pi/game/impl/AssetLoaderImpl.cpp"
#include "pi/game/impl/CameraImpl.cpp"
#include "pi/game/impl/ComponentImpl.cpp"
#include "pi/game/impl/DynamicMeshImpl.cpp"
#include "pi/game/impl/ParticleImpl.cpp"
#include "pi/game/impl/RendererImpl.cpp"
#include "pi/game/impl/SystemImpl.cpp"
#include "pi/game/impl/TransformImpl.cpp"
#include "pi/game/particle/ParticleEmitter.cpp"
#include "pi/game/particle/ParticleLib.cpp"
#include "pi/game/particle/ParticleShader.cpp"
#include "pi/game/particle/ParticleSystem.cpp"
#include "pi/graphics/Bitmap.cpp"
#include "pi/graphics/GraphicsConst.cpp"
#include "pi/graphics/GraphicsContext.cpp"
#include "pi/graphics/GraphicsVM.cpp"
#include "pi/graphics/PixelFormat.cpp"
#include "pi/graphics/android/GraphicsContext_android.cpp"
#include "pi/graphics/iOS/GraphicsContext_iOS.cpp"
#include "pi/graphics/macOS/GraphicsContext_macOS.cpp"
#include "pi/input/AVCapture.cpp"
#include "pi/input/FaceTracker.cpp"
#include "pi/input/HID.cpp"
#include "pi/input/HIDEvent.cpp"
#include "pi/input/Keyboard.cpp"
#include "pi/input/Motion.cpp"
#include "pi/input/Touch.cpp"
#include "pi/input/impl/HIDEventImpl.cpp"
#include "pi/lua/LuaScript.cpp"
#include "pi/media/AudioEngine.cpp"
#include "pi/media/AudioRenderer.cpp"
#include "pi/media/MediaConfig.cpp"
#include "pi/media/MediaFrame.cpp"
#include "pi/media/apple/MediaFrame_AVFoundation.cpp"
#include "pi/wrapper/GameBuffer.cpp"
#include "pi/wrapper/GameImpl.cpp"
#include "pi/wrapper/QuadRenderer.cpp"
#include "pi/wrapper/iOS/PIGameView.cpp"
#include "pi/wrapper/macOS/PIGameView.cpp"
void lua_pi_Init(lua_State* L)
{
    lua_pi_Asset_Init(L);
    lua_pi_CV_Init(L);
    lua_pi_Core_Init(L);
    lua_pi_Crypto_Init(L);
    lua_pi_DOM_Init(L);
    lua_pi_Game_Init(L);
    lua_pi_Graphics_Init(L);
    lua_pi_Input_Init(L);
    lua_pi_Media_Init(L);
    lua_pi_PIConfig_Init(L);
    lua_pi_Scripting_Init(L);
    lua_pi_Test_Init(L);
    lua_pi_asset_AnimSeq_Init(L);
    lua_pi_asset_AssetManager_Init(L);
    lua_pi_asset_BitmapAsset_Init(L);
    lua_pi_asset_GraphicsAsset_Init(L);
    lua_pi_asset_MemoryAsset_Init(L);
    lua_pi_asset_Texture_Init(L);
    lua_pi_core_Algorithm_Init(L);
    lua_pi_core_Async_Init(L);
    lua_pi_core_CoreArray_Init(L);
    lua_pi_core_CoreMath_Init(L);
    lua_pi_core_CoreMemory_Init(L);
    lua_pi_core_CoreStream_Init(L);
    lua_pi_core_CoreString_Init(L);
    lua_pi_core_CoreTime_Init(L);
    lua_pi_core_CoreTypes_Init(L);
    lua_pi_core_Log_Init(L);
    lua_pi_core_MessageSystem_Init(L);
    lua_pi_core_Platform_Init(L);
    lua_pi_core_RefObject_Init(L);
    lua_pi_core_Reflection_Init(L);
    lua_pi_core_System_Init(L);
    lua_pi_core_ToString_Init(L);
    lua_pi_core_Var_Init(L);
    lua_pi_core_impl_ArrayImpl_Init(L);
    lua_pi_core_impl_ReflectionImpl_Init(L);
    lua_pi_core_impl_StreamImpl_Init(L);
    lua_pi_core_impl_ThreadLocalImpl_Init(L);
    lua_pi_cv_Calibrate_Init(L);
    lua_pi_cv_FaceTracker_Init(L);
    lua_pi_cv_SenseTimeSDK_Init(L);
    lua_pi_game_2D_Init(L);
    lua_pi_game_2d_FrameAnim2D_Init(L);
    lua_pi_game_2d_Material2D_Init(L);
    lua_pi_game_2d_Quad_Init(L);
    lua_pi_game_3D_Init(L);
    lua_pi_game_3d_Material_Init(L);
    lua_pi_game_3d_MeshFile_Init(L);
    lua_pi_game_3d_Model_Init(L);
    lua_pi_game_3d_ModelMaterial_Init(L);
    lua_pi_game_3d_ModelMesh_Init(L);
    lua_pi_game_3d_ModelNode_Init(L);
    lua_pi_game_3d_ModelSkin_Init(L);
    lua_pi_game_3d_RenderState_Init(L);
    lua_pi_game_3d_RenderSystem3D_Init(L);
    lua_pi_game_3d_base_mesh_Init(L);
    lua_pi_game_3d_base_mesh_BaseMeshCommon_Init(L);
    lua_pi_game_3d_base_mesh_Face2DMesh_Init(L);
    lua_pi_game_3d_base_mesh_Face2DPointBinder_Init(L);
    lua_pi_game_Animation_Init(L);
    lua_pi_game_Camera_Init(L);
    lua_pi_game_Component_Init(L);
    lua_pi_game_Entity_Init(L);
    lua_pi_game_FaceCapture_Init(L);
    lua_pi_game_Filter_Init(L);
    lua_pi_game_FrameCapture_Init(L);
    lua_pi_game_GameConfig_Init(L);
    lua_pi_game_Particle_Init(L);
    lua_pi_game_Physics_Init(L);
    lua_pi_game_Scene_Init(L);
    lua_pi_game_SceneParent_Init(L);
    lua_pi_game_ScriptComp_Init(L);
    lua_pi_game_Scripting_Init(L);
    lua_pi_game_Serialization_Init(L);
    lua_pi_game_System_Init(L);
    lua_pi_game_Table_Init(L);
    lua_pi_game_Transform_Init(L);
    lua_pi_game_animation_AnimCurve_Init(L);
    lua_pi_game_animation_Animation_Init(L);
    lua_pi_game_animation_ModelAnim_Init(L);
    lua_pi_game_base_system_Init(L);
    lua_pi_game_base_system_CaptureBackRemovalSystem_Init(L);
    lua_pi_game_impl_AnimationImpl_Init(L);
    lua_pi_game_impl_AssetAnimImpl_Init(L);
    lua_pi_game_impl_AssetLoaderImpl_Init(L);
    lua_pi_game_impl_CameraImpl_Init(L);
    lua_pi_game_impl_ComponentImpl_Init(L);
    lua_pi_game_impl_DynamicMeshImpl_Init(L);
    lua_pi_game_impl_ParticleImpl_Init(L);
    lua_pi_game_impl_RendererImpl_Init(L);
    lua_pi_game_impl_SystemImpl_Init(L);
    lua_pi_game_impl_TransformImpl_Init(L);
    lua_pi_game_particle_ParticleEmitter_Init(L);
    lua_pi_game_particle_ParticleLib_Init(L);
    lua_pi_game_particle_ParticleShader_Init(L);
    lua_pi_game_particle_ParticleSystem_Init(L);
    lua_pi_graphics_Bitmap_Init(L);
    lua_pi_graphics_GraphicsConst_Init(L);
    lua_pi_graphics_GraphicsContext_Init(L);
    lua_pi_graphics_GraphicsVM_Init(L);
    lua_pi_graphics_PixelFormat_Init(L);
    lua_pi_graphics_android_GraphicsContext_android_Init(L);
    lua_pi_graphics_iOS_GraphicsContext_iOS_Init(L);
    lua_pi_graphics_macOS_GraphicsContext_macOS_Init(L);
    lua_pi_input_AVCapture_Init(L);
    lua_pi_input_FaceTracker_Init(L);
    lua_pi_input_HID_Init(L);
    lua_pi_input_HIDEvent_Init(L);
    lua_pi_input_Keyboard_Init(L);
    lua_pi_input_Motion_Init(L);
    lua_pi_input_Touch_Init(L);
    lua_pi_input_impl_HIDEventImpl_Init(L);
    lua_pi_lua_LuaScript_Init(L);
    lua_pi_media_AudioEngine_Init(L);
    lua_pi_media_AudioRenderer_Init(L);
    lua_pi_media_MediaConfig_Init(L);
    lua_pi_media_MediaFrame_Init(L);
    lua_pi_media_apple_MediaFrame_AVFoundation_Init(L);
    lua_pi_wrapper_GameBuffer_Init(L);
    lua_pi_wrapper_GameImpl_Init(L);
    lua_pi_wrapper_QuadRenderer_Init(L);
    lua_pi_wrapper_iOS_PIGameView_Init(L);
    lua_pi_wrapper_macOS_PIGameView_Init(L);
}