/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.8   0.1     Create
 ******************************************************************************/
#include "FBXUtil.h"


static int AnimInterpFbx2PI(FbxAnimCurveDef::EInterpolationType type)
{
    switch (type)
    {
        case FbxAnimCurveDef::eInterpolationCubic:
            return eInterpMode_Cubic;
        case FbxAnimCurveDef::eInterpolationLinear:
            return eInterpMode_Linear;
        case FbxAnimCurveDef::eInterpolationConstant:
        default:
            return eInterpMode_Constant;
    }
}

static int AnimTangentFbx2PI(FbxAnimCurveDef::ETangentMode mode)
{
    switch (mode)
    {
        case FbxAnimCurveDef::eTangentTCB:
            return eTangentMode_TCB;
        case FbxAnimCurveDef::eTangentAuto:
        default:
            return eTangentMode_Auto;
    }
}

static void ParseAnimCurveKeys(iAnimCurveKeyArray* keys, FbxAnimCurve* curve)
{
    auto count = curve->KeyGetCount();
    for (auto i = 0; i < count; ++i)
    {
        SmartPtr<iAnimCurveKey> animKey = CreateAnimCurveKey();
        
        FbxAnimCurveKey key = curve->KeyGet(i);
        
        FbxAnimCurveDef::EInterpolationType intepType = key.GetInterpolation();
        animKey->SetInterpMode(AnimInterpFbx2PI(intepType));
        
        FbxAnimCurveDef::ETangentMode tangentMode = key.GetTangentMode();
        animKey->SetTangentMode(AnimTangentFbx2PI(tangentMode));
        
        auto leftTan = curve->KeyGetLeftDerivative(i);
        animKey->SetLeftTangent(leftTan);
        
        auto rightTan = curve->KeyGetRightDerivative(i);
        animKey->SetRightTangent(rightTan);
        
        auto value = key.GetValue();
        animKey->SetValue(value);
        
        auto time = key.GetTime().GetMilliSeconds();
        animKey->SetTime(time);
        
        keys->PushBack(animKey);
    }
}

struct FbxChannel
{
    const char* field;
    const char* uri;
};

static const char* TranslatePropertyName(const char* name)
{
    if (strcmp(name, "Lcl Translation") == 0)
    {
        return "Translation";
    }
    else if (strcmp(name, "Lcl Rotation") == 0)
    {
        return "Orientation";
    }
    else if (strcmp(name, "Lcl Scaling") == 0)
    {
        return "Scale";
    }
    
    return nullptr;
}

static const char* TranslateChannelName(const char* name)
{
    if (strcmp("X", name) == 0)
    {
        return "x";
    }
    else if (strcmp("Y", name) == 0)
    {
        return "y";
    }
    else if (strcmp("Z", name) == 0)
    {
        return "z";
    }
    else
    {
        return nullptr;
    }
}

static void ParseAnimCurveNode(iAnimChannelArray* channels, FbxAnimLayer* fbxLayer, FbxAnimCurveNode* node)
{
    const auto& fbxProp = node->GetDstProperty(0);
    auto propName = TranslatePropertyName(fbxProp.GetNameAsCStr());
    piCheck(propName != nullptr, ;);
    
    auto sceneNode = static_cast<FbxNode*>(fbxProp.GetFbxObject());
    auto jointName = sceneNode->GetName();
    
    auto channelCount = node->GetChannelsCount();
    for (auto i = 0; i < channelCount; ++i)
    {
        FbxAnimCurve* fbxCurve = node->GetCurve(i);
        if (!fbxCurve)
        {
            continue;
        }
        
        string channelName = TranslateChannelName(node->GetChannelName(i).Buffer());
        if (channelName.empty())
        {
            continue;
        }
        
        SmartPtr<iAnimChannel> channel = CreateAnimChannel();
        
        string uri = piFormat("%s/%s/%s", jointName, propName, channelName.c_str());
        channel->SetUri(uri);
        
        ParseAnimCurveKeys(channel->GetKeys(), fbxCurve);
        
        channels->PushBack(channel);
    }
    
}

static iAnimLayer* ParseAnimLayer(FbxAnimLayer* fbxLayer)
{
    SmartPtr<iAnimLayer> layer = CreateAnimLayer();
    layer->SetName(fbxLayer->GetName());
    SmartPtr<iAnimChannelArray> channels = layer->GetChannels();
    
    auto nodeCount = fbxLayer->GetMemberCount<FbxAnimCurveNode>();
    for (auto j = 0; j < nodeCount; ++j)
    {
        FbxAnimCurveNode* node = fbxLayer->GetMember<FbxAnimCurveNode>(j);
        piAssert(node->GetDstPropertyCount() == 1, nullptr);
        
        ParseAnimCurveNode(channels, fbxLayer, node);
    }
    
    return layer.PtrAndSetNull();
}

static iAnimLib* ParseAnimStack(FbxAnimStack* stack)
{
    SmartPtr<iAnimLib> anim = CreateAnimLib();
    SmartPtr<iAnimLayerArray> layers = anim->GetLayers();
    
    string name = stack->GetName();
    anim->SetName(name);
    
    auto layerCount = stack->GetMemberCount<FbxAnimLayer>();
    for (auto i = 0; i < layerCount; ++i)
    {
        FbxAnimLayer* fbxLayer = stack->GetMember<FbxAnimLayer>(i);
        SmartPtr<iAnimLayer> layer = ParseAnimLayer(fbxLayer);
        if (!layer.IsNull())
        {
            layers->PushBack(layer);
        }
    }
    
    return anim.PtrAndSetNull();
}

iAnimLibArray* ParseAnimations(FbxScene* scene)
{
    SmartPtr<iAnimLibArray> array = CreateAnimLibArray();
    
    auto count = scene->GetSrcObjectCount<FbxAnimStack>();
    for (auto i = 0; i < count; ++i)
    {
        FbxAnimStack* stack = scene->GetSrcObject<FbxAnimStack>(i);
        SmartPtr<iAnimLib> anim = ParseAnimStack(stack);
        if (!anim.IsNull())
        {
            array->PushBack(anim);
        }
    }
    
    return array.PtrAndSetNull();
}












