/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.23   0.1     Create
 ******************************************************************************/
#include "FBXUtil.h"

static iMeshComp* ReadPosition(FbxMesh* mesh,
                               const FbxVector4* points,
                               int32_t vertCount,
                               int32_t triangleCount,
                               const mat4& matrix)
{
    SmartPtr<iMemory> buffer = CreateMemory(sizeof(float) * 3 * vertCount);
    float* mem = (float*)buffer->Ptr();
    
    for (int32_t i = 0; i < triangleCount; ++i)
    {
        for (int32_t j = 0; j < 3; ++j)
        {
            int32_t index = mesh->GetPolygonVertex(i, j);
            int32_t vIndex = i * 3 + j;
            
            float* v = mem + vIndex * 3;
            
            const FbxDouble4& p = points[index];
            vec4 pos = matrix * vec4(p[0], p[1], p[2], 1);
            v[0] = pos.x/pos.w;
            v[1] = pos.y/pos.w;
            v[2] = pos.z/pos.w;
        }
    }
    
    SmartPtr<iMeshComp> comp = CreateMeshComp();
    comp->SetName(eMesh_Position);
    comp->SetData(buffer);
    buffer.PtrAndSetNull();
    return comp.PtrAndSetNull();
}

static iMeshComp* ReadNormal(FbxMesh* mesh,
                             const FbxVector4* points,
                             int32_t vertCount,
                             int32_t triangleCount,
                             const mat4& matrix)
{
    const FbxGeometryElementNormal* normal = mesh->GetElementNormal();
    piCheck(normal != nullptr, nullptr);
    
    SmartPtr<iMemory> buffer = CreateMemory(sizeof(float) * 3 * vertCount);
    float* mem = (float*)buffer->Ptr();
    
    const FbxLayerElementArrayTemplate<FbxVector4>& array = normal->GetDirectArray();
    
    auto mapMode = normal->GetMappingMode();
    auto refMode = normal->GetReferenceMode();
    
    if (refMode == FbxGeometryElement::eDirect)
    {
        for (int32_t i = 0; i < triangleCount; ++i)
        {
            for (int32_t j = 0; j < 3; ++j)
            {
                int32_t vIndex = i * 3 + j;
                float* v = mem + vIndex * 3;
                
                FbxVector4 n;
                if (mapMode == FbxGeometryElement::eByControlPoint)
                {
                    int32_t index = mesh->GetPolygonVertex(i, j);
                    n = array[index];
                }
                else if (mapMode == FbxGeometryElement::eByPolygonVertex)
                {
                    n = array[vIndex];
                }
                
                vec4 normal = matrix * vec4(n[0], n[1], n[2], 1);
                v[0] = normal.x/normal.w;
                v[1] = normal.y/normal.w;
                v[2] = normal.z/normal.w;
            }
        }
    }
    else if (refMode == FbxGeometryElement::eIndexToDirect)
    {
        const FbxLayerElementArrayTemplate<int>& indexArray = normal->GetIndexArray();
        
        for (int32_t i = 0; i < triangleCount; ++i)
        {
            for (int32_t j = 0; j < 3; ++j)
            {
                int32_t vIndex = i * 3 + j;
                
                float* v = mem + vIndex * 3;
                
                FbxVector4 n;
                if (mapMode == FbxGeometryElement::eByControlPoint)
                {
                    int32_t index = indexArray[mesh->GetPolygonVertex(i, j)];
                    n = array[index];
                }
                else if (mapMode == FbxGeometryElement::eByPolygonVertex)
                {
                    int32_t index = indexArray[vIndex];
                    n = array[index];
                }
                
                vec4 normal = matrix * vec4(n[0], n[1], n[2], 1);
                v[0] = normal.x/normal.w;
                v[1] = normal.y/normal.w;
                v[2] = normal.z/normal.w;
            }
        }
    }
    
    SmartPtr<iMeshComp> comp = CreateMeshComp();
    comp->SetName(eMesh_Normal);
    comp->SetData(buffer);
    buffer.PtrAndSetNull();
    return comp.PtrAndSetNull();
}

static iMeshComp* ReadUV(FbxMesh* mesh,
                         const FbxVector4* points,
                         int32_t vertCount,
                         int32_t triangleCount,
                         const mat4& matrix)
{
    const FbxGeometryElementUV* uv = mesh->GetElementUV();
    piCheck(uv != nullptr, nullptr);
    
    SmartPtr<iMemory> buffer = CreateMemory(sizeof(float) * 2 * vertCount);
    float* mem = (float*)buffer->Ptr();
    
    const FbxLayerElementArrayTemplate<FbxVector2>& array = uv->GetDirectArray();
    
    auto mapMode = uv->GetMappingMode();
    auto refMode = uv->GetReferenceMode();
    
    if (refMode == FbxGeometryElement::eDirect)
    {
        for (int32_t i = 0; i < triangleCount; ++i)
        {
            for (int32_t j = 0; j < 3; ++j)
            {
                int32_t vIndex = i * 3 + j;
                float* v = mem + vIndex * 2;
                
                FbxVector2 coord;
                if (mapMode == FbxGeometryElement::eByControlPoint)
                {
                    int32_t index = mesh->GetPolygonVertex(i, j);
                    coord = array[index];
                }
                else if (mapMode == FbxGeometryElement::eByPolygonVertex)
                {
                    coord = array[vIndex];
                }
                
                v[0] = coord[0];
                v[1] = coord[1];
            }
        }
    }
    else if (refMode == FbxGeometryElement::eIndexToDirect)
    {
        const FbxLayerElementArrayTemplate<int>& indexArray = uv->GetIndexArray();
        
        for (int32_t i = 0; i < triangleCount; ++i)
        {
            for (int32_t j = 0; j < 3; ++j)
            {
                int32_t vIndex = i * 3 + j;
                
                float* v = mem + vIndex * 2;
                
                FbxVector2 coord;
                if (mapMode == FbxGeometryElement::eByControlPoint)
                {
                    int32_t index = indexArray[mesh->GetPolygonVertex(i, j)];
                    coord = array[index];
                }
                else if (mapMode == FbxGeometryElement::eByPolygonVertex)
                {
                    int32_t index = indexArray[vIndex];
                    coord = array[index];
                }
                
                v[0] = coord[0];
                v[1] = coord[1];
            }
        }
    }
    
    SmartPtr<iMeshComp> comp = CreateMeshComp();
    comp->SetName(eMesh_UV);
    comp->SetData(buffer);
    buffer.PtrAndSetNull();
    return comp.PtrAndSetNull();
}

static void ReadGeometry(iMeshCompArray* comps,
                         FbxMesh* fbxMesh,
                         const mat4& matrix,
                         int32_t vertCount,
                         int32_t triangleCount)
{
    const FbxVector4* points = fbxMesh->GetControlPoints();
    piCheck(points != nullptr, ;);
    
    SmartPtr<iMeshComp> positions = ReadPosition(fbxMesh, points, vertCount, triangleCount, matrix);
    if (!positions.IsNull())
    {
        comps->PushBack(positions);
    }
    
    SmartPtr<iMeshComp> normals = ReadNormal(fbxMesh, points, vertCount, triangleCount, matrix);
    if (!normals.IsNull())
    {
        comps->PushBack(normals);
    }
    
    SmartPtr<iMeshComp> uvs = ReadUV(fbxMesh, points, vertCount, triangleCount, matrix);
    if (!uvs.IsNull())
    {
        comps->PushBack(uvs);
    }
}


static iSkeletonLib* ReadBindPose(FbxSkin* skin)
{
    FbxSkin::EType type = skin->GetSkinningType();
    if (type != FbxSkin::eRigid && type != FbxSkin::eLinear)
    {
        printf("Unsupported skin type:%d\n", type);
        return nullptr;
    }
    
    int32_t jointCount = skin->GetClusterCount();
    piAssert(jointCount > 0, nullptr);
    
    SmartPtr<iSkeletonLib> pose = CreateSkeletonLib();
    SmartPtr<iSkeletalJointArray> joints = pose->GetJoints();
    
    // Build skeleton hierarchy
    for (int32_t i = 0; i < jointCount; ++i)
    {
        FbxCluster* cluster = skin->GetCluster(i);
        
        FbxCluster::ELinkMode linkMode = cluster->GetLinkMode();
        if (linkMode != FbxCluster::eNormalize)
        {
            printf("Link Mode '%d' of cluster is not supported.\n", linkMode);
            return nullptr;
        }
        
        FbxNode* jointNode = cluster->GetLink();
        string name = jointNode->GetName();
        
        SmartPtr<iSkeletalJoint> joint = CreateSkeletalJoint();
        joint->SetName(name);
        
        if (i == 0)
        {
            pose->SetName(name);
        }
        
        joints->PushBack(joint);
    }
    
    for (int32_t i = 0; i < jointCount; ++i)
    {
        /**
         * In FBXSDK ViewScene sample, joint Transform is in object space.
         *
         * Joint Bind Pose Matrix = GM * TM * TLM_Inverse * JTM * MTM_Inverse
         *
         * GM is geometry transform of the Mesh Node. Geometry transform is '3DS MAX' thing, it's
         * called 'Object Offset' in '3DS MAX'. GM transforms the mesh vertices.
         *
         * TM is cluster transform. Retrieved from FbxCluster::GetTransformMatrix.
         *
         * TLM_Inverse is the inverse of link transform of cluster. TLM_Inverse bring to 'World
         * Space(In Scene)'.
         *
         * JTM is the global tranform of joint in bind pose at current time.
         *
         * MTM_Inverse is the inverse of the transform of the mesh node. MTM_Inverse bring to
         * 'Object Space'.
         *
         * In PI engine, We define 'World Space(In Scene)' as object space. So,
         *
         * Joint Bind Pose Matrix = TM * TLM_Inverse * JTM
         *
         * GM is removed from the equation. It's baked into the mesh vertices.
         * MTM_Inverse is removed from the equation, since our skeleton is in 'World Space(In Scene)'.
         */
        
        FbxCluster* cluster = skin->GetCluster(i);
        
        FbxNode* jointNode = cluster->GetLink();
        string name = jointNode->GetName();
        
        SmartPtr<iSkeletalJoint> joint = joints->GetItem(i);
    
        FbxNode* parentNode = jointNode->GetParent();
        if (parentNode != nullptr)
        {
            string parentName = parentNode->GetName();
            int32_t index = pose->FindJointIndex(parentName);
            joint->SetParent(index);
        }
        
        FbxDouble3 lcl = jointNode->LclRotation;
        
        FbxAMatrix fbxTM;
        cluster->GetTransformMatrix(fbxTM);
        mat4 TM = FbxAMatrix2Mat4(fbxTM);
        
        FbxAMatrix fbxLTM;
        cluster->GetTransformLinkMatrix(fbxLTM);
        mat4 LTM_Inverse = piglm::inverse(FbxAMatrix2Mat4(fbxLTM));
        
        mat4 BPTM = LTM_Inverse * TM;
        joint->SetMatrix(BPTM);
    }
    
    return pose.PtrAndSetNull();
}

static iMeshComp* ReadSkin(FbxSkin* skin)
{
    int32_t jointCount = skin->GetClusterCount();
    int32_t weightCount = 0;
    for (int32_t i = 0; i < jointCount; ++i)
    {
        FbxCluster* cluster = skin->GetCluster(i);
        
        int32_t count = cluster->GetControlPointIndicesCount();
        weightCount += count;
    }
    piAssert(weightCount > 0, nullptr);
    
    SmartPtr<iMemory> mem = CreateMemory(weightCount * sizeof(VertexWeight));
    VertexWeight* w = (VertexWeight*)mem->Ptr();
    
    for (int32_t i = 0; i < jointCount; ++i)
    {
        FbxCluster* cluster = skin->GetCluster(i);
        
        string name = cluster->GetLink()->GetName();
        
        int* indices = cluster->GetControlPointIndices();
        double* weights = cluster->GetControlPointWeights();
        
        int32_t count = cluster->GetControlPointIndicesCount();

        for (int32_t j = 0; j < count; ++j)
        {
            w->jointIndex = i;
            w->vertexIndex = indices[j];
            w->bias = weights[j];
            
            ++w;
        }
    }

    SmartPtr<iMeshComp> comp = CreateMeshComp();
    comp->SetName(eMesh_Weight);
    comp->SetData(mem);
    mem.PtrAndSetNull();
    return comp.PtrAndSetNull();
}

static iMeshLib* ParseMesh(FbxMesh* fbxMesh, iSkeletonLibArray* skels)
{
    // Read data
    int32_t triangleCount = fbxMesh->GetPolygonCount();
    piCheck(triangleCount > 0, nullptr);
    
    int32_t vertCount = triangleCount * 3;
    
    SmartPtr<iMeshLib> mesh = CreateMeshLib();
    
    // name
    string name = fbxMesh->GetNode()->GetName();
    mesh->SetName(name);
    
    SmartPtr<iMeshCompArray> comps = mesh->GetComps();

    // Geometry
    {
        mesh->SetVertexCount(vertCount);
        
        mat4 matrix;

        FbxNode* node = fbxMesh->GetNode();

        mat4 GM = FbxMatrix2Mat4(GetGeometryTransform(fbxMesh->GetNode()));
        matrix *= GM;
        
        mat4 GTM = FbxMatrix2Mat4(node->EvaluateGlobalTransform());
        matrix *= GTM;
        
        ReadGeometry(comps, fbxMesh, matrix, vertCount, triangleCount);
    }
    
    // Skin
    int32_t skinCount = fbxMesh->GetDeformerCount(FbxDeformer::eSkin);
    if (skinCount > 0)
    {
        FbxSkin* skin = (FbxSkin*)fbxMesh->GetDeformer(0, FbxDeformer::eSkin);
        FbxSkin::EType type = skin->GetSkinningType();
        if (type != FbxSkin::eRigid && type != FbxSkin::eLinear)
        {
            printf("Unsupported skin type:%d\n", type);
            return mesh.PtrAndSetNull();
        }
        
        // Read bind pose
        SmartPtr<iSkeletonLib> bindPose = ReadBindPose(skin);
        piCheck(!bindPose.IsNull(), mesh.PtrAndSetNull());
        skels->PushBack(bindPose);
        
        // Read weights
        SmartPtr<iMeshComp> weights = ReadSkin(skin);
        if (!weights.IsNull())
        {
            comps->PushBack(weights);
        }
    }
    else if (skinCount > 1)
    {
        printf("WARNING:Mesh influenced by multi-joints is not supported.\n");
    }
    
    return mesh.PtrAndSetNull();
}

iMeshLibArray* ParseMeshes(FbxScene* scene, iSkeletonLibArray* skels)
{
    SmartPtr<iMeshLibArray> meshes = CreateMeshLibArray();
    
    auto count = scene->GetGeometryCount();
    for (auto i = 0; i < count; ++i)
    {
        FbxMesh* fbxMesh = FbxCast<FbxMesh>(scene->GetGeometry(i));
        if (!fbxMesh)
        {
            continue;
        }
        
        SmartPtr<iMeshLib> mesh = ParseMesh(fbxMesh, skels);
        if (!mesh.IsNull())
        {
            meshes->PushBack(mesh);
        }
    }
    
    return meshes.PtrAndSetNull();
}





































