/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.12   0.1     Create
 ******************************************************************************/
#include "JsonUtil.h"
#include "FBXUtil.h"

static json AnimKey2Json(iAnimCurveKey* key)
{
    return Object2Json(key);
}

static json AnimChannel2Json(iAnimChannel* ch)
{
    SmartPtr<iClass> klass = ch->GetClass();
    
    json v;
    v["Type"] = klass->GetFullName();
    v["Uri"] = ch->GetUri();
    
    json jsonKeys = json::array();
    
    SmartPtr<iAnimCurveKeyArray> keys = ch->GetKeys();
    for (auto i = 0; i < keys->GetCount(); ++i)
    {
        SmartPtr<iAnimCurveKey> key = keys->GetItem(i);
        json jsonKey = AnimKey2Json(key);
        jsonKeys.push_back(jsonKey);
    }
    
    v["Keys"] = jsonKeys;
    
    return v;
}

static json AnimLayer2Json(iAnimLayer* layer)
{
    SmartPtr<iClass> klass = layer->GetClass();
    
    json v;
    v["Type"] = klass->GetFullName();
    v["Name"] = layer->GetName();
    
    json jsonChannels = json::array();
    
    SmartPtr<iAnimChannelArray> channels = layer->GetChannels();
    for (auto i = 0; i < channels->GetCount(); ++i)
    {
        SmartPtr<iAnimChannel> ch = channels->GetItem(i);
        json jsonCh = AnimChannel2Json(ch);
        jsonChannels.push_back(jsonCh);
    }
    
    v["Channels"] = jsonChannels;
    
    return v;
}

static bool WriteAnim(iAnimLib* anim, iStream* o)
{
    SmartPtr<iClass> klass = anim->GetClass();
    
    json v;
    
    v["Type"] = klass->GetFullName();
    v["Name"] = anim->GetName();
    
    json jsonLayers = json::array();
    SmartPtr<iAnimLayerArray> layers = anim->GetLayers();
    for (auto i = 0; i < layers->GetCount(); ++i)
    {
        SmartPtr<iAnimLayer> layer = layers->GetItem(i);
        jsonLayers.push_back(AnimLayer2Json(layer));
    }
    
    v["Layers"] = jsonLayers;
    
    return WriteJson(v, o);
}

void WriteAnimations(iAnimLibArray* anims, const string& dir)
{
    for (auto i = 0; i < anims->GetCount(); ++i)
    {
        SmartPtr<iAnimLib> anim = anims->GetItem(i);
        
        string uri = dir + piFormat("/%s.anim", anim->GetName().c_str());
        uri = NormalizeUri(uri);
        SmartPtr<iStream> output = CreateFileStream(uri, "w");
        if (output.IsNull())
        {
            printf("Unable to create file '%s'.\n", uri.c_str());
            continue;
        }
        
        if (WriteAnim(anim, output))
        {
            printf("Write animation to '%s'.\n", uri.c_str());
        }
    }
}




















