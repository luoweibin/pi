/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.12.11   1     Create
 ******************************************************************************/
#include "FBXUtil.h"
#include <regex>
#include "JsonUtil.h"

mat4 FbxAMatrix2Mat4(const FbxAMatrix& m)
{
    return mat4(m[0][0],  m[0][1],  m[0][2],  m[0][3],
                m[1][0],  m[1][1],  m[1][2],  m[1][3],
                m[2][0],  m[2][1],  m[2][2],  m[2][3],
                m[3][0],  m[3][1],  m[3][2],  m[3][3]);
}

mat4 FbxMatrix2Mat4(const FbxMatrix& m)
{
    return mat4(m[0][0],  m[1][0],  m[2][0],  m[3][0],
                m[0][1],  m[1][1],  m[2][1],  m[3][1],
                m[0][2],  m[1][2],  m[2][2],  m[3][2],
                m[0][3],  m[1][3],  m[2][3],  m[3][3]);
}

vec3 FbxVector32Vec3(const FbxDouble3& v)
{
    return vec3(v.mData[0], v.mData[1], v.mData[2]);
}

FbxAMatrix GetGeometryTransform(const FbxNode* node)
{
    auto lT = node->GetGeometricTranslation(FbxNode::eSourcePivot);
    auto lR = node->GetGeometricRotation(FbxNode::eSourcePivot);
    auto lS = node->GetGeometricScaling(FbxNode::eSourcePivot);
    
    return FbxAMatrix(lT, lR, lS);
}


string NormalizeUri(const string& uri)
{
    return regex_replace(uri, regex("\\s+"), "_");
}


















