/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.12   0.1     Create
 ******************************************************************************/
#include "FBXUtil.h"
#include "JsonUtil.h"

static json Joint2Json(iSkeletalJoint* joint)
{
    json v;
    v["Name"] = joint->GetName();
    v["Parent"] = joint->GetParent();
    v["Offset"] = Var2Json(joint->GetOffset());
    
    if (joint->IsMatrix())
    {
        v["M"] = Var2Json(joint->GetMatrix());
    }
    else
    {
        v["T"] = Var2Json(joint->GetTranslation());
        v["S"] = Var2Json(joint->GetScale());
        v["O"] = Var2Json(joint->GetOrientation());
    }
    
    return v;
}

static bool WriteSkeleton(iSkeletonLib* skeleton, iStream* o)
{
    SmartPtr<iClass> klass = skeleton->GetClass();
    
    json v;
    v["Name"] = skeleton->GetName();
    
    SmartPtr<iSkeletalJointArray> sklJoints = skeleton->GetJoints();
    json joints = json::array();
    
    for (auto i = 0; i <sklJoints->GetCount(); ++i)
    {
        SmartPtr<iSkeletalJoint> j = sklJoints->GetItem(i);
        
        json joint = Joint2Json(j);
        joints.push_back(joint);
    }
    
    v["Joints"] = joints;
    
    return WriteJson(v, o);
}

void WriteSkeletons(iSkeletonLibArray* skeletons, const string& dir)
{
    for (auto i = 0; i < skeletons->GetCount(); ++i)
    {
        SmartPtr<iSkeletonLib> skel = skeletons->GetItem(i);
        
        string uri = dir + piFormat("/%s.skl", skel->GetName().c_str());
        uri = NormalizeUri(uri);
        SmartPtr<iStream> output = CreateFileStream(uri, "w");
        if (output.IsNull())
        {
            printf("Unable to create file '%s'.\n", uri.c_str());
            continue;
        }
        
        if (WriteSkeleton(skel, output))
        {
            printf("Write skeleton to '%s'.\n", uri.c_str());
        }
    }
}




























