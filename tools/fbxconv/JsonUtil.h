/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.11   0.1     Create
 ******************************************************************************/
#ifndef PI_TOOLS_FBXCONV_JSONUTIL_H
#define PI_TOOLS_FBXCONV_JSONUTIL_H

#include <pi/Game.h>
#include <json.h>

using namespace std;
using namespace nspi;
using namespace nlohmann;

json Var2Json(const Var& v);
json Object2Json(iRefObject* o);
bool WriteJson(const json& v, iStream* o);

#endif
