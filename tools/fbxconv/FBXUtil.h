/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2016.11.21   0.1     Create
 ******************************************************************************/
#ifndef PI_TOOLS_FBXCONV_FBXUTIL_H
#define PI_TOOLS_FBXCONV_FBXUTIL_H

#include <pi/Game.h>
#include <fbxsdk.h>

using namespace std;
using namespace nspi;

mat4 FbxAMatrix2Mat4(const FbxAMatrix& m);
mat4 FbxMatrix2Mat4(const FbxMatrix& m);
vec3 FbxVector32Vec3(const FbxDouble3& v);

FbxAMatrix GetGeometryTransform(const FbxNode* node);

string NormalizeUri(const string& uri);

#endif

























