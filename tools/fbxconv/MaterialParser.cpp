/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.10   0.1     Create
 ******************************************************************************/
#include "FBXUtil.h"

static int FbxWrapMode2WrapMode(FbxTexture::EWrapMode mode)
{
    switch (mode)
    {
        case FbxTexture::eRepeat:
            return eWrapMode_Repeat;
        default:
            return eWrapMode_Clamp;
    }
}

static iTextureLib* ParseTexture(FbxFileTexture* fbxTex)
{
    SmartPtr<iTextureLib> tex = CreateTextureLib();
    string fileName = piGetBaseName(fbxTex->GetRelativeFileName());
    string fileExt = piGetExtendName(fileName);
    fileName = fileName.substr(0, fileName.size() - fileExt.size() - 1);
    fileName += ".tex";
    tex->SetImage(fileName);

    FbxTexture::EWrapMode fbxWrapMode = fbxTex->GetWrapModeU();
    tex->SetWrapMode(FbxWrapMode2WrapMode(fbxWrapMode));
    
    return tex.PtrAndSetNull();
}

iMaterial3DLibArray* ParseMaterials(FbxScene* scene)
{
    SmartPtr<iMaterial3DLibArray> materials = CreateMaterial3DLibArray();
    
    auto count = scene->GetGeometryCount();
    for (auto i = 0; i < count; ++i)
    {
        FbxNode* fbxNode = scene->GetGeometry(i)->GetNode();
        if (fbxNode->GetMaterialCount() <= 0)
        {
            continue;
        }
        
        FbxSurfaceMaterial* fbxMtl = fbxNode->GetMaterial(0);
        const FbxProperty& prop = fbxMtl->FindProperty(FbxSurfaceMaterial::sDiffuse);
        if (prop.GetSrcObjectCount<FbxFileTexture>() <= 0)
        {
            continue;
        }
        
        SmartPtr<iMaterial3DLib> mtl = CreateMaterial3DLib();
        
        string name = fbxMtl->GetName();
        if (name.empty())
        {
            name = piFormat("unanmed_%d", i);
        }
        mtl->SetName(name);
        
        FbxFileTexture* fbxTex = prop.GetSrcObject<FbxFileTexture>(0);
        SmartPtr<iTextureLib> tex = ParseTexture(fbxTex);
        if (!tex.IsNull())
        {
            mtl->SetAlbedo(tex->GetImage());
        }
        
        materials->PushBack(mtl);
    }
    
    return materials.PtrAndSetNull();
}





















