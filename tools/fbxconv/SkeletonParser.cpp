/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.23   0.1     Create
 ******************************************************************************/
#include "FBXUtil.h"

static bool ParseJoint(iSkeletalJointArray* joints, int32_t parent, FbxNode* node)
{
    bool hasJoints = false;
    
    int32_t parentIndex = parent;
    
    FbxNodeAttribute* attr = node->GetNodeAttribute();
    if (attr && attr->GetAttributeType() == FbxNodeAttribute::eSkeleton)
    {
        parentIndex = joints->GetCount();

        SmartPtr<iSkeletalJoint> joint = CreateSkeletalJoint();
        joint->SetName(node->GetName());
        joint->SetParent(parent);
        
        joint->SetTranslation(FbxVector32Vec3(node->LclTranslation.Get()));
        joint->SetScale(FbxVector32Vec3(node->LclScaling.Get()));
        
        node->SetRotationOrder(FbxNode::eDestinationPivot, eEulerXYZ);
        joint->SetOrientation(FbxVector32Vec3(node->LclRotation.Get()));
        
        FbxDouble4 scalePivot = node->GetScalingPivot(FbxNode::eDestinationPivot);
        
        joints->PushBack(joint);
        
        hasJoints = true;
    }
    
    auto childCount = node->GetChildCount();
    for (auto i = 0; i < childCount; ++i)
    {
        FbxNode* child = node->GetChild(i);
        if (ParseJoint(joints, parentIndex, child))
        {
            hasJoints = true;
        }
    }

    return hasJoints;
}

static void TravelNode(FbxNode* node, iSkeletonLibArray* skeletons)
{
    FbxNodeAttribute* attr = node->GetNodeAttribute();
    if (attr && attr->GetAttributeType() == FbxNodeAttribute::eSkeleton)
    {
        string name = node->GetName();
        FbxSkeleton* fbxSkel = FbxCast<FbxSkeleton>(attr);
        if (fbxSkel->IsSkeletonRoot())
        {
            SmartPtr<iSkeletonLib> skeleton = CreateSkeletonLib();
            SmartPtr<iSkeletalJointArray> joints = skeleton->GetJoints();
            skeleton->SetName(node->GetName());
            ParseJoint(joints, -1, node);
            skeletons->PushBack(skeleton);
        }
    }
    else
    {
        auto childCount = node->GetChildCount();
        for (auto i = 0; i < childCount; ++i)
        {
            FbxNode* child = node->GetChild(i);
            TravelNode(child, skeletons);
        }
    }
}

iSkeletonLibArray* ParseSkeletons(FbxScene* scene)
{
    SmartPtr<iSkeletonLibArray> skeletons = CreateSkeletonLibArray();
    FbxNode* root = scene->GetRootNode();
    TravelNode(root, skeletons);
    return skeletons.PtrAndSetNull();
}





























