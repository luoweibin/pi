/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.23   0.1     Create
 ******************************************************************************/
#include <pi/Game.h>
#include <fbxsdk.h>

using namespace nspi;
using namespace std;

iMeshLibArray* ParseMeshes(FbxScene* scene, iSkeletonLibArray* skels);
iSkeletonLibArray* ParseSkeletons(FbxScene* scene);
iAnimLibArray* ParseAnimations(FbxScene* scene);
iMaterial3DLibArray* ParseMaterials(FbxScene* scene);
void ParseModel(iModel3DLib* model, FbxScene* scene);
void WriteMeshes(iMeshLibArray* meshes, const string& dir);
void WriteMaterials(iMaterial3DLibArray* materials, const string& dir);
void WriteSkeletons(iSkeletonLibArray* skeletons, const string& dir);
void WriteAnimations(iAnimLibArray* anims, const string& dir);
void WriteModel(iModel3DLib* model, const string& dir);

static void PrintUsage()
{
    printf("fbxconv -i /path/to/file.fbx -o /path/to/output/dir\n -u [Y|Z]");
}


static void NormalizeNode(FbxNode* node)
{
    FbxVector4 zero(0,0,0);
    
    // Activate pivot converting
    node->SetPivotState(FbxNode::eSourcePivot, FbxNode::ePivotActive);
    node->SetPivotState(FbxNode::eDestinationPivot, FbxNode::ePivotActive);
    
    // We want to set all these to 0 and bake them into the transforms.
    node->SetPostRotation(FbxNode::eDestinationPivot, zero);
    node->SetPreRotation(FbxNode::eDestinationPivot, zero);
    node->SetRotationOffset(FbxNode::eDestinationPivot, zero);
    node->SetScalingOffset(FbxNode::eDestinationPivot, zero);
    node->SetRotationPivot(FbxNode::eDestinationPivot, zero);
    node->SetScalingPivot(FbxNode::eDestinationPivot, zero);
    
    node->SetRotationOrder(FbxNode::eDestinationPivot, FbxEuler::eOrderXYZ);
    
    node->SetGeometricTranslation(FbxNode::eDestinationPivot, zero);
    node->SetGeometricRotation(FbxNode::eDestinationPivot, zero);
    node->SetGeometricScaling(FbxNode::eDestinationPivot, zero);
    
    for (auto i = 0; i < node->GetChildCount(); ++i)
    {
        NormalizeNode(node->GetChild(i));
    }
}

// bake transform components: pre- and post-rotation, rotation and scale pivots and offsets - inside the standard transforms - Translation, Rotation, Scaling. 
static void NormalizeScene(FbxScene* scene)
{
    FbxNode* root = scene->GetRootNode();
    NormalizeNode(root);
    root->ConvertPivotAnimationRecursive(scene->GetCurrentAnimationStack(), FbxNode::eDestinationPivot, 24);
}

int main(int argc, char * const argv[])
{
    string input;
    string dir;
    int up = 0;
    
    int c = 0;
    while ((c = getopt(argc, argv, "i:o:u:")) != -1)
    {
        switch (c)
        {
            case 'i':
                input = optarg;
                break;
            case 'o':
                dir = optarg;
                break;
            case 'u':
                up = *optarg;
                break;
            default:
                break;
        }
    }
    
    bool err = false;
    
    if (input.empty())
    {
        printf("input file not set.\n");
        err = true;
    }
    
    if (dir.empty())
    {
        printf("output file not set.\n");
        err = true;
    }
    dir = piTrimRight(dir, "/");
    
    if (err)
    {
        PrintUsage();
        return -1;
    }
    
    FbxManager* fbxMgr = FbxManager::Create();
    piAssert(fbxMgr != nullptr, -1);
    
    FbxIOSettings* ios = FbxIOSettings::Create(fbxMgr, IOSROOT);
    piAssert(ios != nullptr, -1);
    fbxMgr->SetIOSettings(ios);
    
    FbxImporter* importer = FbxImporter::Create(fbxMgr, "");
    piCheck(importer->Initialize(input.c_str(), -1, fbxMgr->GetIOSettings()), -1);
    
    FbxScene* scene = FbxScene::Create(fbxMgr, "fbx");
    if (scene == nullptr)
    {
        importer->Destroy();
        return -1;
    }
    
    if (!importer->Import(scene))
    {
        importer->Destroy();
        fbxMgr->Destroy();
        return -1;
    }
    
    NormalizeScene(scene);

    switch (up)
    {
        case 'Z':
            FbxAxisSystem::MayaZUp.ConvertScene(scene);
            break;
        case 'Y':
            FbxAxisSystem::MayaYUp.ConvertScene(scene);
            break;
        default:
            break;
    }
    
    if (!piDirectoryExist(dir) && CreateDirectory(dir, true))
    {
        printf("Unable to create directory '%s'.\n", dir.c_str());
        return -1;
    }
    
    SmartPtr<iSkeletonLibArray> skeletons = CreateSkeletonLibArray();
//    SmartPtr<iAssetSkelArray> skeletons = ParseSkeletons(scene);
    SmartPtr<iMeshLibArray> meshes = ParseMeshes(scene, skeletons);
    SmartPtr<iMaterial3DLibArray> materials = ParseMaterials(scene);
    SmartPtr<iAnimLibArray> anims = ParseAnimations(scene);
    
    SmartPtr<iModel3DLib> model = CreateModel3DLib();
    {
        string name = piGetBaseName(input);
        string ext = piGetExtendName(name);
        name = name.substr(0, name.size() - ext.size() - 1);
        model->SetName(name);
    }
    ParseModel(model, scene);
    
    importer->Destroy();
    fbxMgr->Destroy();

    WriteMeshes(meshes, dir);
    WriteMaterials(materials, dir);
    WriteSkeletons(skeletons, dir);
    WriteAnimations(anims, dir);
    WriteModel(model, dir);
    
    
    return 0;
}


























