/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.8   0.1     Create
 ******************************************************************************/
#include "FBXUtil.h"


static int MakeType(int32_t count)
{
    if (count <= std::numeric_limits<uint8_t>::max())
    {
        return eType_U8;
    }
    else if (count <= std::numeric_limits<uint16_t>::max())
    {
        return eType_U16;
    }
    else
    {
        return eType_U32;
    }
}

static bool WriteMesh(iMeshLib* mesh, iStream* o)
{
    SmartPtr<iMeshCompArray> comps = mesh->GetComps();
    
    MeshFile meshFile;
    string name = mesh->GetName();
    strncpy(meshFile.name, name.c_str(), piMin(PI_MODEL_NAMESIZE, name.size()));
    
    meshFile.vertexCount = mesh->GetVertexCount();
    meshFile.attrCount = comps->GetCount();
    meshFile.attrStart = sizeof(meshFile);

    o->Write(&meshFile, sizeof(meshFile));
    
    uint32_t start = sizeof(MeshAttr) * meshFile.attrCount;

    for (auto i = 0; i < comps->GetCount(); ++i)
    {
        SmartPtr<iMeshComp> comp = comps->GetItem(i);
        SmartPtr<iMemory> data = comp->GetData();
        
        MeshAttr attr;
        attr.start = start;
        attr.size = (int32_t)data->Size();
        attr.type = comp->GetName();
        o->Write(&attr, sizeof(attr));
        
        uint32_t alignSize = piAlignPow2(attr.size, 2);
        start += alignSize;
    }
    
    for (auto i = 0; i < comps->GetCount(); ++i)
    {
        SmartPtr<iMeshComp> comp = comps->GetItem(i);
        SmartPtr<iMemory> data = comp->GetData();
        
        int32_t size = (int32_t)data->Size();
        int32_t alignSize = piAlignPow2(size, 2);
        
        o->Write(data->Ptr(), data->Size());
        
        int32_t b = 0;
        o->Write(&b, alignSize - size);
    }
    
    return true;
}

void WriteMeshes(iMeshLibArray* meshes, const string& dir)
{
    for (int32_t i = 0; i < meshes->GetCount(); ++i)
    {
        SmartPtr<iMeshLib> mesh = meshes->GetItem(i);
        
        string name = mesh->GetName();
        if (name.empty())
        {
            name = piFormat("unanmed_%d", i);
        }
        
        string path = dir + piFormat("/%s.msh", name.c_str());
        path = NormalizeUri(path);
        SmartPtr<iStream> output = CreateFileStream(path, "w");
        if (output.IsNull())
        {
            printf("Unable to create file '%s'.\n", path.c_str());
            continue;
        }
        
        if (WriteMesh(mesh, output))
        {
            printf("Write mesh to '%s'.\n", path.c_str());
        }
    }
}









