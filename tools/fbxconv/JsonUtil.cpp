/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.11   0.1     Create
 ******************************************************************************/
#include "JsonUtil.h"

json Object2Json(iRefObject* o)
{
    SmartPtr<iClass> klass = o->GetClass();
    string className = klass->GetFullName();
    
    json v = { {"Type", className} };
    
    SmartPtr<iPropertyArray> props = klass->GetProperties();
    for (auto i = 0; i < props->GetCount(); ++i)
    {
        SmartPtr<iProperty> prop = props->GetItem(i);
        
        string name = prop->GetName();
        v[name] = Var2Json(prop->GetValue(o));
    }
    
    return v;
}

static json Vec2ToJson(const vec2& v)
{
    return json({v.x, v.y});
}

static json Vec3ToJson(const vec3& v)
{
    return json({v.x, v.y, v.z});
}

static json Vec4ToJson(const vec4& v)
{
    return json({v.x, v.y, v.z, v.w});
}

static json RectToJson(const rect& v)
{
    return json({v.x, v.y, v.width, v.height});
}

static json QuatToJson(const quat& v)
{
    return json({v.x, v.y, v.z, v.w});
}

static json AABBToJson(const aabb& v)
{
    return json({v.a.x, v.a.y, v.a.z, v.b.x, v.b.y, v.b.z});
}

static json Mat4ToJson(const mat4& v)
{
    return json({
        v[0][0],  v[0][1],  v[0][2],  v[0][3],
        v[1][0],  v[1][1],  v[1][2],  v[1][3],
        v[2][0],  v[2][1],  v[2][2],  v[2][3],
        v[3][0],  v[3][1],  v[3][2],  v[3][3]
    });
}

json Var2Json(const Var& v)
{
    switch (v.GetType())
    {
        case eType_Boolean:
            return v.GetBoolean();
        case eType_I8:
        case eType_I16:
        case eType_I32:
        case eType_Enum:
            return v.GetI32();
        case eType_U8:
        case eType_U16:
        case eType_U32:
            return v.GetU32();
        case eType_I64:
            return v.GetI64();
        case eType_U64:
            return v.GetU64();
        case eType_F32:
            return v.GetF32();
        case eType_F64:
            return v.GetF64();
        case eType_String:
            return v.GetString();
        case eType_Vec2:
            return Vec2ToJson(v);
        case eType_Vec3:
            return Vec3ToJson(v);
        case eType_Vec4:
            return Vec4ToJson(v);
        case eType_Rect:
            return RectToJson(v);
        case eType_Quat:
            return QuatToJson(v);
        case eType_AABB:
            return AABBToJson(v);
        case eType_Mat4:
            return Mat4ToJson(v);
        case eType_Object:
            return Object2Json(v.GetObject());
        default:
            return nullptr;
    }
}


bool WriteJson(const json& v, iStream* o)
{
    stringstream ss;
    ss << setprecision(8) << std::setw(2) << fixed << v;
    
    string buffer = ss.str();
    return o->WriteString(buffer) == buffer.size();
}
























