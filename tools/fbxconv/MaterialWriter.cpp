/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.10   0.1     Create
 ******************************************************************************/
#include "JsonUtil.h"
#include "FBXUtil.h"

void WriteMaterials(iMaterial3DLibArray* materials, const string& dir)
{
    for (auto i = 0; i < materials->GetCount(); ++i)
    {
        SmartPtr<iMaterial3DLib> mtl = materials->GetItem(i);
        
        string name = mtl->GetName();
        
        string uri = dir + piFormat("/%s.mtl", name.c_str());
        uri = NormalizeUri(uri);
        
        SmartPtr<iStream> output = CreateFileStream(uri, "w");
        if (output.IsNull())
        {
            printf("Unable to create file '%s'.\n", uri.c_str());
            continue;
        }
        
        json v = Object2Json(mtl);
        if (WriteJson(v, output))
        {
            printf("Write material to '%s'.\n", uri.c_str());
        }
    }
}














