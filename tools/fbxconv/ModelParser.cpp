/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.13   0.1     Create
 ******************************************************************************/
#include "FBXUtil.h"

void ParseModel(iModel3DLib* model, FbxScene* scene)
{
    SmartPtr<iModelFragArray> frags = model->GetFrags();
    
    auto count = scene->GetGeometryCount();
    for (auto i = 0; i < count; ++i)
    {
        FbxMesh* fbxMesh = FbxCast<FbxMesh>(scene->GetGeometry(i));
        if (!fbxMesh)
        {
            continue;
        }

        SmartPtr<iModelFrag> frag = CreateModelFrag();
        FbxNode* meshNode = fbxMesh->GetNode();
   
        // mesh
        string meshName = meshNode->GetName();
        string meshUri = NormalizeUri(piFormat("%s.msh", meshName.c_str()));
        frag->SetMeshUri(meshUri);
        
        // skeleton
        bool hasSkel = fbxMesh->GetDeformerCount(FbxDeformer::eSkin) == 1;
        if (hasSkel)
        {
            FbxSkin* skin = (FbxSkin*)fbxMesh->GetDeformer(0, FbxDeformer::eSkin);
            FbxSkin::EType type = skin->GetSkinningType();
            if ((type == FbxSkin::eRigid || type == FbxSkin::eLinear) && skin->GetClusterCount() > 0)
            {
                FbxCluster* cluster = skin->GetCluster(0);
                string name = cluster->GetLink()->GetName();
                string skelUri = NormalizeUri(piFormat("%s.skl", name.c_str()));
                frag->SetSkeletonUri(skelUri);
            }
        }
        
        // material
        int32_t mtlCount = meshNode->GetMaterialCount();
        if (mtlCount > 1)
        {
            printf("WARNING:Mesh with multi-materials is not supported. The first material is applyed.\n");
        }
        
        if (mtlCount >= 1)
        {
            FbxSurfaceMaterial* fbxMtl = meshNode->GetMaterial(0);
            string name = fbxMtl->GetName();
            if (name.empty())
            {
                name = piFormat("unanmed_%d", i);
            }
            string mtlUri = NormalizeUri(piFormat("%s.mtl", name.c_str()));
            frag->SetMaterialUri(mtlUri);
        }
        
        // animation
        FbxAnimStack* animStack = scene->GetCurrentAnimationStack();
        if (animStack != nullptr && hasSkel)
        {
            string name = animStack->GetName();
            string animUri = NormalizeUri(piFormat("%s.anim", name.c_str()));
            frag->SetAnimationUri(animUri);
        }
        
        frags->PushBack(frag);
    }
}


















