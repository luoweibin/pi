/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.13   0.1     Create
 ******************************************************************************/
#include "FBXUtil.h"
#include "JsonUtil.h"


void WriteModel(iModel3DLib* model, const string& dir)
{
    string uri = dir + piFormat("/%s.mdl", model->GetName().c_str());
    uri = NormalizeUri(uri);
    SmartPtr<iStream> output = CreateFileStream(uri, "w");
    if (output.IsNull())
    {
        printf("Unable to create file '%s'.\n", uri.c_str());
        return;
    }
    
    SmartPtr<iClass> klass = model->GetClass();
    
    json v;
    v["Type"] = klass->GetFullName();

    json frags = json::array();
    SmartPtr<iModelFragArray> modelFrags = model->GetFrags();
    for (auto i = 0; i < modelFrags->GetCount(); ++i)
    {
        SmartPtr<iModelFrag> frag = modelFrags->GetItem(i);
        frags.push_back(Object2Json(frag));
    }
    
    v["Frags"] = frags;
    
    if (WriteJson(v, output))
    {
        printf("Write model to '%s'.\n", uri.c_str());
    }
}

















