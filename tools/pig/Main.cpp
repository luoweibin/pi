/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.4.18   0.1     Create
 ******************************************************************************/
#include <pi/Crypto.h>

using namespace nspi;
using namespace std;

static const char* key = "How are you. Fine, think you. How old are you.";

static void PrintHelp()
{
    printf("Usage:pig -i /path/to/input/file -o /path/to/output/file\n");
}

static iMemory* BuildKeyMemory()
{
    SmartPtr<iHash> hash = CreateHash(eHash_MD5);
    hash->Update(key, strlen(key));
    
    SmartPtr<iMemory> mem = CreateMemory(32);
    hash->Final(mem->Ptr(), mem->Size());
    
    return mem.PtrAndSetNull();
}

int main(int argc, char * const argv[])
{
    piInit();
    
    if (argc < 3)
    {
        PrintHelp();
        return 1;
    }
    
    
    string inFile;
    string outFile;
    
    int c = 0;
    while ((c = getopt(argc, argv, "i:o:")) != -1)
    {
        switch (c)
        {
            case 'i':
                inFile = optarg;
                break;
            case 'o':
                outFile = optarg;
                break;
            default:
                break;
        }
    }
    
    SmartPtr<iStream> inStream = CreateFileStream(inFile, "r");
    if (inStream.IsNull())
    {
        printf("Input file not found. Path:%s\n", inFile.c_str());
        return -1;
    }
    
    SmartPtr<iStream> outStream = CreateFileStream(outFile, "w+");
    if (outStream.IsNull())
    {
        printf("Output file not found. Path:%s\n", outFile.c_str());
        return -1;
    }

    SmartPtr<iMemory> keyMem = BuildKeyMemory();
    SmartPtr<iStream> encoder = CreateCipherStream(outStream, eEncoder_Blowfish, keyMem, nullptr);
    
    char b[512];
    int64_t l = 0;
    int64_t total = 0;
    while ((l = inStream->Read(b, 512)) > 0)
    {
        encoder->Write(b, l);
        total += l;
    }
    
    return 0;
}


























