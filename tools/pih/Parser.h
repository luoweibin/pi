/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.5   0.1     Create
 ******************************************************************************/
#ifndef PI_TOOLS_PIH_PARSER_H
#define PI_TOOLS_PIH_PARSER_H

#include "Lexer.h"

using namespace std;

namespace nspi
{
    typedef map<string, Token> ParserConfig;
    
    struct Module;
    
    struct Meta
    {
        typedef map<int, int> FieldMap;
        FieldMap fieldMap;
    };
    
    enum
    {
        eTypeFlag_Object = piBit(0),
        eTypeFlag_Struct = piBit(1),
        eTypeFlag_Const  = piBit(2),
    };
    
    struct Type
    {
        int32_t flags;
        int name;
        char literal[LITERAL_MAX + 1];
        
        Type():
        name(eType_Void), flags(0)
        {
            memset(literal, 0, sizeof(literal));
        }
    };
    
    struct Param
    {
        Type type;
        char name[LITERAL_MAX + 1];
        char defaultValue[LITERAL_MAX + 1];
        
        ParserConfig config;
        
        Param()
        {
            memset(name, 0, sizeof(name));
            memset(defaultValue, 0, sizeof(defaultValue));
        }
        
        Param(const Param& o):
        type(o.type), config(o.config)
        {
            strcpy(name, o.name);
            strcpy(defaultValue, o.defaultValue);
        }
    };
    
    enum
    {
        eProperty_Getter = piBit(0),
        eProperty_Setter = piBit(1),
    };
    
    struct Property
    {
        Type type;
        char name[LITERAL_MAX + 1];
        int32_t flags;
        
        ParserConfig config;
        
        Property():
        flags(0)
        {
            memset(name, 0, sizeof(name));
        }
        
        Property(const Property& o):
        type(o.type), flags(o.flags), config(o.config)
        {
            strcpy(name, o.name);
        }
    };
    
    enum
    {
        eFunction_Const = piBit(0),
    };
    
    struct Function
    {
        Module& module;
        
        ParserConfig config;
        
        int32_t flags;
        Type returnType;
        char name[LITERAL_MAX + 1];
        std::vector<Param> params;
        
        Function(Module& m):
        flags(0), module(m)
        {
            memset(name, 0, sizeof(name));
        }
        
        Function(const Function& a):
        module(a.module),
        config(a.config),
        flags(a.flags),
        returnType(a.returnType),
        params(a.params)
        {
            strcpy(name, a.name);
        }
    };
    
    enum
    {
        eArrayType_None,
        eArrayType_Primitive,
        eArrayType_Struct,
        eArrayType_Object,
    };
    
    struct Interface
    {
        Module& module;
        
        ParserConfig config;
        
        char name[LITERAL_MAX + 1];
        char parent[LITERAL_MAX + 1];
        std::vector<Function> methods;
        std::vector<Property> properties;
        int arrayType;
        bool genDef;
        
        Type elClass;
        
        Interface(Module& m):
        module(m), arrayType(eArrayType_None), genDef(false)
        {
            memset(name, 0, sizeof(name));
            memset(parent, 0, sizeof(parent));
        }
        
        Interface(const Interface& i):
        module(i.module),
        methods(i.methods),
        properties(i.properties),
        arrayType(i.arrayType),
        genDef(i.genDef),
        elClass(i.elClass),
        config(i.config)
        {
            strcpy(name, i.name);
            strcpy(parent, i.parent);
        }
    };
    
    struct Constant
    {
        Module& module;
        
        ParserConfig config;
        
        char name[LITERAL_MAX + 1];
        
        typedef map<string, int> ValueMap;
        ValueMap valueMap;
        
        Constant(Module& m):
        module(m)
        {
            memset(name, 0, sizeof(name));
        }
        
        Constant(const Constant& c):
        module(c.module), valueMap(c.valueMap), config(c.config)
        {
            strcpy(name, c.name);
        }
    };
    
    struct Module
    {
        char name[LITERAL_MAX + 1];
        std::vector<Interface> interfaces;
        std::vector<Constant> constants;
        std::vector<Function> functions;
        
        Module()
        {
            memset(name, 0, sizeof(name));
        }
    };
    
    
    class Parser
    {
    private:
        Lexer mLexer;
        
    public:
        Parser(Lexer& lexer);
        bool Parse(Module& module);
        
    private:
        bool FindModule(Module& module);
        void ParseInterface(Module& module);
        void ParseGlobalFunction(Module& module);
        void ParseFunction(Function& method);
        void ParseType(const Token& token, Type& type);
        void ParseEnum(Module& module);
        void ParsePrimitiveArray(Module& module);
        void ParseStructArray(Module& module);
        void ParseObjectArray(Module& module);
        void ParseConfig(ParserConfig& config);
        string ParseDefaultValue();
    };
    
}

#endif





















