/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.5   0.1     Create
 ******************************************************************************/
#ifndef PI_TOOLS_PIH_LEXER_H
#define PI_TOOLS_PIH_LEXER_H

#include <pi/Core.h>

using namespace std;

#define LITERAL_MAX   127

namespace nspi
{
    
    enum
    {
        eToken_Identity = 0,
        eToken_Colon,
        eToken_Star,
        eToken_At,
        eToken_Comma,
        eToken_Assignment,
        eToken_Semicolon,
        eToken_DoubleColon,
        eToken_OpenBracket,
        eToken_OpenParen,
        eToken_CloseParen,
        eToken_CloseBracket,
        eToken_LessThan,
        eToken_MoreThan,
        eToken_Tilde,
        eToken_And,
        eToken_Comment,
        eToken_Struct,
        eToken_String,
        eToken_Public,
        eToken_Namespace,
        eToken_Virtual,
        eToken_Const,
        eToken_False,
        eToken_True,
        eToken_Nullptr,
        eToken_Void,
        eToken_I,
        eToken_I8,
        eToken_I16,
        eToken_I32,
        eToken_I64,
        eToken_U8,
        eToken_U16,
        eToken_U32,
        eToken_U64,
        eToken_F32,
        eToken_F64,
        eToken_BitFunc,
        eToken_Enum,
        eToken_PIEnum,
        eToken_Extern,
        eToken_Boolean,
        eToken_Interface,
        eToken_Method,
        eToken_Function,
        eToken_Setter,
        eToken_Getter,
        eToken_PrimitiveArray,
        eToken_StructArray,
        eToken_ObjectArray,
    };
    
    
    struct Token
    {
        int type;
        char literal[LITERAL_MAX+1];
        
        Token()
        {
            type = eType_Void,
            memset(literal, 0, sizeof(literal));
        }
    };
    
    class Lexer
    {
    private:
        SmartPtr<iReaderUTF8> mSource;
        Token mLastToken;
        bool mHasToken;
        list<Token> mTokens;
        
    public:
        Lexer(iReaderUTF8 *source);
        
        bool Read(Token& token);
        void Skip(int32_t count);
        void Restore();
        
    private:
        bool Parse();
        
        void ParseToken(const char *start, int64_t size);
        
        void PushAnd();
        
        void PushColon();
        
        void PushDoubleColon();
        
        void PushOpenBracket();
        
        void PushCloseBracket();
        
        void PushOpenParen();
        
        void PushCloseParen();
        
        void PushLessThan();
        
        void PushMoreThan();
        
        void PushTilde();
        
        void PushSemicolon();
        
        void PushAssignment();
        
        void PushStar();
        
        void PushComment(const char *comment, int64_t size);
        
        void PushComma();
        
        void PushString(const std::string& content);
    };
    
}

#endif 





























