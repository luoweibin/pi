/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.5   0.1     Create
 ******************************************************************************/
#include "Parser.h"

using namespace nspi;
using namespace std;

void ParsePrimitive(const Token& in, Type& type)
{
    switch (in.type)
    {
        case eToken_Void:
            type.name = eType_Void;
            break;
        case eToken_Boolean:
            type.name = eType_Boolean;
            break;
        case eToken_I:
            type.name = eType_Enum;
            break;
        case eToken_I8:
            type.name = eType_I8;
            break;
        case eToken_I16:
            type.name = eType_I16;
            break;
        case eToken_I32:
            type.name = eType_I32;
            break;
        case eToken_I64:
            type.name = eType_I64;
            break;
        case eToken_U8:
            type.name = eType_U8;
            break;
        case eToken_U16:
            type.name = eType_U16;
            break;
        case eToken_U32:
            type.name = eType_U32;
            break;
        case eToken_U64:
            type.name = eType_U64;
            break;
        case eToken_F32:
            type.name = eType_F32;
            break;
        case eToken_F64:
            type.name = eType_F64;
            break;
        default:
            type.name = eType_Void;
            break;
    }
    
    strcat(type.literal, in.literal);
}


Parser::Parser(Lexer& lexer):
mLexer(lexer)
{
}

bool Parser::Parse(Module& module)
{
    piCheck(FindModule(module), true);
    
    Token last;
    
    while (true)
    {
        Token token;
        if (!mLexer.Read(token))
        {
            break;
        }
        
        if (token.type == eToken_Interface)
        {
            ParseInterface(module);
        }
        else if (token.type == eToken_PIEnum)
        {
            ParseEnum(module);
        }
        else if (token.type == eToken_Function)
        {
            ParseGlobalFunction(module);
        }
        else if (token.type == eToken_PrimitiveArray)
        {
            // Skip "("
            mLexer.Skip(1);
            ParsePrimitiveArray(module);
        }
        else if (token.type == eToken_StructArray)
        {
            // Skip "("
            mLexer.Skip(1);
            ParseStructArray(module);
        }
        else if (token.type == eToken_ObjectArray)
        {
            // Skip "("
            mLexer.Skip(1);
            ParseObjectArray(module);
        }
        
        last = token;
    }
    
    return true;
}

bool Parser::FindModule(Module& module)
{
    while (true)
    {
        Token token;
        piCheck(mLexer.Read(token), false);
        
        if (token.type == eToken_Namespace)
        {
            Token name;
            mLexer.Read(name);
            strcpy(module.name, name.literal);
            return true;
        }
    }
    
    return false;
}

static string MakePropertyName(const Function& function)
{
    int32_t size = (int32_t)strlen(function.name);
    
    if (function.returnType.name == eType_Boolean)
    {
        return string(function.name + 2, size - 2);
    }
    else
    {
        return string(function.name + 3, size - 3);
    }
}

static vector<Property>::iterator FindProperty(vector<Property>& properties, const string& name)
{
    for (vector<Property>::iterator it = properties.begin();
         it != properties.end();
         ++it)
    {
        if (strcmp(it->name, name.c_str()) == 0)
        {
            return it;
        }
    }
    
    return properties.end();
}

void Parser::ParseEnum(Module& module)
{
    Constant constant(module);

    // ParseConfig
    ParseConfig(constant.config);
    
    // enum '
    mLexer.Skip(1);
    
    Token enumName;
    piCheck(mLexer.Read(enumName), ;);
    piCheck(enumName.type == eToken_Identity, ;);
    
    strcpy(constant.name, enumName.literal);
    
    // skip '{'
    mLexer.Skip(1);
    
    // Expect identifier
    Token tokenPeek;
    mLexer.Read(tokenPeek);
    if (tokenPeek.type != eToken_OpenBracket)
    {
        mLexer.Restore();
    }
    
    int32_t value = 0;
    
    while (true)
    {
        Token tokenName;
        mLexer.Read(tokenName);
        
        if (tokenName.type == eToken_Comma)
        {
            mLexer.Read(tokenName);
        }
        
        if (tokenName.type == eToken_CloseBracket)
        {
            // Skip ';'
            mLexer.Skip(1);
            break;
        }
        
        // Expect '='
        Token tokenAssign;
        mLexer.Read(tokenAssign);
        
        if (tokenAssign.type == eToken_Assignment)
        {
            Token tokenValue;
            mLexer.Read(tokenValue);
            
            if (tokenValue.type == eToken_BitFunc)
            {
                // Something like piBit(2)
                // Skip '('
                mLexer.Skip(1);
                
                mLexer.Read(tokenValue);
                value = piBit(stoi(tokenValue.literal));
                
                // Skip ')'
                mLexer.Skip(1);
            }
            else
            {
                value = stoi(tokenValue.literal);
            }
            
            constant.valueMap[tokenName.literal] = value;
            ++value;
        }
        else
        {
            constant.valueMap[tokenName.literal] = value;
            ++value;
        }
    }
    
    module.constants.push_back(constant);
}

void Parser::ParseConfig(ParserConfig &config)
{
    // Skip '('
    mLexer.Skip(1);
    
    while (true)
    {
        Token tokenKey;
        mLexer.Read(tokenKey);
     
        if (tokenKey.type == eToken_CloseParen)
        {
            break;
        }
        
        Token tokenAssign;
        mLexer.Read(tokenAssign);
        
        Token tokenValue;
        mLexer.Read(tokenValue);
        
        string key = tokenKey.literal;
        config[key] = tokenValue;
        
        Token test;
        mLexer.Read(test);
        if (test.type != eToken_Comma)
        {
            mLexer.Restore();
        }
    }
}

void Parser::ParseInterface(Module& module)
{
    Interface interface(module);

    // ParseConfig
    ParseConfig(interface.config);
    
    Token tokenStruct;
    mLexer.Read(tokenStruct);
    if (tokenStruct.type != eToken_Struct)
    {
        mLexer.Restore();
        return;
    }
    
    // Expect identifier
    Token tokenName;
    mLexer.Read(tokenName);
    strcpy(interface.name, tokenName.literal);
    
    Token tokenColon;
    mLexer.Read(tokenColon);
    if (tokenColon.type == eToken_Colon)
    {
        // Skip 'public'
        mLexer.Skip(1);
        
        // Expect identifier
        Token tokenParent;
        mLexer.Read(tokenParent);
        strcpy(interface.parent, tokenParent.literal);
    }
    else
    {
        mLexer.Restore();
    }
    
    // Skip '{'
    mLexer.Skip(1);

    
    while (true)
    {
        Token token;
        if (!mLexer.Read(token) || token.type == eToken_CloseBracket)
        {
            // Break when no more token or encountering '}'
            break;
        }
        
        if (token.type == eToken_Method)
        {
            Function function(module);

            // ParseConfig
            ParseConfig(function.config);
            
            // Skip 'virtual'
            mLexer.Skip(1);
            
            ParseFunction(function);
            
            interface.methods.push_back(function);
        }
        else if (token.type == eToken_Setter)
        {
            Function function(module);

            // ParseConfig
            ParseConfig(function.config);
            
            // Skip 'virtual'
            mLexer.Skip(1);
            
            ParseFunction(function);

            string propName = MakePropertyName(function);
            vector<Property>::iterator it = FindProperty(interface.properties, propName);
            if (it == interface.properties.end() && !function.params.empty())
            {
                Property property;
                property.config = function.config;
                strcpy(property.name, propName.c_str());
                property.type = function.params.front().type;
                piFlagOn(property.flags, eProperty_Setter);
                
                interface.properties.push_back(property);
            }
            else
            {
                it->config.insert(function.config.begin(), function.config.end());
                piFlagOn(it->flags, eProperty_Setter);
            }
        }
        else if (token.type == eToken_Getter)
        {
            Function function(module);

            // ParseConfig
            ParseConfig(function.config);
            
            // Skip 'virtual'
            mLexer.Skip(1);
            
            ParseFunction(function);
            
            string propName = MakePropertyName(function);
            vector<Property>::iterator it = FindProperty(interface.properties, propName);
            if (it == interface.properties.end())
            {
                Property property;
                property.config = function.config;
                strcpy(property.name, propName.c_str());
                property.type = function.returnType;
                piFlagOn(property.flags, eProperty_Getter);
                
                interface.properties.push_back(property);
            }
            else
            {
                it->config.insert(function.config.begin(), function.config.end());
                piFlagOn(it->flags, eProperty_Getter);
            }
        }
    }
    
    module.interfaces.push_back(interface);
}

string Parser::ParseDefaultValue()
{
    string value;
    
    Token tokenValue;
    while (mLexer.Read(tokenValue))
    {
        if (tokenValue.type == eToken_OpenParen)
        {
            value += "()";
            
            // Skip ')'
            mLexer.Skip(1);
            break;
        }
        else if (tokenValue.type == eToken_Comma)
        {
            break;
        }
        else if (tokenValue.type == eToken_CloseParen)
        {
            // End of parameter list, shound be restored.
            mLexer.Restore();
            break;
        }
        
        value += tokenValue.literal;
    }
    
    return value;
}

void Parser::ParseFunction(Function& function)
{
    // Expect return type
    Token tokenReturn;
    mLexer.Read(tokenReturn);
    ParseType(tokenReturn, function.returnType);
    
    // Expect name
    Token funcName;
    mLexer.Read(funcName);
    strcpy(function.name, funcName.literal);
    
    // Skip '('
    mLexer.Skip(1);
    
    // Parameters
    {
        Token tokenType;
        while (mLexer.Read(tokenType))
        {
            if (tokenType.type == eToken_CloseParen)
            {
                break;
            }
            
            if (tokenType.type == eToken_Comma)
            {
                mLexer.Read(tokenType);
            }
            
            Param param;
            ParseType(tokenType, param.type);
            
            Token paramName;
            mLexer.Read(paramName);
            strcpy(param.name, paramName.literal);
            
            Token tokenAssign;
            mLexer.Read(tokenAssign);
            if (tokenAssign.type == eToken_Assignment)
            {
                string defValue = ParseDefaultValue();
                strcpy(param.defaultValue, defValue.c_str());
            }
            else if (tokenAssign.type != eToken_Comma)
            {
                mLexer.Restore();
            }
            
            function.params.push_back(param);
        }
    }
    
    Token tokenConst;
    mLexer.Read(tokenConst);
    
    if (tokenConst.type == eToken_Const)
    {
        piFlagOn(function.flags, eFunction_Const);
    }
}


void Parser::ParseGlobalFunction(Module& module)
{
    Function function(module);
    
    // ParseConfig
    ParseConfig(function.config);
    
    ParseFunction(function);
    module.functions.push_back(function);
}


static void AddArrayMethod(Interface& i)
{
    // IsEmpty
    {
        Function m(i.module);
        piFlagOn(m.flags, eFunction_Const);
        strcpy(m.name, "IsEmpty");
        strcpy(m.returnType.literal, "bool");
        m.returnType.name = eType_Boolean;
        
        i.methods.push_back(m);
    }
    
    // GetBack
    {
        Function m(i.module);
        piFlagOn(m.flags, eFunction_Const);
        strcpy(m.name, "GetBack");
        m.returnType = i.elClass;
        
        i.methods.push_back(m);
    }
    
    // GetFront
    {
        Function m(i.module);
        piFlagOn(m.flags, eFunction_Const);
        strcpy(m.name, "GetFront");
        m.returnType = i.elClass;
        
        i.methods.push_back(m);
    }
    
    // PushBack
    {
        Function m(i.module);
        strcpy(m.name, "PushBack");
        
        strcpy(m.returnType.literal, "void");
        m.returnType.name = eType_Void;
        
        Param param;
        param.type = i.elClass;
        strcpy(param.name, "value");
        m.params.push_back(param);
        
        i.methods.push_back(m);
    }
    
    // PopBack
    {
        Function m(i.module);
        strcpy(m.name, "PopBack");
        
        strcpy(m.returnType.literal, "void");
        m.returnType.name = eType_Void;
        
        i.methods.push_back(m);
    }
    
    // GetCount
    {
        Function m(i.module);
        piFlagOn(m.flags, eFunction_Const);
        strcpy(m.name, "GetCount");
        
        strcpy(m.returnType.literal, "int32_t");
        m.returnType.name = eType_I32;
        
        i.methods.push_back(m);
    }
    
    // GetItem
    {
        Function m(i.module);
        piFlagOn(m.flags, eFunction_Const);
        strcpy(m.name, "GetItem");
        m.returnType = i.elClass;
        
        Param param;
        param.type.name = eType_I32;
        strcpy(param.type.literal, "int32_t");
        strcpy(param.name, "index");
        m.params.push_back(param);
        
        i.methods.push_back(m);
    }
    
    // SetItem
    {
        Function m(i.module);
        strcpy(m.name, "SetItem");
        
        strcpy(m.returnType.literal, "void");
        m.returnType.name = eType_Void;
        
        {
            Param param;
            param.type.name = eType_I32;
            strcpy(param.type.literal, "int32_t");
            strcpy(param.name, "index");
            m.params.push_back(param);
        }
        
        {
            Param param;
            param.type = i.elClass;
            strcpy(param.name, "value");
            m.params.push_back(param);
        }
        
        i.methods.push_back(m);
    }
    
    // Resize
    {
        Function m(i.module);
        strcpy(m.name, "Resize");
        
        strcpy(m.returnType.literal, "void");
        m.returnType.name = eType_Void;
        
        {
            Param param;
            param.type.name = eType_I32;
            strcpy(param.type.literal, "int32_t");
            strcpy(param.name, "size");
            m.params.push_back(param);
        }
        
        i.methods.push_back(m);
    }
    
    // Clear
    {
        Function m(i.module);
        strcpy(m.name, "Clear");
        
        strcpy(m.returnType.literal, "void");
        m.returnType.name = eType_Void;
        
        i.methods.push_back(m);
    }
    
    // Remove
    {
        Function m(i.module);
        strcpy(m.name, "Remove");
        
        strcpy(m.returnType.literal, "void");
        m.returnType.name = eType_Void;
        
        {
            Param param;
            param.type.name = eType_I32;
            strcpy(param.type.literal, "int32_t");
            strcpy(param.name, "index");
            m.params.push_back(param);
        }
        
        i.methods.push_back(m);
    }
    
    // ToMemory
    if (i.arrayType == eArrayType_Primitive)
    {
        Function m(i.module);
        piFlagOn(m.flags, eFunction_Const);
        strcpy(m.name, "ToMemory");
        
        Type memType;
        piFlagOn(memType.flags, eTypeFlag_Object);
        strcpy(memType.literal, "nspi::iMemory");
        memType.name = eType_Object;
        
        m.returnType = memType;
        
        i.methods.push_back(m);
    }
}


void Parser::ParsePrimitiveArray(Module &module)
{
    Token typeName;
    mLexer.Read(typeName);
    
    if (strcmp(typeName.literal, "type") == 0)
    {
        return;
    }
    
    Interface arrayClass(module);
    arrayClass.arrayType = eArrayType_Primitive;

    ParseType(typeName, arrayClass.elClass);
    
    //Skip ","
    mLexer.Skip(1);
    
    Token arrayName;
    mLexer.Read(arrayName);
        
    strcpy(arrayClass.parent, "iRefObject");
    strcpy(arrayClass.name, arrayName.literal);
    
    AddArrayMethod(arrayClass);
    
    module.interfaces.push_back(arrayClass);
}


void Parser::ParseStructArray(Module &module)
{
    Token typeName;
    mLexer.Read(typeName);
    
    if (strcmp(typeName.literal, "type") == 0)
    {
        return;
    }
    
    Interface arrayClass(module);
    arrayClass.arrayType = eArrayType_Struct;
    ParseType(typeName, arrayClass.elClass);
    
    //Skip ","
    mLexer.Skip(1);
    
    Token arrayName;
    mLexer.Read(arrayName);
    
    strcpy(arrayClass.parent, "iRefObject");
    strcpy(arrayClass.name, arrayName.literal);
    
    AddArrayMethod(arrayClass);
    
    module.interfaces.push_back(arrayClass);
}


void Parser::ParseObjectArray(Module &module)
{
    Token typeName;
    mLexer.Read(typeName);
    
    if (strcmp(typeName.literal, "type") == 0)
    {
        return;
    }
    
    //Skip ");"
    mLexer.Skip(2);
    
    Interface arrayClass(module);
    arrayClass.arrayType = eArrayType_Object;
    
    ParseType(typeName, arrayClass.elClass);
    piFlagOn(arrayClass.elClass.flags, eTypeFlag_Object);
    
    strcpy(arrayClass.parent, "iRefObject");
    strcpy(arrayClass.name, piFormat("%sArray", typeName.literal).c_str());
    
    AddArrayMethod(arrayClass);
    
    module.interfaces.push_back(arrayClass);
}


static int ParseObjectType(const string& str)
{
    if ("iI8Array" == str)
    {
        return eType_I8Array;
    }
    else if ("iU8Array" == str)
    {
        return eType_U8Array;
    }
    else if ("nspi::iI8Array" == str)
    {
        return eType_I8Array;
    }
    else if ("nspi::iU8Array" == str)
    {
        return eType_U8Array;
    }
    else if ("iI16Array" == str)
    {
        return eType_I16Array;
    }
    else if ("iU16Array" == str)
    {
        return eType_U16Array;
    }
    if ("nspi::iI16Array" == str)
    {
        return eType_I16Array;
    }
    else if ("nspi::iU16Array" == str)
    {
        return eType_U16Array;
    }
    else if ("iI32Array" == str)
    {
        return eType_I32Array;
    }
    else if ("iU32Array" == str)
    {
        return eType_U32Array;
    }
    else if ("nspi::iI32Array" == str)
    {
        return eType_I32Array;
    }
    else if ("nspi::iU32Array" == str)
    {
        return eType_U32Array;
    }
    else if ("iI64Array" == str)
    {
        return eType_I64Array;
    }
    else if ("iU64Array" == str)
    {
        return eType_U64Array;
    }
    else if ("nspi::iI64Array" == str)
    {
        return eType_I64Array;
    }
    else if ("nspi::iU64Array" == str)
    {
        return eType_U64Array;
    }
    else if ("iF64Array" == str)
    {
        return eType_F64Array;
    }
    else if ("nspi::iF64Array" == str)
    {
        return eType_F64Array;
    }
    else if ("iF32Array" == str)
    {
        return eType_F32Array;
    }
    else if ("nspi::iF32Array" == str)
    {
        return eType_F32Array;
    }
    else
    {
        return eType_Object;
    }
}

void Parser::ParseType(const Token& in, Type& type)
{
    Token token = in;
    if (token.type == eToken_Const)
    {
        mLexer.Read(token);
        piFlagOn(type.flags, eTypeFlag_Const);
    }
    
    if (token.type == eToken_Identity)
    {
        type.name = eType_Object;
        piFlagOn(type.flags, eTypeFlag_Struct);
        strcpy(type.literal, token.literal);
        
        do
        {
            mLexer.Read(token);
            
            if (token.type != eToken_DoubleColon)
            {
                mLexer.Restore();
                break;
            }
            
            strcat(type.literal, "::");
            
            // Expect identifier
            mLexer.Read(token);
            strcat(type.literal, token.literal);
        }
        while (true);
    }
    else
    {
        ParsePrimitive(token, type);
    }
    
    mLexer.Read(token);
    if (token.type == eToken_Star)
    {
        piFlagOff(type.flags, eTypeFlag_Struct);
        piFlagOn(type.flags, eTypeFlag_Object);
        
        type.name = ParseObjectType(type.literal);
    }
    else if (token.type == eToken_And)
    {
        piFlagOff(type.flags, eTypeFlag_Object);
        piFlagOn(type.flags, eTypeFlag_Struct);
    }
    else
    {
        mLexer.Restore();
    }
}





























