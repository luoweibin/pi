/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.5   0.1     Create
 ******************************************************************************/

#include "Lexer.h"

using namespace nspi;

struct Keyword
{
    const char* literal;
    int len;
    int type;
};

#define MakeKeyword(literal, type) \
{ literal, sizeof(literal)-1, type }

static const Keyword gKeywords[] =
{
    MakeKeyword("struct", eToken_Struct),
    MakeKeyword("public", eToken_Public),
    MakeKeyword("namespace", eToken_Namespace),
    MakeKeyword("virtual", eToken_Virtual),
    MakeKeyword("const", eToken_Const),
    MakeKeyword("bool", eToken_Boolean),
    MakeKeyword("false", eToken_False),
    MakeKeyword("true", eToken_True),
    MakeKeyword("nullptr", eToken_Nullptr),
    MakeKeyword("int8_t", eToken_I8),
    MakeKeyword("int16_t", eToken_I16),
    MakeKeyword("int32_t", eToken_I32),
    MakeKeyword("int64_t", eToken_I64),
    MakeKeyword("uint8_t", eToken_U8),
    MakeKeyword("uint16_t", eToken_U16),
    MakeKeyword("uint32_t", eToken_U32),
    MakeKeyword("uint64_t", eToken_U64),
    MakeKeyword("int", eToken_I),
    MakeKeyword("float", eToken_F32),
    MakeKeyword("double", eToken_F64),
    MakeKeyword("void", eToken_Void),
    MakeKeyword("void", eToken_Void),
    MakeKeyword("piBit", eToken_BitFunc),
    MakeKeyword("enum", eToken_Enum),
    MakeKeyword("extern", eToken_Extern),
    MakeKeyword("PIInterface", eToken_Interface),
    MakeKeyword("PIMethod", eToken_Method),
    MakeKeyword("PIFunction", eToken_Function),
    MakeKeyword("PISetter", eToken_Setter),
    MakeKeyword("PIGetter", eToken_Getter),
    MakeKeyword("PIEnum", eToken_PIEnum),
    MakeKeyword("PI_DEFINE_PRIMITIVE_ARRAY", eToken_PrimitiveArray),
    MakeKeyword("PI_DEFINE_STRUCT_ARRAY", eToken_StructArray),
    MakeKeyword("PI_DEFINE_OBJECT_ARRAY", eToken_ObjectArray),
    {nullptr, -1, 0},
};


/**
 * =================================================================================================
 */

Lexer::Lexer(iReaderUTF8 *source):
mHasToken(false), mSource(source)
{
    
}

bool Lexer::Read(Token& token)
{
    if (!mTokens.empty() || Parse())
    {
        mLastToken = mTokens.front();
        token = mLastToken;
        mHasToken = true;
        
        mTokens.pop_front();
        return true;
    }
    else
    {
        mHasToken = false;
        return false;
    }
}

void Lexer::Skip(int32_t count)
{
    for (int32_t i = 0; i < count; ++i)
    {
        Token token;
        Read(token);
    }
}

void Lexer::Restore()
{
    if (mHasToken)
    {
        mTokens.push_front(mLastToken);
    }
}


bool Lexer::Parse()
{
    bool readComment = false;
    string comment;
    
    while (mSource->ReadLine())
    {
        int64_t size = mSource->GetSize();
        if (size == 0)
        {
            continue;
        }
        
        const char *start = mSource->GetLine();
        const char *current = start;
        const char *end = start + size;
        
        while (current != end)
        {
            if (readComment)
            {
                while (current != end)
                {
                    if (*current == '*')
                    {
                        ++current;
                        
                        if (current == end)
                        {
                            continue;
                        }
                        
                        if (*current != '/')
                        {
                            comment.append(1, '/');
                            continue;
                        }
                        
                        comment.append(1, '/');
                        PushComment(comment.c_str(), comment.size());
                        readComment = false;
                        comment.clear();
                        ++current;
                        start = current;
                        break;
                    }
                    
                    comment.append(1, *current);
                    ++current;
                }
                
                if (!readComment)
                {
                    continue;
                }
                else if (current == end)
                {
                    comment.append(1, '\n');
                    continue;
                }
            }
            
            if (*current == ':')
            {
                if (start != current)
                {
                    ParseToken(start, current - start);
                    start = current;
                }
                else
                {
                    if (current + 1 != end && current[1] == ':')
                    {
                        PushDoubleColon();
                        current += 2;
                        start = current;
                    }
                    else
                    {
                        PushColon();
                        ++current;
                        start = current;
                    }
                }
            }
            else if (*current == '{')
            {
                if (start != current)
                {
                    ParseToken(start, current - start);
                    start = current;
                }
                else
                {
                    PushOpenBracket();
                    ++current;
                    start = current;
                }
            }
            else if (*current == '}')
            {
                if (start != current)
                {
                    ParseToken(start, current - start);
                    start = current;
                }
                else
                {
                    PushCloseBracket();
                    ++current;
                    start = current;
                }
            }
            else if (*current == '(')
            {
                if (start != current)
                {
                    ParseToken(start, current - start);
                    start = current;
                }
                else
                {
                    PushOpenParen();
                    ++current;
                    start = current;
                }
            }
            else if (*current == ')')
            {
                if (start != current)
                {
                    ParseToken(start, current - start);
                    start = current;
                }
                else
                {
                    PushCloseParen();
                    ++current;
                    start = current;
                }
            }
            else if (*current == '<')
            {
                if (start != current)
                {
                    ParseToken(start, current - start);
                    start = current;
                }
                else
                {
                    PushLessThan();
                    ++current;
                    start = current;
                }
            }
            else if (*current == '>')
            {
                if (start != current)
                {
                    ParseToken(start, current - start);
                    start = current;
                }
                else
                {
                    PushMoreThan();
                    ++current;
                    start = current;
                }
            }
            else if (*current == '~')
            {
                if (start != current)
                {
                    ParseToken(start, current - start);
                    start = current;
                }
                else
                {
                    PushTilde();
                    ++current;
                    start = current;
                }
            }
            else if (*current == ',')
            {
                if (start != current)
                {
                    ParseToken(start, current - start);
                    start = current;
                }
                else
                {
                    PushComma();
                    ++current;
                    start = current;
                }
            }
            else if (*current == '&')
            {
                if (start != current)
                {
                    ParseToken(start, current - start);
                    start = current;
                }
                else
                {
                    PushAnd();
                    ++current;
                    start = current;
                }
            }
            else if (*current == '=')
            {
                if (start != current)
                {
                    ParseToken(start, current - start);
                    start = current;
                }
                else
                {
                    PushAssignment();
                    ++current;
                    start = current;
                }
            }
            else if (*current == ';')
            {
                if (start != current)
                {
                    ParseToken(start, current - start);
                    start = current;
                }
                else
                {
                    PushSemicolon();
                    ++current;
                    start = current;
                }
            }
            else if (*current == '/')
            {
                ++current;
                
                readComment = true;
                if (*current == '/')
                {
                    ++current;
                    readComment = false;
                    
                    if (end - current > 0)
                    {
                        comment.append(current, end - current);
                    }
                    
                    current = end;
                }
                else
                {
                    comment.append(1, '/');
                    ++current;
                }
            }
            else if (*current == '*')
            {
                if (start != current)
                {
                    ParseToken(start, current - start);
                    start = current;
                }
                else
                {
                    PushStar();
                    ++current;
                    start = current;
                }
            }
            else if (*current == '"')
            {
                ++current;
                
                string content;
                while (current != end)
                {
                    if (*current != '"')
                    {
                        content.append(1, *current);
                        ++current;
                    }
                    else
                    {
                        ++current;
                        break;
                    }
                }
                
                PushString(content);
                start = current;
            }
            else if (isspace(*current))
            {
                if (start == current)
                {
                    ++current;
                    start = current;
                }
                else
                {
                    ParseToken(start, current - start);
                    ++current;
                    start = current;
                }
            }
            else if (current + 1 == end)
            {
                ParseToken(start, current + 1 - start);
                ++current;
                start = current;
            }
            else
            {
                ++current;
            }
        }
        
        if (!mTokens.empty())
        {
            break;
        }
    }
    
    return !mTokens.empty();
}


void Lexer::ParseToken(const char *start, int64_t size)
{
    const Keyword* current = gKeywords;
    while (current->literal != nullptr)
    {
        if (strncmp(current->literal, start, current->len) == 0)
        {
            Token token;
            token.type = current->type;
            strcpy(token.literal, current->literal);
            mTokens.push_back(token);
            return;
        }
        else
        {
            ++current;
        }
    }
    
    Token token;
    token.type = eToken_Identity;
    strncpy(token.literal, start, size);
    mTokens.push_back(token);
}

void Lexer::PushAnd()
{
    Token token;
    token.type = eToken_And;
    token.literal[0] = '&';
    mTokens.push_back(token);
}

void Lexer::PushColon()
{
    Token token;
    token.type = eToken_Colon;
    token.literal[0] = ':';
    mTokens.push_back(token);
}

void Lexer::PushDoubleColon()
{
    Token token;
    token.type = eToken_DoubleColon;
    token.literal[0] = ':';
    token.literal[1] = ':';
    mTokens.push_back(token);
}

void Lexer::PushOpenBracket()
{
    Token token;
    token.type = eToken_OpenBracket;
    token.literal[0] = '{';
    mTokens.push_back(token);
}

void Lexer::PushCloseBracket()
{
    Token token;
    token.type = eToken_CloseBracket;
    token.literal[0] = '}';
    mTokens.push_back(token);
}

void Lexer::PushOpenParen()
{
    Token token;
    token.type = eToken_OpenParen;
    token.literal[0] = '(';
    mTokens.push_back(token);
}

void Lexer::PushCloseParen()
{
    Token token;
    token.type = eToken_CloseParen;
    token.literal[0] = ')';
    mTokens.push_back(token);
}

void Lexer::PushLessThan()
{
    Token token;
    token.type = eToken_LessThan;
    token.literal[0] = '<';
    mTokens.push_back(token);
}

void Lexer::PushMoreThan()
{
    Token token;
    token.type = eToken_MoreThan;
    token.literal[0] = '>';
    mTokens.push_back(token);
}

void Lexer::PushTilde()
{
    Token token;
    token.type = eToken_Tilde;
    token.literal[0] = '~';
    mTokens.push_back(token);
}

void Lexer::PushSemicolon()
{
    Token token;
    token.type = eToken_Semicolon;
    token.literal[0] = ';';
    mTokens.push_back(token);
}

void Lexer::PushAssignment()
{
    Token token;
    token.type = eToken_Assignment;
    token.literal[0] = '=';
    mTokens.push_back(token);
}

void Lexer::PushStar()
{
    Token token;
    token.type = eToken_Star;
    token.literal[0] = '*';
    mTokens.push_back(token);
}

void Lexer::PushString(const string &content)
{
    Token token;
    token.type = eToken_String;
    memcpy(token.literal, content.c_str(), piMin(content.size(), LITERAL_MAX));
    mTokens.push_back(token);
}

void Lexer::PushComment(const char *comment, int64_t size)
{
}

void Lexer::PushComma()
{
    Token token;
    token.type = eToken_Comma;
    token.literal[0] = ',';
    mTokens.push_back(token);
}























