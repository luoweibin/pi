/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.20   0.1     Create
 ******************************************************************************/
#include "../pih/Parser.h"
#include "Util.h"

using namespace nspi;

string MakeEntryName(const string& genDir, const string& defFile);
string MakeModuleFile(const string& genDir, const string& entryName);


int GenLuaInit(int argc, char * const argv[])
{
    if (argc < 4)
    {
        PrintUsage();
        return 1;
    }
    
    string outFile = argv[2];
    
    SmartPtr<iStream> stream = CreateFileStream(outFile, "w");
    piCheck(!stream.IsNull(), 1);
    
    printf("Generating %s.\n", outFile.c_str());
    
    for (int i = 3; i < argc; ++i)
    {
        string file = argv[i];
        file = file.substr(0, file.size() - 1);
        stream->WriteString(piFormat("#include \"%scpp\"\n", file.c_str()));
    }
    
    stream->WriteString("void lua_pi_Init(lua_State* L)\n{\n");
    
    for (int i = 3; i < argc; ++i)
    {
        string name = argv[i];
        
        std::replace(name.begin(), name.end(), '/', '_');
        name = name.substr(0, name.size() - 2);
        
        string content = piFormat("lua_%s_Init(L);", name.c_str());
        stream->WriteString(piFormat("    %s\n", content.c_str()));
        
//        printf("Add %s\n", content.c_str());
    }
    
    stream->WriteString("}");
    
    return 0;
}

bool GenLuaInit1(const string& genDir, const string& path, const string& defFile)
{
    {
        if (!piFileExist(path))
        {
            if (piFileExist(defFile))
            {
                SmartPtr<iStream> s = CreateFileStream(path, "w");
                if (s.IsNull())
                {
                    printf("Unable to open file '%s'.", path.c_str());
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
    }
    
    string entryName = MakeEntryName(genDir, defFile);
    
    bool hasContent = false;

    string decl = "void lua_pi_Init(lua_State* L)";

    string buffer;
    {
        SmartPtr<iStream> stream = CreateFileStream(path, "r");
        if (stream.IsNull())
        {
            printf("Unable to open file '%s'.\n", path.c_str());
            return false;
        }
        
        stream->Seek(eSeek_Set, 0);
        
        SmartPtr<iReaderUTF8> reader = CreateReaderUTF8(stream, 1024);
        
        string content = piFormat("%s(L);", entryName.c_str());
        
        while (reader->ReadLine())
        {
            string line(reader->GetLine(), reader->GetSize());
            
            if (line.find(content) != string::npos)
            {
                hasContent = true;
                buffer.append(piFormat("    %s\n", content.c_str()));
            }
            else if (line.find(decl) != string::npos
                     || line.find("{") != string::npos
                     || line.find("}") != string::npos
                     || line.empty())
            {
                // nothing need to be done
            }
            else
            {
                string tmp = line.substr(4, line.size() - 8);
                string defFileName = MakeModuleFile(genDir, tmp);
                if (piFileExist(defFileName))
                {
                    buffer.append(piFormat("%s\n", line.c_str()));
                }
            }
        }
        
        if (!hasContent)
        {
            buffer.append(piFormat("    %s\n", content.c_str()));
        }
    }
    
    if (!hasContent)
    {
        SmartPtr<iStream> stream = CreateFileStream(path, "w");
        if (stream.IsNull())
        {
            printf("Unable to open file '%s'.\n", path.c_str());
            return false;
        }
        
        stream->WriteString(piFormat("%s\n", decl.c_str()));
        stream->WriteString("{\n");
        stream->WriteString(buffer);
        stream->WriteString("}\n");
    }
    
    return true;
}












































