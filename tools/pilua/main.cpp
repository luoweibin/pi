/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2014.6.21   0.1     Create
 ******************************************************************************/
#include "../pih/Parser.h"
#include "Util.h"

using namespace nspi;
using namespace std;


int GenLuaModule(int argc, char * const argv[]);
int GenLuaInit(int argc, char * const argv[]);


int main(int argc, char * const argv[])
{
    piInit();
    
    if (argc < 2)
    {
        printf("Action [module|init] not set.\n");
        PrintUsage();
        return 1;
    }
    
    string action = argv[1];
    
    if (action == "module")
    {
        return GenLuaModule(argc, argv);
    }
    else if (action == "init")
    {
        return GenLuaInit(argc, argv);
    }
    else
    {
        PrintUsage();
        return 1;
    }
}






















