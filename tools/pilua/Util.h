//
//  Util.h
//  pi
//
//  Created by Weibin Luo on 28/06/2017.
//  Copyright © 2017 pi. All rights reserved.
//

#ifndef Util_h
#define Util_h

void PrintUsage();

string MakeEntryName(const string& genDir, const string& defFile);
string MakeModuleFile(const string& genDir, const string& entryName);

#endif /* Util_h */
