/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.3.20   0.1     Create
 ******************************************************************************/
#include <pi/Core.h>

using namespace nspi;
using namespace std;

string MakeEntryName(const std::string& genDir, const std::string& defFile)
{
    string tmp = defFile.substr(genDir.size(), defFile.size() - genDir.size());
    std::replace(tmp.begin(), tmp.end(), '/', '_');
    tmp = tmp.substr(0, tmp.size() - 4);
    
    return piFormat("lua_%s_Init", tmp.c_str());
}

string MakeModuleFile(const string& genDir, const string& entryName)
{
    string tmp = entryName.substr(4, entryName.size() - 9);
    std::replace(tmp.begin(), tmp.end(), '_', '/');
    tmp += ".cpp";
    return genDir + tmp;
}


void PrintUsage()
{
    printf("Usage:pilua action [options]\n");
    printf("module\n");
    printf("    -h path/to/header/file\n");
    printf("    -o path/to/output/file\n");
    printf("    -g path/to/root/dir");
    printf("pilua init -o /output/file header1.h header2.h ...\n");
    printf("    -o /output/file\n");
}























