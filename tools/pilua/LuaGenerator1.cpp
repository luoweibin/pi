/*******************************************************************************
 The MIT License (MIT)
 Copyright © 2016 <copyright holders>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 History:
 wilburluo     2014.6.21   0.1     Create
 ******************************************************************************/
#include "../IDL.h"
#include <pi/Sandbox.h>

using namespace std;
using namespace nspi;

//将驼峰命名转换成下划线命名
#if 0
static String ScriptName(const String &name)
{
    const char *begin = name.c_str();
    const char *end = begin + name.size();
    const char *c = begin;
    
    String result;
    
    char last = '\0';
    while (c != end)
    {
        if (isupper(*c))
        {
            if (!result.empty() && !isupper(last) && last != '_')
            {
                result.append(1, '_');
            }
        }
        result.append(1, tolower(*c));
        last = *c;
        ++c;
    }
    
    return result;
}
#endif

#if 0
static String ScriptName(const String &name)
{
    string value;
    value.resize(name.size());
    std::transform(name.begin(), name.end(), value.begin(), tolower);
    return value;
}
#endif

static inline string ScriptName(const String &name)
{
    return name;
}

#if 0
static string GenericName(const string& name)
{
    piAssert(!name.empty(), name);
    return name.substr(1, name.size() - 1);
}

static bool IsGetter(const string& name)
{
    return name.find("Get") == 0;
}

static bool IsSetter(const string& name)
{
    return name.find("Set") == 0;
}
#endif

static void GenLicense(iStream* o)
{
    o->WriteString("/*******************************************************************************\n");
    o->WriteString(" See copyright notice in LICENSE.\n");
    o->WriteString(" ******************************************************************************/\n");
}

static void GenConst(iConstant* c, iStream* o)
{
    String name = c->GetName();
    o->WriteString(piFormat("    lua_pushinteger(L, %d);\n", c->GetValue()));
    o->WriteString(piFormat("    lua_setglobal(L, \"%s\");\n", name.c_str()));
}

static String GetFunctionName(iFunction* func)
{
    return ScriptName(func->GetName());
}

static void GenParam(int i, iParam* p, iStream* o)
{
    SmartPtr<iType> type = p->GetType();
    if (type->IsArray())
    {
        SmartPtr<iSimpleType> sType = type->GetType1();
        switch (sType->GetId())
        {
            case eType_I8:
                o->WriteString(piFormat("    vector<int8_t> a%d;\n", i, i + 1));
                break;
            case eType_I16:
                o->WriteString(piFormat("    vector<int16_t> a%d;\n", i, i + 1));
                break;
            case eType_I32:
                o->WriteString(piFormat("    vector<int32_t> a%d;\n", i, i + 1));
                break;
            case eType_I64:
                o->WriteString(piFormat("    vector<int64_t> a%d;\n", i, i + 1));
                break;
            case eType_U8:
                o->WriteString(piFormat("    vector<uint8_t> a%d;\n", i, i + 1));
                break;
            case eType_U16:
                o->WriteString(piFormat("    vector<uint16_t> a%d;\n", i, i + 1));
                break;
            case eType_U32:
                o->WriteString(piFormat("    vector<uint32_t> a%d;\n", i, i + 1));
                break;
            case eType_U64:
                o->WriteString(piFormat("    vector<uint64_t> a%d;\n", i, i + 1));
                break;
            case eType_F32:
                o->WriteString(piFormat("    vector<float> a%d;\n", i, i + 1));
                break;
            case eType_F64:
                o->WriteString(piFormat("    vector<double> a%d;\n", i, i + 1));
                break;
            case eType_Vec2:
                o->WriteString(piFormat("    vector<vec2> a%d;\n", i, i + 1));
                break;
            case eType_Vec3:
                o->WriteString(piFormat("    vector<vec3> a%d;\n", i, i + 1));
                break;
            case eType_Vec4:
                o->WriteString(piFormat("    vector<vec4> a%d;\n", i, i + 1));
                break;
            case eType_Quat:
                o->WriteString(piFormat("    vector<quat> a%d;\n", i, i + 1));
                break;
            case eType_Mat4:
                o->WriteString(piFormat("    vector<mat4> a%d;\n", i, i + 1));
                break;
            case eType_Rect:
                o->WriteString(piFormat("    vector<rect> a%d;\n", i, i + 1));
                break;
            case eType_AABB:
                o->WriteString(piFormat("    vector<aabb> a%d;\n", i, i + 1));
                break;
            default:
                break;
        }
    }
    else if (type->IsTable())
    {
        
    }
    else
    {
        SmartPtr<iSimpleType> t = type->GetType1();
        switch (t->GetId())
        {
            case eType_Boolean:
                o->WriteString(piFormat("    auto a%d = lua_toboolean(L, %d);\n", i, i + 1));
                break;
            case eType_I8:
                o->WriteString(piFormat("    auto a%d = (int8_t)lua_tointeger(L, %d);\n", i, i + 1));
                break;
            case eType_U8:
                o->WriteString(piFormat("    auto a%d = (uint8_t)lua_tointeger(L, %d);\n", i, i + 1));
                break;
            case eType_I16:
                o->WriteString(piFormat("    auto a%d = (int16_t)lua_tointeger(L, %d);\n", i, i + 1));
                break;
            case eType_U16:
                o->WriteString(piFormat("    auto a%d = (uint16_t)lua_tointeger(L, %d);\n", i, i + 1));
                break;
            case eType_I32:
                o->WriteString(piFormat("    auto a%d = (int32_t)lua_tointeger(L, %d);\n", i, i + 1));
                break;
            case eType_U32:
                o->WriteString(piFormat("    auto a%d = (uint32_t)lua_tointeger(L, %d);\n", i, i + 1));
                break;
            case eType_I64:
                o->WriteString(piFormat("    auto a%d = (int64_t)lua_tointeger(L, %d);\n", i, i + 1));
                break;
            case eType_U64:
                o->WriteString(piFormat("    auto a%d = (uint64_t)lua_tointeger(L, %d);\n", i, i + 1));
                break;
            case eType_String:
                o->WriteString(piFormat("    string a%d = lua_tostring(L, %d);\n", i, i + 1));
                break;
            case eType_F32:
                o->WriteString(piFormat("    auto a%d = (float)lua_tonumber(L, %d);\n", i, i + 1));
                break;
            case eType_F64:
                o->WriteString(piFormat("    auto a%d = lua_tonumber(L, %d);\n", i, i + 1));
                break;
            case eType_Enum:
                o->WriteString(piFormat("    auto a%d = (int)lua_tointeger(L, %d);\n", i, i + 1));
                break;
            case eType_Vec2:
                o->WriteString(piFormat("    auto a%d = !lua_isnil(L, %d) ? *(vec2*)lua_touserdata(L, %d) : vec2();\n", i, i + 1, i + 1));
                break;
            case eType_Vec3:
                o->WriteString(piFormat("    auto a%d = !lua_isnil(L, %d) ? *(vec3*)lua_touserdata(L, %d) : vec3();\n", i, i + 1, i + 1));
                break;
            case eType_Vec4:
                o->WriteString(piFormat("    auto a%d = !lua_isnil(L, %d) ? *(vec4*)lua_touserdata(L, %d) : vec4();\n", i, i + 1, i + 1));
                break;
            case eType_Rect:
                o->WriteString(piFormat("    auto a%d = !lua_isnil(L, %d) ? *(rect*)lua_touserdata(L, %d) : rect();\n", i, i + 1, i + 1));
                break;
            case eType_Quat:
                o->WriteString(piFormat("    auto a%d = !lua_isnil(L, %d) ? *(quat*)lua_touserdata(L, %d) : quat();\n", i, i + 1, i + 1));
                break;
            case eType_Mat4:
                o->WriteString(piFormat("    auto a%d = !lua_isnil(L, %d) ? *(mat4*)lua_touserdata(L, %d) : mat4();\n", i, i + 1, i + 1));
                break;
            case eType_AABB:
                o->WriteString(piFormat("    auto a%d = !lua_isnil(L, %d) ? *(aabb*)lua_touserdata(L, %d) : aabb();\n", i, i + 1, i + 1));
                break;
            default:
            {
                string name = t->GetName();
                o->WriteString(piFormat("    auto a%d = (%s*)QueryLuaObject<%s>(L, %d);\n", i, name.c_str(), name.c_str(), i + 1));
                break;
            }
        }
    }
}

static void GenPushValue(iType* type, const String& name, iStream* o)
{
    if (type->IsArray())
    {
        
    }
    else if (type->IsTable())
    {
        
    }
    else
    {
        SmartPtr<iSimpleType> t = type->GetType1();
        switch (t->GetId())
        {
            case eType_Boolean:
                o->WriteString(piFormat("    lua_pushboolean(L, %s);\n", name.c_str()));
                break;
            case eType_I8:
            case eType_U8:
            case eType_I16:
            case eType_U16:
            case eType_I32:
            case eType_U32:
            case eType_I64:
            case eType_U64:
                o->WriteString(piFormat("    lua_pushinteger(L, %s);\n", name.c_str()));
                break;
            case eType_String:
                o->WriteString(piFormat("    lua_pushstring(L, %s.c_str());\n", name.c_str()));
                break;
            case eType_F32:
            case eType_F64:
                o->WriteString(piFormat("    lua_pushnumber(L, %s);\n", name.c_str()));
                break;
            case eType_Enum:
                o->WriteString(piFormat("    lua_pushinteger(L, %s);\n", name.c_str()));
                break;
            case eType_Var:
                o->WriteString(piFormat("    Var_Value(L, %s);\n", name.c_str()));
                break;
            default:
                o->WriteString(piFormat("    %s_Create(L, %s);\n", type->GetType1()->GetName().c_str(), name.c_str()));
                break;
        }
    }
}

static void GenFunctionDefiniton(iFunction* func, iStream* o)
{
    String name = GetFunctionName(func);
    o->WriteString(piFormat("static int lua_%s(lua_State* L) {\n", name.c_str()));
    vector<SmartPtr<iParam>> params = func->GetParams();
    for (int i = 0; i < params.size(); ++i)
    {
        auto param = params[i];
        GenParam(i, param, o);
    }
    
    SmartPtr<iType> retType = func->GetReturnType();
    if (!retType->IsTable() && !retType->IsArray() && retType->GetType1()->GetId() != eType_Void)
    {
        o->WriteString(piFormat("    auto ret = %s(", func->GetName().c_str()));
        
        for (int i = 0; i < params.size(); ++i)
        {
            o->WriteString(piFormat("a%d", i));
            if (i < params.size() - 1)
            {
                o->WriteString(",");
            }
        }
        
        o->WriteString(");\n");
        
        GenPushValue(func->GetReturnType(), "ret", o);
    }
    else
    {
        o->WriteString(piFormat("    %s(", func->GetName().c_str()));
        for (int i = 0; i < params.size(); ++i)
        {
            o->WriteString(piFormat("a%d", i));
            
            if (i < params.size() - 1)
            {
                o->WriteString(",");
            }
        }
        o->WriteString(");\n");
    }
    
    if (retType->GetType1()->GetId() != eType_Void)
    {
        o->WriteString("    return 1;\n");
    }
    else
    {
        o->WriteString("    return 0;\n");
    }
    
    o->WriteString("}\n");
}

static void GenFunctionReg(iFunction* func, iStream* o)
{
    String name = GetFunctionName(func);
    
    o->WriteString(piFormat("    lua_pushcfunction(L, lua_%s);\n", name.c_str()));
    o->WriteString(piFormat("    lua_setglobal(L, \"%s\");\n", name.c_str()));
}

/**
 * =================================================================================================
 * Generate interface definition
 */

static void GenMethodDefinition(iInterface* i, iFunction* m, iStream* o)
{
    string ifName = i->GetName();
    
    string name = m->GetName();
    
    o->WriteString(piFormat("static int %s_%s(lua_State* L)\n", ifName.c_str(), name.c_str()));
    o->WriteString("{\n");
    
    o->WriteString(piFormat("    auto refObject = QueryLuaObject<%s>(L, 1);\n", ifName.c_str()));
    
    vector<SmartPtr<iParam>> params = m->GetParams();
    for (int i = 0; i < params.size(); ++i)
    {
        auto param = params[i];
        GenParam(i + 1, param, o);
    }
    
    SmartPtr<iType> retType = m->GetReturnType();
    if (!retType->IsTable() && !retType->IsArray() && retType->GetType1()->GetId() != eType_Void)
    {
        o->WriteString(piFormat("    auto ret = refObject->%s(", m->GetName().c_str()));
        
        for (int i = 0; i < params.size(); ++i)
        {
            o->WriteString(piFormat("a%d", i + 1));
            if (i < params.size() - 1)
            {
                o->WriteString(",");
            }
        }
        
        o->WriteString(");\n");
        
        GenPushValue(retType, "ret", o);
    }
    else
    {
        o->WriteString(piFormat("    refObject->%s(", m->GetName().c_str()));
        for (int i = 0; i < params.size(); ++i)
        {
            o->WriteString(piFormat("a%d", i + 1));
            
            if (i < params.size() - 1)
            {
                o->WriteString(",");
            }
        }
        o->WriteString(");\n");
    }
    
    if (retType->GetType1()->GetId() != eType_Void)
    {
        o->WriteString("    return 1;\n");
    }
    else
    {
        o->WriteString("    return 0;\n");
    }
    
    o->WriteString("}\n");
}

static void GenInterfaceReg(iModule* module, iInterface* i, iStream* o)
{
    string ifName = i->GetName();
    
    auto methods = i->GetMethods();
    for (auto m : methods)
    {
        GenMethodDefinition(i, m, o);
    }
        
    o->WriteString(piFormat("void %s_Init(lua_State* L, bool derived) {\n", ifName.c_str()));
    
    o->WriteString("    if (!derived)\n");
    o->WriteString("    {\n");
    o->WriteString(piFormat("        luaL_newmetatable(L, \"%s\");\n", ifName.c_str()));
    
    // Write type information into metatable
    o->WriteString("        lua_pushinteger(L, eType_Object);\n");
    o->WriteString("        lua_setfield(L, -2, \"__type\");\n");
    
    o->WriteString(piFormat("        lua_pushstring(L, \"%s\");\n", ifName.c_str()));
    o->WriteString("        lua_setfield(L, -2, \"__typename\");\n");
    
    // Write GC method
    o->WriteString(piFormat("        lua_pushcfunction(L, %s_GC);\n", ifName.c_str()));
    o->WriteString("        lua_setfield(L, -2, \"__gc\");\n");
    
    // Write tostring method
    o->WriteString(piFormat("        lua_pushcfunction(L, %s_ToString);\n", ifName.c_str()));
    o->WriteString("        lua_setfield(L, -2, \"__tostring\");\n");
        
    o->WriteString("        lua_newtable(L);\n");
    
    o->WriteString("    }\n");
    
    for (auto m : methods)
    {
        string name = m->GetName();
        auto params = m->GetParams();
        
        string scriptName = ScriptName(name);
        
        o->WriteString(piFormat("    lua_pushcfunction(L, %s_%s);\n", ifName.c_str(), scriptName.c_str()));
        o->WriteString(piFormat("    lua_setfield(L, -2, \"%s\");\n", scriptName.c_str()));
    }
    
    SmartPtr<iInterface> current = i;
    while (true)
    {
        string parentName = current->GetSuperInterface();
        if (parentName.empty())
        {
            break;
        }
        
        SmartPtr<iInterface> parent = module->GetInterface(parentName);
        if (parent.IsNull())
        {
            break;
        }
        
        o->WriteString(piFormat("    %s_Init(L, true);\n", parentName.c_str()));
        
        current = parent;
    }
    
    o->WriteString("    if (!derived)\n");
    o->WriteString("    {\n");
    o->WriteString("        lua_setfield(L, -2, \"__index\");\n");
    o->WriteString("        lua_pop(L, 1);\n");
    o->WriteString("    }\n");
    o->WriteString("};\n");
}

static void GenInterfaceConstructor(iInterface* i, iStream* o)
{
    string name = i->GetName();
    o->WriteString(piFormat("void nspi::%s_Create(lua_State* L, %s* o)\n", name.c_str(), name.c_str()));
    o->WriteString("{\n");
    o->WriteString(piFormat("    CreateLuaObject<%s>(L, o);\n", name.c_str()));
    o->WriteString(piFormat("    luaL_getmetatable(L, \"%s\");\n", name.c_str()));
    o->WriteString("    lua_setmetatable(L, -2);\n");
    o->WriteString("}\n");
}

static void GenInterfaceDestructor(iInterface* i, iStream* o)
{
    string name = i->GetName();
    o->WriteString(piFormat("static int %s_GC(lua_State* L)\n", name.c_str(), name.c_str()));
    o->WriteString("{\n");
    o->WriteString(piFormat("    auto refObject = QueryLuaObject<%s>(L, 1);\n", name.c_str()));
    o->WriteString("    refObject->Release();\n");
    o->WriteString("    return 0;\n");
    o->WriteString("}\n");
}

static void GenInterfaceToString(iInterface* i, iStream* o)
{
    string name = i->GetName();
    o->WriteString(piFormat("static int %s_ToString(lua_State* L)\n", name.c_str(), name.c_str()));
    o->WriteString("{\n");
    o->WriteString(piFormat("    auto refObject = QueryLuaObject<%s>(L, 1);\n", name.c_str()));
    o->WriteString("    auto str = refObject->ToString();\n");
    o->WriteString("    lua_pushstring(L, str.c_str());\n");
    o->WriteString("    return 1;\n");
    o->WriteString("}\n");
}

static void GenInterfaceDefinition(iModule* module, iInterface* i, iStream* o)
{
    GenInterfaceConstructor(i, o);
    GenInterfaceDestructor(i, o);
    GenInterfaceToString(i, o);
    GenInterfaceReg(module, i, o);
}

static void GenInterfaceDec(iInterface* i, iStream* srcPI, iStream* hdrPI)
{
    string name = i->GetName();
    hdrPI->WriteString(piFormat("void %s_Init(lua_State* L, bool derived);\n", name.c_str()));
}

static void GenInterfaceRegInit(iInterface* i, iStream* o)
{
    o->WriteString(piFormat("    %s_Init(L, false);\n", i->GetName().c_str()));
}

/**
 * =================================================================================================
 *
 */

static void InitSrcPI(const vector<SmartPtr<iInterface>> ifs, iStream* srcPI)
{
    srcPI->WriteString("#include <pi/Game.h>\n");
    srcPI->WriteString("#include \"PI.h\"\n");
    srcPI->WriteString("#include \"../Wrapper.h\"\n");
    
    srcPI->WriteString("using namespace nspi;\n");
    srcPI->WriteString("using namespace std;\n");
    
    for (auto i : ifs)
    {
        string name = i->GetName();
        srcPI->WriteString(piFormat("#include \"%s.cpp\"\n", name.c_str()));
    }
}

static void InitHdrPI(iStream* hdrPI)
{
    hdrPI->WriteString("#ifndef PI_BINDING_LUA_GEN_PI_H\n");
    hdrPI->WriteString("#define PI_BINDING_LUA_GEN_PI_H\n");
    
    hdrPI->WriteString("#include <lua.hpp>\n");
    hdrPI->WriteString("#include <pi/lua/PILua.h>\n");
    
    hdrPI->WriteString("using namespace std;\n");
    hdrPI->WriteString("using namespace nspi;\n");
}

static void GenExternalHeader(iStream* o, const vector<SmartPtr<iInterface>>& ifs)
{
    GenLicense(o);
    
    o->WriteString("namespace nspi\n");
    o->WriteString("{\n");
    
    //Generate interface definition
    for (auto i : ifs)
    {
        string name = i->GetName();
        o->WriteString(piFormat("    void %s_Create(lua_State* L, %s* o);\n", name.c_str(), name.c_str()));
    }
    
    o->WriteString("}\n");
}

void GenLua(iModule *module, iSandbox *sandbox)
{
#define SRC_GEN "gen"
#define HDR_GEN "include/pi/lua/gen"
    
    if (!sandbox->DirectoryExist(SRC_GEN))
    {
        piAssert(sandbox->CreateDirectory(SRC_GEN, true), ;);
    }
    
    if (!sandbox->DirectoryExist(HDR_GEN))
    {
        piAssert(sandbox->CreateDirectory(HDR_GEN, true), ;);
    }
    
    SmartPtr<iSandbox> srcSandbox = sandbox->OpenSandbox(SRC_GEN);
    SmartPtr<iSandbox> hdrSandbox = sandbox->OpenSandbox(HDR_GEN);
    
    SmartPtr<iStream> srcPI = srcSandbox->Open("PI.cpp", "w");
    if (srcPI.IsNull())
    {
        printf("Failed to create file '%s'\n", srcSandbox->Resolve("PI.cpp").c_str());
        return;
    }
    GenLicense(srcPI);
    
    SmartPtr<iStream> hdrPI = srcSandbox->Open("PI.h", "w");
    if (srcPI.IsNull())
    {
        printf("Failed to create file '%s'\n", srcSandbox->Resolve("PI.h").c_str());
        return;
    }
    GenLicense(hdrPI);
    
    vector<SmartPtr<iConstant>> consts = module->GetConstants();
    vector<SmartPtr<iFunction>> funcs = module->GetFunctions();
    vector<SmartPtr<iInterface>> ifs = module->GetInterfaces();
    
    InitSrcPI(ifs, srcPI);
    InitHdrPI(hdrPI);
    
    //Generate global function definition
    for (auto func : funcs)
    {
        GenFunctionDefiniton(func, srcPI);
    }
    
    //Generate interface definition
    for (auto i : ifs)
    {
        string name = i->GetName();
        string path = piFormat("%s.cpp", name.c_str());
        
        SmartPtr<iStream> stream = srcSandbox->Open(path, "w");
        if (stream.IsNull())
        {
            printf("Failed to create file '%s'\n", srcSandbox->Resolve(path).c_str());
            return;
        }
        
        GenLicense(stream);
        
        GenInterfaceDefinition(module, i, stream);
        GenInterfaceDec(i, srcPI, hdrPI);
    }
    
    //lua库初始化函数
    srcPI->WriteString("void piInitPI(lua_State* L)");
    srcPI->WriteString("{\n");
    
    for (auto i : consts)
    {
        GenConst(i, srcPI);
    }
    
    for (auto func : funcs)
    {
        GenFunctionReg(func, srcPI);
    }
    
    for (auto i : ifs)
    {
        GenInterfaceRegInit(i, srcPI);
    }
    
    srcPI->WriteString("}\n");
    
    hdrPI->WriteString("#endif\n");
    
    SmartPtr<iStream> PIGen = hdrSandbox->Open("PIGen.h", "w");
    if (PIGen.IsNull())
    {
        printf("Failed to create file '%s'\n", sandbox->Resolve("PIGen.h").c_str());
        return;
    }
    GenExternalHeader(PIGen, ifs);
}




























