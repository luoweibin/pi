/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.5   0.1     Create
 ******************************************************************************/
#include "../pih/Parser.h"
#include "CppGenerator.h"
#include <pi/Crypto.h>

using namespace nspi;

static void PrintHelp()
{
    printf("Usage:pic path/to/headerfile path/to/output/file\n");
}

static int ParseHeader(const string& inFile, Module& module)
{
    SmartPtr<iStream> source = CreateFileStream(inFile, "r");
    if (source.IsNull())
    {
        printf("File not found:%s\n", inFile.c_str());
        return 1;
    }
    
    SmartPtr<iReaderUTF8> reader = CreateReaderUTF8(source, 1024);
    piAssert(!reader.IsNull(), 1);
    
    Lexer lexer(reader);
    Parser parser(lexer);
    if (!parser.Parse(module))
    {
        printf("Unable to parse '%s'.", inFile.c_str());
        return 1;
    }
    
    return 2;
}

int main(int argc, const char * argv[])
{
    piInit();
    
    if (argc < 3)
    {
        PrintHelp();
        return 1;
    }

    Module module;
    string inFile = argv[1];
    int ret = ParseHeader(inFile, module);
    piCheck(ret >= 2, ret);
    
    bool write = !module.interfaces.empty();
    if (!write)
    {
        write = !module.constants.empty();
    }
    
    piCheck(write, 0);
    
    string outFile = argv[2];
    string dir = piGetDirectory(outFile);
    
    if (!piDirectoryExist(dir) && !CreateDirectory(dir, true))
    {
        printf("Failed to create directory '%s'\n", dir.c_str());
        return 1;
    }
    
    SmartPtr<iStream> output = CreateFileStream(outFile, "w");
    if (output.IsNull())
    {
        printf("Unable to open file '%s'.\n", outFile.c_str());
        return 1;
    }
    
    SmartPtr<iHash> hash = CreateHash(eHash_MD5);
    hash->Update(outFile.c_str(), outFile.size());
    string uuid = hash->FinalHex();
        
    CppGenerator generator(module);
    if (generator.Generate(output, uuid))
    {
        printf("Generate '%s' for '%s'.\n", outFile.c_str(), inFile.c_str());
    }
    
    return 0;
}
























