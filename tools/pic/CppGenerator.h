/*******************************************************************************
 See copyright notice in LICENSE.
 
 History:
 luo weibin     2017.2.6   0.1     Create
 ******************************************************************************/
#ifndef PI_TOOLS_PIC_CPPGENERATOR_H
#define PI_TOOLS_PIC_CPPGENERATOR_H

#include "../pih/Parser.h"

namespace nspi
{
    
    class CppGenerator
    {
    private:
        Module& mModule;
        
    public:
        CppGenerator(Module& module);
        
        bool Generate(iStream* output, const string& uuid);
    };
    
}

#endif

























