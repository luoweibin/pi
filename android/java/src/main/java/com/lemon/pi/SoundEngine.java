package com.lemon.pi;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.Log;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by cwq on 2017/7/11.
 */

class SoundEngine {

    private static final int MAX_SIMULTANEOUS_STREAMS_DEFAULT = 5;
    private static final int SOUND_QUALITY = 0;
    private static final int SOUND_PRIORITY = 1;

    private static final int INVALID_SOUNDPOOL_SOUND_ID = 0;
    private static final int INVALID_SOUNDPOOL_STREAM_ID = 0;

    private static final int LOAD_TIME_OUT = 500;

    private final Context mContext;
    private SoundPool mSoundPool;
    private int mSoundId;

    // path - soundPool.soundId
    private HashMap<String, Integer> mSoundCache = new HashMap<>();

    // soundPool.soundId - LoadSoundSyncInfo
    private class LoadSoundSyncInfo {
        private boolean completed = false;
    }
    private ConcurrentHashMap<Integer, LoadSoundSyncInfo> mLoadingSounds= new ConcurrentHashMap<>();

    // soundId - SoundPlayer
    private class SoundPlayer {
        private int soundPoolSoundId;
        private int soundPoolStreamID = INVALID_SOUNDPOOL_STREAM_ID;
        private boolean isLoop = false;
        private String filePath;

        private SoundPlayer(String path, int soundId) {
            filePath = path;
            soundPoolSoundId = soundId;
        }

        private String getFilePath() {
            return filePath;
        }

        private void setLoop(boolean loop) {
            isLoop = loop;
            mSoundPool.setLoop(soundPoolStreamID, isLoop ? -1 : 0);
        }

        private void play() {
            mSoundPool.stop(soundPoolStreamID);
            soundPoolStreamID = mSoundPool.play(soundPoolSoundId, 1.0f, 1.0f, SOUND_PRIORITY, isLoop ? -1 : 0, 1.0f);
        }

        private void pause() {
            mSoundPool.pause(soundPoolStreamID);
        }

        private void resume() {
            mSoundPool.resume(soundPoolStreamID);
        }

        private void stop() {
            mSoundPool.stop(soundPoolStreamID);
        }
    }
    private HashMap<Integer, SoundPlayer> mSoundPlayers = new HashMap<>();

    SoundEngine(final Context context) {
        mContext = context;
        mSoundId = 0;
    }

    public boolean start() {
        if (mSoundPool == null) {
            mSoundPool = new SoundPool(MAX_SIMULTANEOUS_STREAMS_DEFAULT, AudioManager.STREAM_MUSIC, SOUND_QUALITY);
            mSoundPool.setOnLoadCompleteListener(new OnLoadCompletedListener());
        }
        return (mSoundPool != null);
    }

    private int loadSoundSync(final String filePath) {
        Integer soundIDInteger = mSoundCache.get(filePath);
        if (soundIDInteger != null)
            return soundIDInteger;

        int soundID = INVALID_SOUNDPOOL_SOUND_ID;

        try {
            if (filePath.startsWith("/")) {
                soundID = this.mSoundPool.load(filePath, 0);
            } else {
                soundID = this.mSoundPool.load(this.mContext.getAssets().openFd(filePath), 0);
            }
        } catch (final Exception e) {
            soundID = INVALID_SOUNDPOOL_SOUND_ID;
            Log.e(AudioEngine.TAG, "error: " + e.getMessage(), e);
        }

        // mSoundPool.load returns 0 if something goes wrong, for example a file does not exist
        if (soundID == 0) {
            soundID = INVALID_SOUNDPOOL_SOUND_ID;
        }

        if (soundID != INVALID_SOUNDPOOL_SOUND_ID) {
            // TODO: if already load completed
            // wait load completed
            LoadSoundSyncInfo info = new LoadSoundSyncInfo();
            mLoadingSounds.put(soundID, info);
            synchronized (info) {
                if (!info.completed) {
                    try {
                        info.wait(LOAD_TIME_OUT);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            mLoadingSounds.remove(soundID);

            mSoundCache.put(filePath, soundID);
        }

        return soundID;
    }

    public boolean preloadSound(final String filePath) {
        int soundID = loadSoundSync(filePath);
        if (soundID != INVALID_SOUNDPOOL_SOUND_ID) {
            return true;
        }
        return false;
    }

    // positive soundId if successful, zero if failed
    public int createSoundId(final String filePath) {
        int soundID = loadSoundSync(filePath);
        if (soundID != INVALID_SOUNDPOOL_SOUND_ID) {
            ++mSoundId;
            SoundPlayer player = new SoundPlayer(filePath, soundID);
            mSoundPlayers.put(mSoundId, player);
            return soundID;
        }
        return 0;
    }

    public void soundSetLoop(final int soundId, final boolean loop) {
        SoundPlayer player = mSoundPlayers.get(soundId);
        if (player != null) {
            player.setLoop(loop);
        }
    }

    public void playSound(final int soundId) {
        SoundPlayer player = mSoundPlayers.get(soundId);
        if (player != null) {
            player.play();
        }
    }

    public void pauseSound(final int soundId) {
        SoundPlayer player = mSoundPlayers.get(soundId);
        if (player != null) {
            player.pause();
        }
    }

    public void resumeSound(final int soundId) {
        SoundPlayer player = mSoundPlayers.get(soundId);
        if (player != null) {
            player.resume();
        }
    }

    public void stopSound(final int soundId) {
        SoundPlayer player = mSoundPlayers.get(soundId);
        if (player != null) {
            player.stop();
        }
    }

    public void destroySoundId(final int soundId) {
        SoundPlayer player = mSoundPlayers.get(soundId);
        if (player != null) {
            player.stop();
            mSoundPlayers.remove(soundId);
        }
    }

    public void unloadSound(final String filePath) {
        Iterator<Map.Entry<Integer, SoundPlayer>> it = mSoundPlayers.entrySet().iterator();
        while (it.hasNext()) {
            SoundPlayer player = it.next().getValue();
            if ((player != null) && (player.getFilePath().equals(filePath))) {
                player.stop();
                it.remove();
            }
        }

        Integer soundID = mSoundCache.get(filePath);
        if (soundID != null) {
            mSoundPool.unload(soundID);
            mSoundCache.remove(filePath);
        }
    }

    public void stopAllSound() {
        for (Map.Entry<Integer, SoundPlayer> entry : mSoundPlayers.entrySet()) {
            entry.getValue().stop();
        }
    }

    public void unloadAllSound() {
        for (Map.Entry<Integer, SoundPlayer> entry : mSoundPlayers.entrySet()) {
            entry.getValue().stop();
        }
        mSoundPlayers.clear();

        for (Map.Entry<String, Integer> entry : mSoundCache.entrySet()) {
            mSoundPool.unload(entry.getValue());
        }
        mSoundCache.clear();
    }

    public void stop() {
        if (mSoundPool != null) {
            mSoundPool.release();
            mSoundPool = null;
        }
        mSoundCache.clear();
        mLoadingSounds.clear();
    }

    private class OnLoadCompletedListener implements SoundPool.OnLoadCompleteListener {

        @Override
        public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
            // This callback is in main thread.
            if (status == 0)
            {
                LoadSoundSyncInfo info = mLoadingSounds.get(sampleId);

                if (info != null) {
                    synchronized (info) {
                        info.completed = true;
                        info.notifyAll();
                    }
                }
            }
        }
    }
}
