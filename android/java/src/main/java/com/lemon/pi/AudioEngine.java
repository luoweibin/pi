package com.lemon.pi;

import android.util.Log;

/**
 * Created by cwq on 2017/7/10.
 */

class AudioEngine {

    static final String TAG = "PI-MEDIA";

    private boolean mIsStarted;
    private BGMController mBGMController;
    private SoundEngine mSoundEngine;
    
    static void init() {
        // init
    }

    AudioEngine() {
        mIsStarted = false;
        if (PIEngine.mContext != null) {
            mBGMController = new BGMController(PIEngine.mContext);
            mSoundEngine = new SoundEngine(PIEngine.mContext);
        } else {
            Log.e(TAG, "AudioEngine did not init!");
        }
    }

    public boolean start() {
        if (!mIsStarted) {
            mIsStarted = mSoundEngine.start();
        }
        return mIsStarted;
    }

    public void stop() {
        if (mIsStarted) {
            mBGMController.stopBackgroundMusic();
            mBGMController.releaseBackgroundMusic();
            mSoundEngine.stop();
            mIsStarted = false;
        }
    }

    // music
    public boolean preloadBackgroundMusic(final String filePath) {
        return mBGMController.preloadBackgroundMusic(filePath);
    }

    public void backgroundMusicSetLoop(final boolean loop) {
        mBGMController.setBackgroundMusicLoop(loop);
    }

    public void playBackgroundMusic(final String filePath) {
        mBGMController.playBackgroundMusic(filePath);
    }

    public void pauseBackgroundMusic() {
        mBGMController.pauseBackgroundMusic();
    }

    public void resumeBackgroundMusic() {
        mBGMController.resumeBackgroundMusic();
    }

    public void rewindBackgroundMusic() {
        mBGMController.rewindBackgroundMusic();
    }

    public void stopBackgroundMusic() {
        mBGMController.stopBackgroundMusic();
    }

    public void releaseBackgroundMusic() {
        mBGMController.releaseBackgroundMusic();
    }

    // sound
    public boolean preloadSound(final String filePath) {
        return mSoundEngine.preloadSound(filePath);
    }

    // positive soundId if successful, zero if failed
    public int createSoundId(final String filePath) {
        return mSoundEngine.createSoundId(filePath);
    }

    public void soundSetLoop(final int soundId, final boolean loop) {
        mSoundEngine.soundSetLoop(soundId, loop);
    }

    public void playSound(final int soundId) {
        mSoundEngine.playSound(soundId);
    }

    public void pauseSound(final int soundId) {
        mSoundEngine.pauseSound(soundId);
    }

    public void resumeSound(final int soundId) {
        mSoundEngine.resumeSound(soundId);
    }

    public void stopSound(final int soundId) {
        mSoundEngine.stopSound(soundId);
    }

    public void destroySoundId(final int soundId) {
        mSoundEngine.destroySoundId(soundId);
    }

    public void unloadSound(final String filePath) {
        mSoundEngine.unloadSound(filePath);
    }

    public void stopAllSound() {
        mSoundEngine.stopAllSound();
    }

    public void unloadAllSound() {
        mSoundEngine.unloadAllSound();
    }
}
