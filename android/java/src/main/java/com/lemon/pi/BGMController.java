package com.lemon.pi;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.util.Log;

import java.io.FileInputStream;

/**
 * Created by cwq on 2017/7/10.
 */

class BGMController {

    private final Context mContext;
    private MediaPlayer mMediaPlayer;
    private boolean mPaused = false; // whether music is paused state.
    private boolean mStopped = false;
    private boolean mIsLoop = false;
    private String mCurrentPath;

    BGMController(final Context context) {
        this.mContext = context;
    }

    private MediaPlayer createMediaPlayer(final String path) {
        MediaPlayer mediaPlayer = new MediaPlayer();

        try {
            if (path.startsWith("/")) {
                final FileInputStream fis = new FileInputStream(path);
                mediaPlayer.setDataSource(fis.getFD());
                fis.close();
            } else {
                final AssetFileDescriptor assetFileDescriptor = this.mContext.getAssets().openFd(path);
                mediaPlayer.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
            }

            mediaPlayer.prepare();
            mStopped = false;
        } catch (final Exception e) {
            mediaPlayer = null;
            Log.e(AudioEngine.TAG, "error: " + e.getMessage(), e);
        }

        return mediaPlayer;
    }

    public boolean preloadBackgroundMusic(final String path) {
        if ((this.mCurrentPath == null) || (!this.mCurrentPath.equals(path))) {
            // preload new background music

            // release old resource and create a new one
            if (this.mMediaPlayer != null) {
                this.mMediaPlayer.release();
            }

            this.mMediaPlayer = this.createMediaPlayer(path);

            // record the path
            this.mCurrentPath = path;
        }
        
        return mMediaPlayer != null;
    }
    
    public void setBackgroundMusicLoop(final boolean loop) {
        mIsLoop = loop;
        if (mMediaPlayer != null) {
            mMediaPlayer.setLooping(loop);
        }
    }

    public void playBackgroundMusic(final String path) {
        if (preloadBackgroundMusic(path)) {
            try {
                // if the music is playing or paused, stop it
                if (mPaused) {
                    mMediaPlayer.seekTo(0);
                    mMediaPlayer.start();
                } else if (mMediaPlayer.isPlaying()) {
                    mMediaPlayer.seekTo(0);
                } else {
                    if (mStopped) {
                        mMediaPlayer.prepare();
                    }
                    mMediaPlayer.start();
                }
                mMediaPlayer.setLooping(mIsLoop);
                mPaused = false;
                mStopped = false;
            } catch (final Exception e) {
                Log.e(AudioEngine.TAG, "playBackgroundMusic: error state");
            }
        }
    }

    public void pauseBackgroundMusic() {
        try {
            if (this.mMediaPlayer != null && this.mMediaPlayer.isPlaying()) {
                this.mMediaPlayer.pause();
                this.mPaused = true;
            }
        } catch (IllegalStateException e) {
            Log.e(AudioEngine.TAG, "pauseBackgroundMusic, IllegalStateException was triggered!");
        }
    }

    public void resumeBackgroundMusic() {
        try {
            if (this.mMediaPlayer != null && this.mPaused) {
                this.mMediaPlayer.start();
                this.mPaused = false;
            }
        } catch (IllegalStateException e) {
            Log.e(AudioEngine.TAG, "resumeBackgroundMusic, IllegalStateException was triggered!");
        }
    }

    public void rewindBackgroundMusic() {
        if (this.mMediaPlayer != null) {
            playBackgroundMusic(mCurrentPath);
        }
    }

    public void stopBackgroundMusic() {
        if (this.mMediaPlayer != null) {
            mMediaPlayer.stop();

            mStopped = true;
            this.mPaused = false;
        }
    }

    public void releaseBackgroundMusic() {
        if (this.mMediaPlayer != null) {
            mMediaPlayer.release();

            mMediaPlayer = null;
            mPaused = false;
            mStopped = false;
            mIsLoop = false;
            mCurrentPath = null;
        }
    }
}
