package com.lemon.pi;

import android.content.Context;

/**
 * Created by cwq on 2017/7/12.
 */

public class PIEngine {

    static Context mContext;

    private PIEngine () {

    }

    public static void init(Context applicationContext) {
        mContext = applicationContext;
    }
}
