#include <jni.h>
#include <pi/Core.h>
#include <pi/Game.h>

using namespace nspi;
using namespace std;

#define LOG_LEVEL   LOG_INFO
//#define LOG_TAG     PI_GRAPHICS_TAG
#define LOG_TAG     PI_GAME_TAG

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    JNIEnv* env;
    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_4) != JNI_OK) {
        return -1;
    }

    piInitJNI(vm);

    piInit();
    piSetLogLevel(eLog_Global, LOG_LEVEL);
    piSetLogLevel(eLog_Console, LOG_LEVEL);
    piSetLogFilter(LOG_TAG);

    return JNI_VERSION_1_4;
}

void JNI_OnUnload(JavaVM* vm, void* reserved)
{
    piDeinit();
    piDeinitJNI(vm);
}

















